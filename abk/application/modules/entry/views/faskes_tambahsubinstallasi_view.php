<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1200px;">

        <div class="row">            
            <div class="col-md-12"> 
                <?php 
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_kode)->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                echo "<h3>{$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
                <h2>Data Sub Installasi / Poli</h2>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/tambahsubinstallasi_proses/<?php echo $faskes_kode ?>">

                    <div class="form-group">

                        <hr/>
                        <?php
                         $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode = ? AND parent > 0 AND eselon = X', $faskes_kode)->order('urut ASC');
                        if (count($faskes_installasi) != 0) {
                         echo "<h3> Sub Installasi/Poli Sudah Terdaftar </h3>";
                        ?> 
                       
                        <table  width="60%" style='margin-top: 10px; margin-left: 10px;'>
                            <tr bgcolor="#dddddd" >
                                <td width="80px"><b>| No |</b> &nbsp; </td>
                                <td width="200px"><b>| Installasi|</b> &nbsp; </td>
                                <td width="650px"><b>| Nama Sub Installasi/Poli|</b> &nbsp; </td>
                            </tr>
                        
                        
                            <?php    $no = 1;
                            
                            foreach ($faskes_installasi as $row) {
                                echo "<tr>
                                                    <td width=\"80px\"><b>".$no."</b></td>
													<td width=\"200px\"><b>
													<select disabled=\"disabled\" class=\"form-control\" name=\"parent[]\">"; 
											foreach($this->orm->faskes_installasi->where('faskes_kode = ? AND parent IS NULL', $faskes_kode) as $row_parent) {
												$selected = ($row_parent['faskes_installasi_id']==$row['parent'])?'selected':'';
												echo "<option $selected value=\"".$row_parent['faskes_installasi_id']."\">".$row_parent['installasi_nama']."</option>";
											}		
											echo	"</select>
													</b></td>
                                                    <td width=\"650px\"><b>".$row['installasi_nama']."</b></td>
                                                    </tr>";
                                $no++;
                            }
                        }
                        ?>
                            </table>
                        <hr/>
                        <input type="hidden" name="existing" value="<?php echo count($faskes_installasi) ?>"/>
                        &nbsp; <i class="fa fa-plus"></i> <input  onclick="javascript:addRow('tbl_unit_instalasi');" type="button" value="tambah" class='btn btn-primary'> 
                        <br/>
                        <table id="tbl_unit_instalasi" width="90%" style='margin-top: 10px; margin-left: 10px;'>
                            <tr bgcolor="#dddddd" >
                                <td width="35px"><b>|#|</b> &nbsp; </td>
                                <td width="40px"><b>| No |</b> &nbsp; </td>
                                <td width="180px"><b>| Installasi |</b> &nbsp; </td>
                                <td width="350px"><b>| Nama Sub Installasi/Poli|</b> &nbsp; </td>
                            </tr>

                          

                            <?php
                            echo "<tr>
                                                    <td width=\"35px\"><input onclick=\"javascript:delRow('tbl_unit_instalasi',this.id);\" type=\"button\" value=\"X\" id=\"1\" class='btn btn-danger'/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_unit_installasi[]\" value=\"1\"/></td>
													<td width=\"180px\"><b>
													<select class=\"form-control\" name=\"parent[]\">"; 
											foreach($this->orm->faskes_installasi->where('faskes_kode = ? AND parent IS NULL', $faskes_kode) as $row_parent) {
												$selected = '';
												echo "<option $selected value=\"".$row_parent['faskes_installasi_id']."\">".$row_parent['installasi_nama']."</option>";
											}		
											echo	"</select>
													</b></td>
												   <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"unit_installasi\"   name=\"installasi_nama[]\" value=\"\"/></td>
                                                    </tr>";
                            ?>
                        </table>
                        <datalist id="unit_installasi">
                            <?php foreach ($this->orm->ref_installasi() as $row) { ?>
                                <option value="<?php echo $row['installasi_nama'] ?>">
<?php } ?>      
                        </datalist>
                    </div>  

                    <div class="form-group" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Simpan</button>
                        &nbsp;
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


<?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/grid/standard.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });


    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
