<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		helpers/email
 * @author		xamzo's Developers
 * @copyright           Copyright (c) 2014, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('tmp_mail')) {

    function tmp_mail($title='',$subject='',$reply_name='', $reply_email='', $reciper_name='',$reciper_email='', $message='') {
        $x = &get_instance();
        $x->__library(array('phpmailer', 'smtp', 'lib_email'), 'email');
        $view['option'] = $x->lib_email->config('option');
        $view['sender']['name'] = $x->lib_email->config('name_sender');
        $view['sender']['email'] = $x->lib_email->config('email_sender');   
        $view['reciper']['name'] = $reciper_name;
        $view['reciper']['email'] = $reciper_email;                  
        $view['replyTo']['name'] = $reply_name;
        $view['replyTo']['email'] = $reply_email;                 
        $view['message']=$message;
        $view['mail'] =  $x->phpmailer;
        $view['title'] = $title;
        $view['subject'] = $subject; 
        
        switch ($view['option']) {
            case 'phpmail':
                $x->__view('email','phpmail',$view);
                break;
            case 'smtp':
                $view['smtp_host'] = $x->lib_email->smtp('smtp','host');
                $view['smtp_port'] = $x->lib_email->smtp('smtp','port');
                $view['smtp_username'] = $x->lib_email->smtp('smtp','username');
                $view['smtp_password'] = $x->lib_email->smtp('smtp','password');
                $x->__view('email','smtp',$view);
                break;
            case 'gmail_smtp':
                $view['smtp_host'] = $x->lib_email->smtp('gmail','host');
                $view['smtp_port'] = $x->lib_email->smtp('gmail','port');
                $view['smtp_username'] = $x->lib_email->smtp('gmail','username');
                $view['smtp_password'] = $x->lib_email->smtp('gmail','password');
                $x->__view('email','gmail',$view);
                break;
            default:
                break;
        }
    }

}

if(!function_exists('send_mail')){
    function send_mail($type,$title='',$subject='',$reply_name='', $reply_email='', $reciper_name='', $reciper_email='',$data=array()) { 
        switch ($type) {
            case 'comment':
                $mail_template = x_file_get_content(MODPATH . 'email/views/template/comment' . EXT);
                break;
            case 'password':
                $mail_template = x_file_get_content(MODPATH . 'email/views/template/password' . EXT);
                break;
            case 'reader':
                $mail_template = x_file_get_content(MODPATH . 'email/views/template/reader' . EXT);
                break;
            case 'register':
                $mail_template = x_file_get_content(MODPATH . 'email/views/template/register' . EXT);
                break;
            case 'general':
                $mail_template = x_file_get_content(MODPATH . 'email/views/template/general' . EXT);
                break;
            default:
                $mail_template = x_file_get_content(MODPATH . 'email/views/template/general' . EXT);
                break;
        }
        foreach ($data as $key => $value) {
            $mail_template = str_replace('{%'.$key.'%}', $value, $mail_template);
        }  
        tmp_mail($title, $subject, $reply_name, $reply_email, $reciper_name, $reciper_email, $mail_template);  
    }
}