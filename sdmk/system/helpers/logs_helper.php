<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/logs
 * @author		xamzo Dev Team, writed by [hadinug, http://hadinug.com]
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('x_getRealIpAddr')) {

    function x_getRealIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}

if (!function_exists('x_user_agent')) {

    function x_user_agent() {
        $X = &get_instance();
        $X->__library(array('user_agent'));
        if ($X->user_agent->is_browser()) {
            $agent = $X->user_agent->browser();
        } elseif ($this->user_agent->is_robot()) {
            $agent = $X->user_agent->robot();
        } elseif ($X->user_agent->is_mobile()) {
            $agent = $X->user_agent->mobile();
        } else {
            $agent = 'Unidentified';
        }
        $os = $X->user_agent->platform();
        switch ($os) {
            case 'Unknown Windows OS':
                $osx = 'Windows';
                break;
            default:
                $osx = $os;
                break;
        }
        return $agent . '#' . $osx;
    }

}

if (!function_exists('x_file_logs')) {

    function x_file_logs($date) {
        $t = explode('-', $date);
        $year = $t['0'];
        $month = $t['1'];
        if (!is_dir(REPORTPATH . 'logs/' . $year)) {
            mkdir(REPORTPATH . 'logs/' . $year);
        }
        if (!is_dir(REPORTPATH . 'logs/' . $year . '/' . $month)) {
            mkdir(REPORTPATH . 'logs/' . $year . '/' . $month);
        }
        if (!is_file(REPORTPATH . 'logs/' . $year . '/' . $month . '/logs' . JSON)) {
            $content = "[]";
            $fp = fopen(REPORTPATH . 'logs/' . $year . '/' . $month . '/logs' . JSON, "wb");
            fwrite($fp, $content);
            fclose($fp);
        }
        return REPORTPATH . 'logs/' . $year . '/' . $month . '/logs' . JSON;
    }

}

if (!function_exists('x_addlogs')) {

    function x_addlogs($status = true) {
        $x = &get_instance();
        $x->__library(array('session'));
        if (is_login()) {
            $id = time();
            $datetime = date('Y-m-d H:i:s');
            $user_id = $x->session->get_data('session_user_id');
            $name = $x->session->get_data('session_username');
            $file_logs = x_file_logs($datetime);
            $agent = x_user_agent();
            $xa = explode('#', $agent);

            $ip = x_getRealIpAddr();
            $lang = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
            $json = x_file_get_content($file_logs);
            $d = x_read_json_decode($json);
            if ($d == null) {
                $d = array();
            }
            $arr_in = array(
                "user_id" => $user_id,
                "fullname" => get_name($user_id),
                "datetime" => $datetime, 
                "ip" => $ip,
                "os" => isset($xa['1']) ? $xa['1'] : '',
                "browser" => isset($xa['0']) ? $xa['0'] : '',
                "language" => $lang['0'],
                "url" => $_SERVER['REQUEST_URI']
            );
            $arr_all = array(
                array(
                    $id => $arr_in
                )
            );
            $join = (array_merge($d, $arr_all)); 
            //put the content
            x_file_put_content($file_logs, json_encode($join));
        }
    }

}

if (!function_exists('x_log_message')) {

    function x_log_message($status, $message) {
        $x = &get_instance();
        $x->__library(array('session'));
        if (is_login()) {
            $id = time();
            $datetime = date('Y-m-d H:i:s');
            $user_id = $x->session->get_data('session_user_id'); 
            $file_logs = x_file_logs($datetime);
            $agent = x_user_agent();
            $xa = explode('#', $agent);
            $ip = x_getRealIpAddr();
            $lang = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
            $json = x_file_get_content($file_logs);
            $d = x_read_json_decode($json);
            $arr_in = array(
                "user_id" => $user_id, 
                "fullname" => get_name($user_id),
                "datetime" => $datetime, 
                "os" => isset($xa['1']) ? $xa['1'] : '',
                "browser" => isset($xa['0']) ? $xa['0'] : '',
                "ip" => $ip,
                "language" => $lang['0'],
                "url" => $_SERVER['REQUEST_URI']
            );
            $arr_all = array(
                array(
                    $id => $arr_in
                )
            );
            $join = (array_merge($d, $arr_all));
            //put the content
            x_file_put_content($file_logs, json_encode($join));
        }
    }

}

if (!function_exists('x_range_logs')) {

    function x_range_logs($from, $to) {
        $sources = array();
        $start = $month = strtotime($from);
        $end = strtotime($to);
        while ($month < $end) {
            $sources[] = date('Y/m', $month);
            $month = strtotime("+1 month", $month);
        }
// Output array to be encoded as JSON
        $output = array();
        $source_array = array();
// Loop over all the source files, retrieve them and add tracks onto the output    
        foreach ($sources as $sourcenum) {
            $source = REPORTPATH . 'logs/' . $sourcenum . '/logs' . JSON;
            if (is_file($source)) {
                $source_json = file_get_contents($source);
                // Decode it as an associative array, passing TRUE as second param
                $source_array[] = json_decode($source_json, TRUE);
            }
        }
        $narray = array();
        foreach ($source_array as $values => $keys) {
            foreach ($keys as $key => $value) {
                $narray[] = $value;
            }
        }
// Look over your array 
        $json_file = json_encode($narray);
        return x_read_json_decode($json_file); 
    }

}
 
/**
 * End of file ./system/helpers/logs_helper.php
 */