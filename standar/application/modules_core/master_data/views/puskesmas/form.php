<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_puskesmas" action="<?php echo site_url('master_data/m_puskesmas/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id_puskesmas" id="id_puskesmas" value="<?php echo isset($value)?$value->id_puskesmas:0?>" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kode</label>
                  <div class="col-md-2">
                    <input name="kode_puskesmas" id="kode_puskesmas" value="<?php echo isset($value)?$value->kode_puskesmas:''?>" class="form-control" type="text" <?php echo isset($value->kode_puskesmas)?'readonly':''?>>
                    <small><i>Ex : P1101010101</i></small>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Puskesmas</label>
                  <div class="col-md-6">
                    <input name="nama_puskesmas_kab" id="nama_puskesmas_kab" value="<?php echo isset($value)?$value->nama_puskesmas_kab:''?>" placeholder="Nama Puskesmas" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Alamat</label>
                  <div class="col-md-8">
                    <input name="alamat" id="alamat" value="<?php echo isset($value)?$value->alamat:''?>" placeholder="Alamat" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      echo Master::get_master_provinsi(isset($value) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      echo Master::get_change_master_kabupaten(isset($value) ? $value->id_kabupaten : $this->session->userdata('data_user')->id_kabupaten ,'id_kabupaten','kab-box','form-control','required','inline');?>
                  </div>
                </div>

                <!-- <div class="form-group" id="form-puskesmas" >
                  <label class="control-label col-md-2">Puskesmas</label>
                  <div class="col-md-3">
                    <?php 
                      echo Master::get_change_master_puskesmas(isset($value) ? $value->kode_puskesmas : $this->session->userdata('data_user')->kode_puskesmas,'kode_puskesmas','puskes-box','form-control','required','inline');?>
                  </div>
                </div> -->

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kawasan</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $data_kawasan = Master::get_tipe_kawasan_data();
                          foreach($data_kawasan as $row_data_kawasan){
                        ?>
                          <label>
                            <input name="id_tipe_kawasan" type="radio" class="ace" value="<?php echo $row_data_kawasan['id_tipe_kawasan']?>" <?php echo isset($value) ? ($value->id_tipe_kawasan == $row_data_kawasan['id_tipe_kawasan']) ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo $row_data_kawasan['nama_tipe_kawasan']?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Rawat Inap</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $data_tipe_puskesmas = Master::get_tipe_puskesmas_data();
                          foreach($data_tipe_puskesmas as $row_data_tipe_puskesmas){
                        ?>
                          <label>
                            <input name="id_tipe_puskesmas" type="radio" class="ace" value="<?php echo $row_data_tipe_puskesmas['id_tipe_puskesmas']?>" <?php echo isset($value) ? ($value->id_tipe_puskesmas == $row_data_tipe_puskesmas['id_tipe_puskesmas']) ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo $row_data_tipe_puskesmas['nama_tipe_puskesmas']?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_jenis_rsk:0?>"> -->

                  <a onclick="getMenu('master_data/m_puskesmas')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">
   
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#puskes-box'));
                    });

                });
            } else {
                $('#puskes-box option').remove()
                $('<option value="">(Pilih Unit Kerja)</option>').appendTo($('#puskes-box'));
            }
        });
    });
</script>
<script src="<?php echo base_url().'assets/js/custom/puskesmas.js'?>"></script>
