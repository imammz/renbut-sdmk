<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pengaturan/role_model','role');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Role";
		$data['subtitle'] = "Daftar Role";
		$this->load->view('role/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Role";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->role->get_by_id($id);
		}
		$this->load->view('role/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->role->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $role) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($role->role_name);
			$row[] = ($role->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $role->updated_date?Tanggal::formatDateTime($role->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($role->id_role,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_role('."'".Regex::_genRegex($role->id_role,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->role->count_all(),
						"recordsFiltered" => $this->role->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_role = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'role_name' => Regex::_genRegex($this->input->post('role_name'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => '',
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_role == 0 ){
			$this->role->save($dataexc);
		}else{
			$this->role->update(array('id_role'=>$id_role), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->role->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
