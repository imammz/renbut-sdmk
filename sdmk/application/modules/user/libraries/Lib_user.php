<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/user
 * @author		CodeAnalytic Dev Team [writed by hadinug.com]
 * @copyright           Copyright (c) 2013, CodeAnalytic, Inc.
 * @license		http://codeanalytic.com/license/
 * @link		http://codeanalytic.com
 * @since		Version 1.0
 * @filesource
 */
class Lib_user {

    /**
     * @name create auth
     * @desc create auth for user in every modules 
     */
    public function create_auth($ses_id = null) {
        if (is_ajax_request() && $ses_id != null) {
            if (is_auth('con_pengguna', 'x')) {
                // load file content
                $json = x_file_get_content(MODPATH . 'user/configs/auth' . JSON);
                // get array config
                $d = x_read_json_decode($json);
                // get list modules in private and public
                $base_mod = MODPATH;
                $dirr = array();
                if ($dir_handle = opendir($base_mod)) {
                    while ($dir = readdir($dir_handle)) {
                        if ($dir != "." && $dir != ".." && $dir != '.htaccess') {
                            if (is_dir($base_mod . $dir . '/configs/')) {
                                $dirr[] = $dir;
                            }
                        }
                    }
                }
                $base_ext = EXTPATH;
                if ($dir_handle = opendir($base_ext)) {
                    while ($dir = readdir($dir_handle)) {
                        if ($dir != "." && $dir != ".." && $dir != '.htaccess') {
                            if (is_file($base_ext . $dir . '/configs/about' . JSON) && $dir != 'error') {
                                $dirr[] = $dir;
                            }
                        }
                    }
                }
                closedir($dir_handle);
                $data = array();
                // create auth for -r -w -x in each application modules
                for ($i = 0; $i < count($dirr); $i++) {
                    $data [] = array(
                        $dirr[$i] => array('r' => '0', 'w' => '0', 'x' => '0')
                    );
                }
                $n = array();
                foreach ($d as $key => $value) {
                    foreach ($value as $k => $v) {
                        if (array_key_exists($ses_id, $value)) {
                            unset($value[$ses_id]);
                        }
                        if ($value != array()) {
                            $n[] = $value;
                        }
                    }
                }
                // create new config
                $j = array(array($ses_id => $data));
                // merge with new config
                $join = array_merge($n, $j);
                //put to file content 
                x_file_put_content(MODPATH . "user/configs/auth" . JSON, json_encode($join));
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    /**
     * @name detel_auth
     * @desc delete auth for user in every modules 
     */
    public function delete_auth($id) {
        $json = x_file_get_content(MODPATH . 'user/configs/auth' . JSON);
        $d = x_read_json_decode($json);
        $n = array();
        foreach ($d as $key => $value) {
            foreach ($value as $k => $v) {
                if (array_key_exists($id, $value)) {
                    unset($value[$id]);
                }
                if ($value != array()) {
                    $n[] = $value;
                }
            }
        }
        x_file_put_content(MODPATH . "user/configs/auth" . JSON, json_encode($n));
    }

}

