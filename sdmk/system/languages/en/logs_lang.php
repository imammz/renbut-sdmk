<?php if (!defined('BASEPATH'))   exit('No direct script access allowed');
 
$lang['error'] = 'error';
$lang['general'] = 'general';
$lang['auth'] = 'auth'; 
$lang['logs'] = 'logs';  
$lang['log_activity'] = 'log activity'; 
$lang['success'] = 'success';
$lang['failed'] = 'failed';
$lang['read_on'] = 'Read on';
$lang['by'] = 'by';
$lang['and_function'] = 'and function';
$lang['with_status'] = 'with status';
$lang['in_module'] = 'in module';
$lang['this_page_to_manage_website_logs,_see_documentation_for_use_it'] = 'This page to manage website logs, see documentation for use it';