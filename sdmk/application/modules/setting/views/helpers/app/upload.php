<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?>  
<div class="modal-setting-header">
    <button type="button" class="close" onclick="close_modal_setting();">&times;</button>
    <h2><i class="fa fa-upload"></i> Upload New</h2>
</div>
<div class="modal-setting-body">
    <form id="jq-upload" style="display: block" method="post" action="<?php echo site_url('setting/do_upload') ?>" enctype="multipart/form-data">
        <div id="jq-drop">
            Drop Here

            <a>Browse</a>
            <input type="file" name="userfile" multiple />
        </div>  
    </form>  
    <ul id="image">

    </ul>  
</div>

<script type="text/javascript">
        $(function() {
            var d = new Date();
            var id = d.getTime();
            var ul = $('ul#image');

            $('#jq-drop a').click(function() {
                $(this).parent().find('input').click();
            });
            $('#jq-upload').fileupload({
                dropZone: $('#jq-drop'),
                dataType: 'json',
                submit: function(e, data) {
                    var et = (data.files[0].name);
                    var ext = (et.split('.').pop());
                    if (!ext.match(/<?php echo $this->lib_media->_config('type_image') ?>/gi)) {
                        $('li#' + id).remove();
                        notive('Only <?php echo $this->lib_media->_config('type_image') ?> files are allowed');
                        return false;
                        data.stop();
                    } else {
                        return true;
                    }
                },
                add: function(e, data) {
                    var tpl = $('<li id="' + id + '" style="padding:20px 0; text-align:center"><input type="text" value="0" data-width="48" data-height="48"' +
                            ' data-fgColor="#50b7dc" data-readOnly="1" data-bgColor="#279ac2" /><p></p><span></span></li>');
                    data.context = tpl.appendTo(ul);
                    tpl.find('input').knob();

                    tpl.find('span').click(function() {
                        if (tpl.attr('id', id)) {
                            jqXHR.abort();
                        }
                        tpl.fadeOut(function() {
                            tpl.remove();
                        });
                    });
                    var jqXHR = data.submit();
                },
                progress: function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    data.context.find('input').val(progress).change();
                },
                fail: function(e, data) {
                    data.context.addClass('error');
                },
                success: function(rs) {
                    if (rs.status !== 'error') {
                        setTimeout(function() {
                            $('li#' + id).remove();
                            $('input[name="<?php echo $_target ?>"]').val(rs.file);
                            close_modal_setting();
                        }, 1000);
                    }
                }
            });
            $(document).on('drop dragover', function(e) {
                e.preventDefault();
            });
        });
</script>    