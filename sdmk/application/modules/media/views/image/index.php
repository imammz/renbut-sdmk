<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<title>Media - Image</title>
<script type="text/javascript" src="<?php echo site_url('application/modules/media/assets/javascript/script.js') ?>"></script>
<div class="xamzo-head-right"> 
    <div class="left">
        <h3>
            <span class="fa fa-folder-open"></span> &nbsp;<?php echo $this->lang['media'] ?>
            <sup class="sup-menu">
                <i class="fa fa-picture-o"></i> <?php echo $this->lang['image'] ?> 
            </sup>
        </h3>
        <span class="xamzo-head-info"><?php echo $this->lang['manage_your_image_file,_see_documentation_for_use_it']; ?>.</span>
    </div>
    <div class="right">
        <div>&nbsp;</div>
        <div class="no-phone">
            <a ajax="true" class="btn-opt" ajax="true" url-push="true" href="#!media/crop" ajax-target=".xamzo-right" original-title="<?php echo $this->lang['crop']; ?>"><i class="fa fa-crop"></i> <?php echo $this->lang['crop']; ?></a>
            <a ajax="true" class="btn-opt" ajax="true" url-push="true" href="#!media/doc" ajax-target=".xamzo-right"  original-title="<?php echo $this->lang['document']; ?>"><i class="fa fa-file-text"></i> <?php echo $this->lang['document']; ?></a>
            <a ajax="true" class="btn-opt" ajax="true" url-push="true" href="#!media/zip" ajax-target=".xamzo-right"  original-title="<?php echo $this->lang['zip']; ?>"><i class="fa fa-archive"></i> <?php echo $this->lang['zip']; ?></a>
        </div>
    </div>
</div> 
<div id="xamzo-head-action">
    <button crud-name="check" class="btn primary large fa fa-check-square-o tip-top" original-title="<?php echo $this->lang['select_all'] ?>" table-check-all="true"></button>
    <?php if(is_admin()): ?>
    <button class="btn primary large fa fa-angle-down" dropdown="true" dropdown-relative="left" dropdown-target="post-add">
        <?php echo $this->lang['option']; ?>
    </button>
        <button ajax="true" url-push="true" href="#!setting/media" ajax-target=".xamzo-right" class="btn primary large fa fa-cogs tip-top" original-title="<?php echo $this->lang['setting'] ?>"></button>
    <?php endif; ?>
    <button dropdown="true" dropdown-relative="left" dropdown-target="upload-target" class="btn primary large fa fa-upload tip-top" original-title="<?php echo $this->lang['upload'] ?>"></button>
    <button crud-name="delete" url-push="true" crud-url="media/delete_all" disabled="disabled" crud-redirect="media/index" crud-target=".xamzo-right" crud-data="#crud-data" crud-table-target="ul.list_image" class="btn primary large fa fa-trash tip-top" original-title="<?php echo $this->lang['delete_data']; ?>"></button>
    <span class="xamzo-input-search">
        <input type="name" name="search" placeholder="<?php echo $this->lang['search']; ?>.." crud-search="true" search-url="media/index" search-target="#crud-target" class="tip-bottom-left small no-phone" original-title="search by name">
        <span class="xamzo-small-loading"></span>
    </span>
    <button crud-name="list" ajax="true" url-push="true" href="#!media/index" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top-right" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div>  
<?php if(is_admin()): ?>
<ul class="dropdown-target dropdown-target-left post-add">
    <div class="arrow-up"></div>
    <li>
        <a ajax="true" url-push="true" href="#!media/index" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> <?php echo $this->lang['general']; ?></a>
    </li>      
    <li>    
        <a ajax="true" url-push="true" href="#!media/report" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> Laporan Proyek</a>
    </li>   
    <li>   
        <a ajax="true" url-push="true" href="#!media/_project" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> Proyek File</a>
    </li>   
</ul>
<?php endif; ?>
<div id="crud-target"> 
    <div class="xamzo-block" id="xamzo-media">
        <div class="xamzo-block-content">
            <div class="content">
                <form id="crud-data">
                    <ul class="list_image xamzo-box-media" id="image">
                        <li style="display: none">
                            <input type="checkbox" table-check-all="true">
                        </li>
                        <?php
                        $id = 0;
                        foreach ($images as $r) :
                            $t = explode('{ca}', $r);
                            $title = isset($t['1']) ? $t['1'] : '';
                            ?>
                            <li id="<?php echo $id ?>"> 
                                <img class="xamzo-media tip-bottom-left" original-title="<?php echo $title ?>" src="<?php echo site_url("upload/i/" . str_replace('thumb_b', 'thumb_s', $r)); ?>"/>
                                <span dir="<?php echo site_url("upload/i/" . str_replace('thumb_b', 'thumb_s', $r)); ?>">
                                    <div class="box-image-act">
                                        <div class="left">
                                            <input table-check-all='true' type="checkbox" onclick="_self_check(this);" name="id[]" value="<?php echo site_url("upload/i/" . $r); ?>" id="<?php echo $r; ?>"> <label for="<?php echo $r; ?>"></label>
                                        </div>
                                        <div class="right">
                                            <a href="javascript:void(0)" onclick="file_url($(this).parent().parent());" class="tip-top" original-title="<?php echo $this->lang["link"] ?>"><i class="fa fa-link"></i></a>
                                            <a href="javascript:void(0)" onclick="img_property($(this).parent().parent().parent());" class="tip-top no-phone" original-title="<?php echo $this->lang["properties"]; ?> "><i class='fa fa-search'></i></a>
                                            <a href="javascript:void(0)" onclick="img_delete(this);" class="tip-top" original-title="<?php echo $this->lang["delete"]; ?>"><i class='fa fa-trash'></i></a> 
                                        </div>
                                    </div>
                            </li>
                            <?php
                            $id++;
                        endforeach;
                        ?>
                    </ul>
                </form>
            </div>
        </div>
    </div>  
    <div class="xamzo-block" id="xamzo-property"></div>
</div>  
<ul class="dropdown-target upload-target">
    <div class="arrow-up"></div>
    <li>
        <form id="jq-upload" method="post" action="<?php echo site_url('media/image_upload') ?>" enctype="multipart/form-data">
            <div id="jq-drop">
                Drop Here
                <a>Browse</a>
                <input type="file" name="userfile" multiple />
            </div>  
        </form> 
    </li> 
</ul>

<div class="xamzo-pagination">
    <div id="option">
        <?php
        echo $this->lang['showing_1_to'] . "&nbsp;" . $limit . "&nbsp;" . $this->lang['of'] . " $total " . $this->lang['entries']
        ?> 
    </div>
    <div id="pagging">
        <?php echo $paging; ?> 
    </div>
</div>   

<?php $this->__view('media', 'js/image'); ?>