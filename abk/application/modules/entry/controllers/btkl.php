<?php

class btkl extends MX_Controller {

    function __construct() {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('abk_model');
        $this->load->model('master_model');
        $this->load->model('faskes_model');
        Message::_modal();

        
    }

    public function index() {
        $data = array();
        $data['class'] = 'btkl';
        $data['title'] = "Entry Perhitungan Metode ABK";
        $data['subtitle'] = "BTKL";
        // echo "<pre>";  print_r($faskes_model->_loadbtkl()); exit;
        $this->load->view('faskes_view', $data);
    }

    public function data() {
        $data = array();
        $data['class'] = 'btkl';
        $data['title'] = "Entry Perhitungan Metode ABK";
        $data['subtitle'] = "BTKL";

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadbtkl(date('Y'), $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_btkl'] = $faskes_model->_loadMasterbtkl($_SESSION['user']['faskes_kode'],$_SESSION['user']['NO_KAB'],$_SESSION['user']['NO_PROV']);
        
//  echo "<pre>";  print_r($data['master_btkl']); exit;
        $this->load->view('btkl_data_view', $data);
    }

    public function get($faskes_kode = null) {
        $data = array();
        $data['class'] = 'btkl';

        if ($faskes_kode == null) {
            $faskes_kode = $this->input->get('faskes_kode', true);
        }
        $faskes = $this->orm->faskes->where('faskes_kode', $faskes_kode)->fetch();
        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $faskes_model = new faskes_model();
        $data['faskes_kode'] = $faskes_kode;
        $data['data_faskes'] = $faskes_model->_loadbtkl(date('Y'), $faskes_kode, $kab['NO_KAB'], $prov['NO_PROV']);
        $data['master_btkl'] = $faskes_model->_loadMasterbtkl($_SESSION['user']['faskes_kode'],$_SESSION['user']['NO_KAB'],$_SESSION['user']['NO_PROV']);
       
        $this->load->view('btkl_data_view', $data);
    }
    
     public function gethasil($tahun_entry = null) {
        $data = array();
        $data['class'] = 'btkl';

        if($tahun_entry==null) { $tahun_entry = date('Y');}
        
        $faskes_kode = $this->input->get('faskes_kode', true);
        
        $faskes = $this->orm->faskes->where('faskes_kode', $faskes_kode)->fetch();
        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $faskes_model = new faskes_model();
        $data['faskes_kode'] = $faskes_kode;
        $data['data_faskes'] = $faskes_model->_loadbtkl($tahun_entry, $faskes_kode, $kab['NO_KAB'], $prov['NO_PROV']);
        $data['master_btkl'] = $faskes_model->_loadMasterbtkl($_SESSION['user']['faskes_kode'],$_SESSION['user']['NO_KAB'],$_SESSION['user']['NO_PROV']);
        $data['tahun_entry'] = $tahun_entry;
//  echo "<pre>";  print_r($data['data_faskes']); exit;
        $this->load->view('hasilentry_btkl_data_view', $data);
    }

    public function tambahFaskes($NO_KAB) {
        $data = array();
        $data['class'] = 'btkl';
        $data['NO_KAB'] = $NO_KAB;
        $faskes_model = new faskes_model();
        $data['master_btkl'] = $faskes_model->_loadMasterbtkl($_SESSION['user']['faskes_kode'],$_SESSION['user']['NO_KAB'],$_SESSION['user']['NO_PROV']);
         $data['btkl'] = $this->orm->ref_btklp->where('faskes_kode',$_SESSION['user']['faskes_kode'])->fetch();
       
        $this->load->view('btkl_tambahfaskes_view', $data);
    }

    public function getData() {

        $faskes_kode = $this->input->post('faskes_kode', TRUE);
        $ref_btkl = $this->orm->ref_kabupaten->where('NO_KAB', $faskes_kode)->fetch();

        $master_model = new master_model();
        $abk_model = new abk_model();


        $data = array();

        if (count($ref_btkl) == 0) {
            $data['result'] = FALSE;
        } else {
            $data['result'] = TRUE;
            $data['faskes_kode'] = $ref_btkl['NO_KAB'].'#5';
            $data['faskes_nama'] = 'BTKL            - '.$ref_btkl['NAMA_KAB'];
            $data['faskes_telp'] = '';
            $data['faskes_email'] = '';
            $data['ref_jenis_faskes_kode'] = '3';

            if ($abk_model->_checkCurrentFaskes($data['faskes_kode'])) {
                $data['current_exist'] = TRUE;
            } else {
                $data['current_exist'] = FALSE;
            }
        }

        header('Content-Type: application/json; charset=utf-8');
        header('Content-Length: ' . strlen(json_encode($data)));
        header('Access-Control-Allow-Origin: *');


        echo json_encode($data);
    }

    public function tambahfaskes_proses($NO_KAB = 0) {

        if ($this->input->post('faskes_kode', true) == null OR $this->input->post('faskes_kode', true) == '') {
            echo "<script>alert('Kode BTKL Harus Diisi');</script>";
            echo "<script>window.history.back();</script>";
        } else {
            $data = array();
            $data['faskes_kode'] = $this->input->post('faskes_kode', true);
            $data['NO_KAB'] = $NO_KAB;
            $data['faskes_nama'] = $this->input->post('faskes_nama', true);
            $data['faskes_telp'] = $this->input->post('faskes_telp', true);
            $data['faskes_alamat'] = $this->input->post('faskes_alamat', true);
            $data['faskes_email'] = $this->input->post('faskes_email', true);
            $data['ref_faskes_kelas_id'] = $this->input->post('ref_faskes_kelas_id', true);
            $data['status_pengelolaan_id'] = $this->input->post('status_pengelolaan_id', true);
            $data['ref_jenis_faskes_kode'] = 3;
            $data['tahun'] = date('Y');
            $data['insert_date'] = date('Y-m-d H:i:s');


            $abk_model = new abk_model();

            if ($abk_model->_checkCurrentFaskes($data['faskes_kode'])) {
                $result = $this->orm->faskes->where('faskes_kode', $data['faskes_kode'])->update($data);
                Message::_set(true, 'Edit Data BTKL Berhasil');
            } else {
                $result = $this->orm->faskes->insert($data);
                Message::_set(true, 'Tambah Data BTKL Berhasil');
            }

            $msg = base_url("index.php/entry/btkl/get/" . $this->input->post('faskes_kode', true));
            Message::_redirect($msg);
        }
    }

    public function tambahBagian($faskes_kode) {
        $data = array();
        $data['class'] = 'btkl';
        $data['faskes_kode'] = $faskes_kode;
        $this->load->view('faskes_tambahbagian_view', $data);
    }

    public function tambahbagian_proses($faskes_kode = 0) {
        $urut_installasi = $this->input->post('urut_unit_installasi', true);
        $installasi_nama = $this->input->post('installasi_nama', true);
        $existing = $this->input->post('existing');

		
		
        foreach ($urut_installasi as $key => $value) {
            
			$urut = $this->orm->faskes_installasi->select('MAX(urut) as val')->where('faskes_kode = ? AND(parent is null OR parent = ?)',$faskes_kode,'0')->fetch();
			
			$data = array();
            $data['faskes_kode'] = $faskes_kode;
            $data['installasi_nama'] = $installasi_nama[$key];
            $data['parent'] = 0;
            $data['urut'] = $urut['val']+1;
            if (!empty($installasi_nama[$key]) OR $installasi_nama[$key] != '') {
                $this->orm->faskes_installasi->insert($data);
            }
			$urut++;
        }

        Message::_set(true, 'Update Data Bidang/Bagian Berhasil');
        $msg = base_url("index.php/entry/btkl/get/" . $faskes_kode);
        Message::_redirect($msg);
    }
	
	
	public function tambahSubBagian($faskes_kode) {
        $data = array();
        $data['class'] = 'btkl';
        $data['faskes_kode'] = $faskes_kode;
		$data['parent'] = $this->orm->faskes_installasi->where('faskes_kode = ? AND(parent is null OR parent = ?)',$faskes_kode,'0')->order('urut ASC');
        $this->load->view('faskes_tambahsubbagian_view', $data);
    }

    public function tambahSubBagian_proses($faskes_kode = 0) {
        $urut_installasi = $this->input->post('urut_unit_installasi', true);
        $installasi_nama = $this->input->post('installasi_nama', true);
		$parent = $this->input->post('parent', true);
        $existing = $this->input->post('existing');


       
		
		
        foreach ($urut_installasi as $key => $value) {
            
			$urut = $this->orm->faskes_installasi->select('urut as val')->where('faskes_installasi_id = ? AND (parent IS NULL OR parent = ? )',$parent[$key],0)->order('urut ASC')->fetch();
			
			$data = array();
            $data['faskes_kode'] = $faskes_kode;
            $data['installasi_nama'] = '  - '.$installasi_nama[$key];
            $data['parent'] = $parent[$key];
            $data['eselon'] = 'IV';
            $data['urut'] = $urut['val'].'1';
            if (!empty($installasi_nama[$key]) OR $installasi_nama[$key] != '') {
                $this->orm->faskes_installasi->insert($data);
            }
			$urut++;
        }

        Message::_set(true, 'Update Data SubBidang /SubBagian Berhasil');
        $msg = base_url("index.php/entry/btkl/get/" . $faskes_kode);
        Message::_redirect($msg);
    }
	

    public function tambahSDMK($faskes_installasi_id) {
        $data = array();
        $data['class'] = 'btkl';
        $data['faskes_installasi_id'] = $faskes_installasi_id;
        $this->load->view('faskes_tambahsdmk_view', $data);
    }

    public function tambahSDMK_proses($faskes_installasi_id) {

        $sdmk = explode('#', $this->input->post('ref_jenis_sdmk_kode'));
        $jft = explode('#', $this->input->post('jft'));

        if (!isset($jft[1])) {
            $jft[1] = '-';
        }

        $data = array();
        $data['ref_jenis_sdmk_kode'] = $sdmk[0];
        $data['sdmk_faskes_installasi_nama'] = $sdmk[1] . ' ' . $jft[1];
        $data['ref_jenis_sdmk_jft_id'] = $jft[0];
        $data['faskes_installasi_id'] = $faskes_installasi_id;
        $data['jumlah_PNS'] = $this->input->post('jumlah_PNS');
        $data['jumlah_PPPK'] = $this->input->post('jumlah_PPPK');
        $data['jumlah_PTT'] = $this->input->post('jumlah_PTT');
        $data['jumlah_HD'] = $this->input->post('jumlah_HD');
        $data['jumlah_DLL'] = $this->input->post('jumlah_DLL');
        $data['jumlah_seluruh'] = $this->input->post('jumlah_PNS');

        $result = $this->orm->sdmk_faskes_installasi->insert($data);

        echo '<meta http-equiv="refresh" content="0;url=' . base_url() . 'index.php/entry/btkl/tambahWKT/' . $result['sdmk_faskes_installasi_id'] . '">';
    }
    
     public function hapusSDMK($sdmk_faskes_installasi_id) {
        $data = array();
        $data['class'] = 'btkl';
        $data['sdmk_faskes_installasi'] = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id',$sdmk_faskes_installasi_id)->fetch();
        $this->load->view('faskes_hapussdmk_view', $data);
    }
    
    public function hapusSDMK_proses($sdmk_faskes_installasi_id) {
        
        $sdmk = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id',$sdmk_faskes_installasi_id);
        $sdmk_fetch = $sdmk->fetch();
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id',$sdmk_fetch['faskes_installasi_id'])->fetch();
        
        $sdmk->delete();
        $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id',$sdmk_faskes_installasi_id)->delete();
        
        Message::_set(true, 'Hapus Data SDMK  Berhasil');
        $msg = base_url("index.php/entry/btkl/get/" . $faskes_installasi['faskes_kode']);
        Message::_redirect($msg);
    }

    public function tambahWKT($sdmk_faskes_installasi_id) {

        $data = array();
        $data['class'] = 'btkl';
        $data['sdmk_faskes_installasi_id'] = $sdmk_faskes_installasi_id;
        $data['wkt'] = $this->orm->ref_komponen_wkt->where('komponen_wkt_kode != ? AND komponen_wkt_kode != ?', 'WKT3JAM', 'WKT3MNT');
        $data['wkt_hasil_jam'] = $this->orm->ref_komponen_wkt->where('komponen_wkt_kode = ?', 'WKT3JAM')->fetch();
        $data['wkt_hasil_menit'] = $this->orm->ref_komponen_wkt->where('komponen_wkt_kode = ?', 'WKT3MNT')->fetch();

        $this->load->view('faskes_tambahwkt_view', $data);
    }

    public function tambahABK($sdmk_faskes_installasi_id,$less = false) {

        $data = array();
        $data['class'] = 'btkl';
        $data['sdmk_faskes_installasi_id'] = $sdmk_faskes_installasi_id;
        $data['ref_satuan2'] = $this->orm->ref_satuan->where('satuan_id', array(1,2,3,4,5, 6, 7, 8));
        $data['ref_satuan1'] = $this->orm->ref_satuan->where('satuan_id > ?', 8);
        $data['kbk'] = $this->orm->kbk();
        $data['info'] = ($less)?'Total Faktor Tugas Penunjang Melebihi 20% Silahkan Dikoreksi Kembali Norma Waktu Tugas Penunjang':'';

        $this->load->view('faskes_tambahabk_view', $data);
    }

    public function tambahABK_proses($sdmk_faskes_installasi_id) {
		$ftp_total = 0;
        $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id',$sdmk_faskes_installasi_id)->delete(); //clear data sebelumnya
        
        $abk_model = new abk_model();

        $urut_kbk = $this->input->post('urut_tugas_pokok');
        $uraian_kbk = $this->input->post('uraian_tugas_pokok');
        $norma_waktu = $this->input->post('norma_waktu_tugas_pokok');
        $capaian = $this->input->post('capaian');
        $satuan_id = $this->input->post('satuan_id_tugas_pokok');
        

        foreach ($urut_kbk as $key_kbk => $val_kbk) {

            $sbk = $abk_model->_getSBK($norma_waktu[$key_kbk]);

            $data_sdmk_kbk = array();
            $data_sdmk_kbk['kbk_uraian'] = $uraian_kbk[$key_kbk];
            $data_sdmk_kbk['norma_waktu'] = $norma_waktu[$key_kbk];
            $data_sdmk_kbk['capaian'] = $capaian[$key_kbk];
            $data_sdmk_kbk['sbk'] = $sbk;
            $data_sdmk_kbk['satuan_id'] = $satuan_id[$key_kbk];
            $data_sdmk_kbk['sdmk_faskes_installasi_id'] = $sdmk_faskes_installasi_id;
            $data_sdmk_kbk['jenis_tugas_id'] = 1;
            $data_sdmk_kbk['urut'] = $key_kbk;

           if (!empty($data_sdmk_kbk['norma_waktu']) AND $data_sdmk_kbk['norma_waktu'] != '' AND $data_sdmk_kbk['norma_waktu'] != ' ') {
                $this->orm->sdmk_kbk->insert($data_sdmk_kbk);
            }
        }

        $urut_tugas_pokok = $this->input->post('urut_tugas_penunjang');
        $uraian_kbk = $this->input->post('uraian_tugas_penunjang');
        $norma_waktu = $this->input->post('norma_waktu_tugas_penunjang');
        $satuan_id = $this->input->post('satuan_id_tugas_penunjang');

        foreach ($urut_tugas_pokok as $key_kbk => $val_kbk) {

            $waktu_kegiatan = $abk_model->_getWaktuKegiatan($norma_waktu[$key_kbk], $satuan_id[$key_kbk]);
            $ftp = $waktu_kegiatan / $abk_model->_getWKTMenit();

            $data_sdmk_kbk = array();
            $data_sdmk_kbk['kbk_uraian'] = $uraian_kbk[$key_kbk];
            $data_sdmk_kbk['norma_waktu'] = $norma_waktu[$key_kbk];
            $data_sdmk_kbk['ftp'] = $ftp;
            $data_sdmk_kbk['waktu_kegiatan'] = $waktu_kegiatan;
            $data_sdmk_kbk['satuan_id'] = $satuan_id[$key_kbk];
            $data_sdmk_kbk['sdmk_faskes_installasi_id'] = $sdmk_faskes_installasi_id;
            $data_sdmk_kbk['jenis_tugas_id'] = 2;
            $data_sdmk_kbk['urut'] = $key_kbk;

           if (!empty($data_sdmk_kbk['norma_waktu']) AND $data_sdmk_kbk['norma_waktu'] != '' AND $data_sdmk_kbk['norma_waktu'] != ' ') {
                $this->orm->sdmk_kbk->insert($data_sdmk_kbk);

                $ftp_total += $ftp;
            }
        }

            if($ftp_total>0.2) {
                redirect('entry/btkl/tambahABK/'.$sdmk_faskes_installasi_id.'/true');
            }

        echo '<meta http-equiv="refresh" content="0;url=' . base_url() . 'index.php/entry/btkl/tambahResult/' . $sdmk_faskes_installasi_id . '">';
    }

    public function tambahResult($sdmk_faskes_installasi_id) {

        $data = array();
        $data['class'] = 'btkl';
        $data['sdmk_faskes_installasi_id'] = $sdmk_faskes_installasi_id;

        $this->load->view('faskes_tambahresult_view', $data);
    }

    public function tambahResult_proses($sdmk_faskes_installasi_id) {

        $abk_model = new abk_model();

        $sdmk_kbk_id = $this->input->post('sdmk_kbk_id');

        foreach ($sdmk_kbk_id as $row) {
            $sdmk_kbk = $this->orm->sdmk_kbk->where('sdmk_kbk_id', $row);
            $sdmk_kbk->update(array('capaian' => $this->input->post($row . '_capaian')));
        }

        $row_sdmk = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $row_sdmk['ref_jenis_sdmk_kode'])->fetch();

        $data_sdmk = array();
        $data_sdmk['sdmk_nama'] = $ref_jenis_sdmk['ref_jenis_sdmk_nama_level2'] . ' ' . $ref_jenis_sdmk['ref_jenis_sdmk_nama_level3'];
        // $data_sdmk['sdmk_jumlah'] = $row_sdmk['jumlah_PNS'] + $row_sdmk['jumlah_PPPK'] + $row_sdmk['jumlah_PTT'] + $row_sdmk['jumlah_HD'] + $row_sdmk['jumlah_DLL'];
        $data_sdmk['sdmk_jumlah'] = $row_sdmk['jumlah_PNS'];
        $data_sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];


        $total_ftp = $this->orm->sdmk_kbk()->where('jenis_tugas_id = ? AND sdmk_faskes_installasi_id = ?', 2, $sdmk_faskes_installasi_id)->sum('FTP');

        $stp = $abk_model->_fmlSTP($total_ftp);


        $kbk = $this->orm->sdmk_kbk()->where('jenis_tugas_id = ? AND sdmk_faskes_installasi_id = ?', 1, $sdmk_faskes_installasi_id);

        $jkt = 0;
        foreach ($kbk as $row_kbk) {

            $jkt = $jkt + $abk_model->_getSDMKPerKBK($row_kbk['capaian'], $row_kbk['SBK']);
        }

        $kebutuhan = $jkt * $stp;

        $data_sdmk['sdmk_kebutuhan'] = $kebutuhan;

        if ($kebutuhan <= 5) {
            $data_sdmk['sdmk_kebutuhan_pembulatan'] = Ceil($kebutuhan);
        } else {
            $data_sdmk['sdmk_kebutuhan_pembulatan'] = round($kebutuhan);
        }

        $sdmk[] = $data_sdmk;

        $update_data = array();
        $update_data['hasil_kebutuhan_sdmk'] = $data_sdmk['sdmk_kebutuhan'];
        $update_data['hasil_kebutuhan_sdmk_pembulatan'] = $data_sdmk['sdmk_kebutuhan_pembulatan'];

        $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->update($update_data);

        echo '<meta http-equiv="refresh" content="0;url=' . base_url() . 'index.php/entry/btkl/result/' . $sdmk_faskes_installasi_id . '">';
    }

    public function result($sdmk_faskes_installasi_id) {

        $data = array();
        $data['class'] = 'btkl';
        $data['sdmk_faskes_installasi_id'] = $sdmk_faskes_installasi_id;
        $this->load->view('faskes_result_view', $data);
    }

    public function editfaskes($faskes_kode) {
        $data = array();
        $data['class'] = 'btkl';
        $data['faskes'] = $this->orm->faskes->where('faskes_kode', $faskes_kode)->fetch();
        $this->load->view('btkl_edit_view', $data);
    }

    public function editfaskes_proses($faskes_kode = null) {
        $data_post = array();
        $data_post['faskes_nama'] = $this->input->post('faskes_nama', true);
        $data_post['ref_faskes_kelas_id'] = $this->input->post('ref_faskes_kelas_id', true);
        $data_post['status_pengelolaan_id'] = $this->input->post('status_pengelolaan_id', true);
        $data_post['faskes_alamat'] = $this->input->post('faskes_alamat', true);
        $data_post['faskes_telp'] = $this->input->post('faskes_telp', true);
        $data_post['faskes_email'] = $this->input->post('faskes_email', true);

        $this->orm->faskes->where('faskes_kode', $faskes_kode)->update($data_post);

        if ($faskes_kode != null) {
            Message::_set(true, 'Edit Data BTKL  Berhasil');
            $msg = base_url("index.php/entry/btkl/get/" . $faskes_kode);
            Message::_redirect($msg);
        }
    }

    public function editinstallasi($faskes_installasi_id) {
        $data = array();
        $data['class'] = 'btkl';
        $data['faskes_installasi'] = $this->orm->faskes_installasi->where('faskes_installasi_id', $faskes_installasi_id)->fetch();
        $data['faskes'] = $this->orm->faskes->where('faskes_kode', $data['faskes_installasi']['faskes_kode'])->fetch();
        $this->load->view('faskes_installasi_edit_view', $data);
    }

    public function editinstallasi_proses($faskes_installasi_id) {
        $data_post = array();
        $data_post['installasi_nama'] = $this->input->post('installasi_nama', true);

        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $faskes_installasi_id);
        $faskes_kode = $faskes_installasi->fetch();
        $faskes_installasi->update($data_post);

        Message::_set(true, 'Edit Data Installasi  Berhasil');
        $msg = base_url("index.php/entry/btkl/get/" . $faskes_kode['faskes_kode']);
        Message::_redirect($msg);
    }

    public function editsdmk($sdmk_faskes_installasi_id) {
        $data = array();
        $data['class'] = 'btkl';
        $data['sdmk_faskes_installasi'] = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
        $data['faskes_installasi'] = $this->orm->faskes_installasi->where('faskes_installasi_id', $data['sdmk_faskes_installasi']['faskes_installasi_id'])->fetch();
        $data['faskes'] = $this->orm->faskes->where('faskes_kode', $data['faskes_installasi']['faskes_kode'])->fetch();
        $this->load->view('faskes_sdmk_edit_view', $data);
    }

    public function editsdmk_proses($sdmk_faskes_installasi_id = null) {
        $data = array();
        $data['jumlah_PNS'] = $this->input->post('jumlah_PNS');
        $data['jumlah_PPPK'] = $this->input->post('jumlah_PPPK');
        $data['jumlah_PTT'] = $this->input->post('jumlah_PTT');
        $data['jumlah_HD'] = $this->input->post('jumlah_HD');
        $data['jumlah_DLL'] = $this->input->post('jumlah_DLL');
        $data['jumlah_seluruh'] = $this->input->post('jumlah_PNS');
        
        if($sdmk_faskes_installasi_id!=null) {
            $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id',$sdmk_faskes_installasi_id)->update($data);
        }
        
        echo '<meta http-equiv="refresh" content="0;url=' . base_url() . 'index.php/entry/btkl/tambahWKT/' . $sdmk_faskes_installasi_id . '">';
    }

     
}

?>