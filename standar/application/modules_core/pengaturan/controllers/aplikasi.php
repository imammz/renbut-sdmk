<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aplikasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pengaturan/aplikasi_model','aplikasi');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Aplikasi";
		$data['subtitle'] = "Template app";
		$data['value'] = $this->aplikasi->get_by_id(Regex::_genRegex(1, 'RGXINT'));
		
		$this->load->view('aplikasi/form', $data);
	}

	public function proses_app()
	{
		$id_conf_application = Regex::_genRegex(1, 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'app_name' => Regex::_genRegex($this->input->post('app_name'), 'RGXQSL'),
			'header_title' => Regex::_genRegex($this->input->post('header_title'), 'RGXQSL'),
			'footer_text' => Regex::_genRegex($this->input->post('text_footer'), 'RGXQSL'),
			'copyright' => Regex::_genRegex($this->input->post('copyright'), 'RGXQSL'),
			'app_description' => Regex::_genRegex($this->input->post('description'), 'RGXQSL'),
			'author' => Regex::_genRegex($this->input->post('auth_name'), 'RGXQSL'),
			'company_name' => Regex::_genRegex($this->input->post('company_name'), 'RGXQSL'),
			'updated_by' => '',
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		$this->aplikasi->update(array('id_conf_application'=>$id_conf_application), $dataexc);


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	
}
