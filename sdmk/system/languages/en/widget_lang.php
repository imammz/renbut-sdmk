<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang['top'] = 'top';
$lang['right'] = 'right';
$lang['bottom'] = 'bottom';
$lang['left'] = 'left';
$lang['position'] = 'position';
$lang['drag_here'] = 'drag here';
$lang['html_script']='html script';
$lang['is_removed_from_list']='is removed from list';
$lang['get_more_widgets'] = 'get more widgets';
$lang['widget_is_up_to_date'] = 'widget is up to date';
$lang['this_page_to_manage_widget,_see_documentation_for_use_it'] = 'This page to manage widget, see documentation for use it';