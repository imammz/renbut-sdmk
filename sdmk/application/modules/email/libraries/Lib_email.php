<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		libraries/email
 * @author		xamzo's developers
 * @copyright           Copyright (c) 2014, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

class lib_email { 
    public function config($opt = null) {
        if ($opt != null) {
            $json = x_file_get_content(MODPATH . 'email/configs/config' . JSON);
            $j = x_read_json_decode($json);
            return $j[$opt];
        }
    }

    public function smtp($file = null, $keys = null) {
        if ($file != null && $keys != null) {
            $json = x_file_get_content(MODPATH . 'email/configs/' . $file . JSON);
            $j = x_read_json_decode($json);
            $n = array();
            foreach ($j as $key => $value) {
                foreach ($value as $k => $v) {
                    $n[$k] = $v;
                }
            }
            return $n[$keys]['val'];
        }
    }

    public function reader() {
        $json = x_file_get_content(MODPATH . 'email/configs/reader' . JSON);
        $j = x_read_json_decode($json);
        return $j;
    }

    public function update_reader($opt = null) {
        $json = x_file_get_content(MODPATH . 'email/configs/reader' . JSON);
        $j = x_read_json_decode($json);
        $n = array();
        foreach ($j as $key => $value) {
            if (array_key_exists($key, $opt)) {
                unset($key);
            } else {
                $n[] = array($key => $value);
            }
        }
        $js1 = json_encode(array_merge(array($opt), $n));
        $js2 = str_replace('},{', ',', $js1);
        $js3 = str_replace('[{', '{', $js2);
        $js4 = str_replace('}]', '}', $js3);
        x_file_put_content(MODPATH . 'email/configs/reader' . JSON, $js4);
    }

    public function update($opt = null) {
        if ($opt != null && is_array($opt)) {
            $json = x_file_get_content(MODPATH . 'email/configs/config' . JSON);
            $j = x_read_json_decode($json);
            $n = array();
            foreach ($j as $key => $value) {
                if (!in_array($key, array_keys($opt))) {
                    $n[] = array($key => $value);
                }
            }
            $js1 = json_encode(array_merge(array($opt), $n));
            $js2 = str_replace('},{', ',', $js1);
            $js3 = str_replace('[{', '{', $js2);
            $js4 = str_replace('}]', '}', $js3);
            x_file_put_content(MODPATH . 'email/configs/config' . JSON, $js4);
        }
    }

}