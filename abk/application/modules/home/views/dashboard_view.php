<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div>
        <?php //echo Modules::run('templates/cliptwo/menu'); ?>
        <div>
            <?php //echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div >
                <div class="wrap-content container" id="container">
                    <!-- start: PAGE TITLE -->
                    <section id="page-title">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="mainTitle">DASHBOARD Aplikasi Perencanaan Kebutuhan Tenaga Kesehatan</h1>
                                <p>&nbsp;</p>                               
                                
                              </div>
                           
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">                                            
                                <div class="panel-body" style="min-height: 700px;">
                                    
                                    
                                    
                                    
                                    
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-02-UU-N0-36-2009-(-KESEHATAN).pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> Master PUSKESMAS </strong>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-03-UU-no-17-th-2007-(RPJPN).pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong>  Master Rumah Sakit</strong>
                                        </a>
                                    </div>
                                    
                                    

                                </div>
                            </div>

                        </div>



                    </div>
                        
                    </section>
                    <!-- end: PAGE TITLE -->
                    <!-- start: YOUR CONTENT HERE -->
                    <!-- end: YOUR CONTENT HERE -->
                </div>
            </div>
        </div>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
