<script>
function myFunction(keys) {

    var pns = document.getElementById('pns'+keys+''); 
    pns = pns.value?pns.value:0;

    var pppk = document.getElementById('pppk'+keys+''); 
    pppk = pppk.value?pppk.value:0;

    var ptt = document.getElementById('ptt'+keys+''); 
    ptt = ptt.value?ptt.value:0;

    var honorer = document.getElementById('honorer'+keys+''); 
    honorer = honorer.value?honorer.value:0;

    var blud = document.getElementById('blud'+keys+''); 
    blud = blud.value?blud.value:0;

    var tks = document.getElementById('tks'+keys+''); 
    tks = tks.value?tks.value:0;

    total = parseInt(pns) + parseInt(pppk) + parseInt(ptt) + parseInt(honorer) + parseInt(blud) + parseInt(tks);
    document.getElementById('total'+keys+'').value = total;

    // SUB PARENT //
    var pns_sub = document.getElementById('pns_sub'+keys+''); 
    pns_sub = pns_sub.value?pns_sub.value:0;

    var pppk_sub = document.getElementById('pppk_sub'+keys+''); 
    pppk_sub = pppk_sub.value?pppk_sub.value:0;

    var ptt_sub = document.getElementById('ptt_sub'+keys+''); 
    ptt_sub = ptt_sub.value?ptt_sub.value:0;

    var honorer_sub = document.getElementById('honorer_sub'+keys+''); 
    honorer_sub = honorer_sub.value?honorer_sub.value:0;

    var blud_sub = document.getElementById('blud_sub'+keys+''); 
    blud_sub = blud_sub.value?blud_sub.value:0;

    var tks_sub = document.getElementById('tks_sub'+keys+''); 
    tks_sub = tks_sub.value?tks_sub.value:0;

    total_sub = parseInt(pns_sub) + parseInt(pppk_sub) + parseInt(ptt_sub) + parseInt(honorer_sub) + parseInt(blud_sub) + parseInt(tks_sub);
    document.getElementById('total_sub'+keys+'').value = total_sub;
    
}
</script>

<script type="text/javascript">

    $(function() {


        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsk_mata-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#rsk_mata-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#rsk_mata-box'));
                    });

                });
            } else {
                $('#rsk_mata-box option').remove()
                $('<option value="">(Pilih Unit Kerja)</option>').appendTo($('#rsk_mata-box'));
            }
        });
    });
</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
  <form class="form-horizontal" method="post" id="form_prc_rsk_mata" action="<?php echo site_url('perencanaan/tr_rsk_mata/ajax_add')?>">
    <div class="row">
      <div class="col-xs-12">
                <br>

                <div class="form-group">

                <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value->tahun)?$value->tahun:'','tahun','tahun','form-control','required','inline');?>
                  </div>

                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value->id_perencanaan)?$value->id_perencanaan : '( Auto )'?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>

                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','6')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','4','6')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($value->id_provinsi) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','4','6')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','4','6')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_kabupaten(isset($value->id_kabupaten) ? $value->id_kabupaten : $this->session->userdata('data_user')->id_kabupaten ,'id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>

                  <label class="control-label col-md-2">Rumah Sakit</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_rsu(isset($value) ? $value->kode_rsu : $this->session->userdata('data_user')->kode_rsu,'kode_rsu','rsk_mata-box','form-control','required '.$readonly.'','inline');?>
                  </div>

                </div>
                <?php }?>
                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('A','B','C','D');
                          foreach($katergori_kelas as $row_katergori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_katergori_kelas?>" <?php echo isset($value) ? ($value->kategori_kelas == $row_katergori_kelas) ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo $row_katergori_kelas?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>


                <?php 
                  $arr_vf = '';
                  $value_form = $this->prc_rsk_mata->get_value_form($value->id_perencanaan); //print_r($this->db->last_query());
                  if(!empty($value_form)){
                  foreach ($form_entry_rsk_mata->result() as $key1 => $value_rsk_mata) {
                    $arr_vf[]= ''.$value_rsk_mata->id_jenis_sdmk.',';
                ?>
                <div class="form-group">
                  <label class="control-label col-md-3"><?php echo $value_rsk_mata->nama_jenis_sdmk?></label>
                  <div class="col-md-2" style="padding-top:10px">
                    <input  name="value[]" id="jml_<?php echo $value_rsk_mata->id_jenis_sdmk?>" value="<?php echo ($value_rsk_mata->id_jenis_sdmk == $value_form[$key1]->id_jenis_sdmk) ? $value_form[$key1]->value : 0 ;?>" class="form-control" type="text">
                    <input  name="id_jenis_sdmk_form[]" value="<?php echo $value_rsk_mata->id_jenis_sdmk?>" class="form-control" type="hidden">
                  </div>
                </div>
                <?php 
                  } }
                  $array_vf = $arr_vf;
                ?>

      </div>
    </div>

    <div class="row">
          <div class="col-xs-12">

              <center><h3>Perencanaan Kebutuhan SDM Kesehatan</h3></center>
              <br>
                 
              <table class="table table-striped table-bordered table-hover">
                 <thead>
                  <tr>  
                    <th class="center" rowspan="2">&nbsp;</th>
                    <th class="center" rowspan="2">Jenis SDMK</th>
                    <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                    <th class="center" rowspan="2">SDMK Standar</th>
                    <th class="center" rowspan="2">Kesenjangan</th>
                  </tr>

                  <tr>
                    <th class="center">PNS/<br>Pegawai Tetap</th>
                    <th class="center">PPPK</th>
                    <th class="center">PTT</th>
                    <th class="center">Honorer/<br>Kontrak</th>
                    <th class="center">BLUD</th>
                    <th class="center">TKS</th>
                    <th class="center">Total</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <?php for($i=1;$i<12;$i++){?>
                    <td class="center">( <?php echo $i?> )</td>
                    <?php }?>
                  </tr>

                  <?php 
                      $no = 1;
                      $jenis_sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$value->id_perencanaan,'id_jenis_faskes'=>15, 'active'=>'Y', 'kategori_kelas'=>$value->kategori_kelas, 'tahun'=>$value->tahun)); 

                      //echo '<pre>';print_r($this->db->last_query());die;
                      //echo '<pre>'; print_r($jenis_sdmk);
                      foreach($jenis_sdmk as $key=>$row_jenis_sdmk){
                        
                      // jika jenis sdmk memiliki sub, maka form input disabled
                      $readonly = ( count($row_jenis_sdmk->sub_parent) == 0) ? '' : 'readonly' ;
                      
                      // jika jenis sdmk tidak memiliki standar minimal maka diblok warna merah //
                      $label = ($row_jenis_sdmk->is_standar == 'N') ? 'style="background-color:#F8E0E0;"' : '' ;
                      $jml_standar = ($row_jenis_sdmk->jml_standar_form != NULL) ? $row_jenis_sdmk->jml_standar_form : $row_jenis_sdmk->jml_standar;

                    
                  ?>
                      <tr <?php echo isset($label)?$label:''?>>
                        <td class="center"> <?php echo ''.$no.'.'?> </td>
                        <td> <?php echo $row_jenis_sdmk->nama_jenis_sdmk?> <input type="hidden" name="id_jenis_sdmk[]" value="<?php echo $row_jenis_sdmk->id_jenis_sdmk?>"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_pns)?$row_jenis_sdmk->jml_pns:0?>" id="pns<?php echo $key?>" name="pns[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px" > </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_pppk)?$row_jenis_sdmk->jml_pppk:0?>" id="pppk<?php echo $key?>" name="pppk[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_ptt)?$row_jenis_sdmk->jml_ptt:0?>" id="ptt<?php echo $key?>" name="ptt[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_honorer)?$row_jenis_sdmk->jml_honorer:0?>" id="honorer<?php echo $key?>" name="honorer[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_blud)?$row_jenis_sdmk->jml_blud:0?>" id="blud<?php echo $key?>" name="blud[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_tks)?$row_jenis_sdmk->jml_tks:0?>" id="tks<?php echo $key?>" name="tks[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->total_jml)?$row_jenis_sdmk->total_jml:0?>" id="total<?php echo $key?>" name="total[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px" readonly> </td>
                        <td class="center"> <input type="hidden" value="<?php echo $jml_standar?>" name="jml_standar[]"> <?php echo $jml_standar?> </td>
                        <td class="center"> 
                          <?php echo Apps::counting('-', $row_jenis_sdmk->total_jml, $jml_standar)?>
                        </td>
                      </tr>
                        <?php 
                          $alp = 'a'; 
                          foreach($row_jenis_sdmk->sub_parent as $key2=>$row_sub_parent){
                          $label2 = ($row_sub_parent->is_standar == 'N') ? 'style="background-color:#F8E0E0;"' : '' ;
                        ?>
                        <tr <?php echo isset($label2)?$label2:''?>>
                          <td> &nbsp; </td>
                          <td> <?php echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$alp.'.&nbsp; '.$row_sub_parent->nama_jenis_sdmk.''?> <input type="hidden" name="id_jenis_sdmk_sub[]" value="<?php echo $row_sub_parent->id_jenis_sdmk?>"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_pns)?$row_sub_parent->jml_pns:0?>" id="pns_sub<?php echo $key2?>" name="pns_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_pppk)?$row_sub_parent->jml_pppk:0?>" id="pppk_sub<?php echo $key2?>" name="pppk_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_ptt)?$row_sub_parent->jml_ptt:0?>" id="ptt_sub<?php echo $key2?>" name="ptt_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_honorer)?$row_sub_parent->jml_honorer:0?>" id="honorer_sub<?php echo $key2?>" name="honorer_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_blud)?$row_sub_parent->jml_blud:0?>" id="blud_sub<?php echo $key2?>" name="blud_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_tks)?$row_sub_parent->jml_tks:0?>" id="tks_sub<?php echo $key2?>" name="tks_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->total_jml)?$row_sub_parent->total_jml:0?>" id="total_sub<?php echo $key2?>" name="total_sub[]" style="width:50px" readonly> </td>
                          <td class="center"> <input type="hidden" value="<?php echo $jml_standar?>" name="jml_standar_sub[]"> <?php echo $jml_standar?> </td>
                          <td class="center"> 
                            <?php echo Apps::counting('-', $row_sub_parent->total_jml, $jml_standar)?>
                          </td>
                        </tr>
                        <?php $alp++; }?>
                  <?php $no++;}?>

                  <!-- <tr>
                    <td class="center" colspan="2"><b>TOTAL</b></td>
                    <?php for($i=0;$i<8;$i++){?>
                      <td class="center" id="totalpns<?php echo $i?>">  </td>
                    <?php }?>
                  </tr> -->

                </tbody>
              </table>

              <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_perencanaan:0?>"> -->

                  <a onclick="getMenu('perencanaan/tr_rsk_mata')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="getMenu('perencanaan/tr_rsk_mata/riwayat_perencanaan')" href="#" class="btn btn-sm btn-warning">
                    <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
                    Lihat Riwayat Perencanaan
                  </a>
                  <button type="reset" onclick="getMenu('perencanaan/tr_rsk_mata')" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
                    Reset Form
                  </button>
                  <button type="submit" name="submit" id="btnSave2" value="save_methode" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>

          </div>
        </div>
      </form>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url().'assets/js/custom/prc_rsk_mata.js'?>"></script>
