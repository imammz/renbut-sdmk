<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<div class="modal-header">
    <button type="button" class="close" onclick='close_box()' data-dismiss="modal" aria-hidden="true">&times;</button>
    <h2><?php echo $this->lang["confirmation"]; ?></h2>
</div>
<div class="modal-body">
    <p><?php echo $this->lang["are_you_sure,_want_to_delete_image_selected_?"]; ?></p>
</div>
<div class="modal-footer border-top">
    <button class="btn primary right medium" onclick="this_img_delete('<?php echo $url ?>');"><span><?php echo $this->lang['yes']; ?></span></button>
</div> 