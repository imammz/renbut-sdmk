<?php echo Modules::run('templates/cliptwo/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >

                <div class="container-fluid container-fullw bg-white">
                    <div class="row">
                        <div class="col-md-8">
                            &nbsp;
                        </div>
                        <ol class="breadcrumb">
                            <li>
                                <span>Laporan</span>
                            </li>
                            <li>
                                <span>Pilih Jenis Faskes</span>
                            </li>
                            <li class="active">
                                <span>UPT</span>
                            </li>
                        </ol>
                    </div>

                    <!-- start: YOUR CONTENT HERE -->                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">                                            
                                <div class="panel-body" style="min-height: 500px;">
                                    <div class="tabbable">
                                        <ul id="myTab1" class="nav nav-tabs">
                                            <?php if ($_SESSION['user']['faskes_kode'] == 'A') { ?>
                                                <li class="active">
                                                    <a href="#tab_kabupaten" data-toggle="tab">Per-Kabupaten/Kota</a>
                                                </li>
                                                <?php
                                            }
                                            if ($_SESSION['user']['NO_KAB'] == 'A') {
                                                ?>
                                                <li>
                                                    <a href="#tab_provinsi" data-toggle="tab">Per-Provinsi</a>
                                                </li>
                                                <?php
                                            }
                                            if ($_SESSION['user']['NO_PROV'] == 'A') {
                                                ?>
                                                <li>
                                                    <a href="#tab_nasional" data-toggle="tab">Nasional</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content">
                                         
                                            <?php if ($_SESSION['user']['faskes_kode'] == 'A') { ?>
                                                <div class="tab-pane fade in active" id="tab_kabupaten">

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            Periode (Tahun) 
                                                        </div>
                                                        <div class="col-md-2">   
                                                            <select class="js-example-basic-single js-states form-control" name="kab_tahun" id="kab_tahun" required>
                                                                <?php
                                                                foreach ($this->orm->tahun_entry() as $row) {
                                                                    $selected = '';
                                                                    if (date('Y') == $row['tahun']) {
                                                                        $selected = 'selected';
                                                                    }
                                                                    ?>    
                                                                    <option <?php echo $selected ?> value="<?php echo $row['tahun'] ?>"> <?php echo $row['tahun'] ?> </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">&nbsp;</div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            Pilih Kabupaten 
                                                        </div>
                                                        <div class="col-md-8">   
                                                            <select class="js-example-basic-single js-states" name="NO_KAB" id="NO_KAB" required>
                                                                <option value=""> ---------------------- Cari Kabupaten ---------------------- </option>
                                                                <?php
                                                                foreach ($master_kab as $row) {
                                                                    $selected = '';
                                                                    if (isset($_SESSION['user']['NO_KAB'])) {
                                                                        if ($_SESSION['user']['NO_KAB'] == $row['NO_KAB']) {
                                                                            $selected = 'selected';
                                                                        }
                                                                    }
                                                                    ?>    
                                                                    <option <?php echo $selected ?> value="<?php echo $row['NO_KAB'] ?>"> <?php echo $row['NAMA_KAB'] . ' Prov. ' . $row['NAMA_PROV'] ?> </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 20px;">
                                                        <div class="col-md-2">

                                                        </div>
                                                        <div class="col-md-12" style="text-align: right;">   
                                                            <button type="button" class="btn  btn-success" onclick="kab_cetak();"><i class="fa fa-file-excel-o"></i> Cetak</button>
                                                            <button type="button" class="btn  btn-primary" onclick="kab_proses();"><i class="fa fa-refresh"></i> Proses</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            <?php } if ($_SESSION['user']['NO_KAB'] == 'A') { ?>
                                                <div class="tab-pane" id="tab_provinsi">

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            Periode (Tahun) 
                                                        </div>
                                                        <div class="col-md-8">   
                                                            <select class="js-example-basic-single js-states form-control" name="prov_tahun" id="prov_tahun" required>
                                                                <?php
                                                                foreach ($this->orm->tahun_entry() as $row) {
                                                                    $selected = '';
                                                                    if (date('Y') == $row['tahun']) {
                                                                        $selected = 'selected';
                                                                    }
                                                                    ?>    
                                                                    <option <?php echo $selected ?> value="<?php echo $row['tahun'] ?>"> <?php echo $row['tahun'] ?> </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">&nbsp;</div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            Pilih Provinsi 
                                                        </div>
                                                        <div class="col-md-8">   
                                                            <select class="js-example-basic-single js-states" name="NO_PROV" id="NO_PROV" required>
                                                                <option value=""> ---------------------- Cari Provinsi ---------------------- </option>
                                                                <?php
                                                                foreach ($master_prov as $row) {
                                                                    $selected = '';
                                                                    if (isset($_SESSION['user']['NO_PROV'])) {
                                                                        if ($_SESSION['user']['NO_PROV'] == $row['NO_PROV']) {
                                                                            $selected = 'selected';
                                                                        }
                                                                    }
                                                                    ?>    
                                                                    <option <?php echo $selected ?> value="<?php echo $row['NO_PROV'] ?>"> <?php echo ' Prov. ' . $row['NAMA_PROV'] ?> </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 20px;">
                                                        <div class="col-md-2">

                                                        </div>
                                                        <div class="col-md-12" style="text-align: right;">   
                                                            <button type="button" class="btn  btn-success" onclick="prov_cetak();"><i class="fa fa-file-excel-o"></i> Cetak</button>
                                                            <button type="button" class="btn  btn-primary" onclick="prov_proses();"><i class="fa fa-refresh"></i> Proses</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            <?php } if ($_SESSION['user']['NO_PROV'] == 'A') { ?>
                                                <div class="tab-pane" id="tab_nasional">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            Periode (Tahun) 
                                                        </div>
                                                        <div class="col-md-8">   
                                                            <select class="js-example-basic-single js-states form-control" name="nas_tahun" id="nas_tahun" required>
                                                                <?php
                                                                foreach ($this->orm->tahun_entry() as $row) {
                                                                    $selected = '';
                                                                    if (date('Y') == $row['tahun']) {
                                                                        $selected = 'selected';
                                                                    }
                                                                    ?>    
                                                                    <option <?php echo $selected ?> value="<?php echo $row['tahun'] ?>"> <?php echo $row['tahun'] ?> </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row" style="padding-top: 20px;">
                                                        <div class="col-md-2">

                                                        </div>
                                                        <div class="col-md-12" style="text-align: right;">   
                                                            <button type="button" class="btn  btn-success" onclick="nas_cetak();"><i class="fa fa-file-excel-o"></i> Cetak</button>
                                                            <button type="button" class="btn  btn-primary" onclick="nas_proses();"><i class="fa fa-refresh"></i> Proses</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>   
                                    </div>                                                   
                                </div>
                            </div>

                        </div>



                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
                                                            jQuery(document).ready(function () {
                                                                Main.init();
                                                                FormElements.init();
                                                            });

                                                            function faskes_cetak() {
                                                                //  alert($("#faskes_kode").val());
                                                                var faskes_kode = $("#faskes_kode").val();
                                                                var faskes_tahun = $("#faskes_tahun").val();

                                                                if (faskes_kode === '') {
                                                                    alert('Pilih Terlebih Dahulu Faskes Dinaskesehatan');
                                                                }
                                                                else {
                                                                    window.location.href = "<?php echo base_url() ?>index.php/laporan/dinaskesehatan/faskes_excel/" + faskes_kode + '/' + faskes_tahun;
                                                                }
                                                            }
                                                            function faskes_proses() {
                                                                //  alert($("#faskes_kode").val());
                                                                var faskes_kode = $("#faskes_kode").val();
                                                                var faskes_tahun = $("#faskes_tahun").val();
                                                                if (faskes_kode === '') {
                                                                    alert('Pilih Terlebih Dahulu Faskes Dinaskesehatan');
                                                                }
                                                                else {
                                                                    GB_show("Laporan Kebutuhan SDMK Faskes ", '<?php echo base_url() ?>index.php/laporan/dinaskesehatan/faskes_proses/' + faskes_kode + '/' + faskes_tahun, 700, 1100);
                                                                    $('html, body').animate({scrollTop: 0}, 'slow');
                                                                }


                                                            }

                                                            function kab_cetak() {
                                                                //  alert($("#faskes_kode").val());
                                                                var NO_KAB = $("#NO_KAB").val();
                                                                var kab_tahun = $("#kab_tahun").val();
                                                                if (NO_KAB === '') {
                                                                    alert('Pilih Terlebih Dahulu Kabupaten/Kota');
                                                                }
                                                                else {
                                                                    window.location.href = "<?php echo base_url() ?>index.php/laporan/dinaskesehatan/kab_excel/" + NO_KAB + '/' + kab_tahun;
                                                                }
                                                            }
                                                            function kab_proses() {
                                                                //  alert($("#faskes_kode").val());
                                                                var NO_KAB = $("#NO_KAB").val();
                                                                var kab_tahun = $("#kab_tahun").val();
                                                                if (NO_KAB === '') {
                                                                    alert('Pilih Terlebih Dahulu Kabupaten/Kota');
                                                                }
                                                                else {
                                                                    GB_show("Laporan Kebutuhan SDMK Kabupaten ", '<?php echo base_url() ?>index.php/laporan/dinaskesehatan/kab_proses/' + NO_KAB + '/' + kab_tahun, 700, 1100);
                                                                    $('html, body').animate({scrollTop: 0}, 'slow');
                                                                }


                                                            }

                                                            function prov_cetak() {
                                                                //  alert($("#faskes_kode").val());
                                                                var NO_PROV = $("#NO_PROV").val();
                                                                var prov_tahun = $("#prov_tahun").val();
                                                                if (NO_PROV === '') {
                                                                    alert('Pilih Terlebih Dahulu Provinsi');
                                                                }
                                                                else {
                                                                    window.location.href = "<?php echo base_url() ?>index.php/laporan/dinaskesehatan/prov_excel/" + NO_PROV + '/' + prov_tahun;
                                                                }
                                                            }
                                                            function prov_proses() {
                                                                //  alert($("#faskes_kode").val());
                                                                var NO_PROV = $("#NO_PROV").val();
                                                                var prov_tahun = $("#prov_tahun").val();
                                                                if (NO_PROV === '') {
                                                                    alert('Pilih Terlebih Dahulu Provinsi');
                                                                }
                                                                else {
                                                                    GB_show("Laporan Kebutuhan SDMK Provinsi ", '<?php echo base_url() ?>index.php/laporan/dinaskesehatan/prov_proses/' + NO_PROV + '/' + prov_tahun, 700, 1100);
                                                                    $('html, body').animate({scrollTop: 0}, 'slow');
                                                                }


                                                            }

                                                            function nas_cetak() {
                                                                //  alert($("#faskes_kode").val());
                                                                var nas_tahun = $("#nas_tahun").val();
                                                                window.location.href = "<?php echo base_url() ?>index.php/laporan/dinaskesehatan/nas_excel/" + nas_tahun;
                                                            }
                                                            function nas_proses() {
                                                                //  alert($("#faskes_kode").val());
                                                                var nas_tahun = $("#nas_tahun").val();

                                                                GB_show("Laporan Kebutuhan SDMK Nasional ", '<?php echo base_url() ?>index.php/laporan/dinaskesehatan/nas_proses/' + nas_tahun, 700, 1100);
                                                                $('html, body').animate({scrollTop: 0}, 'slow');



                                                            }

    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
