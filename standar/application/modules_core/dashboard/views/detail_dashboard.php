<link rel="stylesheet" href="<?php echo base_url()?>assets/css/fullcalendar.css" />
<div class="page-header">
  <h1>
    <?php echo $modul->title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $modul->subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">
  <!-- PAGE CONTENT BEGINS -->
    <?php //echo '<pre>'; print_r($this->session->all_userdata())?>
      <div class="row">

        <div class="space-6"></div>

        <div class="col-sm-7">
          
          <!--CONTENT-->
          <div>
            <div id="user-profile-1" class="user-profile row">
              <div class="col-xs-12 col-sm-3 center">
                <div>
                  <!-- #section:pages/profile.picture -->
                  <span class="profile-picture">
                    <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="<?php echo base_url()?>assets/avatars/profile-pic.jpg" />
                  </span>
                </div>

                <div class="space-6"></div>

              </div>

              <div class="col-xs-12 col-sm-9">

                <!-- #section:pages/profile.info -->
                <div class="profile-user-info profile-user-info-striped">
                  <div class="profile-info-row">
                    <div class="profile-info-name"> Username </div>

                    <div class="profile-info-value">
                      <span class="editable" id="username"><?php echo $this->session->userdata('data_user')->fullname?></span>
                    </div>
                  </div>

                  <div class="profile-info-row">
                    <div class="profile-info-name"> Location </div>

                    <div class="profile-info-value">
                      <i class="fa fa-map-marker light-orange bigger-110"></i>
                      <span class="editable" id="country">Netherlands</span>
                      <span class="editable" id="city">Amsterdam</span>
                    </div>
                  </div>

                  <div class="profile-info-row">
                    <div class="profile-info-name"> Age </div>

                    <div class="profile-info-value">
                      <span class="editable" id="age">38</span>
                    </div>
                  </div>

                  <div class="profile-info-row">
                    <div class="profile-info-name"> Joined </div>

                    <div class="profile-info-value">
                      <span class="editable" id="signup">2010/06/20</span>
                    </div>
                  </div>

                  <div class="profile-info-row">
                    <div class="profile-info-name"> Last Online </div>

                    <div class="profile-info-value">
                      <span class="editable" id="login">3 hours ago</span>
                    </div>
                  </div>

                  <div class="profile-info-row">
                    <div class="profile-info-name"> About Me </div>

                    <div class="profile-info-value">
                      <span class="editable" id="about">Editable as WYSIWYG</span>
                    </div>
                  </div>
                </div>

                <!-- /section:pages/profile.info -->
                <div class="space-20"></div>

                <div class="widget-box transparent">
                  <div class="widget-header widget-header-small">
                    <h4 class="widget-title blue smaller">
                      <i class="ace-icon fa fa-rss orange"></i>
                      Recent Activities
                    </h4>

                    <div class="widget-toolbar action-buttons">
                      <a href="#" data-action="reload">
                        <i class="ace-icon fa fa-refresh blue"></i>
                      </a>
&nbsp;
                      <a href="#" class="pink">
                        <i class="ace-icon fa fa-trash-o"></i>
                      </a>
                    </div>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main padding-8">
                      <!-- #section:pages/profile.feed -->
                      <div id="profile-feed-1" class="profile-feed">
                        <div class="profile-activity clearfix">
                          <div>
                            <img class="pull-left" alt="Alex Doe's avatar" src="<?php echo base_url()?>assets/avatars/avatar5.png" />
                            <a class="user" href="#"> <?php echo $this->session->userdata('data_user')->fullname?> </a>
                            update master data jenis sdmk

                            <div class="time">
                              <i class="ace-icon fa fa-clock-o bigger-110"></i>
                              2 hour ago
                            </div>
                          </div>

                          <div class="tools action-buttons">
                            <a href="#" class="blue">
                              <i class="ace-icon fa fa-pencil bigger-125"></i>
                            </a>

                            <a href="#" class="red">
                              <i class="ace-icon fa fa-times bigger-125"></i>
                            </a>
                          </div>
                        </div>


                      </div>

                      <!-- /section:pages/profile.feed -->
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>


        </div>

        <div class="vspace-12-sm"></div>

        <div class="col-sm-5">
          <center> <h2>Kalender Kegiatan</h2></center>
          <br >
          <div id="calendar"></div>
        </div><!-- /.col -->
      </div><!-- /.row -->

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url()?>assets/js/jquery-ui.custom.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/moment.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar.js"></script>
<script src="<?php echo base_url()?>assets/js/bootbox.js"></script>
<!-- inline scripts related to this page -->
    <script type="text/javascript">
      jQuery(function($) {

/* initialize the external events
  -----------------------------------------------------------------*/

  $('#external-events div.external-event').each(function() {

    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
    // it doesn't need to have a start or end
    var eventObject = {
      title: $.trim($(this).text()) // use the element's text as the event title
    };

    // store the Event Object in the DOM element so we can get to it later
    $(this).data('eventObject', eventObject);

    // make the event draggable using jQuery UI
    $(this).draggable({
      zIndex: 999,
      revert: true,      // will cause the event to go back to its
      revertDuration: 0  //  original position after the drag
    });
    
  });




  /* initialize the calendar
  -----------------------------------------------------------------*/

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();


  var calendar = $('#calendar').fullCalendar({
    //isRTL: true,
     buttonHtml: {
      prev: '<i class="ace-icon fa fa-chevron-left"></i>',
      next: '<i class="ace-icon fa fa-chevron-right"></i>'
    },
  
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    events: [
      {
      title: 'All Day Event',
      start: new Date(y, m, 1),
      className: 'label-important'
      },
      {
      title: 'Long Event',
      start: moment().subtract(5, 'days').format('YYYY-MM-DD'),
      end: moment().subtract(1, 'days').format('YYYY-MM-DD'),
      className: 'label-success'
      },
      {
      title: 'Some Event',
      start: new Date(y, m, d-3, 16, 0),
      allDay: false,
      className: 'label-info'
      }
    ]
    ,
    editable: true,
    droppable: true, // this allows things to be dropped onto the calendar !!!
    drop: function(date, allDay) { // this function is called when something is dropped
    
      // retrieve the dropped element's stored Event Object
      var originalEventObject = $(this).data('eventObject');
      var $extraEventClass = $(this).attr('data-class');
      
      
      // we need to copy it, so that multiple events don't have a reference to the same object
      var copiedEventObject = $.extend({}, originalEventObject);
      
      // assign it the date that was reported
      copiedEventObject.start = date;
      copiedEventObject.allDay = allDay;
      if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];
      
      // render the event on the calendar
      // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
      $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
      
      // is the "remove after drop" checkbox checked?
      if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
      }
      
    }
    ,
    selectable: true,
    selectHelper: true,
    select: function(start, end, allDay) {
      
      bootbox.prompt("New Event Title:", function(title) {
        if (title !== null) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay,
              className: 'label-info'
            },
            true // make the event "stick"
          );
        }
      });
      

      calendar.fullCalendar('unselect');
    }
    ,
    eventClick: function(calEvent, jsEvent, view) {

      //display a modal
      var modal = 
      '<div class="modal fade">\
        <div class="modal-dialog">\
         <div class="modal-content">\
         <div class="modal-body">\
           <button type="button" class="close" data-dismiss="modal" style="margin-top:-10px;">&times;</button>\
           <form class="no-margin">\
            <label>Change event name &nbsp;</label>\
            <input class="middle" autocomplete="off" type="text" value="' + calEvent.title + '" />\
           <button type="submit" class="btn btn-sm btn-success"><i class="ace-icon fa fa-check"></i> Save</button>\
           </form>\
         </div>\
         <div class="modal-footer">\
          <button type="button" class="btn btn-sm btn-danger" data-action="delete"><i class="ace-icon fa fa-trash-o"></i> Delete Event</button>\
          <button type="button" class="btn btn-sm" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Cancel</button>\
         </div>\
        </div>\
       </div>\
      </div>';
    
    
      var modal = $(modal).appendTo('body');
      modal.find('form').on('submit', function(ev){
        ev.preventDefault();

        calEvent.title = $(this).find("input[type=text]").val();
        calendar.fullCalendar('updateEvent', calEvent);
        modal.modal("hide");
      });
      modal.find('button[data-action=delete]').on('click', function() {
        calendar.fullCalendar('removeEvents' , function(ev){
          return (ev._id == calEvent._id);
        })
        modal.modal("hide");
      });
      
      modal.modal('show').on('hidden', function(){
        modal.remove();
      });


      //console.log(calEvent.id);
      //console.log(jsEvent);
      //console.log(view);

      // change the border color just for fun
      //$(this).css('border-color', 'red');

    }
    
  });


})
    </script>