<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['edit_profile'] = 'ubah profil';
$lang['change_password'] = 'ubah password';
$lang['this_page_to_manage_dashboard,_see_documentation_for_use_it'] = 'Halaman ini untuk mengatur dashboard,  lihat dokumentasi untuk menggunakannya';
$lang['blog_post'] = 'artikel blog'; 
$lang['comment'] = 'komentar';
$lang['visitor'] = 'pengunjung';
$lang['language'] = 'bahasa'; 
$lang['browsers'] = 'browser';
$lang['countries'] = 'negara';
$lang['inbox'] = 'Inbox';
$lang['message'] = 'Pesan';
$lang['contact'] = 'Kontak';