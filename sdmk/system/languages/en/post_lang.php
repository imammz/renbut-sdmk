<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang['article'] = 'article';
$lang['photo'] = 'photo';
$lang['shop'] = 'shop';
$lang['polling'] = 'polling';
$lang['quote'] = 'quote';
$lang['link'] = 'link';
$lang['audio'] = 'audio';
$lang['video'] = 'video';
$lang['choose_category'] = 'choose category';
$lang['this_page_to_manage_post_content,_see_documentation_for_use_it'] = 'This page to manage post content, see documentation for use it';
$lang['this_page_to_manage_post_categories,_see_documentation_for_use_it'] = 'This page to manage post categories, see documentation for use it';
/**
 * audio
 */
$lang['enter_track_name'] = 'Enter track name';
$lang['external_url'] = 'External url';
$lang['play_list'] = 'Play list';
$lang['embed_code'] = 'Embed code';
$lang['video_url'] = 'Video url';
/**
 * photo
 */
$lang['browse_from_media'] = 'browse from media';
$lang['drop_here'] = 'Drop Here';

$lang['set_as_publish'] = 'set as publish';
$lang['set_as_draft'] = 'set as draft';