<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
?>  
<form id="crud-data">  
    <table class="sort table table-no-border" style="position: relative" table-check="true" ajax-sort-url="user/sort" cellspacing="0" cellpadding="0">
        <tr tabindex="0" class="ui-state-disabled">
            <th style="width: 3%"><input type="checkbox" table-check-all="true" id="check-all"> <label for="check-all"></label></th>  
            <th style="width: 30%">
                <span class="left"><?php echo $this->lang['name'] ?></span>
                <span class="right">
                    <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('fullname', 'user/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('fullname', 'user/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th>  
            <th style="width: 15%">
                <span class="left">Level</span>
                <span class="right">
                    <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('level', 'user/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('level', 'user/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th>  
            <th style="width: 23%">
                <span class="left">Email</span>
                <span class="right">
                    <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('email', 'user/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('email', 'user/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th>  
            <th class="no-phone"> &nbsp; </th> 
        </tr>
        <?php
        $no = $offset + 1;
        foreach ($rs as $row) {
            ?>
            <tr tabindex="<?php echo $no ?>" table-check-this="1" id="id_<?php echo $row->id ?>" style="position: relative">
                <td>
                    <p><input type="checkbox" name="id[]" value="<?php echo $row->id ?>" id="<?php echo $row->id ?>"> <label for ="<?php echo $row->id ?>"></label></p>
                </td> 
                <td> <?php if (isset($row->photo)) : ?>
                        <img class="xamzo-user-photo xamzo-image-circle xamzo-photo-small" src="<?php echo $row->photo ?>">
                    <?php else:
                        ?>
                        <img class="xamzo-user-photo xamzo-image-circle xamzo-photo-small" src="<?php echo site_url('upload/pp/default_x_user.jpg') ?>">
                    <?php endif; ?>      

                    <span class="xamzo-name">  
                            <?php echo character_limiter($row->fullname, 10) ?> 
                    </span>
                </td>
                <td>
                    <?php
                    switch ($row->level) {
                        case 'admin':
                            echo 'Administrator';
                            break;
                        case 'user':
                            echo 'Regular User';
                            break;
                        default:
                            break;
                    }
                    ?>
                </td>
                <td>
                    <?php echo $row->email; ?>
                </td>
                <td class="no-phone"> 
                    <span class="xamzo-post-link blue" ><a ajax="true" href="<?php echo site_url('user/get_auth/' . $row->id) ?>" modal-fix="false" ajax-target="modalbox" modal-width="500px" class="tip-bottom-right pd" original-title="<?php echo $this->lang['set_user_authentication'] ?>"><i class="fa fa-cog"></i> <?php echo $this->lang['set_user_authentication'] ?></a></span>
                </td>
            </tr>
            <?php
            $no++;
        }
        ?>
    </table>
    <div class="xamzo-pagination">
        <div id="option">
            <?php
            $selected = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : '';
            echo $this->lang['showing_1_to'] . form_dropdown($opt_limit, $selected, array('title' => 'limit', 'onchange' => 'change_limit(this)')) . "&nbsp;" . $this->lang['of'] . " $total " . $this->lang['entries'];
            ?>                     
        </div>
        <div id="pagging">
            <?php echo $paging; ?> 
        </div>
    </div>    
</form>