<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Portal Aplikasi Perencanaan Kebutuhan SDMK</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/style-responsive.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/open-sans.css">
        <link rel="stylesheet" href="css/dialog.css">
        <link rel="stylesheet" href="css/dialog-sally.css">
        <link rel="icon" href="images/icon.png">
        <!-- CHART -->
        <link rel="stylesheet" href="css/chartist.min.css">
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        
        <style>
        /*
Inspired by the dribble shot http://dribbble.com/shots/1285240-Freebie-Flat-Pricing-Table?list=tags&tag=pricing_table
*/

/*--------- Font ------------*/
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);
@import url(http://weloveiconfonts.com/api/?family=fontawesome);
/* fontawesome */
[class*="fontawesome-"]:before {
  font-family: 'FontAwesome', sans-serif;
}
* {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
    vertical-align: baseline;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
/*------ utiltity classes -----*/
.fl{ float:left; }
.fr{ float: right; }
/*its also known as clearfix*/
.group:before,
.group:after {
    content: "";
    display: table;
} 
.group:after {
    clear: both;
}
.group {
    zoom: 1;  /*For IE 6/7 (trigger hasLayout) */
}

body {
    background: #F2F2F2;
    font-family: 'Droid Sans', sans-serif;
    line-height: 1;
    font-size: 16px;    
}
.wrapper {
    
}
.pricing-table {
    width: 80%;
    margin: 50px auto;
    text-align: center;
    padding: 10px;
    padding-right: 0;
}
.pricing-table .heading{
    color: #9C9E9F;
    text-transform: uppercase;
    font-size: 1.3rem;
    margin-bottom: 4rem;
}
.block{
    width: 30%;    
    margin: 0 15px;
    overflow: hidden;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;    
/*    border: 1px solid red;*/
}
/*Shared properties*/
.title,.pt-footer{
    color: #FEFEFE;
    text-transform: capitalize;
    line-height: 2.5;
    position: relative;
}
.content{
    position: relative;
    color: #FEFEFE;
    padding: 20px 0 10px 0;
}
/*arrow creation*/
.content:after, .content:before,.pt-footer:before,.pt-footer:after {
	top: 100%;
	left: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;
}
.pt-footer:after,.pt-footer:before{
    top:0;
}
.content:after,.pt-footer:after {
	border-color: rgba(136, 183, 213, 0);	
	border-width: 5px;
	margin-left: -5px;
}
/*/arrow creation*/
.price{
    position: relative;
    display: inline-block;
    margin-bottom: 0.625rem;
}
.price span{    
    font-size: 6rem;
    letter-spacing: 8px;
    font-weight: bold;        
}
.price sup{
    font-size: 1.5rem;    
    position: absolute;    
    top: 12px;
    left: -12px;
}
.hint{
    font-style: italic;
    font-size: 0.9rem;
}
.features{
    list-style-type: none;    
    background: #FFFFFF;
    text-align: left;
    color: #9C9C9C;
    padding:30px 22%;
    font-size: 0.9rem;
}
.features li{
    padding:15px 0;
    width: 100%;
}
.features li span{
   padding-right: 0.4rem; 
}
.pt-footer{
    font-size: 0.95rem;
    text-transform: capitalize;
}
/*PERSONAL*/
.personal .title{        
    background: #78CFBF;    
}
.personal .content,.personal .pt-footer{
    background: #82DACA;
}
.personal .content:after{	
	border-top-color: #82DACA;	
}
.personal .pt-footer:after{
    border-top-color: #FFFFFF;
}
/*PROFESSIONAL*/
.professional .title{
    background: #3EC6E0;
}
.professional .content,.professional .pt-footer{
    background: #53CFE9;
}
.professional .content:after{	
	border-top-color: #53CFE9;	
}
.professional .pt-footer:after{
    border-top-color: #FFFFFF;
}
/*BUSINESS*/
.business .title{
    background: #E3536C;
}
.business .content,.business .pt-footer{
    background: #EB6379;
}
.business .content:after{	
	border-top-color: #EB6379;	
}
.business .pt-footer:after {	
	border-top-color: #FFFFFF;	
}
        </style>
        
        
    </head>
    <body onload="video();">
        <!-- HEADER -->
     
        <!-- END HEADER -->

        <!-- MENU -->
        <section id="menu">
            <div class="container">
                <div class="row">
                <img src="main_portal.jpg" style="margin-top:-20px">
                
                <div class="row" style="margin-top:-53%;padding-bottom:18%">
                   
                    <div class="wrapper">
        <!-- PRICING-TABLE CONTAINER -->
        <div class="pricing-table group">
            <!-- PERSONAL -->
            <div class="block personal fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/klinik/add" ><b>KLINIK</b></a></h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block professional fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/bapelkes/add" ><b>BAPELKES</b></a></h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block business fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/kkp/add" ><b>KKP</b></a> </h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block personal fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/labkes/add" ><b>LABKES</b></a></h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block professional fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/bpkm/add" ><b>BPKM</b></a></h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block business fl">
                <h2 class="title"> <a href="abk/index.php/config/masterdata/btklp/add" ><b>BPTLKPP</b></a></h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block personal fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/faskeslain/add" ><b>FASKES LAIN</b></a></h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block professional fl">
                <h2 class="title"> <a href="abk/index.php/config/masterdata/upt/add" ><b>UPT Pusat</b></a> </h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block business fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/uptdprov/add" ><b>UPT Provinsi</b></a></h2>
            </div>
            <!-- /PERSONAL -->
            <!-- PERSONAL -->
            <div class="block personal fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/uptdkab/add" ><b>UPT Kab/Kota</b></a></h2>
            </div>
            <!-- /PERSONAL -->
             <!-- PERSONAL -->
            <div class="block professional fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/rumahsakit/add" ><b>Rumah Sakit</b></a></h2>
            </div>
            <!-- /PERSONAL -->
             <!-- PERSONAL -->
            <div class="block business fl">
                <h2 class="title"><a href="abk/index.php/config/masterdata/puskesmas/add" ><b>PUSKESMAS</b></a></h2>
            </div>
            <!-- /PERSONAL -->            
            
            
           
            <!-- /BUSINESS -->
        </div>
        <!-- /PRICING-TABLE -->
    </div>
                    
                   
                    </div>
                </div>
                

                
                </div>
                
                


        </section>
        <!-- END MENU -->


     
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/dialogFx.js"></script>
        <script type="text/javascript" src="js/chartist.min.js"></script>
        <script type="text/javascript" src="js/DKPP.js"></script>
       

        
    </body>
</html>