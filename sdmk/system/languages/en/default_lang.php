<?php if (!defined('BASEPATH')) exit('no direct script user allowed');
 
$lang['cancel'] = 'cancel';
$lang['upgrade'] = 'upgrade'; 
$lang['go_to_web'] = 'go to web';
$lang['user_statistic'] = 'user statistic';
$lang['hello'] = 'hello';
$lang['website'] = 'website'; 
$lang['logout'] = 'logout';
$lang['update_available'] = 'update available';
$lang['comments_unread'] = 'comments unread';
$lang['view_all'] = 'View all';
/**
 * Heading
 */
$lang['order_notice'] = 'order notice';
$lang['click_here'] = 'click here'; 
$lang['see_documentation'] = 'see documentation';
$lang['search_all_modules_by_name'] = 'Search all modules by name';
/**
 * Tables
 */
$lang['form'] = 'form';
$lang['author'] = 'author';
$lang['create'] = 'create'; 
$lang['insert'] = 'insert'; 
$lang['update'] = 'update'; 
$lang['update_now'] = 'update now';
$lang['edit'] = 'edit'; 
$lang['back'] 	= 'back'; 
$lang['index'] 	= 'index'; 
$lang['upload'] = 'upload';
$lang['delete'] = 'delete';
$lang['reset'] 	= 'reset'; 
$lang['close'] 	= 'close'; 
$lang['exit'] 	= 'exit'; 
$lang['submit'] = 'submit'; 
$lang['yes'] = 'yes';
$lang['no'] = 'no';
$lang['copy'] = 'copy';
$lang['browse'] = 'browse';
$lang['select'] = 'select';
$lang['link'] = 'link';
$lang['all'] = 'all';
$lang['draft'] = 'draft';
$lang['publish'] = 'publish';
$lang['preview'] = 'preview';
$lang['search'] = 'search';
$lang['type'] = 'type';
$lang['search_by_name'] = 'search by name';
$lang['sort_by_ascending'] = 'sort by ascending';
$lang['sort_by_descending'] = 'sort by descending';
$lang['name'] = 'name';
$lang['title'] = 'title'; 
$lang['date'] = 'date';
$lang['url'] = 'url';
$lang['permalink'] = 'permalink';
$lang['content'] = 'content'; 
$lang['keyword'] = 'keyword';
$lang['description'] = 'description';
$lang['email'] = 'email';  
$lang['writer'] = 'writer';
$lang['select_all'] = 'select all';
$lang['create_data'] = 'create data';
$lang['update_data'] = 'update data';
$lang['delete_data'] = 'delete data';
$lang['data_list'] = 'data list';
$lang['refresh'] = 'refresh';
$lang['location'] = 'location';
$lang['country'] = 'country';
$lang['province'] = 'province';
$lang['regency'] = 'regency';
$lang['utility'] = 'utility';
$lang['slide'] = 'slide';
$lang['gallery'] = 'gallery';
$lang['phone'] = 'phone';
$lang['loker_category'] = 'loker category';
$lang['data_not_found'] = 'data not found';
$lang['experience'] = 'experience';
$lang['education'] = 'education';
$lang['loker'] = 'loker';
/**
 * Notive
 */
$lang['confirmation'] = 'confirmation';
$lang['only'] = 'only';
$lang['files_are_allowed'] = 'files are allowed';
$lang['file_max_size'] = 'file max size';
$lang['are_you_sure,_want_to_delete'] = 'are you sure, want to delete';
$lang['choose_one_yes_or_no'] = 'choose one yes or no';
/**
 * pagging 
 */
$lang['showing_1_to'] = 'showing 1 to';
$lang['of'] = 'of';
$lang['entries'] = 'entries';
/**
 * left bar 
 */
$lang['dashboard'] = 'dashboard';
$lang['page'] = 'page';
$lang['user'] = 'user';
$lang['post'] = 'post';
$lang['setting'] = 'setting';
$lang['category'] = 'category';
$lang['media'] = 'media';
$lang['menu'] = 'menu';
$lang['module'] = 'module';
$lang['widget'] = 'widget';
$lang['theme'] = 'theme';
$lang['activity'] = 'activity';
$lang['expand_menu'] = 'expand menu';
$lang['extensions'] = 'extensions';
$lang['market'] = 'market';
$lang['community'] = 'community';
$lang['codebin'] = 'codebin';
/**
 * bottom bar 
 */
$lang['about'] = 'about';
$lang['term_of_service'] = 'term of service';
$lang['privacy_policy'] = 'privacy policy';
$lang['help'] = 'help';