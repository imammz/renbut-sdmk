<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelompok_sdmk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_kelompok_sdmk_model','kelompok_sdmk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Kelompok SDMK";
		$data['subtitle'] = "Daftar Kelompok SDMK";
		$this->load->view('kelompok_sdmk/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Kelompok SDMK";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->kelompok_sdmk->get_by_id($id);
		}
		$this->load->view('kelompok_sdmk/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->kelompok_sdmk->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $kelompok_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($kelompok_sdmk->nama_kelompok_sdmk);
			$row[] = ($kelompok_sdmk->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $kelompok_sdmk->updated_date?Tanggal::formatDateTime($kelompok_sdmk->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($kelompok_sdmk->id_kelompok_sdmk,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_kelompok_sdmk('."'".Regex::_genRegex($kelompok_sdmk->id_kelompok_sdmk,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->kelompok_sdmk->count_all(),
						"recordsFiltered" => $this->kelompok_sdmk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_kelompok_sdmk = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'nama_kelompok_sdmk' => Regex::_genRegex($this->input->post('nama_kelompok_sdmk'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_kelompok_sdmk == 0 ){
			$this->kelompok_sdmk->save($dataexc);
		}else{
			$this->kelompok_sdmk->update(array('id_kelompok_sdmk'=>$id_kelompok_sdmk), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->kelompok_sdmk->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
