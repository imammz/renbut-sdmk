<script type="text/javascript">
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#puskes-box'));
                    });

                });
            } else {
                $('#puskes-box option').remove()
                $('<option value="">(Pilih Unit Kerja)</option>').appendTo($('#puskes-box'));
            }
        });

        $('select[name="kode_puskesmas"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_puskesmas/get_puskes_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_puskesmas').show();
                    $('#alamat_puskesmas_form').val(data.alamat);
                    $("input[name=id_tipe_kawasan][value=" + data.id_tipe_kawasan + "]").prop('checked', true);
                    $("input[name=id_tipe_puskesmas][value=" + data.id_tipe_puskesmas + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_prc_puskesmas" action="<?php echo site_url('perencanaan/tr_puskesmas/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_perencanaan:'( Auto )'?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                  <label class="control-label col-md-2">Jenis Faskes</label>
                  <div class="col-md-3">
                    <?php echo Master::get_master_jenis_faskes(1,'id_jenis_faskes','id_jenis_faskes','form-control','required readonly','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value)?$value->tahun:'','tahun','tahun','form-control','required','inline');?>
                  </div>
                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','5')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($value) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('5')) ) ? 'readonly' : '' ;  
                      echo Master::get_change_master_kabupaten(isset($value) ? $value->id_kabupaten : ($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '' ,'id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                <div class="form-group" id="form-puskesmas" >
                  <label class="control-label col-md-2">Puskesmas</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('5')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_puskesmas(isset($value) ? $value->kode_puskesmas : $this->session->userdata('data_user')->kode_puskesmas,'kode_puskesmas','puskes-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>
                
                <div class="form-group" id="alamat_puskesmas" style="display: none;">
                  <label class="control-label col-md-2">&nbsp;</label>
                  <div class="col-md-6">
                    <input name="alamat" id="alamat_puskesmas_form" value=""  class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kawasan</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $data_kawasan = Master::get_tipe_kawasan_data();
                          foreach($data_kawasan as $row_data_kawasan){
                        ?>
                          <label>
                            <input name="id_tipe_kawasan" type="radio" id="idtk<?php echo $row_data_kawasan['id_tipe_kawasan']?>" class="ace" value="<?php echo $row_data_kawasan['id_tipe_kawasan']?>" <?php echo isset($value) ? ($value->id_tipe_kawasan == $row_data_kawasan['id_tipe_kawasan']) ? 'checked="checked"' : '' : ''; ?>  />
                            <span class="lbl"> <?php echo $row_data_kawasan['nama_tipe_kawasan']?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Rawat Inap</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $data_tipe_puskesmas = Master::get_tipe_puskesmas_data();
                          foreach($data_tipe_puskesmas as $row_data_tipe_puskesmas){
                        ?>
                          <label>
                            <input name="id_tipe_puskesmas" type="radio" class="ace" value="<?php echo $row_data_tipe_puskesmas['id_tipe_puskesmas']?>" <?php echo isset($value) ? ($value->active == $row_data_tipe_puskesmas['id_tipe_puskesmas']) ? 'checked="checked"' : '' : ''; ?>  />
                            <span class="lbl"> <?php echo $row_data_tipe_puskesmas['nama_tipe_puskesmas']?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Jumlah Pustu</label>
                  <div class="col-md-2">
                    <input name="jumlah_pustu" id="jumlah_pustu" value="<?php echo isset($value)?$value->jumlah_pustu:''?>" placeholder="Jumlah Pustu" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Jumlah Desa</label>
                  <div class="col-md-2">
                    <input name="jumlah_desa" id="jumlah_desa" value="<?php echo isset($value)?$value->jumlah_desa:''?>" placeholder="Jumlah Desa" class="form-control" type="text">
                  </div>
                </div>

                <!-- <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div> -->

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_perencanaan:0?>"> -->

                  <a onclick="getMenu('perencanaan')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="getMenu('perencanaan/tr_puskesmas/riwayat_perencanaan')" href="#" class="btn btn-sm btn-warning">
                    <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
                    Lihat Riwayat Perencanaan
                  </a>
                  <button type="reset" onclick="getMenu('perencanaan/tr_puskesmas')" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/prc_puskesmas.js'?>"></script>
<script type="text/javascript">
  $(function() {
        $('select[name="organization_id"]').change(function() {

            $.getJSON("<?php echo site_url('rpc/get_div_by_org') ?>/" + $(this).val(), '', function(data) {
                $('#div-box option').remove()

                $.each(data, function(i, o) {
                    $('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#div-box'));
                });

            });

        });
        $('select[name="division_id"]').change(function() {

            $.getJSON("<?php echo site_url('rpc/get_dept_by_div') ?>/" + $(this).val(), '', function(data) {
                $('#dept-box option').remove()
                // $('<option value=""><?php echo l("(Please select)") ?></option>').appendTo($('#dept-box'));
                $.each(data, function(i, o) {
                    $('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#dept-box'));
                });

            });

        });
        $('select[name="org_id"]').change(function() {

            $.getJSON("<?php echo site_url('rpc/get_sub_by_org') ?>/" + $(this).val(), '', function(data) {
                $('#sub-box option').remove()
                // $('<option value=""><?php echo l("(Please select)") ?></option>').appendTo($('#sub-box'));
                $.each(data, function(i, o) {
                    $('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#sub-box'));
                });

            });

        });
        $('select[name="suborg_id"]').change(function() {

           $.getJSON("<?php echo site_url('rpc/get_unit_by_komp') ?>/" + $(this).val(), '', function(data) {
               $('#unit-box option').remove()
               $('<option value=""><?php echo l("(Please select)") ?></option>').appendTo($('#unit-box'));
               $.each(data, function(i, o) {
                   $('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#unit-box'));
               });

           });

       });
        $('select[name="ditjen_id"]').change(function() {

            $.getJSON("<?php echo site_url('rpc/get_subdit_by_ditjen') ?>/" + $(this).val(), '', function(data) {
                $('#subdit-box option').remove()
                $('<option value=""><?php echo l("(Please select)") ?></option>').appendTo($('#subdit-box'));
                $.each(data, function(i, o) {
                    $('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#subdit-box'));
                });

            });

        });
        $('select[name="subdit_id"]').change(function() {

            $.getJSON("<?php echo site_url('rpc/get_kasie_by_subdit') ?>/" + $(this).val(), '', function(data) {
                $('#subbag-box option').remove()
                $('<option value=""><?php echo l("(Please select)") ?></option>').appendTo($('#subbag-box'));
                $.each(data, function(i, o) {
                    $('<option value="' + o.id + '">' + o.name + '</option>').appendTo($('#subbag-box'));
                });

            });

        });
    });
</script>
