<?php

if(!function_exists('proyeksi_calculate')){
    function proyeksi_calculate($no_prov, $no_jenis_sdmk, $ref){
        $x = &get_instance();
        $x->__library(array('database','session'));
        $prefix = $x->db->prefix;
        return $x->db->select('th_2014, th_2015, th_2016, th_2017, th_2018, th_2019, th_2020, th_2021, th_2022, th_2023, th_2024, th_2025')
                ->from($prefix."tahun_proyeksi as t")
                ->left_join($prefix."proyeksi as p","p.id_proyeksi=t.id_proyeksi")
                ->where(array('p.no_prov'=>$no_prov,'p.no_jenis_sdmk'=>$no_jenis_sdmk, 't.kode_ref_proyeksi'=>$ref)) 
                ->get()->row();
    }
}