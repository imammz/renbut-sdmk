<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('rekapitulasi_model','rekapitulasi');
		$this->load->model('master_data/m_jenis_sdmk_model','jenis_sdmk');
		$this->load->model('perencanaan/prc_puskesmas_model','prc_puskesmas');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Laporan";
		$data['subtitle'] = "Daftar Menu Laporan";
		$this->load->view('index_main_menu', $data);
	}

	public function form_rekapitulasi($id_jenis_faskes)
	{
		
		$data['title'] = "Laporan";
		$data['subtitle'] = "Perencanaan Kebutuhan Minimal SDMK Puskesmas";
		$data['id_jenis_faskes'] = $id_jenis_faskes;
		$this->load->view('form_rekapitulasi', $data);
	}

	public function form_import()
	{
		
		$data['title'] = "Laporan";
		$data['subtitle'] = "Import Data Hasil Laporan";
		$this->load->view('form_import', $data);
	}

	

	public function preview($type='', $id_jf='', $id_perencanaan='', $all='', $class)
	{
		$data = array();
		$data['id_jenis_faskes'] = $id_jf;
		$data['type'] = $type;

		$perencanaan = $this->prc_puskesmas->get_by_id($id_perencanaan);

		if($all == 'N'){

			$this->view_rekapitulasi_not_all($type, $perencanaan, $class);

		}else{

			// VIEW ALL //
			//$this->view_rekapitulasi_all();

			if( $type == 'json' ){

				$query = $this->db->get_where('m_puskesmas', array('id_kabupaten'=>$perencanaan->id_kabupaten))->result_array();
				
				$getDatas = array();

				// looping puskesmas from no_kab
				foreach ($query as $key => $value) {
					# code...
					// get standar puskesmas //
					$query2 = $this->db->get_where('t_perencanaan', array('id_perencanaan'=>$id_perencanaan))->result_array(); 
					$getData = array();
					// for loop standar puskesmas
					foreach ($query2 as $key2 => $value2) {
						# code...
						// get detil puskesmas standar
						$detilPuskesmas = $this->_getDetilPuskesmas($value2['id_perencanaan']); //get detil puskesmas//
						
							$data = array(
								'id_perencanaan' => $value2['id_perencanaan'],
								'tahun' => $value2['tahun'],
								'id_jenis_faskes' => $value2['id_jenis_faskes'],
								'id_provinsi' => $value2['id_provinsi'],
								'id_kabupaten' => $value2['id_kabupaten'],
								'kode_puskesmas' => $value2['kode_puskesmas'],
								'id_tipe_puskesmas' => $value2['id_tipe_puskesmas'],
								'id_tipe_kawasan' => $value2['id_tipe_kawasan'],
								'jumlah_desa' => $value2['jumlah_desa'],
								'jumlah_pustu' => $value2['jumlah_pustu'],
								'standar_detil' => $detilPuskesmas,
							);
						// collect in array
						$getData[] = $data;

					}

				}
				//echo '<pre>';print_r($getData);die;
				header('Content-Type: application/json; charset=utf-8');
				header('Access-Control-Allow-Origin: *');
				$filename = ''.str_replace('-','',date('Y-m-d')).'-'.$perencanaan->kode_puskesmas.'-EXP-STANDAR.json';
				header('Content-Disposition: attachment; filename=' . $filename);
				echo json_encode($getData);

			}

			elseif ($type='excel') {
				$data['value'] = $perencanaan;
				$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$id_perencanaan,'id_jenis_faskes'=>$perencanaan->id_jenis_faskes,'id_tipe_puskesmas'=>$perencanaan->id_tipe_puskesmas, 'id_tipe_kawasan'=>$perencanaan->id_tipe_kawasan));
					//print_r($this->db->last_query());die;
				$this->load->view('preview', $data);
			}


		}

	}

	function view_rekapitulasi_not_all($type, $perencanaan, $class){

		if( $type == 'json' ){

			$query = $this->db->get_where('m_puskesmas', array('kode_puskesmas'=>$perencanaan->kode_puskesmas))->result_array();
			
			$getDatas = array();

			// looping puskesmas from no_kab
			foreach ($query as $key => $value) {
				# code...
				// get standar puskesmas //
				$query2 = $this->db->get_where('t_perencanaan', array('id_perencanaan'=>$id_perencanaan))->result_array(); 
				$getData = array();
				// for loop standar puskesmas
				foreach ($query2 as $key2 => $value2) {
					# code...
					// get detil puskesmas standar
					$detilPuskesmas = $this->_getDetilPuskesmas($value2['id_perencanaan']); //get detil puskesmas//
					
						$data = array(
							'id_perencanaan' => $value2['id_perencanaan'],
							'tahun' => $value2['tahun'],
							'id_jenis_faskes' => $value2['id_jenis_faskes'],
							'id_provinsi' => $value2['id_provinsi'],
							'id_kabupaten' => $value2['id_kabupaten'],
							'kode_puskesmas' => $value2['kode_puskesmas'],
							'id_tipe_puskesmas' => $value2['id_tipe_puskesmas'],
							'id_tipe_kawasan' => $value2['id_tipe_kawasan'],
							'jumlah_desa' => $value2['jumlah_desa'],
							'jumlah_pustu' => $value2['jumlah_pustu'],
							'standar_detil' => $detilPuskesmas,
						);
					// collect in array
					$getData[] = $data;

				}

			}
			header('Content-Type: application/json; charset=utf-8');
			header('Access-Control-Allow-Origin: *');
			$filename = 'EXP-PUSKESMAS.json';
			header('Content-Disposition: attachment; filename=' . $filename);
			echo json_encode($getData);

		}else{

			$data['type'] = $type;
			$data['value'] = $perencanaan; //print_r($data['value']);die;

			if($perencanaan->id_jenis_faskes == 3){
					$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_jenis_faskes'=>$perencanaan->id_jenis_faskes,'tahun'=>$perencanaan->tahun, 'kategori_kelas'=>$class));
				}elseif ($data['value']->id_jenis_faskes == 1) {
					$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$perencanaan->id_perencanaan,'id_jenis_faskes'=>$perencanaan->id_jenis_faskes,'id_tipe_puskesmas'=>$perencanaan->id_tipe_puskesmas, 'id_tipe_kawasan'=>$perencanaan->id_tipe_kawasan));
				}else{
					$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$perencanaan->id_perencanaan,'id_jenis_faskes'=>$perencanaan->id_jenis_faskes, 'active'=>'Y', 'kategori_kelas'=>$class, 'tahun'=>$perencanaan->tahun));
				}

				/*
			if($perencanaan->id_jenis_faskes == 1){
				$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$id_perencanaan,'id_jenis_faskes'=>$perencanaan->id_jenis_faskes,'id_tipe_puskesmas'=>$perencanaan->id_tipe_puskesmas, 'id_tipe_kawasan'=>$perencanaan->id_tipe_kawasan,'tahun'=>$perencanaan->tahun));
			}else{
				$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$perencanaan->id_perencanaan,'id_jenis_faskes'=>$perencanaan->id_jenis_faskes, 'active'=>'Y', 'kategori_kelas'=>$class, 'tahun'=>$perencanaan->tahun));
			}*/	
			
			//print_r($this->db->last_query());die;
			$this->load->view('preview', $data);
		
		}

	}


	function _getDetilPuskesmas($id_perencanaan){
		$detil_perencanaan = "SELECT * FROM t_detail_perencanaan WHERE id_perencanaan='".$id_perencanaan."'";
		$getDataDetil = array();
		$exc_query = $this->db->query($detil_perencanaan)->result_array();		
		foreach ($exc_query as $key => $value) {
			# code...
			$data = array(
			'id_perencanaan' => $value['id_perencanaan'],
			'id_jenis_sdmk' => $value['id_jenis_sdmk'],
			'jml_pns' => $value['jml_pns'],
			'jml_pppk' => $value['jml_pppk'],
			'jml_ptt' => $value['jml_ptt'],
			'jml_honorer' => $value['jml_honorer'],
			'jml_blud' => $value['jml_blud'],
			'jml_tks' => $value['jml_tks'],
			'total_standar' => $value['total_standar'],
			'total_jml' => $value['total_jml'],
			);
			$getDataDetil[] = $data;
		}

		return $getDataDetil;
	}

	public function view_rekapitulasi($y=0,$idp=0,$idk=0,$kp=0,$itk=0,$itp=0,$ijf='',$all='', $class='')
	{

		$all_puskesmas = Regex::_genRegex($all, 'RGXAZ');

		$data['title'] = "Laporan";
		$data['subtitle'] = "Perencanaan Kebutuhan Minimal SDMK Puskesmas";
		$data['id_jenis_faskes'] = $ijf;
		$data['isall'] = $all_puskesmas;
		$data['class'] = $class;

		$posted_data = array();
		$posted_data['tahun'] 					= Regex::_genRegex($y, 'RGXINT');
		$posted_data['id_jenis_faskes']			= Regex::_genRegex($ijf, 'RGXINT');

		if($idp != 0){
			$posted_data['id_provinsi'] 		= Regex::_genRegex($idp, 'RGXINT');
		}
		if($idk != 0){
			$posted_data['id_kabupaten'] 		= Regex::_genRegex($idk, 'RGXINT');
		}

		

		if( $all_puskesmas == 'Y' ){

			if($ijf == 1){
				$posted_data['id_tipe_kawasan'] 	= Regex::_genRegex($itk, 'RGXINT');
				$posted_data['id_tipe_puskesmas'] 	= Regex::_genRegex($itp, 'RGXINT');
			}else{
				$posted_data['id_tipe_kawasan'] 	= 0;
				$posted_data['id_tipe_puskesmas'] 	= 0;
			}

			$this->db->where($posted_data);
			$perencanaan = $this->db->get('t_perencanaan')->result();
			//print_r($this->db->last_query());die;

			$posted_data['id_provinsi'] 		= $idp;
			$posted_data['id_kabupaten'] 		= $idk;
			$posted_data['kategori_kelas'] 		= Regex::_genRegex($class, 'RGXAZ');

			//$data['value'] = $perencanaan;
			$data['posted'] = $posted_data;
			
			//echo '<pre>';print_r($data);die;
			$this->load->view('view_rekapitulasi_all', $data);

		}else{


			if($ijf == 1){ // puskesmas

				$posted_data['kode_puskesmas'] = Regex::_genRegex($kp, 'RGXQSL');
				if($itk != 0){
					$posted_data['id_tipe_kawasan'] 	= Regex::_genRegex($itk, 'RGXINT');
				}
				if($itp != 0){
					$posted_data['id_tipe_puskesmas'] 	= Regex::_genRegex($itp, 'RGXINT');
				}

			}elseif($ijf == 3){ // rs
				$posted_data['kode_rsu'] = Regex::_genRegex($kp, 'RGXQSL');
			}elseif($ijf == 29){ // BTKL
				$posted_data['kode_btkl'] = Regex::_genRegex($kp, 'RGXQSL');
			}elseif($ijf == 30){ // KKP
				$posted_data['kode_kkp'] = Regex::_genRegex($kp, 'RGXQSL');
			}elseif($ijf == 31){ // KLinik
				$posted_data['kode_klinik'] = Regex::_genRegex($kp, 'RGXQSL');
			}

			$this->db->where($posted_data);
			$data_row = $this->db->get('t_perencanaan')->row(); //print_r($this->db->last_query());die;

			if(!empty($data_row)){

				$detail_perencanaan = $this->prc_puskesmas->get_by_id($data_row->id_perencanaan);
				$data['value'] = $this->rekapitulasi->get_data_perencanaan($detail_perencanaan);

				if($data['value']->id_jenis_faskes == 3){
					$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_jenis_faskes'=>$data['value']->id_jenis_faskes,'tahun'=>$data['value']->tahun, 'kategori_kelas'=>$class));
				}elseif ($data['value']->id_jenis_faskes == 1) {
					$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$data['value']->id_perencanaan,'id_jenis_faskes'=>$data['value']->id_jenis_faskes,'id_tipe_puskesmas'=>$data['value']->id_tipe_puskesmas, 'id_tipe_kawasan'=>$data['value']->id_tipe_kawasan));
				}else{
					$data['jenis_sdmk'] = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$data['value']->id_perencanaan,'id_jenis_faskes'=>$data['value']->id_jenis_faskes, 'active'=>'Y', 'kategori_kelas'=>$class, 'tahun'=>$data['value']->tahun));
				}
				
			}else{

				$data['value'] = array();
			
			}
			//print_r($data['value']);die;
			$this->load->view('view_rekapitulasi', $data);
		}
	}

	public function post_form()
	{
		//print_r($_POST);die;
		$this->db->trans_begin();

		$posted_data = array(
			'tahun' => $this->input->post('tahun') ? Regex::_genRegex($this->input->post('tahun'), 'RGXINT') : 0,
			'id_provinsi' => $this->input->post('id_provinsi') ? Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT') : 0,
			'id_kabupaten' => $this->input->post('id_kabupaten') ? Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT') : 0,
			/*'kode_puskesmas' => $this->input->post('kode_puskesmas')?Regex::_genRegex($this->input->post('kode_puskesmas'), 'RGXQSL'):0,*/
			'id_tipe_kawasan' => $this->input->post('id_tipe_kawasan') ? Regex::_genRegex($this->input->post('id_tipe_kawasan'), 'RGXINT') : 0,
			'id_tipe_puskesmas' => $this->input->post('id_tipe_puskesmas') ? Regex::_genRegex($this->input->post('id_tipe_puskesmas'), 'RGXINT') : 0,
			'id_jenis_faskes' => $this->input->post('id_jenis_faskes') ? Regex::_genRegex($this->input->post('id_jenis_faskes'), 'RGXINT') : 0,
			'all_puskesmas' => $this->input->post('all_puskesmas')?Regex::_genRegex($this->input->post('all_puskesmas'), 'RGXAZ'):'N',
			'kategori_kelas' => Regex::_genRegex($this->input->post('kategori_kelas'), 'RGXAZ'),
			);

		if($this->input->post('id_jenis_faskes') == 1){ // puskesmas
			$posted_data['kode_puskesmas'] = $this->input->post('kode_puskesmas')?Regex::_genRegex($this->input->post('kode_puskesmas'), 'RGXQSL'):0;
		}elseif($this->input->post('id_jenis_faskes') == 3){ // rs
			$posted_data['kode_puskesmas'] = $this->input->post('kode_rs')?Regex::_genRegex($this->input->post('kode_rs'), 'RGXQSL'):0;
		}elseif($this->input->post('id_jenis_faskes') == 29){ // BTKL
			$posted_data['kode_puskesmas'] = $this->input->post('kode_btkl')?Regex::_genRegex($this->input->post('kode_btkl'), 'RGXQSL'):0;

		}elseif($this->input->post('id_jenis_faskes') == 30){ // KKP
			$posted_data['kode_puskesmas'] = $this->input->post('kode_kkp')?Regex::_genRegex($this->input->post('kode_kkp'), 'RGXQSL'):0;
		}elseif($this->input->post('id_jenis_faskes') == 31){ // Klinik
			$posted_data['kode_puskesmas'] = $this->input->post('kode_klinik')?Regex::_genRegex($this->input->post('kode_klinik'), 'RGXQSL'):0;
		}

		//print_r($posted_data);die;

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode($posted_data);
		}
		
	}

	public function preview_all($type='',$y='',$idp='',$idk='',$itk='',$itp='',$ijf='',$all='')
	{
		//$this->output->enable_profiler(TRUE);

		$posted_data = array();
		$posted_data['t_perencanaan.tahun'] 				= Regex::_genRegex($y, 'RGXINT');

		if($idp > 0){
			$posted_data['t_perencanaan.id_provinsi'] 		= Regex::_genRegex($idp, 'RGXINT');
		}

		if($idk > 0){
			$posted_data['t_perencanaan.id_kabupaten']		= Regex::_genRegex($idk, 'RGXINT');
		}

		if($ijf==1){
			if($itk != 0){
				$posted_data['t_perencanaan.id_tipe_kawasan'] 	= Regex::_genRegex($itk, 'RGXINT');
			}
			if($itp != 0){
				$posted_data['t_perencanaan.id_tipe_puskesmas'] 	= Regex::_genRegex($itp, 'RGXINT');
			}
		}

		$posted_data['t_perencanaan.id_jenis_faskes']		= Regex::_genRegex($ijf, 'RGXINT');
		

		$perencanaan = $this->prc_puskesmas->get_data_by_custom_for_rekapitulasi($posted_data);
		//print_r($this->db->last_query());die;

		if( $type == 'json' ){

			$rekapitulasi = $this->rekapitulasi->get_rekapitulasi($posted_data); //echo '<pre>';print_r($rekapitulasi); die;

			header('Content-Type: application/json; charset=utf-8');
			header('Access-Control-Allow-Origin: *');
			$filename = 'EXP-PUSKESMAS.json';
			header('Content-Disposition: attachment; filename=' . $filename);
			echo json_encode($rekapitulasi);

		}else{

			$data['result'] = $perencanaan;
			$data['posted'] = $posted_data;
			$data['id_jenis_faskes'] = $ijf;
			$data['isall'] = $all;
			$data['type'] = $type;

			//echo '<pre>';print_r($posted_data);die;
			$this->load->view('preview_all', $data);

		}


	}

	
}
