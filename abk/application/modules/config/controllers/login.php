<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends MX_Controller {

    public function __construct() {
        parent::__construct();
        
    }

    public function index() {
        // $this->checkLogout();
        $this->_checkMessage();
        $this->load->view('login_view');
    }

    public function proses() {

        $account_model = new account_model;

        $data = array();

        $this->output->enable_profiler(FALSE);

        $nama_pengguna = $this->input->post('nama_pengguna');
        $password = md5($this->input->post('password'));


        if (!empty($nama_pengguna) || !empty($password)) {

            if ($this->_mustBeRegister($nama_pengguna)) {
                $data = array();
                $data['nama_akun'] = $nama_pengguna;
                $_SESSION['user'] = $data;
                redirect('home/register');
            } else {

                if ($account_model->cekLogin($nama_pengguna, $password)) {

                    $user = $this->orm->tbl_akun->where('nama_akun', $nama_pengguna)->fetch();

                    $data['nama_akun'] = $user['nama_akun'];
                    $data['faskes_kode'] = $user['faskes_kode'];
                    $data['NO_KAB'] = $user['NO_KAB'];
                    $data['NO_PROV'] = $user['NO_PROV'];
                    $data['ref_jenis_faskes_kode'] = $user['ref_jenis_faskes_kode'];
                     
                            
                    $_SESSION['user'] = $data;
                    $_SESSION['login'] = TRUE;
                    $_SESSION['message'] = 'Berhasil Login';
                    
                    redirect('home');
                } else {
                    $_SESSION['info'] = 'nama pengguna atau password salah';
                    redirect('config/login');
                }
            }
        } else {
            $_SESSION['info'] = 'nama pengguna dan password harus diisi';
            redirect('config/login');
        }
    }

    public function logout() {
        unset($_SESSION['login']);
        session_destroy();
        $_SESSION['message'] = 'Berhasil Logout';
        redirect('config/login');
    }

    public function _mustBeRegister($nama_pengguna) {

        $account_model = new account_model();

        $ref_jenis_faskes_id = $account_model->_checkJenisFaskes($nama_pengguna);

        switch ($ref_jenis_faskes_id) {
            case 3 : //btkl
            case 4 : //kkp
            case 5 : //balai pelatihan kesehatan
            case 8 : // Balai Pengobatan / Kesehatan Masyarakat
            case 9 : //klinik
            case 10 : // Lab Kesehatan
            case 99 : //lainnya
                $return = TRUE;
                break;
            case 1:
            case 2:
            case 6:
            case 7:
                $return = FALSE;
                break;
            default :
                $return = FALSE;
                break;
        }
        
        
        if($return) {
             $user = $this->orm->tbl_akun->where('nama_akun', $nama_pengguna);
             $return = (COUNT($user)==0)?TRUE:FALSE;
        }


        return $return;
    }

}

?>