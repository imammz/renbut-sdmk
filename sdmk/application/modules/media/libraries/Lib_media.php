<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/media
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Lib_media {

    function _list() {
        $json = x_file_get_content(MODPATH . 'media/configs/config' . JSON);
        return x_read_json_decode($json);
    }

    function _config($keys) {
        $con = $this->_list();
        for ($i = 0; $i < count($con); $i++) {
            foreach ($con[$i] as $key => $val) {
                if ($keys == $key) {
                    return $val['val'];
                }
            }
        }
    }
    
    function _print() {
        $json = x_file_get_content(MODPATH . 'media/configs/report' . JSON);
        return x_read_json_decode($json);
    }

    function _report($keys) {
        $con = $this->_print();
        for ($i = 0; $i < count($con); $i++) {
            foreach ($con[$i] as $key => $val) {
                if ($keys == $key) {
                    return $val['val'];
                }
            }
        }
    }
    
    function _proj() {
        $json = x_file_get_content(MODPATH . 'media/configs/project' . JSON);
        return x_read_json_decode($json);
    }

    function _project($keys) {
        $con = $this->_proj();
        for ($i = 0; $i < count($con); $i++) {
            foreach ($con[$i] as $key => $val) {
                if ($keys == $key) {
                    return $val['val'];
                }
            }
        }
    }

    function set_thumb_big($path, $img) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $img;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $this->_config('big_thumb_width');
        $config['height'] = $this->_config('big_thumb_height');
        $config['thumb_marker'] = '{x}h_thumb_b';
        return $config;
    }

    function set_thumb_middle($path, $img) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $img;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $this->_config('middle_thumb_width');
        $config['height'] = $this->_config('middle_thumb_height');
        $config['thumb_marker'] = '{x}h_thumb_m';
        return $config;
    }

    function set_thumb_small($path, $img) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $img;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $this->_config('small_thumb_width');
        $config['height'] = $this->_config('small_thumb_height');
        $config['thumb_marker'] = '{x}h_thumb_s';
        return $config;
    }
    
    function set_thumb_avatar($path, $img) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $img;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 270;
        $config['height'] = 300;
        $config['thumb_marker'] = '{x}h_thumb_s';
        return $config;
    }

    function set_thumb_pp($path, $img) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $img;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = '150';
        $config['height'] = '150';
        $config['thumb_marker'] = '_pp';
        return $config;
    }

    function set_watermark($path, $img) {
        $wm_config['source_image'] = $path . $img; 
        $wm_config['image_library'] = 'gd2';
        $wm_config['wm_type'] = 'overlay';
        $wm_config['wm_overlay_path'] = $path.'wm/watermark.png'; 
        $wm_config['wm_vrt_alignment'] = 'middle';
        $wm_config['wm_hor_alignment'] = 'center'; 

        return $wm_config;
    }

} 