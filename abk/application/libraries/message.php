<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message {

    static function _set($id = FALSE, $msg) {

        $_SESSION['message']['title'] = ($id) ? 'Success' : 'Error';
        $_SESSION['message']['content'] = $msg;
        $_SESSION['message']['icon'] = ($id) ? 'fa-smile-o' : 'fa-frown-o';
    }

    static function _modal() {
      
          if (isset($_SESSION['message'])) {
        $title = (isset($_SESSION['message']['title'])?$_SESSION['message']['title']:'');
        $content = (isset($_SESSION['message']['content'])?$_SESSION['message']['content']:'');
        $icon = (isset($_SESSION['message']['title'])?$_SESSION['message']['title']:'');
          

        $html = '<div class="modal fade bs-example-modal-sm"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-dialog modal-sm">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
														<h4 class="modal-title" id="myModalLabel"><i class="fa '.$icon.'"></i>'.$title.'</h4>
													</div>
													<div class="modal-body">
														'.$content.'
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-primary btn-o" data-dismiss="modal">
															Close
														</button>
													</div>
												</div>
											</div>
										</div>
    ';




            echo $html;
           
           unset($_SESSION['message']);
        }
    }
    
static function _redirect($msg) {
     echo "<script> "
                    . "parent.window.location.replace('".$msg."'); "
                    . "</script>";
}

}

?>