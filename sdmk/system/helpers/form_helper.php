<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/form
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

if (!function_exists('form_dropdown')) {

    function form_dropdown($option = array(), $selected = null, $attr = array()) {
        $dropdown = '';
        $at = '';
        $opt = ''; 
        $dropdown.= "<select ";
        if (is_array($attr)) {
            foreach ($attr as $key => $value) {
                $at.=$key . '=' . $value." ";
            }
        }
        $dropdown.=$at . '>';

        if (is_array($option)) {
            foreach ($option as $key => $value) {
                if ($key == $selected) {
                    $sel=' selected';
                    $opt .="<option value='$key' $sel>$value</option>";
                }
                else{
                    $opt .="<option value='$key'>$value</option>";
                }
            }
        }
        $dropdown.=$opt . ' </select>';
        return $dropdown;
    }

}

/**
 * End of file ./system/helpers/form_helper.php
 */