<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sdmk extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
    }

    public function index($function, $subtitle) {
        $data = array();
        $data['class'] = 'masterdata';
        $data['subclass'] = $function;
        $data['title'] = "Master Data";
        $data['subtitle'] = $subtitle;
        $data['function'] = "$function";
        $this->load->view('masterdata_view', $data);
    }

    public function _example_output($output = null) {
        $this->load->view('sdmk_view', $output);
    }

    public function all() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_jenis_sdmk');
        $crud->set_subject('Jenis SDMK');
        $crud->display_as('ref_jenis_sdmk_kode', 'Kode SDMK');
        $crud->display_as('ref_jenis_sdmk_nama_level2', 'Nama SDMK');
        $crud->display_as('ref_kelompok_sdmk_id', 'Nama Kelompok');

        $crud->set_theme('twitter-bootstrap');

        $crud->set_relation('ref_kelompok_sdmk_id', 'ref_kelompok_sdmk', 'ref_kelompok_sdmk_nama');

        $crud->unset_fields('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');
        $crud->unset_columns('ref_jenis_sdmk_kode', 'ref_kelompok_sdmk_id', 'ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');

        $output = $crud->render();
        $output->title = 'Data Jenis SDMK';
        $this->_example_output($output);
    }

    public function jenis($rumpun) {
        $rumpun = str_replace('%20', ' ', $rumpun);

        $crud = new grocery_CRUD();

        $crud->set_table('ref_jenis_sdmk');
        $crud->set_subject('Jenis SDMK');
        $crud->display_as('ref_jenis_sdmk_kode', 'Kode SDMK');
        $crud->display_as('ref_jenis_sdmk_nama_level2', 'Nama SDMK');
        $crud->display_as('ref_kelompok_sdmk_id', 'Nama Kelompok');

        $crud->where('ref_jenis_sdmk_nama_level1', $rumpun);

        $crud->set_theme('twitter-bootstrap');

        $crud->set_relation('ref_kelompok_sdmk_id', 'ref_kelompok_sdmk', 'ref_kelompok_sdmk_nama');

        $crud->unset_fields('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');
        $crud->unset_columns('ref_kelompok_sdmk_id', 'ref_jenis_sdmk_kode', 'ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');

        
       $crud->callback_column('text_to_show', array($this, '_full_text'));
        
        $output = $crud->render();
        $output->title = 'Data Jenis SDMK - ' . $rumpun;
        $this->_example_output($output);
    }

    public function jenis2() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_jenis_sdmk');
        $crud->set_subject('Jenis SDMK');
        $crud->display_as('ref_jenis_sdmk_kode', 'Kode SDMK');
        $crud->display_as('ref_jenis_sdmk_nama_level2', 'Nama SDMK');
        $crud->display_as('ref_kelompok_sdmk_id', 'Nama Kelompok');

        $crud->where('ref_jenis_sdmk_nama_level1', 'Jabatan Fungsional Tertentu lainnya');

        $crud->set_theme('twitter-bootstrap');

        $crud->set_relation('ref_kelompok_sdmk_id', 'ref_kelompok_sdmk', 'ref_kelompok_sdmk_nama');

        $crud->unset_fields('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');
        $crud->unset_columns('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');

        $output = $crud->render();
        $output->title = 'Data Jenis SDMK';
        $this->_example_output($output);
    }

    function _full_text($value, $row) {
        return $value = wordwrap($row->text_to_show, 50, "<br>", true);
    }

}
