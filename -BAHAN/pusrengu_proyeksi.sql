-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 29, 2016 at 01:06 AM
-- Server version: 5.5.36-cll-lve
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pusrengu_proyeksi`
--

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenis_sdmk`
--

CREATE TABLE IF NOT EXISTS `ref_jenis_sdmk` (
  `no_jenis_sdmk` smallint(2) NOT NULL AUTO_INCREMENT,
  `nama_jenis_sdmk` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`no_jenis_sdmk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `ref_jenis_sdmk`
--

INSERT INTO `ref_jenis_sdmk` (`no_jenis_sdmk`, `nama_jenis_sdmk`) VALUES
(1, 'Dokter Spesialis'),
(2, 'Dokter Umum'),
(3, 'Dokter Gigi'),
(4, 'Perawat'),
(5, 'Bidan'),
(6, 'Perawat Gigi'),
(7, 'Apoteker'),
(8, 'Ass. Apoteker'),
(9, 'SKM'),
(10, 'Sanitarian'),
(11, 'Nutrision / Ahli Gizi'),
(12, 'Keterapian Fisik'),
(13, 'Keterapian Medis');

-- --------------------------------------------------------

--
-- Table structure for table `ref_provinsi`
--

CREATE TABLE IF NOT EXISTS `ref_provinsi` (
  `no_prov` smallint(2) NOT NULL,
  `nama_prov` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`no_prov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ref_provinsi`
--

INSERT INTO `ref_provinsi` (`no_prov`, `nama_prov`) VALUES
(11, 'Aceh'),
(12, 'Sumatera Utara'),
(13, 'Sumatera Barat'),
(14, 'Riau'),
(15, 'Jambi'),
(16, 'Sumatera Selatan'),
(17, 'Bengkulu'),
(18, 'Lampung'),
(19, 'Kep. Bangka Belitung '),
(21, 'Kep. Riau'),
(31, 'DKI Jakarta '),
(32, 'Jawa Barat'),
(33, 'Jawa Tengah'),
(34, 'DI Jogja2'),
(35, 'Jawa Timur'),
(36, 'Banten'),
(51, 'Bali'),
(52, 'Nusa Tenggara Barat'),
(53, 'Nusa Tenggara Timur'),
(61, 'Kalimantan Barat'),
(62, 'Kalimantan Tengah'),
(63, 'Kalimantan Selatan'),
(64, 'Kalimantan Timur'),
(65, 'Kalimantan Utara'),
(71, 'Sulawesi Utara'),
(72, 'Sulawesi Tengah'),
(73, 'Sulawesi Selatan'),
(74, 'Sulawesi Tenggara'),
(75, 'Gorontalo'),
(76, 'Sulawesi Barat'),
(81, 'Maluku'),
(82, 'Maluku Utara'),
(91, 'Papua Barat'),
(94, 'Papua ');

-- --------------------------------------------------------

--
-- Table structure for table `ref_proyeksi`
--

CREATE TABLE IF NOT EXISTS `ref_proyeksi` (
  `kode_ref_proyeksi` smallint(2) NOT NULL AUTO_INCREMENT,
  `nama_ref_proyeksi` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kode_ref_proyeksi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `ref_proyeksi`
--

INSERT INTO `ref_proyeksi` (`kode_ref_proyeksi`, `nama_ref_proyeksi`) VALUES
(1, 'Penduduk pada tahun awal'),
(2, 'Target Rasio Nakes 2014, 2019, 2025'),
(3, 'Kebutuhan Nakes berdasarkan Rasio Penduduk	'),
(4, 'Jumlah tenaga'),
(5, 'Pengangkatan baru'),
(6, 'Pindah masuk'),
(7, 'Keseluruhan masuk'),
(8, 'Pensiun'),
(9, 'Meninggal dan tdk mampu bekerja karena sakit'),
(10, 'Keluar-cuti besar-dipecat'),
(11, 'Keseluruhan keluar'),
(12, 'Tenaga (Perawat) yang ada diakhir tahun'),
(13, 'Kesenjangan Kebutuhan Nakes');

-- --------------------------------------------------------

--
-- Table structure for table `sdmk_proyeksi`
--

CREATE TABLE IF NOT EXISTS `sdmk_proyeksi` (
  `id_proyeksi` int(11) NOT NULL AUTO_INCREMENT,
  `no_prov` smallint(2) DEFAULT NULL,
  `no_jenis_sdmk` smallint(2) DEFAULT NULL,
  `ratio` varchar(5) DEFAULT NULL,
  `pengangkatan_baru` varchar(5) DEFAULT NULL,
  `pindah_masuk` varchar(5) DEFAULT NULL,
  `pensiun` varchar(5) DEFAULT NULL,
  `meninggal_tdkmampu_cacat` varchar(5) DEFAULT NULL,
  `keluar_cuti_dipecat` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_proyeksi`),
  KEY `idx_proyeksi_1` (`no_prov`),
  KEY `idx_proyeksi_2` (`no_jenis_sdmk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sdmk_proyeksi`
--

INSERT INTO `sdmk_proyeksi` (`id_proyeksi`, `no_prov`, `no_jenis_sdmk`, `ratio`, `pengangkatan_baru`, `pindah_masuk`, `pensiun`, `meninggal_tdkmampu_cacat`, `keluar_cuti_dipecat`) VALUES
(1, 35, 4, '0.75', '6.81', '0.50', '1.00', '0.50', '1.00');

-- --------------------------------------------------------

--
-- Table structure for table `sdmk_tahun_proyeksi`
--

CREATE TABLE IF NOT EXISTS `sdmk_tahun_proyeksi` (
  `id_proyeksi` int(11) DEFAULT NULL,
  `kode_ref_proyeksi` smallint(2) DEFAULT NULL,
  `th_2014` varchar(15) DEFAULT NULL,
  `th_2015` varchar(15) DEFAULT NULL,
  `th_2016` varchar(15) DEFAULT NULL,
  `th_2017` varchar(15) DEFAULT NULL,
  `th_2018` varchar(15) DEFAULT NULL,
  `th_2019` varchar(15) DEFAULT NULL,
  `th_2020` varchar(15) DEFAULT NULL,
  `th_2021` varchar(15) DEFAULT NULL,
  `th_2022` varchar(15) DEFAULT NULL,
  `th_2023` varchar(15) DEFAULT NULL,
  `th_2024` varchar(15) DEFAULT NULL,
  `th_2025` varchar(15) DEFAULT NULL,
  KEY `idx_th_proyeksi_1` (`id_proyeksi`),
  KEY `idx_th_proyeksi_2` (`kode_ref_proyeksi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sdmk_tahun_proyeksi`
--

INSERT INTO `sdmk_tahun_proyeksi` (`id_proyeksi`, `kode_ref_proyeksi`, `th_2014`, `th_2015`, `th_2016`, `th_2017`, `th_2018`, `th_2019`, `th_2020`, `th_2021`, `th_2022`, `th_2023`, `th_2024`, `th_2025`) VALUES
(1, 1, '39000000', '39292500', '39587193.75', '39884097.703125', '40183228.435898', '44000000', '44330000', '44662475', '44997443.5625', '45334924.389218', '45674936.322137', '47000000'),
(1, 2, '158', '162', '167', '171', '176', '180', '183', '187', '190', '193', '197', '200'),
(1, 3, '61620', '63811.02', '66031.437924000', '68281.574064000', '70561.748368', '79200', '81271.666666666', '83369.953333333', '85495.141700000', '87647.519733333', '89827.374133333', '94000'),
(1, 4, '43000', '45068.3', '47236.0708', '49508.1224', '53167.8985', '55725.2312', '58405.6037', '61214.8842', '64159.2776', '67245.3255', '70479.810000000', '73869.849900000'),
(1, 5, '2928.2999999999', '3069.1308', '3216.7715999999', '4649.9361', '3620.6726999999', '3794.8724999999', '3977.3804999999', '4168.6734', '4369.2279', '4579.3844999999', '4799.6199', '5030.4788999999'),
(1, 6, '215', '225.34', '236.18', '247.54', '265.835', '278.625', '292.02500000000', '306.07', '320.795', '336.225', '352.395', '369.345'),
(1, 7, '3143.2999999999', '3294.4708', '3452.9515999999', '4897.4761', '3886.5076999999', '4073.4974999999', '4269.4055', '4474.7433999999', '4690.0229', '4915.6095', '5152.0149', '5399.8238999999'),
(1, 8, '430', '450.68', '472.36', '495.08', '531.67', '557.25', '584.05000000000', '612.14', '641.59', '672.45', '704.79', '738.69'),
(1, 9, '215', '225.34', '236.18', '247.54', '265.835', '278.625', '292.02500000000', '306.07', '320.795', '336.225', '352.395', '369.345'),
(1, 10, '430', '450.68', '472.36', '495.08', '531.67', '557.25', '584.05000000000', '612.14', '641.59', '672.45', '704.79', '738.69'),
(1, 11, '1075', '1126.7', '1180.9', '1237.7', '1329.1749999999', '1393.125', '1460.125', '1530.35', '1603.975', '1681.125', '1761.975', '1846.7250000000'),
(1, 12, '45068.3', '47236.0708', '49508.1224', '53167.8985', '55725.2312', '58405.6037', '61214.8842', '64159.2776', '67245.3255', '70479.810000000', '73869.849900000', '77422.948800000'),
(1, 13, '16551.699999999', '16574.949199999', '16523.315524000', '15113.675564000', '14836.517167999', '20794.3963', '20056.782466666', '19210.675733333', '18249.8162', '17167.709733333', '15957.524233333', '16577.051199999');

-- --------------------------------------------------------

--
-- Table structure for table `sdmk_user`
--

CREATE TABLE IF NOT EXISTS `sdmk_user` (
  `id` int(11) NOT NULL,
  `level` enum('admin','user') COLLATE utf8_bin DEFAULT 'user',
  `fullname` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `born` text COLLATE utf8_bin,
  `birth_day` date DEFAULT NULL,
  `sex` enum('1','0') COLLATE utf8_bin DEFAULT '1',
  `photo` varchar(225) COLLATE utf8_bin DEFAULT 'upload/pp/default_x_user.jpg',
  `address` text COLLATE utf8_bin,
  `registred` datetime NOT NULL,
  `description` varchar(225) COLLATE utf8_bin DEFAULT NULL,
  `publish` enum('0','1') COLLATE utf8_bin DEFAULT '1',
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lspg_fk13` (`fullname`,`photo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `sdmk_user`
--

INSERT INTO `sdmk_user` (`id`, `level`, `fullname`, `email`, `password`, `born`, `birth_day`, `sex`, `photo`, `address`, `registred`, `description`, `publish`, `order`) VALUES
(1374050643, 'admin', 'Administrator', 'administrator@sdmk.io', '200ceb26807d6bf99fd6f4f0d1ca54d4', 'Jakarta', '1990-11-28', '1', 'upload/pp/xamzo{x}amir_fast3{x}2016-02-16{x}03-12-30{x}1374050643_pp.jpg', '-', '2015-01-20 00:00:00', '', '1', 0),
(1430679644, 'user', 'Regular User', 'regularuser@sdmk.io', '7f4f225298a70f22e1e46645d3f5e60f', 'Jakarta', '1990-11-28', '1', 'upload/pp/default_x_user.jpg', NULL, '2015-05-04 02:01:22', NULL, '1', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
