<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrasi extends MX_Controller {

    function __construct() {
		parent::__construct();
                $this->load->library('grocery_CRUD');
				$this->load->model('entry/faskes_model');
				
	}
        
        
        public function rumah_sakit() {

	$this->output->enable_profiler(TRUE);

	$this->db->select('*');
	$this->db->from('ref_rumahsakit_import');
	$new_rs = $this->db->get()->result_array();
	
	foreach($new_rs as $row) {
		
		$check = $this->_checkKodeRS($row['KODE RS']);

		if($check) {
			$data = array();
			$data['kode_rs'] = $row['KODE RS'];
			$data['no_kab'] = $row['KODE Kab'];
			$data['no_prov'] = $row['Kode Prov'];
			$data['Nama_rs'] = $row['NAMA RS'];
			$data['alamat'] = $row['ALAMAT'];
			$data['operator'] = $row['PENYELENGGARA'];
			$data['kd_op'] = 'NULL';
			$data['JENIS_RS'] = $row['JENIS RS'];
			$data['KD_JENIS_RS'] = $row['Jenis RS 2'];
			$data['KELAS_RS'] = $row['KLS RS'];
			$data['JML_TT'] = 'NULL';

			$this->db->insert('ref_rumahsakit',$data);	
		}

			
		
	}
            
        }
        
        
        public function rumah_sakit_update() {

	$this->output->enable_profiler(TRUE);

	$this->db->select('*');
	$this->db->from('ref_rumahsakit_import');
	$new_rs = $this->db->get()->result_array();
	
	foreach($new_rs as $row) {
		

			$data = array();
			$data['no_kab'] = $row['KODE Kab'];
			$data['no_prov'] = $row['Kode Prov'];
			
			$this->db->update('ref_rumahsakit',$data,'kode_rs = "'.$row['KODE RS'].'"');	
		
	}
            
        }
        
        


	public function _checkKodeRS($kode_rs) {

		$this->db->select('kode_rs');
		$this->db->from('ref_rumahsakit');
                $this->db->where('kode_rs',$kode_rs);
                
		$get = $this->db->get()->result_array();

		if(COUNT($get)==0) {
			return TRUE;
		} 
		
		else{
			return FALSE;
		}
	
	}
        
        
        
        public function puskesmas() {

	$this->output->enable_profiler(TRUE);

	$this->db->select('*');
	$this->db->from('ref_puskesmas_import');
	$new_rs = $this->db->get()->result_array();
	
	foreach($new_rs as $row) {
		
		$check = $this->_checkKodePuskesmas($row['id_unit']);

		if($check) {
                    
                    $Kode_pus_kab = substr($row['id_unit'], 0, 5);
                    $no_kab = str_replace('P', '', $Kode_pus_kab);
                    
			$data = array();
			$data['Kode_puskesmas'] = $row['id_unit'];
			$data['Kode_pus_kab'] = $Kode_pus_kab;
			$data['no_kab'] = $no_kab;
			$data['no_prov'] = $row['kode_prov'];
			$data['Nama_pus_kab'] = $row['nama_unit'];
			$data['Alamat'] = $row['Alamat'];
			

			$this->db->insert('ref_puskesmas',$data);	
		}

			
		
	}
	
            
        }
        
        public function puskesmas_akun() {

	$this->output->enable_profiler(TRUE);

	$this->db->select('*');
	$this->db->from('ref_puskesmas_import');
	$new_rs = $this->db->get()->result_array();
	
	foreach($new_rs as $row) {
		
		$check = $this->_checkKodePuskesmasAkun($row['id_unit']);

		if($check) {
                    
                    $Kode_pus_kab = substr($row['id_unit'], 0, 5);
                    $no_kab = str_replace('P', '', $Kode_pus_kab);
                    
			$data = array();
			$data['nama_akun'] = $row['id_unit'];
			$data['password_akun'] = $Kode_pus_kab;
			$data['NO_KAB'] = $no_kab;
			$data['NO_PROV'] = $row['kode_prov'];
			$data['faskes_kode'] = $row['id_unit'];

                        $this->db->insert('tbl_akun',$data);	
		}

			
		
	}
	
            
        }
        

        public function _checkKodePuskesmasAkun($kode) {

		$this->db->select('nama_akun');
		$this->db->from('tbl_akun');
                $this->db->where('nama_akun',$kode);
                
		$get = $this->db->get()->result_array();

		if(COUNT($get)==0) {
			return TRUE;
		} 
		else{
			return FALSE;
		}
	
	}
        

	public function _checkKodePuskesmas($kode) {

		$this->db->select('Kode_puskesmas');
		$this->db->from('ref_puskesmas');
                $this->db->where('Kode_puskesmas',$kode);
                
		$get = $this->db->get()->result_array();

		if(COUNT($get)==0) {
			return TRUE;
		} 
		else{
			return FALSE;
		}
	
	}
        
		
		        
         
        
     
		
}

