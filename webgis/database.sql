/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.34 : Database - polygon2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`polygon2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `new_polygon`;

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(100) DEFAULT NULL,
  `lan` varchar(100) DEFAULT NULL,
  `lnt` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `location` */

insert  into `location`(`id`,`location_name`,`lan`,`lnt`) values (1,'Polisi',NULL,NULL);

/*Table structure for table `markers` */

DROP TABLE IF EXISTS `markers`;

CREATE TABLE `markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `address` varchar(80) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `type` varchar(30) NOT NULL,
  `info1` varchar(100) DEFAULT NULL,
  `info2` varchar(100) DEFAULT NULL,
  `info3` varchar(100) DEFAULT NULL,
  `info4` varchar(100) DEFAULT NULL,
  `id_provinsi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `markers` */

insert  into `markers`(`id`,`name`,`address`,`lat`,`lng`,`type`,`info1`,`info2`,`info3`,`info4`,`id_provinsi`) values (33,'asd','asd',-0.380857,112.888184,'','asd','asd','asd','asd',3),(34,'asd','asd',-1.025336,112.573242,'','asd','asd','asd','asd',3),(35,'asd','asd',0.468743,115.825195,'','asd','asd','asd','asd',3),(36,'asd','asd',-1.659436,114.074707,'','l','l','l','l',3),(37,'t','l',-2.110363,120.974121,'','l','l','l','l',4),(38,'ads','asd',-6.945194,107.637634,'','k','k','k','k',2),(39,'a','kj',-6.248866,106.899605,'','k','k','k','k',2),(40,'qwe','we',-7.228066,112.750854,'','qwe','w','w','w',2),(41,'s','sssdf',-7.836808,112.021179,'','sdf','sdf','sdf','sdf',2),(42,'qwe','sd',-0.539787,115.744629,'','sdf','sdf','sdf','sdf',3),(43,'ascgfdg','dfg',2.514330,116.491699,'','dfg','dfg','dfg','dfgdgf',3),(44,'asd','asd',-3.590908,119.655762,'','asd','asd','asd','asd',4);

/*Table structure for table `profesi` */

DROP TABLE IF EXISTS `profesi`;

CREATE TABLE `profesi` (
  `id_profesi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_profesi` varchar(100) NOT NULL,
  PRIMARY KEY (`id_profesi`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `profesi` */

insert  into `profesi`(`id_profesi`,`nama_profesi`) values (1,'Dokter'),(2,'Tenaga Kesehatan'),(3,'Suster'),(4,'Bidan');

/*Table structure for table `provinsi` */

DROP TABLE IF EXISTS `provinsi`;

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `polygon2` text,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `provinsi` */

insert  into `provinsi`(`id_provinsi`,`nama_provinsi`,`latitude`,`longitude`,`polygon2`) values (1,'Sumatera','-0.589724','101.343106',''),(2,'Jawa','-7.53132','110.74877','-6.69256,111.69757 -6.51885,111.02172 -6.39353,110.85276 -6.50838,110.72774 -6.93435,110.0822 -6.79344,109.06577 -6.76662,108.66742 -6.3141,108.324 -6.16479,107.75194 -6.06818,107.01431 -5.99768,105.9773 -6.51693,105.74229 -6.79259,105.43888 -6.91543,106.36594 -7.36105,106.44939 -7.82798,108.24671 -8.17075,110.20438 -8.25928,110.32961 -8.33848,110.50993 -8.39321,110.68749 -8.41571,110.89979 -8.56547,111.65837 -8.43308,112.77728 -8.30029,113.20902 -8.47183,113.59681 -8.64093,114.43831 -7.76584,114.39729 -6.96787,114.10212'),(3,'Kalimantan','-0.82880','113.79958','-2.18197,110.10867 0.74534,109.08557 1.57394,114.39643 2.41409,115.00532 4.1308,116.75678 3.52833,117.53509 1.26814,118.12492 -1.33827,116.5516 -3.32792,116.20875 -4.05725,114.90781'),(4,'Sulawesi','-1.847900','120.527900','1.76509,125.26672 1.07969,124.95206 0.46228,124.48163 0.2799,123.41145 0.44578,122.89122 0.46885,122.0414 0.41501,121.08721 0.49246,120.48377 0.25654,120.16557 -0.64145,120.0614 -0.9322,120.53316 -1.29981,120.75222 -1.40379,121.07566 -1.07135,121.20991 -0.85971,121.50895 -0.94448,121.89007 -0.80956,122.1723 -0.7735,122.43257 -0.76096,122.63411 -0.60638,122.89968 -0.5287,123.22017 -0.69942,123.52127 -1.03869,123.39604 -1.05454,123.23455 -0.92515,123.07141 -0.96481,122.84702 -1.20982,122.65792 -1.50805,122.37385 -1.60361,122.21122 -1.62256,122.08046 -1.69518,121.91072 -1.75894,121.81074 -1.88309,121.77667 -3.93352,122.75557 -5.58542,122.60287 -5.50974,121.98557 -3.47441,120.83886 -2.68804,120.78666 -3.04615,120.24279 -3.61797,120.34162 -5.4593,120.2427 -5.67304,119.36912 -2.7811,118.82478 -1.81535,119.20329 0.94161,119.81801 1.61978,121.2571 0.84811,122.9159'),(5,'Bali','','',NULL),(6,'Irian','','',NULL);

/*Table structure for table `sdm` */

DROP TABLE IF EXISTS `sdm`;

CREATE TABLE `sdm` (
  `id_sdm` int(11) NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(11) NOT NULL,
  `id_profesi` int(25) NOT NULL,
  `keterangan` text NOT NULL,
  `total` int(11) DEFAULT NULL,
  `target` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sdm`),
  KEY `kecamatan` (`id_provinsi`),
  KEY `status` (`id_profesi`),
  CONSTRAINT `sdm_ibfk_1` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id_provinsi`),
  CONSTRAINT `sdm_ibfk_2` FOREIGN KEY (`id_profesi`) REFERENCES `profesi` (`id_profesi`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `sdm` */

insert  into `sdm`(`id_sdm`,`id_provinsi`,`id_profesi`,`keterangan`,`total`,`target`) values (1,2,1,'Ket 1',10000,1000),(2,2,3,'Ket 2',20,1000),(3,3,3,'Ket 3',734,500),(4,4,4,'Ket 3',800,500),(5,2,2,'ket 5',50,500),(6,4,1,'',300,500),(7,4,2,'',100,500),(8,4,3,'',50,500),(9,3,1,'',343,500),(10,3,2,'',234,500),(11,3,4,'',455,500),(13,2,4,'',678,500);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
