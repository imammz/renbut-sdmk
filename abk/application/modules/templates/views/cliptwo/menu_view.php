<!-- sidebar -->
<div class="sidebar app-aside" id="sidebar">
    <div class="sidebar-container perfect-scrollbar">
        <nav>
            <!-- start: SEARCH FORM -->
            <div class="search-form">
                <a class="s-open" href="<?php echo base_url() ?>#">
                    <i class="ti-search"></i>
                </a>
                <form class="navbar-form" role="search">
                    <a class="s-remove" href="<?php echo base_url() ?>#" target=".navbar-form">
                        <i class="ti-close"></i>
                    </a>

                </form>
            </div>
            <!-- end: SEARCH FORM -->
            <!-- start: MAIN NAVIGATION MENU -->
            <div class="navbar-title">
                <span>Menu Utama</span>
            </div>
            <ul class="main-navigation-menu">
                <!--							<li class="active open">-->
                <li <?php echo ($class == 'home') ? 'class="active open"' : ""; ?>>
                    <a href="<?php echo base_url() ?>index.php/home/">
                        <div class="item-content">
                            <div class="item-media">
                                <span class="fa-stack"> <i class="fa fa-square fa-stack-2x"></i> <i class="fa fa-home fa-stack-1x fa-inverse"></i> </span>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Home </span>
                            </div>
                        </div>
                    </a>
                </li>
                <li <?php echo ($class == 'entry') ? 'class="active open"' : ""; ?>>
                    <a href="<?php echo base_url() ?>index.php/entry/">
                        <div class="item-content">
                            <div class="item-media">
                                <span class="fa-stack"> <i class="fa fa-square fa-stack-2x"></i> <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i> </span>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Entry ABK </span>
                            </div>
                        </div>
                    </a>
                </li>
                <li <?php echo ($class == 'hasilentry') ? 'class="active open"' : ""; ?>>
                    <a href="<?php echo base_url() ?>index.php/entry/hasilentry/">
                        <div class="item-content">
                            <div class="item-media">
                                <span class="fa-stack"> <i class="fa fa-square fa-stack-2x"></i> <i class="fa fa-file-archive-o fa-stack-1x fa-inverse"></i> </span>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Hasil Entry ABK </span>
                            </div>
                        </div>
                    </a>
                </li>
                <li <?php echo ($class == 'laporan') ? 'class="active open"' : ""; ?>>
                    <a href="<?php echo base_url() ?>index.php/laporan/">
                        <div class="item-content">
                            <div class="item-media">
                                <span class="fa-stack"> <i class="fa fa-square fa-stack-2x"></i> <i class="fa fa-book fa-stack-1x fa-inverse"></i> </span>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Laporan Perencanaan SDMK </span>
                            </div>
                        </div>
                    </a>
                </li>

            </ul>
            <!-- end: MAIN NAVIGATION MENU -->

            <!--						 start: KONFIGURASI-->
            <div class="navbar-title">
                <span>Konfigurasi & Refrensi</span>
            </div>
            <ul class="folders">
                <li <?php echo ($class == 'refrensiabk') ? 'class="active open"' : ""; ?>>
                    <a href="<?php echo base_url() ?>index.php/refrensi/refrensiabk">
                        <div class="item-content">
                            <div class="item-media">
                                <i class="ti-clipboard"></i>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Refrensi ABK </span>
                            </div>
                        </div>
                    </a>
                </li>
                <li <?php echo ($class == 'dasarhukum') ? 'class="active open"' : ""; ?>>
                    <a href="<?php echo base_url() ?>index.php/refrensi/dasarhukum">
                        <div class="item-content">
                            <div class="item-media">
                                <i class="ti-agenda "></i>
                            </div>
                            <div class="item-inner">
                                <span class="title"> Dasar Hukum </span>
                            </div>
                        </div>
                    </a>
                </li>
                <?php if ($_SESSION['user']['NO_PROV'] == 'A') { ?>  
                    <li <?php echo ($class == 'masterdata') ? 'class="active open"' : ""; ?>>
                        <a href="#">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="ti-menu-alt"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> Master Data </span>
                                </div>
                            </div>
                        </a>
                        <ul class="sub-menu">
                            <li <?php if (isset($subclass)) echo ($subclass == 'kota') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/kota/Kota-Kabupaten">
                                    <span class="title">Kota/Kab</span>
                                </a>
                            </li>			
                            <li <?php if (isset($subclass)) echo ($subclass == 'provinsi') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/provinsi/Provinsi">
                                    <span class="title">Provinsi</span>
                                </a>
                            </li>			
                            <li <?php if (isset($subclass)) echo ($subclass == 'satuan') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/satuan/Satuan">
                                    <span class="title">Satuan Tugas</span>
                                </a>
                            </li>			
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/all/all">
                                    <span class="title">Jenis SDMK</span>
                                </a>
                            </li>

                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/TahunEntry/TahunEntry">
                                    <span class="title">Periode Tahun Entry</span>
                                </a>
                            </li>

                        </ul>    




                    </li>
                    <li <?php echo ($class == 'masterdata') ? 'class="active open"' : ""; ?>>
                        <a href="#">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="ti-menu-alt"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> Master Faskes </span>
                                </div>
                            </div>
                        </a>
                        <ul class="sub-menu">
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/Puskesmas/Puskesmas">
                                    <span class="title">Puskesmas</span>
                                </a>
                            </li>

                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/Rumahsakit/Rumahsakit">
                                    <span class="title">Rumah Sakit</span>
                                </a>
                            </li>
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/Bapelkes/Bapelkes">
                                    <span class="title">Bapelkes</span>
                                </a>
                            </li>

                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/kkp/kkp">
                                    <span class="title">KKP</span>
                                </a>
                            </li>
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/btklp/btklp">
                                    <span class="title">BTKLP</span>
                                </a>
                            </li>
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/klinik/klinik">
                                    <span class="title">Klinik</span>
                                </a>
                            </li>
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/bpkm/bpkm">
                                    <span class="title">BPKM</span>
                                </a>
                            </li>
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/labkes/labkes">
                                    <span class="title">Labkes</span>
                                </a>
                            </li>
                            <li <?php if (isset($subclass)) echo ($subclass == 'jenisSDMK') ? 'class="active"' : ""; ?>>
                                <a href="<?php echo base_url() ?>index.php/config/masterdata/index/Faskeslain/Faskeslain">
                                    <span class="title">Faskes Lain</span>
                                </a>
                            </li>
                        </ul>

                    </li>

                <?php } ?>

<!--                                                        <li <?php echo ($class == 'usermanagement') ? 'class="active open"' : ""; ?>>
        <a href="<?php echo base_url() ?>index.php/config/usermanagement">
                <div class="item-content">
                        <div class="item-media">
                                <i class="ti-user"></i>
                        </div>
                        <div class="item-inner">
                                <span class="title"> Management Pengguna </span>
                        </div>
                </div>
        </a>
</li>-->
                    <li <?php echo ($class == 'export') ? 'class="active open"' : ""; ?>>
                        <a href="<?php echo base_url() ?>index.php/config/export">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="ti-shift-right"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> Export Data Hasil ABK </span>
                                </div>
                            </div>
                        </a>
                    </li>
               

                    <li <?php echo ($class == 'import') ? 'class="active open"' : ""; ?>>
                        <a href="<?php echo base_url() ?>index.php/config/import">
                            <div class="item-content">
                                <div class="item-media">
                                    <i class="ti-shift-left"></i>
                                </div>
                                <div class="item-inner">
                                    <span class="title"> Import Data Hasil ABK </span>
                                </div>
                            </div>
                        </a>
                    </li>
  			
            </ul>
            <!--
            end: KONFIGURASI FEATURES -->
            <!-- start: LOGOUT BUTTON -->
            <div class="wrapper">
                <a href="<?php echo base_url() ?>index.php/config/login/logout" class="button-o">
                    <i class="ti-power-off"></i>
                    <span>LOGOUT</span>
                </a>
            </div>
            <!-- end: DOCUMENTATION BUTTON -->
        </nav>
    </div>
</div>
<!-- / sidebar -->