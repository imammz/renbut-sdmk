<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_rs" action="<?php echo site_url('master_data/m_rs/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_rs:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kode RS</label>
                  <div class="col-md-2">
                    <input name="kode_rs" id="kode_rs" value="<?php echo isset($value)?$value->kode_rs:''?>" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Rumah Sakit</label>
                  <div class="col-md-6">
                    <input name="nama_rs" id="nama_rs" value="<?php echo isset($value)?$value->nama_rs:''?>" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Alamat</label>
                  <div class="col-md-8">
                    <input name="alamat" id="alamat" value="<?php echo isset($value)?$value->alamat:''?>" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kelas RS</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('A','B','C','D');
                          foreach($katergori_kelas as $row_katergori_kelas){
                        ?>
                          <label>
                            <input name="kelas_rs" type="radio" class="ace" value="<?php echo $row_katergori_kelas?>" <?php echo isset($value) ? ($value->kelas_rs == $row_katergori_kelas) ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo $row_katergori_kelas?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Jumlah Tempat Tidur</label>
                  <div class="col-md-2">
                    <input name="jumlah_tt" id="jumlah_tt" value="<?php echo isset($value)?$value->jumlah_tt:''?>" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Jenis Rumah Sakit</label>
                  <div class="col-md-3">
                    <?php 
                      echo Master::get_master_jenis_faskes(isset($value) ? $value->jenis_rs : '' ,'id_jenis_rsk','id_jenis_rsk','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      echo Master::get_master_provinsi(isset($value) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      echo Master::get_change_master_kabupaten(isset($value) ? $value->id_kabupaten : $this->session->userdata('data_user')->id_kabupaten ,'id_kabupaten','kab-box','form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_jenis_rsk:0?>"> -->

                  <a onclick="getMenu('master_data/m_rs')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">
   
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#puskes-box'));
                    });

                });
            } else {
                $('#puskes-box option').remove()
                $('<option value="">(Pilih Unit Kerja)</option>').appendTo($('#puskes-box'));
            }
        });
    });
</script>
<script src="<?php echo base_url().'assets/js/custom/rumah_sakit.js'?>"></script>
