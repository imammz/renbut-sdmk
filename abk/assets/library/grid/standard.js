/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function addRow(tableID) {

    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var colCount = table.rows[1].cells.length;

    for (var i = 0; i < colCount; i++) {

        var newcell = row.insertCell(i);

        newcell.innerHTML = table.rows[1].cells[i].innerHTML;
        //alert(newcell.childNodes[0].type);
        

                
        switch (newcell.childNodes[0].type) {
            case "text":
                newcell.childNodes[0].value = "";
                break;
            case "checkbox":
                newcell.childNodes[0].checked = false;
                break;
            case "button":
                newcell.childNodes[0].setAttribute('id', '' + (rowCount));
                break;    
            case "select-one":
                newcell.childNodes[0].selectedIndex = 0;
                break;
        }
        switch (newcell.childNodes[0].name) {
            case "urut_unit_installasi[]":
                newcell.childNodes[0].value = (rowCount);
                newcell.childNodes[0].setAttribute('id', 'urut_unit_installasi_' + (rowCount - 1));
                //alert(rowCount);
                break;
             case "urut_tugas_pokok[]":
                newcell.childNodes[0].value = (rowCount);
                newcell.childNodes[0].setAttribute('id', 'urut_tugas_pokok_' + (rowCount - 1));
                //alert(rowCount);
                break;   
             case "urut_tugas_penunjang[]":
                newcell.childNodes[0].value = (rowCount);
                newcell.childNodes[0].setAttribute('id', 'urut_tugas_penunjang_' + (rowCount - 1));
                //alert(rowCount);
                break;   

        }

        switch (newcell.childNodes[0].id) {
            case "urut":
                newcell.childNodes[0].value = (rowCount);
                //alert(rowCount);
                break;

        }
    }
}

function deleteRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        for (var i = 0; i < rowCount; i++) {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            if (null != chkbox && true == chkbox.checked) {
                if (rowCount <= 2) {
                    alert("Tidak bisa menghapus semua baris\nCukup kosongkan field agar nilai\ntidak tersimpan");
                    break;
                }
                table.deleteRow(i);
                rowCount--;
                i--;
            }

        }
    } catch (e) {
        alert(e);
    }


}


function delRow(tableID,rowID) {
    
    
    try {
        var table = document.getElementById(tableID);
        
        
        if (rowID==1) {
            alert("Tidak bisa Menghapus Baris Pertama");
        }
        else {
            table.deleteRow(rowID);
        }

    } catch (e) {
        alert(e);
    }
}