<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Page Not Found</title>
        <style type="text/css">

            ::selection{ background-color: #E13300; color: white; }
            ::moz-selection{ background-color: #E13300; color: white; }
            ::webkit-selection{ background-color: #E13300; color: white; }

            body {
                background-color: #fff; 
                margin: 40px auto;
                font: 13px/20px normal Helvetica, Arial, sans-serif;
                color: #4F5155;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                color: #f0f0f0;
                background-color: transparent; 
                font-size: 19px;
                float: left;
                font-weight: normal;
                text-transform: capitalize;
                margin: 0 0 14px 0px;
                padding: 14px 15px 0px 10px;
            }

            code {
                font-family: Consolas, Monaco, Courier New, Courier, monospace;
                font-size: 12px;
                background-color: #f9f9f9;
                border: 1px solid #D0D0D0;
                color: #002166;
                display: block;
                margin: 14px 0 14px 0;
                padding: 12px 10px 12px 10px;
            }

            #container {
                margin: 10px;
                border:1px solid #ddd;
            }

            p {
                margin: 12px 15px 12px 15px;
            }
            .x-top{
                float: left;
                width: 100%;
                border-bottom: 1px solid #ccc;
                -webkit-box-shadow: rgba(0,0,0,0.3) 0px 2px 2px -1px;
                -moz-box-shadow: rgba(0,0,0,0.3) 0px 2px 2px -1px;
                -o-box-shadow: rgba(0,0,0,0.3) 0px 2px 2px -1px;
                box-shadow: rgba(0,0,0,0.3) 0px 2px 2px -1px;
                border-bottom: 1px solid #2693ba;
                background: #50b7dc url(<?php echo site_url('application/templates/private/assets/img/xamzo-bg-texture.png') ?>) repeat;
                margin-bottom:20px;
            }
            span.icon-xamzo{
                width: 45px;
                height: 45px;
                cursor: pointer;
                background: url(<?php echo site_url('application/templates/private/assets/img/xamzo/xamzo40x39.png') ?>) no-repeat center;
                float: left;
                position: relative;
                margin-left: 5px;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <div class="x-top"><a href="http://xamzo.com"><span class="icon-xamzo"></span></a><h1><?php echo $this->X->lang["sorry,_can't_open_this_content"] ?>!</h1></div>
            <p><?php echo $this->X->lang["you_does_not_have_access_rights_to_the_content_so_server_is_rejecting_to_give_proper_response"]; ?>.</p>
            <p>
                <a href="javascript:history.back(-1)" title="<?php echo $this->X->lang['back_to_site'] ?>">&larr; <?php echo $this->X->lang['back_to_site'] ?></a>
                <span style="float: right"><a href="http://xamzo.com">Powered by xamzo</a></span>
            </p>
        </div> 
    </body>
</html>
