<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<script type="text/javascript" src="<?php echo site_url("application/modules/media/assets/javascript/script.js") ?>"></script>
<li id="<?php echo $id; ?>">
    <span class="icon_<?php echo $ex[1]; ?> xamzo-media tip-bottom-left" original-title="<?php echo $oldname; ?> " dir="<?php echo site_url("upload/d/$_name"); ?>"><?php echo $filename; ?> </span>  
    <div class="box-image-act">
        <div class="left">
            <input table-check-all='true' type="checkbox" onclick="_self_check(this);" name="id[]" value="<?php echo site_url("upload/d/$_name"); ?>" id="<?php echo $_name; ?>"><label for="<?php echo $_name; ?>"></label>
        </div>
        <div class="right">
            <a href="javascript:void(0)" onclick="file_url($(this).parent());" class="tip-top" original-title="<?php echo $this->lang["link"]; ?> "><i class="fa fa-link"></i></a>
            <a href="javascript:void(0)" onclick="file_property($(this).parent(), 'doc');" class="tip-top" original-title="<?php echo $this->lang["properties"]; ?>"><i class="fa fa-search"></i></a> 
            <a href="javascript:void(0)" onclick="file_delete($(this).parent());" class="tip-top" original-title="<?php echo $this->lang["delete"]; ?>"><i class="fa fa-trash"></i></a> 
        </div>
    </div>
</li>