<script type="text/javascript">
    
    /*$('#form-provinsi').hide();
    $('#form-kabupaten').hide();
    $('#form-puskesmas').hide();*/

    $('select[name=role]').change(function () {
        if ($(this).val() == 1) { // Administrator
            $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        } else if ($(this).val() == 2) { // Pusat
            $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        } else if ($(this).val() == 3 || $(this).val() == 11) { // Staff Provinsi
            $('#form-provinsi').show();
              $('#form-kabupaten').hide();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        }
        else if ($(this).val() == 4) { // Staff Kabupaten
            $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        }else if ($(this).val() == 5) { // Staff puskesmas
            $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-puskesmas').show();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        }else if ($(this).val() == 8) { // Staff KKP
            $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').show();
              $('#form-klinik').hide();
        }else if ($(this).val() == 9) { // Staff BTKL
            $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').show();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        }else if ($(this).val() == 10) { // Staff KLINIK
            $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').show();
        }else if ($(this).val() == 6 || $(this).val() == 7) { // Staf RSU RSK
            $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-puskesmas').hide();
              $('#form-rsu').show();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        } else {
            $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        }
    });

    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            // puskesmas //
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#puskes-box'));
                    });

                });
            } else {
                $('#puskes-box option').remove()
                $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
            }

            // rumah sakit umum //
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#puskes-box'));
            }

        });
    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_user" action="<?php echo site_url('perencanaan/ajax_add')?>">
                <br>

                <!-- <div>
                    <label class="mandatory">Provinsi</label>
                    <?php echo form_dropdown('provinsi_id', $provinsi) ?>
                </div>
                <div>
                    <label class="mandatory">Kabupaten</label>
                    <?php echo form_dropdown('kabupaten_id', $kabupaten, array(), 'id="kab-box"') ?>
                </div>
                <div>
                    <label class="mandatory">Puskesmas</label>
                    <?php echo form_dropdown('ditjen_id', $puskesmas, array(), 'id="puskes-box"') ?>
                </div> -->

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_user:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Pengguna</label>
                  <div class="col-md-6">
                    <input name="fullname" id="fullname" value="<?php echo isset($value)?$value->fullname:''?>" placeholder="Nama Pengguna" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Username/Email</label>
                  <div class="col-md-4">
                    <input name="email" id="email" placeholder="Email" value="<?php echo isset($value)?$value->email:''?>" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Password</label>
                  <div class="col-md-3">
                    <input name="password" id="password" placeholder="Password" value="<?php echo isset($value)?Encryption::decrypt_password_callback($value->password, SECURITY_KEY):''?>" class="form-control" type="password">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Konfirmasi password</label>
                  <div class="col-md-3">
                    <input name="confirm_password" id="confirm_password" placeholder="Konfirmasi password" class="form-control" type="password">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Role</label>
                  <div class="col-md-3">
                    <?php echo Master::get_master_role(isset($value)?$value->id_role:'','role','role','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group" id="form-provinsi" <?php echo isset($value->id_provinsi) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php echo Master::get_master_provinsi(isset($value)?$value->id_provinsi:'','id_provinsi','id_provinsi','form-control','required','inline');?>

                    <?php //echo form_dropdown('provinsi_id', $provinsi) ?>
                  </div>
                </div>

                <div class="form-group" id="form-kabupaten" <?php echo isset($value->id_kabupaten) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php echo Master::get_change_master_kabupaten(isset($value)?$value->id_kabupaten:'','id_kabupaten','kab-box','form-control','','inline');?>

                    <?php //echo form_dropdown('kabupaten_id', $kabupaten, array(), 'id="kab-box" class="orm-control"') ?>
                  </div>
                </div>

                <div class="form-group" id="form-puskesmas" <?php echo isset($value->kode_puskesmas) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Puskesmas</label>
                  <div class="col-md-3">
                    <?php echo Master::get_change_master_puskesmas(isset($value)?$value->kode_puskesmas:'','kode_puskesmas','puskes-box','form-control','required','inline');?>

                    <?php //echo form_dropdown('puskesmas_id', $puskesmas, array(), 'id="puskes-box"') ?>
                  </div>
                </div>

                <div class="form-group" id="form-rsu" <?php echo isset($value->kode_rs) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Rumah Sakit</label>
                  <div class="col-md-3">
                    <?php echo Master::get_change_master_rsu(isset($value)?$value->kode_rs:'','kode_rs','rsu-box','form-control','required','inline');?>

                    <?php //echo form_dropdown('puskesmas_id', $puskesmas, array(), 'id="puskes-box"') ?>
                  </div>
                </div>

                <div class="form-group" id="form-btkl" <?php echo isset($value->kode_btkl) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">BTKL</label>
                  <div class="col-md-3">
                    <?php echo Master::get_change_master_btkl(isset($value)?$value->kode_btkl:'','kode_btkl','btkl-box','form-control','required','inline');?>

                    <?php //echo form_dropdown('puskesmas_id', $puskesmas, array(), 'id="puskes-box"') ?>
                  </div>
                </div>

                <div class="form-group" id="form-kkp" <?php echo isset($value->kode_kkp) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">KKP</label>
                  <div class="col-md-3">
                    <?php echo Master::get_change_master_kkp(isset($value)?$value->kode_kkp:'','kode_kkp','kkp-box','form-control','required','inline');?>

                    <?php //echo form_dropdown('puskesmas_id', $puskesmas, array(), 'id="puskes-box"') ?>
                  </div>
                </div>

                <div class="form-group" id="form-klinik" <?php echo isset($value->kode_klinik) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Klinik</label>
                  <div class="col-md-3">
                    <?php echo Master::get_change_master_klinik(isset($value)?$value->kode_klinik:'','kode_klinik','klinik-box','form-control','required','inline');?>

                    <?php //echo form_dropdown('puskesmas_id', $puskesmas, array(), 'id="puskes-box"') ?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_user:0?>"> -->

                  <a onclick="getMenu('pengaturan/user')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/user.js'?>"></script>
