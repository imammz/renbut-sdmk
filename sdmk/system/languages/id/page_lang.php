<?php if (!defined('BASEPATH')) exit('no direct script user allowed');
$lang['set_as_publish'] = 'atur sebagai publish';
$lang['set_as_draft'] = 'atur sebagai draft';
$lang['page_type'] = 'tipe halaman';
$lang['this_page_to_manage_page_content,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola konten halaman, lihat dokumentasi untuk menggunakannya';