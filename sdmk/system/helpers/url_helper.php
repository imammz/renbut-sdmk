<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/url
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('redirect')) {

    function redirect($modul = null) {
        if (!preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $modul)) {
            header('Location: ' . site_url($modul));
        } else {
            header('Location: ' . $modul);
        }

        exit();
    }

}
if (!function_exists('x_back')) {

    function x_back() {
        ?>
        <script type="text/javascript">
            history.back(-1);
        </script>
        <?php

    }

}
if (!function_exists('site_url')) {

    /**
     * get base URL
     * @param type $method
     * @return type 
     */
    function site_url($method = null) {
        $base_url = X_SITE_URL;
        if ($method != null) {
            $base_url.=$method;
        }
        return $base_url;
    }

}

if (!function_exists('site_domain')) {

    /**
     * get base URL
     * @param type $method
     * @return type 
     */
    function site_domain($method = null) {
        $site_domain = X_SITE_DOMAIN;
        if ($method != null) {
            $site_domain.=$method;
        }
        return $site_domain;
    }

}

if (!function_exists('is_ajax_request')) {

    function is_ajax_request() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        } else {
            return false;
        }
    }

}


/**
 * 
 */
if (!function_exists('url_clean')) {

    function url_clean($pemalink) {
        $link = str_replace("'", "", $pemalink);
        $new_data = preg_replace('/[^\p{L}\p{N}]/u', '-', $link);
        return strtolower($new_data);
    }

}
if (!function_exists('is_url')) {

    function is_url($uri) {
        if (preg_match('/^(http|https):\/\/[a-z0-9_]+([\-\.]{1}[a-z_0-9]+)*\.[_a-z]{2,5}' . '((:[0-9]{1,5})?\/.*)?$/i', $uri)) {
            return $uri;
        } else {
            return false;
        }
    }

}

if (!function_exists('subdomain')) {

    function subdomain() {
        $s = explode(".", $_SERVER['HTTP_HOST']);
        if (count($s) > 0) {
            if($s['0']=='www'){
               return $s['1'];
            }else{
               return $s['0'];            
            }
        }
        return '';
    }

}
/**
 * End of file ./system/helpers/url_helper.php
 */