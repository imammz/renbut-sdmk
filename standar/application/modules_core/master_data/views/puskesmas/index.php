<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        <div class="row">
          <div class="space-6"></div>

          <div class="col-sm-12 infobox-container">

            <!-- #section:pages/dashboard.infobox -->
            <div class="infobox infobox-green">
              <div class="infobox-icon">
                <i class="ace-icon fa fa-cloud "></i>
              </div>
              <div class="infobox-data">
                <span class="infobox-data-number"><?php echo $total_rows_server?></span>
                <div class="infobox-content"><small>Total puskesmas online</small></div>
              </div>
            </div>

            <div class="infobox infobox-blue">
              <div class="infobox-icon">
                <i class="ace-icon fa fa-building"></i>
              </div>
              <div class="infobox-data">
                <span class="infobox-data-number"><?php echo $total_rows_existing?></span>
                <div class="infobox-content"><small>Total puskesmas existing</small></div>
              </div>
            </div>

            <div class="infobox infobox-red">
              <div class="infobox-icon">
                <i class="ace-icon fa fa-lightbulb-o"></i>
              </div>
              <div class="infobox-data">
                <span class="infobox-data-number"><?php $selisih = $total_rows_server - $total_rows_existing; echo $selisih; ?></span>
                <div class="infobox-content"><small>Selisih server - existing</small></div>
              </div>
            </div>

            <!-- /section:pages/dashboard.infobox -->
            <div class="space-6"></div>

          </div>

        </div><!-- /.row -->

        <div class="clearfix">
          <button class="btn btn-sm btn-primary" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Puskesmas </button>
          <button class="btn btn-sm btn-warning" onclick="sinkronisasi()"><i class="fa fa-refresh"></i> Sinkronisasi </button>

          <div class="pull-right tableTools-container"></div>
        </div>

        <div>

          <table id="dynamic-table" class="table table-striped table-bordered table-hover">
             <thead>
              <tr>  
                <th class="center"></th>
                <th>Kode Puskesmas</th>
                <th>Nama Puskesmas</th>
                <th>Tipe Puskesmas</th>
                <th>Kabubaten</th>
                <th>Status</th>
                <th>Update terakhir</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/puskesmas.js'?>"></script>
