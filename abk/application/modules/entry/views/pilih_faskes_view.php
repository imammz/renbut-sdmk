<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >
                <div class="wrap-content container" id="container">
                    <!-- start: PAGE TITLE -->
                    <section id="page-title">



                        <div class="row">
                            <div class="col-sm-8">
                                <h1 class="mainTitle">Entry ABK Periode Tahun <?php echo date('Y'); ?></h1>
                                <span class="mainDescription">Pilihlah Jenis Faskes</span>
                            </div>
                            <ol class="breadcrumb">
                                <li>
                                    <span>Entry</span>
                                </li>
                                <li class="active">
                                    <span>Pilih Jenis Faskes</span>
                                </li>
                            </ol>
                        </div>
                    </section>
                    <!-- end: PAGE TITLE -->
                    <!-- start: YOUR CONTENT HERE -->
                    <div class="container-fluid container-fullw bg-white">
                        <div class="row">
                            <div class="col-md-12"> 
                                <?php $account_model = new account_model(); ?>

                                <div class="row">
                                    <?php if ($account_model->_getKodeLevelUser($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']) == 42) { ?>     
                                        <div class="col-sm-3">
                                            <div class="panel panel-white">                                            
                                                <div class="panel-body" style="text-align: center">
                                                    <a href="<?php echo base_url() ?>index.php/entry/rumahsakit">
                                                        <h3>Rumah Sakit</h3>
                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/rumah sakit.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>  
                                    <?php if ($account_model->_getKodeLevelUser($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']) == 41) { ?>   
                                        <div class="col-sm-3">
                                            <div class="panel panel-white">                                            
                                                <div class="panel-body" style="text-align: center">
                                                    <a href="<?php echo base_url() ?>index.php/entry/puskesmas">
                                                        <h3>Puskesmas</h3>
                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/puskesmas.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($account_model->_getKodeLevelUser($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']) == 3) { ?>  
                                        <div class="col-sm-3">
                                            <div class="panel panel-white">                                            
                                                <div class="panel-body" style="text-align: center">
                                                    <a href="<?php echo base_url() ?>index.php/entry/dinaskesehatan">
                                                        <h3>Dinas Kesehatan Kab/Kota</h3>
                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/kantor dinas kesehatan.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>
                                    <?php if ($account_model->_getKodeLevelUser($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']) == 2) { ?>  
                                        <div class="col-sm-3">
                                            <div class="panel panel-white">                                            
                                                <div class="panel-body" style="text-align: center">
                                                    <a href="<?php echo base_url() ?>index.php/entry/dinaskesehatanprov">
                                                        <h3>Dinas Kesehatan Provinsi</h3>
                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/kantor dinas kesehatan.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>


                                    <?php if ($account_model->_getKodeLevelUser($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']) == 1) { ?>     
                                        <div class="col-sm-3">
                                            <div class="panel panel-white">                                            
                                                <div class="panel-body" style="text-align: center">
                                                    <a href="<?php echo base_url() ?>index.php/entry/pusat">
                                                        <h3>Kementerian Kesehatan Republik Indonesia</h3>
                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 4) { ?>   
                                        <!--KKP-->                                  
                                        <div class="col-sm-3">
                                            <div class="panel panel-white">                                            
                                                <div class="panel-body" style="text-align: center">
                                                    <a href="<?php echo base_url() ?>index.php/entry/kkp">
                                                        <h3>KKP</h3>
                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/kkp.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                        
                                    <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 5) { ?>   
                                        <!--Balai Pelatihan Kesehatan-->                                  
                                          <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/balaikesehatan">
                                                                                        <h3>Balai Pelatihan Kesehatan</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/balai kesehatan.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>
                                         <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 8) { ?>   
                                        <!--BPKM-->                                  
                                          <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/bpkm">
                                                                                        <h3>Balai Pengobatan / Kesehatan Masyarakat</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/balai kesehatan.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>
                                         <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 9) { ?>   
                                        <!--KLinik-->                                  
                                          <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/klinik">
                                                                                        <h3>Klinik</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/balai kesehatan.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>
                                         <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 10) { ?>   
                                        <!--Labkes-->                                  
                                          <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/labkes">
                                                                                        <h3>Laboratorium Kesehatan</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/balai kesehatan.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>
                                     <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 3) { ?>   
                                        <!--BTKL-->                                  
                                          <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/btkl">
                                                                                        <h3>Balai Teknis Kesehatan Lingkungan</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/balai kesehatan.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>   
                                        
                                        
                                         <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 99) { ?>   
                                        <!--Faskes Lainnya-->                                  
                                           <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/lainnya">
                                                                                        <h3>Faskes Lainnya</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/lainnya.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>
                                        
                                        
                                         <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 98) { ?>   
                                        <!--Faskes Lainnya-->                                  
                                           <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/upt">
                                                                                        <h3>UPT Pusat</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/lainnya.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>
                                        
                                         <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 97) { ?>   
                                        <!--Faskes Lainnya-->                                  
                                           <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/uptdprov">
                                                                                        <h3>UPT Provinsi</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/lainnya.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>
                                        
                                          <?php if ($_SESSION['user']['ref_jenis_faskes_kode'] == 96) { ?>   
                                        <!--Faskes Lainnya-->                                  
                                           <div class="col-sm-3">
                                                                            <div class="panel panel-white">                                            
                                                                                <div class="panel-body" style="text-align: center">
                                                                                    <a href="<?php echo base_url() ?>index.php/entry/uptdkab">
                                                                                        <h3>UPT Kabupaten Kota</h3>
                                                                                        <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/lainnya.png"/>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                    <?php } ?>





                                                <!--
                                                                        
                                                                        
                                                                                                            -->


                                    <!-- Faskes Lainnya                                  
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    <!-- end: YOUR CONTENT HERE -->

                                </div>
                            </div>
                            <?php echo Modules::run('templates/cliptwo/footer'); ?>
                        </div>
                        <?php echo Modules::run('templates/cliptwo/js'); ?>
                        <!-- start: JavaScript Event Handlers for this page -->
                        <script>
                            jQuery(document).ready(function () {
                                Main.init();
                            });
                        </script>
                        <!-- end: JavaScript Event Handlers for this page -->
                        <!-- end: CLIP-TWO JAVASCRIPTS -->
                        </body>
                        </html>
