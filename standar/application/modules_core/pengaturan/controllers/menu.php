<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pengaturan/menu_model','menu');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Menu";
		$data['subtitle'] = "Daftar Menu";
		$this->load->view('menu/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Menu";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->menu->get_by_id($id);
		}
		$this->load->view('menu/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->menu->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $menu) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = $menu->id_menu;
			$row[] = strtoupper($menu->name);
			$row[] = $menu->class;
			$row[] = $menu->link;
			$row[] = $menu->icon;
			$row[] = $menu->parent_menu;
			$row[] = $menu->counter;
			$row[] = ($menu->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $menu->updated_date?Tanggal::formatDateTime($menu->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($menu->id_menu,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_menu('."'".Regex::_genRegex($menu->id_menu,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->menu->count_all(),
						"recordsFiltered" => $this->menu->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		// Authentication //
		
		$id_menu = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'name' => Regex::_genRegex($this->input->post('name'), 'RGXQSL'),
			'class' => Regex::_genRegex($this->input->post('class'), 'RGXQSL'),
			'link' => Regex::_genRegex($this->input->post('link'), 'RGXQSL'),
			'icon' => Regex::_genRegex($this->input->post('icon'), 'RGXQSL'),
			'parent_menu' => Regex::_genRegex($this->input->post('parent_menu'), 'RGXINT'),
			'counter' => Regex::_genRegex($this->input->post('counter'), 'RGXINT'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => '',
			'updated_date' => date('Y-m-d H:i:s')
		);
		//$this->menu->save($dataexc);
		
		if( $id_menu == 0 ){
			$this->menu->save($dataexc);
		}else{
			$this->menu->update(array('id_menu'=>$id_menu), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->menu->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_reset($id)
	{
		$this->menu->reset_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	
}
