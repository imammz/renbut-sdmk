<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/extension
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
/**
 * Get extensions for each module
 * @param type $module
 * @param type $ex_position
 * @param type $ex_block
 * @return boolean
 */
if (!function_exists('x_read_ext')) {

    function x_read_ext($module = null, $ex_position = null, $in_array = array()) {
        if ($module !== null) {
            $data = array();
            if ($dir_handle = opendir(EXTPATH)) {
                while ($dir = readdir($dir_handle)) {
                    if (is_dir(EXTPATH . $dir)) {
                        $jsfile = EXTPATH . $dir . '/configs/extend' . JSON;
                        if (is_file($jsfile)) {
                            $json = x_file_get_content($jsfile);
                            $js = x_read_json_decode($json);
                            if (isset($js['0']['ext'])) {
                                $exts = explode(',', $js['0']['ext']);
                                if (count($exts) > 0) {
                                    for ($i = 0; $i < count($exts); $i++) {
                                        $iext = $exts[$i];
                                        if ($iext == $module) {
                                            $data[] = $js;
                                        }
                                    }
                                } else {
                                    $iext = $js['0']['ext'];
                                    if ($iext == $module) {
                                        $data[] = $js;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            closedir($dir_handle);
            foreach ($data as $key => $val) {
                foreach ($val as $k => $v) {
                    $module = ($v['module']);
                    $active = ($v['active']);
                    $position = ($v['position']);
                    if ($active && $position == $ex_position) {
                        ?>
                        <div id="ca-ext-<?php echo $module ?>" ></div>
                        <textarea id="ext-<?php echo $module ?>" name="ext-<?php echo $module ?>" style="display:none;"><?php echo $in_array[$module] ?></textarea>

                        <script type="text/javascript">
                        <?php
                        if (isset($in_array[$module])) {
                            ?>
                                $.ajax({
                                    beforeSend: function() {
                                        loading();
                                    },
                                    type: 'post',
                                    url: '<?php echo site_url($module) ?>',
                                    data: $('form#crud-form').serialize(),
                                    success: function(rs) {
                                        close_loading_box(true);
                                        $('#ca-ext-<?php echo $module ?>').html(rs);
                                        $('textarea#ext-<?php echo $module ?>').remove();
                                        close_box();
                                    }
                                });
                            <?php
                        } else {
                            ?>
                                _load('<?php echo site_url($module) ?>', 'false', undefined, '#ca-ext-<?php echo $module ?>', "true");
                            <?php
                        }
                        ?>
                        </script>
                        <?php
                    }
                }
            }
        } else {
            return false;
        }
    }

}

/**
 * Read file cofig.JSON for each extensions
 * @param type $ext
 * @param type $keys
 * @return type
 */
if (!function_exists('x_ext_config')) {

    function x_ext_config($ext = null, $file = null) {
        if ($ext != null) {
            $vcon = array();
            if ($file != null) {
                $ff = EXTPATH . $ext . '/configs/' . $file . JSON;
            } else {
                $ff = EXTPATH . $ext . '/configs/config' . JSON;
            }
            if (is_file($ff)) {
                $json = x_file_get_content($ff);
                $arr = x_read_json_decode($json);
                foreach ($arr as $k => $v) {
                    foreach ($v as $keys => $vals) {
                        $vcon[$keys] = $v[$keys]['val'];
                    }
                }
                return $vcon;
            }
        }
    }

}

if (!function_exists('x_include_ext')) {

    function x_include_ext($module = null) {
        if ($module !== null) {
            $data = array();
            if ($dir_handle = opendir(EXTPATH)) {
                while ($dir = readdir($dir_handle)) {
                    if (is_dir(EXTPATH . $dir)) {
                        $jsfile = EXTPATH . $dir . '/configs/extend' . JSON;
                        if (is_file($jsfile)) {
                            $json = x_file_get_content($jsfile);
                            $js = x_read_json_decode($json);
                            if (isset($js['0']['ext'])) {
                                $ext = explode(',', $js['0']['ext']);
                                if (count($ext) > 0) {
                                    for ($i = 0; $i < count($ext); $i++) {
                                        $iext = $ext[$i];
                                        if ($iext == $module) {
                                            $data[] = $js;
                                        }
                                    }
                                } else {
                                    $iext = $js['0']['ext'];
                                    if ($iext == $module) {
                                        $data[] = $js;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            closedir($dir_handle);
        }
        return $data;
    }

}

if (!function_exists('x_page_ext')) {

    function x_page_ext($module = null, $data = null) {
        foreach (x_include_ext($module) as $key => $val) {
            foreach ($val as $k => $v) {
                $module = ($v['module']);
                $active = ($v['active']);
                $type = ($v['type']);
                $position = ($v['position']);
                if ($active && $position == 'frontend') {
                    if ($type == "include") {
                        include_once EXTPATH . $module . '/include/index' . EXT;
                    }
                }
            }
        } 
    }

}

/**
 * End of file ./system/helpers/extension_helper.php
 */