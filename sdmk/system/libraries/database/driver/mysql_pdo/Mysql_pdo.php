<?php if (!defined('BASEPATH')){exit('No direct script access allowed');}

/**
 * CodeAnalytic
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		libraries/driver_mysql
 * @author		CodeAnalytic Dev Team  
 * @copyright           Copyright (c) 2013, CodeAnalytic, Inc.
 * @license		http://codeanalytic.com/license/
 * @link		http://codeanalytic.com
 * @since		Version 1.0
 * @filesource
 */
class Mysql_pdo extends Db_active_rec {

    public $table = '';
    public $prefix = '';
    public $conn = '';
    public $pdo;
    public $status = false;                      // conection status
    public $settings = array();              // connection setting 

    public function __construct() {
        $db = x_db_config();           
        // load connection array setting
        $this->settings = $db['database'];  // definet to variable
        $this->prefix = $this->settings['prefix'];  // define table prefix
        if ($this->status == false) {
            $this->__connect();
        }// call connection
    }

    public function __connect() {
        $dsn = 'mysql:dbname=' . $this->settings["name"]. ';host=' . $this->settings["host"] . ';port=' . $this->settings["port"];
        try {
            # Read settings from INI file
            $this->pdo = new PDO($dsn, $this->settings["user"], $this->settings["pass"], array(
                PDO::ATTR_PERSISTENT => true
            ));

            # We can now log any exceptions on Fatal error. 
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            # Disable emulation of prepared statements, use REAL prepared statements instead.
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);  

            # Connection succeeded, set the boolean to true.
            $this->status = true;
            return $this->pdo;
        } catch (PDOException $e) {
            # Write into log
            $this->status = false;
            $heading = 'Database Error';
            $message = $e->getMessage();
            include(BASEPATH . 'libraries/error/views/error_db' . EXT);
            die();
        }
    }

    public function __execute_all() {
        // execute query query 
        
        try {
            $query = $this->pdo->query($this->query);
            $query->execute();
            // return value as array 
            $this->_unset();
            return $query->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            # Write into log
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }
    
    public function __execute_all_array() {
        // execute query query 
        
        try {
            $query = $this->pdo->query($this->query);
            $query->execute();
            // return value as array 
            $this->_unset();
            return $query->fetchAll(PDO::FETCH_BOTH);
        } catch (PDOException $e) {
            # Write into log
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function exec() {
        // execute query query 
        try {
            $query = $this->pdo->query(trim($this->query));
            $query->execute(PDO::FETCH_ASSOC);
            // return value as array
            $this->_unset();
        } catch (PDOException $e) {
            # Write into log
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function __execute_row() {
        // execute query query 
        try {
            $query = $this->pdo->query(trim($this->query));
            $query->execute();
            // return value as array
            $this->_unset();
            return $query->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            # Write into log
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function num_rows() {
        try {
            $q = $this->pdo->query($this->query);
            $this->_unset();
            return $q->rowCount();
        } catch (PDOException $e) {
            # Write into log
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function insert($data = array(), $tbl = null) {
        $this->query = $this->prepare_insert($data, $tbl);
        try {
            return $this->pdo->exec($this->query);
            $this->_unset();
        } catch (Exception $e) {
            # Write into log
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function delete($wheres = array(), $tbl = null) {
        $this->query = $this->prepare_delete($wheres, $tbl);
        try {
            $q = $this->pdo->query($this->query);
            $this->_unset();
            return $q->execute();
        } catch (PDOException $e) {
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function delete_in($key, $wheres = array(), $tbl = null) {
        $this->query = $this->prepare_delete_in($key, $wheres, $tbl);
        try {
            $q = $this->pdo->query($this->query);
            $this->_unset();
            return $q->execute();
        } catch (PDOException $e) {
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function update($update = array(), $wheres = array(), $tbl = null) {
        $this->query = $this->prepare_update($update, $wheres, $tbl);
        try {
            $q = $this->pdo->query($this->query);
            $this->_unset();
            return $q->execute();
        } catch (PDOException $e) {
            show_error($e->getMessage() . $this->__get_query(), 'error_db');
            die();
        }
    }

    public function __get_query() {
        return '<br/><b>Your Query</b><br/>' . $this->query;
    }

}

?>
