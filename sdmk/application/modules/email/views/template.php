<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-wrench"></span> &nbsp;<?php echo $this->lang['setting'] ?>
            <sup class="sup-menu">
                <i class="fa fa-envelope"></i> <?php echo $this->lang['email']; ?>
            </sup>
        </h3>
        <span class="xamzo-head-info"><?php echo $this->lang['this_page_to_manage_website_setting,_see_documentation_for_use_it'] ?>.</span>
    </div>
    <div class="right">
    </div>
</div>
<div id="xamzo-head-action">
    <button class="btn primary large left fa fa-chevron-left tip-top" href="#!email" crud-name="list" ajax="true" url-push="true" ajax-target=".xamzo-right" original-title="back"></button>
    &nbsp;<button class="btn primary large fa fa-star left tip-top" style="margin-left: 5px; padding: 7.3px;" href="#!email/template" ajax="true" url-push="true" ajax-target=".xamzo-right" original-title="<?php echo $this->lang['template']; ?>"><span class="no-phone "><?php echo $this->lang['template']; ?></span></button>
    <button ajax="true" url-push="true" href="#!email/template" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top-right" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div>

<div id="crud-target" class="xamzo-setting"> 
    <div class="xamzo-block small no-tablet">
        <div class="xamzo-block-content">
            <div class="xamzo-block-header">
                <h4><?php echo $this->lang['email']; ?> <?php echo $this->lang['template']; ?> <span active="false" class="right xamzo-btn-search-setting" onclick="xamzo_search_show(this);"><i class="fa fa-search"></i></span></h4>
            </div>
            <div class="content">
                <ul class="xamzo-setting-list">
                    <div class="xamzo-setting-search">
                        <div class="arrow-up"></div>
                        <input type="text" name="search" onkeyup="xamzo_search_dd(this);" style="width: 98% !important" placeholder="<?php echo $this->lang['search'] ?>...">
                    </div>
                    <div class="xamzo-setting-content">
                        <?php
                        if ($dir_handle = opendir(MODPATH . 'email/views/template/')) {
                            while ($files = readdir($dir_handle)) {
                                if (strpos($files, '.php', 1)) {
                                    $files = str_replace(EXT, '', $files);
                                    ?>
                                    <li> 
                                        <a href="#!email/editor/<?php echo $files ?>" onclick="xamzo_set_list_active(this);" ajax="true" ajax-target="#xamzo-setting-config"> <i class="fa fa-dot-circle-o"></i> <?php echo strtolower($files) ?></a>
                                    </li>
                                    <?php
                                }
                            }
                        }
                        closedir($dir_handle);
                        ?>
                    </div>
                </ul>
            </div>
        </div>
    </div>
    <div class="xamzo-block separated"></div>
    <div class="xamzo-block big">
        <div class="xamzo-block-content" >
            <div id="xamzo-setting-config">
                <p><?php echo $this->lang['no_template_selected'] ?></p>
            </div>
        </div>
    </div>
</div> 