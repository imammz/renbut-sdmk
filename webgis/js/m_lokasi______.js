$(document).ready(function() {
    var mapCenter = new google.maps.LatLng(-3.5451241, 112.7293194); //Google map Coordinates
    var map;
	var iconPath  = "../icon/marker_1.png";

    map_initialize(); 
    function map_initialize() {

        var markers = [];
        var googleMapOptions = 
			{ 
				center: mapCenter, // map center
				zoom: 5,
				maxZoom: 18,
				//minZoom: 16,
				zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL //zoom control size
			},
				scaleControl: true, // enable scale control
				mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
			};
		
		   	map = new google.maps.Map(document.getElementById("map-canvas"), googleMapOptions);			
			
		$.get("get_marker.php", function (data) {
				$(data).find("marker").each(function () {
					  var name 		= $(this).attr('name');
					  var address 	= '<p>'+ $(this).attr('address') +'</p>';
					  var type 		= $(this).attr('type');
					  var point 	= new google.maps.LatLng(parseFloat($(this).attr('lat')),parseFloat($(this).attr('lng')));
					  create_marker(point, name, address, false, false, false,iconPath);
				});
			});	

        var input = /** @type {HTMLInputElement} */
            (
                document.getElementById('pac-input'));
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		

			google.maps.event.addListener(map, 'rightclick', function(event) {
				var EditForm = '<p style="width:400px"><div class="marker-edit">'+
				'<form action="ajax-save.php" method="POST" name="SaveMarker" id="SaveMarker">'+
				'<label for="pName"><span>Place Name :</span><input type="text" name="pName" class="save-name" placeholder="Enter Title" maxlength="40" /></label>'+
				'<label for="pDesc"><span>Description :</span><textarea name="pDesc" class="save-desc" placeholder="Enter Address" maxlength="150"></textarea></label>'+
				'<label for="pInfo1"><span>Info 1 :</span><textarea name="pInfo1" class="save-info1" placeholder="Enter Address" maxlength="150"></textarea></label>'+
				'<label for="pInfo2"><span>Info 2 :</span><textarea name="pInfo2" class="save-info2" placeholder="Enter Address" maxlength="150"></textarea></label>'+
				'<label for="pInfo3"><span>Info 3 :</span><textarea name="pInfo3" class="save-info3" placeholder="Enter Address" maxlength="150"></textarea></label>'+
				'<label for="pInfo4"><span>Info 4 :</span><textarea name="pInfo4" class="save-info4" placeholder="Enter Address" maxlength="150"></textarea></label>'+
				'</form>'+
				'</div></p><button name="save-marker" class="save-marker">Save Marker Details</button>';
				create_marker(event.latLng, 'Create New Location', EditForm, true, true, true, iconPath);
			});
		
		
        var searchBox = new google.maps.places.SearchBox(
            /** @type {HTMLInputElement} */
            (input));
        google.maps.event.addListener(searchBox, 'places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }

            markers = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });

                markers.push(marker);

                bounds.extend(place.geometry.location);
            }

            map.fitBounds(bounds);
        });

        google.maps.event.addListener(map, 'bounds_changed', function() {
            var bounds = map.getBounds();
            searchBox.setBounds(bounds);
        });

        // Trigger search on button click

        document.getElementById('trigger-search').onclick = function() {

            var input = document.getElementById('pac-input');

            google.maps.event.trigger(input, 'focus')
            google.maps.event.trigger(input, 'keydown', {
                keyCode: 13
            });
        };
    }


    function create_marker(MapPos, MapTitle, MapDesc, InfoOpenDefault, DragAble, Removable, iconPath) {
        //console.log(MapPos);
        var marker = new google.maps.Marker({
            position: MapPos,
            map: map,
            draggable: DragAble,
            animation: google.maps.Animation.DROP,
            title: "Hello World!",
            icon: iconPath
        });

        var contentString = $('<div class="marker-info-win" style="width:400px">' +
            '<div class="marker-inner-win"><span class="info-content">' +
            '<h1 class="marker-heading">' + MapTitle + '</h1>' +
            MapDesc +
            '</span><button name="remove-marker" class="remove-marker" title="Remove Marker">Remove Marker</button>' +
            '</div></div>');


        //Create an infoWindow
        var infowindow = new google.maps.InfoWindow();
        //set the content of infoWindow
        infowindow.setContent(contentString[0]);


        //Find remove button in infoWindow
        var removeBtn = contentString.find('button.remove-marker')[0];
        var saveBtn = contentString.find('button.save-marker')[0];

        //add click listner to remove marker button
        google.maps.event.addDomListener(removeBtn, "click", function(event) {
            remove_marker(marker);
        });

        if (typeof saveBtn !== 'undefined') //continue only when save button is present
        {
            //add click listner to save marker button
            google.maps.event.addDomListener(saveBtn, "click", function(event) {
                var mReplace = contentString.find('span.info-content'); //html to be replaced after success
                var mName = contentString.find('input.save-name')[0].value; //name input field value
                var mDesc = contentString.find('textarea.save-desc')[0].value; //description input field value
                var mInfo1 = contentString.find('textarea.save-info1')[0].value; //type of marker
				var mInfo2 = contentString.find('textarea.save-info2')[0].value; //type of marker
				var mInfo3 = contentString.find('textarea.save-info3')[0].value; //type of marker
				var mInfo4 = contentString.find('textarea.save-info4')[0].value; //type of marker

                if (mName == '' || mDesc == ''||mInfo1 == '' || mInfo2 == ''||mInfo3 == '' || mInfo4 == '') {
                    alert("Please enter the value!");
                } else {
                    save_marker(marker, mName, mDesc,mInfo1,mInfo2,mInfo3,mInfo4, mReplace); //call save marker function
                }
            });
        }

        //add click listner to save marker button		 
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker); // click on marker opens info window 
        });

        if (InfoOpenDefault) //whether info window should be open by default
        {
            infowindow.open(map, marker);
        }
    }

    function remove_marker(Marker) {
        if (Marker.getDraggable()) {
            Marker.setMap(null); //just remove new marker
        } else {
            var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
            var myData = {
                del: 'true',
                latlang: mLatLang
            }; //post variables
            $.ajax({
                type: "POST",
                url: "get_marker.php",
                data: myData,
                success: function(data) {
                    Marker.setMap(null);
                    alert(data);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError); //throw any errors
                }
            });
        }

    }

    function save_marker(Marker, mName, mAddress, mInfo1,mInfo2,mInfo3,mInfo4,replaceWin) {
        var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
        var myData = {
            name: mName,
            address: mAddress,
            latlang: mLatLang,
            info1 : mInfo1,
			info2 : mInfo2,
			info3 : mInfo3,
			info4 : mInfo4
        }; //post variables
        console.log(replaceWin);
        $.ajax({
            type: "POST",
            url: "get_marker.php",
            data: myData,
            success: function(data) {
                replaceWin.html(data); //replace info window with new html
                Marker.setDraggable(false); //set marker to fixed
                Marker.setIcon(iconPath); //replace icon
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError); //throw any errors
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', map_initialize);

});