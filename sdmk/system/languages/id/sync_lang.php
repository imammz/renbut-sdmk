<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang['synchronize'] = 'sinkronisasi'; 
$lang['upgrade_now'] = 'Upgrade Now'; 
$lang['update_now'] = 'Update Now'; 
$lang['an_update_engine_version_of_codeanalytic_is_available'] = 'Versi terbaru mesin CodeAnalytic tersedia'; 
$lang['this_page_to_synchronize_your_cms_with_market,_see_documentation_for_use_it'] = 'Halaman ini untuk menyinkronkan cms dengan pasar, lihat dokumentasi untuk menggunakannya.';
$lang['before_updating,_please_backup_your_database_and_file'] = 'Sebelum memperbarui, silahkan backup database dan berkas';
$lang['the_following_modules_have_new_version_available'] = 'module berikut memiliki versi terbaru';
$lang['the_following_extensions_have_new_version_available'] = 'extension berikut memiliki versi terbaru';
$lang['the_following_widgets_have_new_version_available'] = 'widget berikut memiliki versi terbaru';
$lang['the_following_themes_have_new_version_available'] = 'Tema-tema berikut memiliki versi terbaru';
$lang['check_the_ones_you_want_to_update_and_then_click_update']='Periksa yang ingin Anda memperbarui dan kemudian klik update';
$lang['not_available_update_for'] = 'Tidak tersedia update untuk';
$lang['update_module_is_checked'] = 'Pilih module untuk update';
$lang['update_extension_is_checked'] = 'Pilih ekstensi untuk update';
$lang['update_widget_is_checked'] = 'Pilih widget untuk update';
$lang['update_theme_is_checked'] = 'Pilih tema untuk update';

$lang['extension'] ='extension';



