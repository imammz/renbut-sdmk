<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<div class="modal-header">
    <button type="button" class="close" onclick='close_box()' data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3><i class="fa fa-link"></i> <?php echo $this->lang['select_and_copy_url']; ?></h3>
</div>
<div class="modal-body"> 
    <p>
        <input type="text" id="file-copy" style="width: 98.5%" value="<?php echo $url ?>">
    </p>
</div> 
<div class="modal-footer border-top"> 
    <a class="btn primary medium right" onclick="txt_select('#file-copy');"><?php echo $this->lang['select']; ?></a>
</div>