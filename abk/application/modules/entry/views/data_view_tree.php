<?php echo Modules::run('templates/cliptwo/header'); ?>
<link href="<?php echo base_url() ?>vendor/jstree/themes/default/style.min.css" rel="stylesheet" media="screen">
<!-- end: HEAD -->
<body>

    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white">
        <div class="row">
            <div class="col-md-8"> 
                <input type="text" class="form-control margin-bottom-10" value="" id="faskes_search" placeholder="Search">
                <div id="faskes" class="tree-demo"></div>

                
            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>vendor/jstree/jstree.min.js"></script>   
    <!-- start: JavaScript Event Handlers for this page -->
    
    <script>
    var UITreeview = function() {
	"use strict";
        
        var faskesTreeView = function() {
		
		$("#faskes").jstree({
			"core" : {
				"themes" : {
					"responsive" : false
				},
				// so that create works
				"check_callback" : true,
				'data' : [
                                    
                                  <?php foreach($data_faskes['provinsi'] as $row_prov) { ?>  
                                     {
                                         "text" : "<?php echo $row_prov['NAMA_PROV'] ?>",
                                         "icon" : "fa fa-circle-o text-warning",
                                         "children" : [
                                         <?php foreach($row_prov['kabupaten'] as $row_kab) { ?>
                                                {
                                                  "text": "<?php echo $row_kab['NAMA_KAB'] ?> - <span style=\"text-align: right; width: right;\"> <button class=\"btn btn-success\" type=\"button\"> Success </button> </span>",
                                                  "icon" : "fa fa-circle-o text-info",
                                                    
                                                },
                                                        
                                         <?php } ?>   
                                         ]
                                         
                                     },
                                     
                                  <?php } ?>  
                                ]
			},
			"types" : {
				"default" : {
					"icon" : "fa fa-folder text-primary fa-lg"
				},
				"file" : {
					"icon" : "fa fa-file text-primary fa-lg"
				}
			},
			"state" : {
				"key" : "demo2"
			},
			"plugins" : ["search", "types"]
		});
		var to = false;
		$('#faskes_search').keyup(function() {
			if (to) {
				clearTimeout(to);
			}
			to = setTimeout(function() {
				var v = $('#faskes_search').val();
				$('#faskes').jstree(true).search(v);
			}, 250);
		});
	};
        
	
	return {
		//main function to initiate template pages
		init : function() {
			faskesTreeView();
		}
	};
}();

    </script>
    
    <script>
        jQuery(document).ready(function () {
            Main.init();
            UITreeview.init();
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
