<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_faskes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_jenis_faskes_model','jenis_faskes');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Jenis Faskes";
		$data['subtitle'] = "Daftar Jenis Faskes";
		$this->load->view('jenis_faskes/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Jenis Faskes";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->jenis_faskes->get_by_id($id);
		}
		$this->load->view('jenis_faskes/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->jenis_faskes->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $jenis_faskes) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($jenis_faskes->nama_jenis_faskes);
			$row[] = ($jenis_faskes->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $jenis_faskes->updated_date?Tanggal::formatDateTime($jenis_faskes->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($jenis_faskes->id_jenis_faskes,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_jenis_faskes('."'".Regex::_genRegex($jenis_faskes->id_jenis_faskes,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->jenis_faskes->count_all(),
						"recordsFiltered" => $this->jenis_faskes->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_jenis_faskes = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'nama_jenis_faskes' => Regex::_genRegex($this->input->post('nama_jenis_faskes'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_jenis_faskes == 0 ){
			$this->jenis_faskes->save($dataexc);
		}else{
			$this->jenis_faskes->update(array('id_jenis_faskes'=>$id_jenis_faskes), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->jenis_faskes->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function autocomplate()
	{
		$keyword = $this->input->get('q'); /*print_r($keyword);die;*/
		$data = $this->jenis_faskes->get_by_keyword($keyword);
		
		echo json_encode($data);
		exit;
	}

	
}
