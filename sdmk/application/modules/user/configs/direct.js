var obj = { 
// Administrator 
    'Administrator': { 
        'proyeksi': {
            'r': true,
            'w': true,
            'x': true,
            'p': true
        },
        'con_pengguna': {
            'r': true,
            'w': true,
            'x': true
        },
        'con_pengaturan_umum': {
            'r': true,
            'w': true,
            'x': true 
        },
        'con_keamanan': {
            'r': true,
            'w': true,
            'x': true
        },
        'con_database': {
            'r': true,
            'w': true,
            'x': true
        }
    },
// RegularUser 
    'RegularUser': { 
        'proyeksi': {
            'r': true,
            'w': true,
            'x': true,
            'p': true
        },
        'con_pengguna': {
            'r': false,
            'w': false,
            'x': false 
        },
        'con_pengaturan_umum': {
            'r': false,
            'w': false,
            'x': false 
        },
        'con_keamanan': {
            'r': false,
            'w': false,
            'x': false 
        },
        'con_database': {
            'r': false,
            'w': false,
            'x': false 
        } 
    }
};