<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<script type="text/javascript" src="<?php echo site_url('application/modules/user/configs/direct.js') ?>"></script>
<div class="modal-header">
    <button type="button" class="close btn success large" style="margin: 2px" onclick="close_box()">×</button>
    <h2 >
        <span id="auth-pp-smile">
            <img src="<?php echo is_url($photo) ? $photo : site_url($photo) ?>" width="60px">
        </span>
        <?php echo character_limiter($user,18) ?> &nbsp;     
        <input  type="text" name="search"  placeholder="<?php echo $this->lang['search'] ?>.." onkeyup="xamzo_list_search(this);" class="right tip-bottom-left auth-package-search" original-title="<?php echo $this->lang['search_package'] ?>">
    </h2>
</div>
<div class="modal-body"> 
    <form id="crud-form-modal">
        <div style="position: relative; min-height: 400px">
            <div id="scrollbar2" style="margin:0" class="xamzo-scrollbar">
                <div class="scrollbar" style="height: 350px;">
                    <div class="track" style="height: 350px;">
                        <div class="thumb" style="top: 96.62027833001987px; height: 39.761431411530815px;">
                            <div class="end"></div>
                        </div>
                    </div>
                </div>
                <div class="viewport" style="height: 350px;">
                    <div class="overview" style="top: -486px;">
                        <?php
                        $kr = array();
                        if (is_array($auth)) {
                            foreach ($auth as $key => $value) {
                                foreach ($value as $k => $v) {
                                    $kr[$k] = $v;
                                }
                            }
                        }
                        foreach ($dirr as $key => $k) {
                            if (is_array($k)) {
                                ?>
                                <ol style="list-style: circle"> 
                                    <li>
                                        <span style="display: none" class="s-parameter"><?php echo strtolower($key) ?></span>
                                        <label class="data-label"><?php echo strtoupper($key) ?></label>
                                        <ol style="list-style: square; margin-left: -25px;">
                                            <?php
                                            foreach ($k as $kk => $c1) {
                                                if (is_array($c1)) {
                                                    ?>
                                                    <ol style="list-style: circle; margin-left: -25px"> 
                                                        <li>
                                                            <span style="display: none" class="s-parameter"><?php echo strtolower($kk) ?></span>
                                                            <label class="data-label"><?php echo ucwords(str_replace(array('lap', 'con', '_'), ' ', $kk)) ?></label>
                                                            <ol style="list-style: square; margin-left: -25px;">
                                                                <?php
                                                                foreach ($c1 as $kkk => $c2) {
                                                                    if (is_array($c2)) {
                                                                        ?>
                                                                        <ol style="list-style: circle; margin-left: -25px"> 
                                                                            <li>
                                                                                <span style="display: none" class="s-parameter"><?php echo strtolower($kkk) ?></span>
                                                                                <label class="data-label"><?php echo ucwords(str_replace(array('lap', 'con', '_'), ' ', $kkk)) ?></label>
                                                                                <ol style="list-style: square; margin-left: -25px;">
                                                                                    <?php
                                                                                    foreach ($c2 as $c3) {
                                                                                        ?> 
                                                                                        <li class="list-search">
                                                                                            <span style="display: none" class="s-parameter"><?php echo strtolower($c3) ?></span>
                                                                                            <label style="width: 150px; display: inline-block;"><?php echo ucwords(str_replace(array('lap', 'con', '_'), ' ', $c3)) ?></label>
                                                                                            <input type="hidden" name="<?php echo $c3 ?>[]" value="<?php echo $c3 ?>">
                                                                                            <input type="checkbox" name="<?php echo $c3 ?>[r]" <?php echo (isset($kr[$c3]['r']) && $kr[$c3]['r'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c3 ?>[r]"> <label for="<?php echo $c3 ?>[r]"> r</label>
                                                                                            <input type="checkbox" name="<?php echo $c3 ?>[w]" <?php echo (isset($kr[$c3]['w']) && $kr[$c3]['w'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c3 ?>[w]"> <label for="<?php echo $c3 ?>[w]"> w</label>
                                                                                            <input type="checkbox" name="<?php echo $c3 ?>[x]" <?php echo (isset($kr[$c3]['x']) && $kr[$c3]['x'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c3 ?>[x]"> <label for="<?php echo $c3 ?>[x]"> x</label>
                                                                                            <?php if(strtolower($key)!='aplikasi' && strtolower($key)!='pengaturan' && strtolower($c3)!='diskusi'): ?>
                                                                                            <input type="checkbox" name="<?php echo $c3 ?>[p]" <?php echo (isset($kr[$c3]['p']) && $kr[$c3]['p'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c3 ?>[p]"> <label for="<?php echo $c3 ?>[p]"> p</label>
                                                                                            <?php endif; ?>
                                                                                        </li> 
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </ol>
                                                                            </li>
                                                                        </ol> 
                                                                        <?php
                                                                    } else {
                                                                        ?> 
                                                                        <li class="list-search">
                                                                            <span style="display: none" class="s-parameter"><?php echo strtolower($c2) ?></span>
                                                                            <label style="width: 150px; display: inline-block;"><?php echo ucwords(str_replace(array('lap', 'con', '_'), ' ', $c2)) ?></label>
                                                                            <input type="hidden" name="<?php echo $c2 ?>[]" value="<?php echo $c2 ?>">
                                                                            <input type="checkbox" name="<?php echo $c2 ?>[r]" <?php echo (isset($kr[$c2]['r']) && $kr[$c2]['r'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c2 ?>[r]"> <label for="<?php echo $c2 ?>[r]"> r</label>
                                                                            <input type="checkbox" name="<?php echo $c2 ?>[w]" <?php echo (isset($kr[$c2]['w']) && $kr[$c2]['w'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c2 ?>[w]"> <label for="<?php echo $c2 ?>[w]"> w</label>
                                                                            <input type="checkbox" name="<?php echo $c2 ?>[x]" <?php echo (isset($kr[$c2]['x']) && $kr[$c2]['x'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c2 ?>[x]"> <label for="<?php echo $c2 ?>[x]"> x</label>
                                                                            <?php if(strtolower($key)!='aplikasi' && strtolower($key)!='pengaturan' && strtolower($c2)!='diskusi'): ?>
                                                                            <input type="checkbox" name="<?php echo $c2 ?>[p]" <?php echo (isset($kr[$c2]['p']) && $kr[$c2]['p'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c2 ?>[p]"> <label for="<?php echo $c2 ?>[p]"> p</label>
                                                                            <?php endif; ?>
                                                                        </li> 
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </ol>
                                                        </li>
                                                    </ol>
                                                    <?php
                                                } else {
                                                    ?> 
                                                    <li class="list-search">
                                                        <span style="display: none" class="s-parameter"><?php echo strtolower($c1) ?></span>
                                                        <label style="width: 150px; text-transform: capitalize; display: inline-block;"><?php echo ucfirst(str_replace(array('lap', 'con', '_'), ' ', $c1)) ?></label>
                                                        <input type="hidden" name="<?php echo $c1 ?>[]" value="<?php echo $c1 ?>">
                                                        <input type="checkbox" name="<?php echo $c1 ?>[r]" <?php echo (isset($kr[$c1]['r']) && $kr[$c1]['r'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $c1 ?>[r]"> <label for="<?php echo $c1 ?>[r]"> r</label>
                                                        <input type="checkbox" name="<?php echo $c1 ?>[w]" <?php echo (isset($kr[$c1]['w']) && $kr[$c1]['w'] == '1') ? 'checked="true"' : '' ?>  id="<?php echo $c1 ?>[w]"> <label for="<?php echo $c1 ?>[w]"> w</label>
                                                        <input type="checkbox" name="<?php echo $c1 ?>[x]" <?php echo (isset($kr[$c1]['x']) && $kr[$c1]['x'] == '1') ? 'checked="true"' : '' ?>  id="<?php echo $c1 ?>[x]"> <label for="<?php echo $c1 ?>[x]"> x</label>
                                                        <?php if(strtolower($key)!='aplikasi' && strtolower($key)!='pengaturan' && strtolower($c1)!='diskusi'): ?>
                                                        <input type="checkbox" name="<?php echo $c1 ?>[p]" <?php echo (isset($kr[$c1]['p']) && $kr[$c1]['p'] == '1') ? 'checked="true"' : '' ?>  id="<?php echo $c1 ?>[p]"> <label for="<?php echo $c1 ?>[p]"> p</label>
                                                        <?php endif; ?>
                                                    </li> 
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ol>
                                    </li>

                                </ol>
                                <?php
                            } else {
                                if (!in_array($k, $kr)) {
                                    ?>
                                    <div class="one">
                                        <p class="xamzo-user-auth list-search">
                                            <span style="display: none" class="s-parameter"><?php echo strtolower($k) ?></span>
                                            <label class="control-label"><?php echo $k ?></label>
                                            <input type="hidden" name="<?php echo $k ?>[]" value="<?php echo $k ?>">
                                            <input type="checkbox" name="<?php echo $k ?>[r]" <?php echo (isset($kr[$k]['r']) && $kr[$k]['r'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $k ?>[r]"> <label for="<?php echo $k ?>[r]"> r</label>
                                            <input type="checkbox" name="<?php echo $k ?>[w]" <?php echo (isset($kr[$k]['w']) && $kr[$k]['w'] == '1') ? 'checked="true"' : '' ?>id="<?php echo $k ?>[w]"> <label for="<?php echo $k ?>[w]"> w</label>
                                            <input type="checkbox" name="<?php echo $k ?>[x]" <?php echo (isset($kr[$k]['x']) && $kr[$k]['x'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $k ?>[x]"> <label for="<?php echo $k ?>[x]"> x</label>
                                            <input type="checkbox" name="<?php echo $k ?>[p]" <?php echo (isset($kr[$k]['p']) && $kr[$k]['p'] == '1') ? 'checked="true"' : '' ?> id="<?php echo $k ?>[p]"> <label for="<?php echo $k ?>[p]"> x</label>
                                        </p>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer border-top">
    <div class="custom-auth">
        <ul> 
            <li>
                <a href="javascript:void(0)" onclick="set_auth_data('1')">Administrator</a>
            </li> 
            <li>
                <a href="javascript:void(0)" onclick="set_auth_data('2')">Regular User</a>
            </li> 
        </ul>
        <div class="arrow-up"></div>
    </div>
    <div class="left">
        <a href="javascript:void(0)" class="btn" id="btn-direct" onclick="show_custom_auth(this)"><b><i class="fa fa-cogs"></i> Direct</b></a> | 
        <b> r </b> : <?php echo $this->lang['read']; ?> |
        <b> w </b> : <?php echo $this->lang['write']; ?> |
        <b> x </b> : <?php echo $this->lang['delete']; ?> |
        <b> p </b> : <?php echo 'cetak'; ?>
    </div>

    <button class="btn primary medium right" href="<?php echo isset($action) ? $action . $id : '' ?>" ajax="true" ajax-data="#crud-form-modal" ajax-target="false" ajax-type="POST" ><?php echo $this->lang['submit'] ?></button>
</div> 

<script type="text/javascript">
    $('#scrollbar2').tinyscrollbar();
   
    function show_custom_auth(i) {
        if ($(i).hasClass('selected')) {
            $('div.custom-auth').hide('slow');
            $(i).removeClass('selected');
        } else {
            $('div.custom-auth').show('slow');
            $(i).addClass('selected');
        }
    }
    function set_auth_data(x) {
        $('#crud-form-modal').find('input[type="checkbox"]').removeAttr('checked');
        switch (x) { 
            case '1':
                jQuery.each(obj.Administrator, function (j, modul) {
                    jQuery.each(modul, function (k, key) {
                        if (key === true) {
                            $('label[for="' + j + '[' + k + ']"]').click();
                        }
                    });
                });
                break; 
            case '2':
                jQuery.each(obj.RegularUser, function (j, modul) {
                    jQuery.each(modul, function (k, key) {
                        if (key === true) {
                            $('label[for="' + j + '[' + k + ']"]').click();
                        }
                    });
                });
                break; 
            default:
                break;
        }
        $('div.custom-auth').hide('slow');
        $('#btn-direct').removeClass('selected');
    }
</script>