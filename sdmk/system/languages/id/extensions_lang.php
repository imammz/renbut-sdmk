<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$lang['config'] = 'konfigurasi';
$lang['extensions_list'] = 'Daftar extensions';
$lang['get_more_extensions'] = 'cari ekstensi lain';
$lang['extension_is_up_to_date'] = 'ekstensi terupdate ';
$lang['or_can'] = 'atau dapat';
$lang['no_configuration_inside_the_extension'] = 'tidak ada konfigurasi dalam ekstensi';
$lang['this_page_to_manage_website_extensions,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola modul situs, lihat dokumentasi untuk menggunakannya';