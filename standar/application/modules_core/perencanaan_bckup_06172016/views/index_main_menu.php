<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        
        <br>
        <div class="row">

            <div class="col-xs-12 col-sm-12 pricing-box">

                <div class="col-sm-12">
                  <h3 class="header smaller lighter red"><center>Perencanaan Kebutuhan Minimal SDM Kesehatan</center></h3>
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                  <div class="col-sm-4">
                    <center>
                      <a href="#" onclick="getMenu('perencanaan/tr_puskesmas')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="ace-icon fa fa-plus-square bigger-250"></i></h2>
                          <h4 class="blue">Puskesmas</h4>
                          Rencana Kebutuhan SDMK Puskesmas
                      </div>
                      </a>
                    </center>
                  </div>
                  <?php }?>
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','6')) ){?>
                  <div class="col-sm-4">
                    <center>
                      <a href="#" onclick="getMenu('perencanaan/tr_rsu')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="ace-icon fa fa-building bigger-250"></i></h2>
                          <h4 class="blue">Rumah Sakit Umum</h4>
                          Rencana Kebutuhan SDMK Rumah Sakit Umum
                        </div>
                      </a>
                    </center>
                  </div>
                  <?php }?>
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','7')) ){?>
                  <div class="col-sm-4">
                    <center>
                      <a href="#" onclick="getMenu('perencanaan/tr_rsk')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="ace-icon fa fa-hospital-o bigger-250"></i></h2>
                          <h4 class="blue">Rumah Sakit Khusus</h4>
                          Rencana Kebutuhan SDMK Rumah Sakit Khusus
                        </div>
                      </a>
                    </center>
                  </div>
                  <?php }?>
                
                </div><!-- /.col -->

                <div class="col-sm-12">
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','8')) ){?>
                  <div class="col-sm-4">
                    <center>
                      <a href="#" onclick="getMenu('perencanaan/tr_kkp')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="ace-icon fa fa-anchor bigger-250"></i></h2>
                          <h4 class="blue">KKP</h4>
                          Rencana Kebutuhan SDMK KKP
                      </div>
                      </a>
                    </center>
                  </div>
                  <?php }?>
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','9')) ){?>
                  <div class="col-sm-4">
                    <center>
                      <a href="#" onclick="getMenu('perencanaan/tr_btkl')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="ace-icon fa fa-stethoscope bigger-250"></i></h2>
                          <h4 class="blue">BTKLPP</h4>
                          Rencana Kebutuhan SDMK BTKLPP
                        </div>
                      </a>
                    </center>
                  </div>
                  <?php }?>
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','10')) ){?>
                  <div class="col-sm-4">
                    <center>
                      <a href="#" onclick="getMenu('perencanaan/tr_klinik')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="ace-icon fa fa-medkit bigger-250"></i></h2>
                          <h4 class="blue">Klinik</h4>
                          Rencana Kebutuhan SDMK Klinik
                        </div>
                      </a>
                    </center>
                  </div>
                  <?php }?>
                
                </div><!-- /.col -->

            </div>

            <!-- /section:pages/pricing.large -->
          </div>

      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/perencanaan.js'?>"></script>
