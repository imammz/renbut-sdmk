<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/user
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Model_user extends X_Model {

    public $table = 'user'; 

    public function __construct() {
        parent::__construct();
        $this->table = $this->db->prefix . $this->table;
    }

    public function object() {
        $this->db
                ->from($this->table);
        $order = ($this->request->post("order") && $this->request->post('by')) ? '' . $this->request->post('order') . ' ' . $this->request->post('by') : '`id` ASC';
        $this->db->order_by($order);
        ($this->request->post('search') && $this->request->post('search') != '') ? $this->db->like('fullname', '%' . $this->request->post('search') . '%') : '';
    }

    public function get_all($offset, $limit) {
        $this->object();
        return $this->db
                        ->limit($limit, $offset)
                        ->get()
                        ->result();
    }

    public function num_rows() {
        $this->object();
        return $this->db
                        ->get()
                        ->num_rows();
    }

    public function insert($values = array()) {
        return $this->db->insert($values, $this->table);
    }

    public function get_where_id($id = null) {
        if ($id != null) {
            return $this->db
                            ->from($this->table)
                            ->where('id', $id)
                            ->get()
                            ->row();
        }
    }

    public function update($data = array(), $wheres = null) {
        $this->db->update($data, $wheres, $this->table);
    }

    public function delete($in = null) {
        $this->db->where('id', $in)->delete($this->table);
        $this->db->where('user_id', $in)->delete($this->tbl_sdm);
    }

    public function get_photo($id) {
        return $this->db->select('photo')->from($this->table)->where(array('id' => $id))->get()->row();
    }

    public function login($data = array()) {
        if (is_array($data)) {
            $q = $this->db
                    ->from($this->table)
                    ->where(array('password' => $data['password'], 'email' => $data['email']))
                    ->get();

            $rs = $q->row();
            $total = $q->num_rows();
            if ($total > 0) {
                $session = array();
                $session['session_fullname'] = $rs->fullname;
                $session['session_user_id'] = $rs->id;
                $session['session_photo'] = $rs->photo;
                $session['session_level'] = $rs->level;
                $session['limit'] = 25;
                $this->session->set_data($session);
                return $rs->id;
            } else {
                return false;
            }
        }
    }

    public function get_where_email($email = null) {
        if ($email != null) {
            return $this->db
                            ->from($this->table)
                            ->where('email', $email)
                            ->get();
        }
    }

}
