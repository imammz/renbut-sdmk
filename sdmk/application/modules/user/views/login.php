<!DOCTYPE html><?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<html id="xamzo"> 
    <head>
        <meta name="fragment" content="!"> 
        <link rel="shortcut icon" href="<?php echo site_url('application/templates/private/assets/favicon.png') ?>" type="image/png" />   
        <title><?php echo isset($meta_title) ? $meta_title : 'Login Apps' ?> &rsaquo; <?php echo $this->lang['log_in'] ?></title>
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('application/templates/private/assets/styles/x_x.css') ?>"> 
        <script src="<?php echo site_url('application/templates/private/assets/javascript/libraries/jquery/1.7.2/jquery.min.js'); ?>"></script>
        <script src="<?php echo site_url('application/templates/private/assets/javascript/libraries/jqueryui/1.7.2/jquery.ui.min.js'); ?>"></script>
        <script type="text/javascript">
            var _h = _h || [];
            _h.push([
                '<?php echo site_url() ?>',
                 '<div id="circularG"><div id="circularG_1" class="circularG"></div><div id="circularG_2" class="circularG"></div><div id="circularG_3" class="circularG"></div><div id="circularG_4" class="circularG"></div><div id="circularG_5" class="circularG"></div><div id="circularG_6" class="circularG"></div><div id="circularG_7" class="circularG"></div><div id="circularG_8" class="circularG"></div></div>',
                '<?php echo X_ANIMATE  ?>',
                '#bg-login']
                    );
            (function() {
                var _s = _h['0']['0'];
                var h = document.createElement('script');
                h.type = 'text/javascript';
                h.async = true;
                h.src = ('https:' === document.location.protocol ? 'https://ssl' : _s) + 'application/templates/private/assets/javascript/libraries/xamzo/wrapper.js';
                var sr = document.getElementsByTagName('script')[1];
                sr.parentNode.insertBefore(h, sr);
            })();
        </script>
        <script type="text/javascript" src="<?php echo site_url('application/templates/private/assets/javascript/libraries/xamzo/validation.js'); ?>"></script>
    </head>
    <body id="bg-login"> 
        <div class="xamzo-center">
            <div class="xamzo-loginbox">
                <div class="xamzo-loginboxinner">  
                    <!--<div class="xamzo-logo"></div>-->
                    <form action="<?php echo site_url($action) ?>" method="post" id="crud-login"> 
                        <input type="hidden" name="emitter" value="1">
                        <span class="error"><?php echo isset($login_error) ? '<p align="center">' . $login_error . '</p>' : ''; ?></span>                        
                        <div class="xamzo-username">
                            <span class="xamzo-form-icon"><i class="fa fa-envelope" style="font-size: 14px;position: relative;top: -3px;"></i></span>
                            <input placeholder="Email" type="text" name="email" value="<?php echo isset($default['email']) ? $default['email'] : '' ?>"> 
                        </div>
                        <span class="error"><?php echo isset($errors['username']) ? $errors["username"] : ''; ?></span>
                        <div class="xamzo-password">
                            <span class="xamzo-form-icon"><i class="fa fa-lock"></i></span>
                            <input type="password" id="password" name="password" placeholder="Password" >                            
                        </div>
                        <span class="error" id="capsplock" style="display: none;"><?php echo $this->lang['capital_letter_is_on'] ?></span> 
                        <span class="error"><?php echo isset($errors['password']) ? $errors['password'] : ''; ?></span>
                        <button  class="btn glow primary" ajax-url="#!user/login/" is_close_box="true" ajax-bind="login" ajax="true" ajax-data="#crud-login" ajax-target="form#crud-login" ajax-type="POST"><?php echo $this->lang['login'] ?></button>
                        <div class="xamzo-keep">&nbsp;&nbsp; <a ajax="true" href="#!user/forgot" ajax-target="modalbox" modal-width="400px" modal-fix="true" title="<?php echo $this->lang['forgot_password'] ?>"><?php echo $this->lang['forgot_password'] ?>?</a></div>
                    </form>
                </div>
            </div>
            <div class="xamzo-forgot"> 
            </div>
        </div> 
    </body>
</html> 