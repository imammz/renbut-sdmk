<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_puskesmas_model extends CI_Model {

	var $table = 'm_puskesmas';
	var $column = array('m_puskesmas.kode_puskesmas','m_puskesmas.nama_puskesmas_kab','m_kabupaten.nama_kabupaten','m_tipe_kawasan.nama_tipe_kawasan','m_tipe_puskesmas.nama_tipe_puskesmas','m_puskesmas.active','m_puskesmas.updated_date');
	var $select = 'm_puskesmas.*, m_provinsi.nama_provinsi, m_kabupaten.nama_kabupaten, m_tipe_kawasan.nama_tipe_kawasan, m_tipe_puskesmas.nama_tipe_puskesmas';

	var $order = array('m_puskesmas.kode_puskesmas' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_kabupaten', ''.$this->table.'.id_kabupaten=m_kabupaten.id_kabupaten' , 'left' );
		$this->db->join('m_provinsi', 'm_provinsi.id_provinsi=m_kabupaten.id_provinsi' , 'left' );
		$this->db->join('m_tipe_puskesmas', ''.$this->table.'.id_tipe_puskesmas=m_tipe_puskesmas.id_tipe_puskesmas' , 'left' );
		$this->db->join('m_tipe_kawasan', ''.$this->table.'.id_tipe_kawasan=m_tipe_kawasan.id_tipe_kawasan' , 'left' );


		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_kabupaten', ''.$this->table.'.id_kabupaten=m_kabupaten.id_kabupaten' , 'left' );
		$this->db->join('m_provinsi', 'm_provinsi.id_provinsi=m_kabupaten.id_provinsi' , 'left' );
		$this->db->join('m_tipe_puskesmas', ''.$this->table.'.id_tipe_puskesmas=m_tipe_puskesmas.id_tipe_puskesmas' , 'left' );
		$this->db->join('m_tipe_kawasan', ''.$this->table.'.id_tipe_kawasan=m_tipe_kawasan.id_tipe_kawasan' , 'left' );
		$this->db->where(''.$this->table.'.id_puskesmas',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.id_puskesmas', $id);
		$this->db->delete($this->table);
	}

	public function get_total_rows_server()
	{
		$file = file_get_contents(base_url().'uploaded_files/data/puskesmas.json');
		$decode_file = json_decode($file);

		return $decode_file->total;
	}

	public function get_by_keyword($q){
		
        //$this->db->from($this->table);
		//$this->db->like(''.$this->table.'.nama_jenis_faskes', $q);

		$rows = $this->db->get($this->table)->result_array(); //echo '<pre>'; print_r($rows);die;
        /*$rows = $this->db->query('
                SELECT `id`,`name`,`nip`,`status` FROM `user` WHERE `status` != 0 AND (`name` LIKE ? OR `nip` LIKE ?)
            ', array('%' . $q . '%', '%' . $q . '%'))->result_array();*/
        $data = array();
        foreach ($rows as $key => $val) {
        	# code...
        	$value['id'] = $val['kode_puskesmas'];
            $value['name'] = $val['nama_puskesmas_kab'];
            $data[] = $value;
        }
        //echo '<pre>';
        //print_r($data);die;
        return $data;
    }



}
