<html>
    <head>
        <title>News Letter</title>
    </head>
    <body style='float:left; width:99%; font-family: sans-serif, serif; background: #f5f5f5; line-height:25px; font-size: 13px'>
        <div style="background: #fff; width: 620px; margin: 0px auto; border: 1px solid #ddd;">
             <h3 style="background: #50b7dc; color:#fff; padding: 5px; margin:0px; border-bottom: 1px solid #ddd">[News Letter], Hi,.. {to} we have something new for you</h3>
             <h3 style="padding: 5px; margin:0px; color:#666; border-bottom: 1px solid #ddd"><a style="text-decoration: none; color:#666" href="<?php echo site_domain() ?>{permalink}" target="_blank">{title}</a></h3>
            <div style="border-bottom: 1px solid #ddd; color:#666; padding: 5px; font-size: 12px">
                 <span><b>Date:</b> <?php echo date_format(date_create(date('Y-m-d')), 'D, d F Y');?> </span>&nbsp;|&nbsp;<span><b>From:</b> {from}</span>                        
            </div> 
            <div style="width: 99%; padding: 5px; color:#666">
                 <p>{mail_content}</p>
            </div>
        </div>
    </body>
</html>