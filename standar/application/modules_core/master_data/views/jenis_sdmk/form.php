<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_jenis_sdmk" action="<?php echo site_url('master_data/m_jenis_sdmk/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_jenis_sdmk:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Jenis SDMK</label>
                  <div class="col-md-6">
                    <input name="nama_jenis_sdmk" id="nama_jenis_sdmk" value="<?php echo isset($value)?$value->nama_jenis_sdmk:''?>" placeholder="Nama Jenis Faskes" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kelompok SDMK</label>
                  <div class="col-md-5">
                    <?php echo Master::get_master_kelompok_sdmk(isset($value)?$value->id_kelompok_sdmk:'','id_kelompok_sdmk','kelompok_sdmk','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Jenis Faskes</label>
                  <div class="col-md-5">
                    <?php echo Master::get_master_jenis_faskes(isset($value)?$value->id_jenis_faskes:'','id_jenis_faskes','jenis_faskes','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Parent</label>
                  <div class="col-md-5">
                    <?php echo Master::get_master_jenis_sdmk(isset($value)?$value->parent:'','id_jenis_sdmk','jenis_sdmk','chosen-select form-control','required','inline');?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Counter</label>
                  <div class="col-md-2">
                    <input name="counter" id="counter" value="<?php echo isset($value)?$value->counter:''?>" placeholder="Counter" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Standar ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="is_standar" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->is_standar == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="is_standar" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->is_standar == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Jadikan Form Input?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="set_form" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->set_form == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="set_form" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->set_form == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_jenis_sdmk:0?>"> -->

                  <a onclick="getMenu('master_data/m_jenis_sdmk')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/jenis_sdmk.js'?>"></script>
