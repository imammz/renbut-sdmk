<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package		xamzo/base
 * @author		hadinug, xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */


class X_Base extends X_Loader{
 
    public function __construct() {  
        new X_Loader();
    }
 
} 
function &get_instance() {
    $obj = new X_Base();
    return $obj;
}