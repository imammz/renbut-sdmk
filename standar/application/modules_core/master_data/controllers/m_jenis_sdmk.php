<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_sdmk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_jenis_sdmk_model','jenis_sdmk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Jenis SDMK";
		$data['subtitle'] = "Daftar Jenis SDMK";
		$this->load->view('jenis_sdmk/index', $data);
	}

	public function form($id='')
	{
		$data['title'] = "Form Jenis SDMK";
		$data['subtitle'] = "";

		if($id==''){
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'R');
			if( $id != '' ){
				$data['value'] = $this->jenis_sdmk->get_by_id($id);
			}
		}
		
		$this->load->view('jenis_sdmk/form', $data);
		
		
	}

	public function ajax_list()
	{
		$list = $this->jenis_sdmk->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $jenis_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$jenis_sdmk->id_jenis_sdmk.' ]';
			$row[] = strtoupper($jenis_sdmk->nama_jenis_sdmk);
			$row[] = $jenis_sdmk->nama_kelompok_sdmk;
			$row[] = $jenis_sdmk->nama_jenis_faskes;
			$row[] = $jenis_sdmk->parent;
			$row[] = $jenis_sdmk->counter;
			$row[] = $jenis_sdmk->is_standar;
			$row[] = ($jenis_sdmk->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $jenis_sdmk->updated_date ? Tanggal::formatDateTime($jenis_sdmk->updated_date) : Tanggal::formatDateTime($jenis_sdmk->created_date);

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($jenis_sdmk->id_jenis_sdmk,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_jenis_sdmk('."'".Regex::_genRegex($jenis_sdmk->id_jenis_sdmk,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->jenis_sdmk->count_all(),
						"recordsFiltered" => $this->jenis_sdmk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_jenis_sdmk = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'nama_jenis_sdmk' => Regex::_genRegex($this->input->post('nama_jenis_sdmk'), 'RGXQSL'),
			'id_kelompok_sdmk' => Regex::_genRegex($this->input->post('id_kelompok_sdmk'), 'RGXINT'),
			'id_jenis_faskes' => Regex::_genRegex($this->input->post('id_jenis_faskes'), 'RGXINT'),
			'parent' => Regex::_genRegex($this->input->post('id_jenis_sdmk'), 'RGXINT'),
			'counter' => Regex::_genRegex($this->input->post('counter'), 'RGXINT'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'is_standar' => Regex::_genRegex($this->input->post('is_standar'), 'RGXAZ'),
			'set_form' => Regex::_genRegex($this->input->post('set_form'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_jenis_sdmk == 0 ){
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->jenis_sdmk->save($dataexc);
			$last_id = $this->db->insert_id();
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->jenis_sdmk->update(array('id_jenis_sdmk'=>$id_jenis_sdmk), $dataexc);
			$last_id = $id_jenis_sdmk;
		}

		// insert standar sdmk
		if($this->input->post('is_standar') == 'Y'){
			
			$id_jenis_faskes = $this->input->post('id_jenis_faskes');

			$is_exist = $this->db->get_where('m_standar_rsk', array('id_jenis_sdmk'=>$last_id));
			// RSK //
			/*if( !in_array($id_jenis_faskes, array('1','3','4','29','30','31')) ){*/
			if( in_array($id_jenis_faskes, array('5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','32','33','34','35') ) ){

				if( $is_exist->num_rows() == 0 ){
					$this->db->insert('m_standar_rsk', array('id_jenis_sdmk'=>$last_id, 'id_jenis_faskes'=>$this->input->post('id_jenis_faskes')));
				}
			// RSU //
			}elseif ( in_array($id_jenis_faskes, array('3')) ) {
				if( $is_exist->num_rows() == 0 ){
					$this->db->insert('m_standar_rsu', array('id_jenis_sdmk'=>$last_id ));
				}
			}
			// KKP //
			elseif ( in_array($id_jenis_faskes, array('30')) ) {
				if( $is_exist->num_rows() == 0 ){
					$this->db->insert('m_standar_kkp', array('id_jenis_sdmk'=>$last_id ));
				}
			}
			// BTKLPP //
			elseif ( in_array($id_jenis_faskes, array('29')) ) {
				if( $is_exist->num_rows() == 0 ){
					$this->db->insert('m_standar_btklpp', array('id_jenis_sdmk'=>$last_id ));
				}
			}
			// KLINIK //
			elseif ( in_array($id_jenis_faskes, array('31')) ) {
				if( $is_exist->num_rows() == 0 ){
					$this->db->insert('m_standar_klinik', array('id_jenis_sdmk'=>$last_id ));
				}
			}

		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->jenis_sdmk->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
