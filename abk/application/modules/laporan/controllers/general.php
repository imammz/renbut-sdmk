<?php

class General extends MX_Controller {

    function __construct() {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('entry/faskes_model');
        
        unset($_SESSION['message']);
    }

    public function labkes() {
        
        $_SESSION['faskes']['class'] = 'labkes';
        $_SESSION['faskes']['nama'] = 'Laboratorium Kesehatan';
        $_SESSION['faskes']['kode'] = 10;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterlabkes($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }
    
    public function bpkm() {
        
        $_SESSION['faskes']['class'] = 'bpkm';
        $_SESSION['faskes']['nama'] = 'Balai Pengobatan / Kesehatan Masyarakat';
        $_SESSION['faskes']['kode'] = 8;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterBalaiKesehatan($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }
    public function btklp() {
        
        $_SESSION['faskes']['class'] = 'btklp';
        $_SESSION['faskes']['nama'] = 'Balai Teknis Kesehatan Lingkungan';
        $_SESSION['faskes']['kode'] = 3;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterbtkl($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }
    
     public function kkp() {
        
        $_SESSION['faskes']['class'] = 'kkp';
        $_SESSION['faskes']['nama'] = 'Kantor Kesehatan Pelabuhan';
        $_SESSION['faskes']['kode'] = 4;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterkkp($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }
    
    public function klinik() {
        
        $_SESSION['faskes']['class'] = 'klinik';
        $_SESSION['faskes']['nama'] = 'Klinik';
        $_SESSION['faskes']['kode'] = 9;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterKlinik($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }
    
    public function uptprov() {
        
        $_SESSION['faskes']['class'] = 'uptprov';
        $_SESSION['faskes']['nama'] = 'UPT Provinsi';
        $_SESSION['faskes']['kode'] = 97;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterUptdprov($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }
    
    public function uptd() {
        
        $_SESSION['faskes']['class'] = 'uptd';
        $_SESSION['faskes']['nama'] = 'UPT Daerah';
        $_SESSION['faskes']['kode'] = 96;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterUptdkab($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }
    
    public function upt() {
        
        $_SESSION['faskes']['class'] = 'upt';
        $_SESSION['faskes']['nama'] = 'UPT';
        $_SESSION['faskes']['kode'] = 98;
        
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = $_SESSION['faskes']['class'];

        $faskes_model = new faskes_model();
        $data['master_faskes'] = $faskes_model->_loadMasterUpt($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);


        
        $this->load->view('general_view', $data);
    }

    public function faskes_proses($faskes_kode, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('faskes_kode = ? AND tahun = ?', $faskes_kode, $tahun);
        $faskes_no_kab = $this->orm->faskes->select('no_kab')->where('faskes_kode = ? AND tahun = ?', $faskes_kode, $tahun);
        
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $this->orm->faskes->where('faskes_kode = ? AND tahun = ?', $faskes_kode, $tahun);
        $data['data_faskes'] = $data['faskes']->fetch();
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;

        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes_no_kab)->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $data['NO_KAB'] = $kab['NO_KAB'];
        $data['NAMA_KAB'] = $kab['NAMA_KAB'];
        $data['NO_PROV'] = $kab['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;

        $this->load->view('general_faskes_view', $data);
    }
    
     public function faskes_excel($faskes_kode, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('faskes_kode = ? AND tahun = ?', $faskes_kode, $tahun);
        $faskes_no_kab = $this->orm->faskes->select('no_kab')->where('faskes_kode = ? AND tahun = ?', $faskes_kode, $tahun);
        
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $this->orm->faskes->where('faskes_kode = ? AND tahun = ?', $faskes_kode, $tahun);
        $data['data_faskes'] = $data['faskes']->fetch();
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;

        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes_no_kab)->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $data['NO_KAB'] = $kab['NO_KAB'];
        $data['NAMA_KAB'] = $kab['NAMA_KAB'];
        $data['NO_PROV'] = $kab['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;

        $this->load->view('general_faskes_excel', $data);
    }
    
    
    public function kab_proses($NO_KAB, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ? AND tahun = ? AND ref_jenis_faskes_kode = ?', $NO_KAB, $tahun, $_SESSION['faskes']['kode']);
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;

        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $NO_KAB)->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $data['NO_KAB'] = $NO_KAB;
        $data['NAMA_KAB'] = $kab['NAMA_KAB'];
        $data['NO_PROV'] = $kab['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;

        $this->load->view('general_kab_view', $data);
    }
    
     public function kab_excel($NO_KAB, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ? AND tahun = ? AND ref_jenis_faskes_kode = ?', $NO_KAB, $tahun, $_SESSION['faskes']['kode']);
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;

        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $NO_KAB)->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $data['NO_KAB'] = $NO_KAB;
        $data['NAMA_KAB'] = $kab['NAMA_KAB'];
        $data['NO_PROV'] = $kab['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;

        $this->load->view('general_kab_excel', $data);
    }
    
    public function prov_proses($NO_PROV, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('LEFT(NO_KAB,2) = ? AND tahun = ? AND ref_jenis_faskes_kode = ?', $NO_PROV, $tahun,$_SESSION['faskes']['kode'])->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $kab = $this->orm->ref_kabupaten->where('NO_PROV', $NO_PROV);
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $NO_PROV)->fetch();
    
        $data['KAB'] = $kab;
        $data['NO_PROV'] = $prov['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;
        
        $this->load->view('general_prov_view',$data);
    }
    
     public function prov_excel($NO_PROV, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('LEFT(NO_KAB,2) = ? AND tahun = ? AND ref_jenis_faskes_kode = ?', $NO_PROV, $tahun,$_SESSION['faskes']['kode'])->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $kab = $this->orm->ref_kabupaten->where('NO_PROV', $NO_PROV);
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $NO_PROV)->fetch();
    
        $data['KAB'] = $kab;
        $data['NO_PROV'] = $prov['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;
        
        $this->load->view('general_prov_excel',$data);
    }
    
    
    public function nas_proses($tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('tahun = ? AND ref_jenis_faskes_kode = ?', $tahun,$_SESSION['faskes']['kode'])->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $prov = $this->orm->ref_provinsi();
        
        $data['PROV'] = $prov;
        $data['TAHUN'] = $tahun;
        
        $this->load->view('general_nas_view',$data);
    }
    
     public function nas_excel($tahun) {

       $faskes = $this->orm->faskes->select('faskes_kode')->where('tahun = ? AND ref_jenis_faskes_kode = ?', $tahun,$_SESSION['faskes']['kode'])->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $prov = $this->orm->ref_provinsi();
        
        $data['PROV'] = $prov;
        $data['TAHUN'] = $tahun;
        
        $this->load->view('general_nas_excel',$data);
    }

}

?>