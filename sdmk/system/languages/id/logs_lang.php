<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['error'] = 'error';
$lang['general'] = 'umum';
$lang['auth'] = 'auth'; 
$lang['logs'] = 'logs'; 
$lang['log_activity'] = 'aktivitas log'; 
$lang['read_on'] = 'Di baca pada';
$lang['by'] = 'oleh';
$lang['and_function'] = 'dan fungsi';
$lang['with_status'] = 'dengan status';
$lang['in_module'] = 'di modul';
$lang['success'] = 'sukses';
$lang['failed'] = 'gagal';
$lang['this_page_to_manage_website_logs,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola logs website, lihat dokumentasi untuk menggunakannya';