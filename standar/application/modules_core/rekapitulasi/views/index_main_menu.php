<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        <center><h2>Laporan Perencanaan Kebutuhan Minimal SDM Kesehatan</h2></center>
        <br>
        <div class="row">
            <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
            <div class="col-xs-6 col-sm-4 pricing-box">
              <div class="widget-box widget-color-dark">
                <div class="widget-header">
                  <h5 class="widget-title bigger lighter"><b>Rencana Kebutuhan SDMK Puskesmas</b></h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <ul class="list-unstyled spaced2">
                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK Se Kabupaten /Kota
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK Se Provinsi
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK Se Indonesia
                      </li>

                    </ul>

                    <hr />
                  </div>

                  <div>
                    <a href="#" onclick="getMenu('rekapitulasi/form_rekapitulasi/1')" class="btn btn-block btn-inverse">
                      <i class="ace-icon fa fa-shopping-cart bigger-110"></i>
                      <span>Lihat Laporan</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <?php }?>

                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','6')) ){?>
            <div class="col-xs-6 col-sm-4 pricing-box">
              <div class="widget-box widget-color-blue">
                <div class="widget-header">
                  <h5 class="widget-title bigger lighter"><b>Rencana Kebutuhan SDMK Rumah Sakit</b></h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <ul class="list-unstyled spaced2">
                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSU Se Kabupaten /Kota
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSU Se Provinsi
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSU Se Indonesia
                      </li>

                    </ul>

                    <hr />

                  </div>

                  <div>
                    <a href="#" onclick="getMenu('rekapitulasi/form_rekapitulasi/3')" class="btn btn-block btn-primary">
                      <i class="ace-icon fa fa-shopping-cart bigger-110"></i>
                      <span>Lihat Laporan</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <?php }?>
                 <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','7')) ){?>
            <div class="col-xs-6 col-sm-4 pricing-box">
              <div class="widget-box widget-color-green">
                <div class="widget-header">
                  <h5 class="widget-title bigger lighter"><b>Rencana Kebutuhan SDMK Rumah Sakit Khusus</b></h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <ul class="list-unstyled spaced2">
                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSK Se Kabupaten /Kota
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSK Se Provinsi
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSK Se Indonesia
                      </li>

                    </ul>

                    <hr />
                    
                  </div>

                  <div>
                    <a href="#" onclick="getMenu('construction')" class="btn btn-block btn-success">
                      <i class="ace-icon fa fa-shopping-cart bigger-110"></i>
                      <span>Lihat Laporan</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <?php }?>

          </div>
          <br>
          <div class="row">
            <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','8')) ){?>
            <div class="col-xs-6 col-sm-4 pricing-box">
              <div class="widget-box widget-color-red">
                <div class="widget-header">
                  <h5 class="widget-title bigger lighter"><b>KKP (Kantor Kesehatan Pelabuhan)</b></h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <ul class="list-unstyled spaced2">
                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK KKP Se Kabupaten /Kota
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK KKP Se Provinsi
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK KKP Se Indonesia
                      </li>

                    </ul>

                    <hr />
                  </div>

                  <div>
                    <a href="#" onclick="getMenu('rekapitulasi/form_rekapitulasi/30')" class="btn btn-block btn-danger">
                      <i class="ace-icon fa fa-shopping-cart bigger-110"></i>
                      <span>Lihat Laporan</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <?php }?>
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','9')) ){?>
            <!-- <div class="col-xs-6 col-sm-4 pricing-box">
              <div class="widget-box widget-color-grey">
                <div class="widget-header">
                  <h5 class="widget-title bigger lighter"><b>BTKL (Balai Teknis Kesehatan Lingkungan)</b></h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <ul class="list-unstyled spaced2">
                      <li>
                        <i class="ace-icon fa fa-check orange"></i>
                        Laporan Kebutuhan SDMK BTKL Se Kabupaten /Kota
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK BTKL Se Provinsi
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK BTKL Se Indonesia
                      </li>

                    </ul>

                    <hr />

                  </div>

                  <div>
                  
                    <a href="#" onclick="getMenu('rekapitulasi/form_rekapitulasi/29')" class="btn btn-block btn-grey">
                      <i class="ace-icon fa fa-shopping-cart bigger-110"></i>
                      <span>Lihat Laporan</span>
                    </a>
                  </div>
                </div>
              </div>
            </div> -->
            <?php }?>
                  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','10')) ){?>
            <div class="col-xs-6 col-sm-4 pricing-box">
              <div class="widget-box widget-color-orange">
                <div class="widget-header">
                  <h5 class="widget-title bigger lighter"><b>Klinik</b></h5>
                </div>

                <div class="widget-body">
                  <div class="widget-main">
                    <ul class="list-unstyled spaced2">
                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSK Se Kabupaten /Kota
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSK Se Provinsi
                      </li>

                      <li>
                        <i class="ace-icon fa fa-check green"></i>
                        Laporan Kebutuhan SDMK RSK Se Indonesia
                      </li>

                    </ul>

                    <hr />
                    
                  </div>

                  <div>
                    <a href="#" onclick="getMenu('rekapitulasi/form_rekapitulasi/31')" class="btn btn-block btn-warning">
                      <i class="ace-icon fa fa-shopping-cart bigger-110"></i>
                      <span>Lihat Laporan</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <?php }?>
            <!-- /section:pages/pricing.large -->
          </div>

      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/rekapitulasi.js'?>"></script>

