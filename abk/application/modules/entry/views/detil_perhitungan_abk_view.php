<html>    

    
    <head>
        <style>
            
            .CSSTableGenerator {
                margin:0px;padding:0px;
                width:100%;	box-shadow: 10px 10px 5px #888888;
                border:1px solid #000000;
            
                -moz-border-radius-bottomleft:0px;
                -webkit-border-bottom-left-radius:0px;
                border-bottom-left-radius:0px;

                -moz-border-radius-bottomright:0px;
                -webkit-border-bottom-right-radius:0px;
                border-bottom-right-radius:0px;

                -moz-border-radius-topright:0px;
                -webkit-border-top-right-radius:0px;
                border-top-right-radius:0px;

                -moz-border-radius-topleft:0px;
                -webkit-border-top-left-radius:0px;
                border-top-left-radius:0px;
            }.CSSTableGenerator table{
                width:100%;
                height:100%;
                margin:0px;padding:0px;
            }.CSSTableGenerator tr:last-child td:last-child {
                -moz-border-radius-bottomright:0px;
                -webkit-border-bottom-right-radius:0px;
                border-bottom-right-radius:0px;
            }
            .CSSTableGenerator table tr:first-child td:first-child {
                -moz-border-radius-topleft:0px;
                -webkit-border-top-left-radius:0px;
                border-top-left-radius:0px;
            }
            .CSSTableGenerator table tr:first-child td:last-child {
                -moz-border-radius-topright:0px;
                -webkit-border-top-right-radius:0px;
                border-top-right-radius:0px;
            }.CSSTableGenerator tr:last-child td:first-child{
                -moz-border-radius-bottomleft:0px;
                -webkit-border-bottom-left-radius:0px;
                border-bottom-left-radius:0px;
            }.CSSTableGenerator tr:hover td{

            }.CSSTableGenerator tr:nth-child(odd){ background-color:#e5e5e5; }
            .CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }
            .CSSTableGenerator td{
                vertical-align:middle;


                border:1px solid #000000;
                border-width:0px 1px 1px 0px;
                text-align:left;
                padding:7px;
                font-size:12px;
                font-family:arial;
                font-weight:normal;
                color:#000000;
            }.CSSTableGenerator tr:last-child td{
                border-width:0px 1px 0px 0px;
            }.CSSTableGenerator tr td:last-child{
                border-width:0px 0px 1px 0px;
            }.CSSTableGenerator tr:last-child td:last-child{
                border-width:0px 0px 0px 0px;
            }
            .CSSTableGenerator tr:first-child td{
                background:-o-linear-gradient(bottom, #4c4c4c 5%, #000000 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #000000) );	background:-moz-linear-gradient( center top, #4c4c4c 5%, #000000 100% );	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#000000");	background: -o-linear-gradient(top,#4c4c4c,000000);
                background-color:#4c4c4c;
                border:0px solid #000000;
                text-align:center;
                border-width:0px 0px 1px 1px;
                font-size:14px;
                font-family:arial;
                font-weight:bold;
                color:#ffffff;
            }
            .CSSTableGenerator tr:first-child:hover td{
                background:-o-linear-gradient(bottom, #4c4c4c 5%, #000000 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #000000) );	background:-moz-linear-gradient( center top, #4c4c4c 5%, #000000 100% );	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#000000");	background: -o-linear-gradient(top,#4c4c4c,000000);
                background-color:#4c4c4c;
            }
            .CSSTableGenerator tr:first-child td:first-child{
                border-width:0px 0px 1px 0px;
            }
            .CSSTableGenerator tr:first-child td:last-child{
                border-width:0px 0px 1px 1px;
            }
        </style>
    </head>

    <body>

          <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();


                echo "<h3><strong>{$sdmk_faskes_installasi['sdmk_faskes_installasi_nama']} - {$faskes_installasi['installasi_nama']}</strong></h3>";
                echo "<h4>{$faskes['faskes_nama']} - {$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
        
        <div style="text-align: center;">
            <table width="100%" class="CSSTableGenerator">

                <tr><th width="15">No</th><th> Tugas Pokok <br> (1)</th> <th width="100"> Capaian <br> (2)  </th> <th width="100"> SBK  <br> (3)</th> <th width="100"> Kebutuhan SDMK <br> (2)/(3) </th></tr>

                <?php
                $no = 1;
                $total_sdmk = 0;
                $jkt = 0;
                foreach ($sdmk_kbk as $row_sdmk_kbk) {

                    if($row_sdmk_kbk['SBK']!=0) {
                    $sdmk = $row_sdmk_kbk['capaian'] / $row_sdmk_kbk['SBK'];
                    }
                    else {
                        $sdmk = 0;
                    }

                    $jkt += $sdmk;

                    ?>

                <tr> <td> <?php echo $no ?> </td> <td > <?php echo $row_sdmk_kbk['kbk_uraian'] ?> </td> <td style="text-align: right;"> <?php echo $row_sdmk_kbk['capaian'] ?> </td> <td style="text-align: right;">  <?php echo $row_sdmk_kbk['SBK'] ?> </td> <td style="text-align: right;"> <?php echo number_format($sdmk, 2); ?>  </td> </tr>

                    <?php
                    $no++;
                }
                $abk_model = new abk_model;
                $stp = $abk_model->_fmlSTP($ftp['total']);
                $kebutuhan = $jkt * $stp;

                if ($kebutuhan <= 5) {
                    $pembulatan = Ceil($kebutuhan);
                } else {
                    $pembulatan = round($kebutuhan);
                }
                
                $terbilang = new Terbilang();
                
                ?>
                    <tr><td colspan="4"> <h4> JKT = Jumlah Kebutuhan Tenaga Tugas Pokok </h4> </td> <td style="text-align: right;"> <?php echo number_format($jkt, 2); ?> </td></tr>
                    <tr><td colspan="4"> <h4> STP = Standard Tugas Penunjang </h4> </td> <td style="text-align: right;"> <?php echo $stp; ?> </td></tr>
                    <tr><td colspan="4"> <h3> Total Kebutuhan SDMK = JKT x STP </h3> </td> <td style="text-align: right;"> <b> <?php echo number_format($kebutuhan,2); ?> </b> </td></tr>
                    <tr><td colspan="4"> <h2> Pembulatan Kebutuhan SDMK </h2> </td> <td style="text-align: right;"> <h3><b> <?php echo $pembulatan ?> </b> <br/> (<i> <?php echo $terbilang->generate($pembulatan) ?> </i>)</h3> </td></tr>

            </table>
            <br/>
            <div style="text-align: center"> 
                <p>
            <?php if($button) { ?>
                    <a style="text-decoration: none; background-color: #007aff; color: white;" href="javascript:void()" onclick="window.history.back()"> > Kembali Ke Hasil Perhitungan < </a>
            <?php } ?>
                </p>
            </div>
            
        </div>

    </body>
</html>