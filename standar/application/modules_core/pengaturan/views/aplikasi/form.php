<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
        <div class="row">
          <div class="col-sm-12">
            <div class="widget-box">
              <div class="widget-header">
                <h4 class="widget-title">Profile Application</h4>
              </div>

              <div class="widget-body">
                <div class="widget-main no-padding">

                  <form class="form-horizontal" method="post" id="form_app" action="<?php echo site_url('pengaturan/aplikasi/proses_app')?>">
                    <br>

                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Application Name </label>
                      <div class="col-sm-9">
                        <input type="text" id="app_name" name="app_name" value="<?php echo isset($value->app_name)?$value->app_name:''?>" class="col-xs-10 col-sm-10" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right"  for="form-field-1"> Header Title </label>
                      <div class="col-sm-9">
                        <input type="text" id="header_title" value="<?php echo isset($value->header_title)?$value->header_title:''?>" name="header_title" class="col-xs-10 col-sm-10" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right"  for="form-field-1"> Footer Text </label>
                      <div class="col-sm-9">
                        <input type="text" id="text_footer" class="col-xs-10 col-sm-10" value="<?php echo isset($value->footer_text)?$value->footer_text:''?>" name="text_footer" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right"  for="form-field-1"> Copyright </label>
                      <div class="col-sm-9">
                        <input type="text" id="copyright" class="col-xs-10 col-sm-10" value="<?php echo isset($value->copyright)?$value->copyright:''?>" name="copyright" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Logo/Icon App</label>
                      <div class="col-sm-9">
                        <input type="file" id="form-field-1" name="icon" style="border:0px solid black" />
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Description </label>
                      <div class="col-sm-9">
                        <textarea name="description" id="description" cols="50"><?php echo isset($value->app_description)?$value->app_description:''?></textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Author Name </label>
                      <div class="col-sm-9">
                        <input type="text" id="auth_name" value="<?php echo isset($value->author)?$value->author:''?>" name="auth_name" class="col-xs-10 col-sm-10" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Company Name </label>
                      <div class="col-sm-9">
                        <input type="text" id="company_name" value="<?php echo isset($value->company_name)?$value->company_name:''?>" name="company_name" class="col-xs-10 col-sm-10" />
                      </div>
                    </div>

                    <div class="form-actions center">
                      <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                        <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                        Submit
                      </button>
                    </div>

                  </form>

                </div>
              </div>
            </div>
          </div>

        </div>

    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/aplikasi.js'?>"></script>
