<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$lang['module_list'] = 'Daftar module';
$lang['get_more_modules'] = 'cari modul yang lain';
$lang['module_is_up_to_date'] = 'module terupdate';
$lang['no_configuration_inside_the_module'] = 'tidak ada konfigurasi dalam modul';
$lang['this_page_to_manage_website_modules,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola modul situs, lihat dokumentasi untuk menggunakannya';