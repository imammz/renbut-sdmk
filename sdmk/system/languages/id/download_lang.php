<?php if (!defined('BASEPATH')) exit('no direct script user allowed');
$lang['set_as_publish'] = 'atur sebagai publish';
$lang['set_as_draft'] = 'atur sebagai draft';
$lang['download_type'] = 'tipe halaman';
$lang['this_download_to_manage_download_content,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola konten halaman, lihat dokumentasi untuk menggunakannya';