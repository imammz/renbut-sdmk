<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		libraries/database
 * @author		xamzo Dev Team 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.5
 * @filesource
 */
class Db_active_rec extends Db_result_obj {

    public $query = null;  // define query object
    public $select = 'SELECT * ';  // define query select
    public $from = ' FROM ';    // define query from 
    public $table = '';
    public $ar_where = array();
    public $ar_between = array();
    public $ar_where_in = array();
    public $ar_limit = array();
    public $ar_group = array();
    public $ar_order = array();
    public $ar_join = array();
    public $ar_like = array();

    /**
     * 
     * @param type $query
     * @return \Db_active_rec
     */
    public function select($query = null) {
        if ($query != null) {
            $this->select = " SELECT $query";
            return $this;  // return as object
        }
    }

    /**
     * 
     * @param type $query
     * @return \Db_active_rec
     */
    public function from($query = null) {
        if ($query != null) {
            $this->from = " FROM $query";
            $this->table = $query;
            return $this; // return as object
        }
    }

    public function where($ar_wheres = null, $value = null, $sensitive = false) {
        $ar_where = '';

        if (is_array($ar_wheres)) {
            if (count($ar_wheres) > 0) {
                $w = 1;
                foreach ($ar_wheres as $key => $val) {
                    $val = addslashes($val);
                    if ($w > 1) {
                        $ar_where.=" AND ";
                    } else {
                        $ar_where.=" ";
                    }
                    if ($sensitive) {
                        $ar_where.="(LOWER(" . $key . "))" . " = '" . strtolower($val) . "'";
                    } else {
                        $ar_where.="$key = '" . strtolower($val) . "'";
                    }
                    $w++;
                }
                $this->ar_where[$this->table][] = " WHERE " . $ar_where;
            }
        } else {
            if (!is_array($ar_wheres) && $value != null) {
                $this->ar_where[$this->table][] = " WHERE (LOWER(" . $ar_wheres . "))" . " = '" . strtolower(addslashes($value)) . "'";
            } else {
                $this->ar_where[$this->table][] = " WHERE " . ($ar_wheres);
            }
        }
        return $this; // return as object
    }

    public function where_in($key = null, $ar_wheres_in = null, $sensitive = false) {
        $ar_where_in = '';
        $w = 1;
        if (is_array($ar_wheres_in)) {
            if (count($ar_wheres_in) > 0) {
                foreach ($ar_wheres_in as $val) {
                    $val = addslashes($val);
                    if ($w > 1) {
                        $ar_where_in.=",";
                    } else {
                        $ar_where_in.=" ";
                    }
                    $ar_where_in.=strtolower($val);
                    $w++;
                }
                if ($sensitive) {
                    $this->ar_where_in[$this->table] = "(LOWER(" . $key . ")) IN(" . addslashes($ar_where_in) . ")";
                } else {
                    $this->ar_where_in[$this->table] = "$key IN(" . addslashes($ar_where_in) . ")";
                }
            }
        }
        return $this; // return as object
    }

    public function where_between($key = null, $ar_wheres_beetween_1 = null, $ar_wheres_beetween_2 = null) {

        $this->ar_where_between[$this->table] = " $key BETWEEN '$ar_wheres_beetween_1' AND '$ar_wheres_beetween_2'";

        return $this; // return as object
    }

    public function like($ar_likes = null, $value = null) {
        $ar_like = '';
        $w = 1;
        if (is_array($ar_likes)) {
            if (count($ar_likes) > 0) {
                foreach ($ar_likes as $key => $val) {
                    $val = addslashes($val);
                    if ($w > 1) {
                        $ar_like.=" AND ";
                    } else {
                        $ar_like.=" ";
                    }
                    $ar_like.=" (LOWER(" . $key . "))" . " LIKE '" . strtolower($val) . "'";
                    $w++;
                }

                $this->ar_like[$this->table][] = "$ar_like";
            }
        } else {
            if (!is_array($ar_likes) && $value != null) {
                $this->ar_like[$this->table][] = " (LOWER(" . $ar_likes . "))" . " LIKE '" . strtolower(addslashes($value)) . "'";
            } else {
                $this->ar_like[$this->table][] = addslashes($ar_likes);
            }
        }
        return $this; // return as object
    }

    public function join($tbl = null, $where = null, $join = null) {
        if ($join == null) {
            $join = 'LEFT';
        } else {
            $join = strtoupper($join);
        }
        $this->ar_join[$this->table][] = " $join JOIN $tbl ON $where";
        return $this; // return as object
    }

    public function left_join($tbl = null, $where = null) {
        $this->ar_join[$this->table][] = " LEFT JOIN $tbl ON $where";
        return $this; // return as object
    }

    public function right_join($tbl = null, $where = null) {
        $this->ar_join[$this->table][] = " RIGHT JOIN $tbl ON $where";
        return $this; // return as object
    }

    public function inner_join($tbl = null, $where = null) {
        $this->ar_join[$this->table][] = " INNER JOIN $tbl ON $where";
        return $this; // return as object
    }

    public function group_by($ar_groups = null) {
        $ar_group = '';
        $g = 1;
        if (is_array($ar_groups)) {
            if (count($ar_groups) > 0) {
                foreach ($ar_groups as $key) {
                    if ($g > 1) {
                        $ar_group.=" AND ";
                    } else {
                        $ar_group.=" ";
                    }
                    $ar_group.="" . $key . "";
                    $g++;
                }
                $this->ar_group[$this->table] = " GROUP BY " . $ar_group;
            }
        } else {
            if (!is_array($ar_groups)) {
                $this->ar_group[$this->table] = " GROUP BY $ar_groups";
            }
        }
        return $this; // return as object
    }

    public function order_by($ar_orders = null, $value = null) {
        $ar_order = '';
        $o = 1;
        if (is_array($ar_orders)) {
            if (count($ar_orders) > 0) {
                foreach ($ar_orders as $key => $val) {
                    if ($o > 1) {
                        $ar_order.=" AND ";
                    } else {
                        $ar_order.=" ";
                    }
                    $ar_order.="$key " . strtoupper($val);
                    $o++;
                }
                $this->ar_order[$this->table] = " ORDER BY " . $ar_order;
            }
        } else {
            if (!is_array($ar_orders) && $value != null) {
                $this->ar_order[$this->table] = " ORDER BY $ar_orders " . strtoupper($value);
            } else {
                $this->ar_order[$this->table] = " ORDER BY $ar_orders";
            }
        }
        return $this; // return as object
    }

    public function limit($ar_limit, $offset = 0) {
        $this->ar_limit[$this->table] = " LIMIT {$offset}, {$ar_limit}";
        return $this; // return as object 
    }

    /**
     * 
     * @return \Db_active_rec
     */
    public function get($tbl = null) {
        if ($tbl != null) {
            $this->table = $tbl;
        }
        $ar_where = '';
        $ar_where_in = '';
        $ar_where_between = '';
        $ar_like = '';
        $ar_limit = '';
        $ar_order = '';
        $ar_group = '';
        $ar_join = '';
        if (isset($this->ar_where[$this->table])) {
            $i = 0;
            foreach ($this->ar_where[$this->table] as $where) {
                if ($i > 0) {
                    $where = str_replace(array('WHERE', 'where'), 'AND', $where);
                }
                $ar_where .= $where;
                $i++;
            }
        }
        if (isset($this->ar_where_in[$this->table])) {
            if (isset($this->ar_where[$this->table]) || isset($this->ar_where_between[$this->table])) {
                $ar_where_in = ' AND ' . $this->ar_where_in[$this->table];
            } else {
                $ar_where_in = ' WHERE ' . $this->ar_where_in[$this->table];
            }
        }
        if (isset($this->ar_where_between[$this->table])) {
            if (isset($this->ar_where[$this->table])) {
                $ar_where_between = ' AND ' . $this->ar_where_between[$this->table];
            } else {
                $ar_where_between = ' WHERE ' . $this->ar_where_between[$this->table];
            }
        }
        if (isset($this->ar_like[$this->table])) {
            if (isset($this->ar_where[$this->table]) || isset($this->ar_where_in[$this->table]) || isset($this->ar_where_between[$this->table])) {
                foreach ($this->ar_like[$this->table] as $like) {
                    $ar_like .= ' AND ' . $like;
                }
            } else {
                $nl = 0;
                foreach ($this->ar_like[$this->table] as $like) {
                    if ($nl == 0) {
                        $ar_like .= ' WHERE ' . $like;
                    } else {
                        $ar_like .= ' AND ' . $like;
                    }
                    $nl++;
                }
            }
        }
        if (isset($this->ar_limit[$this->table])) {
            $ar_limit = $this->ar_limit[$this->table];
        }
        if (isset($this->ar_group[$this->table])) {
            $ar_group = $this->ar_group[$this->table];
        }
        if (isset($this->ar_order[$this->table])) {
            $ar_order = $this->ar_order[$this->table];
        }
        if (isset($this->ar_join[$this->table])) {
            foreach ($this->ar_join[$this->table] as $join) {
                $ar_join .= $join;
            }
        }
        $in_condition[$this->table] = $ar_join . // add join table if exist
                $ar_where . // add where condition if exist       
                $ar_where_between . // and where beetween
                $ar_where_in . // add where_in condition if exist 
                $ar_like . // add like condition if exist 
                $ar_group . // add group condition if exist
                $ar_order . // add order condition if exist
                $ar_limit; // add limit condition if exist
        if ($tbl == null) {
            $this->query = $this->select . $this->from . $in_condition[$this->table];
        } else {
            $this->query = "SELECT * FROM $tbl" . $in_condition[$this->table];
        }
        $this->_unset();
        return $this; // return as object 
    }

    /**
     * 
     * @param type $query
     * @return \Db_active_rec
     */
    public function query($query = null) {
        if ($query != null) {
            $this->query = $query;
        }
        return $this;
    }

    public function _unset() {
        $data_set = array(
            'select' => 'SELECT * ',
            'table' => '',
            'from' => '',
            'ar_where' => '',
            'ar_group' => '',
            'ar_order' => '',
            'ar_limit' => '',
            'ar_join' => '',
            'ar_table' => '',
            'ar_like' => ''
        );
        foreach ($data_set as $key => $val) {
            unset($this->$key);
            $this->$key = $val;
        }
        unset($data_set);
    }

}

?>
