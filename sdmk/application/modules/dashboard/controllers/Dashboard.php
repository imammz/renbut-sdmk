<?php

if (!defined('BASEPATH')){
exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium application development framework and CMS for PHP 5 or newer
 *
 * @package		modules/dashboard
 * @author		xamzo developers 
 * @copyright           Copyright (c) 2014, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Dashboard extends X_Controller {

    public function __construct() {
        parent::__construct();
        $this->__composer('dashboard');
    }

    public function h() {
        if (is_login()) {
            $this->__template('private/index');
        } else {
            redirect('login');
        }
    }

    public function index() {
        if (is_login()) {
            $view['extensions'] = x_include_ext('dashboard', 'backend');
            $this->__view('dashboard', 'index', $view);
        } else {
            redirect('login');
        }
    }
 
}
