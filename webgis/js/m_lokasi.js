var nama = [];
var nama_provinsi = [];
var alamat = [];
var keterangan = [];
var total = [];
var lokasi = [];
var cords = '';
var peta;
var id = [];
var infoWindow;
var loading ='<div align="center"><img class="img-circle" src="../assets/images/ajax-loader.gif"></div>';
var iconPath  = "../icon/marker_1.png";
function peta_awal(){
var indonesia = new google.maps.LatLng(-3.5451241, 112.7293194);
var petaoption = {
zoom: 5,
center: indonesia,
mapTypeId: google.maps.MapTypeId.ROADMAP
};

peta = new google.maps.Map(document.getElementById("map-canvas"),petaoption);

url = "get_data.php";
$.ajax({
url: url,
dataType: 'json',
cache: false,
success: function(msg){
var polygon;
var cords = [];
for(i=0;i<msg.indonesia.lahan.length;i++){
id[i] = msg.indonesia.lahan[i].id;
nama_provinsi[i] = msg.indonesia.lahan[i].nama_provinsi;
total[i] = msg.indonesia.lahan[i].total;
keterangan[i] = msg.indonesia.lahan[i].keterangan;
lokasi[i] = msg.indonesia.lahan[i].polygon;

var str = lokasi[i].split(" "); 

for (var j=0; j < str.length; j++) { 
var point = str[j].split(",");
cords.push(new google.maps.LatLng(parseFloat(point[0]), parseFloat(point[1])));
}

var contentString = '<b>'+nama_provinsi[i]+'</b><br>' +
'Keterangan: '+ keterangan[i] +
'<br>' +
'Total : '+ total[i] +
'<br>';

polygon = new google.maps.Polygon({
paths: [cords],
strokeColor: msg.indonesia.lahan[i].warna,
strokeOpacity: 0,
strokeWeight: 1,
fillColor: msg.indonesia.lahan[i].warna,
fillOpacity: 0.5,
html: contentString,
id  :id[i],
nama_provinsi: nama_provinsi[i]
});     

cords = []; 
polygon.setMap(peta); 

var lat = parseFloat(msg.indonesia.lahan[i].lat);
console.log(cords);
var lng  =parseFloat(msg.indonesia.lahan[i].lng);
var myLatLng = {lat:lat, lng:lng};

$.get("get_marker.php", function (data) {
$(data).find("marker").each(function () {
var name = $(this).attr('name');
var address = $(this).attr('address');
var type = $(this).attr('type');
var point = new google.maps.LatLng(parseFloat($(this).attr('lat')), parseFloat($(this).attr('lng')));
var info1 = $(this).attr('info1');
var info2 = $(this).attr('info2');
var info3 = $(this).attr('info3');
var info4 = $(this).attr('info4');
create_marker2(point, name, address, false, false, false,iconPath,info1,info2,info3,info4, peta);
});
});	


google.maps.event.addListener(polygon, 'click', function(event) {
id_provinsi = this.id;
var EditForm = '<p style="width:400px"><div class="marker-edit">'+
'<form action="ajax-save.php" method="POST" name="SaveMarker" id="SaveMarker">'+
'<label for="pName"><span>Place Name :</span><input type="text" name="pName" class="save-name" placeholder="Enter Title" maxlength="40" /></label>'+
'<label for="pDesc"><span>Description :</span><textarea name="pDesc" class="save-desc" placeholder="Enter Address" maxlength="150"></textarea></label>'+
'<label for="pInfo1"><span>Info 1 :</span><textarea name="pInfo1" class="save-info1" placeholder="Enter Address" maxlength="150"></textarea></label>'+
'<label for="pInfo2"><span>Info 2 :</span><textarea name="pInfo2" class="save-info2" placeholder="Enter Address" maxlength="150"></textarea></label>'+
'<label for="pInfo3"><span>Info 3 :</span><textarea name="pInfo3" class="save-info3" placeholder="Enter Address" maxlength="150"></textarea></label>'+
'<label for="pInfo4"><span>Info 4 :</span><textarea name="pInfo4" class="save-info4" placeholder="Enter Address" maxlength="150"></textarea></label>'+
'</form>'+
'</div></p><button name="save-marker" class="save-marker">Save Marker Details</button>';
if(id_provinsi==''){
alert('Create Marker harus didalam Polygon(Provinsi)');
}else{
create_marker(event.latLng, 'Create New Location', EditForm, true, true, true, iconPath,id_provinsi);
}
});
}               
}
});

}

function create_marker(MapPos, MapTitle, MapDesc, InfoOpenDefault, DragAble, Removable, iconPath,id_provinsi) {
console.log(id_provinsi);
var marker = new google.maps.Marker({
position: MapPos,
map: peta,
draggable: DragAble,
animation: google.maps.Animation.DROP,
title: "Hello World!",
icon: iconPath
});

var contentString = $('<div class="marker-info-win" style="width:400px">' +
'<div class="marker-inner-win"><span class="info-content">' +
'<h1 class="marker-heading">' + MapTitle + '</h1>' +
MapDesc +
'</span><button name="remove-marker" class="remove-marker" title="Remove Marker">Remove Marker</button>' +
'</div></div>');


//Create an infoWindow
var infowindow = new google.maps.InfoWindow();
//set the content of infoWindow
infowindow.setContent(contentString[0]);


//Find remove button in infoWindow
var removeBtn = contentString.find('button.remove-marker')[0];
var saveBtn = contentString.find('button.save-marker')[0];

//add click listner to remove marker button
google.maps.event.addDomListener(removeBtn, "click", function(event) {
remove_marker(marker);
});

if (typeof saveBtn !== 'undefined') //continue only when save button is present
{
//add click listner to save marker button
google.maps.event.addDomListener(saveBtn, "click", function(event) {
var mReplace = contentString.find('span.info-content'); //html to be replaced after success
var mName = contentString.find('input.save-name')[0].value; //name input field value
var mDesc = contentString.find('textarea.save-desc')[0].value; //description input field value
var mInfo1 = contentString.find('textarea.save-info1')[0].value; //type of marker
var mInfo2 = contentString.find('textarea.save-info2')[0].value; //type of marker
var mInfo3 = contentString.find('textarea.save-info3')[0].value; //type of marker
var mInfo4 = contentString.find('textarea.save-info4')[0].value; //type of marker

if (mName == '' || mDesc == ''||mInfo1 == '' || mInfo2 == ''||mInfo3 == '' || mInfo4 == '') {
alert("Please enter the value!");
} else {
save_marker(marker, mName, mDesc,mInfo1,mInfo2,mInfo3,mInfo4, mReplace,id_provinsi); //call save marker function
}
});
}

//add click listner to save marker button		 
google.maps.event.addListener(marker, 'click', function() {
infowindow.open(peta, marker); // click on marker opens info window 
});

if (InfoOpenDefault) //whether info window should be open by default
{
infowindow.open(peta, marker);
}
}

function save_marker(Marker, mName, mAddress, mInfo1,mInfo2,mInfo3,mInfo4,replaceWin,id_provinsi) {
var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
//alert(Marker.getPosition().lat());

var myData = {
name: mName,
address: mAddress,
latlang: mLatLang,
info1 : mInfo1,
info2 : mInfo2,
info3 : mInfo3,
info4 : mInfo4,
id_provinsi:id_provinsi
}; //post variables
console.log(replaceWin);
$.ajax({
type: "POST",
url: "get_marker.php",
data: myData,
success: function(data) {
replaceWin.html(data); //replace info window with new html
Marker.setDraggable(false); //set marker to fixed
Marker.setIcon(iconPath); //replace icon
},
error: function(xhr, ajaxOptions, thrownError) {
alert(thrownError); //throw any errors
}
});
}

function remove_marker(Marker) {
if (Marker.getDraggable()) {
Marker.setMap(null); //just remove new marker
} else {
var mLatLang = Marker.getPosition().toUrlValue(); //get marker position
var myData = {
del: 'true',
latlang: mLatLang
}; //post variables
$.ajax({
type: "POST",
url: "get_marker.php",
data: myData,
success: function(data) {
Marker.setMap(null);
// alert(data);
},
error: function(xhr, ajaxOptions, thrownError) {
alert(thrownError); //throw any errors
}
});
}

}

function create_marker2(MapPos, MapTitle, address, InfoOpenDefault, DragAble, Removable,iconPath,info1,info2,info3,info4, map) {
console.log(MapPos);
//new marker
var marker = new google.maps.Marker({
position: MapPos,
map: map,
draggable: DragAble,
animation: google.maps.Animation.DROP,
title: "Hello World!",
icon: iconPath
});


var contentString = '<div class="info-window">' +
'<h3>' + MapTitle + '</h3>' +
'<div class="info-content">' +
'<table class="table" style="">' +
'<tr>' +
'<th>Address</th>'+

'<td>'+address+'</td>' +
'</tr>' +
'<tr>' +
'<th>Info1</th>' +

'<td>'+info1+'</td>' +
'</tr>' +
'<tr>' +
'<th>Info2</th>' +

'<td>'+info2+'</td>' +
'</tr>' +
'<tr>' +
'<th>Info3</th>' +

'<td>'+info3+'</td>' +
'</tr>' +
'<tr>' +
'<th>Info4</th>' +

'<td>'+info4+'</td>' +
'</tr>' +
'</table>' +
'</div>' +
'</div>';

var infowindow = new google.maps.InfoWindow({
content: contentString
});


//add click listner to save marker button		 
google.maps.event.addListener(marker, 'click', function() {
infowindow.open(map, marker); // click on marker opens info window 
});

if (InfoOpenDefault) {
infowindow.open(map, marker);
}
}
