<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        
        <br>
        <div class="row">

            <div class="col-xs-12 col-sm-12 pricing-box">

                <div class="col-sm-12">
                  <h3 class="header smaller lighter red"><center>Perencanaan Kebutuhan Minimal SDM Kesehatan Rumah Sakit Khusus</center></h3>
                  <?php foreach ($rsk->result() as $row_rsk){?>
                  <div class="col-sm-3">
                    <center>
                      <a href="#" onclick="get_modul_rsk('<?php echo $row_rsk->id_jenis_rsk?>')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="ace-icon fa fa-hospital-o bigger-250"></i></h2>
                          <?php echo 'ID.0'.$row_rsk->id_jenis_rsk.' &nbsp;'.$row_rsk->nama_jenis_rsk?>
                      </div>
                      </a>
                    </center>
                  </div>
                  <?php }?>

                </div><!-- /.col -->

            </div>

            <!-- /section:pages/pricing.large -->
          </div>

      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/prc_rsk.js'?>"></script>
