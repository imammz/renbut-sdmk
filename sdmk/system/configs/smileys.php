<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		configs/smileys
 * @author		xamzo Dev Team 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
$icon = array(
    ':angry:' => 'angry.gif',
    ':bigsurprise:' => 'bigsurprise.gif',
    ':blank:' => 'blank.gif',
    ':cheese:' => 'cheese.gif',
    ':confused:' => 'confused.gif',
    ':downer:' => 'downer.gif',
    ':embarrassed:' => 'embarrassed.gif',
    ':exclaim:' => 'exclaim.gif',
    ':grin:' => 'grin.gif',
    ':grrr:' => 'grrr.gif',
    ':gulp:' => 'gulp.gif',
    ':hmm:' => 'hmm.gif',
    ':kiss:' => 'kiss.gif',
    ':lol:' => 'lol.gif',
    ':longface:' => 'longface.gif',
    ':mad:' => 'mad.gif',
    ':ohh:' => 'ohh.gif',
    ':ohoh:' => 'ohoh.gif',
    ':question:' => 'question.gif',
    ':raspberry:' => 'raspberry.gif',
    ':rolleyes:' => 'rolleyes.gif',
    ':shade_cheese:' => 'shade_cheese.gif',
    ':shade_grin:' => 'shade_grin.gif',
    ':shade_hmm:' => 'shade_hmm.gif',
    ':shade_mad:' => 'shade_mad.gif',
    ':shade_smile:' => 'shade_smile.gif',
    ':shade_smirk:' => 'shade_smirk.gif',
    ':shock:' => 'shock.gif',
    ':shuteye:' => 'shuteye.gif',
    ':sick:' => 'sick.gif',
    ':smile:' => 'smile.gif',
    ':smirk:' => 'smirk.gif',
    ':snake:' => 'snake.gif',
    ':surprise:' => 'surprise.gif',
    ':tongue_laugh:' => 'tongue_laugh.gif',
    ':tongue_rolleye:' => 'tongue_rolleye.gif',
    ':tongue_wink:' => 'tongue_wink.gif',
    ':vampire:' => 'vampire.gif',
    ':wink:' => 'wink.gif',
    ':zip:' => 'zip.gif'
);