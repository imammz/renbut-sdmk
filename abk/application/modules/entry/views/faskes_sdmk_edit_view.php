<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>
    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1200px;">
        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
                echo "<h2>Langkah 1 - Menetapkan Jenis SDMK</h2>";
                echo "<h3>{$faskes_installasi['installasi_nama']} - {$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
                <hr/>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/editSDMK_proses/<?php echo $sdmk_faskes_installasi['sdmk_faskes_installasi_id'] ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ref_jenis_sdmk_kode">
                            Jenis SDMK
                        </label>
                        <div class="col-sm-10">
                            <select class="js-example-basic-single js-states form-control"disabled="disabled">
                              
                                <?php foreach ($this->orm->ref_jenis_sdmk->order('ref_jenis_sdmk_kode') as $row) { 
                                    $selected = ($sdmk_faskes_installasi['ref_jenis_sdmk_kode']==$row['ref_jenis_sdmk_kode'])?'selected':'';
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $row['ref_jenis_sdmk_kode'].'#'.$row['ref_jenis_sdmk_nama_level2'] ?>"> <?php echo $row['ref_jenis_sdmk_nama_level2'] ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_PNS">
                            Jumlah PNS
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah PNS" id="jumlah_PNS" name="jumlah_PNS" value="<?php echo $sdmk_faskes_installasi['jumlah_PNS']  ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_PPPK">
                            Jumlah PPPK
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah PPPK" id="jumlah_PPPK" name="jumlah_PPPK" value="<?php echo $sdmk_faskes_installasi['jumlah_PPPK']  ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_PTT">
                            Jumlah PTT
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah PTT" id="jumlah_PTT" name="jumlah_PTT" value="<?php echo $sdmk_faskes_installasi['jumlah_PTT']  ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_HD">
                            Jumlah Honor Daerah
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah HD" id="jumlah_HD" name="jumlah_HD" value="<?php echo $sdmk_faskes_installasi['jumlah_HD']  ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_DLL">
                            Jumlah Yang Lainnya
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah DLL" id="jumlah_DLL" name="jumlah_DLL" value="<?php echo $sdmk_faskes_installasi['jumlah_DLL']  ?>" class="form-control">
                        </div>
                    </div>                   
                    <div class="form-group">
                        <div class="col-sm-6"> 
                        </div>
                        <div class="col-sm-6" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Selanjutnya >> </button>
                        &nbsp;
                        </div>
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });

                            function genKode() {

                                var selected = document.getElementById('faskes_kode');
                                var faskes_kode = selected.options[selected.selectedIndex].value;

                                var url = '<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/getData/';

                                $.post(url,
                                        {
                                            faskes_kode: faskes_kode
                                        },
                                function (data) {

                                    if (data.result) {

                                            $('#faskes_kode').val(data.faskes_kode);
                                            $('#NO_KAB option[value=' + data.NO_KAB + ']').attr('selected', 'selected');
                                            $('#faskes_nama').val(data.faskes_nama);
                                            $('#faskes_telp').val(data.faskes_telp);
                                            $('#faskes_alamat').val(data.faskes_alamat);
                                            $('#faskes_email').val(data.faskes_email);
                                            $('#ref_faskes_kelas_id option[value=' + data.ref_faskes_kelas_id + ']').attr('selected', 'selected');
                                            $('#ref_jenis_faskes_kode option[value=' + data.ref_jenis_faskes_kode + ']').attr('selected', 'selected');
                                            $('#status_pengelolaan_id option[value=' + data.status_pengelolaan_id + ']').attr('selected', 'selected');
                                        
                                    }
                                    else {
                                        alert('Data <?php echo $subtitle ?> Tidak Tersedia');
                                    }

                                }, "json");


                            }

    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
