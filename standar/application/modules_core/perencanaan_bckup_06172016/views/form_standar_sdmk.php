<div class="row">
          <div class="col-xs-12">

              <center><h3>Perencanaan Kebutuhan SDM Kesehatan</h3></center>
              <br>
                 
              <table class="table table-striped table-bordered table-hover">
                 <thead>
                  <tr>  
                    <th class="center" rowspan="2" style="width: 50px">&nbsp;</th>
                    <th class="center" rowspan="2">Jenis SDMK</th>
                    <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                    <th class="center" rowspan="2">SDMK Standar</th>
                    <th class="center" rowspan="2">Kesenjangan<br>( 9 ) - ( 10 )</th>
                  </tr>

                  <tr>
                    <th class="center">PNS/<br>Pegawai Tetap</th>
                    <th class="center">PPPK</th>
                    <th class="center">PTT</th>
                    <th class="center">Honorer/<br>Kontrak</th>
                    <th class="center">BLUD</th>
                    <th class="center">TKS</th>
                    <th class="center">Total</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <?php for($i=1;$i<12;$i++){?>
                    <td class="center">( <?php echo $i?> )</td>
                    <?php }?>
                  </tr>

                  <?php 
                      $no = 1;

                      $jenis_sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$value->id_perencanaan,'id_jenis_faskes'=>$id_jenis_faskes, 'active'=>'Y', 'kategori_kelas'=>$value->kategori_kelas, 'tahun'=>$value->tahun)); 

                      //echo '<pre>';print_r($this->db->last_query());die;
                      //echo '<pre>'; print_r($jenis_sdmk);
                      foreach($jenis_sdmk as $key=>$row_jenis_sdmk){
                        
                      // jika jenis sdmk memiliki sub, maka form input disabled
                      $readonly = ( count($row_jenis_sdmk->sub_parent) == 0) ? '' : 'readonly' ;
                      
                      // jika jenis sdmk tidak memiliki standar minimal maka diblok warna merah //
                      $label = ($row_jenis_sdmk->is_standar == 'N') ? 'style="background-color:#F8E0E0;"' : '' ;
                      $jml_standar = ($row_jenis_sdmk->jml_standar_form != NULL) ? $row_jenis_sdmk->jml_standar_form : $row_jenis_sdmk->jml_standar;

                    
                  ?>
                      <tr <?php echo isset($label)?$label:''?>>
                        <td class="center"> <?php echo ''.$no.'.'?> </td>
                        <td> <?php echo $row_jenis_sdmk->nama_jenis_sdmk?> <input type="hidden" name="id_jenis_sdmk[]" value="<?php echo $row_jenis_sdmk->id_jenis_sdmk?>"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_pns)?$row_jenis_sdmk->jml_pns:0?>" id="pns<?php echo $key?>" name="pns[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px" > </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_pppk)?$row_jenis_sdmk->jml_pppk:0?>" id="pppk<?php echo $key?>" name="pppk[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_ptt)?$row_jenis_sdmk->jml_ptt:0?>" id="ptt<?php echo $key?>" name="ptt[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_honorer)?$row_jenis_sdmk->jml_honorer:0?>" id="honorer<?php echo $key?>" name="honorer[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_blud)?$row_jenis_sdmk->jml_blud:0?>" id="blud<?php echo $key?>" name="blud[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->jml_tks)?$row_jenis_sdmk->jml_tks:0?>" id="tks<?php echo $key?>" name="tks[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px"> </td>
                        <td class="center"> <input type="text" <?php echo isset($readonly)?$readonly:'';?> value="<?php echo isset($row_jenis_sdmk->total_jml)?$row_jenis_sdmk->total_jml:0?>" id="total<?php echo $key?>" name="total[]" onKeyUp="myFunction(<?php echo $key?>)" style="width:50px" readonly> </td>
                        <td class="center"> <input type="hidden" value="<?php echo $jml_standar?>" name="jml_standar[]"> <?php echo $jml_standar?> </td>
                        <td class="center">
                          <?php echo Apps::counting('-', $row_jenis_sdmk->total_jml, $jml_standar)?>
                        </td>
                      </tr>
                        <?php 
                          $alp = 'a'; 
                          foreach($row_jenis_sdmk->sub_parent as $key2=>$row_sub_parent){
                          $label2 = ($row_sub_parent->is_standar == 'N') ? 'style="background-color:#F8E0E0;"' : '' ;
                        ?>
                        <tr <?php echo isset($label2)?$label2:''?>>
                          <td> &nbsp; </td>
                          <td> <?php echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$alp.'.&nbsp; '.$row_sub_parent->nama_jenis_sdmk.''?> <input type="hidden" name="id_jenis_sdmk_sub[]" value="<?php echo $row_sub_parent->id_jenis_sdmk?>"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_pns)?$row_sub_parent->jml_pns:0?>" id="pns_sub<?php echo $key2?>" name="pns_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_pppk)?$row_sub_parent->jml_pppk:0?>" id="pppk_sub<?php echo $key2?>" name="pppk_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_ptt)?$row_sub_parent->jml_ptt:0?>" id="ptt_sub<?php echo $key2?>" name="ptt_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_honorer)?$row_sub_parent->jml_honorer:0?>" id="honorer_sub<?php echo $key2?>" name="honorer_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_blud)?$row_sub_parent->jml_blud:0?>" id="blud_sub<?php echo $key2?>" name="blud_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->jml_tks)?$row_sub_parent->jml_tks:0?>" id="tks_sub<?php echo $key2?>" name="tks_sub[]" onKeyUp="myFunction(<?php echo $key2?>)" style="width:50px"> </td>
                          <td class="center"> <input type="text" value="<?php echo isset($row_sub_parent->total_jml)?$row_sub_parent->total_jml:0?>" id="total_sub<?php echo $key2?>" name="total_sub[]" style="width:50px" readonly> </td>
                          <td class="center"> <input type="hidden" value="<?php echo $jml_standar?>" name="jml_standar_sub[]"> <?php echo $jml_standar?> </td>
                          <td class="center"> 
                            <?php echo Apps::counting('-', $row_sub_parent->total_jml, $jml_standar)?>
                           </td>
                        </tr>
                        <?php $alp++; }?>
                  <?php $no++;}?>

                  <!-- <tr>
                    <td class="center" colspan="2"><b>TOTAL</b></td>
                    <?php for($i=0;$i<8;$i++){?>
                      <td class="center" id="totalpns<?php echo $i?>">  </td>
                    <?php }?>
                  </tr> -->

                </tbody>
              </table>

          </div>
        </div>