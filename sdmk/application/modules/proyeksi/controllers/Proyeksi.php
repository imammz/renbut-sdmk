<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/proyeksi
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource 
 */
class Proyeksi extends X_Controller {

    public $limit = 25;
    public $user_id = null;

    public function __construct() {
        parent::__construct();
        $this->__composer('proyeksi');
        $this->user_id = $this->session->get_data('session_user_id');
    }

    public function index($offset = null) {
        if (true) {
            if (is_auth('proyeksi') && is_login()) {
                $limit = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : $this->limit;
                $view['offset'] = ($offset == null) ? 0 : $offset;
                $view['rs'] = $this->model_proyeksi->get_all($view['offset'], $limit);
                $view['total'] = $this->model_proyeksi->num_rows();

                $view['opt_limit'] = array();
                for ($i = 5; $i < 30; $i++) {
                    if ($i % 5 == 0) {
                        $view['opt_limit'][$i] = $i;
                    }
                }

                $propinsi = $this->model_proyeksi->get_propinsi();
                $view['propinsi'][''] = '-pilih-';
                foreach ($propinsi as $row) {
                    $view['propinsi'][$row->no_prov] = $row->no_prov . '-' . $row->nama_prov;
                }

                $jenis_sdmk = $this->model_proyeksi->get_jenis_sdmk();
                $view['jenis_sdmk'][''] = '-select-';
                foreach ($jenis_sdmk as $row) {
                    $view['jenis_sdmk'][$row->no_jenis_sdmk] = $row->no_jenis_sdmk . '-' . $row->nama_jenis_sdmk;
                }

                $config['base_url'] = site_url('index');
                $config['total_rows'] = $view['total'];
                $config['per_page'] = $limit;
                $config['div'] = 'div.xamzo-right';
                $config['form'] = '#crud-data';
                $config['uri_segment'] = $view['offset'];
                $this->pagination->initialize($config);
                $view['paging'] = $this->pagination->crawler_links();


                $view['order'] = $this->request->post('order');
                $view['by'] = $this->request->post('by');
                $view['search'] = $this->request->post('search');
                $view['s_no_prov'] = $this->request->post('s_no_prov');
                $view['s_no_jenis_sdmk'] = $this->request->post('s_no_jenis_sdmk');
                $this->__view('proyeksi', 'index', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function create() {
        if (is_ajax_request()) {
            if (is_auth('proyeksi', 'w')) {
                $this->validator->addRule('no_jenis_sdmk', array('require'));
                $this->validator->addRule('no_prov', array('require'));
                $this->validator->addRule('ratio', array('require'));
                $this->validator->addRule('pengangkatan_baru', array('require'));
                $this->validator->addRule('pindah_masuk', array('require'));
                $this->validator->addRule('pensiun', array('require'));
                $this->validator->addRule('meninggal_tdkmampu_cacat', array('require'));
                $this->validator->addRule('keluar_cuti_dipecat', array('require'));

                $view['action'] = 'proyeksi/create';

                $propinsi = $this->model_proyeksi->get_propinsi();
                $view['propinsi'][''] = '-select-';
                foreach ($propinsi as $row) {
                    $view['propinsi'][$row->no_prov] = $row->no_prov . '-' . $row->nama_prov;
                }

                $jenis_sdmk = $this->model_proyeksi->get_jenis_sdmk();
                $view['jenis_sdmk'][''] = '-select-';
                foreach ($jenis_sdmk as $row) {
                    $view['jenis_sdmk'][$row->no_jenis_sdmk] = $row->no_jenis_sdmk . '-' . $row->nama_jenis_sdmk;
                }

                $view['ref_proyeksi'] = $this->model_proyeksi->get_proyeksi();

                $data = array(
                    'id_proyeksi' => time(),
                    'no_jenis_sdmk' => $this->request->post('no_jenis_sdmk'),
                    'no_prov' => $this->request->post('no_prov'),
                    'ratio' => $this->request->post('ratio'),
                    'pengangkatan_baru' => $this->request->post('pengangkatan_baru'),
                    'pindah_masuk' => $this->request->post('pindah_masuk'),
                    'pensiun' => $this->request->post('pensiun'),
                    'meninggal_tdkmampu_cacat' => $this->request->post('meninggal_tdkmampu_cacat'),
                    'keluar_cuti_dipecat' => $this->request->post('keluar_cuti_dipecat')
                );


                $this->validator->setData($data);
                if ($this->request->post('emitter') != '') {
                    if ($this->validator->isValid()) {
                        $th_2014 = $_POST['th_2014'];
                        $th_2015 = $_POST['th_2015'];
                        $th_2016 = $_POST['th_2016'];
                        $th_2017 = $_POST['th_2017'];
                        $th_2018 = $_POST['th_2018'];
                        $th_2019 = $_POST['th_2019'];
                        $th_2020 = $_POST['th_2020'];
                        $th_2021 = $_POST['th_2021'];
                        $th_2022 = $_POST['th_2022'];
                        $th_2023 = $_POST['th_2023'];
                        $th_2024 = $_POST['th_2024'];
                        $th_2025 = $_POST['th_2025'];
                        $this->model_proyeksi->insert($data);
                        for ($i = 1; $i < 14; $i++) {
                            $this->model_proyeksi->insert(array(
                                'id_proyeksi' => $data['id_proyeksi'],
                                'kode_ref_proyeksi' => $i,
                                'th_2014' => $th_2014[$i],
                                'th_2015' => $th_2015[$i],
                                'th_2016' => $th_2016[$i],
                                'th_2017' => $th_2017[$i],
                                'th_2018' => $th_2018[$i],
                                'th_2019' => $th_2019[$i],
                                'th_2020' => $th_2020[$i],
                                'th_2021' => $th_2021[$i],
                                'th_2022' => $th_2022[$i],
                                'th_2023' => $th_2023[$i],
                                'th_2024' => $th_2024[$i],
                                'th_2025' => $th_2025[$i]
                                    ), $this->model_proyeksi->tbl_tahun_proyeksi);
                        }
                    } else {
                        foreach ($this->validator->getErrors() as $field => $messages) {
                            $view['errors'][$field] = $messages['0'];
                        }
                        $this->__view('proyeksi', 'form', $view);
                    }
                } else {
                    $this->__view('proyeksi', 'form', $view);
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function update() {
        if (is_ajax_request()) {
            if (is_auth('proyeksi', 'w')) {
                $no_jenis_sdmk = $this->request->post('no_jenis_sdmk');
                $no_prov = $this->request->post('no_prov');
                $ratio = $this->request->post('ratio'); 
                $pengangkatan_baru = $this->request->post('pengangkatan_baru');
                $pindah_masuk = $this->request->post('pindah_masuk');
                $pensiun = $this->request->post('pensiun');
                $meninggal_tdkmampu_cacat = $this->request->post('meninggal_tdkmampu_cacat');
                $keluar_cuti_dipecat = $this->request->post('keluar_cuti_dipecat');
                $view['action'] = 'proyeksi/update';
                $this->validator->addRule('no_jenis_sdmk', array('require'));
                $this->validator->addRule('no_prov', array('require'));
                $this->validator->addRule('ratio', array('require')); 
                $this->validator->addRule('pengangkatan_baru', array('require'));
                $this->validator->addRule('pindah_masuk', array('require'));
                $this->validator->addRule('pensiun', array('require'));
                $this->validator->addRule('meninggal_tdkmampu_cacat', array('require'));
                $this->validator->addRule('keluar_cuti_dipecat', array('require'));

                $view['ref_proyeksi'] = $this->model_proyeksi->get_proyeksi();

                $propinsi = $this->model_proyeksi->get_propinsi();
                $view['propinsi'][''] = '-select-';
                foreach ($propinsi as $row) {
                    $view['propinsi'][$row->no_prov] = $row->no_prov . '-' . $row->nama_prov;
                }

                $jenis_sdmk = $this->model_proyeksi->get_jenis_sdmk();
                $view['jenis_sdmk'][''] = '-select-';
                foreach ($jenis_sdmk as $row) {
                    $view['jenis_sdmk'][$row->no_jenis_sdmk] = $row->no_jenis_sdmk . '-' . $row->nama_jenis_sdmk;
                }

                $id = $this->request->post('id', false);
                if ($this->request->post('emitter') != '') {
                    $view['id'] = $id;
                } else {
                    $view['id'] = $id['0'];
                }
                $row = $this->model_proyeksi->get_where_id($view['id']);
                $view['default']['no_jenis_sdmk'] = ($no_jenis_sdmk) ? $no_jenis_sdmk : $row->no_jenis_sdmk;
                $view['default']['no_prov'] = ($no_prov) ? $no_prov : $row->no_prov;
                $view['default']['ratio'] = ($ratio) ? $ratio : $row->ratio;
                $view['default']['pengangkatan_baru'] = ($pengangkatan_baru) ? $pengangkatan_baru : $row->pengangkatan_baru;
                $view['default']['pindah_masuk'] = ($pindah_masuk) ? $pindah_masuk : $row->pindah_masuk;
                $view['default']['pensiun'] = ($pensiun) ? $pensiun : $row->pensiun;
                $view['default']['meninggal_tdkmampu_cacat'] = ($meninggal_tdkmampu_cacat) ? $meninggal_tdkmampu_cacat : $row->meninggal_tdkmampu_cacat;
                $view['default']['keluar_cuti_dipecat'] = ($keluar_cuti_dipecat) ? $keluar_cuti_dipecat : $row->keluar_cuti_dipecat;

                $this->validator->setData($view['default']);
                if ($this->request->post('emitter') != '') {
                    if ($this->validator->isValid()) {
                        $th_2014 = $_POST['th_2014'];
                        $th_2015 = $_POST['th_2015'];
                        $th_2016 = $_POST['th_2016'];
                        $th_2017 = $_POST['th_2017'];
                        $th_2018 = $_POST['th_2018'];
                        $th_2019 = $_POST['th_2019'];
                        $th_2020 = $_POST['th_2020'];
                        $th_2021 = $_POST['th_2021'];
                        $th_2022 = $_POST['th_2022'];
                        $th_2023 = $_POST['th_2023'];
                        $th_2024 = $_POST['th_2024'];
                        $th_2025 = $_POST['th_2025'];

                        $this->model_proyeksi->delete_tahun_where_in($id);
                        $this->model_proyeksi->update($view['default'], array('id_proyeksi' => $id));
                        for ($i = 1; $i < 14; $i++) {
                            $this->model_proyeksi->insert(array(
                                'id_proyeksi' => $id,
                                'kode_ref_proyeksi' => $i,
                                'th_2014' => $th_2014[$i],
                                'th_2015' => $th_2015[$i],
                                'th_2016' => $th_2016[$i],
                                'th_2017' => $th_2017[$i],
                                'th_2018' => $th_2018[$i],
                                'th_2019' => $th_2019[$i],
                                'th_2020' => $th_2020[$i],
                                'th_2021' => $th_2021[$i],
                                'th_2022' => $th_2022[$i],
                                'th_2023' => $th_2023[$i],
                                'th_2024' => $th_2024[$i],
                                'th_2025' => $th_2025[$i]
                                    ), $this->model_proyeksi->tbl_tahun_proyeksi);
                        }
                    } else {
                        foreach ($this->validator->getErrors() as $field => $messages) {
                            $view['errors'][$field] = $messages['0'];
                        }
                        $this->__view('proyeksi', 'form', $view);
                    }
                } else {
                    $this->__view('proyeksi', 'form', $view);
                }
            } else {
                print_r('Direct access is not allowed');
            }
        }
    }

    public function delete() {
        if (is_ajax_request()) {
            if (is_auth('proyeksi', 'x') && is_login()) {
                extract($_POST);
                $in = '';
                for ($i = 0; $i < count($id); $i++) {
                    $this->model_proyeksi->delete_where_id($id[$i]);
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function json_() {
        if (is_ajax_request() && is_auth('proyeksi')) {
            $no_jenis_sdmk = $this->request->post('no_jenis_sdmk', false);
            $no_prov = $this->request->post('no_prov', false);
            $p = $this->model_proyeksi->get_json_id($no_prov, $no_jenis_sdmk);
            if ($p != null) {
                $p1 = $this->model_proyeksi->get_in_tahun_proyeksi($p['id_proyeksi']);
                $array = array_merge($p, $p1);
                header('Content-type: application/json');
                echo json_encode($array);
            } else {
                echo null;
            }
        } else {
            redirect('404');
        }
    }

    public function search($offset = null) {
        if (is_ajax_request()) {
            if (is_auth('proyeksi', 'x') && is_login()) {
                $limit = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : $this->limit;
                if ($offset == null) {
                    $view['offset'] = 0;
                } else {
                    $view['offset'] = $offset;
                }

                $view['rs'] = $this->model_proyeksi->get_all($view['offset'], $limit);
                $total = $this->model_proyeksi->num_rows();
                $view['total'] = $total;
                $view['opt_limit'] = array();
                for ($i = 5; $i < 30; $i++) {
                    if ($i % 5 == 0) {
                        $view['opt_limit'][$i] = $i;
                    }
                }

                $config['base_url'] = site_url('proyeksi/index/');
                $config['total_rows'] = $total;
                $config['per_page'] = $limit;
                $config['div'] = 'div.xamzo-right';
                $config['form'] = '#crud-data';
                $config['uri_segment'] = $view['offset'];
                $this->pagination->initialize($config);
                $view['paging'] = $this->pagination->crawler_links();
                $view['order'] = $this->request->post('order');
                $view['by'] = $this->request->post('by');
                $view['search'] = $this->request->post('search');
                $view['s_no_prov'] = $this->request->post('s_no_prov');
                $view['s_no_jenis_sdmk'] = $this->request->post('s_no_jenis_sdmk');
                $this->__view('proyeksi', 'search', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function preview() {
        if (is_ajax_request() && is_auth('proyeksi')) {
            
            $view['jenis_sdmk'] = $this->model_proyeksi->get_jenis_sdmk();
            $propinsi = $this->model_proyeksi->get_propinsi();
            $view['propinsi'][''] = '-pilih-';
            foreach ($propinsi as $row) {
                $view['propinsi'][$row->no_prov] = $row->no_prov . '-' . $row->nama_prov;
            }
            $view['default']['no_prov'] = $this->request->post('no_prov');
            $query = $this->model_proyeksi->get_print();
            $view['rs'] = $query;
            $this->__view('proyeksi', 'preview', $view);
        }
    }

    public function print_excel($filename) {
        if (is_login() && is_auth('proyeksi', 'p')) {  
            $title = $this->request->post('title');
            $header = $this->request->post('header'); 
            $rs = $this->model_proyeksi->get_print(); 
            $jenis_sdmk = $this->model_proyeksi->get_jenis_sdmk();
            $date = '<b>Printed: </b>' . date('D, d F Y H:i:s');  
            $tr = '';
                foreach ($rs as $prov) { 
                    $tr.="<tr><td colspan='61' class='text-center'>&nbsp;</td></tr>".
                    "<tr><th colspan='61' class='text-center'>$prov->nama_prov</th></tr>".   
                    "<tr><td colspan='61' class='text-center'>&nbsp;</td></tr>". 
                    "<tr class='text-center'>". 
                        "<th rowspan='2' style='width: 25%;'>Jenis SDMK</th>". 
                        "<th colspan='12' style='width: 15%'>Target Rasio Nakes</th>".
                        "<th colspan='12' style='width: 15%'>Kebutuhan Nakes</th>".
                        "<th colspan='12' style='width: 15%'>Jumlah Nakes Awal Tahun</th>".
                        "<th colspan='12' style='width: 15%'>Jumlah Nakes Akhir Tahun</th>".
                        "<th colspan='12' style='width: 15%'>Kesenjangan Kebutuhan Nakes</th>".
                    "</tr>".
                    "<tr class='text-center'>  
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>   
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>  
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>  
                         
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>  
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>   
                    </tr>"; 
                    $t = 0;
                    foreach ($jenis_sdmk as $rp) {
                        $rt1 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 2);
                        $rt2 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 3);
                        $rt3 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 3);
                        $rt4 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 12);
                        $rt5 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 13);
                        $t11 = isset($rt1) ? round($rt1->th_2014) : '0';
                        $t12 = isset($rt1) ? round($rt1->th_2015) : '0';
                        $t13 = isset($rt1) ? round($rt1->th_2016) : '0';
                        $t14 = isset($rt1) ? round($rt1->th_2017) : '0';
                        $t15 = isset($rt1) ? round($rt1->th_2018) : '0';
                        $t16 = isset($rt1) ? round($rt1->th_2019) : '0';
                        $t17 = isset($rt1) ? round($rt1->th_2020) : '0';
                        $t18 = isset($rt1) ? round($rt1->th_2021) : '0';
                        $t19 = isset($rt1) ? round($rt1->th_2022) : '0';
                        $t110 = isset($rt1) ? round($rt1->th_2023) : '0';
                        $t111 = isset($rt1) ? round($rt1->th_2024) : '0';
                        $t112 = isset($rt1) ? round($rt1->th_2025) : '0';
                        
                        $t21 = isset($rt2) ? round($rt2->th_2014) : '0';
                        $t22 = isset($rt2) ? round($rt2->th_2015) : '0';
                        $t23 = isset($rt2) ? round($rt2->th_2016) : '0';
                        $t24 = isset($rt2) ? round($rt2->th_2017) : '0';
                        $t25 = isset($rt2) ? round($rt2->th_2018) : '0';
                        $t26 = isset($rt2) ? round($rt2->th_2019) : '0'; 
                        $t27 = isset($rt2) ? round($rt2->th_2020) : '0';
                        $t28 = isset($rt2) ? round($rt2->th_2021) : '0';
                        $t29 = isset($rt2) ? round($rt2->th_2022) : '0';
                        $t210 = isset($rt2) ? round($rt2->th_2023) : '0';
                        $t211 = isset($rt2) ? round($rt2->th_2024) : '0';
                        $t212 = isset($rt2) ? round($rt2->th_2025) : '0';  
                         
                        $t31 = isset($rt3) ? round($rt3->th_2014) : '0';
                        $t32 = isset($rt3) ? round($rt3->th_2015) : '0';
                        $t33 = isset($rt3) ? round($rt3->th_2016) : '0';
                        $t34 = isset($rt3) ? round($rt3->th_2017) : '0';
                        $t35 = isset($rt3) ? round($rt3->th_2018) : '0';
                        $t36 = isset($rt3) ? round($rt3->th_2019) : '0';
                        $t37 = isset($rt3) ? round($rt3->th_2020) : '0';
                        $t38 = isset($rt3) ? round($rt3->th_2021) : '0';
                        $t39 = isset($rt3) ? round($rt3->th_2022) : '0';
                        $t310 = isset($rt3) ? round($rt3->th_2023) : '0';
                        $t311 = isset($rt3) ? round($rt3->th_2024) : '0';
                        $t312 = isset($rt3) ? round($rt3->th_2024) : '0';
                        
                        $t41 = isset($rt4) ? round($rt4->th_2014) : '0';
                        $t42 = isset($rt4) ? round($rt4->th_2015) : '0';
                        $t43 = isset($rt4) ? round($rt4->th_2016) : '0';
                        $t44 = isset($rt4) ? round($rt4->th_2017) : '0';
                        $t45 = isset($rt4) ? round($rt4->th_2018) : '0';
                        $t46 = isset($rt4) ? round($rt4->th_2019) : '0';
                        $t47 = isset($rt4) ? round($rt4->th_2020) : '0';
                        $t48 = isset($rt4) ? round($rt4->th_2021) : '0';
                        $t49 = isset($rt4) ? round($rt4->th_2022) : '0';
                        $t410 = isset($rt4) ? round($rt4->th_2023) : '0';
                        $t411 = isset($rt4) ? round($rt4->th_2024) : '0';
                        $t412 = isset($rt4) ? round($rt4->th_2025) : '0';
                        
                        $t51 = isset($rt5) ? round($rt5->th_2014) : '0';
                        $t52 = isset($rt5) ? round($rt5->th_2015) : '0';
                        $t53 = isset($rt5) ? round($rt5->th_2016) : '0';
                        $t54 = isset($rt5) ? round($rt5->th_2017) : '0';
                        $t55 = isset($rt5) ? round($rt5->th_2018) : '0';
                        $t56 = isset($rt5) ? round($rt5->th_2019) : '0';
                        $t57 = isset($rt5) ? round($rt5->th_2020) : '0';
                        $t58 = isset($rt5) ? round($rt5->th_2021) : '0';
                        $t59 = isset($rt5) ? round($rt5->th_2022) : '0';
                        $t510 = isset($rt5) ? round($rt5->th_2023) : '0';
                        $t511 = isset($rt5) ? round($rt5->th_2024) : '0';
                        $t512 = isset($rt5) ? round($rt5->th_2025) : '0';
                        
                        $tr.="<tr> 
                            <td>$rp->nama_jenis_sdmk</td>  
                            <td class='text-center'>$t11</td>  
                            <td class='text-center'>$t12</td>  
                            <td class='text-center'>$t13</td>  
                            <td class='text-center'>$t14</td>  
                            <td class='text-center'>$t15</td>  
                            <td class='text-center'>$t16</td>  
                            <td class='text-center'>$t17</td>  
                            <td class='text-center'>$t18</td>  
                            <td class='text-center'>$t19</td>  
                            <td class='text-center'>$t110</td>  
                            <td class='text-center'>$t111</td>  
                            <td class='text-center'>$t112</td>  
                                
                            <td class='text-center'>$t21</td>  
                            <td class='text-center'>$t22</td>  
                            <td class='text-center'>$t23</td>  
                            <td class='text-center'>$t24</td>  
                            <td class='text-center'>$t25</td>  
                            <td class='text-center'>$t26</td>  
                            <td class='text-center'>$t27</td>  
                            <td class='text-center'>$t28</td>  
                            <td class='text-center'>$t29</td>  
                            <td class='text-center'>$t210</td>  
                            <td class='text-center'>$t211</td>  
                            <td class='text-center'>$t212</td>  
                                
                            <td class='text-center'>$t31</td>  
                            <td class='text-center'>$t32</td>  
                            <td class='text-center'>$t33</td>  
                            <td class='text-center'>$t34</td>  
                            <td class='text-center'>$t35</td>  
                            <td class='text-center'>$t36</td>  
                            <td class='text-center'>$t37</td>  
                            <td class='text-center'>$t38</td>  
                            <td class='text-center'>$t39</td>  
                            <td class='text-center'>$t310</td>  
                            <td class='text-center'>$t311</td>  
                            <td class='text-center'>$t312</td>  
                                
                            <td class='text-center'>$t41</td>  
                            <td class='text-center'>$t42</td>  
                            <td class='text-center'>$t43</td>  
                            <td class='text-center'>$t44</td>  
                            <td class='text-center'>$t45</td>  
                            <td class='text-center'>$t46</td>  
                            <td class='text-center'>$t47</td>  
                            <td class='text-center'>$t48</td>  
                            <td class='text-center'>$t49</td>  
                            <td class='text-center'>$t410</td>  
                            <td class='text-center'>$t411</td>  
                            <td class='text-center'>$t412</td>  
                                
                            <td class='text-center'>$t51</td>  
                            <td class='text-center'>$t52</td>  
                            <td class='text-center'>$t53</td>  
                            <td class='text-center'>$t53</td>  
                            <td class='text-center'>$t54</td>  
                            <td class='text-center'>$t55</td>  
                            <td class='text-center'>$t56</td>  
                            <td class='text-center'>$t57</td>  
                            <td class='text-center'>$t58</td>  
                            <td class='text-center'>$t59</td>  
                            <td class='text-center'>$t510</td>  
                            <td class='text-center'>$t512</td>  
                        </tr>"; 
                        $t++;
                    }
                }  
             
            $f = fopen(REPORTPATH . 'proyeksi/' . $filename, 'wb');
            if (!$f)
                $this->Error('Unable to create output file: ' . $filename); 
            $html = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=Windows-1252;us-ascii"> 
                <meta name=ProgId content=Excel.Sheet>
                <meta name=Generator content="Microsoft Excel 11">
                <style type="text/css"> 
                    table th{
                        background: #ccc;
                    } 
                </style>
            </head>
            <body>
                <table class="table hack-table table-border" data-id="spredsheet" cellpadding="0" cellspacing="0">
                     <tr style="height:30px">
                        <td colspan="10" style="text-align:left;">' . $date . '</td>
                        <td colspan="51" style="text-align:right;">kementerian kesehatan RI</td>
                    </tr>
                    <tr style="height:15px;"><td colspan="61">&nbsp;</td></tr>
                    <tr>
                        <td rowspan="2" colspan="3" style="">
                            <span style="float:left;margin-top:5px;">&nbsp;<img src="./depkes.png"></span>
                            <span style="float:left;margin-top:15px"><img src="./aiphss.png"></span>
                        </td> 
                        <td colspan="58" style="text-align: left; height:10px"><h2>' . $title . '</h2></td>
                    </tr> 
                    <tr>
                        <td style="text-align: left;" colspan="61"><h3>' . $header . '</h3></td>
                    </tr> 
                    <tr style="height:15px;"><td colspan="61">&nbsp;</td></tr>
                </table>
                <table border="1">
                     '.$tr.'
                </table>
            </body>
        </html>'; 
            fwrite($f, $html, strlen($html));
            fclose($f);
            echo REPORTPATH . 'proyeksi/' . $filename;
        }
    }

}
