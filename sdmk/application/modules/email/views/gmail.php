<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>  
<?php

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');


//Tell PHPMailer to use SMTP
$mail->IsSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = $smtp_host;
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = $smtp_port;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = $smtp_username;
//Password to use for SMTP authentication
$mail->Password = $smtp_password;
//Set who the message is to be sent from
$mail->SetFrom($sender['email'], $sender['name']);
//Set an alternative reply-to address
$mail->AddReplyTo($replyTo['email'], $replyTo['name']);
//Set who the message is to be sent to
$mail->AddAddress($reciper['email'], $reciper['name']);

$mail->Subject = $subject;
//Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
$mail->MsgHTML($message);
//Replace the plain text body with one created manually
$mail->AltBody = '';
//Attach an image file
//$mail->AddAttachment(MODPATH.'private/email/images/phpmailer_mini.gif');
//Send the message, check for errors
if (!$mail->Send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} 