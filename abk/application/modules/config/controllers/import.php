<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('entry/faskes_model');
    }

    public function index() {
   
        $data = array();
        $data['class'] = 'import';
        $data['title'] = "Import";

       $this->load->view('import_view', $data);
    }
    
    public function clear($faskes_kode) {
        $faskes_model = new faskes_model();
        $faskes_model->clearKBKFaskes($faskes_kode, date('Y'));  
    }

    public function proses($faskes_tahun) {
        
         $this->orm->debug = TRUE;
        
        $faskes_model = new faskes_model();
         $faskes_kode = '';
         $faskes_nama = '';
         
        $uploads_dir = './uploads';

        $tmp_name = $_FILES["import"]["tmp_name"];
        $name = rand(0, 9999999999).md5($_FILES["import"]["tmp_name"]);
        move_uploaded_file($tmp_name, "$uploads_dir/$name");

        $files = file_get_contents(base_url() . 'uploads/' . $name);
        //$files = file_get_contents('http://103.29.215.167/renbut-sdmk/abk/uploads/1701014_2016(1).json');

        $data = json_decode($files);
        
//        echo $name.'<br/>';
//        print_r($data);die;
        
        
        foreach($data as $row) {
        

         
            $faskes_model->clearKBKFaskes($row->faskes_kode, $faskes_tahun); 
          
         
            
         $faskes_kode = $row->faskes_kode;
         $faskes_nama = $row->faskes_nama;
         
         $faskes = array();
         $faskes['faskes_kode'] = $row->faskes_kode;
         $faskes['NO_KAB'] = $row->NO_KAB;
         $faskes['faskes_nama'] = $row->faskes_nama;
         $faskes['faskes_telp'] = $row->faskes_telp;
         $faskes['faskes_alamat'] = $row->faskes_alamat;
         $faskes['faskes_email'] = $row->faskes_email;
         $faskes['ref_faskes_kelas_id'] = $row->ref_faskes_kelas_id;
         $faskes['ref_jenis_faskes_kode'] = $row->ref_jenis_faskes_kode;
         $faskes['status_pengelolaan_id'] = $row->status_pengelolaan_id;
         $faskes['tahun'] = $row->tahun;
         $faskes['update_date'] = $row->update_date;
         $faskes['insert_date'] = $row->insert_date;
         
         $this->orm->faskes->insert($faskes);
         
         foreach($row->faskes_installasi as $row_installasi) {
             
             $faskes_installasi = array();
             $faskes_installasi['faskes_kode'] = $row_installasi->faskes_kode;
             $faskes_installasi['installasi_nama'] = $row_installasi->installasi_nama;
             $faskes_installasi['eselon'] = $row_installasi->eselon;
             $faskes_installasi['parent'] = $row_installasi->parent;
             $faskes_installasi['urut'] = $row_installasi->urut;
        	//echo $row->ref_jenis_faskes_kode; exit;
//	       if($row->ref_jenis_faskes_kode!=1) {
//			$faskes_installasi_id = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode',$row_installasi->faskes_kode)->fetch();	
//			$ress_faskes_installasi = array();
//			$ress_faskes_installasi['faskes_installasi_id'] = $faskes_installasi_id['faskes_installasi_id'];
//		}
//		else {     
// 	            $ress_faskes_installasi =  $this->orm->faskes_installasi->insert($faskes_installasi);
//             	}
                
                $ress_faskes_installasi =  $this->orm->faskes_installasi->insert($faskes_installasi);
                
             foreach($row_installasi->sdmk as $row_sdmk) {

 

                 $sdmk_faskes_installasi = array();
                 $sdmk_faskes_installasi['ref_jenis_sdmk_kode'] = $row_sdmk->ref_jenis_sdmk_kode;
                 $sdmk_faskes_installasi['sdmk_faskes_installasi_nama'] = $row_sdmk->sdmk_faskes_installasi_nama;
                 $sdmk_faskes_installasi['faskes_installasi_id'] = $ress_faskes_installasi['faskes_installasi_id'];
                 $sdmk_faskes_installasi['jumlah_PNS'] = $row_sdmk->jumlah_PNS;
                 $sdmk_faskes_installasi['jumlah_PPPK'] = $row_sdmk->jumlah_PPPK;
                 $sdmk_faskes_installasi['jumlah_PTT'] = $row_sdmk->jumlah_PTT;
                 $sdmk_faskes_installasi['jumlah_HD'] = $row_sdmk->jumlah_HD;
                 $sdmk_faskes_installasi['jumlah_DLL'] = $row_sdmk->jumlah_DLL;
                 $sdmk_faskes_installasi['jumlah_seluruh'] = $row_sdmk->jumlah_seluruh;
                 $sdmk_faskes_installasi['hasil_kebutuhan_sdmk'] = $row_sdmk->hasil_kebutuhan_sdmk;
                 $sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan'] = $row_sdmk->hasil_kebutuhan_sdmk_pembulatan;

                $ress_sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->insert($sdmk_faskes_installasi);
                 
                 foreach($row_sdmk->kbk as $row_kbk) {
                     
                     $sdmk_kbk = array();
                     $sdmk_kbk['sdmk_faskes_installasi_id'] = $ress_sdmk_faskes_installasi['sdmk_faskes_installasi_id']; 
                     $sdmk_kbk['capaian'] = $row_kbk->capaian; 
                     $sdmk_kbk['norma_waktu'] = $row_kbk->norma_waktu; 
                     $sdmk_kbk['kbk_uraian'] = $row_kbk->kbk_uraian; 
                     $sdmk_kbk['jenis_tugas_id'] = $row_kbk->jenis_tugas_id; 
                     $sdmk_kbk['SBK'] = $row_kbk->SBK; 
                     $sdmk_kbk['waktu_kegiatan'] = $row_kbk->waktu_kegiatan; 
                     $sdmk_kbk['FTP'] = $row_kbk->FTP; 
                     $sdmk_kbk['STP'] = $row_kbk->STP; 
                     $sdmk_kbk['satuan_id'] = $row_kbk->satuan_id; 
                     $sdmk_kbk['urut'] = $row_kbk->urut;
                     
                     $ress_sdmk_kbk = $this->orm->sdmk_kbk->insert($sdmk_kbk);
                     
                 }
                 
             }
         }
                 
               
        }
        
       Message::_set(true, 'Import Data Faskes <br/><b>'.$faskes_nama.' - ('.$faskes_kode.') Berhasil</b>');
    
        redirect('config/import/');
    }

}
