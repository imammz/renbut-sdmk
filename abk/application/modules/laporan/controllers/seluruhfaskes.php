<?php

class Seluruhfaskes extends MX_Controller {

    function __construct() {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('entry/faskes_model');
       
        unset($_SESSION['message']);
    }

    public function index() {
        $data = array();
        $data['class'] = 'laporan';
        $data['title'] = 'Laporan Kebutuhan SDMK Dengan Metode ABK';
        $data['subtitle'] = '';
        $data['faskes'] = 'seluruhfaskes';

        $faskes_model = new faskes_model();
        $data['master_kab'] = $faskes_model->_loadMasterKabupaten($_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_prov'] = $faskes_model->_loadMasterProvinsi($_SESSION['user']['NO_PROV']);

        $this->load->view('seluruhfaskes_view', $data);
    }

  
    
    public function kab_proses($NO_KAB, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ? AND tahun = ?', $NO_KAB, $tahun);
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;

        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $NO_KAB)->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $data['NO_KAB'] = $NO_KAB;
        $data['NAMA_KAB'] = $kab['NAMA_KAB'];
        $data['NO_PROV'] = $kab['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;

        $this->load->view('seluruhfaskes_kab_view', $data);
    }
    
     public function kab_excel($NO_KAB, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ? AND tahun = ? ', $NO_KAB, $tahun);
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;

        $kab = $this->orm->ref_kabupaten->where('NO_KAB', $NO_KAB)->fetch();
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

        $data['NO_KAB'] = $NO_KAB;
        $data['NAMA_KAB'] = $kab['NAMA_KAB'];
        $data['NO_PROV'] = $kab['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;

        $this->load->view('seluruhfaskes_kab_excel', $data);
    }
    
    public function prov_proses($NO_PROV, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('LEFT(NO_KAB,2) = ? AND tahun = ? ', $NO_PROV, $tahun)->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $kab = $this->orm->ref_kabupaten->where('NO_PROV', $NO_PROV);
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $NO_PROV)->fetch();
    
        $data['KAB'] = $kab;
        $data['NO_PROV'] = $prov['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;
        
        $this->load->view('seluruhfaskes_prov_view',$data);
    }
    
     public function prov_excel($NO_PROV, $tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('LEFT(NO_KAB,2) = ? AND tahun = ? ', $NO_PROV, $tahun)->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $kab = $this->orm->ref_kabupaten->where('NO_PROV', $NO_PROV);
        $prov = $this->orm->ref_provinsi->where('NO_PROV', $NO_PROV)->fetch();
    
        $data['KAB'] = $kab;
        $data['NO_PROV'] = $prov['NO_PROV'];
        $data['NAMA_PROV'] = $prov['NAMA_PROV'];
        $data['TAHUN'] = $tahun;
        
        $this->load->view('seluruhfaskes_prov_excel',$data);
    }
    
    
    public function nas_proses($tahun) {

        $faskes = $this->orm->faskes->select('faskes_kode')->where('tahun = ? ', $tahun)->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $prov = $this->orm->ref_provinsi();
        
        $data['PROV'] = $prov;
        $data['TAHUN'] = $tahun;
        
        $this->load->view('seluruhfaskes_nas_view',$data);
    }
    
     public function nas_excel($tahun) {

       $faskes = $this->orm->faskes->select('faskes_kode')->where('tahun = ? ', $tahun)->order('NO_KAB ASC');
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi)->order('ref_jenis_sdmk_kode');

        $ref_jenis_sdmk_kode = $this->orm->sdmk_faskes_installasi->select('ref_jenis_sdmk_kode')->where('faskes_installasi_id', $faskes_installasi);

        $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->order('ref_jenis_sdmk_kode');
        $ref_jenis_sdmk_level1 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->group('ref_jenis_sdmk_nama_level1')->order('ref_jenis_sdmk_kode');

        $data = array();
        $data['faskes'] = $faskes;
        $data['faskes_installasi'] = $faskes_installasi;
        $data['sdmk_faskes_installasi'] = $sdmk_faskes_installasi;
        $data['ref_jenis_sdmk_kode'] = $ref_jenis_sdmk_kode;
        $data['ref_jenis_sdmk'] = $ref_jenis_sdmk;
        $data['ref_jenis_sdmk_level1'] = $ref_jenis_sdmk_level1;
        
        $prov = $this->orm->ref_provinsi();
        
        $data['PROV'] = $prov;
        $data['TAHUN'] = $tahun;
        
        $this->load->view('seluruhfaskes_nas_excel',$data);
    }

}

?>