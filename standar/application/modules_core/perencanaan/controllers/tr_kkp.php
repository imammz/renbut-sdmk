<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_kkp extends CI_Controller {

	var $table = 't_perencanaan';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('prc_kkp_model','prc_kkp');
		$this->load->model('master_data/m_jenis_sdmk_model','jenis_sdmk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Perencanaan";
		$data['subtitle'] = "Entry Data Perencanaan Kebutuhan Minimal SDMK KKP (Kantor Kesehatan Pelabuhan)";
		$data['form_entry_kkp'] = $this->db->order_by('id_jenis_sdmk','ASC')->get_where('m_jenis_sdmk',array('id_jenis_faskes'=>30, 'set_form'=>'Y'));
		$this->load->view('tr_kkp/form_entry_kkp', $data);
	}
	
	public function riwayat_perencanaan()
	{
		
		$data['title'] = "Perencanaan";
		$data['subtitle'] = "Riwayat Perencanaan Kebutuhan Minimal SDMK KKP (Kantor Kesehatan Pelabuhan)";
		$this->load->view('tr_kkp/riwayat_perencanaan', $data);
	}

	public function form_standar_sdmk($kode_kkp, $tahun, $kategori_kelas)
	{
		
		$data['title'] = "Perencanaan";
		$data['subtitle'] = "Riwayat Perencanaan Kebutuhan Minimal SDMK KKP (Kantor Kesehatan Pelabuhan)";
		$data['form_entry_kkp'] = $this->db->order_by('id_jenis_sdmk','ASC')->get_where('m_jenis_sdmk',array('id_jenis_faskes'=>30, 'set_form'=>'Y')); 
		$data['id_jenis_faskes'] = 30; 
		$data['value'] = $this->prc_kkp->get_data_by_custom(array(''.$this->table.'.kode_kkp'=>$kode_kkp,''.$this->table.'.tahun'=>$tahun,'kategori_kelas'=>$kategori_kelas));

		$this->load->view('tr_kkp/form_standar_sdmk', $data);
	}

	public function ajax_list()
	{
		$list = $this->prc_kkp->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $prc_kkp) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($prc_kkp->nama_kabupaten);
			$row[] = strtoupper($prc_kkp->nama_kkp);
			$row[] = strtoupper($prc_kkp->tahun);
			$row[] = strtoupper($prc_kkp->kategori_kelas);
			$row[] = $prc_kkp->jumlah_instalasi;
			$row[] = $prc_kkp->jumlah_wilayah_kerja;
			/*$row[] = ($prc_kkp->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';*/
			/*$row[] = $prc_kkp->updated_date?Tanggal::formatDateTime($prc_kkp->updated_date):'-';*/

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="form_standar_sdmk('."'".Regex::_genRegex($prc_kkp->kode_kkp,'RGXQSL')."'".','."'".Regex::_genRegex($prc_kkp->tahun,'RGXINT')."'".','."'".Regex::_genRegex($prc_kkp->kategori_kelas,'RGXAZ')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_prc_kkp('."'".Regex::_genRegex($prc_kkp->id_perencanaan,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->prc_kkp->count_all(),
						"recordsFiltered" => $this->prc_kkp->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		//print_r($_POST);die;
		$id_perencanaan = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();

		// form post header //
		$dataexc = array(
			'tahun' => Regex::_genRegex($this->input->post('tahun'), 'RGXINT'),
			'id_jenis_faskes' => Regex::_genRegex($this->input->post('id_jenis_faskes'), 'RGXINT'),
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT'),
			'id_kabupaten' => Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT'),
			'kode_kkp' => Regex::_genRegex($this->input->post('kode_kkp'), 'RGXQSL'),
			'kategori_kelas' => Regex::_genRegex($this->input->post('kategori_kelas'), 'RGXAZ'),
			'jumlah_instalasi' => Regex::_genRegex($this->input->post('jumlah_instalasi'), 'RGXINT'),
			'jumlah_wilayah_kerja' => Regex::_genRegex($this->input->post('jumlah_wilayah_kerja'), 'RGXINT'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $this->input->post('submit') == 'save_methode' ){
			// main //
			$pns = Regex::_genRegex($this->input->post('pns'), 'RGXINT');
			$pppk = Regex::_genRegex($this->input->post('pppk'), 'RGXINT');
			$ptt = Regex::_genRegex($this->input->post('ptt'), 'RGXINT');
			$honorer = Regex::_genRegex($this->input->post('honorer'), 'RGXINT');
			$blud = Regex::_genRegex($this->input->post('blud'), 'RGXINT');
			$tks = Regex::_genRegex($this->input->post('tks'), 'RGXINT');
			$total = Regex::_genRegex($this->input->post('total'), 'RGXINT');
			$jml_standar = Regex::_genRegex($this->input->post('jml_standar'), 'RGXINT');
			$id_jenis_sdmk = Regex::_genRegex($this->input->post('id_jenis_sdmk'), 'RGXINT');
			
			// sub parent //
			$pns_sub = Regex::_genRegex($this->input->post('pns_sub'), 'RGXINT');
			$pppk_sub = Regex::_genRegex($this->input->post('pppk_sub'), 'RGXINT');
			$ptt_sub = Regex::_genRegex($this->input->post('ptt_sub'), 'RGXINT');
			$honorer_sub = Regex::_genRegex($this->input->post('honorer_sub'), 'RGXINT');
			$blud_sub = Regex::_genRegex($this->input->post('blud_sub'), 'RGXINT');
			$tks_sub = Regex::_genRegex($this->input->post('tks_sub'), 'RGXINT');
			$total_sub = Regex::_genRegex($this->input->post('total_sub'), 'RGXINT');
			$jml_standar_sub = Regex::_genRegex($this->input->post('jml_standar_sub'), 'RGXINT');
			$id_jenis_sdmk_sub = Regex::_genRegex($this->input->post('id_jenis_sdmk_sub'), 'RGXINT');

			// delete existing //
			$this->db->delete('t_detail_perencanaan', array('id_perencanaan'=>$id_perencanaan));
			//print_r($this->db->last_query());die;
			foreach ($pns as $key => $rows_value) {
				# code...
				$insert = array(
					'id_perencanaan' => $id_perencanaan,
					'id_jenis_sdmk' => $id_jenis_sdmk[$key],
					'jml_pns' => $rows_value,
					'jml_pppk' => $pppk[$key],
					'jml_ptt' => $ptt[$key],
					'jml_honorer' => $honorer[$key],
					'jml_blud' => $blud[$key],
					'jml_tks' => $tks[$key],
					'total_jml' => $total[$key],
					'total_standar' => $jml_standar[$key],
					);

				$this->db->insert('t_detail_perencanaan', $insert);

			}

			if( $pns_sub ){
				foreach ($pns_sub as $key2 => $rows_value_sub) {
					# code...
					$insert2 = array(
						'id_perencanaan' => $id_perencanaan,
						'id_jenis_sdmk' => $id_jenis_sdmk_sub[$key2],
						'jml_pns' => $rows_value_sub,
						'jml_pppk' => $pppk_sub[$key2],
						'jml_ptt' => $ptt_sub[$key2],
						'jml_honorer' => $honorer_sub[$key2],
						'jml_blud' => $blud_sub[$key2],
						'jml_tks' => $tks_sub[$key2],
						'total_jml' => $total_sub[$key2],
						'total_standar' => $jml_standar_sub[$key2],
						);

					$this->db->insert('t_detail_perencanaan', $insert2);

				}
			}
			

		}else{

			if( $id_perencanaan == 0 ){

				// cek existing by tahun,kode_kkp,tipe kkp, tipe kawasan
				$existing = $this->db->get_where('t_perencanaan', array('tahun'=>$dataexc['tahun'], 'kode_kkp'=>$dataexc['kode_kkp'],'kategori_kelas'=>$dataexc['kategori_kelas']));
				
				if( $existing->num_rows() > 0 ){
					$this->prc_kkp->update( array('id_perencanaan'=>$existing->row()->id_perencanaan ), $dataexc);	
					$idp = $existing->row()->id_perencanaan;
				} else {
					$this->prc_kkp->save($dataexc);
					$idp = $this->db->insert_id();
				}

			}else{
				$this->prc_kkp->update(array('id_perencanaan'=>$id_perencanaan), $dataexc);
				$idp = $id_perencanaan;
			}

			$this->db->delete('t_detail_perencanaan_form', array('id_perencanaan'=>$idp));
			$id_jenis_sdmk_form = $this->input->post('id_jenis_sdmk_form');
			$value_form_sdmk = $this->input->post('value');
			foreach ($id_jenis_sdmk_form as $key => $value) {
				$detail_rsu_form = array(
					'id_perencanaan' => $idp,
					'id_jenis_sdmk' => $value,
					'value' => $value_form_sdmk[$key],
					'updated_date' => date('Y-m-d H:i:s'),
					'updated_by' => $this->session->userdata('data_user')->fullname,
					);
				$this->db->insert('t_detail_perencanaan_form', $detail_rsu_form);
			}


		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE ));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array(
							"status" => TRUE,
							"tahun" => Regex::_genRegex($this->input->post('tahun'), 'RGXINT'),
							"kode_kkp" => Regex::_genRegex($this->input->post('kode_kkp'), 'RGXQSL'),
							"kategori_kelas" => Regex::_genRegex($this->input->post('kategori_kelas'), 'RGXAZ')
							));
		}
		
	}


	public function ajax_delete($id)
	{
		$this->prc_kkp->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
