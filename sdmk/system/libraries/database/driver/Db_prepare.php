<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		libraries/database
 * @author		xamzo Dev Team 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.5
 * @filesource
 */

class Db_prepare {

    public function prepare_insert($data, $tbl) {
        if (is_array($data)) {
            $fkey = '';
            $fvalue = '';
            $no = 1;
            foreach ($data as $key => $val) {
                if (!is_array($val)) {
                    $val = addslashes($val);
                    if ($no == 1) {
                        $fkey .= '`' . $key . '`';
                        $fvalue .= "'" . $val . "'";
                    } else {
                        $fkey .= ',`' . $key . '`';
                        $fvalue .= ",'" . $val . "'";
                    }
                }
                $no++;
            }
        }
        if ($tbl !== null) {
            $table = $tbl;
        } else {
            $table = $this->table;
        }
        return "INSERT into $table ($fkey) VALUE ($fvalue)";
    }

    public function prepare_delete($wheres, $tbl) {
        $where = '';
        $w = 1;
        if (count($wheres) > 0) {
            foreach ($wheres as $key => $val) {
                $val = addslashes($val);
                if ($w > 1) {
                    $where.=" AND ";
                } else {
                    $where.=" ";
                }
                $where.="`" . $key . "`" . " = '" . $val . "'";
                $w++;
            }
            return "DELETE FROM $tbl WHERE $where";
        }
    }

    public function prepare_delete_in($key, $wheres, $tbl) {
        $where = '';
        $w = 1;
        if (count($wheres) > 0) {
            foreach ($wheres as $val) {
                $val = addslashes($val);
                if ($w > 1) {
                    $where.=",";
                }
                $where.="$val";
                $w++;
            }
            return "DELETE FROM $tbl WHERE $key IN($where)";
        }
    }

    public function prepare_update($update = array(), $wheres = array(), $tbl = null) {
        $i = 1;
        if ($tbl !== null) {
            $table = $tbl;
        } else {
            $table = $this->table;
        }
        $query = "UPDATE " . $table . " SET ";
        foreach ($update as $key => $val) {
            $val = addslashes($val);
            if ($i > 1) {
                $query.=", ";
            } else {
                $query.=" ";
            }
            $query.="`" . $key . "`" . " = '" . $val . "'";
            $i++;
        }
        $where = '';
        $w = 1;
        if (count($wheres) > 0) {
            foreach ($wheres as $key => $val) {
                $val = addslashes($val);
                if ($w > 1) {
                    $where.=" AND ";
                } else {
                    $where.=" ";
                }
                $where.="`" . $key . "`" . " = '" . $val . "'";
                $w++;
            }
            $query.=" WHERE " . $where;
        }
        return $query;
    }

} 