<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-home"></span> &nbsp;Beranda</h3>
        <span class="xamzo-head-info">halaman dashboard, lihat dokumentasi cara penggunaannya. </span>
    </div>
    <div class="right">
    </div>
</div>
<div class="xamzo-block">
    <ul class="list-dashboard">
        <li>
            <a href="#!proyeksi/index" ajax-title="Proyeksi">
                <i class="fa fa-bar-chart"></i>
                <span>Daftar Proyeksi</span>
            </a>
        </li>
        <li>
            <a href="#!proyeksi/create" ajax-title="Tambah Proyeksi">
                <i class="fa fa-plus"></i>
                <span>Tambah Proyeksi</span>
            </a>
        </li>
        <li>
            <a href="#!proyeksi/preview" ajax-title="Cetak Proyeksi">
                <i class="fa fa-print"></i>
                <span>Cetak Proyeksi</span>
            </a>
        </li>
        
        <li>
            <a href="#!user/index" ajax-title="Pengguna">
                <i class="fa fa-group"></i>
                <span>Pengguna Aplikasi</span>
            </a>
        </li>
        <li>
            <a href="#!setting/index" ajax-title="Master - Pengaturan">
                <i class="fa fa-wrench"></i>
                <span>Pengaturan Aplikasi</span>
            </a>
        </li>
    </ul>
</div>   