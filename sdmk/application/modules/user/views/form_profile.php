<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<div class="modal-header">
    <button type="button" class="close btn success large" onclick="close_box();">&times;</button>
    <h3>Ubah Profile</h3>
</div>
<div class="modal-body">
    <form id="crud-form-modal" onsubmit="false">
        <input type="hidden" name="emitter" value="1">   
        <p>
            <label class="control-label">Nama Lengkap *</label>
            <input type="text" name="fullname" validation="required" value="<?php echo isset($default['fullname']) ? $default['fullname'] : '' ?>"> 
            <?php echo isset($errors['fullname']) ? $errors['fullname'] : ''; ?>
        </p>  
        <p>
            <label class="control-label">Tempat / Tanggal Lahir</label>
            <span style="float: left; width: 48%;">
                <input type="text" name="born" value="<?php echo isset($default['born']) ? $default['born'] : '' ?>"> 
                <?php echo isset($errors['born']) ? $errors['born'] : ''; ?> 
            </span>
            <span style="float: left; width: 48%; margin-left: 3%;">
                <input type="text" name="birth_day" class="date" placeholder="yyyy-mm-dd" value="<?php echo isset($default['birth_day']) ? $default['birth_day'] : '' ?>"> 
                <?php echo isset($errors['birth_day']) ? $errors['birth_day'] : ''; ?>
            </span>
        </p>  
        <p>
            <label class="control-label">Jenis Kelamin</label>
            <input type="radio" id="male" name="sex" value="1" <?php echo isset($default['sex']) ? ($default['sex'] == '1') ? 'checked="checked"' : '' : '' ?>> <label for="male"> Laki-laki</label>
            <input type="radio" id="female" name="sex" value="0" <?php echo isset($default['sex']) ? ($default['sex'] == '0') ? 'checked="checked"' : '' : '' ?>> <label for="female"> Perempuan</label>
        </p>   
        <p>
            <label class="control-label">Email Personal</label> 
            <input type="text" name="email" validation="required|email" value="<?php echo isset($default['email']) ? $default['email'] : '' ?>"> 
            <?php echo isset($errors['email']) ? $errors['email'] : ''; ?> 
        </p>  
        <p class="modal-footer"> 
            <button class="btn primary medium" is_close_box="false" href="<?php echo isset($action) ? $action : '' ?>" ajax="true" ajax-data="#crud-form-modal" ajax-target="false" ajax-type="POST"><?php echo $this->lang['submit'] ?></button>
            <b>*</b> tidak boleh kosong
            <a class="btn warning right medium" onclick="form_reset('#crud-form-modal');"><?php echo $this->lang['reset'] ?></a>
        </p>
    </form> 
</div>

<script type="text/javascript">
    $(".date").mask('00-00-0000');
</script>