<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/application
 * @author		xamzo developer
 * @copyright   Copyright (c) 2013, xamzo.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('x_post_limiter')) {

    function x_post_limiter($id = null, $permalink = null, $content = null, $text = 'Read More') {
        if ($content != null && $id != null) {
            $rt = '';
            $ex = explode('<hr class="editor-split">', $content);
            if (isset($ex['1'])) {
                $rt.= $ex['0'];
            } else {
                $rt.= character_limiter($content, 1000);
            }
            $link = "<a class='read-more' title='$text' href='" . site_url('post/' . $id . '/' . $permalink . URL_EXT) . "'>$text</a>";
            return $rt . '<p class="ca-read-more">' . $link . '</p>';
        }
    }

}

if (!function_exists('x_related_post')) {

    function x_related_post($post_id = null, $cat_id = null, $limit = null) {
        $X = &get_instance();
        $X->__library(array('database'));
        $query = $X->db->query("SELECT id,title,permalink,content,cat_id FROM " . $X->db->prefix . "post WHERE id!='$post_id' AND cat_id LIKE '%$cat_id%' LIMIT $limit");
        return $query;
    }

}

if (!function_exists('x_get_category_name')) {

    function x_get_category_name($cat_id = null) {
        $X = &get_instance();
        $X->__library(array('database'));
        $query = $X->db->query("SELECT title FROM " . $X->db->prefix . "post_category WHERE id='$cat_id'")->row();
        return $query->title;
    }

}

if (!function_exists('x_random_post')) {

    function x_random_post($limit = null) {
        $X = &get_instance();
        $X->__library(array('database'));
        $query = $X->db->query('SELECT id,title, permalink, content,cat_id FROM ' . $X->db->prefix . 'post WHERE publish="1" ORDER BY rand() LIMIT ' . $limit . '');
        return $query;
    }

}


if (!function_exists('x_content_post')) {

    function x_content_post($content) {
        return str_replace(array('src="application'), 'src="' . site_url() . 'application', $content);
    }

}

if (!function_exists('x_post_type')) {

    function x_post_type($id) {
        $X = &get_instance();
        $X->__library(array('database'));
        $query = $X->db->query('SELECT meta_key FROM ' . $X->db->prefix . 'meta WHERE meta_id="' . $id . '" 
            AND meta_key IN("photo","audio","video","quote","link")');
        if ($query->num_rows() > 0) {
            return $query->row()->meta_key;
        } else {
            return false;
        }
    }

}

if (!function_exists('x_post_comment')) {

    function x_post_comment($id) {
        $X = &get_instance();
        $X->__library(array('database'));
        $query = $X->db->query('SELECT COUNT(*) as num FROM ' . $X->db->prefix . 'comment WHERE post_id="' . $id . '" ');
        return $query->row()->num;
    }

}

if (!function_exists('x_meta_type')) {

    function x_meta_value($id, $type) {
        $X = &get_instance();
        $X->__library(array('database'));
        $query = $X->db->query('SELECT meta_value FROM ' . $X->db->prefix . 'meta WHERE meta_id="' . $id . '" 
            AND meta_key="' . $type . '"');
        if ($query->num_rows() > 1) {
            return $query->result();
        } elseif ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

}
if (!function_exists('x_post_category')) {

    function x_post_category($cat_id = null) {
        $in = explode(',', $cat_id);
        $X = &get_instance();
        $X->__library(array('database'));
        $query = $X->db
                ->select('title,permalink')
                ->from($X->db->prefix . "post_category")
                ->where_in('id', $in)
                ->get()
                ->result();
        $rs = '';
        $no = 1;
        foreach ($query as $r) {
            if ($no == 1) {
                $rs.="<a href='" . site_url($r->permalink) . "' tagret='_blank'>$r->title</a>";
            } else {
                $rs.=", <a href='" . site_url($r->permalink) . "' tagret='_blank'>$r->title</a>";
            }
            $no++;
        }
        return $rs;
    }

}