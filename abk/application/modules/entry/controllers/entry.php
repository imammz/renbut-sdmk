<?php

class entry extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('abk_model');
       
        
    }
    
    	public function index()
	{       
                $data = array();
                $data['class'] = 'entry';
                $this->load->view('pilih_faskes_view',$data);
                
	}
        
        
   public function detil_perhitungan_abk($sdmk_faskes_installasi_id,$button = false) {
        
        $this->load->library('Terbilang');    
            
        $data = array();        
        $data['sdmk_kbk'] = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,1)->order('urut ASC');
        $data['ftp'] = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,2)->select('SUM(FTP) as total')->fetch();
        $data['sdmk_faskes_installasi_id'] = $sdmk_faskes_installasi_id;
        $data['button'] = $button;
        
        $this->load->view('detil_perhitungan_abk_view',$data);
        
    }
    
    public function detil_perhitungan_abk_excel($sdmk_faskes_installasi_id) {
        
        $this->load->library('Terbilang');
        
        $data = array();        
        $data['sdmk_faskes_installasi'] = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id);
        $data['sdmk_kbk'] = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,1)->order('urut ASC');
        $data['ftp'] = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,2)->select('SUM(FTP) as total')->fetch();
        
        $this->load->view('detil_perhitungan_abk_excel',$data);
        
    }
        
        
        
        

}

?>