<?php
header('Content-Type: application/json');

require ('../include/connect.php');

$xhr = $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'; 
if (!$xhr){ 
	header('HTTP/1.1 500 Error: Request must come from Ajax!'); 
	exit();	
}


$sql = "SELECT a.id_provinsi,a.nama_provinsi,SUM(total) AS total,a.polygon2,a.latitude,a.longitude,b.`keterangan`
		FROM provinsi a,sdm b
		WHERE a.id_provinsi=b.`id_provinsi`
		GROUP BY a.nama_provinsi";		
$sql = "SELECT a.id_provinsi,a.nama_provinsi,SUM(total) AS total,SUM(target) AS target,a.polygon2,a.latitude,a.longitude,b.`keterangan`
		FROM provinsi a,sdm b
		WHERE a.id_provinsi=b.`id_provinsi`
		GROUP BY a.nama_provinsi";		

$data = mysql_query($sql);

$json = '{"indonesia": {';
$json .= '"lahan":[ ';
while($x = mysql_fetch_assoc($data)){
	if($x['total'] < $x['target']){
		$warna ="#fc0f2f"; //merah
	}else if($x['total'] > $x['target']){
		$warna ="#1ddb23"; //hijau
	}else{
		$warna ="#104099"; //biru
	}
	$json .= '{';
	$json .= '"id":"'.$x['id_provinsi'].'",
		"nama_provinsi":"'.htmlspecialchars($x['nama_provinsi']).'",
		"total":"'.$x['total'].'",
		"keterangan":"'.htmlspecialchars($x['keterangan']).'",
		"polygon":"'.$x['polygon2'].'",
		"warna":"'.$warna.'",
		"lat":"'.$x['latitude'].'",
		"lng":"'.$x['longitude'].'"
	},';
}

$json = substr($json,0,strlen($json)-1);
$json .= ']';
$json .= '}}';

echo $json;

?>
