$(document).ready(function() {
    var mapCenter = new google.maps.LatLng(-3.5451241, 112.7293194); //Google map Coordinates
    var map;
	var iconPath  = "../icon/marker_1.png";

    map_initialize();

    function map_initialize() {
        var googleMapOptions = {
            center: mapCenter, // map center
            zoom: 5,
            //maxZoom: 18,
            //minZoom: 16,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL //zoom control size
            },
            scaleControl: true, // enable scale control
            mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), googleMapOptions);

        $.get("get_marker.php", function(data) {
            $(data).find("marker").each(function() {
                var name = $(this).attr('name');
                var address = $(this).attr('address');
                var info1 = $(this).attr('info1');
				var info2 = $(this).attr('info2');
				var info3 = $(this).attr('info3');
				var info4 = $(this).attr('info4');
                var point = new google.maps.LatLng(parseFloat($(this).attr('lat')), parseFloat($(this).attr('lng')));
                create_marker_new(point, name, address, false, false, false, info1,info2,info3,info4,iconPath);
				
            });
        });
    }


    $("#search_location").click(function() {
        var mapCenter = new google.maps.LatLng(-3.5451241, 112.7293194); //Google map Coordinates
        var map;

        var googleMapOptions = {
            center: mapCenter, // map center
            zoom: 5,
            //maxZoom: 18,
            //minZoom: 16,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL //zoom control size
            },
            scaleControl: true, // enable scale control
            mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), googleMapOptions);
        search = $('#search').val();
        $.post("get_marker.php", {
            search: search
        }, function(data) {
            $(data).find("marker").each(function() {
                var name = $(this).attr('name');
                var address = $(this).attr('address');
                var type = $(this).attr('type');
                var point = new google.maps.LatLng(parseFloat($(this).attr('lat')), parseFloat($(this).attr('lng')));
				var info1 = $(this).attr('info1');
				var info2 = $(this).attr('info2');
				var info3 = $(this).attr('info3');
				var info4 = $(this).attr('info4');
                create_marker2(point, name, address, false, false, false,iconPath,info1,info2,info3,info4, map);
            });
        });
    });

    function create_marker2(MapPos, MapTitle, address, InfoOpenDefault, DragAble, Removable,iconPath,info1,info2,info3,info4, map) {
        console.log(MapPos);
        //new marker
        var marker = new google.maps.Marker({
            position: MapPos,
            map: map,
            draggable: DragAble,
            animation: google.maps.Animation.DROP,
            title: "Hello World!",
            icon: iconPath
        });

		
        var contentString = '<div class="info-window">' +
                '<h3>' + MapTitle + '</h3>' +
                '<div class="info-content">' +
					'<table class="table" style="">' +
						'<tr>' +
							'<th>Address</th>'+
							
							'<td>'+address+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info1</th>' +
							
							'<td>'+info1+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info2</th>' +
							
							'<td>'+info2+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info3</th>' +
							
							'<td>'+info3+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info4</th>' +
							
							'<td>'+info4+'</td>' +
						'</tr>' +
					'<table>' +
                '</div>' +
                '</div>';
		
		 var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 600
        });


        //add click listner to save marker button		 
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker); // click on marker opens info window 
        });

        if (InfoOpenDefault) {
            infowindow.open(map, marker);
        }
    }

	
	function create_marker_new(MapPos, MapTitle, MapDesc, InfoOpenDefault, DragAble, Removable,Info1,Info2,Info3,Info4, iconPath) {
        console.log(MapPos);
        //new marker
        var marker = new google.maps.Marker({
            position: MapPos,
            map: map,
            draggable: DragAble,
            animation: google.maps.Animation.DROP,
            title: "Hello World!",
            icon: iconPath
        });

        var contentString = '<div class="info-window">' +
                '<h3>' + MapTitle + '</h3>' +
                '<div class="info-content">' +
					'<table class="table" style="">' +
						'<tr>' +
							'<th>Address</th>'+
							
							'<td>'+MapDesc+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info1</th>' +
							
							'<td>'+Info1+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info2</th>' +
							
							'<td>'+Info2+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info3</th>' +
							
							'<td>'+Info3+'</td>' +
						'</tr>' +
						'<tr>' +
							'<th>Info4</th>' +
							
							'<td>'+Info4+'</td>' +
						'</tr>' +
					'<table>' +
                '</div>' +
                '</div>';
		 var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 400
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
        });

        if (InfoOpenDefault) {
            infowindow.open(map, marker);
        }
    }

});