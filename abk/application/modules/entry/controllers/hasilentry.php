<?php

class hasilentry extends MX_Controller {

    function __construct() {
        parent:: __construct();
        $this->checkLogin();
        $this->load->model('abk_model');
        $this->load->model('faskes_model');
    }

    public function index() {
        $data = array();
        $data['class'] = 'hasilentry';
        $this->load->view('hasil_pilih_faskes_view', $data);
    }

    public function rumahsakit() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'rumahsakit';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Rumah Sakit";
        $this->load->view('hasil_view', $data);
    }

    public function rumahsakit_data($tahun_entry = null) {
        
    if($tahun_entry==null) { $tahun_entry = date('Y');}
        
        $data = array();
        $data['class'] = 'rumahsakit';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Rumah Sakit";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadRumahSakit($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_rumahsakit'] = $faskes_model->_loadMasterRumahSakit($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        // echo "<pre>";  print_r($faskes_model->_loadRumahSakit()); exit;
        $this->load->view('hasilentry_rumahsakit_data_view', $data);
    }

    public function puskesmas() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'puskesmas';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Puskesmas";
        $this->load->view('hasil_view', $data);
    }

    public function puskesmas_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'puskesmas';
        $data['title'] = "Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Puskesmas";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadPuskesmas($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_puskesmas'] = $faskes_model->_loadMasterPuskesmas($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
         //echo "<pre>";  print_r($data['master_puskesmas']); exit;
        $this->load->view('hasilentry_puskesmas_data_view', $data);
    }

    public function dinaskesehatan() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'dinaskesehatan';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $this->load->view('hasil_view', $data);
    }

    public function dinaskesehatan_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'Dinaskesehatan';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadDinasKesehatan($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_Dinaskesehatan'] = $faskes_model->_loadMasterDinasKesehatan($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        // echo "<pre>";  print_r($faskes_model->_loadUPT()); exit;
        $this->load->view('hasilentry_dinaskesehatan_data_view', $data);
    }
    
    public function dinaskesehatanprov() {
        $data = array();
        $data['class'] = 'hasilentryprov';
        $data['faskes'] = 'dinaskesehatanprov';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Dinkes Provinsi";
        $this->load->view('hasil_view', $data);
    }

    public function dinaskesehatanprov_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'Dinaskesehatanprov';
        $data['title'] = "Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Dinkes Provinsi";
        $data['tahun_entry'] = $tahun_entry;
        
        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadDinasKesehatanProv($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_Dinaskesehatanprov'] = $faskes_model->_loadMasterDinasKesehatanProv($_SESSION['user']['NO_PROV']);
        // echo "<pre>";  print_r($faskes_model->_loadUPT()); exit;
        $this->load->view('hasilentry_dinaskesehatanprov_data_view', $data);
    }
    
    
    public function balaikesehatan() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'balaikesehatan';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Balai Kesehatan";
        $this->load->view('hasil_view', $data);
    }

    public function balaikesehatan_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'balaikesehatan';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Balai Kesehatan";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadbalaikesehatan($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_balaikesehatan'] = $faskes_model->_loadMasterBalaiKesehatan($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_balaikesehatan_data_view', $data);
    }
    
    
    
    public function kkp() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'kkp';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "KKP";
        $this->load->view('hasil_view', $data);
    }

    public function kkp_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'kkp';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "KKP";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadkkp($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_kkp'] = $faskes_model->_loadMasterkkp($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_kkp_data_view', $data);
    }


      public function klinik() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'klinik';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Klinik";
        $this->load->view('hasil_view', $data);
    }

    public function klinik_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'klinik';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Klinik";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadKlinik($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_klinik'] = $faskes_model->_loadMasterKlinik($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_klinik_data_view', $data);
    }


      public function labkes() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'labkes';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Labkes";
        $this->load->view('hasil_view', $data);
    }

    public function labkes_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'labkes';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Labkes";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadlabkes($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_labkes'] = $faskes_model->_loadMasterlabkes($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_labkes_data_view', $data);
    }
    
    
     public function bpkm() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'bpkm';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Labkes";
        $this->load->view('hasil_view', $data);
    }

    public function bpkm_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'bpkm';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "BPKM";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadbpkm($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_bpkm'] = $faskes_model->_loadMasterbpkm($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_bpkm_data_view', $data);
    }
    
    
     public function bptklpp() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'bptklpp';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "BPTKLPP";
        $this->load->view('hasil_view', $data);
    }

    public function bptklpp_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'bptklpp';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "BPTKLPP";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadbpkm($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_bptklpp'] = $faskes_model->_loadMasterbpkm($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_bptklpp_data_view', $data);
    }
    
     public function upt() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'upt';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $this->load->view('hasil_view', $data);
    }

    public function upt_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'upt';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadUpt($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_upt'] = $faskes_model->_loadMasterUpt($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_upt_data_view', $data);
    }
    
     public function uptdprov() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'uptdprov';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $this->load->view('hasil_view', $data);
    }

    public function uptdprov_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'upt';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadUptdprov($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_upt'] = $faskes_model->_loadMasterUptdprov($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_uptdprov_data_view', $data);
    }
    
     public function uptdkab() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'uptdkab';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $this->load->view('hasil_view', $data);
    }

    public function uptdkab_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'upt';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "UPT";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadUptdkab($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_upt'] = $faskes_model->_loadMasterUptdkab($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
      //   echo "<pre>";  print_r($data['master_balaikesehatan']); exit;
        $this->load->view('hasilentry_uptdkab_data_view', $data);
    }
    
    
    public function lainnya() {
        $data = array();
        $data['class'] = 'hasilentry';
        $data['faskes'] = 'lainnya';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Faskes Lainnya";
        $this->load->view('hasil_view', $data);
    }

    public function lainnya_data($tahun_entry = null) {
            if($tahun_entry==null) { $tahun_entry = date('Y');}
        $data = array();
        $data['class'] = 'lainnya';
        $data['title'] = "Hasil Entry Perhitungan Metode ABK";
        $data['subtitle'] = "Faskes Lainnya";
        $data['tahun_entry'] = $tahun_entry;

        $faskes_model = new faskes_model();
        $data['data_faskes'] = $faskes_model->_loadlainnya($tahun_entry, $_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        $data['master_lainnya'] = $faskes_model->_loadMasterlainnya($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
        // echo "<pre>";  print_r($data['master_lainnya']); exit;
        $this->load->view('hasilentry_lainnya_data_view', $data);
    }
    
    
  
    

}

?>