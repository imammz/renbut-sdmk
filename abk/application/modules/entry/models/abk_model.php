<?php

class abk_model extends CI_Model {

    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();
    }
    
    public function _getWKTMenit() {
    
        $ress = $this->orm->ref_komponen_wkt->where('komponen_wkt_kode', 'WKT3MNT')->fetch();

        return $ress['komponen_wkt_jumlah'];
    }

    public function _getSBK($normaWaktu) {

        if($normaWaktu==0||$normaWaktu=='') {
            $normaWaktu = 1;
        }
        
        
       

        $wkt = $this->_getWKTMenit();
        $sbk = $wkt / $normaWaktu;

        return $sbk;
    }

    public function _getFTP($normaWaktu, $satuan_id) {

        
       

        $kl = 1;

        switch ($satuan_id) {
            case 1 :
                $kl = 720;
                break;
            case 2 :
                $kl = 3120;
                break;
            case 3 :
                $kl = 15840;
                break;
            case 4 :
                $kl = 60;
                break;
            case 5 :
                $kl = 1;
                break;
            case 6 :
                $kl = 12;
                break;
            case 7 :
                $kl = 52;
                break;
            case 8 :
                $kl = 264;
                break;
        }


        $waktu_kegiatan = $normaWaktu * $kl;

        $wkt = $this->_getWKTMenit();

        $ftp = ($waktu_kegiatan / $wkt) * 100;


        return $ftp;
    }

    public function _getWaktuKegiatan($normaWaktu, $satuan_id) {

        
       

        $kl = 1;

        switch ($satuan_id) {
           
            case 1 :
                $kl = 720;
                break;
            case 2 :
                $kl = 3120;
                break;
            case 3 :
                $kl = 15840;
                break;
            case 4 :
                $kl = 60;
                break;
            case 5 :
                $kl = 1;
                break;
            case 6 :
                $kl = 12;
                break;
            case 7 :
                $kl = 52;
                break;
            case 8 :
                $kl = 264;
                break;
        }


        $waktu_kegiatan = $normaWaktu * $kl;


        return $waktu_kegiatan;
    }

    public function _fmlSTP($total_ftp) {

        $stp = 1 / (1 - $total_ftp);

        return number_format($stp, 2);
    }

    public function _getSDMKPerKBK($capaian, $sbk) {

        $sdmk = $capaian / $sbk;

        return $sdmk;
    }

    public function _checkCurrentFaskes($faskes_kode) {

        
       

        $faskes = $this->orm->faskes->where('faskes_kode', $faskes_kode);

        if (count($faskes) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function _getFaskesInstallasiIdByName($faskes_kode, $installasi_nama) {
        
       

        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode = ? AND installasi_nama = ?', $faskes_kode, $installasi_nama)->fetch();

        return $faskes_installasi['faskes_installasi_id'];
    }

    public function _getSDMKFaskesInstallasiId($ref_jenis_sdmk_kode, $faskes_installasi_id) {
        
       

        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('ref_jenis_sdmk_kode = ? AND faskes_installasi_id = ?', $ref_jenis_sdmk_kode, $faskes_installasi_id)->fetch();

        return $sdmk_faskes_installasi['sdmk_faskes_installasi_id'];
    }

    public function clearKBKFaskes($faskes_kode) {
        
       

        $faskes = $this->orm->faskes->where('faskes_kode', $faskes_kode);
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes_kode);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi);
        $sdmk_kbk = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi);

        $sdmk_kbk->delete();
        $sdmk_faskes_installasi->delete();
        $faskes_installasi->delete();
        $faskes->delete();
    }

    public function clearInstallasiFaskes($faskes_kode) {
        
       

        $faskes = $this->orm->faskes->where('faskes_kode', $faskes_kode);
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes_kode);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi);
        $sdmk_kbk = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi);

        $sdmk_kbk->delete();
        $sdmk_faskes_installasi->delete();
        $faskes_installasi->delete();
    }

    public function _checkInstallasiRumahSakit($faskes_kode, $installasi_nama) {
        
       

        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode = ? AND installasi_nama = ?', $faskes_kode, $installasi_nama);

        if (COUNT($faskes_installasi) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function save_faskes($faskes,$ref_jenis_faskes_kode) {
        
        
       
        
        $data = array();
        $data['faskes_kode'] = _valRegex($faskes['faskes_kode'], 'RGXSQL');
        $data['NO_KAB'] = _valRegex($faskes['NO_KAB'], 'RGXSQL');
        $data['faskes_nama'] = _valRegex($faskes['faskes_nama'], 'RGXSQL');
        $data['ref_faskes_kelas_id'] = _valRegex($faskes['ref_faskes_kelas_id'], 'RGXSQL');
        $data['faskes_alamat'] = _valRegex($faskes['faskes_alamat'], 'RGXSQL');
        $data['faskes_telp'] = _valRegex($faskes['faskes_telp'], 'RGXSQL');
        $data['faskes_email'] = _valRegex($faskes['faskes_email'], 'RGXSQL');
        $data['ref_jenis_faskes_kode'] = $ref_jenis_faskes_kode;
        $data['tahun'] = date('Y');
        $data['insert_date'] = date("Y-m-d H:i:s");

        $this->orm->faskes->insert($data);
    }

    public function save_installasi($faskes_installasi,$faskes_kode) {
        
        
       
        
        foreach ($faskes_installasi as $key_faskes_installasi => $val_faskes_installasi) {
            $data_faskes_installasi = array();
            $data_faskes_installasi['faskes_kode'] = $faskes_kode;
            $data_faskes_installasi['installasi_nama'] = $val_faskes_installasi['installasi'];

           $_SESSION['faskes_installasi'][] = $this->orm->faskes_installasi->insert($data_faskes_installasi);
        }
    }

    public function save_sdmk_faskes_installasi($faskes_installasi) {

        
       
        
        foreach ($faskes_installasi as $key_faskes_installasi => $val_faskes_installasi) {
            foreach ($val_faskes_installasi['sdmk'] as $key_sdmk_faskes_installasi => $val_sdmk_faskes_installasi) {

                $ref_jenis_sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $val_sdmk_faskes_installasi['ref_jenis_sdmk_kode'])->fetch();

                $data_sdmk_faskes_installasi = array();
                $data_sdmk_faskes_installasi['ref_jenis_sdmk_kode'] = $val_sdmk_faskes_installasi['ref_jenis_sdmk_kode'];
                $data_sdmk_faskes_installasi['sdmk_faskes_installasi_nama'] = $ref_jenis_sdmk['ref_jenis_sdmk_nama_level2'] . ' ' . $ref_jenis_sdmk['ref_jenis_sdmk_nama_level3'];
                $data_sdmk_faskes_installasi['faskes_installasi_id'] = $ress_faskes_installasi['faskes_installasi_id'];
                $data_sdmk_faskes_installasi['jumlah_PNS'] = $val_sdmk_faskes_installasi['jumlah_PNS'];
                $data_sdmk_faskes_installasi['jumlah_PPPK'] = $val_sdmk_faskes_installasi['jumlah_PPPK'];
                $data_sdmk_faskes_installasi['jumlah_PTT'] = $val_sdmk_faskes_installasi['jumlah_PTT'];
                $data_sdmk_faskes_installasi['jumlah_HD'] = $val_sdmk_faskes_installasi['jumlah_HD'];
                $data_sdmk_faskes_installasi['jumlah_DLL'] = $val_sdmk_faskes_installasi['jumlah_DLL'];

                $_SESSION['sdmk_faskes_installasi'][] = $this->orm->sdmk_faskes_installasi->insert($data_sdmk_faskes_installasi);
            }
        }
    }

    public function save_sdmk_kbk_pokok($faskes_installasi) {

        
       
        
        foreach ($faskes_installasi as $key_faskes_installasi => $val_faskes_installasi) {

            foreach ($val_faskes_installasi['sdmk'] as $key_sdmk_faskes_installasi => $val_sdmk_faskes_installasi) {

                $urut_sdmk = $_SESSION['post_abk'][$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#urut_sdmk'];
                $uraian_kbk = $_SESSION['post_abk'][$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#uraian_kbk'];
                $norma_waktu = $_SESSION['post_abk'][$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#norma_waktu'];
                $capaian = $_SESSION['post_abk'][$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#capaian'];
                $satuan_id = $_SESSION['post_abk'][$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '_satuan_id'];

                foreach ($urut_sdmk as $key_kbk => $val_kbk) {

                    $sbk = $abk_model->_getSBK($norma_waktu[$key_kbk]);

                    $data_sdmk_kbk = array();
                    $data_sdmk_kbk['kbk_uraian'] = $uraian_kbk[$key_kbk];
                    $data_sdmk_kbk['norma_waktu'] = $norma_waktu[$key_kbk];
                    $data_sdmk_kbk['capaian'] = $capaian[$key_kbk];
                    $data_sdmk_kbk['sbk'] = $sbk;
                    $data_sdmk_kbk['satuan_id'] = $satuan_id[$key_kbk];
                    $data_sdmk_kbk['sdmk_faskes_installasi_id'] = $ress_sdmk_faskes_installasi['sdmk_faskes_installasi_id'];
                    $data_sdmk_kbk['jenis_tugas_id'] = 1;
                    $data_sdmk_kbk['urut'] = $key_kbk;
                    $this->orm->sdmk_kbk->insert($data_sdmk_kbk);
                }
            }
        }
    }
    
    
    public function save_sdmk_kbk_penunjang($faskes_installasi) {
      
        
       
        
        foreach ($faskes_installasi as $key_faskes_installasi => $val_faskes_installasi) {
            foreach ($val_faskes_installasi['sdmk'] as $key_sdmk_faskes_installasi => $val_sdmk_faskes_installasi) {
          
                $penunjang_urut_sdmk = $_POST[$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#penunjang_urut_sdmk'];
                $penunjang_uraian_kbk = $_POST[$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#penunjang_uraian_kbk'];
                $penunjang_norma_waktu = $_POST[$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#penunjang_norma_waktu'];
                //$penunjang_capaian = $_POST[$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#penunjang_capaian'];
                $penunjang_satuan_id = $_POST[$key_faskes_installasi . '#' . $key_sdmk_faskes_installasi . '#penunjang_satuan_id'];

                foreach ($penunjang_urut_sdmk as $key_penunjang_kbk => $val_penunjang_kbk) {
                    $waktu_kegiatan = $abk_model->_getWaktuKegiatan($penunjang_norma_waktu[$key_penunjang_kbk], $penunjang_satuan_id[$key_penunjang_kbk]);
                    $ftp = $waktu_kegiatan / $abk_model->_getWKTMenit();

                    $data_sdmk_penunjang_kbk = array();
                    $data_sdmk_penunjang_kbk['kbk_uraian'] = $penunjang_uraian_kbk[$key_penunjang_kbk];
                    $data_sdmk_penunjang_kbk['norma_waktu'] = $penunjang_norma_waktu[$key_penunjang_kbk];
                    //$data_sdmk_penunjang_kbk['capaian'] = $penunjang_capaian[$key_penunjang_kbk];
                    $data_sdmk_penunjang_kbk['waktu_kegiatan'] = $waktu_kegiatan;
                    $data_sdmk_penunjang_kbk['ftp'] = $ftp;
                    $data_sdmk_penunjang_kbk['satuan_id'] = $penunjang_satuan_id[$key_penunjang_kbk];
                    $data_sdmk_penunjang_kbk['sdmk_faskes_installasi_id'] = $ress_sdmk_faskes_installasi['sdmk_faskes_installasi_id'];
                    $data_sdmk_penunjang_kbk['jenis_tugas_id'] = 2;
                    $data_sdmk_penunjang_kbk['urut'] = $key_penunjang_kbk;
                    $this->orm->sdmk_kbk->insert($data_sdmk_penunjang_kbk);
                }
            }
        }
        
    }
    
    public function _checkKBKPokok($sdmk_faskes_installasi_id) {
        $sdmk_kbk = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,1);
        
        if(count($sdmk_kbk)==0) {
            return true;
        }
        else {
            return false;
        }
        
    }
    
    public function _checkKBKPenunjang($sdmk_faskes_installasi_id) {
       $sdmk_kbk = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,2);
        
        if(count($sdmk_kbk)==0) {
            return true;
        }
        else {
            return false;
        }
        
    }
    
    
     public function _checkMasterKBKPokok($ref_jenis_sdmk_kode) {
        $sdmk_kbk = $this->orm->kbk->where('ref_jenis_sdmk_kode = ? AND jenis_tugas_id = ?',$ref_jenis_sdmk_kode,1);
        
        if(count($sdmk_kbk)==0) {
            return true;
        }
        else {
            return false;
        }
        
    }
    
    public function _checkMasterKBKPenunjang($ref_jenis_sdmk_kode) {
       $sdmk_kbk = $this->orm->kbk->where('ref_jenis_sdmk_kode = ? AND jenis_tugas_id = ?',$ref_jenis_sdmk_kode,2);
        
        if(count($sdmk_kbk)==0) {
            return true;
        }
        else {
            return false;
        }
        
    }

}
