<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">


<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1200px;">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $abk_model = new abk_model();
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                echo "<h2>Langka 3 - Menetapkan Komponen Beban Kerja dan Norma Waktu</h2>";
                echo "<h3>{$sdmk_faskes_installasi['sdmk_faskes_installasi_nama']}</h3>";
                echo "<h3>{$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
                <br/>

                <?php if (!$abk_model->_checkKBKPenunjang($sdmk_faskes_installasi_id) OR ! $abk_model->_checkKBKPokok($sdmk_faskes_installasi_id)) { ?>
                    <a href="<?php echo base_url('index.php/entry/'.$class.'/tambahABK/' . $sdmk_faskes_installasi_id); ?>" class='btn btn-danger btn-sm'>
                        <i class="fa fa-refresh"></i> Reset Data
                    </a>
                    <br/>
                <?php } ?>

                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/tambahABK_proses/<?php echo $sdmk_faskes_installasi_id ?>">

                    <hr/>

                    <?php if(!empty($info)) { ?>
                    <div class="list-group-item list-group-item-danger" style="text-align: center;">
			        <h3><?php echo $info; ?></h3>     
                    </div> <br/>
                    <?php } ?>
                    
                                        <div class="list-group-item list-group-item-warning" style="text-align: center;">
			
                    </div>
                    
                    <br/>
                    
                    <div class="list-group-item list-group-item-info" style="text-align: center;">
			Kosongkan Norma Waktu Jika Uraian Pekerjaan Yang Tidak Dipilih
                    </div>
                    
                    
                    
                    

                    <br/>
                    <h3> Tugas Pokok </h3>
                    &nbsp; <i class="fa fa-plus"></i> <input  onclick="javascript:addRow('tbl_tugas_pokok');" type="button" value="tambah" class='btn btn-primary'>  &nbsp; &nbsp;  <i class="fa fa-trash-o"></i> <input onclick="javascript:deleteRow('tbl_tugas_pokok');" type="button" value="hapus" class="btn btn-danger" />
                    <br/>
                    <table id="tbl_tugas_pokok" width="90%" style='margin-top: 10px; margin-left: 10px;'>
                        <tr bgcolor="#dddddd" >
                            <td width="25px"><b>|#|</b> &nbsp; </td>
                            <td width="40px"><b>| No |</b> &nbsp; </td>
                            <td width="350px"><b>| Uraian Pekerjaan |</b> &nbsp; </td>
                            <td width="60px"><b>| Norma Waktu |</b> &nbsp; </td>
                            <td width="60px"><b>| Satuan |</b> &nbsp; </td>
                        </tr>

                        <?php
                        if ($abk_model->_checkKBKPokok($sdmk_faskes_installasi_id)) {  // jika SDMK data baru
                            if ($abk_model->_checkMasterKBKPokok($sdmk_faskes_installasi['ref_jenis_sdmk_kode'])) {
                                echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_pokok[]\" value=\"1\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_pokok[]\" value=\"\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_pokok\"   name=\"norma_waktu_tugas_pokok[]\" value=\"\"/>
                                                    <input type=\"hidden\" name=\"capaian[]\" value=\"\"/>  
                                                    </td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_pokok[]\" data-placeholder=\"Pilih Satuan\">"
                                . "";


                                foreach ($ref_satuan1 as $row) {

                                    echo "<option  value=\"{$row['satuan_id']}\"> <sup>{$row['satuan_nama']}</sup> </option>";
                                }

                                echo "</select></td>
                                                    </tr>";
                            } else {
                                $no = 1;
                             
                                if($sdmk_faskes_installasi['ref_jenis_sdmk_jft_id']==9) {
                                    $ref_jenis_sdmk_jft_id = array(1,2,3,4,11,12,13,14);
                                    $uraian_tugas_pokok = $this->orm->kbk->where('ref_jenis_sdmk_kode = ? AND jenis_tugas_id = ? AND ref_jenis_sdmk_jft_id IN(1,2,3,4,11,12,13,14)', $sdmk_faskes_installasi['ref_jenis_sdmk_kode'], 1)->order('kbk_id');
                                }
                                elseif($sdmk_faskes_installasi['ref_jenis_sdmk_jft_id']==10) {
                                    $ref_jenis_sdmk_jft_id = array(5,6,7,8);
                                    $uraian_tugas_pokok = $this->orm->kbk->where('ref_jenis_sdmk_kode = ? AND jenis_tugas_id = ? AND ref_jenis_sdmk_jft_id IN(5,6,7,8)', $sdmk_faskes_installasi['ref_jenis_sdmk_kode'], 1)->order('kbk_id');
                                }
                                else {
                                
                                $uraian_tugas_pokok = $this->orm->kbk->where('ref_jenis_sdmk_kode = ? AND jenis_tugas_id = ? AND ref_jenis_sdmk_jft_id = ?', $sdmk_faskes_installasi['ref_jenis_sdmk_kode'], 1,$sdmk_faskes_installasi['ref_jenis_sdmk_jft_id'])->order('kbk_id');
                                
                                }
                                
                            foreach ($uraian_tugas_pokok as $row) {
                                echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_pokok[]\" value=\"" . $no . "\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_pokok[]\" value=\"" . strip_tags($row['kbk_uraian']) . "\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_pokok\"   name=\"norma_waktu_tugas_pokok[]\" value=\"" . $row['norma_waktu'] . "\"/>
                                                    <input type=\"hidden\" name=\"capaian[]\" value=\"\"/>      
                                                    </td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_pokok[]\" data-placeholder=\"Pilih Satuan\">"
                                . "";

                                foreach ($ref_satuan1 as $row_satuan) {
                                    $selected = ($row['satuan_id'] == $row_satuan['satuan_id']) ? 'selected' : '';
                                    echo "<option {$selected}  value=\"{$row_satuan['satuan_id']}\"> <sup>{$row_satuan['satuan_nama']}</sup> </option>";
                                }

                                echo "</select></td>
                                                    </tr>";
                                $no++;
                            }
                                 echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_pokok[]\" value=\"".$no."\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_pokok[]\" value=\"\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_pokok\"   name=\"norma_waktu_tugas_pokok[]\" value=\"\"/>
                                                    <input type=\"hidden\" name=\"capaian[]\" value=\"\"/>  
                                                    </td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_pokok[]\" data-placeholder=\"Pilih Satuan\">"
                                . "";
                                   foreach ($ref_satuan1 as $row) {

                                    echo "<option  value=\"{$row['satuan_id']}\"> <sup>{$row['satuan_nama']}</sup> </option>";
                                }

                                echo "</select></td>
                                                    </tr>";
                                
                            }
                        } else { // jika SDMK edit
                            $no = 1;
                            foreach ($this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?', $sdmk_faskes_installasi_id, 1)->order('urut') as $row) {
                                echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_pokok[]\" value=\"" . $no . "\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_pokok[]\" value=\"" . strip_tags($row['kbk_uraian']) . "\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_pokok\"   name=\"norma_waktu_tugas_pokok[]\" value=\"" . $row['norma_waktu'] . "\"/>
                                                    <input type=\"hidden\" name=\"capaian[]\" value=\"" . $row['capaian'] . "\"/>    
                                                    </td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_pokok[]\" data-placeholder=\"Pilih Satuan\">"
                                . "";

                                foreach ($ref_satuan1 as $row_satuan) {
                                    $selected = ($row['satuan_id'] == $row_satuan['satuan_id']) ? 'selected' : '';
                                    echo "<option {$selected}  value=\"{$row_satuan['satuan_id']}\"> <sup>{$row_satuan['satuan_nama']}</sup> </option>";
                                }

                                echo "</select></td>
                                                    </tr>";
                                $no++;
                                
                            }
                        }
                        ?>
                    </table>

                    <p> <hr/> <hr/> </p>


                    <h3> Tugas Penunjang </h3>
                    &nbsp; <i class="fa fa-plus"></i> <input  onclick="javascript:addRow('tbl_tugas_penunjang');" type="button" value="tambah" class='btn btn-primary'> &nbsp; &nbsp;  <i class="fa fa-trash-o"></i> <input onclick="javascript:deleteRow('tbl_tugas_penunjang');" type="button" value="hapus" class="btn btn-danger" />
                    <br/>
                    <table id="tbl_tugas_penunjang" width="90%" style='margin-top: 10px; margin-left: 10px;'>
                        <tr bgcolor="#dddddd" >
                            <td width="25px"><b>|#|</b> &nbsp; </td>
                            <td width="40px"><b>| No |</b> &nbsp; </td>
                            <td width="350px"><b>| Uraian Pekerjaan |</b> &nbsp; </td>
                            <td width="60px"><b>| Norma Waktu |</b> &nbsp; </td>
                            <td width="60px"><b>| Satuan |</b> &nbsp; </td>
                        </tr>
<?php
if ($abk_model->_checkKBKPenunjang($sdmk_faskes_installasi_id)) {  // jika SDMK data baru
    if ($abk_model->_checkMasterKBKPenunjang($sdmk_faskes_installasi['ref_jenis_sdmk_kode'])) {
      
        $id_daftar_tugas_penunjang = array(1742,1745,1747,1751);
        $no = 1;        
        foreach ($this->orm->kbk->where('kbk_id',$id_daftar_tugas_penunjang)->order('kbk_id') as $row) {
        
         echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_penunjang[]\" value=\"" . $no . "\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_penunjang[]\" value=\"" . strip_tags($row['kbk_uraian']) . "\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_penunjang\"   name=\"norma_waktu_tugas_penunjang[]\" value=\"" . $row['norma_waktu'] . "\"/></td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_penunjang[]\" data-placeholder=\"Pilih Satuan\">"
        . "";

        foreach ($ref_satuan2 as $row) {

            echo "<option  value=\"{$row['satuan_id']}\"> <sup>{$row['satuan_nama']}</sup> </option>";
        }

        echo "</select></td>
       </tr>";
        $no++;
    }  
    
    
            }
    
    
  else {
       $id_daftar_tugas_penunjang = array(1742,1745,1747,1751);
        $no = 1;        
        foreach ($this->orm->kbk->where('kbk_id',$id_daftar_tugas_penunjang)->order('kbk_id') as $row) {
 
        echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_penunjang[]\" value=\"" . $no . "\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_penunjang[]\" value=\"" . strip_tags($row['kbk_uraian']) . "\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_penunjang\"   name=\"norma_waktu_tugas_penunjang[]\" value=\"" . $row['norma_waktu'] . "\"/></td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_penunjang[]\" data-placeholder=\"Pilih Satuan\">"
        . "";

        foreach ($ref_satuan2 as $row_satuan) {
            $selected = ($row['satuan_id'] == $row_satuan['satuan_id']) ? 'selected' : '';
            echo "<option {$selected}  value=\"{$row_satuan['satuan_id']}\"> <sup>{$row_satuan['satuan_nama']}</sup> </option>";
        }

        echo "</select></td>
                                                    </tr>";
        $no++;
    } 
    
    echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_penunjang[]\" value=\"".$no."\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_penunjang[]\" value=\"\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_penunjang\"   name=\"norma_waktu_tugas_penunjang[]\" value=\"\"/></td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_penunjang[]\" data-placeholder=\"Pilih Satuan\">"
        . "";

        foreach ($ref_satuan2 as $row) {

            echo "<option  value=\"{$row['satuan_id']}\"> <sup>{$row['satuan_nama']}</sup> </option>";
        }

        echo "</select></td>
       </tr>";
    
    }
} else { // jika sdmk edit
    $no = 1;
    foreach ($this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?', $sdmk_faskes_installasi_id, 2)->order('urut') as $row) {

        echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_penunjang[]\" value=\"" . $no . "\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_penunjang[]\" value=\"" . strip_tags($row['kbk_uraian']) . "\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"number\" autocomplete=\"off\" list=\"norma_waktu_tugas_penunjang\"   name=\"norma_waktu_tugas_penunjang[]\" value=\"" . $row['norma_waktu'] . "\"/></td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_penunjang[]\" data-placeholder=\"Pilih Satuan\">"
        . "";

        foreach ($ref_satuan2 as $row_satuan) {
            $selected = ($row['satuan_id'] == $row_satuan['satuan_id']) ? 'selected' : '';
            echo "<option {$selected}  value=\"{$row_satuan['satuan_id']}\"> <sup>{$row_satuan['satuan_nama']}</sup> </option>";
        }

        echo "</select></td>
                                                    </tr>";
        $no++;
    }
}
?>
                    </table>

                    <datalist id="list_kbk">
                        <?php foreach ($kbk as $row) { ?>
                            <option value="<?php echo strip_tags($row['kbk_uraian']) ?>">
                        <?php } ?>                          
                    </datalist>
                    <p>
                    <br/>
                    </p>
                    <div class="form-group">
                        <div class="col-sm-6"> 
                          <a href="<?php echo base_url('index.php/entry/'.$class.'/tambahWKT/'.$sdmk_faskes_installasi_id) ?>" class="btn btn-danger btn-flat" ><< Kembali</a>   
                        </div>
                        <div class="col-sm-6" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Selanjutnya >> </button>
                        &nbsp;
                        </div>
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


<?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/grid/standard.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });


    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
