<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang['synchronize'] = 'synchronize';
$lang['upgrade_now'] = 'Upgrade Now'; 
$lang['update_now'] = 'Update Now'; 
$lang['an_update_engine_version_of_codeanalytic_is_available'] = 'An update engine version of CodeAnalytic is available'; 
$lang['this_page_to_synchronize_your_cms_with_market,_see_documentation_for_use_it'] = 'This page to synchronize your cms with market, see documentation for use it';
$lang['before_updating,_please_backup_your_database_and_file'] = 'Before updating, please backup your database and file';
$lang['the_following_modules_have_new_version_available'] = 'The following modules have new version available';
$lang['the_following_extensions_have_new_version_available'] = 'The following extensions have new version available';
$lang['the_following_widgets_have_new_version_available'] = 'The following widgets have new version available';
$lang['the_following_themes_have_new_version_available'] = 'The following themes have new version available';
$lang['check_the_ones_you_want_to_update_and_then_click_update']='Check the ones you want to update and then click update';
$lang['not_available_update_for'] = 'Not available update for';
$lang['update_module_is_checked'] = 'Update module is checked';
$lang['update_extension_is_checked'] = 'Update extension is checked';
$lang['update_widget_is_checked'] = 'Update widget is checked';
$lang['update_theme_is_checked'] = 'Update theme is checked';

$lang['extension'] ='extension';