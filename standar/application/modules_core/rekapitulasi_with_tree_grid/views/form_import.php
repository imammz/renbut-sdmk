<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_import" action="<?php echo site_url('rekapitulasi/post_form')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">Import File</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <input class="form-control input-mask-date" type="file" id="form-field-mask-1" />
                      <span class="input-group-btn">
                        <button class="btn btn-sm btn-primary" type="button">
                          <i class="ace-icon fa fa-upload bigger-110"></i>
                          Upload
                        </button>
                        <a class="btn btn-sm btn-danger" href="#" onclick="show_hide_form_import('hide')">
                          <i class="ace-icon fa fa-remove bigger-110"></i>
                          Cancel
                        </a>
                      </span>
                    </div>
                  </div>
                </div>

              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/rekapitulasi.js'?>"></script>
