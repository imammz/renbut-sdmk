<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/widget
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('x_widget_position')) {
    function x_widget_position($p = null) {
        $json = x_file_get_content(THEMEPATH . 'public/' . theme_active() . '/configs/widget' . JSON);
        $d = x_read_json_decode($json);
        if ($p != null) {
            $pos = array();
            foreach ($d as $key => $value) {
                if (in_array($p, $value)) {
                    $pos[] = $d[$key];
                    $order[$key] = $value['order'];
                }
            }
            if (count($pos) > 0) {
                array_multisort($order, SORT_ASC, $pos);
            }
            return $pos;
        } else {
            sort($d);
            return $d;
        }
    }
}

if (!function_exists('x_widget_config')) {
    function x_widget_config($dir = null, $file = null) {
        if ($dir != null) {
            $vcon = array();
            if ($file != null) {
                $ff = WIPATH . $dir . '/configs/' . $file . JSON;
            } else {
                $ff = WIPATH . $dir . '/configs/config' . JSON;
            }
            if (is_file($ff)) {
                $json = x_file_get_content($ff);
                $arr = x_read_json_decode($json);
                foreach ($arr as $k => $v) {
                    foreach ($v as $keys => $vals) {
                        $vcon[$keys] = $v[$keys]['val'];
                    }
                }
                return $vcon;
            }
        }
    }
}

if (!function_exists('x_show_widget_position')) {
    function x_show_widget_position($pos = null) {
        if ($pos != null) {
            foreach (x_widget_position($pos) as $l) {
                if ($l['type'] == 'html') {
                    ?>
                    <div class="widget">
                        <div class="w-header">
                            <div class="w-title"><?php echo $l['title'] ?></div>
                        </div>
                        <div class="w-content"> 
                            <?php echo x_content_post($l['content']); ?> 
                        </div>
                    </div>
                    <?php
                } else {
                    $filename = WIPATH . strtolower($l['key']) . '/main_wi' . EXT;
                    if(is_file($filename)){
                    include_once $filename;
                    }
                }
            }
        }
    }
}

if (!function_exists('x_show_widget')) {
    function x_show_widget($widget = null) {
        if ($widget != null) {
            $X = &get_instance();
            $X->__widget($widget);
        }
    }
}