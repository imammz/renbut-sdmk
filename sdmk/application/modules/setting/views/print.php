<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?> 
<title>Pengaturan - Print</title>
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-wrench"></span> &nbsp;<?php echo $this->lang['setting'] ?>
         <sup class="sup-menu">
                <i class="fa fa-print"></i> report
            </sup>
        </h3>
        <span class="xamzo-head-info"><?php echo $this->lang['manage_your_configuration_report_file,_see_documentation_for_use_it'] ?></span>
    </div>
    <div class="right">
    </div>
</div>
<div id="xamzo-head-action">
    <button class="btn primary large fa fa-angle-down" dropdown="true" dropdown-relative="left" dropdown-target="post-add"><span class="no-phone "><?php echo $this->lang['setting']; ?></span></button>
    <button class="btn primary large fa fa-angle-down" dropdown="true" dropdown-relative="left" dropdown-target="dd-menu">
        <span class="no-phone "><?php echo $this->lang['option']; ?></span>
    </button>
    <button ajax="true" url-push="true" href="#!setting/_report" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div>
<ul class="dropdown-target dropdown-target-left dd-menu">
    <div class="arrow-up"></div>
    <li>
        <a ajax="true" url-push="true" href="#!setting/media" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> <?php echo $this->lang['general']; ?></a>
    </li>      
    <li>    
        <a ajax="true" url-push="true" href="#!setting/_report" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> Laporan Proyek</a>
    </li>   
    <li>   
        <a ajax="true" url-push="true" href="#!setting/_project" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> Proyek File</a>
    </li>   
</ul>
<ul class="dropdown-target dropdown-target-left post-add">
    <div class="arrow-up"></div>
    <li>
        <a ajax="true" url-push="true" href="#!setting/index" ajax-target=".xamzo-right"><i class="fa fa-bullhorn"></i> <?php echo $this->lang['info']; ?></a>
    </li>   
    <li>
        <a ajax="true" url-push="true" href="#!setting/general" ajax-target=".xamzo-right"><i class="fa fa-cog"></i> <?php echo $this->lang['general']; ?></a>
    </li>     
    <li>    
        <a ajax="true" url-push="true" href="#!setting/database" ajax-target=".xamzo-right"><i class="fa fa-hdd-o"></i> <?php echo $this->lang['database']; ?></a>
    </li> 
    <li>    
        <a ajax="true" url-push="true" href="#!setting/security" ajax-target=".xamzo-right"><i class="fa fa-lock"></i> <?php echo $this->lang['security']; ?></a>
    </li> 
</ul>
<div id="crud-target"> 
    <div class="xamzo-block xamzo-setting">
        <form id="crud-form">
            <?php
            if(count($con)>0){
                x_display_config($con);  
            } 
            ?>
            <p>
                <label  class="control-label big">&nbsp;</label>
                <button class="btn medium primary" href="<?php echo site_url('media/print_update'); ?>" ajax="true" ajax-target=".xamzo-right" ajax-redirect="<?php echo site_url('setting/_report'); ?>" ajax-data="#crud-form" ajax-type="POST" ><?php echo $this->lang['submit'] ?></button>
            </p> 
        </form> 
    </div>
</div>
