<?php


function mtimecmp($a, $b) {

    $fa = MODPATH . 'media/upload/image/' . $a;
    $fb = MODPATH . 'media/upload/image/' . $b;
    if (is_file($fa) && is_file($fb)) {
        $mt_a = filemtime($fa);
        $mt_b = filemtime($fb);

        if ($mt_a == $mt_b)
            return 0;
        else if ($mt_a > $mt_b)
            return -1;
        else
            return 1;
    }else {
        return 1;
    }
}

function ctimecmp($a, $b) {

    $fa = MODPATH . 'media/upload/crop/' . $a;
    $fb = MODPATH . 'media/upload/crop/' . $b;
    if (is_file($fa) && is_file($fb)) {
        $mt_a = filemtime($fa);
        $mt_b = filemtime($fb);

        if ($mt_a == $mt_b)
            return 0;
        else if ($mt_a > $mt_b)
            return -1;
        else
            return 1;
    }else {
        return 1;
    }
}

function doctimecmp($a, $b) {

    $fa = MODPATH . 'media/upload/doc/' . $a;
    $fb = MODPATH . 'media/upload/doc/' . $b;
    if (is_file($fa) && is_file($fb)) {
        $mt_a = filemtime($fa);
        $mt_b = filemtime($fb);

        if ($mt_a == $mt_b)
            return 0;
        else if ($mt_a > $mt_b)
            return -1;
        else
            return 1;
    }else {
        return 1;
    }
}

function ziptimecmp($a, $b) {

    $fa = MODPATH . 'media/upload/zip/' . $a;
    $fb = MODPATH . 'media/upload/zip/' . $b;
    if (is_file($fa) && is_file($fb)) {
        $mt_a = filemtime($fa);
        $mt_b = filemtime($fb);

        if ($mt_a == $mt_b)
            return 0;
        else if ($mt_a > $mt_b)
            return -1;
        else
            return 1;
    }else {
        return 1;
    }
}

function rtimecmp($a, $b) {

    $fa = REPORTPATH . $a;
    $fb = REPORTPATH . $b;
    if (is_file($fa) && is_file($fb)) {
        $mt_a = filemtime($fa);
        $mt_b = filemtime($fb);

        if ($mt_a == $mt_b)
            return 0;
        else if ($mt_a > $mt_b)
            return -1;
        else
            return 1;
    }else {
        return 1;
    }
}