<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        <div class="row">
          <div class="col-sm-12">
            <!-- #section:elements.tab -->
            <div class="tabbable">
              <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                  <a data-toggle="tab" href="#" onclick="backlist()">
                    <i class="green ace-icon fa fa-plus-square bigger-120"></i>
                    Puskesmas
                  </a>
                </li>

                <li>
                  <a data-toggle="tab" href="#" onclick="tab_rsu()">
                    <i class="green ace-icon fa fa-building bigger-120"></i>
                    RSU (Rumah Sakit Umum)
                  </a>
                </li>

                <li>
                  <a data-toggle="tab" href="#" onclick="tab_rsk()">
                    <i class="green ace-icon fa fa-hospital-o bigger-120"></i>
                    RSK (Rumah Sakit Khusus)
                  </a>
                </li>

                <li>
                  <a data-toggle="tab" href="#" onclick="tab_kkp()">
                    <i class="green ace-icon fa fa-anchor bigger-120"></i>
                    KKP
                  </a>
                </li>

                <li>
                  <a data-toggle="tab" href="#" onclick="tab_btklpp()">
                    <i class="green ace-icon fa fa-stethoscope bigger-120"></i>
                    BTKLPP
                  </a>
                </li>

                <li>
                  <a data-toggle="tab" href="#" onclick="tab_klinik()">
                    <i class="green ace-icon fa fa-medkit bigger-120"></i>
                    Klinik
                  </a>
                </li>

              </ul>

              <div class="tab-content">

                <div id="content_tab_jenis_faskes" class="tab-pane fade in active">

                <h3 class="center header smaller lighter green"><?php echo $tab_title?></h3>

                  <form class="form-horizontal" method="post" id="form_jml_standar_sdmk" action="<?php echo site_url('master_data/m_jenis_sdmk/ajax_add')?>">
                    <table id="dynamic-table" rel="puskesmas" class="table table-striped table-bordered table-hover">
                       <thead>
                        <tr>  
                          <th class="center" style="width: 50px"></th>
                          <th class="center" style="width: 90px">ID</th>
                          <th>Nama Jenis SDMK</th>
                          <th>Tipe Puskesmas</th>
                          <th>Tipe Kawasan</th>
                          <th class="center" style="width:250px">Jumlah Standar Minimal</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="center">( 1 )</td>
                          <td class="center">( 2 )</td>
                          <td class="center">( 3 )</td>
                          <td class="center">( 4 )</td>
                          <td class="center">( 5 )</td>
                          <td class="center">( 6 )</td>
                        </tr>
                      </tbody>
                    </table>

                  </form>

                </div>

              </div>
            </div>

            <!-- /section:elements.tab -->
          </div><!-- /.col -->

        </div><!-- /.row -->

        <div>
          
        </div>
      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/standar_sdmk.js'?>"></script>
