<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?> 
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-print"></span> &nbsp;Proyeksi Print Preview</h3>
        <span class="xamzo-head-info">Perhitungan Proyeksi Kebutuhan SDMK  Tahun 2014-2025</span>
    </div>
    <div class="right no-phone"> 
        <?php $uid = $this->session->get_data('session_user_id'); ?>
        <?php $header = 'Perhitungan Rencanan Kebutuhan SDMK Menggunakan Metode Proyeksi Penduduk' ?>
        <?php $title = 'BPPSDM Kementrian Kesehatan RI' ?>
        <?php $fname = 'c{x}name{x}date{x}time{x}' . $uid . ''; ?>  
    </div>
</div>
<div id="xamzo-head-action">
    <button class="btn primary large fa fa-chevron-left tip-top-left" ajax-url="#!proyeksi/index" crud-name="list" ajax="true" url-push="true"  ajax-target=".xamzo-right" original-title="<?php echo $this->lang['back']; ?>" accesskey="ctrl+backspace"></button>
    <button class="btn primary fa fa-print tip-top-right right" accesskey="p" original-title="print" onclick="print_excel('proyeksi/print_excel', '<?php echo $fname ?>', '<?php echo $header ?>', '<?php echo $title ?>');"></button>
</div> 
<div id="crud-target">  
    <form id="crud-form" class="print" onsubmit="return false;">
        <input type="hidden" name="id" value="<?php echo isset($id) ? $id : '' ?>">
        <input type="hidden" name="emitter" value="1">
        <div class="xamzo-block xamzo-block-full">
            <table class="table hack-table table-border" data-id="spredsheet" cellpadding="0" cellspacing="0">
                <tr> 
                    <td colspan="62" class="text-center">&nbsp;</td>
                </tr>  
                <tr>  
                    <th class="text-right" colspan="5">Kode Propinsi</th>
                    <td colspan="57">
                        <?php echo isset($id) ? '<span class="enable-dd"></span>' : '' ?> 
                        <span style="float: left;width:150px;margin-top: 3px">
                            <?php
                            echo form_dropdown($propinsi, isset($default['no_prov']) ? $default['no_prov'] : '', array('name' => 'no_prov', 'class' => 'combobox', 'validation' => 'required', 'placeholder' => 'semua-propinsi', 'style' => 'width:100px'));
                            ?> 
                            <?php echo isset($errors['no_prov']) ? $errors['no_prov'] : ''; ?>
                        </span>
                    </td> 
                </tr>  
                <tr> 
                    <td colspan="62" class="text-center">&nbsp;</td>
                </tr> 
                 <tr> 
                    <th colspan="62" class="text-center">&nbsp;</th>
                </tr>  
                <?php
                foreach ($rs as $prov) {
                    ?>
                    <tr> 
                        <td colspan="62" class="text-center">&nbsp;</td>
                    </tr>
                    <tr>
                        <th colspan="62" class="text-center"><?php echo $prov->nama_prov ?></th>
                    </tr>   
                    <tr> 
                        <td colspan="62">&nbsp;</td>
                    </tr> 
                    <tr class="text-center"> 
                        <th rowspan="2" style="width: 200px;">Jenis SDMK</th> 
                        <th colspan="12" style="width: 15%">Target Rasio Nakes</th>
                        <th colspan="12" style="width: 15%">Kebutuhan Nakes</th>
                        <th colspan="12" style="width: 15%">Jumlah Nakes Awal Tahun</th>
                        <th colspan="12" style="width: 15%">Jumlah Nakes Akhir Tahun</th>
                        <th colspan="12" style="width: 15%">Kesenjangan Kebutuhan Nakes</th>
                    </tr>
                    <tr class="text-center">  
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>   
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>  
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>  
                         
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>  
                        
                        <th>2014</th>   
                        <th>2015</th>   
                        <th>2016</th>   
                        <th>2017</th>  
                        <th>2018</th>  
                        <th>2019</th>  
                        <th>2020</th>   
                        <th>2021</th>   
                        <th>2022</th>   
                        <th>2023</th>   
                        <th>2024</th>   
                        <th>2025</th>  
                        
                        
                    </tr>
                    <?php
                    $t = 0;
                    foreach ($jenis_sdmk as $rp) {
                        $rt1 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 2);
                        $rt2 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 3);
                        $rt3 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 3);
                        $rt4 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 12);
                        $rt5 = proyeksi_calculate($prov->no_prov, $rp->no_jenis_sdmk, 13);
                        ?>
                        <tr> 
                            <td><?php echo $rp->nama_jenis_sdmk ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2014) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2015) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2016) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2017) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2018) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2019) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2020) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2021) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2022) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2023) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2024) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt1) ? round($rt1->th_2025) : '0' ?></td>
                            
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2014) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2015) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2016) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2017) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2018) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2019) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2020) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2021) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2022) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2023) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2024) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt2) ? round($rt2->th_2025) : '0' ?></td>
                            
                            
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2014) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2015) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2016) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2017) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2018) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2019) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2020) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2021) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2022) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2023) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2024) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2025) : '0' ?></td>
                            
                            <td class="text-center"><?php echo isset($rt4) ? round($rt4->th_2014) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt4) ? round($rt4->th_2015) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt4) ? round($rt4->th_2016) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2017) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2018) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2019) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2020) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2021) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2022) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2023) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2024) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt3) ? round($rt3->th_2025) : '0' ?></td>
                            
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2014) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2015) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2016) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2017) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2018) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2019) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2020) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2021) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2022) : '0' ?></td>
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2023) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2024) : '0' ?></td>  
                            <td class="text-center"><?php echo isset($rt5) ? round($rt5->th_2025) : '0' ?></td>
                        </tr>
                        <?php
                        $t++;
                    }
                }
                ?>
            </table>
        </div> 
    </form> 
</div> 
<script type="text/javascript">
    $('.combobox').combobox();
    $('select[name="no_prov"]').combobox({
        selected: function () {
            var no_prov = $(this).find(':selected').val();
            _preview(no_prov);
        }
    });

    function _preview(no_prov) {
        $.ajax({
            type: 'POST',
            beforeSend: function () {
                loading();
            },
            url: '<?php echo site_url('proyeksi/preview') ?>',
            data: 'no_prov=' + no_prov,
            success: function (rs) { 
                $('.xamzo-right').html(rs);
                close_box();
            }
        });
    }

</script>