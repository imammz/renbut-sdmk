<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$lang['module_list'] = 'Module list';
$lang['get_more_modules'] = 'get more modules';
$lang['module_is_up_to_date'] = 'module is up to date';
$lang['no_configuration_inside_the_module'] = 'no configuration inside the module';
$lang['this_page_to_manage_website_modules,_see_documentation_for_use_it'] = 'This page to manage website modules, see documentation for use it';