<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/module
 * @author		xamzo Developers
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://www.xamzo.com/license/
 * @link		http://www.xamzo.com
 * @since		Version 1.0
 * @filesource
 */
 
function x_mod_config($mod = null, $file = null) {
    if ($mod != null) {
        $vcon = array();
        if ($file != null) {
            $ff = MODPATH . $mod . '/configs/' . $file . JSON;
        } else {
            $ff = MODPATH . $mod . '/configs/config' . JSON;
        }
        if (is_file($ff)) {
            $json = x_file_get_content($ff);
            $arr = x_read_json_decode($json);
            foreach ($arr as $k => $v) {
                foreach ($v as $keys => $vals) {
                    $vcon[$keys] = $v[$keys]['val'];
                }
            }
            return $vcon;
        }
    }
}


/**
 * End of file ./system/helpers/module_helper.php
 */

