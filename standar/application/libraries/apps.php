<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class Apps {

   
    function get_template_content() {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $temp = $db->where(array('id_conf_application'=>1))->get('conf_application')->row();
				
		return $temp;
		
    }

    function counting($operator, $a, $b) {
		
		if( $operator == '+' ){
			$c = $a + $b;
		}elseif ( $operator == '-' ) {
			$c = $a - $b;
		}

		$format = Apps::get_format($c);
				
		return $format;
		
    }

    function get_format($params){
    	
    	if($params == 0){
    		$format = '<span style="color:blue"><strong>'.$params. ' ( Sesuai ) </strong></span>';
    	}elseif ($params > 0) {
    		$format = '<span style="color:green"><strong>'.$params. ' ( Lebih ) </strong></span>';
    	}else{
    		$format = '<span style="color:red"><strong>'.$params. ' ( Kurang ) </strong></span>';
    	}

    	return $format;

    }

    function get_header_report($params){
        
        $html = '';
        if($params->id_jenis_faskes == 1){
            $html .= strtoupper('Puskesmas '.$params->nama_puskesmas_kab. ' Kategori '.$params->nama_tipe_puskesmas.' Kawasan '.$params->nama_tipe_kawasan);  
        }else if ($params->id_jenis_faskes == 3) {
            $html .= strtoupper('Rumah Sakit '.$params->nama_rs. ' Kelas '.$params->kategori_kelas); 
        }else if ($params->id_jenis_faskes == 29) {
            $html .= strtoupper(''.$params->nama_btkl. ' Kelas '.$params->kategori_kelas); 
        }else if ($params->id_jenis_faskes == 30) {
            $html .= strtoupper('KKP '.$params->nama_kkp. ' Kelas '.$params->kategori_kelas); 
        }
        $html .= '<br>';
        $html .= strtoupper('Tahun '. $params->tahun);
        return $html;

    }

	
}

?> 