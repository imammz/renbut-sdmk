<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1200px;">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                
                echo "<h3>{$sdmk_faskes_installasi['sdmk_faskes_installasi_nama']}</h3>";
                echo "<h3>{$faskes_installasi['installasi_nama']} - {$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
                <hr/>
                <hr/>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/tambahResult_proses/<?php echo $sdmk_faskes_installasi_id ?>">
                    
                    <div class="box-body">
                        <?php echo "<h2>Langkah 4 - Standar Beban Kerja (SBK)</h2>"; ?>
                        <h4>Tugas Pokok</h4>
                        <table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th>Norma Waktu</th><th>Satuan</th><th>WKT (mnt/th)</th><th>SBK <br/> (Standar Beban Kerja)</th></tr>   
                            <?php 
                            $abk_model = new abk_model();
                            $no = 1;
                            $kbk1 = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,1)->order('urut ASC');
                            foreach($kbk1 as $row3) {
                                $satuan = $this->orm->ref_satuan->where('satuan_id',$row3['satuan_id'])->fetch();
                                ?>
                                <tr><td><?php echo $no ?></td><td><?php echo $row3['kbk_uraian'] ?></td><td style="text-align: right"><?php echo $row3['norma_waktu'] ?></td><td style="text-align: right"><?php echo $satuan['satuan_nama'] ?></td><td style="text-align: right"><?php echo number_format($abk_model->_getWKTMenit(),0); ?></td><td style="text-align: right"><?php echo $row3['SBK'] ?></td></tr>
                            <?php $no++; } ?>
                            </table>
                            <hr/>
                            <h2>Langkah 5 - Menghitung Faktor Tugas Penunjang (FTP) dan Standar Tugas Penunjang (STP)</h2>
                            <h4>Tugas Penunjang</h4>
                            <table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th>Norma Waktu</th><th>Satuan</th><th>Waktu Kegiatan (mnt/th)</th><th>WKT <br/> (mnt/th)</th><th style="text-align: center;"> <b> FTP <br/> % </b></th></tr>   
                            <?php 
                            $no = 1;
                            $ftp_total = 0;
                            $kbk2 = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,2)->order('urut ASC');
                            foreach($kbk2 as $row3) {
                                $satuan = $this->orm->ref_satuan->where('satuan_id',$row3['satuan_id'])->fetch();
                                $ftp_total += $row3['FTP'];
                                ?>
                                <tr><td><?php echo $no ?></td><td><?php echo $row3['kbk_uraian'] ?></td><td style="text-align: right"><?php echo $row3['norma_waktu'] ?></td><td style="text-align: right"><?php echo $satuan['satuan_nama'] ?></td> <td style="text-align: right"><?php echo $row3['waktu_kegiatan'] ?></td> <td style="text-align: right"> <?php echo number_format($abk_model->_getWKTMenit(),0); ?> </td><td style="text-align: right"><?php echo number_format($row3['FTP']*100, 2); ?></td></tr>
                            <?php $no++; } ?>
                                <tr><td colspan="5">  </td><td><b>TOTAL FTP</b></td><td style="text-align: right"><?php echo number_format($ftp_total*100, 2); ?></td></tr>
                                <tr><td colspan="6" style="text-align: right"><b>Standard  Tugas Penunjang  (STP)	=(1/(1 – FTP/100)</b></td><td style="text-align: right"><?php  echo $abk_model->_fmlSTP($ftp_total) ?></td></tr>
                            </table>
                            
                            <hr/>
                             <h2>Langkah 6 - Menghitung Kebutuhan SDMK</h2>
                            <table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th width="30" style="background-color: #FFE495"> Capaian </th><th>SBK <br/> (Standar Beban Kerja)</th><th> Kebutuhan SDMK </th></tr>   
                            <?php 
                            $no = 1;
                            foreach($kbk1 as $row3) {
                                $satuan = $this->orm->ref_satuan->where('satuan_id',$row3['satuan_id'])->fetch();
                                ?>
                                <tr><td><?php echo $no ?></td><td><?php echo $row3['kbk_uraian'] ?></td><td style="text-align: right"> 
                                        <input style="background-color: #FFE495" size="30" type="text" value="<?php echo $row3['capaian'] ?>" name="<?php echo $row3['sdmk_kbk_id'] ?>_capaian" required/> 
                                        <input type="hidden" name="sdmk_kbk_id[]" value="<?php echo $row3['sdmk_kbk_id'] ?>" />
                                    </td><td style="text-align: right"><?php echo $row3['SBK'] ?></td><td style="text-align: center"> ? </td></tr>
                            <?php $no++; } ?>
                            </table>
                        
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-sm-6"> 
                          <a href="<?php echo base_url('index.php/entry/'.$class.'/tambahABK/'.$sdmk_faskes_installasi_id) ?>" class="btn btn-danger btn-flat" ><< Kembali</a>   
                        </div>
                        <div class="col-sm-6" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Proses Perhitungan </button>
                        &nbsp;
                        </div>
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });


    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
