<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		helpers/auth
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.5
 * @filesource
 */
/**
 * read authentification user for each modules
 * @param type $modul
 * @param type $access
 * @return boolean
 */
if (!function_exists('is_auth')) {

    function is_auth($modul = null, $access = 'r') {
        $h = &get_instance();
        $h->__library(array('session'));
        $ses_id = $h->session->get_data('session_user_id');
        if ($ses_id != '') {
            $json = x_file_get_content(MODPATH . 'user/configs/auth' . JSON);
            $d = x_read_json_decode($json);
            $auth = array();
            foreach ($d as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($k == $ses_id) {
                        $auth[] = $value[$k];
                    }
                }
            }

            if ($modul != null && isset($auth['0'])) {
                foreach ($auth['0'] as $key => $value) {
                    foreach ($value as $k => $v) {
                        if (array_key_exists($k, $value)) {
                            if ($k == $modul) {
                                return $value[$modul][$access];
                            }
                        } else {
                            return false;
                        }
                    }
                }
            } else {
                return false;
            }
        } else {
            ?>
            <script type="text/javascript">
                window.open('<?php echo site_url('login') ?>', 'xamzo login', 'width=' + 600 + ',height=' + 520 + ',top=' + (screen.height - 520) / 2 + ', left=' + (screen.width - 600) / 2);
            </script>
            <?php
        }
    }

}
if (!function_exists('is_admin')) {

    function is_admin() {
        $h = &get_instance();
        $h->__library(array('session'));
        if ($h->session->get_data('session_level') == 'admin'){
            return true;
        } else {
            return false;
        }
    }

} 
if (!function_exists('is_user')) {

    function is_user() {
        $h = &get_instance();
        $h->__library(array('session'));
        if ($h->session->get_data('session_level') == 'regular_user') {
            return true;
        } else {
            return false;
        }
    }

}  
if (!function_exists('is_login')) {

    function is_login() {
        $h = &get_instance();
        $h->__library(array('session'));
        if ($h->session->get_data('session_user_id') != '') {
            return true;
        } else {
            return false;
        }
    }

}

/**
 * End of file ./system/helpers/auth_helper.php
 */