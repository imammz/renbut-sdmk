var loading_ = _h['0']['1'], animate_ = _h['0']['2'], base_ = _h['0']['0'], wrapper_ = _h['0']['3'];
$(function () {
    $('html').on('keyup', 'textarea.xamzo-textarea', function (e) {
        $(this).css({'height': 'auto', 'overflow': 'hidden'});
        $(this).height(this.scrollHeight);
    });
});
$(window).keydown(function (event) {
    if (event.keyCode === 123 || (event.ctrlKey && event.keyCode === 85)) {
        event.preventDefault();
    }
});
$(window).keydown(function (event) {
    if (event.ctrlKey && event.keyCode === 72) {
        var hs = '', hash = '';
        var val = i.value;
        if (window.location.hash) {
            hs = window.location.hash.substring(1);
            hash = hs.replace('/', '-');
        }
        load(hash, '.xamzo-right');
        event.preventDefault();
    }
});
function _set_check(_ck) {
    if ($(_ck).is(':checked')) {
        $(_ck).removeAttr('checked').parent().parent().parent().css({'opacity': '1', 'background': 'transparent'});
    } else {
        $(_ck).attr('checked', 'checked').parent().parent().parent().css({'opacity': '0.6', 'background': '#fffccc'});
        if ($(_ck).parent().find("td input:checked").length === 0) {
            $('[crud-name=delete],[crud-name=void],[crud-name=update]').removeAttr('disabled');
        } else {
            $('[crud-name=delete],[crud-name=void],[crud-name=update]').attr('disabled', 'disabled');
        }
    }
}
function _self_check(_ck) {
    var lcek = $('body').find("input:checked").length;
    if ($(_ck).is(':checked')) {
        $(_ck).parent().parent().parent().css({'opacity': '0.6', 'background': '#fffccc'});
    } else {
        $(_ck).parent().parent().parent().css({'opacity': '1', 'background': 'transparent'});
    }
    if (lcek > 0) {
        $('[crud-name=delete],[crud-name=void],[crud-name=update]').removeAttr('disabled');
    } else {
        $('[crud-name=delete],[crud-name=void],[crud-name=update]').attr('disabled', 'disabled');
    }
}
function _crud_cek() {
    if ($('body').find("td input:checked").length > 0) {
        $('[crud-name=delete],[crud-name=void],[crud-name=update]').removeAttr('disabled');
    } else {
        $('[crud-name=delete],[crud-name=void],[crud-name=update]').attr('disabled', 'disabled');
    }
}
!function ($) {
    $(function () {
        $('html').on('click', "[data-id='spredsheet']", function (e) {
            var $this = $(this);
            $this.find('td').click(function () {
                $(this).find('input[type="text"]').focus();
            });
        });
    });
}(window.$);
!function ($) {
    $(function () {
        $('html').on('click', "[type='checkbox']", function (e) {
            var $this = $(this);
            if ($this.parent().parent().attr('table-check-this') !== undefined) {
                _set_check(this);
                _crud_cek();
            }
        });
        $('html').on('click', '[table-check-this]', function (e) {
            var $this = $(this), _ck = $this.find("[type='checkbox']");
            _set_check(_ck);
            _crud_cek();
        });
        $('html').on('click', '[table-check-all=true]', function (e) {
            var $this = $(this), _parent = $this.parent().parent().parent();
            if ($this.hasClass('btn primary large')) {
                if ($this.parent().parent().find('input[table-check-all=true]').is(':checked')) {
                    $this.parent().parent().find("td input, li input").attr('checked', true);
                    $('[crud-name=delete],[crud-name=void],[crud-name=update]').attr('disabled', 'disabled');
                } else {
                    $this.parent().parent().find("td input, li input").removeAttr('checked');
                    $('[crud-name=delete],[crud-name=void],[crud-name=update]').removeAttr('disabled');
                }
            } else {
                if ($this.is(':checked')) {
                    $this.parent().parent().find("td input, li input").attr('checked', true);
                } else {
                    $this.parent().parent().find("td input, li input").removeAttr('checked');
                }
            }
            if ($this.is(':checked')) {
                $this.attr('checked', 'checked');
                var all = $(_parent).find("[type='checkbox']:not([table-check-all='true'])");
                all.removeAttr('checked');
                all.parent().parent().parent().click();
            } else {
                $this.removeAttr('checked');
                var all = $(_parent).find("[type='checkbox']:not([table-check-all='true'])");
                all.removeAttr('checked');
                all.parent().parent().parent().css({'background': 'transparent', 'opacity': '1'});
            }
        });
        $('html').on('click', '[crud-name=check]', function (e) {
            var ck = $('input[table-check-all="true"]'), _parent = ck.parent().parent().parent().parent();
            var all = $(_parent).find("[type='checkbox']:not([table-check-all='true'])");
            var x = all.parent().parent().parent();
            if (ck.is(':checked')) {
                ck.removeAttr('checked');
                x.css({'background': 'transparent', 'opacity': '1'});
            } else {
                ck.attr('checked', 'checked');
                x.click();
            }
        });
        $('html').on('click', '[crud-name=list],[crud-name=create]', function (e) {
            $('[crud-name=check]').removeAttr('disabled');
            $('[crud-name=delete],[crud-name=void],[crud-name=update]').attr('disabled', 'disabled');
        });
        $('html').on('click', '[crud-name=delete],[crud-name=void],[crud-name=update]', function (e) {
            var $this = $(this), _act_url = $this.attr('crud-url'), _crud_data = $this.attr('crud-data'), _crud_redirect = $this.attr('crud-redirect'), _crud_target = $this.attr('crud-target'), _act_name = $this.attr('crud-name'), _modal_width = $this.attr('modal-width'), _table_taget = $this.attr('crud-table-target');
            if ($(_table_taget + " td input:checked, " + _table_taget + " input:checked").length === 0) {
                var content = 'Target yang akan diubah atau dihapus tidak boleh kosong';
                var box = '<div class="modal-content" style="postion:relative">' +
                        '<button type="button" class="close" style="position:absolute;top:-7px;right:0px;cursor:pointer" onclick="close_box();">&times;</button>' +
                        '<div class="text-display"><i class="icon-bullhorn"></i>&nbsp; <blockquote class="style1"><span>' + content + '</span></blockquote>' + '</div>' +
                        '</div>';
                return_modalbox(box, 'true', '450px');
            } else {
                if ($(_table_taget + " td input:checked, " + _table_taget + " input:checked").length > 1 && _act_name === 'update') {
                    var content = 'Silahkan pilih satu untuk mengubah atau melihat detail';
                    var box = '<div class="modal-content" style="postion:relative;">' +
                            '<button type="button" class="close" style="position:absolute;top:-7px;right:0px;cursor:pointer" onclick="close_box();">&times;</button>' +
                            '<div class="text-display"><i class="icon-bullhorn"></i>&nbsp; <blockquote class="style1"><span>' + content + '</span></blockquote>' + '</div>' +
                            '</div>';
                    return_modalbox(box, 'true', '450px');
                } else {
                    switch (_act_name) {
                        case'delete':
                            o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Konfirmasi</h2>' + '</div>' + '<div class="modal-body"><p>';
                            _message_ = 'Apakah anda yakin untuk menghapus data yang terpilih';
                            f_str = '<div class="modal-footer border-top">' + '<button class="btn medium right primary" id="btn-del" onclick="_post(\'' + _crud_data + '\',\'true\',\'' + _act_url + '\',undefined,\'' + _crud_redirect + '\',\'' + _crud_target + '\')">Yes</button>';
                            c_str_ = '</div>';
                            return_modalbox(o_str_ + _message_ + c_str_ + f_str, '', '400px');
                            $('button#btn-del').focus();
                            break;
                        case'void':
                            o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Konfirmasi</h2>' + '</div>' + '<div class="modal-body"><p>';
                            _message_ = 'Apakah anda yakin untuk mem-void data yang terpilih';
                            f_str = '<div class="modal-footer border-top">' + '<button class="btn medium right primary" id="btn-void" onclick="_post(\'' + _crud_data + '\',\'true\',\'' + _act_url + '\',undefined,\'' + _crud_redirect + '\',\'' + _crud_target + '\')">Yes</button>';
                            c_str_ = '</div>';
                            return_modalbox(o_str_ + _message_ + c_str_ + f_str, '', '400px');
                            $('button#btn-void').focus();
                            break;
                        default:
                            if (_crud_target === 'modalbody') {
                                _post(_crud_data, 'false', _act_url, true, undefined, _crud_target, undefined, 'false', _modal_width);
                            } else {
                                _post(_crud_data, 'true', _act_url, true, undefined, _crud_target);
                                setTimeout(function () {
                                    $(_crud_target).removeClass(animate_);
                                }, 1000);
                            }
                            $('#tooltip').remove();
                            $('[crud-name=delete],[crud-name=void],[crud-name=update]').attr('disabled', 'disabled');
                            break
                    }
                }
            }
        });
        $('html').on('click', '[crud-name=expand-search]', function (e) {
            if ($('html').find('.xamzo-block-full').size() > 0) {
                $('.xamzo-block.small').show();
                $('.xamzo-block-full').addClass('big').removeClass('xamzo-block-full').show();
            } else {
                $('.xamzo-block.small').hide();
                $('.xamzo-block.big').addClass('xamzo-block-full').removeClass('big').show();
            }
        });
        is_search = false;
        $('html').on('keyup', '[crud-search="true"]', function (e) {
            var $this = $(this), _val = $this.val(), _name = $this.attr('name'), _act_url = $this.attr('search-url'), _act_target = $this.attr('search-target');
            if (is_search === false) {
                (e.keyCode) ? key = e.keyCode : key = e.which;
                switch (key) {
                    case 40:
                        $('.modal-content, body').find('tr[tabindex]').eq(1).focus().css({'background': 'rgb(228, 250, 228)'});
                        break;
                    default:
                        var _data;
                        if ($('form.print').length > 0) {
                            _data = $('form.print').serialize();
                        }
                        if (connetionState()) {
                            $.ajax({beforeSend: function () {
                                    is_search = true;
                                    $('#fadingBarsG').remove();
                                    $('span.small-loading').html(animate_small);
                                }, type: "POST", url: _act_url, data: _data + '&' + _name + '=' + _val, success: function (result) {
                                    $('span.small-loading').html('');
                                    $(_act_target).html(result);
                                    is_search = false;
                                }, dataType: "html"});
                        } else {
                            o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
                            c_str_ = '</p></div>';
                            return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
                        }
                        break
                }
            }
        });
    });
}(window.$);
(function ($) {
    if (typeof $.widget !== 'undefined') {
        $.widget("ui.combobox", {_create: function () {
                var self = this;
                var select = this.element, theWidth = select.width(), selected = select.children(":selected"), theTitle = (select.attr("placeholder")) ? select.attr("placeholder").replace(/-/g, ' ') : 'select', tabIndex = (select.attr("tabindex")) ? select.attr("tabindex") : '', readOnly = (select.attr("readonly")) ? select.attr("readonly") : '', value = selected.val() ? selected.text() : "";
                select.hide();
                var input = $("<input>");
                if (tabIndex !== '') {
                    input.attr('tabindex', tabIndex);
                }
                if (readOnly !== '') {
                    input.attr('readonly', readOnly);
                }
                input.val(value).attr('placeholder', '' + theTitle + '').autocomplete({delay: 0, minLength: 0, source: function (request, response) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        response(select.children("option").map(function () {
                            var text = $(this).text();
                            if (this.value && (!request.term || matcher.test(text)))
                                return{label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "$1"), value: text, option: this};
                        }));
                    }, select: function (event, ui) {
                        ui.item.option.selected = true;
                        self._trigger("selected", event, {item: ui.item.option});
                    }, change: function (event, ui) {
                        if (!ui.item) {
                            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"), valid = false;
                            select.children("option").each(function () {
                                if (this.value.match(matcher)) {
                                    this.selected = valid = true;
                                    return false;
                                }
                            });
                            if (!valid) {
                                $(this).val("");
                                select.val("");
                                return false;
                            }
                        }
                    }}).addClass("ui-widget ui-widget-content ui-corner-left");
                var span = $("<span style=\" white-space: nowrap;\" class='custom-combobox'></span>").append(input).insertAfter(select);
                $("<a>").attr("tabIndex", -1).css({'background': 'rgb(65, 173, 29)'}).attr("title", "Show All Items").insertAfter(input).button({icons: {primary: "ui-fa fa-triangle-1-s"}, text: false}).removeClass("ui-corner-all").addClass("custom-combobox-toggle ui-corner-right").click(function () {
                    if (input.autocomplete("widget").is(":visible")) {
                        input.autocomplete("close");
                        return;
                    }
                    input.autocomplete("search", "");
                    input.focus();
                });
            }});
    }
})(jQuery);
function _load(_url, _is_close_box, _url_push, _target, is_fix, _width, _is_iframe, _meth) {
    $.ajax({
        dataType: 'html',
        type: 'GET',
        beforeSend: function () {
            if (_meth === undefined) {
                loading();
            }
        }, url: _url.replace('#!', ''), success: function (result) {
            if (_meth === undefined) {
                close_loading_box(_is_close_box);
            }
            if (_url_push !== undefined) {
                if (_url_push !== 'false') {
                    if (window.history.pushState) {
                        if (typeof (window.history.pushState) === 'function') {
                            window.history.pushState(null, null, _url);
                        } else {
                            window.location.hash = '#!' + path;
                        }
                    }
                }
            }
            if (_meth !== undefined) {
                switch (_meth) {
                    case'prepend':
                        $(_target).prepend(result);
                        break;
                    case'append':
                        $(_target).append(result);
                        break;
                    default:
                        $(_target).html(result);
                        break
                }
            } else {
                _result_convert(_target, result, is_fix, _width, _is_iframe);
            }
            setTimeout(function () {
                $(_target).removeClass(animate_);
            }, 1000);
        }
    });
}
;
function _post(_data, _is_close_box, _url, _url_push, _url_redirect, _target, _meth, _is_fix, _width, _is_iframe) {
    $.ajax({type: 'POST', beforeSend: function () {
            loading();
        }, url: _url.replace('#!', ''),
        data: $(_data).serialize(),
        success: function (result) {
            switch (result.method) {
                case'login':
                    $('.xamzo-loginboxinner').html('<div><span style="margin-top:3px;float:left">' + animate_small + '</span>&nbsp; &nbsp; Sedang mengarahkan ke aplikasi...</div>');
                    setTimeout(function () {
                        window.location.href = base_ + 'h';
                    }, 1000);
                    break;
                case'logout':
                    location.replace(base_);
                    break;
                case'publish':
                    result = result.data.content;
                    break;
                default:
                    break
            }
            if (_url_redirect !== undefined) {
                _load(_url_redirect, _is_close_box, _url_push, _target, _is_fix, _width);
                if (_is_close_box !== 'false') {
                    if (_target !== 'modalbody') {
                        close_box();
                    }
                    notive('form berhasil disubmit');
                }
            } else {
                if (_meth !== undefined) {
                    switch (_meth) {
                        case'prepend':
                            $(_target).prepend(result);
                            break;
                        case'append':
                            $(_target).append(result);
                            break;
                        default:
                            $(_target).html(result);
                            break
                    }
                    notive('form berhasil disubmit');
                } else {
                    if (_target === 'false') {
                        close_box();
                        if (result === '') {
                            notive('form berhasil disubmit');
                        } else {
                            notive(result);
                        }
                    } else {
                        _result_convert(_target, result, _is_fix, _width, _is_iframe);
                    }
                }
                if (_is_close_box !== 'false') {
                    close_box();
                }
            }
        }});
}
function _method(_type, _url, _target, _is_close_box, _url_push, _data, _url_redirect, _meth, _is_fix, _width, _is_iframe) {
    if (connetionState()) {
        switch (_type) {
            case'GET':
                $(_data).validation();
                if (!$(_data).validate()) {
                } else {
                    $.ajax({type: 'GET', beforeSend: function () {
                            loading();
                        }, url: _url.replace('#!', ''), data: $(_data).serialize(), success: function (result) {
                            if (_url_redirect !== undefined) {
                                _load(_url_redirect, _is_close_box, _url_push, _target, _is_fix, _width, _is_iframe);
                                reset_form();
                            } else {
                                _result_convert(_target, result, _is_fix, _width, _is_iframe);
                            }
                            notive('Data Berhasil Disubmit');
                        }});
                }
                break;
            case'POST':
                $(_data).validation();
                if (!$(_data).validate()) {
                } else {
                    _post(_data, _is_close_box, _url, _url_push, _url_redirect, _target, _meth, _is_fix, _width, _is_iframe);
                }
                break;
            default:
                _load(_url, _is_close_box, _url_push, _target, _is_fix, _width, _is_iframe, _meth);
                break
        }
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
!function ($) {
    $(function () {
        $('html').on("keydown", function (e) {
            (e.keyCode) ? key = e.keyCode : key = e.which;
            if (key === 83 && e.ctrlKey) {
                $('body').find('[accesskey="ctrl+s"]').click();
                e.preventDefault();
            }
            if (key === 73 && e.ctrlKey) {
                if ($('body').find('.child-tab.active').find('[accesskey="ctrl+i"]').length > 0) {
                    $('body').find('.child-tab.active').find('[accesskey="ctrl+i"]').click();
                } else {
                    $('body').find('[accesskey="ctrl+i"]').click();
                }
                e.preventDefault();
            }
            if (key === 68 && e.ctrlKey) {
                if ($('body').find('.child-tab.active').find('[accesskey="ctrl+d"]').length > 0) {
                    $('body').find('.child-tab.active').find('[accesskey="ctrl+d"]').click();
                } else {
                    $('body').find('[accesskey="ctrl+d"]').click();
                }
                e.preventDefault();
            }
            if (key === 85 && e.ctrlKey) {
                if ($('body').find('.child-tab.active').find('[accesskey="ctrl+u"]').length > 0) {
                    $('body').find('.child-tab.active').find('[accesskey="ctrl+u"]').click();
                } else {
                    $('body').find('[accesskey="ctrl+u"]').click();
                }
                e.preventDefault();
            }
            if (key === 8 && e.ctrlKey) {
                $('body').find('[accesskey="ctrl+backspace"]').click();
                e.preventDefault();
            }
            if (key === 70 && e.ctrlKey) {
                $('body').find('[accesskey="ctrl+f"]').click();
                e.preventDefault();
            }
            if ((key === 116 && e.ctrlKey) || key === 116) {
                if ($('body').find('.child-tab.active').find('[accesskey="ctrl+f5"]').length > 0) {
                    $('body').find('.child-tab.active').find('[accesskey="ctrl+f5"]').click();
                } else {
                    $('body').find('[accesskey="ctrl+f5"]').click();
                }
                e.preventDefault();
            }
        });
    });
}(window.$);
!function ($) {
    $(function () {
        $('html').on('click', '[ajax=true]', function (e) {
            var tHtml = $('html').find('title').html();
            $this = $(this), _type = $this.attr('ajax-type'), _url = ($this.attr('ajax-url')) ? $this.attr('ajax-url') : $this.attr('href'), _target = $this.attr('ajax-target'), _meth = ($this.attr('ajax-method')) ? $this.attr('ajax-method') : undefined, _data = $this.attr('ajax-data'), _is_close_box = ($this.attr('is_close_box')) ? $this.attr('is_close_box') : 'false', _url_redirect = $this.attr('ajax-redirect'), _is_iframe = ($this.attr('ajax-iframe')) ? $this.attr('ajax-iframe') : 'false',
                    ajax_title = ($this.attr('ajax-title') !== '') ? $this.attr('ajax-title') : ((tHtml !== '') ? tHtml : ''), _url_push = $this.attr('url-push'), _is_fix = ($this.attr('modal-fix')) ? $this.attr('modal-fix') : 'true', _width = ($this.attr('modal-width')) ? $this.attr('modal-width') : '50%';
            e.preventDefault();
            if (_is_close_box === 'true') {
                close_box();
            }
            _method(_type, _url, _target, _is_close_box, _url_push, _data, _url_redirect, _meth, _is_fix, _width, _is_iframe);
            if (ajax_title !== '') {
                document.title = ajax_title;
            }
        });
    });
}(window.$);
!function ($) {
    $(function () {
        $('html').on('click', '[ajax-title]', function (e) {
            var tHtml = $('html').find('title').html();
            $this = $(this), ajax_title = ($this.attr('ajax-title') !== '') ? $this.attr('ajax-title') : ((tHtml !== '') ? tHtml : '');
            if (ajax_title !== '') {
                document.title = ajax_title;
            }
        });
    });
}(window.$);
function close_box(iframe) {
    if (iframe === 'iframe') {
        parent = $("html#xamzo", window.parent.document);
        parent.find('body').css({opacity: '1'});
        parent.find('.modal').removeClass('zoomIn animated').addClass('zoomOut animated').fadeOut('slow', function () {
            $(this).remove();
        });
    } else {
        $(".modal-box").removeClass('zoomIn animated').addClass('zoomOut animated').fadeOut('slow', function () {
            $(this).remove();
        });
        $('.modal').removeClass('zoomIn animated').addClass('zoomOut animated').fadeOut('slow', function () {
            $(this).remove();
        });
        $(wrapper_ + ', body, #in-cms').css({opacity: 1});
        $("#tooltip").remove();
    }
    uf = $('input[name="userfile[]"]');
    for (i = 0; i < uf.size(); i++) {
        if (i > 1) {
            uf[i].fadeOut('slow', function () {
                $(this).remove();
            });
        }
    }
}
function close_loading_box(_is_close_box, iframe) {
    if (_is_close_box === 'true') {
        $(wrapper_ + ', body').css({opacity: '1'});
    }
    if (iframe === 'iframe') {
        parent = $("html#xamzo", window.parent.document);
        if (_is_close_box === 'true') {
            parent.find('body').css({opacity: '1'});
        }
        parent.find('.xamzo-loading-box').remove();
    } else {
        $(".xamzo-loading-box").remove();
        if (_is_close_box === 'true') {
            $(wrapper_ + ', body').css({opacity: '1'});
        }
        $("#tooltip").remove();
    }
    uf = $('input[name="userfile[]"]');
    for (i = 0; i < uf.size(); i++) {
        if (i > 1) {
            uf[i].remove();
        }
    }
}
function _url_craw() {
    b = window.document.location.hash;
    patern = "^(#!)";
    c = new RegExp(patern);
    if (b.match(c)) {
        close_box();
        _load(b, 'true', '', wrapper_);
    }
}
window.addEventListener("hashchange", _url_craw, false);
$(document).ready(function () {
    _url_craw();
    $('html').click(function () {
        if ($(".modal-notive").length > 0) {
            $(".modal-notive").remove();
            $('body').css({'opacity': '1'});
        }
    });
});
$(document).keyup(function (e) {
    e.preventDefault();
    if (e.keyCode === 27) {
        close_box('iframe');
        close_box();
        $('.collapse').hide();
        $('.xamzo-autocomplete').html('');
    }
});
!function ($) {
    $(function () {
        _par = $('.collapse');
        $.each(_par, function () {
            $(this).find('div.coll-child').hide().first().show();
        });
        $('html').on('click', '.coll-menu', function (e) {
            $this = $(this), _parent = ($this.parent()) ? $this.parent() : '0', _speed = (_parent.attr('coll-speed')) ? _parent.attr('coll-speed') : 'fast', _target = ($this.attr('coll-target')) ? $this.attr('coll-target') : '0';
            if ($this.hasClass('active-coll-menu')) {
            } else {
                $(_parent).find('.coll-menu').removeClass('active-coll-menu');
                $this.addClass('active-coll-menu');
                $(_parent).find('.coll-child').hide(_speed).removeClass('active-coll-content');
                $(_parent).find(_target).slideDown(_speed).addClass('active-coll-content');
            }
        });
    });
}(window.$);
$(function () {
    $.ajaxSetup({error: function (jqXHR, exception) {
            o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
            c_str_ = '</p></div>';
            if (jqXHR.status === 0) {
                return_modalbox(o_str_ + 'Not connect.\n Verify Network.' + c_str_, 'true', '400px');
            } else if (jqXHR.status === 404) {
                return_modalbox(o_str_ + 'Requested page not found. [404]' + c_str_, 'true', '400px');
            } else if (jqXHR.status === 500) {
                return_modalbox(o_str_ + 'Internal Server Error [500].' + c_str_, 'true', '400px');
            } else if (exception === 'parsererror') {
            } else if (exception === 'timeout') {
            } else if (exception === 'abort') {
            } else {
            }
        }});
});
function before_loading(_target) {
    $(_target).removeClass(animate_);
    $('.pop-content').parent().remove();
    loading();
}
function reset_form() {
    $('form input, form textarea').val('');
    $('[contenteditable], .xamzo-tag span.pTag').html('');
}
function loading(a) {
    $(".xamzo-loading-box").remove();
    cmd = "<div class='modal-box xamzo-loading-box' style='display:none;border:none;padding:0px; z-index:1000;'>" + "<div class='modal-content' style='background: transparent; text-align:center;' align='center'>" + loading_ + "</div>" + "</div>";
    $(wrapper_).css({opacity: 0.3});
    $('html').append(cmd);
    function _make_loading() {
        b = $(window).height(), c = $(".modal-box").height(), d = (b - c) / 2, e = $(window).width(), f = (e - $(".modal-box").width()) / 2;
        $(".modal-box").css({position: 'fixed', left: f, top: d}).fadeIn('slow');
    }
    _make_loading();
}
function notive(text) {
    $(".modal").remove();
    var cmd = "<div class='modal-box modal-notive' onclick='$(this).remove();close_box()' style='padding:0px 10px !important; z-index:1000;'>" + "<div class='modal-content' style='width:100%; background: transparent; text-align:center; cursor:pointer;' align='center'> <i style='font-size: 18px;float: left;left: 0px;padding:4px 0;' class='fa fa-bullhorn'></i> &nbsp;" + text + "</div>" + "</div>";
    $('html').append(cmd);
    function _make_notive() {
        c = $(".modal-box").height(), d = ('40%'), e = $(window).width(), f = (e - $(".modal-notive").width()) / 2;
        $(".modal-box").css({position: 'fixed', left: f, top: d});
    }
    _make_notive();
    $('body').css({'opacity': '0.2'});
}
function _result_convert(_target, result, is_fix, _width, _is_iframe) {
    if (_target === 'modalbody') {
        return_bodybox(result, is_fix, _width, _is_iframe);
    }
    if (_target === 'modalbox') {
        return_modalbox(result, is_fix, _width, _is_iframe);
    } else {
        $(_target).html(result).addClass(animate_).addClass("animated");
        is_box = $(_target).parent().parent().parent().find('.modal').size();
        if (is_box === 0) {
        }
    }
    $('html').find('div#ui-datepicker-div').removeClass('ui-helper-hidden-accessible');
    $(window).resize(function () {
        mobile_tip();
    });
    mobile_tip();
    insortable();
}
function mobile_tip() {
    if ($(window).width() < 768) {
        var pos = $('.no-tablet').attr('position');
        switch (pos) {
            case'left':
                $('.no-tablet').parent().addClass('tip-left');
                break;
            case'right':
                $('.no-tablet').parent().addClass('tip-right');
                break;
            case'bottom':
                $('.no-tablet').parent().addClass('tip-bottom');
                break;
            case'top-left':
                $('.no-tablet').parent().addClass('tip-top-left');
                break;
            case'top-right':
                $('.no-tablet').parent().addClass('tip-top-right');
                break;
            case'bottom-right':
                $('.no-tablet').parent().addClass('tip-bottom-right');
                break;
            case'bottom-left':
                $('.no-tablet').parent().addClass('tip-bottom-left');
                break;
            default:
                $('.no-tablet').parent().addClass('tip-top');
                break
        }
    } else {
        $('.no-tablet').parent().removeClass('tip-top').removeClass('tip-right').removeClass('tip-bottom').removeClass('tip-left').removeClass('tip-top-right').removeClass('tip-top-left').removeClass('tip-bottom-left').removeClass('tip-bottom-right');
        $('.xamzo-right, .xamzo-main').css('opacity', '1');
    }
}
function return_modalbox(content, is_fix, _width, _is_iframe) {
    var date = new Date();
    var id = date.getTime();
    close_box();
    var el = '';
    if (_is_iframe === 'true') {
        el = $("html#xamzo", window.parent.document);
    } else {
        el = $("html#xamzo");
    }
    el.append('<div class="modal zoomIn animated">&nbsp;</div>');
    if ($(window).width() <= 480) {
        width_ = '90%';
    } else {
        width_ = _width;
    }
    el.find(".modal").append('<div class="modal-box modal-box-' + id + ' zoomIn animated" style="width:' + width_ + '">' + "<div class='modal-content'></div>" + "</div>");
    el.find(".modal-content").append(content);
    function _make_modalbox() {
        b = el.width();
        c = (b - el.find(".modal-box-" + id).width()) / 2;
        d = $(window).height();
        e = (d - el.find(".modal-box-" + id).height()) / 3;
        var xe;
        if (e > 0) {
            xe = e;
        } else {
            xe = '50px';
        }
        if (is_fix !== 'false') {
            el.find(".modal").css({'position': 'fixed'});
            el.find(".modal-box-" + id).css({'position': 'fixed', left: c, top: xe});
        } else {
            el.find(".modal-box-" + id).css({'position': 'absolute', left: c, top: xe});
        }
    }
    _make_modalbox();
    el.find(".loading").html("");
    el.find('.modal').css({'height': $('body').height(), 'min-height': '100%'});
    if (_is_iframe === 'true') {
        el.find('.modal').css({'top': '10px'});
        el.find(".modal-box-" + id).css({'position': 'absolute'});
    }
    el.find('body').css({opacity: .1});
    if (is_fix === 'false') {
        $('body').scrollTop(0);
    }
}
function return_bodybox(content, is_fix, _width, _is_iframe) {
    var date = new Date();
    var id = date.getTime();
    close_box();
    var el = '';
    if (_is_iframe === 'true') {
        el = $("body", window.parent.document);
    } else {
        el = $("body");
    }
    el.append('<div class="modal zoomIn animated">&nbsp;</div>');
    if ($(window).width() <= 480) {
        width_ = '90%';
    } else {
        width_ = _width;
    }
    el.find(".modal").append('<div class="modal-box modal-box-' + id + ' zoomIn animated" style="width:' + width_ + '">' + "<div class='modal-content'></div>" + "</div>");
    el.find(".modal-content").append(content);
    function _make_modalbox() {
        b = el.width();
        c = (b - el.find(".modal-box-" + id).width()) / 2;
        d = $(window).height();
        e = (d - el.find(".modal-box-" + id).height()) / 3;
        if (is_fix !== 'false') {
            el.find(".modal").css({'position': 'fixed'});
            el.find(".modal-box-" + id).css({'position': 'fixed', left: c, top: 80});
        } else {
            el.find(".modal-box-" + id).css({'position': 'absolute', left: c, top: 80});
        }
    }
    _make_modalbox();
    el.find(".loading").html("");
    el.find('.modal').css({'height': $('body').height(), 'min-height': '100%'});
    if (_is_iframe === 'true') {
        el.find('.modal').css({'top': '10px'});
        el.find(".modal-box-" + id).css({'position': 'absolute'});
    }
    el.find('#in-cms').css({opacity: .1});
    if (is_fix === 'false') {
        $('body').scrollTop(0);
    }
}
function _view_dropdown(t, _relative, _target, _scroll) {
    function _make_dropdown() {
        $(wrapper_).removeClass('animated').removeClass(animate_);
        _version = '1.0', _owidth = t.innerWidth(), _oheight = t.innerHeight();
        if (_scroll !== undefined) {
            _oleft = t.offset().left - ($(window).scrollLeft()), _otop = (t.offset().top - ($(window).scrollTop()));
        } else {
            _oleft = t.offset().left;
            _otop = (t.offset().top);
        }
        $this = $("." + _target);
        _odropdown_width = $this.innerWidth(), _odropdown_height = t.innerHeight();
        var modal = $('html').find('.modal-box');
        if (modal.length > 0) {
            $this.css({"list-style": "none", "top": (_otop + (_odropdown_height)) - modal.css('top') + "px"});
            if (_relative === 'right') {
                $this.css({"left": _oleft - _odropdown_width + _owidth - modal.css('left') + "px"});
            }
            if (_relative === 'left') {
                $this.css({"left": _oleft - modal.css('left') + "px"});
            }
        } else {
            $this.css({"list-style": "none", "top": (_otop + (_odropdown_height)) + 2 + "px"});
            if (_relative === 'right') {
                $this.css({"left": _oleft - _odropdown_width + _owidth + "px"});
            }
            if (_relative === 'left') {
                $this.css({"left": _oleft + "px"});
            }
        }
        $this.find('li').each(function (i, el) {
            $(el).attr('tabindex', parseInt(i + 1));
        });
        $this.css({"position": "absolute", "z-index": '1000'}).fadeIn("fast").focus();
    }
    _make_dropdown();
}
!function ($) {
    $(function () {
        _par = $('[dropdown="true"]');
        $.each(_par, function () {
            $('.' + $(this).attr('dropdown-target')).hide();
        });
        $('html').on('click', '[dropdown="true"]', function (e) {
            $('html').find('.dropdown-target').hide();
            $('html').find('[dropdown="true"]').attr({'_target_dropdown': ''});
            $this = $(this), _id = new Date(), _relative = ($this.attr('dropdown-relative')) ? $this.attr('dropdown-relative') : '0', _target = ($this.attr('dropdown-target')) ? $this.attr('dropdown-target') : 'dropdown-menu', _init = ($this.attr('dropdown-init')) ? $this.attr('dropdown-init') : '0', _scroll = $this.attr('dropdown-scroll');
            if (_init === '1') {
                $('.dropdown-target').hide();
            }
            if ($this.hasClass('active')) {
                $this.removeClass('active');
                $('.' + _target).hide();
                $this.attr('_target_dropdown', '');
            } else {
                $this.attr('_target_dropdown', '' + _id.getTime() + '');
                $this.addClass('active');
                _view_dropdown($this, _relative, _target, _scroll);
            }
        });
    });
}(window.$);
!function ($) {
    $(function () {
        _par = $('[modal="true"]');
        $.each(_par, function () {
            $('#' + $(this).attr('modal-target')).hide();
        });
        $('html').on('click', '[modal="true"]', function (e) {
            $this = $(this), _id = new Date(), _target = $('#' + $this.attr('modal-target')), _is_fix = ($this.attr('modal-fix')) ? $this.attr('modal-fix') : 'true', _width = ($this.attr('modal-width')) ? $this.attr('modal-width') : '50%', _content = _target.html();
            close_box();
            return_modalbox(_content, _is_fix, _width);
        });
    });
}(window.$);
function _view_pop(_id, t, _top, _left, _right, _bottom, _header, _content, _icon) {
    $(wrapper_).removeClass('animated').removeClass(animate_);
    function _make_popover() {
        _version = '1.0', _oleft = t.offset().left, _otop = t.offset().top, _owidth = t.width(), _oheight = t.height();
        $('html').append("<div id='" + _id + "' style='display:none'><div class='pop-header'><span class='" + _icon + "'></span>&nbsp;" + _header + "</div><div class='pop-content'>" + _content + "</div></div>");
        $this = $("#" + _id);
        _opop_width = $this.width(), _opop_height = $this.height();
        if (_left !== '0') {
            $("#" + _id).css({"top": (_otop) - (_opop_height / 2) + "px", "left": _oleft - (_opop_width + 30) + "px"}).append('<span class="fa fa-caret-right t-carret"></span>');
            $("#" + _id + " span.t-carret").css({"position": "absolute", "right": -5, "top": (_opop_height / 2) + 5});
        }
        if (_right !== '0') {
            $("#" + _id).css({"top": (_otop) - (_opop_height / 2) + "px", "left": _owidth + _oleft + 25 + "px"}).append('<span class="fa fa-caret-left t-carret"></span>');
            $("#" + _id + " span.t-carret").css({"position": "absolute", "left": -5, "top": (_opop_height / 2) + 5});
        }
        if (_top !== '0') {
            $("#" + _id).css({"left": (_oleft) - (_opop_width / 2) + (_owidth / 2) + "px", "top": (_otop - ((_oheight * 2) + _opop_height)) - 5 + "px"}).append('<span class="fa fa-caret-down t-carret"></span>');
            $("#" + _id + " span.t-carret").css({"position": "absolute", "bottom": -15, "left": (_opop_width / 2)});
        }
        if (_bottom !== '0') {
            $("#" + _id).css({"top": (_otop + (_oheight * 2)) + 3 + "px", "left": _oleft - ((_opop_width / 2)) + (_owidth / 2) + "px"}).append('<span class="fa fa-caret-up t-carret"></span>');
            $("#" + _id + " span.t-carret").css({"position": "absolute", "top": -14, "left": (_opop_width / 2)});
        }
        $("#" + _id).css({"z-index": 10, "position": "absolute", "display": "none", "background": "#FFF"}).fadeIn("fast");
        $("#" + _id + " .pop-header").css({"padding": "5px"});
        $("#" + _id + " .pop-content").css({"padding": "10px"});
    }
    _make_popover();
    $(window).resize(function () {
        $("#" + _id).remove();
        t.removeClass('active');
    });
}
!function ($) {
    $(function () {
        $('html').on('click', '[pop="true"]', function (e) {
            $this = $(this), _id = new Date(), _left = ($this.hasClass('pop-left')) ? $this.hasClass('pop-left') : '0', _icon = ($this.attr('pop-icon')) ? $this.attr('pop-icon') : '0', _top = ($this.hasClass('pop-top')) ? $this.hasClass('pop-top') : '0', _right = ($this.hasClass('pop-right')) ? $this.hasClass('pop-right') : '0', _bottom = ($this.hasClass('pop-bottom')) ? $this.hasClass('pop-bottom') : '0', _content = ($this.attr('pop-content')) ? $this.attr('pop-content') : 'content popover', _header = ($this.attr('pop-header')) ? $this.attr('pop-header') : 'header popover';
            if ($this.hasClass('active')) {
                $(this).removeClass('active');
                _id_pop = $this.attr('_id_pop');
                $('#' + _id_pop).remove();
                $(this).attr('_id_pop', '');
            } else {
                $(this).attr('_id_pop', '' + _id.getTime() + '');
                $(this).addClass('active');
                _view_pop(_id.getTime(), $this, _top, _left, _right, _bottom, _header, _content, _icon);
            }
        });
    });
}(window.$);
!function ($) {
    $(function () {
        _par = $('.tab');
        $.each(_par, function () {
            $($(this).attr('tab-parent')).find('.child-tab').hide().first().show();
        });
        $('html').on('click', '.tab a', function (e) {
            $this = $(this), _parent = ($this.parent().attr('tab-parent')) ? $this.parent().attr('tab-parent') : '0', _target = ($this.attr('tab-target')) ? $this.attr('tab-target') : '0', _animation = ($this.parent().attr('tab-animation')) ? $this.parent().attr('tab-animation') : 'none';
            $this.parent().find('a').removeClass('active');
            $this.addClass('active');
            $(_parent + ' .child-tab').removeClass(_animation).removeClass('active').hide().addClass("animated");
            $(_parent + ' ' + _target).addClass(_animation).addClass('active').show().addClass("animated");
            e.preventDefault();
        });
    });
}(window.$);
function _view_tip(t, _title, _icon, _top, _left, _bottom, _right, _top_left, _top_right, _bottom_left, _bottom_right) {
    $(wrapper_).removeClass('animated').removeClass(animate_);
    _version = '1.0', _oleft = t.offset().left, _otop = t.offset().top, _owidth = t.innerWidth(), _oheight = t.innerHeight();
    var paddT = t.innerHeight() - t.height();
    $("html").append("<p id='tooltip' style='display:none;position:'><span class='fa fa-tip " + _icon + "'></span>&nbsp;" + _title + "</p>");
    $this = $("p#tooltip");
    _otip_width = $this.innerWidth(), _otip_height = $this.innerHeight();
    if (_left !== '0') {
        $("#tooltip").css({"top": (_otop - (paddT / 1.3)) + "px", "left": _oleft - (_otip_width + 5) + "px"}).append('<span class="fa fa-caret-right t-carret"></span>');
        $("span.t-carret").css({"right": -4, "top": 9});
    }
    if (_top !== '0') {
        $("#tooltip").css({"left": _oleft - ((_otip_width / 2)) + (_owidth / 2) + "px", "top": (_otop - _otip_height) - paddT + "px"}).append('<span class="fa fa-caret-down t-carret"></span>');
        $("span.t-carret").css({"bottom": -8, "left": (_otip_width / 2)});
    }
    if (_right !== '0') {
        $("#tooltip").css({"top": (_otop - (paddT / 1.3)) + "px", "left": _owidth + _oleft + 9 + "px"}).append('<span class="fa fa-caret-left t-carret"></span>');
        $("span.t-carret").css({"left": -4, "top": 9});
    }
    if (_bottom !== '0') {
        $("#tooltip").css({"top": (_otop + _oheight) + "px", "left": _oleft - ((_otip_width / 2)) - (paddT / 3) + (_owidth / 2) + "px"}).append('<span class="fa fa-caret-up t-carret"></span>');
        $("span.t-carret").css({"top": -8, "left": (_otip_width / 2)});
    }
    if (_bottom_left !== '0') {
        $("#tooltip").css({"top": (_otop + _oheight) + "px", "left": _oleft + "px"}).append('<span class="fa fa-caret-up t-carret"></span>');
        $("span.t-carret").css({"top": -8, "left": 10});
    }
    if (_bottom_right !== '0') {
        $("#tooltip").css({"top": (_otop + _oheight) + "px", "left": _oleft - (_otip_width) + _owidth + "px"}).append('<span class="fa fa-caret-up t-carret"></span>');
        $("span.t-carret").css({"top": -8, "right": 10});
    }
    if (_top_left !== '0') {
        $("#tooltip").css({"top": (_otop - _oheight) - paddT + "px", "left": _oleft + "px"}).append('<span class="fa fa-caret-down t-carret"></span>');
        $("span.t-carret").css({"bottom": -8, "left": 10});
    }
    if (_top_right !== '0') {
        $("#tooltip").css({"top": (_otop - _oheight) - paddT + "px", "left": _oleft - (_otip_width) + _owidth + "px"}).append('<span class="fa fa-caret-down t-carret"></span>');
        $("span.t-carret").css({"bottom": -8, "right": 10});
    }
    $("#tooltip").css({position: 'absolute', "z-index": '1000'}).fadeIn("fast");
}
!function ($) {
    $(function () {
        $('html').on('hover', '[class*="tip"]', function (e) {
            $this = $(this), _left = ($this.hasClass('tip-left')) ? $this.hasClass('tip-left') : '0', _bottom_left = ($this.hasClass('tip-bottom-left')) ? $this.hasClass('tip-bottom-left') : '0', _bottom_right = ($this.hasClass('tip-bottom-right')) ? $this.hasClass('tip-bottom-right') : '0', _top_right = ($this.hasClass('tip-top-right')) ? $this.hasClass('tip-top-right') : '0', _top_left = ($this.hasClass('tip-top-left')) ? $this.hasClass('tip-top-left') : '0', _icon = ($this.attr('tip-icon')) ? $this.attr('tip-icon') : '0', _top = ($this.hasClass('tip-top')) ? $this.hasClass('tip-top') : '0', _right = ($this.hasClass('tip-right')) ? $this.hasClass('tip-right') : '0', _bottom = ($this.hasClass('tip-bottom')) ? $this.hasClass('tip-bottom') : '0', _title = ($this.attr('original-title')) ? $this.attr('original-title') : 'tooltip';
            _view_tip($this, _title, _icon, _top, _left, _bottom, _right, _top_left, _top_right, _bottom_left, _bottom_right);
        });
        $('html').on('mouseleave', '[class*="tip"]', function (e) {
            $('p#tooltip').remove();
        });
    });
}(window.$);
!function ($) {
    $(function () {
        $('html').on('keydown', '[data-type="autocomplete"]', function (e) {
            (e.keyCode) ? key = e.keyCode : key = e.which;
            var $this = $(this);
            var x = $this.parent().find('.xamzo-autocomplete');
            switch (key) {
                case 37:
                case 38:
                    idx = $(this).attr('tabindex');
                    $('body').find('[tabindex]').eq(parseInt(idx) + 1).focus().css({'background': 'rgb(228, 250, 228)'});
                    break;
                case 39:
                case 40:
                    x.find('[tabindex]').eq(0).focus().css({'background': 'rgb(228, 250, 228)'});
                    break;
                default:
                    var _url = ($this.attr('auto-url')) ? $this.attr('auto-url') : '#', _name = ($this.attr('data-name')) ? $this.attr('data-name') : $this.attr('name');
                    _autocomplete($this, _url, _name);
                    break
            }
        });
    });
}(window.$);
function _autocomplete(i, uri, name) {
    var parent = i.parent();
    parent.css({'position': 'relative'});
    var x = parent.find('.xamzo-autocomplete');
    if (x.length > 1) {
        x.remove();
    }
    parent.append('<div class="xamzo-autocomplete"></div>');
    var xhr = $.ajax({beforeSend: function () {
            $('#fadingBarsG').remove();
            x.html(animate_small);
        }, type: 'POST', url: base_ + uri, data: name + '=' + i.val(), success: function (rs) {
            x.html(rs);
            keyK(x);
            x.find('li, tr').click(function () {
                x.html('');
            });
        }});
}
function keyK(x) {
    var col = 1;
    var t = x.find('li, tr');
    t.on("keydown", function (e) {
        var current = $('.xamzo-autocomplete').attr('tabindex');
        (e.keyCode) ? key = e.keyCode : key = e.which;
        switch (key) {
            case 37:
                next = current - 1;
                break;
            case 38:
                next = current - col;
                break;
            case 39:
                next = current + 1;
                break;
            case 40:
                next = current + col;
                break
        }
        function cursor(x) {
            var m = x.find('li[tabindex="' + next + '"] a');
            if (m.length > 0) {
                m.focus().css({'background': 'rgb(228, 250, 228)'});
            } else {
                x.find('li[tabindex="' + next + '"], tr[tabindex="' + next + '"]').focus().css({'background': 'rgb(228, 250, 228)'}).on('keydown', function (e) {
                    (e.keyCode) ? key = e.keyCode : key = e.which;
                    if (key === 13) {
                        $(this).click();
                        $(this).click(function () {
                            x.html('');
                        });
                        $(x).parent().find('input').focus();
                    }
                });
            }
        }
        if (key === 37 | key === 38 | key === 39 | key === 40) {
            t.removeAttr('style').find('a').removeAttr('style');
            if (typeof attr !== 'undefined' && attr !== false) {
                if (key === 37 | key === 39) {
                    cursor(x);
                }
            } else {
                cursor(x);
            }
            current = next - 1;
        }
    });
}
function _sort_asc(sort, uri, dest) {
    var data_ = $(dest).find('form').serialize();
    var _data = '';
    if ($('form.print').length > 0) {
        _data = $('form.print').serialize();
    }
    $.ajax({type: 'POST', url: base_ + uri, data: _data + '&' + data_ + '&order=' + sort + '&by=ASC' + '&search=' + $('#xamzo-head-action input[name="search"]').val(), success: function (rs) {
            $(dest).html(rs);
        }});
}
function _sort_desc(sort, uri, dest) {
    var data_ = $(dest).find('form').serialize();
    var _data = '';
    if ($('form.print').length > 0) {
        _data = $('form.print').serialize();
    }
    $.ajax({type: 'POST', url: base_ + uri, data: _data + '&' + data_ + '&order=' + sort + '&by=DESC' + '&search=' + $('#xamzo-head-action input[name="search"]').val(), success: function (rs) {
            $(dest).html(rs);
        }});
}
function form_reset(id) {
    $(id + ' input[type="text"]:not([readonly]), ' + id + ' input[type="password"]:not([readonly]), ' + id + ' input[type="radio"], ' + id + ' input[type="checkbox"], ' + id + ' textarea:not([readonly])').val('');
    $(id + ' span.xamzo-errorlist').remove();
}
function close_search() {
    $('.in-s-content').remove();
    $('input[name="search"]').val('');
}
function preview(uri, data, ret) {
    $(data).validation();
    if (!$(data).validate()) {
    } else {
        $.ajax({type: 'post', data: $(data).serialize(), url: base_ + uri + '/draft/' + ret, success: function (rs) {
                if (ret === '0') {
                    $.ajax({url: base_ + uri + '/index', success: function (result) {
                            $('.xamzo-right').html(result);
                        }});
                } else {
                    window.open(base_ + rs, '_newtab');
                }
            }, dataType: 'html'});
    }
}
function slide_(t, id) {
    var c = $(t).attr("itemid");
    switch (c) {
        case'show':
            $(id).fadeIn("slow");
            $(t).removeAttr("itemid").attr("itemid", "hide");
            break;
        default:
            $(id).fadeOut("slow");
            $(t).removeAttr("itemid").attr("itemid", "show");
            break
    }
}
function change_limit(i) {
    var hs = '', hash = '';
    var val = i.value;
    if (window.location.hash) {
        hs = window.location.hash.substring(1);
        hash = hs.replace('/', '-');
    }
    document.location.href = base_ + 'setting/session_change/limit/' + val + '/' + hash;
}
function insortable() {
    if ($('html').find('table.sort tbody').size() > 0) {
        if (connetionState()) {
            $("table.sort tbody").sortable({
                opacity: .6,
                cursor: "move",
                placeholder: "ui-state-highlight",
                cancel: ".ui-state-disabled",
                update: function () {
                    var _act_url = $(this).parent().attr('ajax-sort-url');
                    var b = $(this).sortable("serialize");
                    $.ajax({
                        dataType: "html",
                        type: "POST",
                        url: _act_url,
                        data: b,
                        success: function () {
                        }
                    });
                }
            });
        } else {
            o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
            c_str_ = '</p></div>';
            return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
        }
    }
}

function popupwindow(url, title, w, h) {
    wLeft = parent.window.screenLeft ? parent.window.screenLeft : parent.window.screenX;
    wTop = parent.window.screenTop ? parent.window.screenTop : parent.window.screenY;
    var left = wLeft + (parent.window.innerWidth / 2) - (w / 2);
    var top = wTop + (parent.window.innerHeight / 2) - (h / 2);
    return parent.window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + 980 + ', height=' + h + ', top=' + top + ', left=' + left);
}
function set_val_dropdown(value, target) {
    $('input[dropdown-target="' + target + '"]').val(value).focus();
    $('.type-search').find('input').val('');
    $('.on-search').find('a').show();
    $('.dropdown-target').hide();
}
function xamzo_sync(i, uri, form) {
    var element = $(i).parent();
    $.ajax({type: 'post', beforeSend: function () {
            element.html('updating process ... please wait for a moment');
        }, data: $(form).serialize(), url: uri, success: function (rs) {
            element.parent().find('sup').html(rs);
            element.html('your extension is up to date');
        }});
}
function xamzo_search_dd(i) {
    if ($(i).val() !== '') {
        $(i).parent().parent().find("a:not('" + $(i).val().toLowerCase() + "')").hide().focus();
        $(i).parent().parent().find("a:contains('" + $(i).val().toLowerCase() + "')").show().focus();
        $(i).focus();
    } else {
        $(i).parent().parent().find("a").show();
    }
}
function xamzo_table_search(i) {
    if ($(i).val() !== '') {
        $(i).parent().parent().find("td span.s-parameter:not('" + $(i).val().toLowerCase() + "')").parent().parent().hide().focus();
        $(i).parent().parent().find("td span.s-parameter:contains('" + $(i).val().toLowerCase() + "')").parent().parent().show().focus();
        $(i).focus();
    } else {
        $(i).parent().parent().find("td span.s-parameter").parent().parent().show();
    }
}
function xamzo_list_search(i) {
    if ($(i).val() !== '') {
        $(i).parent().parent().parent().find(".list-search span.s-parameter:not('" + $(i).val().toLowerCase() + "')").parent().hide().focus();
        $(i).parent().parent().parent().find(".list-search span.s-parameter:contains('" + $(i).val().toLowerCase() + "')").parent().show().focus();
        $(i).focus();
    } else {
        $(i).parent().parent().parent().find(".list-search span.s-parameter").parent().show();
    }
}
function xamzo_search_show(i) {
    var st = $(i).attr('active');
    if (st === 'false') {
        $('.xamzo-setting-search').slideDown();
        $(i).attr('active', 'true');
    } else {
        $('.xamzo-setting-search').slideUp();
        $(i).attr('active', 'false');
    }
}
function xamzo_set_list_active(i) {
    $(i).parent().parent().find('a').removeClass('active');
    $(i).addClass('active');
}
function get_child(cls) {
    if ($('table').find('tr.' + cls).hasClass('active')) {
        $('table').find('tr.' + cls).removeClass('active').hide();
    } else {
        $('table').find('tr.' + cls).addClass('active').show();
    }
}
function get_detail(id) {
    if ($(id).hasClass('active')) {
        $(id).removeClass('active').show();
    } else {
        $(id).addClass('active').hide();
    }
}
function dbrestore(dir) {
    if (connetionState()) {
        $.ajax({type: "POST", beforeSend: function () {
                loading();
            }, url: base_ + "setting/db_restore", data: "dir=" + dir, success: function (rs) {
                close_box();
                notive("database restore is successfull");
                load("setting/database", ".xamzo-right");
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
function db_delete(dir, id) {
    if (connetionState()) {
        $.ajax({type: "POST", url: base_ + "setting/db_delete", data: "dir=" + dir, success: function (rs) {
                close_box();
                $("div" + id).remove();
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
function load(url, id) {
    if (connetionState()) {
        $.ajax({beforeSend: function () {
                loading();
            }, url: base_ + url, success: function (rs) {
                $(id).html(rs);
                close_box();
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
function backupdb() {
    field = document.getElementsByTagName("input");
    if ($("form#backup_db input:checked").length === 0) {
        notive("Mohon Pilih/Cek Salah Satu Field");
    } else {
        if (connetionState()) {
            $.ajax({type: "POST", beforeSend: function () {
                    loading();
                }, url: base_ + "setting/db_backup", data: $("form#backup_db").serialize(), success: function (rs) {
                    close_box();
                    load("setting/database", ".xamzo-right");
                }});
        } else {
            o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
            c_str_ = '</p></div>';
            return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
        }
    }
    return false;
}
var wordCounts = {};
var animate_small = '<div id="floatingBarsG"><div class="blockG" id="rotateG_01"></div><div class="blockG" id="rotateG_02"></div>' + '<div class="blockG" id="rotateG_03"></div><div class="blockG" id="rotateG_04"></div><div class="blockG" id="rotateG_05"></div>' + '<div class="blockG" id="rotateG_06"></div><div class="blockG" id="rotateG_07"></div><div class="blockG" id="rotateG_08"></div></div>';
$(document).ready(function () {
    console.log('You like to look under the hood? Why not help us build the engine? http://xamzo.com/');
});
function show_chart(i, uri) {
    $('.data-chart').find('.arrow-left').hide();
    load(uri, '.xamzo-right');
    $(i).find('.arrow-left').show();
}
function removeRow(t) {
    $(t).parent().parent().parent().remove();
}
function addRow(t) {
    var table = $(t).attr('data-table');
    $.ajax({type: "post", url: $(t).attr('data-url'), success: function (rs) {
            $('#' + table + ' tr:first').after(rs);
            $('.combobox').combobox();
        }});
}
function addRowSlider(t) {
    var table = $(t).attr('data-table');
    $.ajax({type: "post", url: $(t).attr('data-url'), success: function (rs) {
            $('#' + table + ' tr:first').before(rs);
        }});
}
function removeRowSlider(t) {
    $(t).parent().parent().remove;
}
function show_left(i) {
    var ck = $(i).attr('id');
    if (ck === 'l-hide') {
        $('.xamzo-left').show();
        $('.xamzo-right').css({'width': '82.5%'});
        $(i).attr({'id': 'l-show', 'accesskey': 'x', 'original-title': 'hide [Alt+X]'});
    } else {
        $('.xamzo-left').hide();
        $('.xamzo-right').css({'width': '97.5%'});
        $(i).attr({'id': 'l-hide', 'accesskey': 'o', 'original-title': 'show [Alt+O]'});
    }
}
function modal_table_focus() {
    $('.modal-content').find('tr[tabindex]').eq(1).focus().css({'background': 'rgb(228, 250, 228)'});
    $('.modal-content').on("keydown", function (e) {
        col = 1;
        var current = $('.modal-content').find('tr[tabindex]:focus').attr('tabindex');
        (e.keyCode) ? key = e.keyCode : key = e.which;
        switch (key) {
            case 37:
                next = current - 1;
                break;
            case 38:
                next = current - col;
                break;
            case 39:
                next = (1 * current) + 1;
                break;
            case 40:
                next = (1 * current) + col;
                break;
            default:
                break
        }
        if (key === 37 | key === 38 | key === 39 | key === 40) {
            $('.modal-content').find('tr[tabindex]').css({'background': 'transparent'});
            $('.modal-content').find('tr[tabindex="' + next + '"]').focus().css({'background': 'rgb(228, 250, 228)'}).on('keydown', function (e) {
                (e.keyCode) ? key = e.keyCode : key = e.which;
                if (key === 13) {
                    $(this).click();
                }
            });
            current = next;
        }
    });
}
function table_focus() {
    $('body').find('tr[tabindex]').eq(0).focus().css({'background': 'rgb(228, 250, 228)'});
    $('body').on("keydown", function (e) {
        col = 1;
        var current = $('body').find('tr[tabindex]:focus').attr('tabindex');
        (e.keyCode) ? key = e.keyCode : key = e.which;
        switch (key) {
            case 37:
                next = current - 1;
                break;
            case 38:
                next = current - col;
                break;
            case 39:
                next = (1 * current) + 1;
                break;
            case 40:
                next = (1 * current) + col;
                break;
            default:
                break
        }
        if (key === 37 | key === 38 | key === 39 | key === 40) {
            if ($('body').find('tr[tabindex]').find('input[type="checkbox"]').length > 0) {
                var listX = $('body').find('tr[tabindex]').find('input:not(:checked)');
                listX.parent().parent().parent().css({'background': 'transparent'});
                var listC = $('body').find('tr[tabindex]').find('input[type="checkbox"]:checked');
                listC.parent().parent().parent().css({'background': '#fffccc'});
            } else {
                $('body').find('tr[tabindex]').css({'background': 'transparent'});
            }
            $('body').find('tr[tabindex="' + next + '"]').focus().css({'background': 'rgb(228, 250, 228)'}).on('keydown', function (e) {
                (e.keyCode) ? key = e.keyCode : key = e.which;
                if (key === 13) {
                    $(this).click();
                }
            });
            current = next;
        }
    });
}
function showTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);
    $(".clock").text(h + ":" + m + ":" + s);
    $(".clock").val(h + ":" + m + ":" + s);
    t = setTimeout('showTime()', 1000);
}
function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}
showTime();
$(document).ready(function () {
    table_focus();
});
setInterval(function () {
    $('.custom-combobox input').keyup(function (e) {
        e.preventDefault();
        if (e.keyCode === 27) {
            $(this).val('');
        }
    });
}, 100);
function displayDB(id) {
    $('.collapse').hide();
    $(id).slideToggle('slow');
}
function setPrint(uri) {
    var post = $('form#crud-form').serialize();
    window.open(base_ + uri + '&' + post, '_blank');
}
$(function () {
    var ul = $('ul.menu-box');
    ul.find('li a').click(function () {
        ul.find('li a').removeClass('selected');
        $(this).addClass('selected');
    });
    setInterval(function () {
        $('html, .xamzo-left').css({'height': 'auto'});
        var hLeft = $('.xamzo-left').height();
        var hRight = $('.xamzo-right').height();
        if (hLeft > hRight) {
            $('html').css({'height': parseInt($('.xamzo-left').height())});
        } else {
            $('html').css({'height': parseInt($('.xamzo-right').height())});
            $('.xamzo-left').css({'height': parseInt($('.xamzo-right').height() + 10)});
        }
    }, 1000);
});
function slide_menu(m) {
    $('.xamzo-relative-menu').hide();
    $(m).slideToggle();
}
function showNotice() {
    $.ajax({beforeSend: function () {
            $('li.load-notice, li.my-notice').remove();
            $('.in-data-notif').append('<li class="load-notice">loading....</li>');
        }, url: base_ + 'notice/_list', success: function (rs) {
            $('li.load-notice, li.my-notice').remove();
            $('.in-data-notif').append(rs);
        }});
}
function showChat() {
    $.ajax({beforeSend: function () {
            $('li.load-comment, li.my-chat').remove();
            $('.in-data-comment').append('<li class="load-comment">loading....</li>');
        }, url: base_ + 'chatbox/_list', success: function (rs) {
            $('li.load-comment, li.my-chat').remove();
            $('.in-data-comment').append(rs);
        }});
}
function setChatRead(t, id) {
    $.ajax({url: base_ + 'chatbox/update/' + id, success: function () {
            $(t).parent().parent().css({background: '#fff'});
            $(t).attr('original-title', 'Sudah Dibaca');
            $(t).find('i').removeClass('fa-circle').addClass('fa-circle-o');
            var cl_ = $('#num-co');
            var num = cl_.html();
            if (num > 0) {
                var new_num = num - 1;
                cl_.html(new_num);
            }
            $(t).parent().parent().remove();
        }});
}
function setAsRead(t, id) {
    if (connetionState()) {
        $.ajax({url: base_ + 'notice/update/' + id, success: function () {
                $(t).parent().parent().css({background: '#fff'});
                $(t).attr('original-title', 'Sudah Dibaca');
                $(t).find('i').removeClass('fa-circle').addClass('fa-circle-o');
                var cl_ = $('#num-no');
                var num = cl_.html();
                if (num > 0) {
                    var new_num = num - 1;
                    cl_.html(new_num);
                }
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
function setLinkRead(t, id) {
    if (connetionState()) {
        $.ajax({url: base_ + 'notice/update/' + id, success: function () {
                $(t).parent().parent().css({background: '#fff'});
                $(t).parent().parent().find('.n-right a.success').attr('original-title', 'Sudah Dibaca');
                $(t).parent().parent().find('.n-right a.tip-top-right i').removeClass('fa-circle').addClass('fa-circle-o');
                var cl_ = $('#num-no');
                var num = cl_.html();
                if (num > 0) {
                    var new_num = num - 1;
                    cl_.html(new_num);
                }
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}

function setPublishAllChat() {
    if (connetionState()) {
        $.ajax({url: base_ + 'chatbox/update_all/', success: function () {
                $('ul.in-data-comment').find('li:not(:first)').css({background: '#fff'});
                $('ul.in-data-comment').find('li a.tip-top-right').attr('original-title', 'Sudah Dibaca');
                $('ul.in-data-comment').find('li a.tip-top-right i').removeClass('fa-circle').addClass('fa-circle-o');
                $('#num-co').html(0);
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
function setPublishAll() {
    if (connetionState()) {
        $.ajax({url: base_ + 'notice/update_all/', success: function () {
                $('ul.in-data-notif').find('li:not(:first)').css({background: '#fff'});
                $('ul.in-data-notif').find('li a.tip-top-right').attr('original-title', 'Sudah Dibaca');
                $('ul.in-data-notif').find('li a.tip-top-right i').removeClass('fa-circle').addClass('fa-circle-o');
                $('#num-no').html(0);
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
function deleteNotice(t, id) {
    setAsRead(t, id);
    if (connetionState()) {
        $.ajax({type: 'post', data: 'id=' + id, url: base_ + 'notice/delete/', success: function () {
                $(t).parent().parent().remove();
            }});
    } else {
        o_str_ = '<div class="modal-header">' + '<button type="button" class="close" onclick="close_box()" data-dismiss="modal" aria-hidden="true">&times;</button>' + '<h2>Error</h2>' + '</div>' + '<div class="modal-body"><p>';
        c_str_ = '</p></div>';
        return_modalbox(o_str_ + 'Not connect.\n Verify Network.', 'true', '80%' + c_str_);
    }
}
function showAgenda(t, id) {
    $('ul.in-a').hide();
    $('ul' + id).show('slow');
    $('a.a-calendar').removeClass('selected');
    $(t).addClass('selected');
}
function showReminder(t, id) {
    $('div.in-b').hide();
    $('div' + id).show('slow');
    $('a.a-reminder').removeClass('selected');
    $(t).addClass('selected');
}
function showRiN(t, id) {
    $('ul.in-rin').hide();
    $('ul' + id).show('slow');
    $('a.a-rin').removeClass('selected');
    $(t).addClass('selected');
}
function showReX(t, id) {
    $('ul.in-rex').hide();
    $('ul' + id).show('slow');
    $('a.a-rex').removeClass('selected');
    $(t).addClass('selected');
}
function in_chat(t) {
    if ($(t).hasClass('active')) {
        $(t).removeClass('active');
        $('.xamzo-inchat').hide();
        $('.chatbox').each(function (index) {
            var before_ = parseInt($(this).css('right').replace('px', ''));
            var next_ = before_ - 182;
            $(this).css('right', next_ + 'px');
        });
    } else {
        $(t).addClass('active');
        $('.xamzo-inchat').show();
        $('.chatbox').each(function (i) {
            var before_ = parseInt($(this).css('right').replace('px', ''));
            var next_ = before_ + 182;
            $(this).css('right', next_ + 'px');
        });
    }
}


function connetionState() {
    var online = navigator.onLine;
    if (typeof online !== 'undefined') {
        if (online === true) {
            return true;
        } else {
            return false;
        }
    }
} 