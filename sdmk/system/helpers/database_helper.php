<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/database
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
 
/**
 * read database configuration
 * @param type $file
 * @return type
 */
if (!function_exists('x_db_config')) {

    function x_db_config($file = null) {
        if ($file != null) {
            $json = x_file_get_content($file);
        } else {
            $json = x_file_get_content(BASEPATH . 'configs/database' . JSON);
        }
        $con = x_read_json_decode($json);
        $data = array();
        for ($i = 0; $i < count($con); $i++) {
            foreach ($con[$i] as $key => $val) {
                $data[$key]['host'] = $val['host'];
                $data[$key]['name'] = $val['name'];
                $data[$key]['user'] = $val['user'];
                $data[$key]['pass'] = $val['pass'];
                $data[$key]['port'] = ($val['port']) ? $val['port'] : '3306';
                $data[$key]['pconnect'] = $val['pconnect'];
                $data[$key]['driver'] = $val['driver'];
                $data[$key]['prefix'] = $val['prefix'];
            }
        }
        return $data;
    }

}

/**
 * End of file ./system/helpers/database_helper.php
 */