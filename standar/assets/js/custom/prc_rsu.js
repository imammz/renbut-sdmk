    var save_method; //for save method string
    var table;

      $(document).ready(function() {
        table = $('#dynamic-table').DataTable({ 
          
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          
          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "perencanaan/tr_rsu/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
          },
          ],

        });
      });

      // greating //
      function greatSuccess() {

        $.gritter.add({
               title: 'SUKSES!',
               text: 'Data berhasil diproses.',
               sticky: false,
               time: '1000',
               class_name: 'gritter-success'
        })

      }

    function greatError() {
        $.gritter.add({
             title: 'PERINGATAN!',
             text: 'Anda tidak memiliki hak akses atau terjadi kesalahan dalam proses.',
             sticky: false,
             time: '1000',
             class_name: 'gritter-error'
        })

    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_prc_rsu(id)
    {

      if(confirm('Apakah anda yakin akan menghapus data ini?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "perencanaan/tr_rsu/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               greatSuccess();
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                greatError();
            }
        });
         
      }

    }

$.validator.setDefaults({

  submitHandler: function() { 
    
      url = "perencanaan/tr_rsu/ajax_add"; 
      
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST", 
            data: $('#form_prc_rsu').serialize(),
            dataType: "JSON",
            success: function(data)
            { 
              greatSuccess();
              
              $('#btnSave').hide();
              $('#btnReset').hide();
              $('#btnAdd').show();
              form_standar_sdmk(data.kode_rsu, data.tahun, data.kategori_kelas);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

              greatError();

            }
        });

  }

});

function form_standar_sdmk(kode_rsu,tahun,kategori_kelas)
    {
      $('#page-area-content').empty();
      $('#page-area-content').load('perencanaan/tr_rsu/form_standar_sdmk/'+ kode_rsu + '/' + tahun + '/'+ kategori_kelas +'/?_=' + (new Date()).getTime());

    }

function disabledBtn()
  {
    $('#btnSave').disabled = true;
    return true;
  }


// jquery validation //
$().ready(function() {

  // validate signup form on keyup and submit
  $("#form_prc_rsu").validate({
    rules: {
      tahun:{
        required: function(element) {
          if( $("#tahun").val() =='0'){
            return false;
          } else {
            return true;
          }
        }
      },
      jumlah_pustu: "required",
      jumlah_desa: "required"
    },

    messages: {
      tahun: "Silahkan pilih tahun!",
      jumlah_pustu: "Masukan jumlah pustu!",
      jumlah_desa: "Masukan jumlah desa!"
    }
  });


});


// jquery choosen //
jQuery(function($) {
      
      if(!ace.vars['touch']) {
        $('.chosen-select').chosen({allow_single_deselect:true}); 
        //resize the chosen on window resize
    
        $(window)
        .off('resize.chosen')
        .on('resize.chosen', function() {
          $('.chosen-select').each(function() {
             var $this = $(this);
             $this.next().css({'width': $this.parent().width()});
          })
        }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
          if(event_name != 'sidebar_collapsed') return;
          $('.chosen-select').each(function() {
             var $this = $(this);
             $this.next().css({'width': $this.parent().width()});
          })
        });
    
      }
    
    
    });