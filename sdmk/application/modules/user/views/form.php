<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<script type="text/javascript">
    $('.combobox').combobox();
</script>
<div class="xamzo-block-content">
    <div class="xamzo-block-header">
        <h4><?php echo $this->lang['form']; ?></h4>
    </div>
    <div class="content">
        <form id="crud-form" onsubmit="false">
            <input type="hidden" name="emitter" value="1">
            <input type="hidden" name="id" value="<?php echo isset($id) ? $id : time() ?>">
            <input type="hidden" name="date" value="<?php echo isset($default['date']) ? $default['date'] : date('Y-m-d') ?>">
            <input type="hidden" name="type" value="employe">
            <p>
                <label class="control-label">Level Pengguna</label>
                 <?php
                echo form_dropdown($levels, isset($default['level']) ? $default['level'] : '', array('name' => 'level','validation'=>'required','class'=>'combobox', 'placeholder'=>'level-pengguna-system'));
                ?>
            </p>   
            <p>
                <label class="control-label">Password</label>
                <input type="password" name="password" >
                <?php echo isset($errors['password']) ? $errors['password'] : ''; ?>
            </p>      
            <p>
                <label class="control-label">Nama Lengkap *</label>
                <input type="text" name="fullname" validation="required" value="<?php echo isset($default['fullname']) ? $default['fullname'] : '' ?>"> 
                <?php echo isset($errors['fullname']) ? $errors['fullname'] : ''; ?>
            </p>  
            <p>
                <label class="control-label">Tempat / Tanggal Lahir</label>
                <span style="float: left; width: 48%;">
                    <input type="text" name="born" value="<?php echo isset($default['born']) ? $default['born'] : '' ?>"> 
                    <?php echo isset($errors['born']) ? $errors['born'] : ''; ?> 
                </span>
                <span style="float: left; width: 48%; margin-left: 2%;">
                    <input type="text" name="birth_day" placeholder="dd-mm-yyyy" class="date" value="<?php echo isset($default['birth_day']) ? $default['birth_day'] : '' ?>"> 
                    <?php echo isset($errors['birth_day']) ? $errors['birth_day'] : ''; ?>
                </span>
            </p>  
            <p>
                <label class="control-label">Jenis Kelamin</label>
                <input type="radio" id="male" name="sex" value="1" <?php echo isset($default['sex']) ? ($default['sex'] == '1') ? 'checked="checked"' : ''  : '' ?>> <label for="male"> Laki-laki</label>
                <input type="radio" id="female" name="sex" value="0" <?php echo isset($default['sex']) ? ($default['sex'] == '0') ? 'checked="checked"' : ''  : '' ?>> <label for="female"> Perempuan</label>
            </p>    
                <label class="control-label">Email Personal</label> 
                    <input type="text" name="email" validation="required|email" value="<?php echo isset($default['email']) ? $default['email'] : '' ?>"> 
                    <?php echo isset($errors['email']) ? $errors['email'] : ''; ?> 
            </p>  
            <p style="float: left; padding: 10px 0px;">
                <button class="btn primary medium" accesskey="ctrl+s" href="<?php echo isset($action) ? $action : '' ?>" ajax="true" ajax-data="#crud-form" ajax-redirect="user/index" ajax-target=".xamzo-right" ajax-type="POST"><?php echo $this->lang['submit'] ?></button>
                <b>*</b> tidak boleh kosong
                <a class="btn warning right medium" onclick="form_reset('#crud-form');"><?php echo $this->lang['reset'] ?></a>
            </p>
        </form> 
    </div>
</div>
<script type="text/javascript">
    $(".date").mask('00-00-0000');
    $('select[name="employee_type"]').combobox({
        selected: function () {
            var filter = $(this).find(':selected').val();
            sws(filter);
        }
    });
    var employee_type_ = $('select[name="employee_type"] :selected').val();
    sws(employee_type_);
    function sws(i) {
        switch (i) {
            case '1': 
                $('p#in-salary').show(); 
                $('input[name="salary"]').attr('validation','required');
                break; 
            default:
                $('input[name="salary"]').removeAttr('validation');
                $('p#in-salary').removeAttr('style').removeClass('error').hide(); 
                break;
        }
    }
    $('.money').mask('000.000.000.000.000', {reverse: true});
</script>


