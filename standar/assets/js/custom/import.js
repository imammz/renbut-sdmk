var save_method; //for save method string
var table;

// greating //
function greatSuccess() {

  $.gritter.add({
         title: 'SUKSES!',
         text: 'Data berhasil diproses.',
         sticky: false,
         time: '1500',
         class_name: 'gritter-success'
  })

}

function greatError() {
  $.gritter.add({
       title: 'PERINGATAN!',
       text: 'Anda tidak memiliki hak akses atau terjadi kesalahan dalam proses.',
       sticky: false,
       time: '1500',
       class_name: 'gritter-error'
  })

}


$.validator.setDefaults({
  submitHandler: function() { 
      
    var formData = new FormData($('#form_import')[0]);

    formData.append('path_file', $('input[type=file]')[0].files[0]); 

    url = "t_import/ajax_add";

       // ajax adding data to database
          $.ajax({

            url : url,

            type: "POST",

            data: formData,

            dataType: "JSON",

            contentType: false,

            processData: false,
            
            success: function(data)
            { 

              greatSuccess();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

              greatError();

            }
        });

  }

});


// jquery validation //
$().ready(function() {

  // validate signup form on keyup and submit
  $("#form_import").validate({
    rules: {
      
      import_name: "required",
      active: "required"
    },

    messages: {
      import_name: "Masukan Nama Ex Master!"
    }
  });


});
