<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<div class="modal-header">
    <button type="button" class="close btn success large" onclick="close_box()">×</button>
    <h3>Ubah Password</h3>
</div>
<div class="modal-body">
    <form id="crud-form-modal" onsubmit="false">
        <?php isset($message) ? $message : '' ?>
        <input type="hidden" name="emitter" value="1"> 
        <p>
            <label class="control-label"><?php echo $this->lang['old_password'] ?> *</label>
            <input type="password" name="old_password" validation="required"> 
        </p>
        <p>
            <label class="control-label"><?php echo $this->lang['new_password'] ?> *</label>
            <input type="password" name="password" validation="required"> 
        </p>
        <p>
            <label class="control-label"><?php echo $this->lang['confirm_new_password'] ?> *</label>
            <input type="password" name="confirm" validation="required|modal_confirm"> 
        </p>
        <p class="modal-footer"> 
            <button class="btn primary medium" is_close_box="false" href="<?php echo isset($action) ? $action : '' ?>" ajax="true" ajax-data="#crud-form-modal" ajax-target="false" ajax-type="POST"><?php echo $this->lang['submit'] ?></button>
             <b>*</b> tidak boleh kosong
            <a class="btn warning right medium" onclick="form_reset('#crud-form-modal');"><?php echo $this->lang['reset'] ?></a>
        </p>
    </form> 
</div>

