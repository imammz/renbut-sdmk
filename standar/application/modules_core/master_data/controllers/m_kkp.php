<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kkp extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('kkp_model','kkp');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan KKP";
		$data['subtitle'] = "Daftar KKP";
		$this->load->view('kkp/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form KKP";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->kkp->get_by_id($id);
		}
		$this->load->view('kkp/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->kkp->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $kkp) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.strtoupper($kkp->kode_kkp).' ]';
			$row[] = strtoupper($kkp->nama_kkp);
			$row[] = $kkp->nama_kabupaten;
			$row[] = $kkp->nama_provinsi;
			$row[] = $kkp->alamat;
			$row[] = $kkp->no_telp;
			$row[] = $kkp->kelas_kkp;
			$row[] = ($kkp->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $kkp->updated_date?Tanggal::formatDateTime($kkp->updated_date):Tanggal::formatDateTime($kkp->created_date);

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($kkp->id_kkp,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_kkp('."'".Regex::_genRegex($kkp->id_kkp,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->kkp->count_all(),
						"recordsFiltered" => $this->kkp->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	public function sinkronisasi()
	{
		
		$data['title'] = "Sinkronisasi Data Puskesmas";
		$data['subtitle'] = "Download data puskesmas";
		
		$this->load->view('kkp/form_sinkronisasi', $data);
	}

	public function ajax_add()
	{
		//print_r($_POST);die;
		$id_kkp = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'kode_kkp' => Regex::_genRegex($this->input->post('kode_kkp'), 'RGXQSL'),
			'nama_kkp' => Regex::_genRegex($this->input->post('nama_kkp'), 'RGXQSL'),
			'kelas_kkp' => Regex::_genRegex($this->input->post('kelas_kkp'), 'RGXQSL'),
			'id_kabupaten' => Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXQSL'),
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXQSL'),
			'alamat' => Regex::_genRegex($this->input->post('alamat'), 'RGXQSL'),
			'no_telp' => Regex::_genRegex($this->input->post('no_telp'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ')
		);
		
		if( $id_kkp == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->id_user;
			$this->kkp->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->id_user;
			$this->kkp->update(array('id_kkp'=>$id_kkp), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->kkp->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	function get_kkp_by_kode_json($kode_rs) {
		
        $this->db->select('m_kkp.*');
        $this->db->from('m_kkp');
       
        $this->db->where('kode_kkp', $kode_rs);
        $this->db->order_by('nama_kkp', 'ASC');
        
        $result = $this->db->get()->row_array();
        echo json_encode($result);
    }
	
}
