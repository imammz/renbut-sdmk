<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1200px;">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id',$faskes_installasi_id)->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
                echo "<h2>Langkah 1 - Menetapkan Jenis SDMK</h2>";
                echo "<h3>{$faskes_installasi['installasi_nama']} - {$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
                <hr/>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/tambahSDMK_proses/<?php echo $faskes_installasi_id ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="ref_jenis_sdmk_kode">
                            Jenis SDMK
                        </label>
                        <div class="col-sm-10">
                           <select class="js-example-basic-single js-states form-control" onChange="loadJft(this.value)" id="ref_jenis_sdmk_kode" name="ref_jenis_sdmk_kode"  required>
                                <option value="">-- Pilih Jenis SDMK --</option>
                                <?php foreach ($this->orm->ref_jenis_sdmk->order('urut ASC','ref_jenis_sdmk_kode ASC') as $row) { ?>
                                    <option value="<?php echo $row['ref_jenis_sdmk_kode'].'#'.$row['ref_jenis_sdmk_nama_level2'] ?>"> <?php echo $row['ref_jenis_sdmk_nama_level2'] ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2" ></div>
                        <div class="col-sm-10" id="div_jft">
                           
                        </div>
                    </div>
                    
                    <script>
    
    function loadJft(ref_jenis_sdmk_kode) {


                var url = '<?php echo site_url('entry/combobox/loadJft') ?>';

                $.post(url,
                        {
                            ref_jenis_sdmk_kode: ref_jenis_sdmk_kode

                        },
                function (data, status) {

                    if (data.result) {
                        var content = '<select class="form-control" name="jft" id="jft">';
                        $.each(data.data_jft, function (key, value) {
                          content += ' <option  value="' + value.ref_jenis_sdmk_jft_id+'#'+value.jft + '">' + value.jft + '</option>';
                        });

                        content += '</select>';

                        $('#div_jft').html(content);
                    }
                 

                }, "json");
            }
    
   </script>
                    
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_PNS">
                        <?php 
                        $tenaga_tetap = 'Jumlah PNS';
                        if($_SESSION['user']['ref_jenis_faskes_kode'] == 2) {
                            $operator_rs = $this->orm->ref_rumahsakit->where('kode_rs',$_SESSION['user']['faskes_kode'])->fetch();
                            $operator_rs = strtoupper($operator_rs['operator']);
                            
                            switch ($operator_rs) {
                                case 'SWASTA':
                                case 'SWASTA NON PROFIT':
                                    $tenaga_tetap = 'Jumlah Tenaga Tetap';
                                    break;
                            }

                            
                         } 
                         echo $tenaga_tetap;
                         ?>
                        
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="<?php echo $tenaga_tetap ?>" id="jumlah_PNS" name="jumlah_PNS" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_PPPK">
                            Jumlah PPPK
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah PPPK" id="jumlah_PPPK" name="jumlah_PPPK" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_PTT">
                            Jumlah PTT
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah PTT" id="jumlah_PTT" name="jumlah_PTT" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_HD">
                            Jumlah Tenaga Honor
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah HD" id="jumlah_HD" name="jumlah_HD" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="jumlah_DLL">
                            Jumlah Yang Lainnya
                        </label>
                        <div class="col-sm-3">
                            <input type="number" placeholder="Jumlah DLL" id="jumlah_DLL" name="jumlah_DLL" class="form-control">
                        </div>
                    </div>                   
                        <div class="form-group">
                        <div class="col-sm-6"> 
                        </div>
                        <div class="col-sm-6" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Selanjutnya >> </button>
                        &nbsp;
                        </div>
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });

                            function genKode() {

                                var selected = document.getElementById('faskes_kode');
                                var faskes_kode = selected.options[selected.selectedIndex].value;

                                var url = '<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/getData/';

                                $.post(url,
                                        {
                                            faskes_kode: faskes_kode
                                        },
                                function (data) {

                                    if (data.result) {

                                            $('#faskes_kode').val(data.faskes_kode);
                                            $('#NO_KAB option[value=' + data.NO_KAB + ']').attr('selected', 'selected');
                                            $('#faskes_nama').val(data.faskes_nama);
                                            $('#faskes_telp').val(data.faskes_telp);
                                            $('#faskes_alamat').val(data.faskes_alamat);
                                            $('#faskes_email').val(data.faskes_email);
                                            $('#ref_faskes_kelas_id option[value=' + data.ref_faskes_kelas_id + ']').attr('selected', 'selected');
                                            $('#ref_jenis_faskes_kode option[value=' + data.ref_jenis_faskes_kode + ']').attr('selected', 'selected');
                                            $('#status_pengelolaan_id option[value=' + data.status_pengelolaan_id + ']').attr('selected', 'selected');
                                        
                                    }
                                    else {
                                        alert('Data  Tidak Tersedia');
                                    }

                                }, "json");


                            }

    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
