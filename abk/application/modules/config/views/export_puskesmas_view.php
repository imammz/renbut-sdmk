<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >



                <div class="wrap-content container" id="container">

                    <!-- start: YOUR CONTENT HERE -->

                    <div class="row">
                        <div class="col-md-12"> 

                            <div class="container-fluid container-fullw bg-white">
                                <div class="row">
                                    <div class="col-md-8">
                                        &nbsp;
                                    </div>
                                    <ol class="breadcrumb">
                                        <li>
                                            <span>Export Data</span>
                                        </li>

                                    </ol>
                                </div>

                                <!-- start: YOUR CONTENT HERE -->                        
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="min-height: 500px;">
                                                <div class="tabbable">
                                                    <ul id="myTab1" class="nav nav-tabs">

                                                        <!--                                                <li class="active">
                                                                                                            <a href="#tab_faskes" data-toggle="tab">Pilih Faskes</a>
                                                                                                        </li>
                                                                                                         <li>
                                                                                                            <a href="#tab_kab" data-toggle="tab">Dinkes Kabupaten/Kota</a>
                                                                                                        </li>
                                                                                                         <li>
                                                                                                            <a href="#tab_prov" data-toggle="tab">Dinkes Provinsi</a>
                                                                                                        </li>-->

                                                        <li class="active">
                                                            <a href="#tab_faskes" data-toggle="tab">Export Import Data Kebutuhan SDMK Setiap Faskes</a>
                                                        </li>  

                                                    </ul>
                                                    <div class="tab-content">

                                                        <div class="tab-pane fade in active" id="tab_faskes">
                                                            <form method="POST" action="<?php echo site_url('config/export/proses/'.$_SESSION['user']['faskes_kode']) ?>">     
                                                            <div class="row">
                                                              
                                                                <div class="col-lg-4">
                                                                   <h4 class="text-azure"> Periode (Tahun)  </h4>   
                                                                </div>
                                                                <div class="col-md-4">   
                                                                    <select class="js-example-basic-single js-states form-control" name="faskes_tahun" id="faskes_tahun" required>
                                                                        <?php
                                                                        foreach ($this->orm->tahun_entry() as $row) {
                                                                            $selected = '';
                                                                            if (date('Y') == $row['tahun']) {
                                                                                $selected = 'selected';
                                                                            }
                                                                            ?>    
                                                                            <option <?php echo $selected ?> value="<?php echo $row['tahun'] ?>"> <?php echo $row['tahun'] ?> </option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div> 
                                                            </div>
                                                            <div class="row"><div> <br/> </div></div>
                                                            <div class="row">
                                                              
                                                                <div class="col-md-12">
                                                                    <h4 class="text-azure">  Pilih Jenis SDMK Yang Akan Di Export </h4>
                                                                </div>
                                                              </div>  
                                                            <table width="100%" id="table_sdmk" class="display">
                                                                <thead>
                                                                <tr>
                                                                    <th width="5%">
                                                                        No
                                                                    </th>
                                                                    <th>
                                                                        SMDK
                                                                    </th>
                                                                    <th width="30%">
                                                                        PILIH
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                               <?php 
                                                               $no = 1;
                                                               foreach($sdmk_faskes_installasi as $row) { 
                                                                 
                                                                   $sdmk = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode',$row['ref_jenis_sdmk_kode'])->fetch();
                                                                   
                                                                   ?> 
                                                                <tbody>
                                                                <tr>
                                                                    <td><?php echo $no ?></td>
                                                                    <td>
                                                                    <?php echo $sdmk['ref_jenis_sdmk_nama_level2'] ?>
                                                                    </td>
                                                                    <td style="text-align: center">
                                                                        <input checked type="checkbox" class="form-control" name="ref_jenis_sdmk_kode[]" value="<?php echo $sdmk['ref_jenis_sdmk_kode'] ?>">
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                               <?php $no++; } ?>
                                                            </table>

                                                            <div class="row">
                                                                <div class="col-md-12">&nbsp;</div>
                                                            </div>


                                                            <div class="row" style="padding-top: 20px;">
                                                              
                                                                <div class="col-lg-12 text-center">   

                                                                    <button type="submit" target="_blank"  class="btn btn-lg  btn-primary" ><i class="fa fa-refresh"></i> Proses</button>
                                                                </div>
                                                            </div>
                                                            </form>
                                                        </div>

                                                        <!--                                            <div class="tab-pane" id="tab_kab">
                                                                                              
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">
                                                                                                                Kabupaten
                                                                                                            </div>
                                                                                                            <div class="col-md-2">
                                                                                                                Periode (Tahun) 
                                                                                                            </div>
                                                                                                            <div class="col-md-2">   
                                                                                                                <select class="js-example-basic-single js-states form-control" name="faskes_tahun" id="faskes_tahun" required>
                                                        <?php
                                                        foreach ($this->orm->tahun_entry() as $row) {
                                                            $selected = '';
                                                            if (date('Y') == $row['tahun']) {
                                                                $selected = 'selected';
                                                            }
                                                            ?>    
                                                                                                                                <option <?php echo $selected ?> value="<?php echo $row['tahun'] ?>"> <?php echo $row['tahun'] ?> </option>
                                                            <?php
                                                        }
                                                        ?>
                                                                                                                </select>
                                                                                                            </div>
                                                                                                        </div>
                                                        
                                                                                                        <div class="row">
                                                                                                            <div class="col-md-12">&nbsp;</div>
                                                                                                        </div>
                                                        
                                                                                                        
                                                                                                        <div class="row" style="padding-top: 20px;">
                                                                                                            <div class="col-md-2">
                                                        
                                                                                                            </div>
                                                                                                            <div class="col-md-12" style="text-align: right;">   
                                                                                                             
                                                                                                                <a type="button" target="_blank"  class="btn  btn-primary" href="<?php echo base_url() ?>index.php/config/export/proses/<?php echo $_SESSION['user']['NO_KAB'] ?>/<?php echo date('Y') ?>"><i class="fa fa-refresh"></i> Proses</a>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    
                                                                                                    </div>-->

                                                        





                                                    </div>   
                                                </div>                                                   
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                
                                
                                
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        
        
        
        jQuery(document).ready(function () {
            Main.init();
             var Table = $('#table_sdmk').dataTable(
                {
                    "order": [[0, "asc"]],
                    "aoColumns": [//Row control
                        {"sClass": "left"},
                        {"sClass": "left"},
                        {"sClass": "center"},
                    ],
                    "processing": true,
                    "language": {
                        "lengthMenu": "Tampilkan _MENU_ Data Perhalaman",
                        "zeroRecords": "Maaf Data Tidak Ditemukan",
                        "info": "Tampilkan _PAGE_ dari _PAGES_",
                        "search": "Filter Data : ",
                        "infoEmpty": "Data Tidak Ditemukan",
                        "infoFiltered": "(difilter dari _MAX_ total data)"
                    }
                }
        );
        });
        
        
        function export_proses() {
            //  alert($("#faskes_kode").val());
            var faskes_kode = <?php echo $_SESSION['user']['faskes_kode']; ?>;
            var faskes_tahun = $("#faskes_tahun").val();

            if (faskes_tahun === '') {
                alert('Pilih Tahun Terlebih Dahulu');
            } else {
                window.location.replace("<?php echo base_url() ?>index.php/config/export/proses/" + faskes_kode + '/' + faskes_tahun);
            }
        }
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
