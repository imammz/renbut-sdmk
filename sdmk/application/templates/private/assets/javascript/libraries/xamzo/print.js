function range_search(uri_, status) {
    var dest = $('#result-search');
    var _s = '';
    if (status === true) {
        _s = '&include_date=on';
    }
    var _data = $('form.print').serialize();
    $.ajax({
        beforeSend: function () {
            loading();
        }, 
        url: base_ + uri_, 
        type: 'post', data: _data + _s, 
        success: function (rs) {
            dest.html(rs);
            close_box();
        }
    });
}

function print_excel(uri, fname, header, title) {
    var d = new Date();
    var content = '<div class="modal-header">' + '<button type="button" class="close btn success large" onclick="close_box();">&times;</button>' + '<h3><i class="fa fa-gear"></i> Pengaturan</h3>' + '</div>' + '<div class="modal-body">' + '<p><label>Header<label>' + '<input type="text" style="padding: 5px; margin:10px 0; width:96%" id="file_header" placehoder="file header" value="' + header + '" name="file_header">' + '</div><p>' + '<p><label>Title<label>' + '<input type="text" style="padding: 5px; margin:10px 0; width:96%" id="file_title" placehoder="file title" value="' + title + '" name="file_title">' + '</div><p>' + '<p><label>File name<label>' + '<input type="text" style="padding: 5px; margin:10px 0; width:96%" id="file_name" placehoder="file name" value="excel_' + d.getTime() + '" name="file_name">' + '</div><p>' + 
            '<div class="modal-footer">' +
                '<a class="btn primary" style="width:92.6%" href="javascript:void(0)" onclick="excel_print(\'' + uri + '\',\'' + fname + '\');"><i class="fa fa-print"></i> Print</a>&nbsp;' + 
            '</div>';
    return_modalbox(content, 'true', '300px');
}
function excel_print(uri, fname) {
    var _data = $('form.print').serialize();
    var d = new Date();
    var date = d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate();
    var time = d.getHours() + '-' + d.getMinutes() + '-' + d.getSeconds();
    var xname = $('input#file_name').val();
    var title = $('input#file_title').val();
    var header = $('input#file_header').val();
    fname = fname.replace('name', xname);
    fname = fname.replace('date', date);
    fname = fname.replace('time', time) + '.xls'; 
    $.ajax({
        beforeSend: function () {
            loading();
        }, 
        url: base_ + uri + '/' + fname, 
        type: 'post', 
        data: _data + '&header=' + header + '&title=' + title + '&type=print', 
        success: function (rs) {
            location.replace(rs);
            close_box();
        }
    });
};
