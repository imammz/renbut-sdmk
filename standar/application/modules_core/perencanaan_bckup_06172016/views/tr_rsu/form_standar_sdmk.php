<script>
function myFunction(keys) {

    var pns = document.getElementById('pns'+keys+''); 
    pns = pns.value?pns.value:0;

    var pppk = document.getElementById('pppk'+keys+''); 
    pppk = pppk.value?pppk.value:0;

    var ptt = document.getElementById('ptt'+keys+''); 
    ptt = ptt.value?ptt.value:0;

    var honorer = document.getElementById('honorer'+keys+''); 
    honorer = honorer.value?honorer.value:0;

    var blud = document.getElementById('blud'+keys+''); 
    blud = blud.value?blud.value:0;

    var tks = document.getElementById('tks'+keys+''); 
    tks = tks.value?tks.value:0;

    total = parseInt(pns) + parseInt(pppk) + parseInt(ptt) + parseInt(honorer) + parseInt(blud) + parseInt(tks);
    document.getElementById('total'+keys+'').value = total;

    // SUB PARENT //
    var pns_sub = document.getElementById('pns_sub'+keys+''); 
    pns_sub = pns_sub.value?pns_sub.value:0;

    var pppk_sub = document.getElementById('pppk_sub'+keys+''); 
    pppk_sub = pppk_sub.value?pppk_sub.value:0;

    var ptt_sub = document.getElementById('ptt_sub'+keys+''); 
    ptt_sub = ptt_sub.value?ptt_sub.value:0;

    var honorer_sub = document.getElementById('honorer_sub'+keys+''); 
    honorer_sub = honorer_sub.value?honorer_sub.value:0;

    var blud_sub = document.getElementById('blud_sub'+keys+''); 
    blud_sub = blud_sub.value?blud_sub.value:0;

    var tks_sub = document.getElementById('tks_sub'+keys+''); 
    tks_sub = tks_sub.value?tks_sub.value:0;

    total_sub = parseInt(pns_sub) + parseInt(pppk_sub) + parseInt(ptt_sub) + parseInt(honorer_sub) + parseInt(blud_sub) + parseInt(tks_sub);
    document.getElementById('total_sub'+keys+'').value = total_sub;
    
}
</script>

<script type="text/javascript">

    $(function() {

        

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Unit Kerja)</option>').appendTo($('#rsu-box'));
            }
        });
    });
</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
  <form class="form-horizontal" method="post" id="form_prc_rsu" action="<?php echo site_url('perencanaan/tr_rsu/ajax_add')?>">
    <div class="row">
      <div class="col-xs-12">
                <br>

                <div class="form-group">

                <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value->tahun)?$value->tahun:'','tahun','tahun','form-control','required','inline');?>
                  </div>

                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value->id_perencanaan)?$value->id_perencanaan : '( Auto )'?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>

                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','6')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','4','6')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($value->id_provinsi) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','4','6')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','4','6')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_kabupaten(isset($value->id_kabupaten) ? $value->id_kabupaten : $this->session->userdata('data_user')->id_kabupaten ,'id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>

                  <label class="control-label col-md-2">Rumah Sakit</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_rsu(isset($value) ? $value->kode_rsu : $this->session->userdata('data_user')->kode_rsu,'kode_rsu','rsu-box','form-control','required '.$readonly.'','inline');?>
                  </div>

                </div>
                <?php }?>
                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('A','B','C','D');
                          foreach($katergori_kelas as $row_katergori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_katergori_kelas?>" <?php echo isset($value) ? ($value->kategori_kelas == $row_katergori_kelas) ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo $row_katergori_kelas?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3">Jumlah Tempat Tidur</label>
                  <div class="col-md-2">
                    <input name="jumlah_tt" id="jumlah_tt" value="<?php echo isset($value)?$value->jumlah_tt:''?>" class="form-control" type="text">
                  </div>
                </div>

                <?php 
                  $arr_vf = '';
                  $value_form = $this->prc_rsu->get_value_form($value->id_perencanaan); //print_r($this->db->last_query());
                  foreach ($form_entry_rsu->result() as $key1 => $value_rsu) {
                    $arr_vf[]= ''.$value_rsu->id_jenis_sdmk.',';
                    if( in_array($value->kategori_kelas, array('C','D')) ){
                      if( !in_array($value_rsu->id_jenis_sdmk, array('212204','212205','212206') )){
                ?>

                <div class="form-group">
                  <label class="control-label col-md-3"><?php echo $value_rsu->nama_jenis_sdmk?></label>
                  <div class="col-md-2" style="padding-top:10px">
                    <input  name="value[]" id="jml_<?php echo $value_rsu->id_jenis_sdmk?>" value="<?php echo ($value_rsu->id_jenis_sdmk == $value_form[$key1]->id_jenis_sdmk) ? $value_form[$key1]->value : 0 ;?>" class="form-control" type="text">
                    <input  name="id_jenis_sdmk_form[]" value="<?php echo $value_rsu->id_jenis_sdmk?>" class="form-control" type="hidden">
                  </div>
                </div>
                <?php 
                      }
                    }else{ ?>
                     <div class="form-group">
                      <label class="control-label col-md-3"><?php echo $value_rsu->nama_jenis_sdmk?></label>
                      <div class="col-md-2" style="padding-top:10px">
                        <input  name="value[]" id="jml_<?php echo $value_rsu->id_jenis_sdmk?>" value="<?php echo ($value_rsu->id_jenis_sdmk == isset($value_form[$key1]->id_jenis_sdmk)?$value_form[$key1]->id_jenis_sdmk:0) ? $value_form[$key1]->value : 0 ;?>" class="form-control" type="text">
                        <input  name="id_jenis_sdmk_form[]" value="<?php echo $value_rsu->id_jenis_sdmk?>" class="form-control" type="hidden">
                      </div>
                    </div>
                <?php
                    }
                  }
                  $array_vf = $arr_vf;
                ?>

      </div>
    </div>

    <?php echo $this->load->view('form_standar_sdmk');?>

    <div class="form-actions center">
      <!--hidden field-->
      <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_perencanaan:0?>"> -->

      <a onclick="getMenu('perencanaan/tr_rsu')" href="#" class="btn btn-sm btn-success">
        <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
        Kembali ke daftar
      </a>
      <a onclick="getMenu('perencanaan/tr_rsu/riwayat_perencanaan')" href="#" class="btn btn-sm btn-warning">
        <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
        Lihat Riwayat Perencanaan
      </a>
      <button type="reset" onclick="getMenu('perencanaan/tr_rsu')" id="btnReset" class="btn btn-sm btn-danger">
        <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
        Reset Form
      </button>
      <button type="submit" name="submit" id="btnSave2" value="save_methode" class="btn btn-sm btn-info">
        <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
        Submit
      </button>
  </div>
  
    </form>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url().'assets/js/custom/prc_rsu.js'?>"></script>
