<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white">

        <div class="row">            
            <div class="col-md-12"> 
                <?php

                $prov = $this->orm->ref_provinsi->where('NO_PROV', $NO_PROV)->fetch();
                echo "<h3>{$prov['NAMA_PROV']}</h3>";
                ?>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/dinaskesehatanprov/tambahfaskes_proses/<?php echo $NO_PROV ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_kode">
                            Kode Dinas Kesehatan Provinsi
                        </label>
                        <div class="col-sm-10">
                            <select class="js-example-basic-single js-states form-control" id="faskes_kode" name="faskes_kode" onchange="genKode()" required>
                                <option value="">-- Pilih Kode Dinas Kesehatan Provinsi --</option>
                                <?php foreach ($this->orm->ref_provinsi->where('NO_PROV', $NO_PROV) as $row) { ?>
                                    <option value="<?php echo $row['NO_PROV'] ?>"><?php echo $row['NO_PROV'] ?> - <?php echo $row['NAMA_PROV'] ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_nama">
                            Nama Dinas Kesehatan Provinsi
                        </label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Nama Dinas Kesehatan Provinsi" id="faskes_nama" name="faskes_nama" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_alamat">
                            Alamat Lengkap Dinas Kesehatan Provinsi
                        </label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Alamat Dinas Kesehatan Provinsi" id="faskes_alamat" name="faskes_alamat" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_telp">
                            Telepon
                        </label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Telepon Dinas Kesehatan Provinsi" id="faskes_telp" name="faskes_telp" class="form-control">
                        </div>
                        <label class="col-sm-2 control-label" for="faskes_email">
                            Email
                        </label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Email Dinas Kesehatan Provinsi" id="faskes_email" name="faskes_email" class="form-control">
                        </div>
                    </div>

                    <div class="form-group" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Simpan</button>
                        &nbsp;
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });

                            function genKode() {

                                var selected = document.getElementById('faskes_kode');
                                var faskes_kode = selected.options[selected.selectedIndex].value;

                                var url = '<?php echo base_url() ?>index.php/entry/dinaskesehatanprov/getData/';

                                $.post(url,
                                        {
                                            faskes_kode: faskes_kode
                                        },
                                function (data) {

                                    if (data.result) {

                                            $('#faskes_kode').val(data.faskes_kode);
                                            $('#NO_PROV option[value=' + data.NO_KAB + ']').attr('selected', 'selected');
                                            $('#faskes_nama').val(data.faskes_nama);
                                            $('#faskes_telp').val(data.faskes_telp);
                                            $('#faskes_alamat').val(data.faskes_alamat);
                                            $('#faskes_email').val(data.faskes_email);
                                           // $('#ref_faskes_kelas_id option[value=' + data.ref_faskes_kelas_id + ']').attr('selected', 'selected');
                                            $('#ref_jenis_faskes_kode option[value=' + data.ref_jenis_faskes_kode + ']').attr('selected', 'selected');
                                           // $('#status_pengelolaan_id option[value=' + data.status_pengelolaan_id + ']').attr('selected', 'selected');
                                        
                                    }
                                    else {
                                        alert('Data Dinas Kesehatan Provinsi Tidak Tersedia');
                                    }

                                }, "json");


                            }

    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
