<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $NO_KAB)->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                echo "<h1>{$balaikesehatan['faskes_nama']}</h1>";
                ?>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/balaikesehatan/tambahfaskes_proses/<?php echo $NO_KAB ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_kode">
                            Kode Balai Kesehatan
                        </label>
                        <div class="col-sm-10">
                            <strong><?php echo $balaikesehatan['faskes_kode'] ?></strong>
                            <input type="hidden" id="faskes_kode" name="faskes_kode" value="<?php echo $balaikesehatan['faskes_kode'] ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_nama">
                            Nama Balai Kesehatan
                        </label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Nama Balai Kesehatan" id="faskes_nama" name="faskes_nama" value="<?php echo $balaikesehatan['faskes_nama'] ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            
                                
                        </div>
                        <div class="col-md-10 text-blue text-bold">
                            WILAYAH
                                
                        </div>
                    </div>  
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="NO_KAB">
                            Kab Kota
                        </label>
                        <div class="col-sm-10">
                            <select class="js-example-basic-single js-states form-control" id="NO_KAB" name="NO_KAB"  required>
                                <?php foreach ($this->orm->ref_kabupaten->where('NO_KAB', $NO_KAB) as $row) { ?>
                                    <option value="<?php echo $row['NO_KAB'] ?>"><?php echo $row['NO_KAB'] ?> - <?php echo $row['NAMA_KAB'] ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="NO_KAB">
                            Provinsi
                        </label>
                        <div class="col-sm-10">
                            <select class="js-example-basic-single js-states form-control" id="NO_KAB" name="NO_KAB"  required>
                                <?php foreach ($this->orm->ref_provinsi->where('NO_PROV', $prov['NO_PROV']) as $row) { ?>
                                    <option value="<?php echo $row['NO_PROV'] ?>"><?php echo $row['NO_PROV'] ?> - <?php echo $row['NAMA_PROV'] ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_alamat">
                            Alamat Lengkap Balai Kesehatan
                        </label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Alamat Balai Kesehatan" id="faskes_alamat" name="faskes_alamat" value="<?php echo $balaikesehatan['faskes_alamat'] ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_telp">
                            Telepon
                        </label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Telepon Balai Kesehatan" id="faskes_telp" name="faskes_telp" value="<?php echo $balaikesehatan['faskes_telp'] ?>"  class="form-control">
                        </div>
                        <label class="col-sm-2 control-label" for="faskes_email">
                            Email
                        </label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Email Balai Kesehatan" id="faskes_email" name="faskes_email" value="<?php echo $balaikesehatan['faskes_email'] ?>" class="form-control">
                        </div>
                    </div>

                    <div class="form-group" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Simpan</button>
                        &nbsp;
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });

                            function genKode() {

                                var selected = document.getElementById('faskes_kode');
                                var faskes_kode = selected.options[selected.selectedIndex].value;

                                var url = '<?php echo base_url() ?>index.php/entry/balaikesehatan/getData/';

                                $.post(url,
                                        {
                                            faskes_kode: faskes_kode
                                        },
                                        function (data) {

                                            if (data.result) {

                                                $('#faskes_kode').val(data.faskes_kode);
                                                $('#NO_KAB option[value=' + data.NO_KAB + ']').attr('selected', 'selected');
                                                $('#faskes_nama').val(data.faskes_nama);
                                                $('#faskes_telp').val(data.faskes_telp);
                                                $('#faskes_alamat').val(data.faskes_alamat);
                                                $('#faskes_email').val(data.faskes_email);
                                                // $('#ref_faskes_kelas_id option[value=' + data.ref_faskes_kelas_id + ']').attr('selected', 'selected');
                                                $('#ref_jenis_faskes_kode option[value=' + data.ref_jenis_faskes_kode + ']').attr('selected', 'selected');
                                                // $('#status_pengelolaan_id option[value=' + data.status_pengelolaan_id + ']').attr('selected', 'selected');

                                            } else {
                                                alert('Data Balai Kesehatan Tidak Tersedia');
                                            }

                                        }, "json");


                            }

    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
