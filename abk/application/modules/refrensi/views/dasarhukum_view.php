<?php echo Modules::run('templates/cliptwo/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >

                <div class="container-fluid container-fullw bg-white">
                    <div class="row">
                        <div class="col-md-8">
                            &nbsp;
                        </div>
                        <ol class="breadcrumb">                           
                            <li class="active">
                                <span>Refrensi Kegiatan Beban Kerja</span>
                            </li>
                        </ol>
                    </div>

                    <!-- start: YOUR CONTENT HERE -->                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">                                            
                                <div class="panel-body" style="min-height: 700px;">
                                    
                                    
                                    
                                    
                                    
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-02-UU-N0-36-2009-(-KESEHATAN).pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> UU No 36-2009 ( KESEHATAN)</strong>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-03-UU-no-17-th-2007-(RPJPN).pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong>  UU No 17 Th 2007 (RPJPN)</strong>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-05-UU-No-25-th-2009-PELAYANAN-PUBLIK.pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> UU No 25 Th 2009 PELAYANAN PUBLIK</strong>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-UU-NO-36-2014-TENAGA-KESEHATAN.pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> UU NO 36 2014 TENAGA KESEHATAN</strong>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-keppres-68-1995-(Hari-Kerja)-(2).pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> KEPRES RI NO 68 TAHUN 1995 <br/>TTG HARI KERJA DILINGKUNGAN LEMBAGA PEMERINTAH</strong>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-PP-No-72-2012-(SKN).pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> PP TAHUN 2012 NO 72 TENTANG SISTEM KESEHATAN NASIONAL</strong>
                                        </a>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/PMK-No.-33-tahun-2015-ttg-Penyusunan-Perencanaan-Kebutuhan-SDM-Kesehatan.pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> PERMENKES RI NOMOR 33 TAHUN 2015</strong>
                                        </a>
                                    </div>
                                    
                                    
                                    
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-04-Perka-BKN-no-19-Th-2011(Pedoman-kebut-PNS).pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong> PERATURAN KEPALA BADAN <b>BKN</b> NO 19 TH 2011 TTG </br> PEDOMAN UMUM PENYUSUNAN KEBUTUHAN PNS</strong>
                                        </a>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/dasarhukum/-JABFUNG-ANALIS-KEPEGAWAIAN-BKN-162-2000.pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong style="font-size: 10px;">  PERATURAN KEPALA BADAN BKN NO 162 TH 2000 TTG  PETUNJUK TEKNIS </br> JABATAN FUNGSIONAL ANALISIS KEPEGAWAIAN DAN ANGKA KREDITNYA</strong>
                                        </a>
                                    </div>
                                    

                                </div>
                            </div>

                        </div>



                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
            FormElements.init();
        });

        function faskes_cetak() {
            //  alert($("#faskes_kode").val());
            var faskes_kode = $("#faskes_kode").val();
            var faskes_tahun = $("#faskes_tahun").val();

            if (faskes_kode === '') {
                alert('Pilih Terlebih Dahulu Faskes Puskesmas');
            }
            else {
                window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/faskes_excel/" + faskes_kode + '/' + faskes_tahun;
            }
        }
        function faskes_proses() {
            //  alert($("#faskes_kode").val());
            var faskes_kode = $("#faskes_kode").val();
            var faskes_tahun = $("#faskes_tahun").val();
            if (faskes_kode === '') {
                alert('Pilih Terlebih Dahulu Faskes Puskesmas');
            }
            else {
                GB_show("Laporan Kebutuhan SDMK Faskes ", '<?php echo base_url() ?>index.php/laporan/puskesmas/faskes_proses/' + faskes_kode + '/' + faskes_tahun, 700, 1100);
                $('html, body').animate({scrollTop: 0}, 'slow');
            }


        }

        function kab_cetak() {
            //  alert($("#faskes_kode").val());
            var NO_KAB = $("#NO_KAB").val();
            var kab_tahun = $("#kab_tahun").val();
            if (NO_KAB === '') {
                alert('Pilih Terlebih Dahulu Kabupaten/Kota');
            }
            else {
                window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/kab_excel/" + NO_KAB + '/' + kab_tahun;
            }
        }
        function kab_proses() {
            //  alert($("#faskes_kode").val());
            var NO_KAB = $("#NO_KAB").val();
            var kab_tahun = $("#kab_tahun").val();
            if (NO_KAB === '') {
                alert('Pilih Terlebih Dahulu Kabupaten/Kota');
            }
            else {
                GB_show("Laporan Kebutuhan SDMK Kabupaten ", '<?php echo base_url() ?>index.php/laporan/puskesmas/kab_proses/' + NO_KAB + '/' + kab_tahun, 700, 1100);
                $('html, body').animate({scrollTop: 0}, 'slow');
            }


        }

        function prov_cetak() {
            //  alert($("#faskes_kode").val());
            var NO_PROV = $("#NO_PROV").val();
            var prov_tahun = $("#prov_tahun").val();
            if (NO_PROV === '') {
                alert('Pilih Terlebih Dahulu Provinsi');
            }
            else {
                window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/prov_excel/" + NO_PROV + '/' + prov_tahun;
            }
        }
        function prov_proses() {
            //  alert($("#faskes_kode").val());
            var NO_PROV = $("#NO_PROV").val();
            var prov_tahun = $("#prov_tahun").val();
            if (NO_PROV === '') {
                alert('Pilih Terlebih Dahulu Provinsi');
            }
            else {
                GB_show("Laporan Kebutuhan SDMK Provinsi ", '<?php echo base_url() ?>index.php/laporan/puskesmas/prov_proses/' + NO_PROV + '/' + prov_tahun, 700, 1100);
                $('html, body').animate({scrollTop: 0}, 'slow');
            }


        }

        function nas_cetak() {
            //  alert($("#faskes_kode").val());
            var nas_tahun = $("#nas_tahun").val();
            window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/nas_excel/" + nas_tahun;
        }
        function nas_proses() {
            //  alert($("#faskes_kode").val());
            var nas_tahun = $("#nas_tahun").val();

            GB_show("Laporan Kebutuhan SDMK Nasional ", '<?php echo base_url() ?>index.php/laporan/puskesmas/nas_proses/' + nas_tahun, 700, 1100);
            $('html, body').animate({scrollTop: 0}, 'slow');



        }

    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
