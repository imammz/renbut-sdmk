<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package		library/loader
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class X_Loader {

    public $_uri = false;  // SERVER REQUEST URI 
    public $_method = ''; // view the method  
    public $lang;

    /**
     * instance and define
     */
    public function __construct() {
        $this->_uri = isset($_GET['uri']) ? strip_tags(trim(preg_replace("/\/$/", "", strtolower($_GET['uri'])))) : false;
        $this->__initialize();
        $this->__library(array('database', 'session', 'request'));
    }

    function __initialize() {
        $autoload_file = BASEPATH . 'configs/autoload' . EXT;
        if (is_file($autoload_file)) {
            require $autoload_file;
            foreach ($autoload as $key => $value) {
                if ($key == 'libraries') {
                    $base_library = BASEPATH . 'libraries/';
                    foreach ($value as $li) {
                        $l = ucfirst($li);
                        $file_lib = $base_library . $l . EXT;
                        if (is_file($file_lib)) {
                            include_once $file_lib;
                            $name = strtolower($li);
                            $this->$name = new $l();
                        }
                    }
                }
                if ($key == 'helpers') {
                    $base_helper = BASEPATH . 'helpers/';
                    foreach ($value as $h) {
                        $file_helper = $base_helper . $h . '_helper' . EXT;
                        if (is_file($file_helper)) {
                            include_once $file_helper;
                        }
                    }
                }
            }
        }
    }

    /**
     * load library
     * @param type $lib 
     */
    function __library($lib = array(), $app = null) {
        if (is_array($lib)) {
            $base_library_mod = MODPATH . $app . '/libraries/';
            // additional extention
            $base_library_ext = EXTPATH . $app . '/libraries/';
            if (is_dir($base_library_mod)) {
                $base_library = $base_library_mod;
            } else if (is_dir($base_library_ext)) {
                // additional ext
                $base_library = $base_library_ext;
            } else {
                $base_library = BASEPATH . 'libraries/';
            }

            if (is_dir($base_library)) {
                foreach ($lib as $li) {
                    $l = strip_tags(ucfirst($li));
                    $file_lib = $base_library . $l . EXT;
                    if (is_file($file_lib)) {
                        include_once $file_lib;
                        $name = strtolower($l);
                        switch ($name) {
                            case 'database':
                                $name = 'db';
                                break;
                            case 'x_config':
                                $name = 'config';
                            default:
                                break;
                        }
                        $this->$name = new $l();
                    }
                }
            }
        }
    }

    /**
     * load model
     * @param type $mod 
     */
    function __model($mod = array(), $app = null) {
        if (is_array($mod)) {
            $base_model_mod = MODPATH . $app . '/models/';
            // additional extention
            $base_model_ext = EXTPATH . $app . '/models/';
            if (is_dir($base_model_mod)) {
                $base_model = $base_model_mod;
            } else if (is_dir($base_model_ext)) {
                // additional extention
                $base_model = $base_model_ext;
            } else {
                $base_model = BASEPATH . 'models/';
            }

            if (is_dir($base_model)) {
                foreach ($mod as $mo) {
                    $m = strip_tags(ucfirst($mo));
                    if (is_file($base_model . $m . EXT)) {
                        include_once $base_model . $m . EXT;
                        $name = strtolower($m);
                        $this->$name = new $m();
                    }
                }
            }
        }
    }

    function __helper($help = array(), $app = null) {
        if (is_array($help)) {
            // if model path not define 
            $base_helper_mod = MODPATH . $app . '/helpers/';
            // additional extention
            $base_helper_ext = EXTPATH . $app . '/helpers/';
            if (is_dir($base_helper_mod)) {
                $base_helper = $base_helper_mod;
            } else if (is_dir($base_helper_ext)) {
                $base_helper = $base_helper_ext;
            } else {
                $base_helper = BASEPATH . 'helpers/';
            }
            if (is_dir($base_helper)) {
                foreach ($help as $hl) {
                    $h = strip_tags(strtolower($hl));
                    if (is_file($base_helper . $h . '_helper' . EXT)) {
                        include_once $base_helper . $h . '_helper' . EXT;
                    }
                }
            }
        }
    }

    /**
     *
     * @param type $lang
     * @param type $app 
     */
    function __language($langs = array(), $app = null) {
        if (is_array($langs)) {
            $this->__library(array('session'));
            $lang_ = ($this->session->get_data('lang')) ? $this->session->get_data('lang') : x_default_lang();

            $base_lang_mod = MODPATH . $app . '/languages/' . $lang_ . '/';
            $base_lang_ext = EXTPATH . $app . '/languages/' . $lang_ . '/';
            if (is_dir($base_lang_mod)) {
                $base_lang = $base_lang_mod;
            } else if (is_dir($base_lang_ext)) {
                $base_lang = $base_lang_ext;
            } else {
                $base_lang = BASEPATH . 'languages/' . $lang_ . '/';
            }

            $base_default = BASEPATH . 'languages/' . $lang_ . '/';
            if (is_dir($base_default)) {
                if ($handle = opendir($base_default)) {
                    while (false !== ($lang_default = readdir($handle))) {
                        if ($lang_default != "." && $lang_default != "info.json" && $lang_default != ".." && $lang_default != "index.html") {
                            require $base_default . $lang_default;
                            foreach ($lang as $key => $val) {
                                $this->lang[$key] = $val;
                            }
                        }
                    }
                }
            }
            if (is_dir($base_lang)) {
                if ($handle = opendir($base_lang)) {
                    while (false !== ($lang_file = readdir($handle))) {
                        if ($lang_file != "." && $lang_file != "info.json" && $lang_file != ".." && $lang_file != "index.html") {
                            require $base_lang . $lang_file;
                            foreach ($lang as $key => $val) {
                                $this->lang[$key] = $val;
                            }
                        }
                    }
                }
            }
        }
    }

    function __view($app = null, $file = null, $data = array()) {
        if ($file != null) {
            if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $$key = $value;
                }
            }
            $view_mod = MODPATH . $app . '/views/' . $file . EXT;
            $view_ext = EXTPATH . $app . '/views/' . $file . EXT;
            if (is_file($view_mod)) {
                $include = $view_mod;
            } else {
                $include = $view_ext;
            }
            if (is_file($include)) {
                include_once $include;
            }
        }
    }

    function __template($file = null, $data = array()) {
        if ($file != null) {
            if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $$key = $value;
                }
            }
            $include = THEMEPATH . $file . EXT;
            if (is_file($include)) {
                include_once $include;
            } else if (is_file($file . EXT)) {

                include_once $file . EXT;
            }
        }
    }

    function __widget($file = null) {
        if ($file != null) {
            $include = WIPATH . $file . '/main_wi' . EXT;
            if (is_file($include)) {
                include_once $include;
            }
        }
    }

    public function __config($file = null, $app = null) {
        if ($file != null) {
            $file_mod = MODPATH . $app . '/configs/' . $file . JSON;
            $file_ext = EXTPATH . $app . '/configs/' . $file . JSON;
            if (is_file($file_mod)) {
                $json = x_file_get_content($file_mod);
            } else {
                $json = x_file_get_content($file_ext);
            }
        } else {
            $json = x_file_get_content(BASEPATH . 'configs/' . $file . JSON);
        }
        return x_read_json_decode($json);
    }

    public function __composer($app = null) {
        if ($app != null) {
            $composer = $this->__config('composer', $app);
            foreach ($composer['required'] as $k => $c) {
                if (is_array($c) && count($c) > 0) {
                    foreach ($c as $r) {
                        switch ($k) {
                            case 'libraries':
                                
                                if(is_array($r)){
                                    foreach ($r as $key => $value) {
                                        $this->__library(array($value), $key);
                                    }
                                }else{ 
                                    $this->__library(array($r));
                                }
                                break;
                            case 'models':
                                if(is_array($r)){
                                    foreach ($r as $key => $value) {
                                        $this->__model(array($value), $key);
                                    }
                                }else{
                                    $this->__model(array($r), $app);
                                }
                                break;
                            case 'helpers':
                                if(is_array($r)){
                                    foreach ($r as $key => $value) {
                                        $this->__helper(array($value), $key);
                                    }
                                }else{
                                    $this->__helper(array($r));
                                }
                                break;
                            case 'languages': 
                                if(is_array($r)){
                                    foreach ($r as $key => $value) {
                                        $this->__language(array($value), $key);
                                    }
                                }else{
                                    $this->__language(array($r));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }
    }

}

