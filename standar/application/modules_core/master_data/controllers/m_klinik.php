<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_klinik extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('klinik_model','klinik');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Klinik";
		$data['subtitle'] = "Daftar Klinik";
		$this->load->view('klinik/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Klinik";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->klinik->get_by_id($id);
		}
		$this->load->view('klinik/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->klinik->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $klinik) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.strtoupper($klinik->kode_klinik).' ]';
			$row[] = strtoupper($klinik->nama_klinik);
			$row[] = $klinik->nama_provinsi;
			$row[] = $klinik->nama_kabupaten;
			$row[] = $klinik->alamat;
			$row[] = $klinik->no_telp;
			$row[] = $klinik->kelas_klinik;
			$row[] = ($klinik->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $klinik->updated_date?Tanggal::formatDateTime($klinik->updated_date):Tanggal::formatDateTime($klinik->created_date);

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($klinik->id_klinik,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_klinik('."'".Regex::_genRegex($klinik->id_klinik,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->klinik->count_all(),
						"recordsFiltered" => $this->klinik->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	public function ajax_add()
	{
		$id_klinik = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'kode_klinik' => Regex::_genRegex($this->input->post('kode_klinik'), 'RGXQSL'),
			'nama_klinik' => Regex::_genRegex($this->input->post('nama_klinik'), 'RGXQSL'),
			'kelas_klinik' => Regex::_genRegex($this->input->post('kelas_klinik'), 'RGXQSL'),
			'id_kabupaten' => Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXQSL'),
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXQSL'),
			'alamat' => Regex::_genRegex($this->input->post('alamat'), 'RGXQSL'),
			'no_telp' => Regex::_genRegex($this->input->post('no_telp'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ')
		);
		
		if( $id_klinik == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->id_user;
			$this->klinik->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->id_user;
			$this->klinik->update(array('id_klinik'=>$id_klinik), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->klinik->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
