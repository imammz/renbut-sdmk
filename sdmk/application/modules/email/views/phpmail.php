<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>  
<?php 
//Set who the message is to be sent from
$mail->SetFrom($sender['email'], $sender['name']);
//Set an alternative reply-to address
$mail->AddReplyTo($replyTo['email'],$replyTo['name']);
//Set who the message is to be sent to
$mail->AddAddress($reciper['email'], $reciper['name']);
//Set the subject line
$mail->Subject = $subject;
//Read an HTML message body from an external file, convert referenced images to embedded, convert HTML into a basic plain-text alternative body
$mail->MsgHTML($message);
//Replace the plain text body with one created manually
$mail->AltBody = '';
//Attach an image file
//$mail->AddAttachment(MODPATH.'private/email/images/phpmailer_mini.gif');

//Send the message, check for errors
if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
}  