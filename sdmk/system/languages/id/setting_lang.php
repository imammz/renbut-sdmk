<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['this_page_to_manage_website_setting,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola pengaturan website, lihat dokumentasi untuk menggunakannya';
$lang['info'] = 'info';
$lang['general'] = 'umum';
$lang['media'] = 'media';
$lang['database'] = 'database';
$lang['email'] = 'email';
$lang['language'] = 'bahasa';
/**
 * info
 */
$lang['engine'] = 'mesin';
$lang['version'] = 'versi';
$lang['copyright'] = 'copyright';
$lang['develop_by'] = 'pengembang oleh';
$lang['this_is_engine_cms_was_create_your_website'] = 'ini adalah mesin cms yang membangun websitemu';
$lang['this_is_engine_version'] = 'ini adalah versi mesin';
$lang['this_is_official_website'] = 'ini adalah situs website resmi';
/**
 * database 
 */
$lang['driver'] = 'driver';
$lang['host'] = 'host';
$lang['user'] = 'user';
$lang['password'] = 'password';
$lang['name'] = 'name';
$lang['database_driver'] = 'database driver';
$lang['database_host'] = 'database host';
$lang['database_name'] = 'database name';
$lang['database_user'] = 'database user';
$lang['database_password'] = 'database password';
$lang['restore'] = 'kembalikan';
$lang['download'] = 'unduh';
$lang['backup'] = 'cadangkan';
$lang['check_all'] = 'pilih semua';
$lang['list_table'] = 'daftar tabel';
$lang['list_backup'] = 'daftar cadangan';
$lang['backup_DB_on'] = 'cadangan database pada';
/**
 * general 
 */
$lang['site_url'] = 'base URL';
$lang['site_name'] = 'site name';
$lang['site_domain'] = 'site domain';
$lang['site_keyword'] = 'site keyword';
$lang['site_description'] = 'site description';
$lang['sess_name'] = 'session name';
$lang['sess_expired'] = 'session expired';
$lang['sess_encrypt'] = 'session encryptpe';
$lang['sess_encrypt_code'] = 'session encryptpe code';
$lang['default_base_url'] = 'default base url';
$lang['your_site_name'] = 'nama situs website mu';
$lang['your_site_domain'] = 'nama domain website';
$lang['your_session_name'] = 'nama session website';
$lang['your_site_keyword'] = 'kata kunci situ mu';
$lang['your_site_description'] = 'keterangan tentang situs mu';
$lang['session_expired_in_second'] = 'lama berlaku session dalam detik';
$lang['encryptpe_your_session'] = 'enkripsi sesion';
$lang['session_encryptpe_code'] = 'kode enkripsi sesion';
$lang['date_format'] = 'Format tanggal';
$lang['default_language'] = 'Bahasa bawaan';
$lang['default_date_format_of_system'] = 'format tanggal bawaan untuk sistem';
$lang['default_language_of_system'] = 'bahasa bawaan untuk sistem';
$lang['google_analytics'] = 'google analytics';
$lang['google_analytics_tracking_code'] = 'kode google analytics tracking';
/**
 * media 
 */
$lang['type_document'] = 'jenis dokumen';
$lang['type_zip'] = 'jenis zip';
$lang['type_image'] = 'jenis image';
$lang['max_file_size'] = 'maksimal ukuran file';
$lang['big_thumb_width'] = 'lebar gambar besar';
$lang['big_thumb_height'] = 'tinggi gambar besar';
$lang['middle_thumb_width'] = 'lebar gambar tengah';
$lang['middle_thumb_height'] = 'tinggi gambar tengah';
$lang['small_thumb_width'] = 'lebar gambar kecil';
$lang['small_thumb_height'] = 'tinggi gambar kecil';
$lang['type_file_document_allowed_to_upload_in_folder_document,_if_type_more_than_one_split_with(|)'] = 'tipe file document yang diizinkan di folder document, jika lebih dari satu pisahkan dengan tanda(|)';
$lang['type_file_zip_allowed_to_upload_in_folder_zip,_if_type_more_than_one_split_with(|)'] = 'tipe file zip yang diizikan di folder zip, jika lebih dari satu pisahkan dengan tanda(|)';
$lang['type_file_image_allowed_to_upload_in_folder_image,_if_type_more_than_one_split_with(|)'] = 'tipe file image yang diizikan di folder image, jika lebih dari satu pisahkan dengan tanda(|)';
$lang['maximal_size_of_file,_that_you_upload'] = 'maximal ukuran file, yang kamu unggah';
$lang['size_width_for_image_big_thumb'] = 'ukuran lebar untuk gambar big thumb';
$lang['size_height_for_image_big_thumb'] = 'ukuran tinggi untuk gambar big thumb';
$lang['size_width_for_image_middle_thumb'] = 'ukuran lebar untuk gambar middle thumb';
$lang['size_height_for_image_middle_thumb'] = 'ukuran tinggi untuk gambar middle thumb';
$lang['size_width_for_image_small_thumb'] = 'ukuran lebar untuk gambar small thumb';
$lang['size_height_for_image_small_thumb'] = 'ukuran tinggi untuk gambar small thumb'; 
$lang['manage_your_configuration_media_file,_see_documentation_for_use_it'] = 'Kelola konfigurasi media file  Anda, lihat dokumentasi untuk menggunakannya';
$lang['manage_your_configuration_project_file,_see_documentation_for_use_it'] = 'Kelola konfigurasi proyek file  Anda, lihat dokumentasi untuk menggunakannya';
/**
 *  email
 */
$lang['template'] = 'template';
$lang['no_template_selected'] = 'tidak ada template yang terpilih';
$lang['no_email_configuration'] = 'Tidak ada pengaturan email';
$lang['send_email_use_phpmail'] = 'Send email use phpmail';
$lang['send_email_use_smtp'] = 'Send email use smtp';
$lang['send_email_use_google_smtp'] = 'Send email use google smtp';
$lang['email_reply'] = 'Balas';
$lang['email_sender'] = 'Pengirim';
$lang['port'] = 'Port';
$lang['username'] = 'Username';
$lang['set_the_hostname_of_the_mail_server'] = 'Atur host name mail server';
$lang['set_the_SMTP_port_number_-_likely_to_be_25,_26,_465_or_587'] = 'Atur nomor port SMTP -misalnya 25, 26, 465 or 587';
$lang['username_to_use_for_SMTP_authentication'] = 'Username untuk menggunakan SMTP authentication';
$lang['password_to_use_for_SMTP_authentication'] = 'Password untuk menggunakan SMTP authentication';
/**
 * security
 */
$lang['security'] = 'Keamanan';
$lang['xss_filtering'] = 'XSS Filtering';
$lang['sql_injection'] = 'SQL Injection';
$lang['ip_proxy'] = 'IP Proxy';
$lang['set_true,_to_make_your_website_defence_from_xss_filtering'] = 'Pilih true, untuk membuat pertahanan website Anda dari XSS filtering';
$lang['set_true,_to_make_your_website_defence_from_sql_injection'] = 'Pilih true, untuk membuat pertahanan website Anda dari SQL injection';
$lang['defend_your_website_from_ip,_separate_with_[#]'] = 'Mempertahankan website Anda dari ip, pisahkan dengan [#]';
/**
 * modules/extension
 */
$lang['choose_extension_to_set'] = 'Pilih Ektensi untuk mengatur';
$lang['choose_module_to_set'] = 'Pilih Module untuk mengatur';