<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/setting
 * @author		xamzo Dev Team 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Setting extends X_Controller {

    public function __construct() {
        parent::__construct(); 
        $this->__composer('setting');
    }
 
    public function index() {
        if (is_ajax_request()) {
            if (is_ajax_request() && is_login()) {
                $json = x_file_get_content(BASEPATH . 'configs/info' . JSON);
                $view['con'] = x_read_json_decode($json);
                $this->__view('setting', 'index', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    public function general() {
        if (is_ajax_request()) {
            if (is_auth('con_pengaturan_umum')) {
                $json = x_file_get_content(BASEPATH . 'configs/general' . JSON);
                $view['con'] = x_read_json_decode($json);
                $this->__view('setting', 'general', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    public function security() {
        if (is_ajax_request()) {
            if (is_auth('con_keamanan')) {
                $json = x_file_get_content(BASEPATH . 'configs/security' . JSON);
                $view['con'] = x_read_json_decode($json);
                $this->__view('setting', 'security', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    } 

    public function upload($target) {
        if (is_ajax_request()) {
            if (is_auth('con_media')) {
                $this->__library(array('lib_media'), 'media');
                $view['_target'] = $target;
                $this->__view('setting', 'helpers/app/upload', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function do_upload() {
        if (is_auth('con_media', 'w')) {
            $this->__library(array('lib_media'), 'media');
            $user_id = $this->session->get_data('session_user_id');
            $file = $_FILES['userfile'];
            $tmpFilePath = $file['tmp_name'];
            $uploaddir = UPLOADPATH . "s/";
            if ($tmpFilePath != "") {
                $uploadfile = $uploaddir . basename($file['name']);
                $theFileSize = $file['size'];
                if ($theFileSize > $this->lib_media->_config('max_file_size')) {
                    echo '{"status":"error"}';
                    exit();
                }
                if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
                    $userfile_name = $file['name'];
                    $t = substr($userfile_name, strrpos($userfile_name, '.') + 1);
                    $f = explode('.', $userfile_name);
                    $arrep = array('#', '$', '@', '{x}', ' ', $t);
                    $oldname = str_replace($arrep, '', $f['0']);
                    $new_name = "setting" . '{x}' . $oldname . '{x}' . date('Y-m-d') . '{x}' . date('h-i-s') . '{x}' . $user_id;
                    chmod($uploadfile, 0777);
                    rename($uploadfile, $uploaddir . $new_name . ".$t");
                    $filename = $uploaddir . $new_name . ".$t";
                    echo '{"status":"success","file":"' . $filename . '"}';
                } else {
                    echo '{"status":"error"}';
                }
            }
        } else {
            print_r("You don't have access in this module");
        }
    }
  
 
    public function database() {
        if (is_ajax_request()) {
            if (is_auth('con_database')) {
                $view['con'] = x_db_config();
                $view['tables_name'] = $this->db->query('SHOW TABLES')->result_array();
                $this->__view('setting', 'database', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    public function session_change($key = null, $val = null, $hash = null, $obj = null) {
        if ($key != null && $val !== null) {
            $this->session->set_data(array($key => $val));
            $back = ($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : site_url();
            if ($hash != null) {
                $hs = str_replace('-', '/', $hash);
                header("Location: $back#$hs/$obj");
            } else {
                header("Location: $back");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    function security_update() {
        if (is_ajax_request()) {
            if (is_auth('con_keamanan', 'w')) {
                x_update_setting(BASEPATH . '/configs/security' . JSON);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    function general_update() {
        if (is_ajax_request()) {
            if (is_auth('con_pengaturan_umum', 'w')) {
                x_update_setting(BASEPATH . '/configs/general' . JSON);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    function db_backup() {
        if (is_ajax_request()) {
            if (is_auth('con_database', 'w')) {
                $connection = x_db_config();
                $db_name = $connection['database']['name'];
                $base = BACKUP;
                $base = str_replace("\\", '/', $base);
                $new_dir = $base . date('Y.m.d.h.i.s');
                mkdir($new_dir, 0777);
                $id = $this->request->post("id", false);
                for ($i = 0; $i < count($id); $i++) {
                    $table = $id[$i];
                    $num_fields = $this->db->get($table)->num_rows();
                    $num_cols = $this->db->query("SELECT count(*) AS total FROM information_schema.columns WHERE table_schema = '$db_name' AND table_name = '$table'")->row()->total;
                    $return = ''; 
                    $row2 = $this->db->query('SHOW CREATE TABLE ' . $table)->row_array();
                    if (isset($row2['Create Table'])) {
                        $return .= "\n\n" . $row2['Create Table'] . ";\n\n";
                    } 
                        foreach ($this->db->query("SELECT * FROM $table")->result_both() as $row) {
                            $return.= 'INSERT INTO ' . $table . ' VALUES(';
                            for ($j = 0; $j < $num_cols; $j++) {
                                if (isset($row[$j])) {
                                    $return .= '"' . $row[$j] . '"';
                                } else {
                                    $return .= '""';
                                }
                                if ($j < ($num_cols - 1)) {
                                    $return.= ',';
                                }
                            }
                            $return.= ");\n";
                        } 
                    $return.="\n\n";   
                    $handle = fopen("$new_dir/$table.sql", 'w+');
                    fwrite($handle, $return); 
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    function db_restore() {
        if (is_ajax_request()) {
            if (is_auth('con_database', 'w')) {
                $dir = $this->request->post("dir", false);
                $base = BACKUP;
                $base = str_replace("\\", '/', $base);
                $new_dir = $base . $dir;
                $base_u = BACKUP . "backup";

                $ca = x_list_dir($base_u . "/" . $dir);
                for ($j = 0; $j < count($ca); $j++) {
                    $txt = explode("/", $ca[$j]);
                    $text = str_replace('.sql', '', $txt[count($txt) - 1]);
                    if ($this->db->query("DELETE FROM $text")->exec()) {
                        $sql_file = "$new_dir/$text.sql";
                        $FP = fopen($sql_file, 'r');
                        $RE = fread($FP, filesize($sql_file));
                        $READ = explode(";\n", $RE);
                        foreach ($READ AS $RED) {
                            $this->db->query($RED)->exec();
                        }
                    }
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    function db_delete() {
        if (is_ajax_request()) {
            if (is_auth('con_database', 'w')) {
                $dir = $this->request->post("dir", false);
                $base_u = BACKUP . $dir;
                x_recursive_delete($base_u);
                echo $dir;
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
 
    function download_db($dir) {
        $this->__library(array('zip'));
        $path = BACKUP;

        $this->zip->read_dir($path);
 
        $this->zip->download($dir.'.zip');
    }

 
    function date_format() {
        if (is_ajax_request() && is_auth('setting', 'x')) {
            $this->__view('setting', 'date_format');
        }
    }

    function default_language() {
        if (is_ajax_request() && is_auth('con_database', 'x')) {
            $languages = BASEPATH . 'languages/';
            $data = array();
            if ($dir_handle = opendir($languages)) {
                while ($dir = readdir($dir_handle)) {
                    if ($dir != "." && $dir != "..") {
                        $info = $languages . $dir . '/info' . JSON;
                        if (is_file($info)) {
                            $json = x_file_get_content($info);
                            $lang = x_read_json_decode($json);
                            $data[] = $dir . ' (' . $lang['name'] . ')';
                        }
                    }
                }
            }
            $view['languages'] = $data;
            $this->__view('setting', 'default_language', $view);
        }
    }
    
    public function _report() {
        if (is_ajax_request()) {
            if (is_auth('con_media')) {
                $json = x_file_get_content(MODPATH . 'media/configs/report' . JSON);
                $view['con'] = x_read_json_decode($json);
                $this->__view('setting', 'print', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }
    
    public function _project() {
        if (is_ajax_request()) {
            if (is_auth('con_media')) {
                $json = x_file_get_content(MODPATH . 'media/configs/project' . JSON);
                $view['con'] = x_read_json_decode($json);
                $this->__view('setting', 'project', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

}