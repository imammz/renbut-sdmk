<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_standar_sdmk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_standar_sdmk_model','standar_sdmk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Standar SDMK";
		$data['subtitle'] = "Daftar Standar SDMK";
		$data['tab_title'] = "Standar Minimal SDMK Puskesmas";
		$this->load->view('standar_sdmk/index', $data);
	}

	public function tab_rsu()
	{
		
		$data['title'] = "Pengaturan Standar SDMK";
		$data['subtitle'] = "Daftar Standar SDMK";
		$data['tab_title'] = "Standar Minimal SDMK RSU (Rumah Sakit Umum)";
		$this->load->view('standar_sdmk/index_rsu', $data);
	}

	public function tab_rsk()
	{
		
		$data['title'] = "Pengaturan Standar SDMK";
		$data['subtitle'] = "Daftar Standar SDMK";
		$data['tab_title'] = "Standar Minimal SDMK RSK (Rumah Sakit Khusus)";
		$this->load->view('standar_sdmk/index_rsk', $data);
	}

	public function tab_kkp()
	{
		
		$data['title'] = "Pengaturan Standar SDMK";
		$data['subtitle'] = "Daftar Standar SDMK";
		$data['tab_title'] = "Standar Minimal SDMK KKP (Kantor Kesehatan Pelabuhan)";
		$this->load->view('standar_sdmk/index_kkp', $data);
	}

	public function tab_btkl()
	{
		
		$data['title'] = "Pengaturan Standar SDMK";
		$data['subtitle'] = "Daftar Standar SDMK";
		$data['tab_title'] = "Standar Minimal SDMK BTKL (Balai Teknik Kesehatan Lingkungan)";
		$this->load->view('standar_sdmk/index_btkl', $data);
	}

	public function tab_klinik()
	{
		
		$data['title'] = "Pengaturan Standar SDMK";
		$data['subtitle'] = "Daftar Standar SDMK";
		$data['tab_title'] = "Standar Minimal SDMK Klinik";
		$this->load->view('standar_sdmk/index_klinik', $data);
	}

	public function form($id='')
	{
		if($id==''){
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}
		$data['title'] = "Form Standar SDMK";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->standar_sdmk->get_by_id($id);
		}
		$this->load->view('standar_sdmk/form', $data);
	}

	public function find_content($type){

		switch ($type) {
			case 'puskesmas':
				# code...
				$this->ajax_search_puskesmas($type);
				break;

			case 'rsu':
				# code...
				$this->ajax_search_rsu($type);
				break;

			case 'rsk':
				# code...
				$this->ajax_search_rsk($type);
				break;

			case 'kkp':
				# code...
				$this->ajax_search_kkp($type);
				break;

			case 'btklpp':
				# code...
				$this->ajax_search_btkl($type);
				break;

			case 'klinik':
				# code...
				$this->ajax_search_klinik($type);
				break;
			
			default:
				# code...
				break;
		}
	}

	public function ajax_search_puskesmas($type)
	{
		$list = $this->standar_sdmk->get_datatables($type);
		$data = array();
		$no = ($_POST['start'])?$_POST['start']:0;
		
		foreach ($list as $standar_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$standar_sdmk->id_jenis_sdmk.' ]';
			$row[] = strtoupper($standar_sdmk->nama_jenis_sdmk);
			$row[] = strtoupper($standar_sdmk->nama_tipe_puskesmas);
			$row[] = strtoupper($standar_sdmk->nama_tipe_kawasan);

			//add html for action
			$row[] = '<input type="text" name="jml_standar[]" id="'.$standar_sdmk->id_standar.'" value="'.$standar_sdmk->jml_standar.'" onKeyUp="update_row('."'".Regex::_genRegex($standar_sdmk->id_standar,'RGXINT')."'".')">';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->standar_sdmk->count_all_puskesmas(),
						"recordsFiltered" => $this->standar_sdmk->count_filtered($type),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_search_rsu($type)
	{
		$list = $this->standar_sdmk->get_datatables($type);
		$data = array();
		$no = ($_POST['start'])?$_POST['start']:0;
		
		foreach ($list as $standar_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$standar_sdmk->id_jenis_sdmk.' ]';
			$row[] = strtoupper($standar_sdmk->nama_jenis_sdmk);
			$row[] = '<input type="text" name="jml_standar_a[]" value="'.$standar_sdmk->A.'" style="width: 100px" id="A_rsu_'.$standar_sdmk->id_standar_rsu.'" onKeyUp="update_row_with_class('."'A_rsu_".Regex::_genRegex($standar_sdmk->id_standar_rsu,'RGXINT')."'".')">';
			$row[] = '<input type="text" name="jml_standar_b[]" value="'.$standar_sdmk->B.'" style="width: 100px" id="B_rsu_'.$standar_sdmk->id_standar_rsu.'" onKeyUp="update_row_with_class('."'B_rsu_".Regex::_genRegex($standar_sdmk->id_standar_rsu,'RGXINT')."'".')">';
			$row[] = '<input type="text" name="jml_standar_c[]" value="'.$standar_sdmk->C.'" style="width: 100px" id="C_rsu_'.$standar_sdmk->id_standar_rsu.'" onKeyUp="update_row_with_class('."'C_rsu_".Regex::_genRegex($standar_sdmk->id_standar_rsu,'RGXINT')."'".')">';
			$row[] = '<input type="text" name="jml_standar_d[]" value="'.$standar_sdmk->D.'" style="width: 100px" id="D_rsu_'.$standar_sdmk->id_standar_rsu.'" onKeyUp="update_row_with_class('."'D_rsu_".Regex::_genRegex($standar_sdmk->id_standar_rsu,'RGXINT')."'".')">';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->standar_sdmk->count_all_rsu(),
						"recordsFiltered" => $this->standar_sdmk->count_filtered($type),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_search_rsk($type)
	{
		$list = $this->standar_sdmk->get_datatables($type); //print_r($this->db->last_query());die;
		$data = array();
		$no = ($_POST['start'])?$_POST['start']:0;
		
		foreach ($list as $standar_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$standar_sdmk->id_jenis_sdmk.' ]';
			$row[] = strtoupper($standar_sdmk->nama_jenis_sdmk);
			$row[] = $standar_sdmk->nama_jenis_faskes;
			$row[] = '<input type="text" style="width: 100px" onKeyUp="update_row_with_class('."'A_rsk_".Regex::_genRegex($standar_sdmk->id_standar_rsk,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->A.'" id="A_rsk_'.$standar_sdmk->id_standar_rsk.'">';
			$row[] = '<input type="text" style="width: 100px" onKeyUp="update_row_with_class('."'B_rsk_".Regex::_genRegex($standar_sdmk->id_standar_rsk,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->B.'" id="B_rsk_'.$standar_sdmk->id_standar_rsk.'">';
			$row[] = '<input type="text" style="width: 100px" onKeyUp="update_row_with_class('."'C_rsk_".Regex::_genRegex($standar_sdmk->id_standar_rsk,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->C.'" id="C_rsk_'.$standar_sdmk->id_standar_rsk.'">';
			$row[] = '<input type="text" style="width: 100px" onKeyUp="update_row_with_class('."'D_rsk_".Regex::_genRegex($standar_sdmk->id_standar_rsk,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->D.'" id="D_rsk_'.$standar_sdmk->id_standar_rsk.'">';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->standar_sdmk->count_all_rsu(),
						"recordsFiltered" => $this->standar_sdmk->count_filtered($type),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_search_kkp($type)
	{
		$list = $this->standar_sdmk->get_datatables($type);
		$data = array();
		$no = ($_POST['start'])?$_POST['start']:0;
		
		foreach ($list as $standar_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$standar_sdmk->id_jenis_sdmk.' ]';
			$row[] = strtoupper($standar_sdmk->nama_jenis_sdmk);
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'I_kkp_".Regex::_genRegex($standar_sdmk->id_standar_kkp,'RGXINT')."'".')" name="jml_standar[]" style="width: 100px" value="'.$standar_sdmk->I.'" id="I_kkp_'.$standar_sdmk->id_standar_kkp.'">';
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'II_kkp_".Regex::_genRegex($standar_sdmk->id_standar_kkp,'RGXINT')."'".')" name="jml_standar[]" style="width: 100px" value="'.$standar_sdmk->II.'" id="II_kkp_'.$standar_sdmk->id_standar_kkp.'">';
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'III_kkp_".Regex::_genRegex($standar_sdmk->id_standar_kkp,'RGXINT')."'".')" name="jml_standar[]" style="width: 100px" value="'.$standar_sdmk->III.'" id="III_kkp_'.$standar_sdmk->id_standar_kkp.'">';
			/*$row[] = '<input type="text" onKeyUp="update_row_with_class('."'D_kkp_".Regex::_genRegex($standar_sdmk->id_standar_kkp,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->D.'" id="D_kkp_'.$standar_sdmk->id_standar_kkp.'">';*/
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->standar_sdmk->count_all_kkp(),
						"recordsFiltered" => $this->standar_sdmk->count_filtered($type),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_search_btkl($type)
	{
		$list = $this->standar_sdmk->get_datatables($type);
		$data = array();
		$no = ($_POST['start'])?$_POST['start']:0;
		
		foreach ($list as $standar_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$standar_sdmk->id_jenis_sdmk.' ]';
			$row[] = strtoupper($standar_sdmk->nama_jenis_sdmk);
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'BB_btklpp_".Regex::_genRegex($standar_sdmk->id_standar_btklpp,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->BB.'" id="BB_btklpp_'.$standar_sdmk->id_standar_btklpp.'">';
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'I_btklpp_".Regex::_genRegex($standar_sdmk->id_standar_btklpp,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->I.'" id="I_btklpp_'.$standar_sdmk->id_standar_btklpp.'">';
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'II_btklpp_".Regex::_genRegex($standar_sdmk->id_standar_btklpp,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->II.'" id="II_btklpp_'.$standar_sdmk->id_standar_btklpp.'">';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->standar_sdmk->count_all_btkl(),
						"recordsFiltered" => $this->standar_sdmk->count_filtered($type),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_search_klinik($type)
	{
		$list = $this->standar_sdmk->get_datatables($type);
		$data = array();
		$no = ($_POST['start'])?$_POST['start']:0;
		
		foreach ($list as $standar_sdmk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$standar_sdmk->id_jenis_sdmk.' ]';
			$row[] = strtoupper($standar_sdmk->nama_jenis_sdmk);
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'utama_klinik_".Regex::_genRegex($standar_sdmk->id_standar_klinik,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->utama.'" id="utama_klinik_'.$standar_sdmk->id_standar_klinik.'">';
			$row[] = '<input type="text" onKeyUp="update_row_with_class('."'pratama_klinik_".Regex::_genRegex($standar_sdmk->id_standar_klinik,'RGXINT')."'".')" name="jml_standar[]" value="'.$standar_sdmk->pratama.'" id="pratama_klinik_'.$standar_sdmk->id_standar_klinik.'">';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->standar_sdmk->count_all_klinik(),
						"recordsFiltered" => $this->standar_sdmk->count_filtered($type),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_search_by_params()
	{
		//print_r($_POST);die;

		//$this->db->trans_begin();

		$params = array(
			'id_jenis_faskes' => Regex::_genRegex($this->input->post('id_jenis_faskes'), 'RGXINT'),
			'id_tipe_kawasan' => Regex::_genRegex($this->input->post('id_tipe_kawasan'), 'RGXINT'),
			'id_tipe_puskesmas' => Regex::_genRegex($this->input->post('id_tipe_puskesmas'), 'RGXINT')
		);

		return $this->ajax_search($params);
		



		/*if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}*/
		
	}

	public function ajax_add()
	{
		$id_standar_sdmk = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'nama_standar_sdmk' => Regex::_genRegex($this->input->post('nama_standar_sdmk'), 'RGXQSL'),
			'id_kelompok_sdmk' => Regex::_genRegex($this->input->post('id_kelompok_sdmk'), 'RGXINT'),
			'id_jenis_faskes' => Regex::_genRegex($this->input->post('id_jenis_faskes'), 'RGXINT'),
			'parent' => Regex::_genRegex($this->input->post('id_standar_sdmk'), 'RGXINT'),
			'counter' => Regex::_genRegex($this->input->post('counter'), 'RGXINT'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_standar_sdmk == 0 ){
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->standar_sdmk->save($dataexc);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->standar_sdmk->update(array('id_standar_sdmk'=>$id_standar_sdmk), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->standar_sdmk->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function update_row($id_standar)
	{
		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
		$this->standar_sdmk->update('puskesmas', array('id_standar'=>$id_standar), array('jml_standar'=>$this->input->post('value'))); //print_r($this->db->last_query());
		echo json_encode(array("status" => TRUE));
	}

	public function update_row_with_class($params)
	{

		$exp_params = explode('_', $params);
		//print_r($exp_params);
		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
		$this->standar_sdmk->update($exp_params[1], array('id_standar_'.$exp_params[1].''=>$exp_params[2]), array(''.$exp_params[0].''=>$this->input->post('value'))); //print_r($this->db->last_query());
		echo json_encode(array("status" => TRUE));
	}

	
}
