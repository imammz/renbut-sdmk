<?php
header("Pragma: public");
header('Content-Type: application/vnd.ms-excel');
$filename = 'Faskes_' . date('d-M-Y') . '.xls';
header('Content-Disposition: attachment; filename=' . $filename);

$abk_model = new abk_model;
$terbilang = new Terbilang();

$html = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Sheet 1</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
	
<style type="text/css">
.rotate-text
 {

/* Safari */
-webkit-transform: rotate(-90deg);

/* Firefox */
-moz-transform: rotate(-90deg);

/* IE */
-ms-transform: rotate(-90deg);

/* Opera */
-o-transform: rotate(-90deg);

/* Internet Explorer */
filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

}
.border-table{
	border:1px solid #000;	
}
</style>

<style>
    th {
        text-align: center;
    }
    
    td.number {
        text-align: right;
        padding-right: 5px;
    }
    
    td.center {
        text-align: center;
        font-weight: bolder;
    }
    
</style>
</head>

  <body style="font-size: 9px; font-family: Arial;">
<table border="1" width="100%"><tr><td>  
 <table border="1"> <tr>';

foreach ($sdmk_faskes_installasi as $row2) {
    
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $row2['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
   $html .=       '<td colspan="2"> <h2>' . $row2['sdmk_faskes_installasi_nama'] . '</h2> </td>
                     <td>       <h3>Jumlah PNS / Tenaga Tetap :' . $row2['jumlah_PNS'] . '</h3> </td>
                     <td>       <h4>Jumlah PPPK :' . $row2['jumlah_PPPK'] . '</h4> </td>
                     <td>       <h4>Jumlah PTT :' . $row2['jumlah_PTT'] . '</h4> </td>
                     <td>       <h4>Jumlah Honor Daerah :' . $row2['jumlah_HD'] . '</h4> </td>
                     <td>       <h4>Jumlah DLL :' . $row2['jumlah_DLL'] . '</h4> </td>
                          
                            ';
}

$html .= '</tr><tr>';

 if($faskes['ref_jenis_faskes_kode']==2) {
        $html .= "<td colspan='4'><h3>{$faskes['faskes_nama']} - {$faskes_installasi['installasi_nama']}  </h3> </td><td colspan='3'>"
        . "<h3>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</strong></h3> </td>";
    }
    else {
        $html .= "<td colspan='7'><h3>{$faskes['faskes_nama']} - {$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</strong></h3> </td>";
    }

$html   .= '</tr></table>'
        . ' <h3> - Standar Beban Kerja (SBK)</h3><h4>Tugas Pokok</h4>';


$html .= '<table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th>Norma Waktu</th><th>Satuan</th><th>WKT (mnt/th)</th><th>SBK <br/> (Standar Beban Kerja)</th></tr>';

$no = 1;
$kbk1 = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?', $row2['sdmk_faskes_installasi_id'], 1)->order('urut ASC');

foreach ($kbk1 as $row3) {
    $satuan = $this->orm->ref_satuan->where('satuan_id', $row3['satuan_id'])->fetch();

    $html .= '<tr><td>' . $no . '</td><td>' . $row3['kbk_uraian'] . '</td><td style="text-align: right">' . $row3['norma_waktu'] . '</td><td style="text-align: right">' . $satuan['satuan_nama'] . '</td><td style="text-align: right">' . number_format($abk_model->_getWKTMenit(), 0) . '</td><td style="text-align: right">' . $row3['SBK'] . '</td></tr>';

    $no++;
}
$html .= '</table>';





$html .= '<h3> - Menghitung Faktor Tugas Penunjang (FTP) dan Standar Tugas Penunjang (STP)</h3>'
        . '<h4>Tugas Penunjang (' . $row2['sdmk_faskes_installasi_nama'] . ')</h4>'
        . '<table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th>Norma Waktu</th><th>Satuan</th><th>Waktu Kegiatan (mnt/th)</th><th>WKT <br/> (mnt/th)</th><th style="text-align: center;"> <b> FTP <br/> % </b></th></tr>';

$no = 1;
$ftp_total = 0;
$kbk2 = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?', $row2['sdmk_faskes_installasi_id'], 2)->order('urut ASC');

foreach ($kbk2 as $row3) {
    $satuan = $this->orm->ref_satuan->where('satuan_id', $row3['satuan_id'])->fetch();
    $ftp_total += $row3['FTP'];

    $html .= '<tr><td>' . $no . '</td><td>' . $row3['kbk_uraian'] . '</td><td style="text-align: right">' . $row3['norma_waktu'] . '</td><td style="text-align: right">' . $satuan['satuan_nama'] . '</td> <td style="text-align: right">' . $row3['waktu_kegiatan'] . '</td> <td style="text-align: right">' . number_format($abk_model->_getWKTMenit(), 0) . '</td><td style="text-align: right">' . number_format($row3['FTP'] * 100, 2) . '</td></tr>';

    $no++;
}
$html .= '<tr><td colspan="5">  </td><td><b>TOTAL FTP</b></td><td style="text-align: right">' . number_format($ftp_total * 100, 2) . '</td></tr>';
$html .= '<tr><td colspan="6" style="text-align: right"><b>Standard  Tugas Penunjang  (STP)	=(1/(1 – FTP/100)</b></td><td style="text-align: right">' . $abk_model->_fmlSTP($ftp_total) . '</td></tr>'
        . '</table>';

$html .= '<h3> - Menghitung Kebutuhan SDMK</h3>';

$html .= '<table width="100%" border="1">
     <tr><th width="15">No</th><th> Tugas Pokok <br> (1)</th> <th width="100"> Capaian <br> (2)  </th> <th width="100"> SBK  <br> (3)</th> <th width="100"> Kebutuhan SDMK <br> (2)/(3) </th></tr>';

$no = 1;
$total_sdmk = 0;
$jkt = 0;
foreach ($sdmk_kbk as $row_sdmk_kbk) {
    $sdmk = $row_sdmk_kbk['capaian'] / $row_sdmk_kbk['SBK'];
    $jkt += $sdmk;
    $html .= '<tr> <td>' . $no . '</td> <td >' . $row_sdmk_kbk['kbk_uraian'] . '</td> <td style="text-align: right;">' . $row_sdmk_kbk['capaian'] . '</td> <td style="text-align: right;">' . $row_sdmk_kbk['SBK'] . '</td> <td style="text-align: right;">' . number_format($sdmk, 2) . '</td> </tr>';
    $no++;
}
$stp = $abk_model->_fmlSTP($ftp['total']);
$kebutuhan = $jkt * $stp;

if ($kebutuhan <= 5) {
    $pembulatan = Ceil($kebutuhan);
} else {
    $pembulatan = round($kebutuhan);
}

$html .= '<tr><td colspan="4"> <h4> JKT = Jumlah Kebutuhan Tenaga Tugas Pokok </h4> </td> <td style="text-align: right;">'.number_format($jkt, 2).'</td></tr>';
$html .= '<tr><td colspan="4"> <h4> STP = Standard Tugas Penunjang </h4> </td> <td style="text-align: right;">'.$stp.'</td></tr>';
$html .= '<tr><td colspan="4"> <h3> Total Kebutuhan SDMK = JKT x STP </h3> </td> <td style="text-align: right;"> <b>'.number_format($kebutuhan, 2).'</b> </td></tr>';
$html .= '<tr><td colspan="4"> <h3> Pembulatan </h3> </td> <td style="text-align: right;"> <b>'.$pembulatan.'</b> </td></tr>'
.'</table>';

$html .= '<h3>Kebutuhan SDMK = <strong>'.$pembulatan.'</strong> (<i> '.$terbilang->generate($pembulatan). ' </i>) </h3>'
        . '</td></tr></table></body>';

echo $html;
?>