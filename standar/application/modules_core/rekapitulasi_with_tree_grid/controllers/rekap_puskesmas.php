<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_puskesmas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('rekapitulasi_model','rekapitulasi');
		$this->load->model('master_data/m_jenis_sdmk_model','jenis_sdmk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Rekapitulasi";
		$data['subtitle'] = "Perencanaan Kebutuhan Minimal SDMK Puskesmas";
		$this->load->view('rekap_puskesmas/form_rekap_puskesmas', $data);
	}

	public function view()
	{
		
		$data['title'] = "Rekapitulasi";
		$data['subtitle'] = "Perencanaan Kebutuhan Minimal SDMK Puskesmas";
		$this->load->view('rekap_puskesmas/view_rekap_puskesmas', $data);
	}


	public function form($id='')
	{
		
		$data['title'] = "Form Role";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->rekapitulasi->get_by_id($id);
		}
		$this->load->view('form', $data);
	}

	public function ajax_list()
	{
		$list = $this->rekapitulasi->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $role) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($role->role_name);
			$row[] = ($role->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $role->updated_date?Tanggal::formatDateTime($role->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($role->id_role,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_role('."'".Regex::_genRegex($role->id_role,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->rekapitulasi->count_all(),
						"recordsFiltered" => $this->rekapitulasi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_role = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'role_name' => Regex::_genRegex($this->input->post('role_name'), 'RGXALNUM'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => '',
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_role == 0 ){
			$this->rekapitulasi->save($dataexc);
		}else{
			$this->rekapitulasi->update(array('id_role'=>$id_role), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->rekapitulasi->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
