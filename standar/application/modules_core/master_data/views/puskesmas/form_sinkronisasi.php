<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_sinkronisasi" action="<?php echo site_url('master_data/m_puskesmas/proses_sinkronisasi')?>">

              <div class="col-sm-12">
                <div class="widget-box">
                  <div class="widget-header widget-header-flat">
                    <h4 class="widget-title smaller">
                      <i class="ace-icon fa fa-quote-left smaller-80"></i>
                      Sinkronisasi Data Puskesmas
                    </h4>
                  </div>

                  <div class="widget-body">
                    <div class="widget-main">
                      <address>
                        <strong>Keterangan</strong>

                        <br />
                        1. Pastikan koneksi internet anda stabil sebelum melakukan proses sinkronisasi data.
                        <br />
                        2. Pastikan user tidak ada yang melakukan transaksi selama proses sinkronisasi berlangsung.
                        <br />
                        2. Sumber data yaitu diambil dari aplikasi Sys Online.
                      </address>

                      <div class="form-group">
                        <label class="control-label col-md-2">Tanggal</label>
                        <div class="col-md-2">
                          <input name="id" id="id" value="<?php echo date('d-m-Y H:i:s')?>" placeholder="Auto" class="form-control" type="text" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2">Oleh</label>
                        <div class="col-md-4">
                          <input name="nama_rs" id="nama_rs" value="<?php echo $this->session->userdata('data_user')->fullname?>" placeholder="Nama Rumah Sakit" class="form-control" type="text" readonly>
                        </div>
                      </div>

                       <div class="form-actions center">

                        <a onclick="getMenu('master_data/m_puskesmas')" href="#" class="btn btn-sm btn-success">
                          <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                          Kembali ke daftar
                        </a>
                        <!-- <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                          <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                          Tambah baru
                        </a>
                        <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                          <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                          Reset
                        </button> -->
                        <button type="submit" onclick="proses_sinkronisasi()" id="btnSave" name="submit" class="btn btn-sm btn-info">
                          <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                          Proses Sinkronisasi
                        </button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

                


                <!-- <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div> -->

               
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/puskesmas.js'?>"></script>
