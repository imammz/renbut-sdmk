<?php
    if(count($result) > 0){
        $filename = "expexcel_".Date("Y-m-d").".xls";
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=$filename");
        header('Cache-Control: public');
    }    
?>

<html>
  <meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
    <body>

      <div class="row">
        <div class="col-xs-12">

          <!-- PAGE CONTENT -->

          HASIL LAPORAN PERENCANAAN KEBUTUHAN SDM KESEHATAN<br>
          <!-- <?php echo ( count($value) != 0 ) ? strtoupper('Puskesmas '.$value->nama_puskesmas_kab.' Kategori '.$value->nama_tipe_puskesmas.' Kawasan '.$value->nama_tipe_kawasan.'<br> Tahun '.$value->tahun.'') : '';?> -->
          <br>

          <?php if(count($result) > 0) { ?>

          <table border="1" style="font-size:12px; ">
              <thead>
                <tr style="background-color: #0B610B; color: white">
                  <th rowspan="2" class="center">&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;</th>
                  <th class="center" rowspan="2">Nama Faskes</th>
                  <?php
                    if($id_jenis_faskes == 1){
                        $jenis_sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_jenis_faskes'=>$id_jenis_faskes,'id_tipe_puskesmas'=>isset($result->id_tipe_puskesmas)?$result->id_tipe_puskesmas:'', 'id_tipe_kawasan'=>isset($result->id_tipe_kawasan)?$result->id_tipe_kawasan:'','tahun'=>isset($posted['t_perencanaan.tahun'])?$posted['t_perencanaan.tahun']:''));
                    }else{
                        $jenis_sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_jenis_faskes'=>$id_jenis_faskes,'tahun'=>isset($posted['t_perencanaan.tahun'])?$posted['t_perencanaan.tahun']:'', 'kategori_kelas'=>isset($result->kategori_kelas)?$result->kategori_kelas:''));
                    }

                    /*echo '<pre>'; print_r($jenis_sdmk);die;
                    print_r($this->db->last_query());die;*/

                    $no=1;
                    foreach ($jenis_sdmk as $key => $value) {
                  ?>
                  <th align="center" colspan="3"><?php echo ''.$no.'. '.$value->nama_jenis_sdmk.'';?></th>
                    
                    <?php $alp='a'; foreach($value->sub_parent as $key2=>$value2){?>
                      <th align="center" colspan="3"><?php echo ''.$no.'.'.$alp.' '.$value2->nama_jenis_sdmk.'';?></th>
                    <?php $alp++; }?>
                  
                  <?php $no++; }?>
                </tr>
                <tr style="background-color: #0B610B; color: white">
                  <?php

                    $jenis_sdmk2 = Master::get_jenis_sdmk_data(isset($id_jenis_faskes)?$id_jenis_faskes:'');
                    $no=1;
                    foreach ($jenis_sdmk2 as $key2 => $value2) {
                  ?>
                  <th align="center">Jumlah SDMK Saat Ini</th>
                  <th align="center">Standar SDMK</th>
                  <th align="center">Kesenjangan</th>
                  <?php $no++; }?>
                </tr>
              </thead>

              <tbody>
                
                <tr style="background-color: #3ADF00; color: black">
                  <td align="center"><b>[ 1 ]</b></td>
                  <td align="center"><b>[ 2 ]</b></td>
                  <?php
                    $count = 3;
                    for ($i=$count; $i < $no*3; $i++) {
                  ?>
                  <td align="center"><b>[ <?php echo $i?> ]</b></td>
                <?php } ?>
                </tr>

                <?php
                  $count_puskes = 1; 
                  //$puskesmas = Master::get_puskesmas_data(); //print_r($puskesmas);die;
                  $params_array = array();
                  $params_array['t_perencanaan.tahun'] = $posted['t_perencanaan.tahun'];
                  $params_array['t_perencanaan.id_jenis_faskes'] = $id_jenis_faskes;

                  if( $id_jenis_faskes == 1 ){
                      if( $result->id_tipe_kawasan != 0){
                        $params_array['t_perencanaan.id_tipe_kawasan'] = $result->id_tipe_kawasan;
                      }

                      if( $result->id_tipe_puskesmas != 0){
                        $params_array['t_perencanaan.id_tipe_puskesmas'] = $result->id_tipe_puskesmas;
                      }

                  }

                  if( in_array($id_jenis_faskes, array('3','29','30','31')) ){
                    if( $result->kategori_kelas != ''){
                        $params_array['kategori_kelas'] = $result->kategori_kelas;
                      }
                  }

                  if( isset($posted['id_provinsi']) ){
                    $params_array['t_perencanaan.id_provinsi'] = $result->id_provinsi;
                  }

                  if( isset($posted['id_kabupaten']) ){
                    $params_array['t_perencanaan.id_kabupaten'] = $result->id_kabupaten;
                  }

                  //print_r($params_array);die;
                  $rekapitulasi = $this->rekapitulasi->get_rekapitulasi($params_array); 
                  //echo '<pre>';print_r($rekapitulasi);die;
                ?>

                <?php foreach ($rekapitulasi as $key3 => $value3) {?>

                <tr style="background-color: #0b57a1; color: white ">
                  <td align="center"><?php echo $count_puskes?></td>
                  <td colspan="<?php echo $i-2?>"><?php echo '<strong>PROVINSI '.strtoupper($value3->nama_provinsi).'</strong>'?></td>
                </tr>

                <?php foreach ($value3->kabupaten as $key4 => $value4) {?>

                <tr style="background-color: #6fb3e0; color: white ">
                  <td align="center">&nbsp;</td>
                  <td colspan="<?php echo $i-2?>"> <?php echo '<strong>'.$value4->nama_kabupaten.'</strong>'?></td>
                </tr>

                <?php foreach ($value4->puskesmas as $key5 => $value5) {?>
                
                <!-- <tr>
                  <td class="center">&nbsp;</td>
                  <td><?php echo $value4->nama_puskesmas_kab?></td>
                </tr> -->

                <tr>
                  <td align="center">&nbsp;</td>
                  <td><?php echo $value5->nama_puskesmas_kab?></td>
                  <?php
                    foreach ($value5->sdmk as $key_row_rekap=>$row_rekap_sdmk) {
                      if( in_array($row_rekap_sdmk->id_jenis_sdmk, array('10420','10410','212244') )){ // perawat pustu
                        $jml_standar = $row_rekap_sdmk->total_standar;
                      }else{
                        $jml_standar = $row_rekap_sdmk->jml_standar;
                      }

                      /*$selisih_main = $row_rekap_sdmk->jml_standar - $row_rekap_sdmk->jml_pns;*/
                      $selisih_main = $row_rekap_sdmk->total_jml - $jml_standar;
                      $p_saat_ini[$key_row_rekap][] = $row_rekap_sdmk->total_jml;
                      $p_standar[$key_row_rekap][] = $jml_standar;
                      $p_selisih[$key_row_rekap][] = $selisih_main;
                  ?>
                  <td align="center"><b> <?php echo ($row_rekap_sdmk->total_jml!='')?$row_rekap_sdmk->total_jml:0?></td>
                  <td align="center"><b><?php echo $jml_standar?$jml_standar:0?></td>
                  <td align="center"><b><?php echo Apps::get_format($selisih_main?$selisih_main:0)?></td>

                  <?php
                    foreach ($row_rekap_sdmk->sub_parent as $key_sub_rekap_row=>$sub_row_rekap_sdmk) {
                      $selisih = $sub_row_rekap_sdmk->jml_standar - $sub_row_rekap_sdmk->jml_pns;
                      $sub_saat_ini[$key_sub_rekap_row][] = $sub_row_rekap_sdmk->jml_pns;
                      $sub_standar[$key_sub_rekap_row][] = ($sub_row_rekap_sdmk->jml_standar)?$sub_row_rekap_sdmk->jml_standar:$sub_row_rekap_sdmk->total_standar;
                      $sub_selisih[$key_sub_rekap_row][] = $selisih;
                  ?>

                  <!-- <td class="center"><b><?php echo $sub_row_rekap_sdmk->jml_pns?></td>
                  <td class="center"><b><?php echo $sub_row_rekap_sdmk->jml_standar?></td>
                  <td class="center"><b><?php echo $selisih?></td> -->

                  <td align="center">-</td>
                  <td align="center">-</td>
                  <td align="center">-</td>
                <?php } ?>

                <?php } ?> 
                </tr>

                <?php }?>

                <?php }?>

                <tr>
                  <td colspan="2" align="center">TOTAL</td>
                  <?php
                    foreach ($value5->sdmk as $key_row_rekap=>$row_rekap_sdmk) {
                  ?>
                  <td align="center"><?php echo array_sum($p_saat_ini[$key_row_rekap])?></td>
                  <td align="center"><?php echo array_sum($p_standar[$key_row_rekap])?></td>
                  <td align="center"><?php echo array_sum($p_selisih[$key_row_rekap])?></td>

                  <?php
                    foreach ($row_rekap_sdmk->sub_parent as $sub_key=>$sub_row_rekap_sdmk) {
                  ?>
                  <td align="center"><?php echo array_sum($sub_saat_ini[$sub_key])?></td>
                  <td align="center"><?php echo array_sum($sub_standar[$sub_key])?></td>
                  <td align="center"><?php echo array_sum($sub_selisih[$sub_key])?></td>


                <?php 
                  } 
                ?>

                <?php 
                  }
                ?> 
                
                </tr>

                <?php $count_puskes++; }?>

              </tbody>
          </table>  
          <?php }else{ echo "<script>alert('Tidak ada data!');</script>"; }?>
          <!-- PAGE CONTENT ENDS -->
        
        </div><!-- /.col -->
      </div><!-- /.row -->

    </body>
</html>
