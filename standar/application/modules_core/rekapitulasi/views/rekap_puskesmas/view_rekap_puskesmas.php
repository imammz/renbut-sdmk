<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT -->

    <center><h2>Hasil Rekapitulasi Perencanaan Kebutuhan Minimal SDM Kesehatan Puskesmas</h2></center>
        <br>

    <button class="btn btn-sm btn-info" onclick="getMenu('rekapitulasi/rekap_puskesmas')"><i class="fa fa-arrow-left"></i> Kembali ke rekapitulasi </button><div class="pull-right tableTools-container"></div>
  

    <table id="example" class="table table-striped table-bordered table-hover" style="font-size:12px">
      <thead>
        <tr>
          <th rowspan="2" class="center">&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;</th>
          <th class="center" rowspan="2">Nama Puskesmas</th>
          <?php
            $jenis_sdmk = Master::get_jenis_sdmk_data(1);
            foreach ($jenis_sdmk as $key => $value) {
          ?>
          <th class="center" colspan="3"><?php echo $value['nama_jenis_sdmk']?></th>
          <?php }?>
        </tr>
        <tr>
          <?php
            $jenis_sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_jenis_faskes'=>1,'id_tipe_puskesmas'=>$value->id_tipe_puskesmas, 'id_tipe_kawasan'=>$value->id_tipe_kawasan)); //echo '<pre>';print_r($jenis_sdmk);die;

            //$jenis_sdmk = Master::get_jenis_sdmk_data(1);
            $no=1;
            foreach ($jenis_sdmk as $key2 => $value2) {
          ?>
          <th class="center">Jumlah SDMK Saat Ini</th>
          <th class="center">Standar SDMK</th>
          <th class="center">Kesenjangan</th>
          <?php $no++; }?>
        </tr>
      </thead>

      <tbody>
        
        <tr class="center">
          <td><b>[ 1 ]</b></td>
          <td><b>[ 2 ]</b></td>
          <?php
            $count = 3;
            for ($i=$count; $i < $no*3; $i++) {
          ?>
          <td><b>[ <?php echo $i?> ]</b></td>
        <?php } ?>
        </tr>
        <?php
          $count_puskes = 1; 
          $puskesmas = Master::get_puskesmas_data(); //print_r($puskesmas);die;
          foreach ($puskesmas as $key3 => $value3) {
        ?>
        <tr>
          <td class="center"><?php echo $count_puskes?></td>
          <td><?php echo $value3['nama_puskesmas_kab']?></td>
          <?php
            $count = 3;
            for ($i=$count; $i < $no*3; $i++) {
          ?>
          <td class="center"><b>0</td>
        <?php } ?>
        </tr>
        <?php $count_puskes++; }?>


        <tr>
          <td colspan="2" class="center"><b>TOTAL</b></td>
          <?php
            $count = 3;
            for ($i=$count; $i < $no*3; $i++) {
          ?>
          <td>0</td>
        <?php } ?>
        </tr>

      </tbody>
    </table>  
    <!-- PAGE CONTENT ENDS -->
  
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/rekapitulasi.js'?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').dataTable( {
        "scrollX": true,
        "orderable": false,
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
</script>