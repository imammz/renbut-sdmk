<h3 class="center header smaller lighter green"><?php echo $tab_title?></h3>
<form class="form-horizontal" method="post" id="form_jml_standar_sdmk" action="<?php echo site_url('master_data/m_jenis_sdmk/ajax_add')?>">
  <table id="dynamic-table" rel="rsu" class="table table-striped table-bordered table-hover">
     <thead>
      <tr>  
        <th class="center" style="width: 50px"></th>
        <th class="center" style="width: 120px">ID</th>
        <th width="300px">Nama Jenis SDMK</th>
        <th>Kelas A</th>
        <th>Kelas B</th>
        <th>Kelas C</th>
        <th>Kelas D</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="center">( 1 )</td>
        <td class="center">( 2 )</td>
        <td class="center">( 3 )</td>
        <td class="center">( 4 )</td>
        <td class="center">( 5 )</td>
        <td class="center">( 6 )</td>
      </tr>
    </tbody>
  </table>

</form>
<script src="<?php echo base_url().'assets/js/custom/standar_sdmk.js'?>"></script>