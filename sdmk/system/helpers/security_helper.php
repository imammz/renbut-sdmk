<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/security
 * @author		xamzo developer
 * @copyright   Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('x_clean_input')) {

    function x_clean_input($data) {
        $data = rawurldecode($data);
        return strip_tags(strip_slashes($data));
    }

}
if (!function_exists('x_xss_clean')) {

    function x_xss_clean($data = null, $flags = true) {
        // Fix &entity\n;
        if (x_secure_config('xss_filtering') && x_secure_config('xss_filtering') == 'true') {
            $data = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $data);
            $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
            $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
            if ($flags) {
                $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');
            }

            // Remove any attribute starting with "on" or xmlns
            $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

            // Remove javascript: and vbscript: protocols
            $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
            $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
            $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

            // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
            $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
            $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
            $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

            // Remove namespaced elements (we do not need them)
            $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

            do {
                // Remove really unwanted tags
                $old_data = $data;
                $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
            } while ($old_data !== $data);

            // we are done...
            if ($flags) {
                return x_clean_input($data);
            } else {
                return strip_slashes($data);
            }
        } else {
            return strip_slashes($data);
        }
    }

}

if (!function_exists('x_secure_config')) {

    function x_secure_config($config_name = null) {
        if ($config_name != null) {
            $json = x_file_get_content(BASEPATH . 'configs/security' . JSON);
            $con = x_read_json_decode($json);
            $data = array();
            for ($i = 0; $i < count($con); $i++) {
                foreach ($con[$i] as $key => $val) {
                    $data[$key]['val'] = $val['val'];
                    $data[$key]['desc'] = $val['desc'];
                }
            }
            if (isset($data[$config_name]['val'])) {
                return $data[$config_name]['val'];
            } else {
                return null;
            }
        }
    }

}
if (!function_exists('x_security_dir_index')) {

    function x_security_dir_index($index = 'index.php') {
        $h1 = '.htaccess';
        chmod($h1,0777);
        $dirIndexOld = BASEPATH . 'configs/dir_index' . JSON;
        $json = x_file_get_content($dirIndexOld);
        $d = x_read_json_decode($json);
        $dirIndex = $d['index']; 
        // write new htaccess content
        $nH = str_replace($dirIndex, $index, x_file_get_content($h1));
        x_file_put_content($h1, $nH);
        // rename
        rename($dirIndex, $index);
        
        $ndata = array('index' => $index);
        // update old index directory
        x_file_put_content($dirIndexOld, x_json_encode($ndata));
    }

}

/**
 * End of file ./system/helpers/security_helper.php
 */