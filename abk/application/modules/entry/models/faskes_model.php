<?php

class faskes_model extends CI_Model {
    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();
    }
    
 
    public function _loadRumahSakit($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 2;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
              
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    public function _loadMasterRumahSakit($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_rumahsakit->order('no_prov ASC, no_kab ASC, kode_rs ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('kode_rs',$faskes_kode);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
                $row_faskes['NO_KLAS_KAB'] = $kab['NO_KLAS_KAB'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
    
    
    
      public function _loadPuskesmas($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 1;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0; 
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode']);
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    public function _loadMasterPuskesmas($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_puskesmas->order('no_prov ASC, no_kab ASC, Kode_puskesmas ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('Kode_puskesmas',$faskes_kode);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
                $row_faskes['NO_KLAS_KAB'] = $kab['NO_KLAS_KAB'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
    
    
    public function _loadDinaskesehatan($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 6;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    public function _loadDinaskesehatanProv($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 7;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            

            
            $query_kabupaten = array('NO_PROV'=>$row_prov['NO_PROV']);
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_prov['NO_PROV'];
                $kab['NO_PROV'] = $row_prov['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_prov['NO_PROV'];
                $kab['NAMA_KAB'] = 'Kebutuhan Nakes';
                $kab['faskes'] = array();
                

                 if($NO_PROV=='A') {
                    $query_faskes = $this->orm->faskes->where('ref_jenis_faskes_kode = ?',$ref_jenis_faskes_kode); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('ref_jenis_faskes_kode = ? AND faskes_kode = ?',$ref_jenis_faskes_kode,$faskes_kode);
                }
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
    public function _loadMasterDinasKesehatan($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_kabupaten->order('NO_PROV ASC, NO_KAB ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('NO_KAB',$faskes_kode);
         }
         
         if($NO_PROV!='A') {
             $query->and('NO_PROV',$NO_PROV);
         }
         
         if($NO_KAB!='A') {
             $query->and('NO_KAB',$NO_KAB);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $row_faskes['NO_PROV'])->fetch();
                
                $row_faskes['NO_KLAS_KAB'] = $row_faskes['NO_KLAS_KAB'];
                $row_faskes['NAMA_KAB'] = $row_faskes['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 
	
	public function _loadMasterKab($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_kabupaten->order('NO_PROV ASC, NO_KAB ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('NO_KAB',$faskes_kode);
         }
         
         if($NO_PROV!='A') {
             $query->and('NO_PROV',$NO_PROV);
         }
         
         if($NO_KAB!='A') {
             $query->and('NO_KAB',$NO_KAB);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $row_faskes['NO_PROV'])->fetch();
                
                $row_faskes['NO_KLAS_KAB'] = $row_faskes['NO_KLAS_KAB'];
                $row_faskes['NAMA_KAB'] = $row_faskes['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	public function _loadMasterBalaiKesehatan($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_bapelkes->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadbalaikesehatan($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 5;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
    public function _loadMasterbpkm($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_bpkm->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadbpkm($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 8;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
    public function _loadMasterKlinik($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_klinik->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadKlinik($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 9;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
	
    
    public function _loadMasterbtkl($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
       $query = $this->orm->ref_btklp->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadbtkl($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 3;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
	
	public function _loadMasterkkp($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
            
        $this->orm->debug = false;    
        $query = $this->orm->ref_kkp->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadkkp($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 4;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    public function _loadMasterlabkes($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $this->orm->debug = true;
        
       $query = $this->orm->ref_labkes->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadlabkes($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 10;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
	
	public function _loadMasterlainnya($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_faskeslain->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
          if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadlainnya($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 99;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
    public function _loadMasterDinasKesehatanProv($NO_PROV = null ) {
     
        $query = $this->orm->ref_provinsi->order('NO_PROV ASC');
         
         if($NO_PROV!='A') {
             $query->and('NO_PROV',$NO_PROV);
         }
        
       
        return $query;
    }
    
    
      public function _loadMasterKabupaten($NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_kabupaten->order('NO_PROV ASC, NO_KAB ASC');
        
        
         if($NO_PROV!='A') {
             $query->and('NO_PROV',$NO_PROV);
         }
         
         if($NO_KAB!='A') {
             $query->and('NO_KAB',$NO_KAB);
         }
        
        $data = array();
        foreach($query as $row_kab) {
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $row_kab['NO_PROV'])->fetch();
                
                $row_kab['NAMA_PROV'] = $prov['NAMA_PROV'];                
                $data[] = $row_kab;
        }
        
        
        return $data;
    }
    
    public function _loadMasterUpt($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_upt->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
         
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadUpt($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 98;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
    
    
    
    public function _loadMasterUptdkab($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_uptdkab->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadUptdkab($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 96;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
     public function _loadMasterUptdprov($faskes_kode = null, $NO_KAB = null, $NO_PROV = null ) {
     
        $query = $this->orm->ref_uptdprov->order('no_kab ASC, faskes_kode ASC');
        
        
         if($faskes_kode!='A') { 
             $query->and('faskes_kode',$faskes_kode);
         }
         
       
         
         if($NO_KAB!='A') {
             $query->and('no_kab',$NO_KAB);
         }
         
         if($NO_PROV!='A') {
             $query->and('no_prov',$NO_PROV);
         }
        
        $data = array();
        foreach($query as $row_faskes) {
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $row_faskes['no_kab'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
				$row_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                $row_faskes['NAMA_KAB'] = $kab['NAMA_KAB'];
                $row_faskes['NAMA_PROV'] = $prov['NAMA_PROV'];
                
                $data[] = $row_faskes;
        }
        
        
        return $data;
    }
	
	 public function _loadUptdprov($tahun_entry = null,$faskes_kode = null, $NO_KAB = null, $NO_PROV = null) {
        
        /****  
         *  Filter Berdasarkan faskes_kode, NO_PROV, NO_KAB 
         * 
         */
        
        if($tahun_entry==null) {
            $tahun_entry = date('Y');
        }

        
        $ref_jenis_faskes_kode = 97;
        
        $data = array();
        $data['provinsi'] = array();
        
        if($NO_PROV=='A') {
        $query_provinsi = $this->orm->ref_provinsi();
        }
        else {
            $query_provinsi = $this->orm->ref_provinsi->where('NO_PROV',$NO_PROV);
        }
        
        foreach($query_provinsi as $row_prov) {
            $prov = array();
            $prov['NO_PROV'] = $row_prov['NO_PROV'];
            $prov['NAMA_PROV'] = $row_prov['NAMA_PROV'];
            $prov['kabupaten'] = array();
            
            
            if($NO_KAB=='A') {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV',$row_prov['NO_PROV']);
            }
            else {
                $query_kabupaten = $this->orm->ref_kabupaten->where('NO_PROV = ? AND NO_KAB = ?',$row_prov['NO_PROV'],$NO_KAB);
            }
            
            
            $prov_jumlah_saat_ini = 0;
            $prov_hasil_kebutuhan_sdmk = 0;
            foreach($query_kabupaten as $row_kab) {
                $kab = array();
                $kab['NO_KAB'] = $row_kab['NO_KAB'];
                $kab['NO_PROV'] = $row_kab['NO_PROV'];
                $kab['NO_KLAS_KAB'] = $row_kab['NO_KLAS_KAB'];
                $kab['NAMA_KAB'] = $row_kab['NAMA_KAB'];
                $kab['faskes'] = array();
                
                if($faskes_kode=='A') {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND tahun =?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$tahun_entry); 
                }
                else {
                    $query_faskes = $this->orm->faskes->where('NO_KAB = ? AND ref_jenis_faskes_kode = ? AND faskes_kode = ? AND tahun = ?',$row_kab['NO_KAB'],$ref_jenis_faskes_kode,$faskes_kode,$tahun_entry);
                }
                
                
                $kab_jumlah_saat_ini = 0;
                $kab_hasil_kebutuhan_sdmk = 0;
                foreach($query_faskes as $row_faskes) {
                    $faskes = array();
                    $faskes['faskes_kode'] = $row_faskes['faskes_kode'];
                    $faskes['NO_KAB'] = $row_faskes['NO_KAB'];
                    $faskes['faskes_nama'] = $row_faskes['faskes_nama'];
                    $faskes['faskes_telp'] = $row_faskes['faskes_telp'];
                    $faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
                    $faskes['faskes_email'] = $row_faskes['faskes_email'];
                    $faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
                    $faskes['ref_faskes_kelas_nama'] = $row_faskes->ref_faskes_kelas['ref_faskes_kelas_nama'];
                    $faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
                    $faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
                    $faskes['installasi'] = array();
                    
                    $query_installasi = $this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode'])->order('urut ASC');
                    $faskes_jumlah_saat_ini = 0;
                    $faskes_hasil_kebutuhan_sdmk = 0;
                    foreach($query_installasi as $row_installasi) {
                        $installasi = array();
                        $installasi['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                        $installasi['faskes_kode'] = $row_installasi['faskes_kode'];
                        $installasi['installasi_nama'] = $row_installasi['installasi_nama'];
                        $installasi['eselon'] = $row_installasi['eselon'];
                        $installasi['parent'] = $row_installasi['parent'];
                        $installasi['urut'] = $row_installasi['urut'];
                        $installasi['sdmk'] = array();
                        
                        $query_sdmk = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id',$row_installasi['faskes_installasi_id']);
                        $installasi_jumlah_saat_ini = 0;
                        $installasi_hasil_kebutuhan_sdmk = 0;
                        foreach($query_sdmk as $row_sdmk) {
                            $sdmk = array();
                            $sdmk['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                            $sdmk['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                            $sdmk['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                            $sdmk['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                            $sdmk['jumlah_saat_ini'] = $row_sdmk['jumlah_PNS'];
                            $sdmk['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];
                            
                            $installasi_hasil_kebutuhan_sdmk += $sdmk['hasil_kebutuhan_sdmk'];
                            $installasi_jumlah_saat_ini += $sdmk['jumlah_saat_ini'];
                            $installasi['sdmk'][] = $sdmk;
                        }
                        $installasi['jumlah_saat_ini'] = $installasi_jumlah_saat_ini;
                        $installasi['hasil_kebutuhan_sdmk'] = $installasi_hasil_kebutuhan_sdmk;
                       $faskes['installasi'][] = $installasi; 
                        
                       $faskes_jumlah_saat_ini += $installasi['jumlah_saat_ini'];
                       $faskes_hasil_kebutuhan_sdmk += $installasi['hasil_kebutuhan_sdmk'];
                    }    
                    $faskes['jumlah_saat_ini'] = $faskes_jumlah_saat_ini;  
                    $faskes['hasil_kebutuhan_sdmk'] = $faskes_hasil_kebutuhan_sdmk;  
                    $kab['faskes'][] = $faskes;
                    
                    $kab_jumlah_saat_ini += $faskes['jumlah_saat_ini'];
                    $kab_hasil_kebutuhan_sdmk += $faskes['hasil_kebutuhan_sdmk'];
                }
                
                $kab['jumlah_saat_ini'] = $kab_jumlah_saat_ini;  
                $kab['hasil_kebutuhan_sdmk'] = $kab_hasil_kebutuhan_sdmk;
                $prov['kabupaten'][] = $kab;
                
                 $prov_jumlah_saat_ini += $kab['jumlah_saat_ini'];
                 $prov_hasil_kebutuhan_sdmk += $kab['hasil_kebutuhan_sdmk'];
            }
            
            $prov['jumlah_saat_ini'] = $prov_jumlah_saat_ini;  
            $prov['hasil_kebutuhan_sdmk'] = $prov_hasil_kebutuhan_sdmk;
            $data['provinsi'][] = $prov;
            
        }
        
        return $data;
        
        
    }
    
    
    
    
    
    
    
    public function clearKBKFaskes($faskes_kode,$tahun) {
    
        $faskes = $this->orm->faskes->where('faskes_kode = ? AND tahun = ?', $faskes_kode,$tahun);
        $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $faskes_kode);
        $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $faskes_installasi);
        $sdmk_kbk = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi);

        $sdmk_kbk->delete();
        $sdmk_faskes_installasi->delete();
        $faskes_installasi->delete();
        $faskes->delete();
    }
    
    public function _loadMasterProvinsi($NO_PROV = null ) {
     
        $query = $this->orm->ref_provinsi->order('NO_PROV ASC');
        
        
         if($NO_PROV!='A') {
             $query->and('NO_PROV',$NO_PROV);
         }
         
        
        $data = array();
        foreach($query as $row_kab) {
                      
                $data[] = $row_kab;
        }
        
        
        return $data;
    }
    
    
    
    
    
}
?>