<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<div class="tab" tab-parent=".email-template" tab-animation="bounce">
    <a class="active" tab-target="#code"><i class="fa fa-code"></i> code</a>
    <a class="" tab-target="#preview"><i class="fa fa-eye-open"></i> preview</a> 
    <button class="right btn primary fa fa-save tip-top-right" style="margin-top: -5px;" accesskey="ctrl+s" ajax="true" ajax-target="false" url-push="false" ajax-type="POST" ajax-data="form#data-template" ajax-url="<?php echo site_url('email/save_editor') ?>" original-title="Simpan (Ctrl+S)"></button> 
</div>
<form id="data-template" style="display: none;">
    <input type="hidden" name="file" value="<?php echo $file_dir ?>">
    <textarea name="content"><?php echo $content ?></textarea>
</form>
<div class="tab-target email-template">
    <div class="child-tab" id="code">
        <pre contenteditable="true" style="width: 98%;" onkeyup="xamzo_mail_template(this);"><?php echo htmlentities($content) ?></pre>
    </div>
    <div class="child-tab" id="preview">
        <?php echo $content ?>
    </div>
</div>
<script type="text/javascript">
    function xamzo_mail_template(i){
        var template = $(i).text();
        $('#preview').html(template);
        $('form#data-template textarea[name="content"]').val(template)
    }
</script>