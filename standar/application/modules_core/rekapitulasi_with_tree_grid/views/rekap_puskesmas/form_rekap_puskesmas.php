<script type="text/javascript">
   
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#puskes-box'));
                    });

                });
            } else {
                $('#puskes-box option').remove()
                $('<option value="">(Pilih Unit Kerja)</option>').appendTo($('#puskes-box'));
            }
        });

        $('select[name="kode_puskesmas"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_puskesmas/get_puskes_by_kode') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-result').html(data.string);
                   
                });
            } else {
                $('#puskes-result').html();
            }
        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_rekapitulasi_puskesmas" action="<?php echo site_url('rekapitulasi/ajax_add')?>">
                <br>


                <div class="form-group">
                  <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value)?$value->tahun:'','rekapitulasi','rekapitulasi','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','5')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($value) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('5')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_kabupaten(isset($value) ? $value->id_kabupaten : $this->session->userdata('data_user')->id_kabupaten ,'id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                <div class="form-group" id="form-puskesmas" >
                  <label class="control-label col-md-2">Puskesmas</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('0')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_puskesmas(isset($value) ? $value->kode_puskesmas : $this->session->userdata('data_user')->kode_puskesmas,'kode_puskesmas','puskes-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                  
                </div>
                <?php }?>

                <div id="puskes-result"></div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4'))){?>
                <div class="form-group">
                  <label class="control-label col-md-2"></label>
                  <div class="col-md-9">

                  <span class="help-inline col-xs-12 col-sm-7">
                        <label class="middle">
                          <input class="ace" type="checkbox" id="id-disable-check" />
                          <span class="lbl"> Checklist untuk semua puskesmas</span>
                        </label>
                      </span>
                    
                  </div>
                </div>
                <?php }?>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kawasan</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <label>
                            <input name="active" type="radio" class="ace" value="0" <?php echo isset($value) ? ($value->active == '0') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Semua Kategori </span>
                          </label>

                        <?php
                          $data = Master::get_tipe_kawasan_data();
                          foreach($data as $row){
                        ?>
                          <label>
                            <input name="active" type="radio" class="ace" value="<?php echo $row['id_tipe_kawasan']?>" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo $row['nama_tipe_kawasan']?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_rekapitulasi:0?>"> -->

                  <a onclick="getMenu('rekapitulasi')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <!-- <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a> -->
                  <!-- <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button> -->
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/rekap_puskesmas.js'?>"></script>
