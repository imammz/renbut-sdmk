<?php
 define('LOGIN_CHECK', '_config/e-account/login/checklogin/');

 $message = '';
 if(isset($_GET['m'])) {
 switch ($_GET['m']) {
     case 'failed' :
         $message = "Anda Belum Login..!";
         break;
     case 'wrong' :
         $message = "Username Atau Password Anda Salah..!";
         break;
     case 'out' :
         $message = "Anda Berhasil Logout..!";
         break;
     default :
         $message = '';
         break;
 }
 }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Portal Aplikasi Perencanaan Kebutuhan SDMK</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/style-responsive.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/open-sans.css">
        <link rel="stylesheet" href="css/dialog.css">
        <link rel="stylesheet" href="css/dialog-sally.css">
        <link rel="icon" href="images/icon.png">
        <!-- CHART -->
        <link rel="stylesheet" href="css/chartist.min.css">
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
    </head>
    <body>
        <?php
         if($message != '') {
         echo '<script>alert("' . $message . '");</script>';
         }
        ?>
        <!-- HEADER -->
        <section id="header">
            <div class="logo"><br><br><br><br><br><br></div>
            <div class="header-title-2"><!-- DEWAN KEHORMATAN PENYELENGGARA PEMILU  --></div>
            <hr class="header-garis" />
            <div class="header-title-2"><!-- SIPEPP --></div>
            <p class="header-text-gold"><!-- Sistem Informasi Peradilan Etika Penyelenggara Pemilu --></p>
            <div class="header-menu">
                <!-- <button class="button-merah" onclick="return goToWeb();">Website DKPP RI</button> 
                <span class="header-text-margin">atau</span> 
                <button class="button-biru trigger"  data-dialog="login" >Login</button> -->
            </div>
        </section>
        <!-- END HEADER -->

        <!-- MENU -->
        <section id="menu">
            <div class="container">
                <div class="row">
                	<h3>PORTAL APLIKASI PERENCANAAN KEBUTUHAN SDMK</h3>
                	<hr>
                	<br>
                    <div class="col-md-4">
                        <i class="fa fa-hospital-o"></i>
                        <h4>(Metode ABK)</h4>
                        <div class="menu-line"></div>
                        <div class="menu-button">
                            <a href="http://pusrengun.info/abk" target="blank" class="button-merah">Selengkapnya</a>
                        </div>
                        <p style="text-align:justify">ABK merupakan metode untuk mengetahui kebutuhan riil pegawai (jenis dan jumlah) di suatu unit organisasi yang dilakukan secara sistematis untuk menjalankan fungsi suatu organisasi, diperolehnya kebutuhan riil yang dilakukan dengan cara merinci seluruh kegiatan/aktivitas yang dilakukan dalam suatu unit kerja atau per jenis kategori SDM (jabatan)</p>

                    </div>
                    <div class="col-md-4">
                        <i class="fa fa-hospital-o"></i>
                        <h4>(Metode Standar)</h4>
                        <div class="menu-line"></div>
                        <div class="menu-button">
                            <a href="http://pusrengun.info/standar" target="blank" class="button-merah">Selengkapnya</a>
                        </div>
                        <p style="text-align:justify">Merupakan metode yang biasa digunakan untuk mengetahui kebutuhan jumlah SDM dengan kategori tertentu sesuai dengan perencanaan kebutuhan.</p>

                    </div>
                    <div class="col-md-4">
                        <i class="fa fa-hospital-o"></i>
                        <h4>(Metode Proyeksi)</h4>
                        <div class="menu-line"></div>
                        <div class="menu-button">
                            <a href="http://pusrengun.info/sdmk" target="blank" class="button-merah">Selengkapnya</a>
                        </div>
                        <p style="text-align:justify">Proyeksi adalah perkiraan mengenai jumlah dan jenis karyawan yang dapat diharapkan merupakan tenaga kerja organisasi pada suatu waktu tertentu yang akan datang. Proyeksi didasarkan atas suatu perkiraan mengenai persediaan SDM yang sudah ada sekarang secara cermat,ditambah dengan pertimbangan mengenai gerakan karyawan melalui organisasi yang bersangkutan sepanjang waktu</p>

                    </div>

                </div>
                <img src="http://localhost/pusrengun/images/aiphss.png" style="padding-left:1000px;padding-top:20px">
                <!-- <div class="row">
                    <div class="col-md-12 text-center menu-bawah">
                        <button class="button-option" onclick="return goToEAkun();"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;E-Akun</button>
                        <button class="button-option" onclick="return goToMasterData();">Master Data</button>
                        <button class="button-option" onclick="return goToESetting();"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;E-Setting</button>
                    </div>
                </div> -->
            </div>
        </section>
        <!-- END MENU -->

        <!-- e-Pengaduan -->
        <!-- <section id="e-pengaduan" class="table-and-graph">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <h1 class="color-red">e-Pengaduan</h1>
                        <p>Jumlah Pengaduan Pelanggaran Kode Etik Penyelenggara Pemilu <br> Diterima Sekertariat Biro DKPP Tahun 2012 s.d. 2014 </p>
                        <div class="garis-title"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="text-right">
                            <button class="button-go-app" onclick="return goToEPengaduan();">Masuk Apllikasi&nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <table class="table-data">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
				    <th>Bulan</th>
                                    <th>2012</th>
				    <th>2013</th>
				    <th>2014</th>
                                    <th class="text-center">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Januari</td>
                                    <td>-</td>
				    <td>38</td>
				    <td>67</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Pebruari</td>
                                    <td>-</td>
				    <td>26</td>
				    <td>22</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Maret</td>
                                    <td>-</td>
				    <td>20</td>
				    <td>33</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>April</td>
                                    <td>-</td>
				    <td>18</td>
				    <td>66</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>Mei</td>
                                    <td>-</td>
				    <td>56</td>
				    <td>345</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td>6</td>
                                    <td>Juni</td>
                                    <td>3</td>
				    <td>57</td>
				    <td>136</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td>7</td>
                                    <td>Juli</td>
                                    <td>12</td>
				    <td>59</td>
				    <td>101</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td>8</td>
                                    <td>Agustus</td>
                                    <td>7</td>
				    <td>99</td>
				    <td>34</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td>9</td>
                                    <td>September</td>
                                    <td>21</td>
				    <td>71</td>
				    <td>40</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td>10</td>
                                    <td>Oktober</td>
                                    <td>13</td>
				    <td>62</td>
				    <td>20</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td>11</td>
                                    <td>November</td>
                                    <td>24</td>
				    <td>62</td>
				    <td>20</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td>12</td>
                                    <td>Desember</td>
                                    <td>19</td>
				    <td>9</td>
				    <td>1</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
				<tr>
                                    <td></td>
                                    <td><b>Jumlah</b></td>
                                    <td><b>99</b></td>
				    <td><b>577</b></td>
				    <td><b>855</b></td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div class="ct-chart chart-pengaduan" style="height: 350px"></div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- End e-Pengaduan -->

        <!-- e-Persidangan -->
        <!-- <section id="e-persidangan" class="table-and-graph">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <h1 class="color-red">e-Persidangan</h1>
                        <p>Sistem e-Persidangan DKPP</p>
                        <div class="garis-title"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="text-right">
                            <button class="button-go-app" onclick="return goToEPersidangan();">Masuk Apllikasi&nbsp;&nbsp;&nbsp;<i class="fa fa-long-arrow-right"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <table class="table-data">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th>Laporan</th>
                                    <th>Tanggal</th>
                                    <th class="text-center">Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Epih  Ibkar Irmansyah, Ir</td>
                                    <td>2014-05-22</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Cornell Adyas</td>
                                    <td>2014-06-02</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Mohammad Luthfie, SE</td>
                                    <td>2014-07-12</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Giat Ruchiat Jonie</td>
                                    <td>2014-08-01</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td>Mimin Karmini</td>
                                    <td>2014-09-06</td>
                                    <td class="text-center"><a href="#popupLaporan"><i class="fa fa-share-square-o"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div class="ct-chart chart-persidangan" style="height: 350px"></div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- End e-Persidangan -->

        <footer>
            &copy;copyright BPPSDM - Badan Pengembangan dan Pemberdayaan Sumber Daya Manusia Kesehatan <?php echo date('Y')?> | AIPHSS

        </footer>

        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/dialogFx.js"></script>
        <script type="text/javascript" src="js/chartist.min.js"></script>
        <script type="text/javascript" src="js/DKPP.js"></script>
    </body>
</html>