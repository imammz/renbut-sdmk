<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_rsk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('jenis_rsk_model','jenis_rsk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Jenis Rumah Sakit";
		$data['subtitle'] = "Daftar Jenis Rumah Sakit";
		$this->load->view('jenis_rsk/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Jenis Rumah Sakit";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->jenis_rsk->get_by_id($id);
		}
		/*echo '<pre>';
		print_r($data);die;*/
		$this->load->view('jenis_rsk/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->jenis_rsk->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $jenis_rsk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($jenis_rsk->nama_jenis_rsk);
			$row[] = ($jenis_rsk->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $jenis_rsk->updated_date?Tanggal::formatDateTime($jenis_rsk->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($jenis_rsk->id_jenis_rsk,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_jenis_rsk('."'".Regex::_genRegex($jenis_rsk->id_jenis_rsk,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->jenis_rsk->count_all(),
						"recordsFiltered" => $this->jenis_rsk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_jenis_rsk = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'nama_jenis_rsk' => Regex::_genRegex($this->input->post('nama_jenis_rsk'), 'RGXQSL'),
			'id_jenis_faskes' => Regex::_genRegex($this->input->post('id_jenis_faskes'), 'RGXINT'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_jenis_rsk == 0 ){
			$this->jenis_rsk->save($dataexc);
		}else{
			$this->jenis_rsk->update(array('id_jenis_rsk'=>$id_jenis_rsk), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->jenis_rsk->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
