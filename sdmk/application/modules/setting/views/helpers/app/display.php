<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}
for ($i = 0; $i < count($con); $i++) :
    foreach ($con[$i] as $key => $val):
        $type = $val['type'];
        switch ($type) {
            case 'input':
                ?>
                <p class="in-<?php echo $key ?>">
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <input type="text" <?php echo isset($val['validation']) ? "validation='$val[validation]'" : '' ?> name="<?php echo $key . '[val]' ?>" value="<?php echo $val['val'] ?>">
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">                                    
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>">
                    <input type="hidden" name="<?php echo $key . '[validation]' ?>" value="<?php echo isset($val['validation']) ? $val['validation'] : '' ?>">
                </p>
                <?php
                break;
            case 'password':
                ?>
                <p class="in-<?php echo $key ?>">
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <input type="password" <?php echo isset($val['validation']) ? "validation='$val[validation]'" : '' ?> name="<?php echo $key . '[val]' ?>" value="<?php echo $val['val'] ?>">
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">                                    
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>">
                    <input type="hidden" name="<?php echo $key . '[validation]' ?>" value="<?php echo isset($val['validation']) ? $val['validation'] : '' ?>">
                </p>
                <?php
                break;
            case 'radio':
                ?>
                <p class="in-<?php echo $key ?>">
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <br/>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <input type="radio" name="<?php echo $key . '[val]' ?>" value="true" <?php echo ($val['val'] == "true" || $val['val'] == true) ? 'checked' : '' ?> id="<?php echo $key ?>true"> <label for="<?php echo $key ?>true">True</label>
                    <input type="radio" name="<?php echo $key . '[val]' ?>" value="false" <?php echo ($val['val'] == "false" || $val['val'] == false) ? 'checked' : '' ?> id="<?php echo $key ?>false">  <label for="<?php echo $key ?>false">False</label>
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">                                   
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>">
                </p>
                <?php
                break;
            case 'textarea':
                ?>
                <p class="in-<?php echo $key ?>">
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <textarea class="xamzo-textarea" <?php echo isset($val['validation']) ? "validation='$val[validation]'" : '' ?> style="min-height: 75px;" name="<?php echo $key . '[val]' ?>"><?php echo $val['val'] ?></textarea>
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>">
                    <input type="hidden" name="<?php echo $key . '[validation]' ?>" value="<?php echo isset($val['validation']) ? $val['validation'] : '' ?>">
                </p>
                <?php
                break;
            case 'dropdown':
                ?>
                <p>
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i> 
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <span style="position: relative;">
                        <input type="text" <?php echo isset($val['validation']) ? "validation='$val[validation]'" : '' ?> name="<?php echo $key . '[val]' ?>" readonly="true" dropdown="true" dropdown-relative="left" dropdown-target="xamzo-dropdown-json-<?php echo $key ?>" value="<?php echo $val['val'] ?>">
                        <a href="javascript:void(0)" class="xamzo-dd-anggle-conf xamzo-btn-icon btn primary small fa fa-angle-down" onclick="$(this).parent().find('input').click();"></a>
                    </span>
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>">
                    <input type="hidden" name="<?php echo $key . '[option]' ?>" value="<?php echo $val['option'] ?>">
                    <input type="hidden" name="<?php echo $key . '[validation]' ?>" value="<?php echo isset($val['validation']) ? $val['validation'] : '' ?>">
                </p>
                <?php
                if (isset($val['option'])) {
                    ?>
                    <div class="t-dd-ca">
                        <ul class="xamzo-dropdown-conf dropdown-target dropdown-target-left xamzo-dropdown-json-<?php echo $key ?>"> 
                            <?php
                            $arrow = isset($val['arrow']) ? $val['arrow'] : 'top';
                            if ($arrow == 'top') {
                                echo '<div class="arrow-up"></div>';
                            } else if ($arrow == 'left') {
                                echo '<div class="arrow-left"></div>';
                            }
                            ?> 
                            <div class="type-search">
                                <input type="text" name="search" onkeyup="xamzo_search_dd(this)" placeholder="search..." style="width: 96% !important">
                            </div>
                            <div class="on-search body-dropdown">
                                <input type="hidden" <?php echo isset($val['validation']) ? "validation='$val[validation]'" : '' ?> name="<?php echo $key . '[arrow]' ?>" value="<?php echo $arrow ?>">
                                <?php
                                $json = x_file_get_content($val['option'] . JSON);
                                $data = x_read_json_decode($json);
                                foreach ($data as $k => $v) {
                                    ?>
                                    <li><a href="javascript:void(0)" onclick="set_val_dropdown('<?php echo $k ?>', 'xamzo-dropdown-json-<?php echo $key ?>');"><?php echo strtolower($v) ?></a></li>
                                    <?php
                                }
                                ?> 
                            </div> 
                        </ul>
                    </div>
                    <input type="hidden" name="<?php echo $key . '[validation]' ?>" value="<?php echo isset($val['validation']) ? $val['validation'] : '' ?>">
                    <?php
                }
                ?> 
                <?php
                break;
            case 'colorpicker':
                ?>
                <link rel="stylesheet" href="<?php echo site_url('application/templates/private/assets/javascript/colorpicker/styles/colorpicker.css'); ?>" type="text/css" />
                <script type="text/javascript" src="<?php echo site_url('application/templates/private/assets/javascript/colorpicker/javascript/colorpicker.js'); ?>"></script>
                <script type="text/javascript" src="<?php echo site_url('application/templates/private/assets/javascript/colorpicker/javascript/eye.js'); ?>"></script>
                <script type="text/javascript" src="<?php echo site_url('application/templates/private/assets/javascript/colorpicker/javascript/utils.js'); ?>"></script>
                <script type="text/javascript" src="<?php echo site_url('application/templates/private/assets/javascript/colorpicker/javascript/layout.js?ver=1.0.2'); ?>"></script>

                <p class="in-<?php echo $key ?>"> 
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <span class="cPick" style="position: relative;">                                        
                        <input type="text" readonly="true" <?php echo isset($val['validation']) ? "validation='$val[validation]'" : '' ?> class="cPicker" name="<?php echo $key . '[val]' ?>" value="<?php echo $val['val'] ?>" title="<?php echo $val['val'] ?>">
                        <a href="javascript:void(0)" style="background: <?php echo $val['val'] ?>" title="<?php echo $val['val'] ?>" class="xamzo-btn-icon xamzo-btn-color" onclick="$(this).parent().find('input').click();"></a>
                    </span>
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">                                    
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>"> 
                    <input type="hidden" name="<?php echo $key . '[validation]' ?>" value="<?php echo isset($val['validation']) ? $val['validation'] : '' ?>">
                    <script type="text/javascript">
                            $(function() {
                                $('input.cPicker').click(function() {
                                    $this = $(this);
                                    $(this).ColorPicker({
                                        color: $this.attr('title'),
                                        onBeforeShow: function(e) {
                                            $(this).attr('id', 'cp_' + $(e).attr('id'));
                                        },
                                        onChange: function() {
                                            var cp = $(this).attr('id');
                                            var set_color = $('html').find('#' + cp + ' .colorpicker_hex input').val();
                                            $('input#cp_' + cp).val('#' + set_color);
                                            $('input#cp_' + cp).parent().find('a').css({
                                                'background': '#' + set_color
                                            });
                                        }
                                    });
                                });
                            });
                    </script>
                </p> 
                <?php
                break;
            case 'checkbox':
                ?>
                <p class="in-<?php echo $key ?>">
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <?php
                    $tck = '#';
                    ksort($val);
                    foreach ($val as $k => $v) {
                        $tv = $v;
                        if (is_bool($v)) {
                            if ($v == true) {
                                $tv = 'true';
                            } else {
                                $tv = 'false';
                            }
                        }
                        if (is_bool($v) || $tv == 'true' || $tv == 'false') {
                            if ($tv == 'false') {
                                $tck.=$k . '#';
                            }
                            ?>
                            <input type="checkbox" onclick="x_set_check(this);" name="<?php echo $key . "[$k]" ?>" value="<?php echo ($v && $v == 'true') ? 'true' : 'false' ?>" <?php echo (($v == "true")) ? 'checked' : '' ?> id="<?php echo $key . "[$k]" ?>"> <label for="<?php echo $key . "[$k]" ?>"><?php echo $k ?> </label>
                            <?php
                        }
                    }
                    ?>
                    <input type="hidden" class="xamzo-tck" name="<?php echo $key . '[tck]' ?>" value="<?php echo $tck ?>">
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>"> 
                </p>
                <?php
                break;
            case 'file':
                ?>
                <p class="in-<?php echo $key ?>">
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <input type="text" ajax="true" ajax-target="modal-setting" modal-width="250px" modal-fix="false" href="<?php echo site_url('setting/upload/') ?><?php echo $key . '[val]' ?>" <?php echo isset($val['validation']) ? "validation='$val[validation]'" : '' ?> name="<?php echo $key . '[val]' ?>" value="<?php echo $val['val'] ?>">
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">                                    
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>">
                    <input type="hidden" name="<?php echo $key . '[validation]' ?>" value="<?php echo isset($val['validation']) ? $val['validation'] : '' ?>">
                </p>
                <?php
                break;
                ?>
                <p class="in-<?php echo $key ?>">
                    <label class="control-label big">
                        <b><?php echo isset($this->lang[$key]) ? $this->lang[$key] : str_replace('_', ' ', $key) ?> </b>
                        <i><?php echo isset($this->lang[$val['desc']]) ? $this->lang[$val['desc']] : str_replace('_', ' ', $val['desc']) ?></i>
                    </label>
                    <input type="hidden" name="<?php echo $key . '[key]' ?>" value="<?php echo $key ?>">
                    <?php
                    $tck = '#';
                    ksort($val);
                    foreach ($val as $k => $v) {
                        $tv = $v;
                        if (is_bool($v)) {
                            if ($v == true) {
                                $tv = 'true';
                            } else {
                                $tv = 'false';
                            }
                        }
                        if (is_bool($v) || $tv == 'true' || $tv == 'false') {
                            if ($tv == 'false') {
                                $tck.=$k . '#';
                            }
                            ?>
                            <input type="checkbox" onclick="xamzo_set_check(this);" name="<?php echo $key . "[$k]" ?>" value="<?php echo ($v && $v == 'true') ? 'true' : 'false' ?>" <?php echo (($v == "true")) ? 'checked' : '' ?> id="<?php echo $key . "[$k]" ?>"> <label for="<?php echo $key . "[$k]" ?>"><?php echo $k ?> </label>
                            <?php
                        }
                    }
                    ?>
                    <input type="hidden" class="xamzo-tck" name="<?php echo $key . '[tck]' ?>" value="<?php echo $tck ?>">
                    <input type="hidden" name="<?php echo $key . '[desc]' ?>" value="<?php echo $val['desc'] ?>">
                    <input type="hidden" name="<?php echo $key . '[type]' ?>" value="<?php echo $val['type'] ?>"> 
                </p>
                <?php
                break;
            default:
                break;
        }
    endforeach;
endfor;
?>
<script type="text/javascript">
                            function xamzo_set_check(t) {
                                var av = $(t).parent().find('.xamzo-tck').val();
                                var tal = t.name.split('[');
                                var nav = '';
                                var tval = tal['1'].replace(']', '');
                                if (av.match(tval)) {
                                    nav = av.replace(tval + '#', '');
                                    $(t).attr('checked', true).val('true');
                                } else {
                                    nav = av + tval + '#';
                                    $(t).removeAttr('checked').val('false');
                                }
                                $(t).parent().find('.xamzo-tck').val(nav);

                            }
</script>