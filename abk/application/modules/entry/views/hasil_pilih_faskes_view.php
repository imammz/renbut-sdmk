<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >
                <div class="wrap-content container" id="container">
                    <!-- start: PAGE TITLE -->
                    <section id="page-title">
                        <div class="row">
                            <div class="col-sm-8">
                                <h1 class="mainTitle">Hasil Entry ABK</h1>
                                <span class="mainDescription">Pilihlah Jenis Faskes</span>
                            </div>
                            <ol class="breadcrumb">
                                <li>
                                    <span>Hasil Entry</span>
                                </li>
                                <li class="active">
                                    <span>Pilih Jenis Faskes</span>
                                </li>
                            </ol>
                        </div>
                    </section>
                    <!-- end: PAGE TITLE -->
                    <!-- start: YOUR CONTENT HERE -->
                    <div class="container-fluid container-fullw bg-white">
                        <div class="row">
                            <div class="col-md-12"> 

                                <div class="row">
                                  <?php 
                                  $account_model = new account_model;
                                  $level = $account_model->_getKodeLevelUser($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
                                  ?>  
                                    <?php if($level==1||$level==2||$level==3||$level==42) { ?>
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/rumahsakit">
                                                    <h3>Rumah Sakit</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/rumah sakit.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                     <?php } if($level==1||$level==2||$level==3||$level==41) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/puskesmas">
                                                    <h3>Puskesmas</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/puskesmas.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                      <?php } if($level==1||$level==2||$level==3||$level==49) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/klinik">
                                                    <h3> Klinik </h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                     <?php } if($level==1||$level==2||$level==3||$level==410) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/labkes">
                                                    <h3> Labkes </h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                     <?php } if($level==1||$level==2||$level==3||$level==48) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/bpkm">
                                                    <h3> BPKM </h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                     <?php } if($level==1||$level==2||$level==3||$level==43) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/bptklpp">
                                                    <h3> BPTKLPP </h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                     <?php } if($level==1||$level==2||$level==3||$level==44) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/kkp">
                                                    <h3>KKP</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/kkp.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                
                                     <?php } if($level==1||$level==2||$level==3||$level==498) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/upt">
                                                    <h3>UPT Pusat</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                   
                                     <?php } if($level==1||$level==2||$level==3||$level==497) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/uptdprov">
                                                    <h3>UPT Provinsi</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                     <?php } if($level==1||$level==2||$level==3||$level==496) { ?>                                   
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/uptdkab">
                                                    <h3>UPT Kab/Kota</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                     <?php } if($level==1||$level==2||$level==3) { ?>
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/dinaskesehatan">
                                                    <h3>Dinas Kesehatan Kab/Kota</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/kantor dinas kesehatan.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/balaikesehatan">
                                                    <h3>Balai Pelatihan Kesehatan</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/balai kesehatan.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="col-sm-6">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/lainnya">
                                                    <h3>Balai POM, Balai Labkes, Bapelkes, BTKL, Gudang Farmasi, dan Faskes Lainnya</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/logo_depkes.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                     <?php } if($level==1||$level==2) { ?>  
                                    
                                    <div class="col-sm-3">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="text-align: center">
                                                <a href="<?php echo base_url() ?>index.php/entry/hasilentry/dinaskesehatanprov">
                                                    <h3>Kantor Dinas Kesehatan Provinsi</h3>
                                                    <img width="80" src="<?php echo base_url() ?>assets/login/img/icon/kantor dinas kesehatan.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                  <?php } ?>

                                 
                                    
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->
          
        </div>
    </div>
    <?php echo Modules::run('templates/cliptwo/footer'); ?>
</div>
<?php echo Modules::run('templates/cliptwo/js'); ?>
<!-- start: JavaScript Event Handlers for this page -->
<script>
    jQuery(document).ready(function () {
        Main.init();
    });
</script>
<!-- end: JavaScript Event Handlers for this page -->
<!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
