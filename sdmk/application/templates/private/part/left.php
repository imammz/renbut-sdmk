<ul class="menu-box">
    <li>
        <a ajax="true" ajax-target=".xamzo-right" url-push="true" href="#!dashboard/index" class="selected" onclick="slide_menu('.xamzo-m-a');" ajax-title="Dashboard">
            <i class="fa fa-home"></i><br/>
            <span>Beranda</span>
        </a>
    </li>
    <?php if(is_auth('proyeksi')): ?>
    <li> 
        <a ajax="true" url-push="true" ajax-target=".xamzo-right" href="#!proyeksi/index" onclick="slide_menu('.xamzo-m-b');" class="sub-menu" ajax-title="Proyeksi">
            <i class="fa fa-bar-chart"></i><br/>
            <span>Proyeksi</span>            
        </a> 
    </li>
    <?php endif; ?>  
    <?php if(is_auth('con_pengguna')): ?>
    <li>
        <a ajax="true" url-push="true" ajax-target=".xamzo-right" href="#!user/index" class="sub-menu" onclick="slide_menu('.xamzo-m-d');" ajax-title="Pengguna">
            <i class="fa fa-users"></i><br/>
            <span>Pengguna</span>
        </a>
    </li>  
    <?php endif; ?>
    <?php if(is_admin()): ?>
    <li>
        <a ajax="true" url-push="true" ajax-target=".xamzo-right" href="#!setting/index" onclick="slide_menu('.xamzo-m-g');" ajax-title="Master - Pengaturan">
            <i class="fa fa-wrench"></i><br/>
            <span>Pengaturan</span>
        </a>
    </li>
    <?php endif; ?> 
</ul>