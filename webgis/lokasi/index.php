<!DOCTYPE html>
<?php $base_url="http://".$_SERVER['SERVER_NAME'];?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Google Maps</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
    <script src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=drawing&geometry"></script>
    <script src="../js/lokasi.js"></script>
	<link href="../js/chosen/chosen.css" rel="stylesheet">
	<script src="../js/chosen/chosen.jquery.js"></script>
	<script src="../js/chosen/prism.js" type="text/javascript" charset="utf-8"></script>

	<script>
		
$(document).ready(function(){
   $('.chosen-select', this).chosen({search_contains:true});
   $('#status').chosen({search_contains:true});
   $('#kecamatan').chosen({search_contains:true});
});
	</script>	
</head>
<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <a class="navbar-brand" href="#">ABCD</a>
		</div>
		<ul class="nav navbar-nav">
		  <li><a href="<?=$base_url?>/new_polygon/peta">Wilayah</a></li>
		  <li class="active"><a href="<?=$base_url?>/new_polygon/lokasi">Lokasi</a></li>
		  <li><a href="<?=$base_url?>/new_polygon/m_lokasi">Master Lokasi</a></li>
		</ul>
	  </div>
	</nav>
    <div class="container">
        <div class="row">
           <div class="col-lg-12">
                <?php require('../include/connect.php'); ?>
                <h2>Peta ABC</h2>
				<table class="table" border="0">
            		<tr  style="">
                		<td style="width:200px">
                    		<select data-placeholder="Choose a File ..." class="chosen-select" id="search" name="search"  class="form-control" style="width:200px"/>
								<option value="">Silahkan Pilih Data</option>
    		                	<?php 
        		                	$sql = mysql_query("SELECT * FROM `markers`");
        		                	while($val = mysql_fetch_array($sql)) {
        		                		echo '<option value="'.$val['name'].'">'.$val['name'].'</option>';
        		                	}
    		                	?>
                    		</select>
                		</td>
                		<td>
                			<button type="button" id="search_location" class="btn btn-primary">Cari Lokasi Kesehatan</button>
                		</td>
                    </tr>
            	</table>
                <div id="map-canvas" style="width:100%; height:600px;"></div>
            </div>
        </div>
        <hr>
    </div>
   </body>
</html>
