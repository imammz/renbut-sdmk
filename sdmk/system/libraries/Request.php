<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package		library/request
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Request {

    public $_get = array();
    public $_post = array();

    function __construct() {
        $this->_post = $_POST;
        $this->_get = $_GET;
    }

    function post($key = null, $flags = true) {
        if ($key != null) {
            $result = isset($this->_post[$key]) ? $this->_post[$key] : '';
            if ($flags) {
                return x_xss_clean($result, $flags);
            } else {
                return $result;
            }
        }
    }

    function get($key = null, $flags = true) {
        if ($key != null) {
            $result = isset($this->_get[$key]) ? $this->_get[$key] : '';
            if ($flags) {
                return x_xss_clean($result, $flags);
            } else {
                return $result;
            }
        }
    }
 
}