<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                echo "<h3>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h3>";
                ?>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/klinik/editfaskes_proses/<?php echo $faskes['faskes_kode'] ?>">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_kode">
                            Kode Klinik
                        </label>
                        <div class="col-sm-10">
                            <h3><?php echo $faskes['faskes_kode']?></h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_nama">
                            Nama Klinik
                        </label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Nama Klinik" id="faskes_nama" name="faskes_nama" class="form-control" value="<?php echo $faskes['faskes_nama'] ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_alamat">
                            Alamat Lengkap Klinik
                        </label>
                        <div class="col-sm-10">
                            <input type="text" placeholder="Alamat Klinik" id="faskes_alamat" name="faskes_alamat" value="<?php echo $faskes['faskes_alamat'] ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="faskes_telp">
                            Telepon
                        </label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Telepon Klinik" id="faskes_telp" name="faskes_telp" value="<?php echo $faskes['faskes_telp'] ?>" class="form-control">
                        </div>
                        <label class="col-sm-2 control-label" for="faskes_email">
                            Email
                        </label>
                        <div class="col-sm-4">
                            <input type="text" placeholder="Email Klinik" id="faskes_email" name="faskes_email" value="<?php echo $faskes['faskes_email'] ?>" class="form-control">
                        </div>
                    </div>

                    <div class="form-group" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Simpan</button>
                        &nbsp;
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });

                   

    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
