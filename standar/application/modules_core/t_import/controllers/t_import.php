<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class T_import extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('import_model','import');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Import Data";
		$data['subtitle'] = "Import Data";
		$this->load->view('index', $data);
	}

	public function ajax_add()
	{
		//print_r($_POST);die;

		// UPLOAD SINGLE FILE //
		$config_upload_single = array(
			'filename' => 'path_file',
			'path_folder' => 'import',
			'type' => 'file',
			);

		$uploadfile = Upload_file::upload_single_file($config_upload_single); //print_r($uploadfile);die;
		
		if( $uploadfile ){
			$files = file_get_contents('uploaded_files/file/import/'.$uploadfile.'');
			//echo '<pre>'; print_r(json_decode($files));die;
			$files_data = json_decode($files); //print_r($files_data);die;
			foreach ($files_data as $key => $value) {
				# code...
				$perencanaan_array = array(
					'tahun' => $value->tahun,
					'id_jenis_faskes' => $value->id_jenis_faskes,
					'id_provinsi' => $value->id_provinsi,
					'id_kabupaten' => $value->id_kabupaten,
					'kode_puskesmas' => $value->kode_puskesmas,
					'id_tipe_kawasan' => $value->id_tipe_kawasan,
					'id_tipe_puskesmas' => $value->id_tipe_puskesmas,
					);

				// hapus t_perencanaan berdasarkan parameter yang dipost //
				$this->db->delete('t_perencanaan', $perencanaan_array );

				$perencanaan_array['jumlah_pustu'] = $value->jumlah_pustu;
				$perencanaan_array['jumlah_desa'] = $value->jumlah_desa;
				$this->db->insert('t_perencanaan', $perencanaan_array);
				$last_perencanaan_id = $this->db->insert_id();

				foreach($value->standar_detil as $row){
					$insert = array(
						'id_perencanaan' => $last_perencanaan_id,
						'id_jenis_sdmk' => $row->id_jenis_sdmk,
						'jml_pns' => $row->jml_pns,
						'jml_pppk' => $row->jml_pppk,
						'jml_ptt' => $row->jml_ptt,
						'jml_honorer' => $row->jml_honorer,
						'jml_blud' => $row->jml_blud,
						'jml_tks' => $row->jml_tks,
						'total_standar' => $row->total_standar,
						'total_jml' => $row->total_jml,
						);
					$this->db->insert('t_detail_perencanaan', $insert);
				}
			}	
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	
}
