<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class Master {

    function get_tahun($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		$year = array();
		$now = date('Y');
		for ($i=$now-2; $i < $now+2 ; $i++) { 
			$year[] = $i;
		}
		$data = $year;

		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row?'selected':'';
					$field.='<option value="'.$row.'" '.$sel.' >'.strtoupper($row).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_tipe_puskesmas($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_tipe_puskesmas')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_tipe_puskesmas']?'selected':'';
					$field.='<option value="'.$row['id_tipe_puskesmas'].'" '.$sel.' >'.strtoupper($row['nama_tipe_puskesmas']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_jenis_faskes($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_jenis_faskes')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_jenis_faskes']?'selected':'';
					$field.='<option value="'.$row['id_jenis_faskes'].'" '.$sel.' >'.strtoupper($row['nama_jenis_faskes']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_jenis_sdmk($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_jenis_sdmk')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_jenis_sdmk']?'selected':'';
					$field.='<option value="'.$row['id_jenis_sdmk'].'" '.$sel.' > ['.$row['id_jenis_faskes'].'] '.strtoupper($row['nama_jenis_sdmk']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_kelompok_sdmk($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_kelompok_sdmk')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_kelompok_sdmk']?'selected':'';
					$field.='<option value="'.$row['id_kelompok_sdmk'].'" '.$sel.' >'.strtoupper($row['nama_kelompok_sdmk']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_role($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		$db->from('m_role');

		if( in_array($this->session->userdata('data_user')->id_role, array('1','2')) ){

			$db->where(array('active'=>'Y'));

		}else{

			$db->where_not_in('id_role', array('1','2'));

		}

        $data = $db->get()->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_role']?'selected':'';
					$field.='<option value="'.$row['id_role'].'" '.$sel.' >'.strtoupper($row['role_name']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_provinsi($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->id_provinsi ){
			$db->where(array('id_provinsi'=>$this->session->userdata('data_user')->id_provinsi));
		}
        $data = $db->get('m_provinsi')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_provinsi']?'selected':'';
					$field.='<option value="'.$row['id_provinsi'].'" '.$sel.' >'.strtoupper($row['nama_provinsi']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_kabupaten($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->id_kabupaten ){
			$db->where(array('id_kabupaten'=>$this->session->userdata('data_user')->id_kabupaten));
		}
        $data = $db->get('m_kabupaten')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';
				if($nid==''){
					$field.='<option value="#" >(Pilih Kabupaten)</option>';
				}else{
					foreach($data as $row){
						$sel = $nid==$row['id_kabupaten']?'selected':'';
						$field.='<option value="'.$row['id_kabupaten'].'" '.$sel.' >'.strtoupper($row['nama_kabupaten']).'</option>';
					}
				}
								
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_change_master_kabupaten($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->id_kabupaten ){
			$db->where(array('id_kabupaten'=>$this->session->userdata('data_user')->id_kabupaten));
		}/*else if( $nid != '' ) {
			$qry = $db->get_where('m_kabupaten', array('id_kabupaten'=>$nid));
			$id_provinsi = (count($qry->num_rows()) > 0) ? $qry->row()->id_provinsi : 0;
			$db->where(array('id_provinsi'=>$id_provinsi));
		}*/

		$data = $db->get('m_kabupaten')->result_array();

		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';
				if($nid==''){
					$field.='<option value="#" >(Pilih Kabupaten)</option>';
				}else{
					foreach($data as $row){
						$sel = $nid==$row['id_kabupaten']?'selected':'';
						$field.='<option value="'.$row['id_kabupaten'].'" '.$sel.' >'.strtoupper($row['nama_kabupaten']).'</option>';
					}
				}
								
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_puskesmas($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_puskesmas')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['kode_puskesmas']?'selected':'';
					$field.='<option value="'.$row['kode_puskesmas'].'" '.$sel.' >'.strtoupper($row['nama_puskesmas_kab']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_change_master_puskesmas($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->kode_puskesmas ){
			$db->where('kode_puskesmas', $this->session->userdata('data_user')->kode_puskesmas);
		}
		
		/*if($nid!=''){
			$qry = $db->get_where('m_puskesmas', array('kode_puskesmas'=>$nid));
			$id_kabupaten = $qry->row()->id_kabupaten;
        	$data = $db->get_where('m_puskesmas', array('id_kabupaten'=>$id_kabupaten))->result_array();
		}*/

        $data = $db->get('m_puskesmas')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';
				if($nid==''){
					$field.='<option value="#" >(Pilih Puskesmas)</option>';
				}else{
					foreach($data as $row){
						$sel = $nid==$row['kode_puskesmas']?'selected':'';
						$field.='<option value="'.$row['kode_puskesmas'].'" '.$sel.' >'.strtoupper($row['nama_puskesmas_kab']).'</option>';
					}
				}
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_change_master_kkp($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->kode_kkp ){
			$db->where('kode_kkp', $this->session->userdata('data_user')->kode_kkp);
		}
		
		/*if($nid!=''){
			$qry = $db->get_where('m_puskesmas', array('kode_puskesmas'=>$nid));
			$id_kabupaten = $qry->row()->id_kabupaten;
        	$data = $db->get_where('m_puskesmas', array('id_kabupaten'=>$id_kabupaten))->result_array();
		}*/

        $data = $db->get('m_kkp')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';
				if($nid==''){
					$field.='<option value="#" >(Pilih KKP)</option>';
				}else{
					foreach($data as $row){
						$sel = $nid==$row['kode_kkp']?'selected':'';
						$field.='<option value="'.$row['kode_kkp'].'" '.$sel.' >'.strtoupper($row['nama_kkp']).'</option>';
					}
				}
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_change_master_klinik($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->kode_klinik ){
			$db->where('kode_klinik', $this->session->userdata('data_user')->kode_klinik);
		}
		
		/*if($nid!=''){
			$qry = $db->get_where('m_puskesmas', array('kode_puskesmas'=>$nid));
			$id_kabupaten = $qry->row()->id_kabupaten;
        	$data = $db->get_where('m_puskesmas', array('id_kabupaten'=>$id_kabupaten))->result_array();
		}*/

        $data = $db->get('m_klinik')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';
				if($nid==''){
					$field.='<option value="#" >(Pilih Klinik)</option>';
				}else{
					foreach($data as $row){
						$sel = $nid==$row['kode_klinik']?'selected':'';
						$field.='<option value="'.$row['kode_klinik'].'" '.$sel.' >'.strtoupper($row['nama_klinik']).'</option>';
					}
				}
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_change_master_btkl($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->kode_btkl ){
			$db->where('kode_btkl', $this->session->userdata('data_user')->kode_btkl);
		}
		
		/*if($nid!=''){
			$qry = $db->get_where('m_puskesmas', array('kode_puskesmas'=>$nid));
			$id_kabupaten = $qry->row()->id_kabupaten;
        	$data = $db->get_where('m_puskesmas', array('id_kabupaten'=>$id_kabupaten))->result_array();
		}*/

        $data = $db->get('m_btkl')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';
				if($nid==''){
					$field.='<option value="#" >(Pilih BTKL)</option>';
				}else{
					foreach($data as $row){
						$sel = $nid==$row['kode_btkl']?'selected':'';
						$field.='<option value="'.$row['kode_btkl'].'" '.$sel.' >'.strtoupper($row['nama_btkl']).'</option>';
					}
				}
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_rumah_sakit($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_rumah_sakit')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['kode_rs']?'selected':'';
					$field.='<option value="'.$row['kode_rs'].'" '.$sel.' >'.strtoupper($row['nama_rs']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_change_master_rsu($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		if( $this->session->userdata('data_user')->kode_rs ){
			$db->where('kode_rs', $this->session->userdata('data_user')->kode_rs);
		}
		
		/*if($nid!=''){
			$qry = $db->get_where('m_puskesmas', array('kode_puskesmas'=>$nid));
			$id_kabupaten = $qry->row()->id_kabupaten;
        	$data = $db->get_where('m_puskesmas', array('id_kabupaten'=>$id_kabupaten))->result_array();
		}*/

        $data = $db->get('m_rumah_sakit')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';
				if($nid==''){
					$field.='<option value="#" >(Pilih Rumah Sakit)</option>';
				}else{
					foreach($data as $row){
						$sel = $nid==$row['kode_rs']?'selected':'';
						$field.='<option value="'.$row['kode_rs'].'" '.$sel.' >'.strtoupper($row['nama_rs']).'</option>';
					}
				}
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_jenis_rumah_sakit_khusus($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_jenis_rsk')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_jenis_rsk']?'selected':'';
					$field.='<option value="'.$row['id_jenis_rsk'].'" '.$sel.' >'.strtoupper($row['nama_jenis_rsk']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_master_menu($nid='',$name,$id,$class='',$required='',$inline='') {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_menu')->result_array();
		
		$selected = $nid?'':'selected';
		$readonly = '';//$CI->session->userdata('nrole')=='approver'?'readonly':'';
		
		$starsign = $required?'*':'';
		
		$fieldset = $inline?'':'<fieldset>';
		$fieldsetend = $inline?'':'</fieldset>';
		
		$field='';
		$field.=$fieldset.'
		<select class="'.$class.'" name="'.$name.'" id="'.$id.'" '.$readonly.' '.$required.' >
			<option value="0" '.$selected.'> - Silahkan pilih - </option>';

				foreach($data as $row){
					$sel = $nid==$row['id_menu']?'selected':'';
					$field.='<option value="'.$row['id_menu'].'" '.$sel.' >'.strtoupper($row['name']).'</option>';
				}	
			
		$field.='
		</select>
		'.$fieldsetend;
		
		return $field;
		
    }

    function get_menu_data() {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		$getData = array();
        $data = $db->where(array('active'=>'Y', 'parent_menu'=>0))->get('m_menu')->result_array();
        foreach($data as $rowdata){
        	$submenu = $db->where(array('active'=>'Y', 'parent_menu'=>$rowdata['id_menu']))->get('m_menu')->result_array();
        	$rowdata['submenu'] = $submenu;
        	$getData[] = $rowdata;
        }
		
		return $getData;
		
    }

    function get_func_action_data() {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->where(array('active'=>'Y'))->get('m_func_action')->result_array();
		
		return $data;
		
    }

    function get_puskesmas_data() {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		$data = $this->db->query('SELECT * FROM m_puskesmas LIMIT 0,10')->result_array();
        //$data = $db->get('m_puskesmas')->limit(0,10)->result_array();
		
		return $data;
		
    }

    function get_tipe_puskesmas_data() {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		$data = $this->db->query('SELECT * FROM m_tipe_puskesmas')->result_array();
        //$data = $db->get('m_puskesmas')->limit(0,10)->result_array();
		
		return $data;
		
    }

    function get_tipe_kawasan_data() {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->get('m_tipe_kawasan')->result_array();
		
		return $data;
		
    }

    function get_jenis_sdmk_data($kode_faskes) {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
        $data = $db->where('id_jenis_faskes', $kode_faskes)->order_by('counter', 'ASC')->get('m_jenis_sdmk')->result_array();
		//print_r($this->db->last_query());
		return $data;
		
    }

    function get_custom_data($params) {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		$data = $db->where($params['where'])->get($params['table'])->result_array();
		
		return $data;
		
    }

    function params_query($params) {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		$data = $db->where($params['where'])->get($params['table']);
		
		return $data;
		
    }

	
}

?> 