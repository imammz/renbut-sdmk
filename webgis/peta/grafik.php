<?php

   require ('../include/connect.php');
		$id_provinsi = $_POST['id'];
		$nama_provinsi = $_POST['name'];
		
		if($id_provinsi=='all'){
			$sql ="SELECT a.id_provinsi,b.`id_profesi`,SUM(total) AS total,nama_profesi
					FROM provinsi a JOIN sdm b
					ON a.id_provinsi=b.`id_provinsi`
					JOIN profesi c
					ON b.`id_profesi`=c.`id_profesi`
					GROUP BY nama_profesi";
		}else{
			$sql = "SELECT c.`nama_profesi`,IFNULL(total,0) as total
					FROM profesi c LEFT OUTER JOIN (SELECT a.id_provinsi,b.`id_profesi`,a.nama_provinsi,total
					FROM provinsi a JOIN sdm b
					ON a.id_provinsi=b.`id_provinsi`
					WHERE a.id_provinsi='$id_provinsi') i
					ON i.id_profesi=c.`id_profesi`
					order by nama_profesi";
		}
		//echo $sql;
	   $data = mysql_query($sql);
	   
	 
?>

	
	<script type="application/javascript">
		$(document).ready(function(){
			var loading ='<div align="center"><img class="img-circle" src="../assets/images/ajax-loader.gif"></div>';
			$('#chartdiv').html(loading);
			<?php
			$status = array('total');
			
			
			//$status = array('sangat_baik','baik','cukup','buruk','sangat_buruk');
			
			$loop = '';

			while($row = mysql_fetch_assoc($data)){
				$loop .=  '{"status":"'.$row['nama_profesi'].'",
							"total":"'.$row['total'].'"},';
			};
			?>
			
			var chart;
			var chartData =
			[
				<?=$loop?>
			];
			 chart = new AmCharts.AmSerialChart();
			chart.dataProvider = chartData;
			chart.categoryField = "status";
			chart.startDuration = 1;
			chart.balloon.color = "#000000";

			// AXES
			var categoryAxis = chart.categoryAxis;
			categoryAxis.fillAlpha = 1;
			categoryAxis.fillColor = "#FAFAFA";
			categoryAxis.gridAlpha = 0;
			categoryAxis.axisAlpha = 0;
			categoryAxis.gridPosition = "start";

			// value
			/*
			var valueAxis = new AmCharts.ValueAxis();
			valueAxis.title = "Total";
			valueAxis.dashLength = 5;
			valueAxis.axisAlpha = 0;
			valueAxis.integersOnly = true;
			valueAxis.gridCount = 10;
			chart.addValueAxis(valueAxis);*/
			
			var valueAxis = new AmCharts.ValueAxis();
			valueAxis.stackType = "regular";
			valueAxis.title = "Total";
			valueAxis.gridAlpha = 0.1;
			valueAxis.axisAlpha = 0;
			chart.addValueAxis(valueAxis);

			// GRAPHS
			// column graph
			<?php
			foreach($status as $status2){
			?>
			/*
			var graph = new AmCharts.AmGraph();
			graph.title = "<?=ucwords(strtolower($status2))?>";
			graph.valueField = "<?=$status2?>";        
			graph.balloonText = "[[title]] : [[value]]";
			graph.lineAlpha = 1;
			graph.bullet = "round";
			chart.addGraph(graph);*/
			
			var graph = new AmCharts.AmGraph();
			graph.title = "<?=ucwords(strtolower($status2))?>";
		   // graph.labelText = "[[value]]";
			//graph.labelPosition = 'top'; 
			graph.valueField = "<?=$status2?>";  
			graph.balloonText = "[[title]] : [[value]]";
			graph.type = "column";
			graph.lineAlpha = 0;
			graph.fillAlphas = 1;
			chart.addGraph(graph);
			
			<?php
			}
			?>
			
			// CURSOR
			var chartCursor = new AmCharts.ChartCursor();
			chartCursor.cursorPosition = "mouse";
			chartCursor.zoomable = false;
			chartCursor.cursorAlpha = 0;
			chart.addChartCursor(chartCursor);                

			// LEGEND
			var legend = new AmCharts.AmLegend();
			legend.useGraphSettings = true;
			chart.addLegend(legend);

			
				// WRITE
			chart.pathToImages = "../assets/js/plugins/amcharts/amcharts/images/";
			chart.amExport = {
							top: 21,
							right: 21,
							buttonColor: '#EFEFEF',
							buttonRollOverColor:'#DDDDDD',
							exportPNG:true,
							exportJPG:true,
							exportPDF:true,
							exportSVG:true};
			chart.write("chartdiv");
		});


		</script>
		<div class="portlet">
			<div class="portlet-heading dark">
				<div class="portlet-title">
					<h4><i class="fa fa-graphics"></i>Aktual <?=$nama_provinsi?></h4>
				</div>
				<div class="clearfix"></div>
			</div>
			<div id="notices" class="panel-collapse collapse in">
			<div class="portlet-body">
				<div id="chartdiv" style="height:250px;"></div>
			</div>
		</div>
		</div>
		