<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<title>Media - Print</title>
<script type="text/javascript" src="<?php echo site_url('application/modules/media/assets/javascript/script.js') ?>"></script>
<div class="xamzo-head-right"> 
    <div class="left">
        <h3>
            <span class="fa fa-folder-open"></span> &nbsp;<?php echo $this->lang['media'] ?>
            <sup class="sup-menu">
                <i class="fa fa-file-word-o"></i> report - <?php echo $dir ?>
            </sup>
        </h3>
        <span class="xamzo-head-info">Kelola dokumen report</span>
    </div>
    <div class="right">
        <div>&nbsp;</div>
        <div class="no-phone">
            <a ajax="true" class="btn-opt <?php echo ($dir=='income') ? 'sm-active"' : '' ?>" ajax="true" url-push="true" href="#!media/report/income" ajax-target=".xamzo-right" original-title="lopran pemasukan"><i class="fa fa-sign-in"></i> Pemasukan</a>
            <a ajax="true" class="btn-opt <?php echo ($dir=='expense') ? 'sm-active"' : '' ?>" ajax="true" url-push="true" href="#!media/report/expense" ajax-target=".xamzo-right" original-title="laporan pengeluaran"><i class="fa fa-sign-out"></i> Pengeluaran</a>
            <a ajax="true" class="btn-opt <?php echo ($dir=='compare') ? 'sm-active"' : '' ?>" ajax="true" url-push="true" href="#!media/report/compare" ajax-target=".xamzo-right"  original-title="laporan perbandingan"><i class="fa fa-compress"></i> Perbandingan</a>
            <a ajax="true" class="btn-opt <?php echo ($dir=='purcase') ? 'sm-active"' : '' ?>" ajax="true" url-push="true" href="#!media/report/purcase" ajax-target=".xamzo-right" original-title="laporan pemasukan pegawai"><i class="fa fa-group"></i> Pegawai</a>
            <a ajax="true" class="btn-opt <?php echo ($dir=='profit') ? 'sm-active"' : '' ?>" ajax="true" url-push="true" href="#!media/report/profit" ajax-target=".xamzo-right"  original-title="laporan profit"><i class="fa fa-money"></i> Keuntungan</a>
            <a ajax="true" class="btn-opt <?php echo ($dir=='summary') ? 'sm-active"' : '' ?>" ajax="true" url-push="true" href="#!media/report/summary" ajax-target=".xamzo-right"  original-title="laporan kesimpulan"><i class="fa fa-magic"></i> Kesimpulan</a>
        </div>
    </div>
</div> 
<div id="xamzo-head-action">
    <button crud-name="check" class="btn primary large fa fa-check-square-o tip-top" original-title="<?php echo $this->lang['select_all'] ?>" table-check-all="true"></button>
    <button class="btn primary large fa fa-angle-down" dropdown="true" dropdown-relative="left" dropdown-target="post-add">
        <?php echo $this->lang['option']; ?>
    </button>
    <?php if (is_auth('con_media')): ?>
    <button ajax="true" url-push="true" href="#!setting/_report" ajax-target=".xamzo-right" crud-name="check" class="btn primary large fa fa-gears tip-top" original-title="<?php echo $this->lang['setting'] ?>"></button>
    <?php endif; ?>
    <button crud-name="delete" url-push="true" crud-url="media/delete_all" disabled="disabled" crud-redirect="media/report/<?php echo $dir ?>" crud-target=".xamzo-right" crud-data="#crud-data" crud-table-target="ul.list_doc" class="btn primary large fa fa-trash tip-top" original-title="<?php echo $this->lang['delete_data']; ?>"></button>
    <input type="name" name="search" placeholder="<?php echo $this->lang['search']; ?>.." crud-search="true" search-url="media/report/<?php echo $dir ?>" search-target="#crud-target" class="tip-bottom-left small no-phone" original-title="cari berdasarkan nama">
    <button crud-name="list" ajax="true" url-push="true" href="#!media/report/<?php echo $dir ?>" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top-right" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div> 

<ul class="dropdown-target dropdown-target-left post-add">
    <div class="arrow-up"></div>
    <li>
        <a ajax="true" url-push="true" href="#!media/index" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> <?php echo $this->lang['general']; ?></a>
    </li>      
    <li>    
        <a ajax="true" url-push="true" href="#!media/report" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> Laporan Proyek</a>
    </li>   
    <li>   
        <a ajax="true" url-push="true" href="#!media/_project" ajax-target=".xamzo-right"><i class="fa fa-angle-right"></i> Proyek File</a>
    </li>   
</ul>
<div id="crud-target"> 
    <div class="xamzo-block" id="xamzo-media">
        <div class="xamzo-block-content">
            <div class="content">
                <form id="crud-data">  
                    <ul class="list_doc xamzo-box-media" id="docs">
                        <li style="display: none;">
                            <input type="checkbox" table-check-all="true" id="check-all">  
                        </li>
                        <?php
                        $id = 0;
                        foreach ($files as $file) {
                            $f = explode('{x}', $file);
                            $ex = explode('.', $file);
                            $filename = substr($f['1'], 0, 9) . '..';
                            echo "<li id='" . $id . "'>
                                    <span class='icon_$ex[1] xamzo-media tip-bottom-left' original-title='$f[1]' dir='" . site_url('report/' .$dir.'/'. $file) . "'>$filename</span>
                                    <div class='box-image-act'>
                                        <div class='left'>
                                            <input table-check-all='true' type='checkbox' onclick='_self_check(this);' name='id[]' value='" . site_url('report/' .$dir.'/'. $file) . "' id='" . 'report/' .$dir.'/'. $file . "'> <label for='" . 'report/' .$dir.'/'. $file . "'></label>
                                        </div>
                                        <div class='right'>
                                                <a href='javascript:void(0)' onclick='file_url($(this).parent());' class='tip-top' original-title='" . $this->lang["link"] . "'><i class='fa fa-link'></i></a>
                                                <a href='javascript:void(0)' onclick='file_property($(this).parent(),\"doc\");' class='tip-top no-phone' original-title='" . $this->lang["properties"] . "'><i class='fa fa-search'></i></a>
                                                <a href='javascript:void(0)' onclick='file_delete($(this).parent());' class='tip-top' original-title='" . $this->lang["delete"] . "'><i class='fa fa-trash'></i></a> 
                                        </div>
                                    </div>
                                  </li>";
                            $id++;
                        }
                        ?>
                    </ul>  
                </form>
            </div>
        </div>
    </div>
    <div class="xamzo-block" id="xamzo-property"></div>
</div> 
<div class="xamzo-pagination">
    <div id="option">
        <?php
        $selected = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : '';
        echo $this->lang['showing_1_to'] . "&nbsp;" . $limit . "&nbsp;" . $this->lang['of'] . " $total " . $this->lang['entries']
        ?> 
    </div>
    <div id="pagging">
        <?php echo $paging; ?> 
    </div>
</div> 

<script type="text/javascript">
    $(function() {
        var d = new Date();
        var id = d.getTime();
        var ul = $('div#xamzo-media ul#docs');

        $('#jq-drop a').click(function() {
            // Simulate a click on the file input button
            // to show the file browser dialog 
            $(this).parent().find('input').click();
        });

        // Initialize the jQuery File Upload plugin
        $('#jq-upload').fileupload({
            // This element will accept file drag/drop uploading
            dropZone: $('#jq-drop'),
            submit: function(e, data) {
                var et = (data.files[0].name);
                var ext = (et.split('.').pop());
                if (!ext.match(/<?php echo $this->lib_media->_config('type_document') ?>/gi)) {
                    $('li#' + id).remove();
                    notive('Only <?php echo $this->lib_media->_config('type_document') ?> files are allowed');
                    return false;
                    data.stop();
                } else {
                    return true;
                }
            },
            // This function is called when a file is added to the queue;
            // either via the browse button, or via drag/drop:
            add: function(e, data) {

                var tpl = $('<li id="' + id + '" style="padding:20px 0; text-align:center"><input type="text" value="0" data-width="48" data-height="48"' +
                        ' data-fgColor="#50b7dc" data-readOnly="1" data-bgColor="#279ac2" /><p></p><span></span></li>');

                // Add the HTML to the UL element
                data.context = tpl.appendTo(ul);

                // Initialize the knob plugin
                tpl.find('input').knob();

                // Listen for clicks on the cancel icon
                tpl.find('span').click(function() {

                    if (tpl.attr('id', id)) {
                        jqXHR.abort();
                    }

                    tpl.fadeOut(function() {
                        tpl.remove();
                    });

                });

                // Automatically upload the file once it is added to the queue
                var jqXHR = data.submit();
            },
            progress: function(e, data) {

                // Calculate the completion percentage of the upload
                var progress = parseInt(data.loaded / data.total * 100, 10);

                // Update the hidden input field and trigger a change
                // so that the jQuery knob plugin knows to update the dial
                data.context.find('input').val(progress).change();

                if (progress === 100) {
                    $('li#' + id).remove();
                }
            },
            fail: function(e, data) {
                // Something has gone wrong!
                data.context.addClass('error');
            },
            success: function(rs) {

                if (rs !== 'error') {
                    $('div#xamzo-media ul#docs').append(rs);
                }
            }
        });
        // Prevent the default action when a file is dropped on the window
        $(document).on('drop dragover', function(e) {
            e.preventDefault();
        });

    });
</script>    