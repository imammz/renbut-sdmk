<script type="text/javascript">
   
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_btkl_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#btkl-box option').remove()
                    $('<option value="">(Pilih BTKLPP)</option>').appendTo($('#btkl-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_btkl+'">'+o.nama_btkl+'</option>').appendTo($('#btkl-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih BTKL)</option>').appendTo($('#kab-box'));
            }
        });
       
    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_prc_btkl" action="<?php echo site_url('perencanaan/tr_btkl/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_perencanaan:'( Auto )'?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                  <label class="control-label col-md-2">Jenis Faskes</label>
                  <div class="col-md-3">
                    <?php echo Master::get_master_jenis_faskes(29,'id_jenis_faskes','id_jenis_faskes','form-control','required readonly','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value)?$value->tahun:'','tahun','tahun','form-control','required','inline');?>
                  </div>
                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','9')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','9')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($value) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

               <!--  <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','9')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('9')) ) ? 'readonly' : '' ;  
                      echo Master::get_change_master_kabupaten(isset($value) ? $value->id_kabupaten : ($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '' ,'id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?> -->

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','9')) ){?>
                <div class="form-group" id="form-btkl" >
                  <label class="control-label col-md-2">BTKL</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('9')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_btkl(isset($value) ? $value->kode_btkl : ($this->session->userdata('data_user')->kode_btkl) ? $this->session->userdata('data_user')->kode_btkl : '','kode_btkl','btkl-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>
                

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('BB','I','II');
                          foreach($katergori_kelas as $row_katergori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_katergori_kelas?>" checked/>
                            <span class="lbl"> <?php echo $name_class = ($row_katergori_kelas=='BB') ? 'BALAI BESAR' : $row_katergori_kelas; ?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                 <div class="page-header">
                  <h1>
                    Bagian berikut diisi sesuai kebutuhan dan beban kerja
                  </h1>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Jumlah Instalasi</label>
                  <div class="col-md-2">
                    <input name="jumlah_instalasi" id="jumlah_instalasi" value="<?php echo isset($value)?$value->jumlah_instalasi:''?>" class="form-control" type="text">
                  </div>
                </div>


                <?php foreach ($form_entry_btkl->result() as $key => $value) {?>
                <div class="form-group">
                  <label class="control-label col-md-2"><?php echo $value->nama_jenis_sdmk?></label>
                  <div class="col-md-2" style="padding-top:10px">
                    <input  name="value[]" id="jml_<?php echo $value->id_jenis_sdmk?>" value="" class="form-control" type="text">
                    <input  name="id_jenis_sdmk_form[]" value="<?php echo $value->id_jenis_sdmk?>" class="form-control" type="hidden">
                  </div>
                </div>
                <?php }?>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_perencanaan:0?>"> -->

                  <a onclick="getMenu('perencanaan')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="getMenu('perencanaan/tr_btkl/riwayat_perencanaan')" href="#" class="btn btn-sm btn-warning">
                    <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
                    Lihat Riwayat Perencanaan
                  </a>
                  <button type="reset" onclick="getMenu('perencanaan/tr_btkl')" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/prc_btkl.js'?>"></script>

