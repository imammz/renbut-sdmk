<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_standar_sdmk_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _query($type){

		$return = new stdClass();

		switch ($type) {
			case 'puskesmas':
				# code...

				$return->table = 'm_standar';
				$return->column = array('m_standar.id_jenis_sdmk','m_jenis_faskes.nama_jenis_faskes','m_jenis_sdmk.nama_jenis_sdmk','m_standar.updated_date','m_tipe_kawasan.nama_tipe_kawasan','m_tipe_puskesmas.nama_tipe_puskesmas');
				$return->select = 'm_standar.*,m_tipe_puskesmas.nama_tipe_puskesmas,m_tipe_kawasan.nama_tipe_kawasan,m_jenis_faskes.nama_jenis_faskes,m_jenis_sdmk.nama_jenis_sdmk';
				$return->order = array('m_jenis_sdmk.counter' => 'asc');
				$return->result_query = $this->query_puskesmas($return);

				return $return;
				break;

			case 'rsu':
				# code...

				$return->table = 'm_standar_rsu';
				$return->column = array('m_standar_rsu.id_jenis_sdmk','m_standar_rsu.A','m_standar_rsu.B','m_standar_rsu.C','m_standar_rsu.D','m_jenis_sdmk.nama_jenis_sdmk','m_standar_rsu.updated_date');
				$return->select = 'm_standar_rsu.*,m_jenis_sdmk.nama_jenis_sdmk';
				$return->order = array('m_jenis_sdmk.counter' => 'asc');
				$return->result_query = $this->same_query($return);

				return $return;
				break;

			case 'rsk':
				# code...

				$return->table = 'm_standar_rsk';
				$return->column = array('m_standar_rsk.id_jenis_sdmk','m_standar_rsk.A','m_standar_rsk.B','m_standar_rsk.C','m_standar_rsk.D','m_jenis_sdmk.nama_jenis_sdmk','m_jenis_faskes.nama_jenis_faskes','m_standar_rsk.updated_date');
				$return->select = 'm_jenis_sdmk.nama_jenis_sdmk,m_jenis_faskes.nama_jenis_faskes,m_standar_rsk.*';
				$return->order = array('m_jenis_sdmk.counter' => 'asc');
				$return->result_query = $this->same_query($return);

				return $return;
				break;

			case 'kkp':
				# code...

				$return->table = 'm_standar_kkp';
				$return->column = array('m_standar_kkp.id_jenis_sdmk','m_standar_kkp.A','m_standar_kkp.B','m_standar_kkp.C','m_standar_kkp.D','m_jenis_sdmk.nama_jenis_sdmk','m_standar_kkp.updated_date');
				$return->select = 'm_standar_kkp.*,m_jenis_sdmk.nama_jenis_sdmk';
				$return->order = array('m_jenis_sdmk.counter' => 'asc');
				$return->result_query = $this->same_query($return);

				return $return;
				break;

			case 'btklpp':
				# code...

				$return->table = 'm_standar_btklpp';
				$return->column = array('m_standar_btklpp.id_jenis_sdmk','m_standar_btklpp.BB','m_standar_btklpp.I','m_standar_btklpp.II','m_jenis_sdmk.nama_jenis_sdmk','m_standar_btklpp.updated_date');
				$return->select = 'm_standar_btklpp.*,m_jenis_sdmk.nama_jenis_sdmk';
				$return->order = array('m_jenis_sdmk.counter' => 'asc');
				$return->result_query = $this->same_query($return);

				return $return;
				break;

			case 'klinik':
				# code...

				$return->table = 'm_standar_klinik';
				$return->column = array('m_standar_klinik.id_jenis_sdmk','m_standar_klinik.utama','m_standar_klinik.pratama','m_jenis_sdmk.nama_jenis_sdmk','m_standar_klinik.updated_date');
				$return->select = 'm_standar_klinik.*,m_jenis_sdmk.nama_jenis_sdmk';
				$return->order = array('m_jenis_sdmk.counter' => 'asc');
				$return->result_query = $this->same_query($return);

				return $return;
				break;
			
			default:
				# code...
				break;
		}
		

	}

	private function query_puskesmas($return){

		$this->db->select($return->select);
		$this->db->from($return->table);
		$this->db->join('m_tipe_puskesmas', ''.$return->table.'.id_tipe_puskesmas=m_tipe_puskesmas.id_tipe_puskesmas' , 'left' );
		$this->db->join('m_tipe_kawasan', ''.$return->table.'.id_tipe_kawasan=m_tipe_kawasan.id_tipe_kawasan' , 'left' );
		$this->db->join('m_jenis_faskes', ''.$return->table.'.id_jenis_faskes=m_jenis_faskes.id_jenis_faskes' , 'left' );
		$this->db->join('m_jenis_sdmk', ''.$return->table.'.id_jenis_sdmk=m_jenis_sdmk.id_jenis_sdmk' , 'left' );

	}

	private function same_query($return){

		$this->db->select($return->select);
		$this->db->from($return->table);
		$this->db->join('m_jenis_sdmk', ''.$return->table.'.id_jenis_sdmk=m_jenis_sdmk.id_jenis_sdmk' , 'left' );
		$this->db->join('m_jenis_faskes', 'm_jenis_faskes.id_jenis_faskes=m_jenis_sdmk.id_jenis_faskes' , 'left' );

	}

	private function same_query_rsk($return){

		$this->db->select($return->select);
		$this->db->from('m_jenis_sdmk');
		$this->db->join('m_standar_rsk', ''.$return->table.'.id_jenis_sdmk=m_jenis_sdmk.id_jenis_sdmk' , 'left' );
		$this->db->join('m_jenis_faskes', 'm_jenis_faskes.id_jenis_faskes=m_jenis_sdmk.id_jenis_faskes' , 'left' );
		$this->db->where_not_in('m_jenis_sdmk.id_jenis_faskes', array('1','3','4','29','30','31'));
		$this->db->where('m_jenis_sdmk.is_standar','Y');
	}

	/*private function query_kkp($return){

		$this->db->select($return->select);
		$this->db->from($return->table);
		$this->db->join('m_jenis_sdmk', ''.$return->table.'.id_jenis_sdmk=m_jenis_sdmk.id_jenis_sdmk' , 'left' );

	}

	private function query_btkl($return){

		$this->db->select($return->select);
		$this->db->from($return->table);
		$this->db->join('m_jenis_sdmk', ''.$return->table.'.id_jenis_sdmk=m_jenis_sdmk.id_jenis_sdmk' , 'left' );

	}

	private function query_klinik($return){

		$this->db->select($return->select);
		$this->db->from($return->table);
		$this->db->join('m_jenis_sdmk', ''.$return->table.'.id_jenis_sdmk=m_jenis_sdmk.id_jenis_sdmk' , 'left' );

	}*/


	private function _get_datatables_query($type)
	{
		//print_r($params);die;
		$return = $this->_query($type);

		$return->result_query;

		$i = 0;
	
		foreach ($return->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($type)
	{
		$this->_get_datatables_query($type);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($type)
	{
		$this->_get_datatables_query($type);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_puskesmas()
	{
		$this->db->from('m_standar');
		return $this->db->count_all_results();
	}

	public function count_all_rsu()
	{
		$this->db->from('m_standar_rsu');
		return $this->db->count_all_results();
	}

	public function count_all_kkp()
	{
		$this->db->from('m_standar_kkp');
		return $this->db->count_all_results();
	}

	public function count_all_btkl()
	{
		$this->db->from('m_standar_btklpp');
		return $this->db->count_all_results();
	}

	public function count_all_klinik()
	{
		$this->db->from('m_standar_klinik');
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($table);
		$this->db->join('m_tipe_puskesmas', ''.$table.'.id_tipe_puskesmas=m_tipe_puskesmas.id_tipe_puskesmas' , 'left' );
		$this->db->join('m_tipe_kawasan', ''.$table.'.id_tipe_kawasan=m_tipe_kawasan.id_tipe_kawasan' , 'left' );
		$this->db->join('m_jenis_faskes', ''.$table.'.id_jenis_faskes=m_tipe_kawasan.id_jenis_faskes' , 'left' );
		$this->db->join('m_jenis_sdmk', ''.$table.'.id_jenis_sdmk=m_jenis_sdmk.id_jenis_sdmk' , 'left' );
		$this->db->where(''.$table.'.id_standar',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update($type, $where, $data)
	{
		$return = $this->_query($type);
		$this->db->update($return->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$table.'.id_standar', $id);
		$this->db->delete($table);
	}


}
