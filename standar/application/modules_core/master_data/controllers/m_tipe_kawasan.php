<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tipe_kawasan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('tipe_kawasan_model','tipe_kawasan');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Jenis Rumah Sakit";
		$data['subtitle'] = "Daftar Jenis Rumah Sakit";
		$this->load->view('tipe_kawasan/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Jenis Rumah Sakit";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->tipe_kawasan->get_by_id($id);
		}
		$this->load->view('tipe_kawasan/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->tipe_kawasan->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $tipe_kawasan) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($tipe_kawasan->nama_tipe_kawasan);
			$row[] = ($tipe_kawasan->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $tipe_kawasan->updated_date?Tanggal::formatDateTime($tipe_kawasan->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($tipe_kawasan->id_tipe_kawasan,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_tipe_kawasan('."'".Regex::_genRegex($tipe_kawasan->id_tipe_kawasan,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->tipe_kawasan->count_all(),
						"recordsFiltered" => $this->tipe_kawasan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_tipe_kawasan = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'nama_tipe_kawasan' => Regex::_genRegex($this->input->post('nama_tipe_kawasan'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_tipe_kawasan == 0 ){
			$this->tipe_kawasan->save($dataexc);
		}else{
			$this->tipe_kawasan->update(array('id_tipe_kawasan'=>$id_tipe_kawasan), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->tipe_kawasan->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
