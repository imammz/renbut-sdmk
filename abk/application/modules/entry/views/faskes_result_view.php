<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();


                echo "<h3><strong>{$sdmk_faskes_installasi['sdmk_faskes_installasi_nama']}</strong></h3>";
                echo "<h3>{$faskes_installasi['installasi_nama']} - {$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
                <hr/>
                <hr/>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/tambahResult_proses/<?php echo $sdmk_faskes_installasi_id ?>">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="panel panel-white no-radius text-center">
                                <div class="panel-body" style="min-height: 195px;">                                    
                                    <h3 class="StepTitle">Jumlah Saat Ini (PNS / Tenaga Tetap)</h3>
                                    
                                    <h1> <strong> <?php echo $sdmk_faskes_installasi['jumlah_PNS']  ?> </strong> </h1>
                                    <table style="text-align: left;"> 
                                        
                                        <tr><td>    Jumlah PPPK </td><td> &nbsp; : &nbsp; </td><td>  <b> <?php echo $sdmk_faskes_installasi['jumlah_PPPK']  ?> </b>  </td></tr>
                                     <tr><td> Jumlah PTT </td><td> &nbsp; : &nbsp; </td><td>  <b> <?php echo $sdmk_faskes_installasi['jumlah_PTT']  ?> </b>  </td></tr>
                                     <tr><td> Jumlah Honor Daerah </td><td> &nbsp; :  &nbsp;</td><td>  <b>  <?php echo $sdmk_faskes_installasi['jumlah_HD']  ?> </b>  </td></tr>
                                     <tr><td> Jumlah Lainnya </td><td> &nbsp; : &nbsp;</td><td>  <b> <?php echo $sdmk_faskes_installasi['jumlah_DLL']  ?> </b>  </td></tr>
                                    </table>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel panel-white no-radius text-center">
                                <div class="panel-body" style="min-height: 195px;">                                    
                                    <h3 class="StepTitle">Jumlah Seharusnya <br/>Berdasarkan ABK</h3>
                                    <p class="text-small">
                                    <h1> <strong> <?php echo $sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan'] ?> </strong> </h1>
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel panel-white no-radius text-center">
                                <div class="panel-body" style="min-height: 195px;">                                    
                                    <h3 class="StepTitle">Keterangan</h3>
                                    <p class="text-small">
                                    
                                    <?php 
									$selisih = $sdmk_faskes_installasi['jumlah_PNS']-$sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan'];
                                        if($selisih<0) {
                                            $keterangan = "<div class='alert alert-danger'><strong>KURANG<br/>".$selisih."</strong></div>";
                                        }
                                        elseif($selisih>0) {
                                            $keterangan = "<div class='alert alert-danger'><strong>LEBIH<br/>".$selisih."</strong></div>";
                                        }
                                        else {
                                            $keterangan="<div class='alert alert-success'><strong>SESUAI</strong></div>";
                                        }
                                    ?>
                                    <h4><?php echo $keterangan ?></h4>
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="text-align: center;">
                        <a class="btn btn-info btn-sm" type="button" name="DETIL" href="<?php echo base_url('index.php/entry/entry/detil_perhitungan_abk/'.$sdmk_faskes_installasi_id.'/true') ?>" ><i class="fa fa-file"></i> Detil Perhitungan  </a>
                        <a class="btn btn-success btn-sm" target="_blank" type="button" name="CETAK" href="<?php echo base_url('index.php/entry/entry/detil_perhitungan_abk_excel/'.$sdmk_faskes_installasi_id) ?>"><i class="fa fa-file-excel-o"></i> Cetak </a>
                        
                    </div>
                    
                    <br/>
                    <div class="form-group" style="text-align: right;">
                        <a class="btn btn-primary btn-sm" type="button" href="<?php echo base_url('index.php/entry/'.$class.'/tambahSDMK/'.$sdmk_faskes_installasi['faskes_installasi_id']) ?>"  ><i class="fa fa-plus"></i> Tambah SDMK Lainnya Pada <?php echo $faskes_installasi['installasi_nama']; ?> </a>
                        &nbsp;
                        <button class="btn btn-danger btn-flat" type="button" name="SELESAI" onclick="parent.window.location.replace('<?php echo base_url("index.php/entry/".$class."/get/".$faskes['faskes_kode']) ?>');"><i class="fa fa-check"></i> SELESAI </button>
                        &nbsp; &nbsp; &nbsp;&nbsp;
                    </div>
                </form>  
            </div>
        </div>
        <!-- end: YOUR CONTENT HERE -->
    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });
                            
                            
                            


    </script>
    
    


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
