<?php if (!defined('BASEPATH')) exit('no direct script user allowed');
$lang['set_as_publish'] = 'set as publish';
$lang['set_as_draft'] = 'set as draft';
$lang['page_type'] = 'page type';
$lang['this_page_to_manage_page_content,_see_documentation_for_use_it'] = 'This page to manage page content, see documentation for use it';