
<script type="text/javascript">

    $(function() {


        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#klinik-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#klinik-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_klinik+'">'+o.nama_klinik_kab+'</option>').appendTo($('#klinik-box'));
                    });

                });
            } else {
                $('#klinik-box option').remove()
                $('<option value="">(Pilih Unit Kerja)</option>').appendTo($('#klinik-box'));
            }
        });
    });
</script>

<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
  <form class="form-horizontal" method="post" id="form_prc_klinik" action="<?php echo site_url('perencanaan/tr_klinik/ajax_add')?>">
    <div class="row">
      <div class="col-xs-12">
                <br>

                <div class="form-group">

                <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value->tahun)?$value->tahun:'','tahun','tahun','form-control','required','inline');?>
                  </div>

                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value->id_perencanaan)?$value->id_perencanaan : '( Auto )'?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>

                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','10')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','4','10')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($value->id_provinsi) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','4','10')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','4','10')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_kabupaten(isset($value->id_kabupaten) ? $value->id_kabupaten : $this->session->userdata('data_user')->id_kabupaten ,'id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>

                  <label class="control-label col-md-2">Klinik</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','4','10')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_klinik(isset($value->kode_klinik) ? $value->kode_klinik : $this->session->userdata('data_user')->kode_klinik,'kode_klinik','klinik-box','form-control','required '.$readonly.'','inline');?>
                  </div>

                </div>
                <?php }?>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('utama','pratama');
                          foreach($katergori_kelas as $row_katergori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_katergori_kelas?>" <?php echo isset($value) ? ($value->kategori_kelas == $row_katergori_kelas) ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo strtoupper($row_katergori_kelas)?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>
                
                <div class="page-header">
                  <h1>
                    Bagian berikut diisi sesuai dengan kebutuhan dan jenis pelayanan yang diberikan
                  </h1>
                </div>
                
                <?php 
                  $arr_vf = '';
                  $value_form = $this->prc_klinik->get_value_form($value->id_perencanaan); //print_r($this->db->last_query());
                  foreach ($form_entry_klinik->result() as $key1 => $value_klinik) {
                    $arr_vf[]= ''.$value_klinik->id_jenis_sdmk.',';
                ?>
                <div class="form-group">
                  <label class="control-label col-md-2"><?php echo $value_klinik->nama_jenis_sdmk?></label>
                  <div class="col-md-2" style="padding-top:10px">
                    <input  name="value[]" id="jml_<?php echo $value_klinik->id_jenis_sdmk?>" value="<?php echo ($value_klinik->id_jenis_sdmk == $value_form[$key1]->id_jenis_sdmk) ? $value_form[$key1]->value : 0 ;?>" class="form-control" type="text">
                    <input  name="id_jenis_sdmk_form[]" value="<?php echo $value_klinik->id_jenis_sdmk?>" class="form-control" type="hidden">
                  </div>
                </div>
                <?php 
                  }
                  $array_vf = $arr_vf;
                ?>

      </div>
    </div>

    <?php echo $this->load->view('form_standar_sdmk');?>


    <div class="form-actions center">

    <!--hidden field-->
    <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_perencanaan:0?>"> -->

    <a onclick="getMenu('perencanaan/tr_klinik')" href="#" class="btn btn-sm btn-success">
      <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
      Kembali ke daftar
    </a>
    <a onclick="getMenu('perencanaan/tr_klinik/riwayat_perencanaan')" href="#" class="btn btn-sm btn-warning">
      <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
      Lihat Riwayat Perencanaan
    </a>
    <button type="reset" onclick="getMenu('perencanaan/tr_klinik')" id="btnReset" class="btn btn-sm btn-danger">
      <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
      Reset Form
    </button>
    <button type="submit" name="submit" id="btnSave2" value="save_methode" class="btn btn-sm btn-info">
      <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
      Submit
    </button>
  </div>

  </form>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url().'assets/js/custom/prc_klinik.js'?>"></script>
