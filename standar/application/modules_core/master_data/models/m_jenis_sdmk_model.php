<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_sdmk_model extends CI_Model {

	var $table = 'm_jenis_sdmk';
	var $column = array('m_jenis_sdmk.id_jenis_sdmk','m_jenis_sdmk.nama_jenis_sdmk','m_jenis_sdmk.counter','m_kelompok_sdmk.nama_kelompok_sdmk','m_jenis_faskes.nama_jenis_faskes','m_jenis_sdmk.active','m_jenis_sdmk.counter','m_jenis_sdmk.is_standar', 'm_jenis_sdmk.updated_date','m_jenis_sdmk.created_date',);
	var $select = 'm_jenis_sdmk.id_jenis_sdmk, m_jenis_sdmk.is_standar, m_jenis_sdmk.id_kelompok_sdmk, m_jenis_sdmk.id_jenis_faskes, m_jenis_sdmk.nama_jenis_sdmk,m_kelompok_sdmk.nama_kelompok_sdmk,m_jenis_faskes.nama_jenis_faskes, m_jenis_sdmk.active, m_jenis_sdmk.counter, m_jenis_sdmk.parent, m_jenis_sdmk.set_form, m_jenis_sdmk.updated_date,m_jenis_sdmk.created_date';

	var $order = array('m_jenis_sdmk.id_jenis_sdmk' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_kelompok_sdmk', ''.$this->table.'.id_kelompok_sdmk=m_kelompok_sdmk.id_kelompok_sdmk' , 'left' );
		$this->db->join('m_jenis_faskes', ''.$this->table.'.id_jenis_faskes=m_jenis_faskes.id_jenis_faskes' , 'left' );

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_kelompok_sdmk', ''.$this->table.'.id_kelompok_sdmk=m_kelompok_sdmk.id_kelompok_sdmk' , 'left' );
		$this->db->join('m_jenis_faskes', ''.$this->table.'.id_jenis_faskes=m_jenis_faskes.id_jenis_faskes' , 'left' );
		$this->db->where(''.$this->table.'.id_jenis_sdmk',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_by_faskes($id_jenis_faskes)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_kelompok_sdmk', ''.$this->table.'.id_kelompok_sdmk=m_kelompok_sdmk.id_kelompok_sdmk' , 'left' );
		$this->db->join('m_jenis_faskes', ''.$this->table.'.id_jenis_faskes=m_jenis_faskes.id_jenis_faskes' , 'left' );
		$this->db->where(array(''.$this->table.'.id_jenis_faskes' => $id_jenis_faskes, ''.$this->table.'.parent'=>0));
		$this->db->order_by(''.$this->table.'.counter', 'ASC');
		$parent = $this->db->get();

		//get jenis sdmk parent
		foreach ($parent->result() as $key => $row_parent) {
			# code...
			$sub_parent = $this->db->get_where($this->table, array('parent'=>$row_parent->id_jenis_sdmk))->result();
			$row_parent->sub_parent = $sub_parent;
			$getData[] = $row_parent;
		}
		return $getData;
	}

	public function get_sdmk_standar($params)
	{
		//print_r($params);die;
		$parent = $this->query_get_sdmk_standar($params, array(''.$this->table.'.parent'=>0)); 
		//print_r($this->db->last_query());die;
		//echo '<pre>';print_r($parent->result());die;

		//get jenis sdmk parent
		foreach ($parent->result() as $key => $row_parent) {
			# code...
			//$sub_parent = $this->db->get_where($this->table, array('parent'=>$row_parent->id_jenis_sdmk))->result();
			$sub_parent = $this->query_get_sdmk_standar( $params, array('parent'=>$row_parent->id_jenis_sdmk) ); 
			$row_parent->sub_parent = $sub_parent->result();
			$getData[] = $row_parent;
		}
		return $getData;
	}

	public function get_value_form_id_jenis_sdmk($id_jenis_sdmk, $id_perencanaan)
	{
		$this->db->from('t_detail_perencanaan_form'); 
		$this->db->where(array('id_perencanaan'=>$id_perencanaan, 'id_jenis_sdmk'=>$id_jenis_sdmk));
		return $this->db->get()->row();
	}

	public function query_get_sdmk_standar($params, $where){

		
		// SELECT QUERY //
		if($params['id_jenis_faskes'] == 3){
			$this->db->select(''.$this->table.'.*,standar.'.$params['kategori_kelas'].' as jml_standar, detail.jml_pns,detail.jml_pppk,detail.jml_ptt, detail.jml_honorer, detail.jml_blud, detail.jml_tks, detail.total_standar,detail.total_jml, detail_form.value as jml_standar_form');	
		}elseif(in_array($params['id_jenis_faskes'], array(5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,32,33,34,35) )) {
			$this->db->select(''.$this->table.'.*,standar.'.$params['kategori_kelas'].' as jml_standar, detail.jml_pns,detail.jml_pppk,detail.jml_ptt, detail.jml_honorer, detail.jml_blud, detail.jml_tks, detail.total_standar,detail.total_jml, detail_form.value as jml_standar_form');	
		}else if($params['id_jenis_faskes'] == 30 || $params['id_jenis_faskes'] == 29 || $params['id_jenis_faskes'] == 31){
			$this->db->select(''.$this->table.'.*,standar.'.$params['kategori_kelas'].' as jml_standar, detail.jml_pns,detail.jml_pppk,detail.jml_ptt, detail.jml_honorer, detail.jml_blud, detail.jml_tks, detail.total_standar,detail.total_jml, detail_form.value as jml_standar_form');	
		}else{
			$where_tipe_kawasan = ( $params['id_tipe_kawasan'] != 0 ) ? 'AND id_tipe_kawasan='.$params['id_tipe_kawasan'].'' : '';
			$where_tipe_puskesmas = ( $params['id_tipe_puskesmas'] != 0 ) ? 'AND id_tipe_puskesmas='.$params['id_tipe_puskesmas'].'' : '';

			$this->db->select(''.$this->table.'.*,standar.jml_standar, detail.jml_pns,detail.jml_pppk,detail.jml_ptt, detail.jml_honorer, detail.jml_blud, detail.jml_tks, detail.total_standar,detail.total_jml');
		}
		
		$this->db->from($this->table);
		$this->db->join('m_kelompok_sdmk', ''.$this->table.'.id_kelompok_sdmk=m_kelompok_sdmk.id_kelompok_sdmk' , 'left' );
		$this->db->join('m_jenis_faskes', ''.$this->table.'.id_jenis_faskes=m_jenis_faskes.id_jenis_faskes' , 'left' );
		
		// JOIN QUERY //
		if($params['id_jenis_faskes'] == 3){
			$this->db->join('(SELECT * FROM m_standar_rsu group by id_jenis_sdmk) AS standar ', ''.$this->table.'.id_jenis_sdmk=standar.id_jenis_sdmk' , 'left' );	
		}elseif( in_array($params['id_jenis_faskes'], array(5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,32,33,34,35)) ){
			$this->db->join('(SELECT * FROM m_standar_rsk group by id_jenis_sdmk) AS standar ', ''.$this->table.'.id_jenis_sdmk=standar.id_jenis_sdmk' , 'left' );	
		}else if($params['id_jenis_faskes'] == 30){
			$this->db->join('(SELECT * FROM m_standar_kkp group by id_jenis_sdmk) AS standar ', ''.$this->table.'.id_jenis_sdmk=standar.id_jenis_sdmk' , 'left' );	
		}else if($params['id_jenis_faskes'] == 29){
			$this->db->join('(SELECT * FROM m_standar_btklpp group by id_jenis_sdmk) AS standar ', ''.$this->table.'.id_jenis_sdmk=standar.id_jenis_sdmk' , 'left' );	
		}else if($params['id_jenis_faskes'] == 31){
			$this->db->join('(SELECT * FROM m_standar_klinik group by id_jenis_sdmk) AS standar ', ''.$this->table.'.id_jenis_sdmk=standar.id_jenis_sdmk' , 'left' );	
		}else{
			$this->db->join('(SELECT * FROM m_standar WHERE id_jenis_faskes='.$params['id_jenis_faskes'].' '.$where_tipe_puskesmas.' '.$where_tipe_kawasan.') AS standar ', ''.$this->table.'.id_jenis_sdmk=standar.id_jenis_sdmk' , 'left' );
		}
		
		if(in_array($params['id_jenis_faskes'], array(3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,32,33,34,35))){
			if(isset($params['id_perencanaan'])){
				$this->db->join('(SELECT * FROM t_detail_perencanaan WHERE id_perencanaan='.$params['id_perencanaan'].') AS detail ', ''.$this->table.'.id_jenis_sdmk=detail.id_jenis_sdmk' , 'left' );

				$this->db->join('(SELECT * FROM t_detail_perencanaan_form WHERE id_perencanaan='.$params['id_perencanaan'].') AS detail_form ', ''.$this->table.'.id_jenis_sdmk=detail_form.id_jenis_sdmk' , 'left' );
			}else{
				$this->db->join('(SELECT t_detail_perencanaan.* FROM t_detail_perencanaan LEFT JOIN t_perencanaan ON t_perencanaan.`id_perencanaan`=t_detail_perencanaan.`id_perencanaan` WHERE t_perencanaan.`tahun`='.$params['tahun'].' GROUP BY t_detail_perencanaan.id_jenis_sdmk) AS detail ', ''.$this->table.'.id_jenis_sdmk=detail.id_jenis_sdmk' , 'left' );
				
				$this->db->join('(SELECT t_detail_perencanaan_form.* FROM t_detail_perencanaan_form LEFT JOIN t_perencanaan ON t_perencanaan.`id_perencanaan`=t_detail_perencanaan_form.`id_perencanaan` WHERE t_perencanaan.`tahun`='.$params['tahun'].' GROUP BY t_detail_perencanaan_form.id_jenis_sdmk) AS detail_form ', ''.$this->table.'.id_jenis_sdmk=detail.id_jenis_sdmk' , 'left' );
			}
		}else{
			if(isset($params['id_perencanaan'])){
				$this->db->join('(SELECT * FROM t_detail_perencanaan WHERE id_perencanaan='.$params['id_perencanaan'].') AS detail ', ''.$this->table.'.id_jenis_sdmk=detail.id_jenis_sdmk' , 'left' );
			}else{
				$this->db->join('(SELECT t_detail_perencanaan.* FROM t_detail_perencanaan LEFT JOIN t_perencanaan ON t_perencanaan.`id_perencanaan`=t_detail_perencanaan.`id_perencanaan` WHERE t_perencanaan.`tahun`='.$params['tahun'].' GROUP BY t_detail_perencanaan.id_jenis_sdmk) AS detail ', ''.$this->table.'.id_jenis_sdmk=detail.id_jenis_sdmk' , 'left' );
			}
		}
		
		
		$this->db->where(''.$this->table.'.id_jenis_faskes', $params['id_jenis_faskes']);
		$this->db->where($where);
		$this->db->where(''.$this->table.'.active', 'Y');
		$this->db->group_by(''.$this->table.'.id_jenis_sdmk');
		$this->db->order_by(''.$this->table.'.counter', 'ASC');

		return $this->db->get();

	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		// m_jenis_sdmk
		$this->db->delete($this->table, array(''.$this->table.'.id_jenis_sdmk'=>$id));
		// standar puskesmas //
		$this->db->delete('m_standar', array('id_jenis_sdmk'=>$id));
		$this->db->delete('m_standar_klinik', array('id_jenis_sdmk'=>$id));
		$this->db->delete('m_standar_kkp', array('id_jenis_sdmk'=>$id));
		$this->db->delete('m_standar_rsk', array('id_jenis_sdmk'=>$id));
		$this->db->delete('m_standar_rsu', array('id_jenis_sdmk'=>$id));
		$this->db->delete('m_standar_btklpp', array('id_jenis_sdmk'=>$id));
		$this->db->delete('t_detail_perencanaan', array('id_jenis_sdmk'=>$id));
		$this->db->delete('t_detail_perencanaan_form', array('id_jenis_sdmk'=>$id));
		
	}


}
