<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?> 
<div class="left"> 

</div>
<div class="mid-logo"></div>
<div class="" style="float: left; color: #333;line-height: 17px;margin-top: 12px;">
    <span style="font-size: 15px;">BPPSDM Kementrian Kesehatan RI</span><br/>
    <span style="font-size: 12px;">Rencanan Kebutuhan SDMK Menggunakan Metode Proyeksi Penduduk</span>
</div>
<div class="right"> 
    <?php
    $id = $this->session->get_data('session_user_id');
    $rw = $this->db->select('fullname,photo')
                    ->from($this->db->prefix.'user')
                    ->where(array('id' => $id))
                    ->get()->row();
    $pp = is_url($rw->photo) ? $rw->photo : site_url($rw->photo);
    $name = character_limiter($rw->fullname, 8);
    ?>
    <span class="xamzo-userinfo-link" dropdown="true" dropdown-relative="userinfo" dropdown-target="user-info" dropdown-scroll="true">
        <span class="pp-small"> <img src="<?php echo $pp ?>" alt=""></span>
        <?php echo $name ?> &nbsp; &nbsp;<i class="fa fa-angle-down"></i> </span> 
    <ul class="dropdown-target dropdown-target-right user-info">
        <div class="arrow-up"></div>
        <div class="xamzo-avatar">
            <form id="pp-upload" method="post" action="<?php echo site_url('user/pp_upload') ?>" enctype="multipart/form-data">
                <div id="pp-drop"> 
                    <a href="javascript:void(0)"><img src="<?php echo $pp ?>" alt=""></a>
                    <input type="file" name="userpp" multiple />
                </div>  
            </form>
        </div>
        <div class="xamzo-userinfo">
            <li class="sparated-link">
                <a  href="#" ><b><?php echo $name ?></b></a>
            </li>
            <li>
                <a ajax="true" href="#!user/edit_profile" ajax-target="modalbox" modal-width="350px" modal-fix="false"><i class="fa fa-angle-right"></i> <?php echo $this->lang['edit_profile'] ?></a>
            </li>
            <li>
                <a ajax="true" href="#!user/edit_password" ajax-target="modalbox" modal-width="350px" modal-fix="true"><i class="fa fa-angle-right"></i> <?php echo $this->lang['change_password'] ?></a>
            </li> 
            <li>
                <form id="crud-logout" style="display: none;">
                    <input type="hidden" name="logout">
                </form>
                <a href="#!user/logout/" ajax="true" ajax-data="#crud-logout" ajax-target=".ca-right" ajax-type="POST"><i class="fa fa-angle-right"></i> <?php echo $this->lang['logout']; ?></a>
            </li>
        </div>
    </ul>  
</div>

<script type="text/javascript">

    $(function () {
        var d = new Date();
        var id = d.getTime();
        var ul = $('#pp-drop a');

        $('#pp-drop a').click(function () {
            // Simulate a click on the file input button
            // to show the file browser dialog 
            $(this).parent().find('input').click();
        });

        // Initialize the jQuery File Upload plugin
        $('#pp-upload').fileupload({
            // This element will accept file drag/drop uploading
            dropZone: $('#pp-drop'),
            limitMultiFileUploads: '1',
            dataType: 'json',
            submit: function (e, data) {
                var ext = data.files[0].type;
                if (!ext.match(/jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF/gi)) {
                    $('li#' + id).remove();
                    notive('Only jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF files are allowed');
                    return false;
                    data.stop();

                } else {
                    return true;
                }
            },
            // This function is called when a file is added to the queue;
            // either via the browse button, or via drag/drop:
            add: function (e, data) {
                var tpl = $('<li id="' + id + '" style="padding:20px 0; text-align:center"><input type="text" value="0" data-width="48" data-height="48"' +
                        ' data-fgColor="#5E5408" data-readOnly="1" data-bgColor="#4B4200" /><p></p><span></span></li>');

                // Add the HTML to the UL element
                data.context = tpl.appendTo(ul);

                // Initialize the knob plugin
                tpl.find('input').knob();

                // Listen for clicks on the cancel icon
                tpl.find('span').click(function () {

                    if (tpl.attr('id', id)) {
                        ppXHR.abort();
                    }

                    tpl.fadeOut(function () {
                        tpl.remove();
                    });

                });

                // Automatically upload the file once it is added to the queue
                var ppXHR = data.submit();
            },
            progress: function (e, data) {

                // Calculate the completion percentage of the upload
                var progress = parseInt(data.loaded / data.total * 100, 10);

                // Update the hidden input field and trigger a change
                // so that the jQuery knob plugin knows to update the dial
                data.context.find('input').val(progress).change();

                if (progress === 100) {
                    $('li#' + id).remove();
                }
            },
            fail: function (e, data) {
                // Something has gone wrong!
                data.context.addClass('error');
            },
            success: function (rs) {
                if (rs.status === 'success') {
                    $.ajax({
                        dataType: "html",
                        url: base_ + "user/pp_thumb/" + rs.file,
                        success: function (rs) {
                            ul.find('img').attr('src', rs);
                            $('.pp-small').find('img').attr('src', rs);
                        }
                    });
                }
            }

        });


        // Prevent the default action when a file is dropped on the window
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

    });

</script>    