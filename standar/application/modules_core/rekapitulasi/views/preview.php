<?
    if( $type=='excel' ){

      $filename = "EXP-".str_replace('-','', Date("Y-m-d")).".xls";
      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=$filename");
      header('Cache-Control: public');

    }
    
?>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
<!-- ace styles -->
<!-- bootstrap & fontawesome -->

<body>

  <div class="row">
    <div class="col-xs-12">

      <!-- PAGE CONTENT -->

      <center>
          <strong>HASIL LAPORAN PERENCANAAN KEBUTUHAN SDM KESEHATAN<br>
          <?php echo 'TAHUN '.$value->tahun;?>
          </strong>
      </center>
          <br>
      <table border="1" style="width: 100%; border: 1px; border-color: white">
       <thead>
        <tr style="background-color: #088A08; color: white;">  
          <th align="center" rowspan="2">&nbsp;</th>
          <th class="center" rowspan="2">Jenis SDMK</th>
          <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
          <th class="center" rowspan="2">SDMK Standar</th>
          <th class="center" rowspan="2">Kesenjangan<br> (9) - (10)</th>
        </tr>

        <tr style="background-color: #088A08; color: white;">
          <th align="center">PNS/<br>Pegawai Tetap</th>
          <th align="center">PPPK</th>
          <th align="center">PTT</th>
          <th align="center">Honorer/<br>Kontrak</th>
          <th align="center">BLUD</th>
          <th align="center">TKS</th>
          <th align="center">Total</th>
        </tr>
      </thead>

      <tbody>
        <tr style="background-color: #3ADF00; color: white;">
          <?php for($i=1;$i<12;$i++){?>
          <td align="center"><?php echo '<b>( '.$i.' )</b>'?></td>
          <?php }?>
        </tr>

        <?php 
            $no = 1;
            if( count($value) != 0 ){
             //echo '<pre>';print_r($jenis_sdmk);die;

            foreach($jenis_sdmk as $key=>$row_jenis_sdmk){
              
            // jika jenis sdmk memiliki sub, maka form input disabled
            $readonly = ( count($row_jenis_sdmk->sub_parent) == 0) ? '' : 'readonly' ;
            
            // jika jenis sdmk tidak memiliki standar minimal maka diblok warna merah //
            $label = ($row_jenis_sdmk->is_standar == 'N') ? 'style="background-color:#F8E0E0;"' : '' ;
              

            // jumlah standar puskesmas //
            if($row_jenis_sdmk->id_jenis_sdmk == '10420'){ // perawat pustu
              $jml_standar = $value->jumlah_pustu;
            }elseif ($row_jenis_sdmk->id_jenis_sdmk == '10410') { // bidan desa
              $jml_standar = $value->jumlah_desa;
            }else{
              $jml_standar = $row_jenis_sdmk->jml_standar;
            }
          
        ?>
            <tr <?php echo isset($label)?$label:''?>>
              <td align="center"> <?php echo ''.$no.'.'?> </td>
              <td> <?php echo $row_jenis_sdmk->nama_jenis_sdmk?> <input type="hidden" name="id_jenis_sdmk[]" value="<?php echo $row_jenis_sdmk->id_jenis_sdmk?>"> </td>
              <td align="center"><?php echo isset($row_jenis_sdmk->jml_pns)?$row_jenis_sdmk->jml_pns:0?> </td>
              <td align="center"><?php echo isset($row_jenis_sdmk->jml_pppk)?$row_jenis_sdmk->jml_pppk:0?> </td>
              <td align="center"><?php echo isset($row_jenis_sdmk->jml_ptt)?$row_jenis_sdmk->jml_ptt:0?> </td>
              <td align="center"><?php echo isset($row_jenis_sdmk->jml_honorer)?$row_jenis_sdmk->jml_honorer:0?> </td>
              <td align="center"><?php echo isset($row_jenis_sdmk->jml_blud)?$row_jenis_sdmk->jml_blud:0?> </td>
              <td align="center"><?php echo isset($row_jenis_sdmk->jml_tks)?$row_jenis_sdmk->jml_tks:0?> </td>
              <td align="center"><?php echo isset($row_jenis_sdmk->total_jml)?$row_jenis_sdmk->total_jml:0?> </td>
              <td align="center"> <input type="hidden" value="<?php echo $jml_standar?>" name="jml_standar[]"> <?php echo $jml_standar?> </td>
              <td align="center"> <?php echo Apps::counting('-', $row_jenis_sdmk->total_jml, $jml_standar)?></td>
            </tr>
              <?php $alp = 'a'; foreach($row_jenis_sdmk->sub_parent as $key2=>$row_sub_parent){
                $label_sub = ($row_sub_parent->is_standar == 'N') ? 'style="background-color:#F8E0E0;"' : '' ;
                ?>
              <tr <?php echo isset($label_sub)?$label_sub:''?>>
                <td> &nbsp; </td>
                <td> <?php echo '&nbsp;&nbsp;&nbsp;&nbsp;'.$alp.'.&nbsp; '.$row_sub_parent->nama_jenis_sdmk.''?> <input type="hidden" name="id_jenis_sdmk_sub[]" value="<?php echo $row_sub_parent->id_jenis_sdmk?>"> </td>
                <td align="center"> <?php echo isset($row_sub_parent->jml_pns)?$row_sub_parent->jml_pns:0?> </td>
                <td align="center"> <?php echo isset($row_sub_parent->jml_pppk)?$row_sub_parent->jml_pppk:0?> </td>
                <td align="center"> <?php echo isset($row_sub_parent->jml_ptt)?$row_sub_parent->jml_ptt:0?> </td>
                <td align="center"> <?php echo isset($row_sub_parent->jml_honorer)?$row_sub_parent->jml_honorer:0?> </td>
                <td align="center"> <?php echo isset($row_sub_parent->jml_blud)?$row_sub_parent->jml_blud:0?> </td>
                <td align="center"> <?php echo isset($row_sub_parent->jml_tks)?$row_sub_parent->jml_tks:0?> </td>
                <td align="center"> <?php echo isset($row_sub_parent->total_jml)?$row_sub_parent->total_jml:0?> </td>
                <td align="center"> -</td>
                <td align="center"> - </td>
              </tr>
              <?php 
                  $alp++;
                  /*$jml_pns_all_sub[] = $row_sub_parent->jml_pns; 
                  $jml_pppk_all_sub[] = $row_sub_parent->jml_pppk; 
                  $jml_ptt_all_sub[] = $row_sub_parent->jml_ptt; 
                  $jml_honorer_all_sub[] = $row_sub_parent->jml_honorer; 
                  $jml_blud_all_sub[] = $row_sub_parent->jml_blud; 
                  $jml_tks_all_sub[] = $row_sub_parent->jml_tks; 
                  $jml_total_all_sub[] = $row_sub_parent->total_jml; 
                  $jml_standar_all_sub[] = $jml_standar;*/ 
                }
              ?>
        <?php 
              $no++; 
              $jml_pns_all[] = $row_jenis_sdmk->jml_pns;
              $jml_pppk_all[] = $row_jenis_sdmk->jml_pppk;
              $jml_ptt_all[] = $row_jenis_sdmk->jml_ptt;
              $jml_honorer_all[] = $row_jenis_sdmk->jml_honorer;
              $jml_blud_all[] = $row_jenis_sdmk->jml_blud;
              $jml_tks_all[] = $row_jenis_sdmk->jml_tks;
              $jml_total_all[] = $row_jenis_sdmk->total_jml;
              $jml_standar_all[] = $jml_standar;
              
            } 
          } else {?>

          <div class="alert alert-block alert-danger">
                  [ <i class="ace-icon fa fa-info"></i> ]<strong class="red">Maaf</strong>,
                  Tidak ada data yang ditemukan, <a href="javascript:void()" onclick="getMenu('rekapitulasi/form_rekapitulasi/<?php echo $id_jenis_faskes?>')"> Kembali ke Rekapitulasi </a>
                   
          </div>

        <?php }?>

        <tr style="background-color: black; color:white">
          <td align="center" colspan="2"><b>TOTAL</b></td>

            <td align="center" id=""> <?php echo $ttl_pns = array_sum($jml_pns_all);?> </td>
            <td align="center" id=""> <?php echo $ttl_pppk = array_sum($jml_pppk_all); ?> </td>
            <td align="center" id=""> <?php echo $ttl_ptt = array_sum($jml_ptt_all); ?> </td>
            <td align="center" id=""> <?php echo $ttl_honorer = array_sum($jml_honorer_all); ?> </td>
            <td align="center" id=""> <?php echo $ttl_blud = array_sum($jml_blud_all); ?> </td>
            <td align="center" id=""> <?php echo $ttl_tks = array_sum($jml_tks_all); ?> </td>
            <td align="center" id=""> <?php echo $ttl_jml = array_sum($jml_total_all); ?> </td>
            <td align="center" id=""> <?php echo $ttl_standar = array_sum($jml_standar_all); ?> </td>
            <td align="center" id=""> <?php echo Apps::counting('-', $ttl_jml, $ttl_standar)?> </td>
            
        </tr>

      </tbody>
    </table>
      <!-- PAGE CONTENT ENDS -->
    
    </div><!-- /.col -->
  </div><!-- /.row -->
</body>
</html>>
