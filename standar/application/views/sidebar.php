<div class="sidebar-shortcuts" id="sidebar-shortcuts">
    <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
        <button class="btn btn-success">
            <i class="ace-icon fa fa-signal"></i>
        </button>

        <button class="btn btn-info">
            <i class="ace-icon fa fa-pencil"></i>
        </button>

        <!-- #section:basics/sidebar.layout.shortcuts -->
        <button class="btn btn-warning">
            <i class="ace-icon fa fa-users"></i>
        </button>

        <button class="btn btn-danger">
            <i class="ace-icon fa fa-cogs"></i>
        </button>

        <!-- /section:basics/sidebar.layout.shortcuts -->
    </div>

    <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
        <span class="btn btn-success"></span>

        <span class="btn btn-info"></span>

        <span class="btn btn-warning"></span>

        <span class="btn btn-danger"></span>
    </div>
</div><!-- /.sidebar-shortcuts -->

<ul class="nav nav-list">

    <!-- <li class="active">
        <a href="<?php echo base_url()?>">
            <i class="menu-icon fa fa-tachometer"></i>
            <span class="menu-text"> Dashboard </span>
        </a>

        <b class="arrow"></b>
    </li> -->

    <!-- <li class="">
        <a href="#" onclick="getMenu('set_menu')">
            <i class="menu-icon fa fa-picture-o"></i>
            <span class="menu-text"> Gallery </span>
        </a>

        <b class="arrow"></b>
    </li> -->

    <?php
        $menu = Lib_menus::get_menus(); //echo'<pre>';print_r($menu);die;
        if(count($menu) > 0){
        foreach ($menu as $key => $value) {
            $string_link = ''.$value['link'].'';
    ?>

    <li class="">
        <a href="#" <?php echo ( $value['link'] == '#' ) ? 'class="dropdown-toggle"' : '' ;?>  <?php if( $value['link'] != '#' ){?> onclick="getMenu('<?php echo $value['link']?>')" <?php }?>>
            <i class="<?php echo $value['icon']?>"></i>
            <span class="menu-text"> <?php echo $value['name']?> </span>
            <?php echo ( $value['link'] == '#' ) ? '<b class="arrow fa fa-angle-down"></b>' : '' ;?>
        </a>

        <b class="arrow"></b>
        <?php if( count($value['submenu']) != 0 ){?>
        <ul class="submenu">
            <?php foreach($value['submenu'] as $row_sub_menu){ ?>
            <li class="">
                <a href="#" onclick="getMenu('<?php echo $row_sub_menu['link']?>')">
                    <i class="menu-icon fa fa-caret-right"></i>
                    <?php echo $row_sub_menu['name']?>
                </a>

                <b class="arrow"></b>
            </li>
            <?php }?>
        </ul>
        <?php }?>

    </li>

    <?php } }?>

    <!-- <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-tag"></i>
            <span class="menu-text"> Pengaturan </span>

            <b class="arrow fa fa-angle-down"></b>
        </a>

        <b class="arrow"></b>

        <ul class="submenu">
            <li class="">
                <a href="#" onclick="getMenu('pengaturan/aplikasi')">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Aplikasi
                </a>

                <b class="arrow"></b>
            </li>

            <li class="">
                <a href="#" onclick="getMenu('pengaturan/user')">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Pengguna
                </a>

                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="#" onclick="getMenu('pengaturan/act_function')">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Fungsi Aksi
                </a>

                <b class="arrow"></b>
            </li>

             <li class="">
                <a href="#" onclick="getMenu('pengaturan/role')">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Role
                </a>

                <b class="arrow"></b>
            </li>
            <li class="">
                <a href="#" onclick="getMenu('pengaturan/menu')">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Menu
                </a>

                <b class="arrow"></b>
            </li>

            <li class="">
                <a href="#" onclick="getMenu('pengaturan/menu_role')">
                    <i class="menu-icon fa fa-caret-right"></i>
                    Menu Role
                </a>

                <b class="arrow"></b>
            </li>

            
        </ul>
    </li> -->
    <li>
        <a href="<?php echo base_url().'login/logout'?>">
            <i class="menu-icon fa fa-power-off"></i>
            <span class="menu-text"> Logout </span>
        </a>
        <b class="arrow"></b>
    </li>

    
</ul><!-- /.nav-list -->
<script src="<?php echo $js?>/custom/menu_load_page.js"></script>