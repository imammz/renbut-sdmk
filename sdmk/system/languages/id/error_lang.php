<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang["sorry,_that_page_doesn't_exist"] = "Maaf, Halaman ini Tidak tersedia";
$lang['the_link_you_followed_may_be_broken,_or_the_page_may_have_been_removed'] = 'Link yang Anda ikuti mungkin rusak, atau halaman mungkin telah dihapus';
$lang["sorry,_can't_open_this_content"] = "Maaf, Tidak dapat membuka konten ini";
$lang['you_does_not_have_access_rights_to_the_content_so_server_is_rejecting_to_give_proper_response'] = 'Anda tidak memiliki hak akses ke konten sehingga server menolak untuk memberikan respon yang tepat';