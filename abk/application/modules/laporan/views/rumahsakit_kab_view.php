<html>
    <head>
        <style>
           * { 
	margin: 0; 
	padding: 0; 
}
body { 
	font: 14px/1.4 Georgia, Serif; 
}
#page-wrap {
	margin: 50px;
}
p {
	margin: 20px 0; 
}

	/* 
	Generic Styling, for Desktops/Laptops 
	*/
	table { 
		width: 100%; 
		border-collapse: collapse; 
	}
	/* Zebra striping */
	tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #333; 
		color: white; 
		font-weight: bold; 
	}
	td, th { 
		padding: 6px; 
		border: 1px solid #ccc; 
		text-align: left; 
	}
        </style>

    </head>

    <body>

        <h2> Rekapitulasi Kebutuhan Tenaga Kesehatan Faskes Rumah Sakit Dengan Metode ABK</h2>
        <h1><?php echo 'Kab/Kota ' . $NAMA_KAB . ' - Prov ' . $NAMA_PROV ?></h1>
        <h1><?php echo 'TAHUN ' . $TAHUN ?></h1>
<div id="page-wrap">
        <table>
            <thead>

                <tr>
                    <th rowspan="4">No</th>        
                    <th rowspan="2" style="min-width: 500px;">Kelompok Dan Jenis SDMK</th>

                    <?php
                    $no = 1;
                    foreach ($ref_jenis_sdmk_level1 as $row) {
                        $ref_jenis_sdmk2 = $this->orm->ref_jenis_sdmk->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode)->and('ref_jenis_sdmk_nama_level1', $row['ref_jenis_sdmk_nama_level1'])->order('ref_jenis_sdmk_kode');
                        ?>
                        <th rowspan="1" colspan="<?php echo 4 * COUNT($ref_jenis_sdmk2); ?>"> <?php echo $no . '. ' . strtoupper($row['ref_jenis_sdmk_nama_level1']); ?> <br/><br/> </th>   
                        <?php $no++;
                    }
                    ?>
                </tr>

                <tr>

                <tr>
                    <th rowspan="2">Rumah Sakit</th>
                    <?php foreach ($ref_jenis_sdmk as $row) { ?>
                        <th rowspan="1" colspan="4"> <?php echo strtoupper($row['ref_jenis_sdmk_nama_level2']) ?> </th> 
<?php } ?>
                </tr>
                <tr style="font-size: smaller;">
                    <?php for ($i = 1; $i <= COUNT($ref_jenis_sdmk); $i++) { ?>
                        <th rowspan="1" colspan="1" style="min-width: 100px;">Jumlah SDMK Saat Ini (PNS / Tenaga Tetap) <br/> <sup>(A)</sup></th> <th>Jumlah SDMK Seharusnya <br/> <sup>(B)</sup> </th> <th>Kesenjangan <br/> <sup>(A)-(B)</sup></th><th>Keadaan <br/> <b>K / S / L</b></th>
<?php } ?>
                </tr>
                <tr>
                    <td style="text-align: center"><b>[1]</b></td>
                    <td style="text-align: center"><b>[2]</b></td>
                    <?php for ($i = 3; $i <= COUNT($ref_jenis_sdmk) * 4 + 2; $i++) { ?>
                        <td style="text-align: center"><b>[<?php echo $i; ?>]</b></td>
<?php } ?>
                </tr>

                
 <?php $no1 = 1;
foreach ($this->orm->faskes->where('NO_KAB = ? AND tahun = ? AND LEFT(ref_jenis_faskes_kode,1) = ?', $NO_KAB, $TAHUN, 2) as $row_faskes) { ?>
                     
       <tr>
           <td style="min-width: 40px;"><b><?php echo $no1; ?></b></td>
                        <td><b><?php echo $row_faskes['faskes_nama'] ?></b></td>  
                         <?php
                        foreach ($ref_jenis_sdmk as $row_sdmk) {
                            $get_faskes_installasi = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode', $row_faskes['faskes_kode']);
                            $faskes_jumlah = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as saat_ini, SUM(hasil_kebutuhan_sdmk_pembulatan) as seharusnya ')->where('faskes_installasi_id', $get_faskes_installasi)->and('ref_jenis_sdmk_kode', $row_sdmk['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $faskes_jumlah['saat_ini'];
                            $kesenjangan = $jumlah_saat_ini - $faskes_jumlah['seharusnya'];

                            if ($kesenjangan == 0) {
                                $keadaan = '<span style="color:green;">S</span>';
                            } elseif ($kesenjangan <= 0) {
                                $keadaan = '<span style="color:red;">K</span>';
                            } elseif ($kesenjangan >= 0) {
                                $keadaan = '<span style="color:red;">L</span>';
                            }

                            ?>    
                            <td style="text-align: center; font-weight: bolder;"><?php echo ($jumlah_saat_ini == '') ? 0 : $jumlah_saat_ini; ?></td>
                            <td style="text-align: center; font-weight: bolder;"><?php echo ($faskes_jumlah['seharusnya'] == '') ? 0 : $faskes_jumlah['seharusnya']; ?></td>
                            <td style="text-align: center; font-weight: bolder;"><?php echo $kesenjangan; ?></td>
                            <td style="text-align: center; font-weight: bolder;"><b><?php echo $keadaan; ?></b></td>
    <?php } ?> 
       </tr>             
                
<?php $no = 1;
foreach ($this->orm->faskes_installasi->where('faskes_kode',$row_faskes['faskes_kode']) as $row) { ?>
                    <tr>
                        <td style="min-width: 40px;"><?php echo $no1.'.'.$no ?></td>
                        <td><?php echo $row['installasi_nama'] ?></td>
                        <?php
                        foreach ($ref_jenis_sdmk as $row_sdmk) {
                            $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $row['faskes_installasi_id'])->and('ref_jenis_sdmk_kode', $row_sdmk['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'];
                            $kesenjangan = $jumlah_saat_ini - $sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan'];

                            if ($kesenjangan == 0) {
                                $keadaan = '<span style="color:green; font-size:smaller;">S</span>';
                            } elseif ($kesenjangan <= 0) {
                                $keadaan = '<span style="color:red; font-size:smaller;">K</span>';
                            } elseif ($kesenjangan >= 0) {
                                $keadaan = '<span style="color:red; font-size:smaller;">L</span>';
                            }

                            ?>    
                            <td style="text-align: center; color: #558; font-size:smaller;"><?php echo ($jumlah_saat_ini == '') ? 0 : $jumlah_saat_ini; ?></td>
                            <td style="text-align: center; color: #558; font-size:smaller;"><?php echo ($sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan'] == '') ? 0 : $sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan']; ?></td>
                            <td style="text-align: center; color: #558; font-size:smaller;"><?php echo $kesenjangan; ?></td>
                            <td style="text-align: center;"><b><?php echo $keadaan; ?></b></td>
    <?php } ?>   
                    </tr>
                        <?php $no++;
} $no1++; }?>
                <tr>
                    <td colspan="2"><h4><?php echo 'Kab/Kota ' . $NAMA_KAB . ' - Prov ' . $NAMA_PROV ?></h4>
                        <h4><?php echo 'Tahun '.$TAHUN ?></h4>
                    </td>
                    <?php

                    foreach ($ref_jenis_sdmk as $row_sdmk) {


                            //$total_jumlah_saat_ini = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS+jumlah_PPPK+jumlah_PTT+jumlah_HD+jumlah_DLL) as total')->where('faskes_installasi_id', $faskes_installasi)->and('ref_jenis_sdmk_kode', $row_sdmk['ref_jenis_sdmk_kode'])->fetch();
                            $total_jumlah_saat_ini = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as total')->where('faskes_installasi_id', $faskes_installasi)->and('ref_jenis_sdmk_kode', $row_sdmk['ref_jenis_sdmk_kode'])->fetch();
                            $hasil_kebutuhan_sdmk_pembulatan = $this->orm->sdmk_faskes_installasi->select('SUM(hasil_kebutuhan_sdmk_pembulatan) as total')->where('faskes_installasi_id', $faskes_installasi)->and('ref_jenis_sdmk_kode', $row_sdmk['ref_jenis_sdmk_kode'])->fetch();
                            
                            $total_kesenjangan = $total_jumlah_saat_ini['total']-$hasil_kebutuhan_sdmk_pembulatan['total'];
                            if ($total_kesenjangan == 0) {
                                $keadaan = '<span style="color:green;">S</span>';
                            } elseif ($total_kesenjangan <= 0) {
                                $keadaan = '<span style="color:red;">K</span>';
                            } elseif ($total_kesenjangan >= 0) {
                                $keadaan = '<span style="color:red;">L</span>';
                            }
                        
                       
                        ?>
                        <td style="text-align: center; font-weight: bolder;"><?php echo $total_jumlah_saat_ini['total']; ?></td>
                        <td style="text-align: center; font-weight: bolder;"><?php echo $hasil_kebutuhan_sdmk_pembulatan['total']; ?></td>
                        <td style="text-align: center; font-weight: bolder;"><?php echo $total_kesenjangan; ?></td>
                        <td style="text-align: center; font-weight: bolder;"><b><?php echo $keadaan; ?></b></td>
<?php } ?>
                </tr>


            </thead>


        </table>
</div>
    </body>

</html>