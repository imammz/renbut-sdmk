<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/theme
 * @author		xamzo Dev Team
 * @copyright   Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('theme_active')) {

    function theme_active() {
        $json = x_file_get_content(MODPATH . '/theme/configs/config' . JSON);
        $act = x_read_json_decode($json);
        $x = &get_instance();
        $x->__library(array('session'));
        if (isset($_COOKIE['theme_opt']) && $x->session->get_data('session_user_id') != '') {
            return $_COOKIE['theme_opt'];
        } else {
            return $act['theme'];
        }
    }

}

if (!function_exists('theme_conf')) {

    function theme_conf($file = null) {
        if ($file != null && is_file(THEMEPATH . 'public/' . theme_active() . '/configs/options/'.$file . JSON)) {
            $vcon = array();
            $json = x_file_get_content(THEMEPATH . 'public/' . theme_active() . '/configs/options/'.$file . JSON);
            $arr = x_read_json_decode($json);
            foreach ($arr as $k => $v) {
                foreach ($v as $keys => $vals) {
                    $vcon[$keys] = $v[$keys]['val'];
                }
            }
            return $vcon;
        }
    }

}

if (!function_exists('theme_frame')) {

    function theme_frame($modul = null, $pos = null) {
        if ($modul != null && $pos != null) {
            $json = x_file_get_content(THEMEPATH . 'public/' . theme_active() . '/configs/options/frame' . JSON);
            $con = x_read_json_decode($json);
            $data = array();
            for ($i = 0; $i < count($con); $i++) {
                foreach ($con[$i] as $key => $val) {
                    $data[$key]['top'] = isset($val['top']) ? $val['top'] : null;
                    $data[$key]['full'] = isset($val['full']) ? $val['full'] : null;
                    $data[$key]['left'] = isset($val['left']) ? $val['left'] : null;
                    $data[$key]['right'] = isset($val['right']) ? $val['right'] : null;
                    $data[$key]['center'] = isset($val['center']) ? $val['center'] : null;
                    $data[$key]['bottom'] = isset($val['bottom']) ? $val['bottom'] : null;
                }
            }
            return ($data[$modul][$pos] && $data[$modul][$pos]=='true') ? $data[$modul][$pos] : null;
        }
    }

}

if (!function_exists('module_frame')) {

    function module_frame($module = null, $pos = null) {
        if ($module != null && $pos != null) {
            $json = x_file_get_content(MODPATH . $module. '/configs/frame' . JSON);
            $con = x_read_json_decode($json);
            $data = array();
            for ($i = 0; $i < count($con); $i++) {
                foreach ($con[$i] as $key => $val) {
                    $data[$key]['top'] = isset($val['top']) ? $val['top'] : null;
                    $data[$key]['full'] = isset($val['full']) ? $val['full'] : null;
                    $data[$key]['left'] = isset($val['left']) ? $val['left'] : null;
                    $data[$key]['right'] = isset($val['right']) ? $val['right'] : null;
                    $data[$key]['center'] = isset($val['center']) ? $val['center'] : null;
                    $data[$key]['bottom'] = isset($val['bottom']) ? $val['bottom'] : null;
                }
            }
            return ($data[$module][$pos] && $data[$module][$pos]=='true') ? $data[$module][$pos] : null;
        }
    }

}

/**
 * End of file ./system/helpers/theme_helper.php
 */