<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<script type="text/javascript" src="<?php echo site_url('application/modules/media/assets/js/script.js') ?>"></script>
<div class="xamzo-block" id="xamzo-media">
    <div class="xamzo-block-content">
        <div class="content">
            <form id="crud-data">
                <ul class="list_image xamzo-box-media" id="image">
                    <li style="display: none">
                        <input type="checkbox" table-check-all="true">
                    </li>
                    <?php
                    $id = 0;
                    foreach ($images as $r) :
                        $t = explode('{ca}', $r);
                        $title = isset($t['1']) ? $t['1'] : '';
                        ?>
                        <li id="<?php echo $id ?>"> 
                            <img class="xamzo-media tip-bottom-left" original-title="<?php echo $title ?>" src="<?php echo site_url("upload/i/" . str_replace('thumb_b', 'thumb_s', $r)); ?>"/>
                            <span dir="<?php echo site_url("upload/i/" . str_replace('thumb_b', 'thumb_s', $r)); ?>">
                                <div class="box-image-act">
                                    <div class="left">
                                        <input table-check-all='true' type="checkbox" onclick="_self_check(this);" name="id[]" value="<?php echo site_url("upload/i/" . $r); ?>" id="<?php echo $r; ?>"> <label for="<?php echo $r; ?>"></label>
                                    </div>
                                    <div class="right">
                                        <a href="javascript:void(0)" onclick="file_url($(this).parent().parent());" class="tip-top" original-title="<?php echo $this->lang["link"] ?>"><i class="fa fa-link"></i></a>
                                        <a href="javascript:void(0)" onclick="img_property($(this).parent().parent().parent());" class="tip-top no-phone" original-title="<?php echo $this->lang["properties"]; ?> "><i class='fa fa-search'></i></a>
                                        <a href="javascript:void(0)" onclick="img_delete(this);" class="tip-top" original-title="<?php echo $this->lang["delete"]; ?>"><i class='fa fa-trash'></i></a> 
                                    </div>
                                </div>
                        </li>
                        <?php
                        $id++;
                    endforeach;
                    ?>
                </ul>
            </form>
        </div>
    </div>
</div>  
<div class="xamzo-block" id="xamzo-property"></div>