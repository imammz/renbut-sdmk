<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class Authuser {

   
    function get_auth_action_user($classlink, $code) {
		        
        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);

        //check auth user for action code
        $menu = $db->get_where('m_menu', array('class'=>$classlink))->row();
        $menu_role = $db->get_where('t_menu_role', array('id_menu'=>$menu->id_menu, 'id_role'=>$this->session->userdata('data_user')->id_role, 'code'=>$code));

        //print_r($access);die;

        if($menu_role->num_rows() > 0){
            return true;
        }else{
            echo '<div class="alert alert-block alert-danger">
                                [ <i class="ace-icon fa fa-info"></i> ]<strong class="red">Maaf</strong>,
                                Anda tidak memiliki hak akses untuk modul ini, <a href="javascript:void()" onclick="getMenu('."'".$menu->link."'".')"> Kembali ke halaman '.$menu->name.'</a>
                                 
                        </div>';
            exit;
        }
		
    }

    function get_user_description(){

        $this->db->from('m_user');
        $this->db->join('m_role', 'm_role.id_role=m_user.id_role','left');
        $this->db->join('m_puskesmas', 'm_puskesmas.kode_puskesmas=m_user.kode_puskesmas','left');
        $this->db->join('m_kkp', 'm_kkp.kode_kkp=m_user.kode_kkp','left');
        $this->db->join('m_btkl', 'm_btkl.kode_btkl=m_user.kode_btkl','left');
        $this->db->join('m_klinik', 'm_klinik.kode_klinik=m_user.kode_klinik','left');
        $this->db->join('m_rumah_sakit', 'm_rumah_sakit.kode_rs=m_user.kode_rs','left');
        $this->db->join('m_kabupaten', 'm_kabupaten.id_kabupaten=m_user.id_kabupaten','left');
        $this->db->join('m_provinsi', 'm_provinsi.id_provinsi=m_user.id_provinsi','left');
        $this->db->where(array('id_user'=>$this->session->userdata('data_user')->id_user));
        $value = $this->db->get()->row(); //print_r($value);die;
        
        $field = 'Anda login sebagai, ';
        if( $this->session->userdata('data_user')->id_role == 5 ){
            $field .= '<strong><i> Puskesmas : '.ucwords($value->nama_puskesmas_kab).' || Kab/Kota : '.ucwords($value->nama_kabupaten).' || Provinsi : '.ucwords($value->nama_provinsi).'</strong></i>';
        }elseif( $this->session->userdata('data_user')->id_role == 10 ){
            $field .= '<strong><i> Klinik : '.ucwords($value->nama_klinik).' || Kab/Kota : '.ucwords($value->nama_kabupaten).' || Provinsi : '.ucwords($value->nama_provinsi).'</strong></i>';
        }elseif( $this->session->userdata('data_user')->id_role == 4 ){
            $field .= '<strong><i> Kab/Kota : '.ucwords($value->nama_kabupaten).' || Provinsi : '.ucwords($value->nama_provinsi).'</strong></i>';
        }elseif( $this->session->userdata('data_user')->id_role == 3 ){
            $field .= '<strong><i> Provinsi : '.ucwords($value->nama_provinsi).'</strong></i>';
        }elseif( $this->session->userdata('data_user')->id_role == 6 ){
            $field .= '<strong><i> Rumah Sakit : '.ucwords($value->nama_rs).' || Kab/Kota : '.ucwords($value->nama_kabupaten).' || Provinsi : '.ucwords($value->nama_provinsi).'</strong></i>';
        }elseif( $this->session->userdata('data_user')->id_role == 8 ){
            $field .= '<strong><i> KKP : '.ucwords($value->nama_kkp).' || Kab/Kota : '.ucwords($value->nama_kabupaten).' || Provinsi : '.ucwords($value->nama_provinsi).'</strong></i>';
        }elseif( $this->session->userdata('data_user')->id_role == 9 ){
            $field .= '<strong><i> BTKLPP : '.ucwords($value->nama_btkl).' || Kab/Kota : '.ucwords($value->nama_kabupaten).' || Provinsi : '.ucwords($value->nama_provinsi).'</strong></i>';
        }else{
            $field .= '<strong><i> '.ucwords($value->role_name).'</strong></i>';
        }

        return $field;
    }

	
}

?> 