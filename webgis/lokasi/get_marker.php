<?php
require('../include/connect.php');

if ($_POST){
    if (isset($_POST["del"]) && $_POST["del"] == true) {
        //make sure request is comming from Ajax
        $xhr = $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
        if (!$xhr) {
            header('HTTP/1.1 500 Error: Request must come from Ajax!');
            exit();
        }
        
        // get marker position and split it for database
        $mLatLang = explode(',', $_POST["latlang"]);
        $mLat     = filter_var($mLatLang[0], FILTER_VALIDATE_FLOAT);
        $mLng     = filter_var($mLatLang[1], FILTER_VALIDATE_FLOAT);
        
        //Delete Marker
        if (isset($_POST["del"]) && $_POST["del"] == true) {
            $results = mysql_query("DELETE FROM markers WHERE lat=$mLat AND lng=$mLng");
            if (!$results) {
                header('HTTP/1.1 500 Error: Could not delete Markers!');
                exit();
            }
            exit("Done!");
        }
        
        $mName    = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
        $mAddress = filter_var($_POST["address"], FILTER_SANITIZE_STRING);
        $mType    = filter_var($_POST["type"], FILTER_SANITIZE_STRING);
        
        $results = mysql_query("INSERT INTO markers (name, address, lat, lng, type) VALUES ('$mName','$mAddress',$mLat, $mLng, '$mType')");
        if (!$results) {
            header('HTTP/1.1 500 Error: Could not create marker!');
            exit();
        }
        
        $output = '<h1 class="marker-heading">' . $mName . '</h1><p>' . $mAddress . '</p>';
        exit($output);
    } 
	
	if (isset($_POST["search"]) && $_POST["search"] == true) {
		$dom     = new DOMDocument("1.0");
		$node    = $dom->createElement("markers"); //Create new element node
		$parnode = $dom->appendChild($node); //make the node show up 
		$search  = $_POST["search"];
		// Select all the rows in the markers table
		$results = mysql_query("SELECT * FROM markers WHERE name='$search'");
		if (!$results) {
			header('HTTP/1.1 500 Error: Could not get markers!');
			exit();
		}

		//set document header to text/xml
		header("Content-type: text/xml");

		// Iterate through the rows, adding XML nodes for each

		while ($x = mysql_fetch_assoc($results)) {
			$node    = $dom->createElement("marker");
			$newnode = $parnode->appendChild($node);
			$newnode->setAttribute("name", $x['name']);
			$newnode->setAttribute("address", $x['address']);
			$newnode->setAttribute("lat", $x['lat']);
			$newnode->setAttribute("lng", $x['lng']);
			$newnode->setAttribute("type", $x['type']);
			$newnode->setAttribute("info1", $x['info1']);
			$newnode->setAttribute("info2", $x['info2']);
			$newnode->setAttribute("info3", $x['info3']);
			$newnode->setAttribute("info4", $x['info4']);
	
		}

		// $dom->saveXML();
		  exit($dom->saveXML());
	
	}
}


$dom     = new DOMDocument("1.0");
$node    = $dom->createElement("markers"); //Create new element node
$parnode = $dom->appendChild($node); //make the node show up 

// Select all the rows in the markers table
$results = mysql_query("SELECT * FROM markers WHERE 1");
if (!$results) {
    header('HTTP/1.1 500 Error: Could not get markers!');
    exit();
}

//set document header to text/xml
header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each

while ($x = mysql_fetch_assoc($results)) {
    $node    = $dom->createElement("marker");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("name", $x['name']);
    $newnode->setAttribute("address", $x['address']);
    $newnode->setAttribute("lat", $x['lat']);
    $newnode->setAttribute("lng", $x['lng']);
    $newnode->setAttribute("type", $x['type']);
	$newnode->setAttribute("info1", $x['info1']);
	$newnode->setAttribute("info2", $x['info2']);
	$newnode->setAttribute("info3", $x['info3']);
	$newnode->setAttribute("info4", $x['info4']);
}

echo $dom->saveXML();
