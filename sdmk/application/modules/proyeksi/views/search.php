<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
?>  
<form id="crud-data">  
    <input type="hidden" name="by" value="<?php echo isset($by) ? $by : '' ?>">
    <input type="hidden" name="order" value="<?php echo isset($order) ? $order : '' ?>">
    <input type="hidden" name="search" value="<?php echo isset($search) ? $search : '' ?>">   
    <input type="hidden" name="s_no_prov" value="<?php echo isset($s_no_prov) ? $s_no_prov : '' ?>"> 
    <input type="hidden" name="s_no_jenis_sdmk" value="<?php echo isset($s_no_jenis_sdmk) ? $s_no_jenis_sdmk : '' ?>">  
    <table id="rincome" class="sort table table-no-border" table-check="true" ajax-sort-url="proyeksi/sort" cellspacing="0" cellpadding="0">
        <tr class="ui-state-disabled" tabindex="0">
            <th><input type="checkbox" table-check-all="true" id="check-all"> <label for="check-all"></label></th>
            <th>
                <span class="left">Nama Propinsi</span>  
                <span class="right">
                    <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('nama_prov', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('nama_prov', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th>
            <th>
                <span class="left">Jenis SDMK</span>  
                <span class="right">
                    <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('nama_jenis_sdmk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('nama_jenis_sdmk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th> 
            <th>
                <span class="left">(%) Ratio Penduduk/th</span>  
                <span class="right">
                    <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('ratio', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('ratio', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th>
            <th>
                <span class="left">Pengangkatan Baru</span>  
                <span class="right">
                    <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('pengangkatan_baru', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('pengangkatan_baru', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th>
            <th colspan="2">
                <span class="left">Pindah Masuk</span>  
                <span class="right">
                    <a class="tip-top-right" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('pindah_masuk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                    <a class="tip-bottom-right" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('pindah_masuk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                </span>
            </th>
        </tr>
        <?php
        $no = $offset + 1;
        if ($total > 0):
            foreach ($rs as $row) {
                $rowhighlight = ($no % 2 == 0) ? 'rowhighlight' : '';
                ?>
                <tr tabindex="<?php echo $no ?>" table-check-this="1" id="id_<?php echo $row->id_proyeksi ?>" class="ui-state-disabled <?php echo $rowhighlight; ?>">
                    <td> 
                        <p><input type="checkbox" name="id[]" value="<?php echo $row->id_proyeksi ?>" id="<?php echo $row->id_proyeksi ?>"> <label for ="<?php echo $row->id_proyeksi ?>"></label></p>
                    </td>
                    <td> 
                        <span class="dt-title"><?php echo $row->nama_prov ?></span>
                    </td>  
                    <td><?php echo $row->nama_jenis_sdmk ?></td> 
                    <td><?php echo $row->ratio ?></td>
                    <td><?php echo $row->pengangkatan_baru ?></td>
                    <td><?php echo $row->pindah_masuk ?></td>
                    <td>&nbsp;</td>
                </tr>
                <?php
                $no++;
            }
        endif;
        ?>
    </table>
    <div class="xamzo-pagination">
        <div id="option">
            <?php
            $selected = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : '';
            echo $this->lang['showing_1_to'] . form_dropdown($opt_limit, $selected, array('title' => 'limit', 'onchange' => 'change_limit(this)')) . "&nbsp;" . $this->lang['of'] . " $total " . $this->lang['entries']
            ?> 
        </div>
        <div id="pagging">
            <?php echo $paging; ?> 
        </div>
    </div>    
</form> 