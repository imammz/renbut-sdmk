<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?>
<div class="modal-header">
    <button type="button" class="close" onclick='close_box()' data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3><i class="fa fa-pencil"></i> <?php echo $this->lang["rename"]; ?></h3>
</div>
<div class="modal-body"> 
    <form id="xamzo-rename-file">
        <input type="hidden" name="old_name" value="<?php echo isset($old_name) ? $old_name : ''; ?>">
        <input type="hidden" name="the_old_name" value="<?php echo isset($new_name) ? $new_name : '' ?>">       
        <p>
            <label><?php echo $this->lang['new_name'] ?> </label>
            <input type="text" validation="required|caracter" name="new_name" style="width: 62%" value="<?php echo isset($new_name) ? $new_name : '' ?>">
        </p>
    </form>
</div>
<div class="modal-footer border-top">
    <button class="btn primary medium right tip-top" original-title="<?php echo $this->lang['yes'] ?>" onclick="this_file_rename('<?php echo $type ?>');"><?php echo $this->lang['yes']; ?></button>
</div> 