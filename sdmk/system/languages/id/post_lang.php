<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang['article'] = 'article';
$lang['photo'] = 'photo';
$lang['shop'] = 'shop';
$lang['polling'] = 'polling';
$lang['quote'] = 'quote';
$lang['link'] = 'link';
$lang['audio'] = 'audio';
$lang['video'] = 'video';
$lang['choose_category'] = 'pilih kategori';
$lang['this_page_to_manage_post_content,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola konten post, lihat dokumentasi untuk menggunakannya';
$lang['this_page_to_manage_post_categories,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola kategori post, lihat dokumentasi untuk menggunakannya';
/**
 * audio
 */
$lang['enter_track_name'] = 'Masukan nama track';
$lang['external_url'] = 'External url';
$lang['play_list'] = 'Play list';
$lang['embed_code'] = 'Embed code';
$lang['video_url'] = 'Video url';
/**
 * photo
 */
$lang['browse_from_media'] = 'browse dari media';
$lang['drop_here'] = 'Tarik di sini';

$lang['set_as_publish'] = 'atur sebagai publish';
$lang['set_as_draft'] = 'atur sebagai draft';