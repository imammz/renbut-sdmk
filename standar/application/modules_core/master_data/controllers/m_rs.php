<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('rs_model','rs');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Rumah Sakit";
		$data['subtitle'] = "Daftar Rumah Sakit";
		$this->load->view('rs/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Rumah Sakit";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->rs->get_by_id($id);
		}
		$this->load->view('rs/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->rs->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $rs) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.strtoupper($rs->kode_rs).' ]';
			$row[] = strtoupper($rs->nama_rs);
			$row[] = $rs->alamat;
			$row[] = $rs->jenis_rs;
			$row[] = $rs->kelas_rs;
			$row[] = $rs->jumlah_tt;
			$row[] = ($rs->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $rs->updated_date?Tanggal::formatDateTime($rs->updated_date):Tanggal::formatDateTime($rs->created_date);

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($rs->id_rs,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_rs('."'".Regex::_genRegex($rs->id_rs,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->rs->count_all(),
						"recordsFiltered" => $this->rs->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	public function sinkronisasi()
	{
		
		$data['title'] = "Sinkronisasi Data Puskesmas";
		$data['subtitle'] = "Download data puskesmas";
		
		$this->load->view('rs/form_sinkronisasi', $data);
	}

	public function proses_sinkronisasi()
	{

		$this->db->trans_begin();

		$files = file_get_contents('uploaded_files/data/rumah_sakit.json');
        $data = json_decode($files);
        echo '<pre>';print_r(count($data));die;

        //$this->db->truncate('m_rumah_sakit');

        foreach ($data as $key => $value) {
        	
        	// cek ada atau tidak 
        	$rs = $this->db->get_where('m_rumah_sakit', array('kode_rs'=>$value->KodeRS)); 

        	$field = array(
        		'kode_rs' => Regex::_genRegex($value->KodeRS,'RGXQSL'),
        		'id_kab' => Regex::_genRegex($value->KodeKabupaten,'RGXINT'),
        		'nama_rs' => Regex::_genRegex($value->NamaRS,'RGXQSL'),
        		'jenis_rs' => Regex::_genRegex($value->JenisRS,'RGXAZ'),
        		'kelas_rs' => Regex::_genRegex($value->KelasRS,'RGXAZ'),
        		'jumlah_tt' => Regex::_genRegex($value->JumlahTT,'RGXINT'),
        		'active' => 'Y',
        		'created_date' => date('Y-m-d H:i:s'),
        		'created_by' => $this->session->userdata('data_user')->fullname,
        		'updated_date' => date('Y-m-d H:i:s'),
        		'updated_by' => $this->session->userdata('data_user')->fullname,
        		);
        	
        	if( $rs->num_rows() > 0 ){
        		$this->db->update('m_rumah_sakit', $field, array('kode_rs'=>$value->KodeRS));
        	}else{
        		$this->db->insert('m_rumah_sakit', $field);
        	}
        
        }

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_add()
	{
		$id_rs = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'kode_rs' => Regex::_genRegex($this->input->post('kode_rs'), 'RGXQSL'),
			'nama_rs' => Regex::_genRegex($this->input->post('nama_rs'), 'RGXQSL'),
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXQSL'),
			'id_kabupaten' => Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXQSL'),
			'alamat' => Regex::_genRegex($this->input->post('alamat'), 'RGXQSL'),
			'jumlah_tt' => Regex::_genRegex($this->input->post('jumlah_tt'), 'RGXQSL'),
			'kelas_rs' => Regex::_genRegex($this->input->post('kelas_rs'), 'RGXQSL'),
			'jenis_rs' => Regex::_genRegex($this->input->post('id_jenis_rsk'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_rs == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->id_user;
			$this->rs->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->id_user;
			$this->rs->update(array('id_rs'=>$id_rs), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->rs->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}	

	function get_rs_by_kode($kode_rs) {
		
        $this->db->select('m_rumah_sakit.*');
        $this->db->from('m_rumah_sakit');
       
        $this->db->where('kode_rs', $kode_rs);
        $this->db->order_by('nama_rs', 'ASC');
        
        $result = $this->db->get()->row_array();
        $string = '
        		<div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-9">
                    <div class="text">
                      <label>
                        <input name="kategori_kelas" type="text" class="ace" value="'.$result['kelas_rs'].'" >
                      </label>
                    </div>
                  </div>
                </div>';
        echo json_encode(array('string'=>$string));
        exit;
    }

    function get_rs_by_kode_json($kode_rs) {
		
        $this->db->select('m_rumah_sakit.*');
        $this->db->from('m_rumah_sakit');
       
        $this->db->where('kode_rs', $kode_rs);
        $this->db->order_by('nama_rs', 'ASC');
        
        $result = $this->db->get()->row_array();
        echo json_encode($result);
    }

	
}
