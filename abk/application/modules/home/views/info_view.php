<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />



<!-- end: HEAD -->
<body>
    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1900px;">

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-7" style="font-size: larger;"> 
                <h3 class="text-azure text-right"> Kepada Yth Bapak/Ibu Pengguna Aplikasi Renbut SDMK  </h3>
                <p class="text-primary text-justify"> 
                	Saat ini aplikasi sedang dalam perbaikan fitur Eksport dan Import. Untuk sementara fitur ini tidak bisa dilakukan  sampai dengan Jumat, tanggal 21 Oktober 2016.<br/>
                	Namun jika ada kebutuhan pelaporan data segera, Bapak/Ibu bisa email file JSon hasil Export aplikasi Offline ke email : <b>imammz@ymail.com  </b>   
                </p>
                    <br/>
                    <p class="text-justify">
                        <b>Mohon Maaf Untuk Ketidaknyamanannya, <br>Salam Hangat Dan Terima Kasih  
                            <br>
                            Pengelola Aplikasi Renbut SDMK, PUSRENGUN BPPSDM Kementerian Kesehatan RI.
                        </b>
                    </p>  

            </div>
            <div class="col-md-1"></div>
        </div>
        
        <div class="row">
            <div class="col-md-12 text-bold">
                
                    <br/>
                    <br/>
                Klik Tombol Merah Dipojok Kiri atas untuk menutup halaman ini</div>
            
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


<?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>


    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                                            jQuery(document).ready(function () {
                                                Main.init();
                                                jQuery('#example-advanced').treetable('expandAll');
                                                //jQuery('#example-advanced').treetable();
                                                $('.modal').modal({show: true});
                                                FormElements.init();

                                            });

    </script>

    <script>

        function tambahFaskes(NO_KAB) {
            GB_show("Tambah Data Faskes", '<?php echo base_url() ?>index.php/entry/dinaskesehatan/tambahFaskes/' + NO_KAB, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function tambahBagian(faskes_kode) {
            GB_show("Tambah Data Bagian/Bidang", '<?php echo base_url() ?>index.php/entry/dinaskesehatan/tambahBagian/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
		
		 function tambahSubBagian(faskes_kode) {
            GB_show("Tambah Data SubBidang / SubBagian", '<?php echo base_url() ?>index.php/entry/dinaskesehatan/tambahSubBagian/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function editInstallasi(faskes_kode) {
            GB_show("Edit Data Bagian/Bidang - SubBidang/SubBagian", '<?php echo base_url() ?>index.php/entry/dinaskesehatan/editInstallasi/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function tambahSDMK(faskes_installasi_id) {
            GB_show("Hitung Data Kebutuhan SDMK", '<?php echo base_url() ?>index.php/entry/dinaskesehatan/tambahSDMK/' + faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
         function hapusSDMK(sdmk_faskes_installasi_id) {
            GB_show("Hapus SDMK", '<?php echo base_url() ?>index.php/entry/dinaskesehatan/hapusSDMK/' + sdmk_faskes_installasi_id, 300, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function hapusInstallasi(faskes_installasi_id) {
            GB_show("Hapus Installasi / Sub Installasi / Bagian / Sub bagian", '<?php echo base_url() ?>index.php/entry/dinaskesehatan/hapusInstallasi/' + faskes_installasi_id, 300, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function detilABK(sdmk_faskes_installasi_id) {
            GB_show("Detil Data ABK", '<?php echo base_url() ?>index.php/entry/entry/detil_perhitungan_abk/' + sdmk_faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editFaskes(faskes_kode) {
            GB_show("Edit Data Dinas Kesehatan Kab/Kota", '<?php echo base_url('index.php/entry/dinaskesehatan/editfaskes'); ?>/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editInstallasi(installasi_id) {
            GB_show("Edit Data Bagian Bidang", '<?php echo base_url('index.php/entry/dinaskesehatan/editinstallasi'); ?>/' + installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editSDMK(sdmk_faskes_installasi_id) {
            GB_show("Edit Data SDMK", '<?php echo base_url('index.php/entry/dinaskesehatan/editsdmk'); ?>/' + sdmk_faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
     


    </script>


    <script>


        $("#example-advanced").treetable({expandable: true});



// Highlight selected row
        $("#example-advanced tbody").on("mousedown", "tr", function () {
            $(".selected").not(this).removeClass("selected");
            $(this).toggleClass("selected");
        });


        $("#example-advanced .folder").each(function () {
            $(this).parents("#example-advanced tr").droppable({
                accept: ".file, .folder",
                drop: function (e, ui) {
                    var droppedEl = ui.draggable.parents("tr");
                    $("#example-advanced").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
                },
                hoverClass: "accept",
                over: function (e, ui) {
                    var droppedEl = ui.draggable.parents("tr");
                    if (this != droppedEl[0] && !$(this).is(".expanded")) {
                        $("#example-advanced").treetable("expandNode", $(this).data("ttId"));
                    }
                }
            });
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
