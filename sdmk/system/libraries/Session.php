<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package		library/session
 * @author		xamzo developer 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Session {

    public $_session_data = array();
    public $_session_config = array();

    public function __construct() {
        $this->_session_data = isset($_COOKIE[$this->_sess_name()]) ? $_COOKIE[$this->_sess_name()] : '';
        $this->set_data($this->_session_data);
    }

    public function _sess_name() {
        return md5(X_SESS_NAME);
    }

    public function _sess_expired() {
        return X_SESS_EXPIRE;
    } 
    
    public function read_data() {
        $arr_sess = $this->_session_data;
        $sess = array();
        $data = explode('{0}', $arr_sess);
        for ($i = 0; $i < count($data) - 1; $i++) {
            $ses = explode('<o>', $data[$i]);
            $sess[$ses['0']] = isset($ses['1']) ? $ses['1'] : '-';
        }
        return $sess;
    }

    public function is_session($sess_) {
        if ($sess_ != null) {
            $sess = $this->read_data(); 
            if (in_array($sess_, array_keys($sess))) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    public function get_data($sess_name = null) {
        if ($sess_name != null) {
            $sess = $this->read_data();
            $sess_ = md5($sess_name);
            if (in_array($sess_, array_keys($sess))) {
                return $sess[$sess_];
            }
        }
    }
    
    public function destroyed() {
        setcookie($this->_sess_name(), '', time() - 3600, '/');
    }
    
    public function set_data($data = array()) {
        if (is_array($data)) {
            $sess_readed = $this->read_data();
            $session_new = ''; 
            $xsession = array_merge($sess_readed, $data);
            foreach ($xsession as $key => $value) {
                if ($this->is_session($key)) {
                    $skey = $key; 
                } else {
                    $skey = md5($key); 
                }
                $session_new.= $skey. '<o>' . $value . '{0}';
            }
            setcookie($this->_sess_name(), $session_new, time() + ($this->_sess_expired()), '/');
        }
    }

}

