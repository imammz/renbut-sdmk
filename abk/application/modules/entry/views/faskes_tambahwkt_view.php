<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<style>
    .CSSTableGenerator {
        margin:0px;padding:0px;
        width:100%;	box-shadow: 10px 10px 5px #888888;
        border:1px solid #000000;

        -moz-border-radius-bottomleft:0px;
        -webkit-border-bottom-left-radius:0px;
        border-bottom-left-radius:0px;

        -moz-border-radius-bottomright:0px;
        -webkit-border-bottom-right-radius:0px;
        border-bottom-right-radius:0px;

        -moz-border-radius-topright:0px;
        -webkit-border-top-right-radius:0px;
        border-top-right-radius:0px;

        -moz-border-radius-topleft:0px;
        -webkit-border-top-left-radius:0px;
        border-top-left-radius:0px;
    }.CSSTableGenerator table{
        width:100%;
        height:100%;
        margin:0px;padding:0px;
    }.CSSTableGenerator tr:last-child td:last-child {
        -moz-border-radius-bottomright:0px;
        -webkit-border-bottom-right-radius:0px;
        border-bottom-right-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:first-child {
        -moz-border-radius-topleft:0px;
        -webkit-border-top-left-radius:0px;
        border-top-left-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:last-child {
        -moz-border-radius-topright:0px;
        -webkit-border-top-right-radius:0px;
        border-top-right-radius:0px;
    }.CSSTableGenerator tr:last-child td:first-child{
        -moz-border-radius-bottomleft:0px;
        -webkit-border-bottom-left-radius:0px;
        border-bottom-left-radius:0px;
    }.CSSTableGenerator tr:hover td{

    }.CSSTableGenerator tr:nth-child(odd){ background-color:#e5e5e5; }
    .CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }
    .CSSTableGenerator td{
        vertical-align:middle;


        border:1px solid #000000;
        border-width:0px 1px 1px 0px;
        text-align:left;
        padding:7px;
        font-size:10px;
        font-family:arial;
        font-weight:normal;
        color:#000000;
    }.CSSTableGenerator tr:last-child td{
        border-width:0px 1px 0px 0px;
    }.CSSTableGenerator tr td:last-child{
        border-width:0px 0px 1px 0px;
    }.CSSTableGenerator tr:last-child td:last-child{
        border-width:0px 0px 0px 0px;
    }
    .CSSTableGenerator tr:first-child td{
        background:-o-linear-gradient(bottom, #4c4c4c 5%, #000000 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #000000) );	background:-moz-linear-gradient( center top, #4c4c4c 5%, #000000 100% );	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#000000");	background: -o-linear-gradient(top,#4c4c4c,000000);
        background-color:#4c4c4c;
        border:0px solid #000000;
        text-align:center;
        border-width:0px 0px 1px 1px;
        font-size:14px;
        font-family:arial;
        font-weight:bold;
        color:#ffffff;
    }
    .CSSTableGenerator tr:first-child:hover td{
        background:-o-linear-gradient(bottom, #4c4c4c 5%, #000000 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #000000) );	background:-moz-linear-gradient( center top, #4c4c4c 5%, #000000 100% );	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#000000");	background: -o-linear-gradient(top,#4c4c4c,000000);
        background-color:#4c4c4c;
    }
    .CSSTableGenerator tr:first-child td:first-child{
        border-width:0px 0px 1px 0px;
    }
    .CSSTableGenerator tr:first-child td:last-child{
        border-width:0px 0px 1px 1px;
    }
    .CSSTableGenerator th {
        text-align: center;
    }

    .CSSTableGenerator td {
        text-align: center;
    }
</style>

<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1200px;">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                echo "<h2>Langkah 2 - Penetapan Waktu Kerja tersedia</h2>";
                echo "<h3>{$sdmk_faskes_installasi['sdmk_faskes_installasi_nama']}</h3>";
                echo "<h3>{$faskes_installasi['installasi_nama']} - {$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                ?>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/tambahABK/<?php echo $sdmk_faskes_installasi_id ?>">

                    <sub style="text-align:justify;">Dalam Keputusan Presiden Nomor 68 Tahun 1995 telah ditentukan jam kerja instansi pemerintah 37 jam 30 menit per minggu, baik untuk yang 5 (lima) hari kerja ataupun yang 6 (enam) hari kerja sesuai dengan yang ditetapkan Kepala Daerah masing-masing. 
                        Berdasarkan Peraturan Badan Kepegawaian Negara Nomor 19 Tahun 2011 tentang Pedoman Umum Penyusunan Kebutuhan Pegawai Negeri Sipil, Jam Kerja Efektif (JKE) sebesar 1200 jam per tahun. Demikian juga menurt Permen PA-RB No. 26 tahun 2011, Jam Kerja Efektif (JKE) sebesar 1200 jam per tahun atau 72000 menit per tahun baik 5 hari kerja atau 6 hari kerja.
                    </sub>
                    
                    <div class="box-body">
                        <table width="100%" border="1" class="CSSTableGenerator" style="text-align: center;">
                            <tr><th width="50" rowspan="2"> No </th><th width="80"> Kode </th> <th> Komponen </th> <th> Keterangan </th> <th> Rumus </th> <th> Jumlah </th> <th> Satuan </th> </tr>
                            <tr><th width="80"> A </th> <th> B </th> <th> C </th> <th> D </th> <th> E </th> <th> F </th> </tr>
                            
                            <?php 
                            $no = 1;
                            foreach($wkt as $row) { ?>
                            
                            <tr><td> <?php echo $no; ?> </td> <td> <?php  echo $row['komponen_wkt_kode']; ?></td> <td> <?php echo $row['komponen_wkt_komponen'] ?></td> <td> <?php echo $row['komponen_wkt_keterangan'] ?></td> <td> <?php echo $row['komponen_wkt_rumus'] ?></td> <td> <?php echo $row['komponen_wkt_jumlah'] ?></td> <td> <?php $satuan =  $this->orm->ref_satuan->where('satuan_id',$row['satuan_id'])->fetch(); echo $satuan['satuan_nama'] ?></td> </tr>  
                            
                                
                                <?php $no++; } ?>
                            
                            <tr> <td colspan="5"> <b> <?php echo $wkt_hasil_jam['komponen_wkt_komponen'] ?> </b> </td>   <td> <b> <?php echo number_format($wkt_hasil_jam['komponen_wkt_jumlah'], 2, '.', ',') ?> </b></td>  <td> <?php $satuan =  $this->orm->ref_satuan->where('satuan_id',$wkt_hasil_jam['satuan_id'])->fetch(); echo $satuan['satuan_nama'] ?></td> </tr> 
                            <tr> <td colspan="5"> <b> <?php echo $wkt_hasil_menit['komponen_wkt_komponen'] ?> </b> </td>   <td> <b> <?php echo number_format($wkt_hasil_menit['komponen_wkt_jumlah'], 2, '.', ',')  ?> </b></td>  <td> <?php $satuan =  $this->orm->ref_satuan->where('satuan_id',$wkt_hasil_menit['satuan_id'])->fetch(); echo $satuan['satuan_nama'] ?></td> </tr> 
                            
                        </table> 
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-sm-6"> 
                        <a href="<?php echo base_url('index.php/entry/'.$class.'/editsdmk/'.$sdmk_faskes_installasi_id) ?>" class="btn btn-danger btn-flat" ><< Kembali</a>
                        </div>
                        <div class="col-sm-6" style="text-align: right;">
                        <a href="#" class="btn btn-danger btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-info btn-flat" type="submit" name="simpan">Selanjutnya >> </button>
                        &nbsp;
                        </div>
                    </div>
                </form>  


            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });


    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
