<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kkp_model extends CI_Model {

	var $table = 'm_kkp';
	var $column = array('m_kkp.kode_kkp','m_kkp.nama_kkp','m_kkp.alamat','m_kkp.no_telp','m_kkp.active','m_kkp.updated_date','m_kkp.created_date','m_kabupaten.nama_kabupaten','m_provinsi.nama_provinsi','m_kkp.kelas_kkp');
	var $select = 'm_kkp.*,m_kabupaten.nama_kabupaten, m_provinsi.nama_provinsi';

	var $order = array('m_kkp.kode_kkp' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _query(){

		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_kabupaten','m_kabupaten.id_kabupaten='.$this->table.'.id_kabupaten','left');
		$this->db->join('m_provinsi','m_provinsi.id_provinsi='.$this->table.'.id_provinsi','left');

	}

	private function _get_datatables_query()
	{
		
		$this->_query();

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->_query();
		$this->db->where(''.$this->table.'.id_kkp',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.id_kkp', $id);
		$this->db->delete($this->table);
	}


}
