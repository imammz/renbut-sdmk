<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi_model extends CI_Model {

	var $table = 't_perencanaan';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _query($params){

		
		$this->db->from('t_perencanaan');
		
		if($params['t_perencanaan.id_jenis_faskes'] == 1){
			$this->db->join('(SELECT nama_puskesmas_kab,kode_puskesmas FROM m_puskesmas) as m_puskesmas', 'm_puskesmas.kode_puskesmas=t_perencanaan.kode_puskesmas','left');
		}elseif ($params['t_perencanaan.id_jenis_faskes'] == 3) {
			$this->db->join('(SELECT m_rumah_sakit.`nama_rs` AS nama_puskesmas_kab, m_rumah_sakit.`kode_rs` AS kode_puskesmas FROM m_rumah_sakit) as m_puskesmas', 'm_puskesmas.kode_puskesmas=t_perencanaan.kode_rsu','left');
		}elseif ($params['t_perencanaan.id_jenis_faskes'] == 29) {
			$this->db->join('(SELECT m_btkl.`nama_btkl` AS nama_puskesmas_kab, m_btkl.`kode_btkl` AS kode_puskesmas FROM m_btkl) as m_puskesmas', 'm_puskesmas.kode_puskesmas=t_perencanaan.kode_btkl','left');
		}elseif ($params['t_perencanaan.id_jenis_faskes'] == 30) {
			$this->db->join('(SELECT m_kkp.`nama_kkp` AS nama_puskesmas_kab, m_kkp.`kode_kkp` AS kode_puskesmas FROM m_kkp) as m_puskesmas', 'm_puskesmas.kode_puskesmas=t_perencanaan.kode_kkp','left');
		}elseif ($params['t_perencanaan.id_jenis_faskes'] == 31) {
			$this->db->join('(SELECT m_klinik.`nama_klinik` AS nama_puskesmas_kab, m_klinik.`kode_klinik` AS kode_puskesmas FROM m_klinik) as m_puskesmas', 'm_puskesmas.kode_puskesmas=t_perencanaan.kode_klinik','left');
		}
		$this->db->join('m_provinsi','m_provinsi.id_provinsi=t_perencanaan.id_provinsi','left');
		$this->db->join('m_kabupaten','m_kabupaten.id_kabupaten=t_perencanaan.id_kabupaten','left');
		$this->db->where($params);

	}

	public function get_rekapitulasi($params)
	{
		//print_r($params);die;
		// get perencanaan by params
		
		$this->db->select('m_provinsi.nama_provinsi,t_perencanaan.id_provinsi');
		$this->_query($params);
		$this->db->group_by('m_provinsi.id_provinsi');
		$by_prov = $this->db->get()->result();  //print_r($this->db->last_query());die;

		$getData = array();
		$getDataPerencanaan = array();
		$getDataKab = array();

		foreach($by_prov as $row_by_prov){
			$this->db->select('m_kabupaten.nama_kabupaten,t_perencanaan.id_perencanaan,t_perencanaan.id_provinsi, t_perencanaan.id_kabupaten');
			$this->_query($params);
			$this->db->where(array('t_perencanaan.id_provinsi'=>$row_by_prov->id_provinsi));
			$this->db->group_by('m_kabupaten.id_kabupaten');
			$kabupaten = $this->db->get()->result(); 

			foreach ($kabupaten as $row_by_kab) {
				$this->db->select('m_puskesmas.nama_puskesmas_kab,t_perencanaan.*');
				$this->_query($params);
				$this->db->where(array('t_perencanaan.id_kabupaten'=>$row_by_kab->id_kabupaten));

				$perencanaan = $this->db->get()->result(); //print_r($this->db->last_query());die;

				foreach ($perencanaan as $key => $value) {
					# code...
					if($params['t_perencanaan.id_jenis_faskes'] == 1){
						$sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$value->id_perencanaan,'tahun'=>$value->tahun, 'id_jenis_faskes'=>$value->id_jenis_faskes, 'id_tipe_puskesmas'=>$value->id_tipe_puskesmas,'id_tipe_kawasan'=>$value->id_tipe_kawasan));
					}else{
						$sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$value->id_perencanaan,'tahun'=>$value->tahun, 'id_jenis_faskes'=>$value->id_jenis_faskes, 'kategori_kelas'=>$params['kategori_kelas']));
					}
					

					$value->sdmk = $sdmk;
					$getData[] = $value;
				}
				
				$row_by_kab->puskesmas = $perencanaan;
				$getDataKab[] = $row_by_kab;
			}

			$row_by_prov->kabupaten = $kabupaten;
			$getDataPerencanaan[] = $row_by_prov;
		}

		//echo '<pre>';print_r($getDataPerencanaan);die;
		return $getDataPerencanaan;

	
	}

	public function get_data_perencanaan($params)
	{
		$this->db->from('t_perencanaan');
		$this->db->where('t_perencanaan.id_perencanaan',$params->id_perencanaan);
		$this->db->join('m_tipe_kawasan','m_tipe_kawasan.id_tipe_kawasan=t_perencanaan.id_tipe_kawasan','left');
		$this->db->join('m_tipe_puskesmas','m_tipe_puskesmas.id_tipe_puskesmas=t_perencanaan.id_tipe_puskesmas','left');
		$this->db->join('m_puskesmas','m_puskesmas.kode_puskesmas=t_perencanaan.kode_puskesmas','left');
		$this->db->join('m_rumah_sakit','m_rumah_sakit.kode_rs=t_perencanaan.kode_rsu','left');
		$this->db->join('m_kkp','m_kkp.kode_kkp=t_perencanaan.kode_kkp','left');
		$this->db->join('m_btkl','m_btkl.kode_btkl=t_perencanaan.kode_btkl','left');
		$this->db->join('m_klinik','m_klinik.kode_klinik=t_perencanaan.kode_klinik','left');
		$this->db->join('m_kabupaten','m_kabupaten.id_kabupaten=m_puskesmas.kode_puskesmas','left');

		$query = $this->db->get();

		return $query->row();

		return $getData;
	
	}


}
