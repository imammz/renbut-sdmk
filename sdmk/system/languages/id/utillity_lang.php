<?php if (!defined('BASEPATH')) exit('no direct script user allowed');
$lang['utillity'] = 'Kegunaan';
$lang['code'] = 'Kode';
$lang['country'] = 'Negara'; 
$lang['this_page_to_manage_gallery,_see_documentation_for_use_it'] = 'Halaman ini untuk mengatur gallery, lihat dokumentasi untuk itu';
$lang['this_page_to_manage_experience,_see_documentation_for_use_it'] = 'Halaman ini untuk mengatur pengalaman, lihat dokumentasi untuk itu';
$lang['this_page_to_manage_education,_see_documentation_for_use_it'] = 'Halaman ini untuk mengatur pendidikan, lihat dokumentasi untuk itu';
$lang['this_page_to_manage_loker,_see_documentation_for_use_it'] = 'Halaman ini untuk mengatur lowongan kerja, lihat dokumentasi untuk itu';
$lang['this_page_to_manage_slide,_see_documentation_for_use_it'] = 'Halaman ini untuk mengatur slide, lihat dokumentasi untuk itu'; 
$lang['this_page_to_manage_phone,_see_documentation_for_use_it'] = 'Halaman ini untuk mengatur phone, lihat dokumentasi untuk itu';
 
$lang['click_in_image_area_and_upload_an_image_from_your_computer'] = 'klik pada area gambar dan unggah sebuah gambar dari komputer';
$lang['required_min_3_max_30_characters'] = 'dibutuhkan minimal 3 maximal 30 karakter';
$lang['required_min_5_max_100_characters'] = 'dibutuhkan minimal 5 maximal 100 karakter';
$lang['required_min_10_max_225_characters'] = 'dibutuhkan minimal 10 maximal 225 karakter';