
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Portal Aplikasi Perencanaan Kebutuhan SDMK</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/style-responsive.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/open-sans.css">
        <link rel="stylesheet" href="css/dialog.css">
        <link rel="stylesheet" href="css/dialog-sally.css">
        <link rel="icon" href="images/icon.png">
        <!-- CHART -->
        <link rel="stylesheet" href="css/chartist.min.css">
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
    </head>
    <body onload="video();">
        <!-- HEADER -->
        <section id="header">
            <div class="logo"><br><br><br><br><br><br></div>
            <div class="header-title-2"><!-- DEWAN KEHORMATAN PENYELENGGARA PEMILU  --></div>
            <hr class="header-garis" />
            <div class="header-title-2"><!-- SIPEPP --></div>
            <p class="header-text-gold"><!-- Sistem Informasi Peradilan Etika Penyelenggara Pemilu --></p>
            <div class="header-menu">
                <!-- <button class="button-merah" onclick="return goToWeb();">Website DKPP RI</button> 
                <span class="header-text-margin">atau</span> 
                <button class="button-biru trigger"  data-dialog="login" >Login</button> -->
            </div>
        </section>
        <!-- END HEADER -->

        <!-- MENU -->
        <section id="menu">
            <div class="container">
                <div class="row">
                <img src="main_portal.jpg" style="margin-top:-20px">
                
                <div class="row" style="margin-top:-53%;padding-bottom:18%">
                    <div class="header-title"><h3><b>APLIKASI PERENCANAAN KEBUTUHAN SDM KESEHATAN</b></h3></div>
                    <div class="col-md-12 text-center menu-bawah">
                        <a href="http://202.70.136.196/abk" ><img src="menu_1.jpg"></a>
                        <a href="http://202.70.136.196/standar" ><img src="menu_2.jpg"></a>
                        <a href="http://pusrengun.info/sdmk" ><img src="menu_3.jpg"></a>
                    </div>
                    <div class="col-md-12 text-center menu-bawah">
                        <a href="download" target="blank"><h3><b>DOWNLOAD MATERI</b></h3></a>
                    </div>
                    <div class="col-md-12 text-center">
                    <h3>Registrasi Pelayanan Kesehatan</h3>
                    <h4>
                     |   <a href="http://202.70.136.196/abk/index.php/config/masterdata/klinik/add" target="blank"><b>KLINIK</b></a> |
                     |   <a href="http://202.70.136.196/abk/index.php/config/masterdata/bapelkes/add" target="blank"><b>BAPELKES</b></a> |
                         |   <a href="http://202.70.136.196/abk/index.php/config/masterdata/kkp/add" target="blank"><b>KKP</b></a> |
                             |   <a href="http://202.70.136.196/abk/index.php/config/masterdata/labkes/add" target="blank"><b>LABKES</b></a> |
                                 |   <a href="http://202.70.136.196/abk/index.php/config/masterdata/bpkm/add" target="blank"><b>BPKM</b></a> |
                                     |   <a href="http://202.70.136.196/abk/index.php/config/masterdata/btklp/add" target="blank"><b>BPTLKPP</b></a> |
                         |   <a href="http://202.70.136.196/abk/index.php/config/masterdata/faskeslain/add" target="blank"><b>FASKES LAIN</b></a> |
                    </h4>
                    </div>
                </div>
                

            </div>
        </section>
        <!-- END MENU -->


        <footer>
            &copy;copyright BPPSDM - Badan Pengembangan dan Pemberdayaan Sumber Daya Manusia Kesehatan 2017 | AIPHSS

        </footer>
     
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/dialogFx.js"></script>
        <script type="text/javascript" src="js/chartist.min.js"></script>
        <script type="text/javascript" src="js/DKPP.js"></script>
        
        <!-- Yose Modal CSS -->
<link rel="stylesheet" type="text/css" href="yosemodal/css/yosemodal.css">
<!-- Gsap Animations -->
<script src="yosemodal/js/gsap.js"></script>
<!-- Yose Modal JS -->
<script src="yosemodal/js/yosemodal.js"></script>
<!-- Font Awesome  -->
<link rel="stylesheet" href="yosemodal/font-awesome/css/font-awesome.min.css">


<script>
    function video() {

     

    }
	
	function video1() {

        $('html, body').animate({scrollTop: 0}, 'slow');

        $.yosemodal({
            title: "Proses Rekomendasi Reklame",
            iframe: "video.mp4",
            loadingmessage: "Sedang Proses...",
            loadingicon: "fa-refresh",
            top: "5%",
            left: "50%",
            width: "80%",
            height: "85%",
            closeonclick: true,
            imgrounded: true,
            bgopacity: 0.7
        });


    }
   


</script>

        
    </body>
</html>