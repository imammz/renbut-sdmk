<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/media
 * @author		xamzo's Developer
 * @copyright           Copyright (c) 2014, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Media extends X_Controller {

    public $limit = 30;

    public function __construct() {
        parent::__construct();
        $this->__composer('media');
    }

    public function index() {
        
    }

}
