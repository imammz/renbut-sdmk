<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source appliXtion development framework and CMS for PHP 5 or newer
 *
 * @package     libraries/Exceptions
 * @author      xamzo Dev Team
 * @copyright   Copyright (c) 2013, xamzo, Inc.
 * @license     http://xamzo.com/license/
 * @link        http://xamzo.com
 * @since       Version 1.0
 * @filesource
 */
class Exceptions {

    public $ob_level;
    public $X;

    /**
     * Constructor
     */
    public function __construct() {
        $this->X = get_instance();
        $this->X->__helper(array('auth'));
        $this->X->__language(array('error'));
        $this->ob_level = ob_get_level();
        // Note:  Do not log messages from this constructor.
    }

    function show_error($heading = '', $message = '', $template = 'error_general', $status_code = 500) {
        $message = '<p>' . implode('</p><p>', (!is_array($message)) ? array($message) : $message) . '</p>';
        if (ob_get_level() > $this->ob_level + 1) {
            ob_end_flush();
        }
        ob_start();
        if (is_login()) {
            include(BASEPATH . 'libraries/error/views/' . $template . EXT);
        } else {
            if ($template == 'error_404') {
                if (is_file(THEMEPATH . 'public/' . theme_active() . '/errors/' . $template . EXT)) {
                    include(THEMEPATH . 'public/' . theme_active() . '/errors/' . $template . EXT);
                } else {
                    include(BASEPATH . 'libraries/error/views/' . $template . EXT);
                }
            } else {
                include(BASEPATH . 'libraries/error/views/' . $template . EXT);
            }
        }

        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }

}

