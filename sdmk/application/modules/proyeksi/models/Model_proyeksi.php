<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/proyeksi
 * @author		xamzo Dev Team 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Model_proyeksi extends X_Model {

    // master 
    public $master = 'ref_';
    public $tbl_proyeksi = 'proyeksi';
    public $tbl_tahun_proyeksi = 'tahun_proyeksi';
    public $ref_proyeksi = 'proyeksi';
    public $ref_propinsi = 'provinsi';
    public $ref_jenis_sdmk = 'jenis_sdmk';

    public function __construct() {
        parent::__construct();
        // get table prefix  
        $this->tbl_proyeksi = $this->db->prefix . $this->tbl_proyeksi;
        $this->tbl_tahun_proyeksi = $this->db->prefix . $this->tbl_tahun_proyeksi;
        $this->ref_proyeksi = $this->master . $this->ref_proyeksi;
        $this->ref_propinsi = $this->master . $this->ref_propinsi;
        $this->ref_jenis_sdmk = $this->master . $this->ref_jenis_sdmk;
    }

    public function object() {
        $this->db
                ->select('  
                    d.*,
                    p.nama_prov, 
                    j.nama_jenis_sdmk')
                ->from("$this->tbl_proyeksi as d")
                ->left_join("$this->ref_propinsi as p", 'p.no_prov = d.no_prov')
                ->left_join("$this->ref_jenis_sdmk as j", 'j.no_jenis_sdmk = d.no_jenis_sdmk');
        $order = ($this->request->post("order") && $this->request->post('by')) ? '' . $this->request->post('order') . ' ' . $this->request->post('by') : 'd.no_prov DESC, j.no_jenis_sdmk ASC';
        $this->db->order_by($order);
        ($this->request->post('search') != '') ? $this->db->like(array('p.nama_prov' => "%" . $this->request->post('search') . "%")) : '';
        ($this->request->post('s_no_prov') != '') ? $this->db->where('p.no_prov', $this->request->post('s_no_prov')) : '';
        ($this->request->post('s_no_jenis_sdmk') != '') ? $this->db->where('j.no_jenis_sdmk', $this->request->post('s_no_jenis_sdmk')) : '';
    }

    public function get_all($offset, $limit) {
        $this->object();
        return $this->db
                        ->limit($limit, $offset)
                        ->get()->result();
    }

    public function _print() {
        $this->db 
                ->from($this->ref_propinsi);
        $order = ($this->request->post("order") && $this->request->post('by')) ? '' . $this->request->post('order') . ' ' . $this->request->post('by') : 'no_prov ASC';
        $this->db->order_by($order);
        ($this->request->post('search') != '') ? $this->db->like(array('nama_prov' => "%" . $this->request->post('search') . "%")) : '';
        ($this->request->post('no_prov') != '') ? $this->db->where('no_prov', $this->request->post('no_prov')) : ''; 
    }
    
    public function get_print() {
        $this->_print();
        return $this->db
                        ->get()->result();
    }

    public function num_rows_report() {
        $this->object();
        return $this->db
                        ->get()->num_rows();
    }

    public function num_rows() {
        $this->object();
        return $this->db
                        ->get()->num_rows();
    }

    public function insert($values = array(), $tbl = null) {
        $tbls = ($tbl == null) ? $this->tbl_proyeksi : $tbl;
        $this->db
                        ->insert($values, $tbls);
    }

    public function get_where_id($id = null) {
        if ($id != null) {
            return $this->db
                            ->select('*')
                            ->from($this->tbl_proyeksi)
                            ->where(array('id_proyeksi' => $id))
                            ->get()->row();
        }
    }
    
    public function get_num_id($id = null) {
        if ($id != null) {
            return $this->db
                            ->select('*')
                            ->from($this->tbl_proyeksi)
                            ->where(array('id_proyeksi' => $id))
                            ->get()->num_rows();
        }
    }

    public function get_json_id($no_prov = null, $no_jenis_sdmk = null) {
        if ($no_prov != null && $no_jenis_sdmk != null) {
            $pr = $this->db
                    ->select('*')
                    ->from($this->tbl_proyeksi)
                    ->where(array('no_prov' => $no_prov, 'no_jenis_sdmk' => $no_jenis_sdmk))
                    ->get();
            if ($pr->num_rows() > 0) {
                return $pr->row_array();
            }
            return null;
        }
    }

    public function update($values = array(), $wheres = null, $tbl = null) {
        $tbls = ($tbl == null) ? $this->tbl_proyeksi : $tbl;
        return $this->db
                        ->update($values, $wheres, $tbls);
    }

    public function delete_where_id($id = null) {
        $this->db->delete(array('id_proyeksi' => $id), $this->tbl_proyeksi);
        $this->db->delete(array('id_proyeksi' => $id), $this->tbl_tahun_proyeksi);
    }

    public function delete_where_in($in = null) {
        $this->db->delete_in('id_proyeksi', array($in), $this->tbl_proyeksi);
        $this->db->delete_in('id_proyeksi', array($in), $this->tbl_tahun_proyeksi);
    }

    public function delete_tahun_where_in($in = null) {
        $this->db->delete_in('id_proyeksi', array($in), $this->tbl_tahun_proyeksi);
    }

    public function get_in_tahun_proyeksi($id_proyeksi) {
        $in = $this->db->select('kode_ref_proyeksi,th_2014, th_2015, th_2019, th_2020, th_2025')->from($this->tbl_tahun_proyeksi)->where(array(
                    'id_proyeksi' => $id_proyeksi,
                ))->get()->result();
        $data = array();
        foreach ($in as $r) { 
            $data['D2'.$r->kode_ref_proyeksi] = $r->th_2014;
            $data['E2'.$r->kode_ref_proyeksi] = $r->th_2015;
            $data['I2'.$r->kode_ref_proyeksi] = $r->th_2019;
            $data['J2'.$r->kode_ref_proyeksi] = $r->th_2020;
            $data['O2'.$r->kode_ref_proyeksi] = $r->th_2025;
        }
        return $data;
    }

    public function get_tahun_proyeksi($id_proyeksi, $kode_ref_proyeksi) {
        return $this->db->from($this->tbl_tahun_proyeksi)->where(array(
                    'id_proyeksi' => $id_proyeksi,
                    'kode_ref_proyeksi' => $kode_ref_proyeksi
                ))->get()->row();
    }

    public function get_propinsi() {
        return $this->db->get($this->ref_propinsi)->result();
    }

    public function get_proyeksi() {
        return $this->db->get($this->ref_proyeksi)->result();
    }

    public function get_jenis_sdmk() {
        return $this->db->get($this->ref_jenis_sdmk)->result();
    }

}
