<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
?> 
<title>Laporan - Pemasukan</title> 
<div class="xamzo-head-right"> 
    <div class="left">
        <h3>
            <span class="fa fa-bar-chart"></span> Proyeksi
        </h3> 
        <span class="xamzo-head-info">Halaman ini untuk mengatur semua data proyeksi</span>
    </div>
    <div class="right no-phone">   
        &nbsp;
    </div>
</div> 
<div id="xamzo-head-action">
    <button crud-name="check" class="btn primary large fa fa-check-square-o tip-top" original-title="<?php echo $this->lang['select_all'] ?>" table-check-all="true"></button>
    <?php if (is_auth('proyeksi', 'w')): ?>
        <button accesskey="ctrl+i" ajax="true" ajax-title="Input data proyeksi" url-push="true" href="#!proyeksi/create" ajax-target=".xamzo-right" class="btn primary large fa fa-plus tip-top" original-title="<?php echo $this->lang['create_data']; ?> (ctrl+i)"></button>
    <?php endif; ?>
    <?php if (is_auth('proyeksi', 'x')): ?>
        <button crud-name="delete" accesskey="ctrl+d" url-push="true" crud-url="proyeksi/delete" disabled="disabled" crud-redirect="proyeksi/index" crud-target=".xamzo-right" crud-data="#crud-data" crud-table-target="table.table" class="btn primary large fa fa-trash tip-top" original-title="<?php echo $this->lang['delete_data']; ?> (ctrl+d)"></button>
    <?php endif; ?>
    <?php if (is_auth('proyeksi', 'w')): ?>
        <button crud-name="update" url-push="true" crud-url="proyeksi/update" crud-target=".xamzo-right" disabled="disabled" crud-data="#crud-data" crud-table-target="table.table" class="btn large primary fa fa-edit tip-top" original-title="<?php echo $this->lang['update_data']; ?>" accesskey="ctrl+u"></button>
    <?php endif; ?>
    <?php if (is_auth('proyeksi', 'w')): ?>
        <button class="btn primary fa fa-print tip-top" original-title="print preview" ajax-title="Cetak Proyeksi" ajax="true" ajax-url="#!proyeksi/preview" ajax-target=".xamzo-right" url-push="true"></button>
    <?php endif; ?>
    <button crud-name="expand-search" accesskey="ctrl+f" class="btn primary large fa fa-expand tip-top" original-title="expand pencarian"></button> 
    <span class="xamzo-input-search">
        <input  type="name" name="search" value="<?php echo isset($search) ? $search : '' ?>" placeholder="<?php echo $this->lang['search'] ?>.." crud-search="true" search-url="proyeksi/search" search-target="#result-search" class="tip-bottom-left small no-phone" original-title="cari berdasarkan nama propinsi">
        <span class="small-loading"></span>
    </span>
    <button accesskey="ctrl+f5" crud-name="list" ajax="true" ajax-title="Proyeksi" url-push="true" href="#!proyeksi/index" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top-right" style="margin-right: 6px;" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div>  
<div id="crud-target">
    <div class="xamzo-block small">
        <div class="xamzo-block-content">
            <div class="xamzo-block-header">
                <h4><?php echo $this->lang['form']; ?> Pencarian</h4>
            </div>
            <div class="content">
                <form id="crud-form" class="print" onsubmit="return false;">
                    <input type="hidden" name="emitter" value="1">  
                    <p>
                        <label class="control-label">Jenis SDMK</label>
                        <?php echo form_dropdown($jenis_sdmk, isset($s_no_jenis_sdmk) ? $s_no_jenis_sdmk : '', array('name' => 's_no_jenis_sdmk', 'class' => 'combobox', 'validation' => 'required', 'placeholder' => 'jenis-sdmk', 'style' => 'width:50px'));?> 
                    </p>
                    <p>
                        <label class="control-label">Kode Propinsi</label>
                        <?php
                        echo form_dropdown($propinsi, isset($s_no_prov) ? $s_no_prov : '', array('name' => 's_no_prov', 'class' => 'combobox', 'placeholder' => 'semua-propinsi', 'style' => 'width:100%'));
                        ?>  
                    </p> 
                    <p class="modal-footer border-top"> 
                        <button class="btn primary medium" onclick="range_search('proyeksi/search');">Cari</button>
                        <a class="btn warning right medium" onclick="form_reset('#crud-form');"><?php echo $this->lang['reset'] ?></a>
                    </p>
                </form> 
            </div>    
        </div>    
    </div>
    <div class="xamzo-block separated"></div>
    <div class="xamzo-block big" id="result-search">
        <form id="crud-data">  
            <input type="hidden" name="by" value="<?php echo isset($by) ? $by : '' ?>">
            <input type="hidden" name="order" value="<?php echo isset($order) ? $order : '' ?>">
            <input type="hidden" name="search" value="<?php echo isset($search) ? $search : '' ?>">      
            <input type="hidden" name="s_no_prov" value="<?php echo isset($s_no_prov) ? $s_no_prov : '' ?>"> 
            <input type="hidden" name="s_no_jenis_sdmk" value="<?php echo isset($s_no_jenis_sdmk) ? $s_no_jenis_sdmk : '' ?>"> 
            <table id="rincome" class="sort table table-no-border" table-check="true" ajax-sort-url="proyeksi/sort" cellspacing="0" cellpadding="0">
                <tr class="ui-state-disabled" tabindex="0">
                    <th><input type="checkbox" table-check-all="true" id="check-all"> <label for="check-all"></label></th>
                    <th>
                        <span class="left">Nama Propinsi</span>  
                        <span class="right">
                            <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('nama_prov', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                            <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('nama_prov', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                        </span>
                    </th>
                    <th>
                        <span class="left">Jenis SDMK</span>  
                        <span class="right">
                            <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('nama_jenis_sdmk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                            <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('nama_jenis_sdmk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                        </span>
                    </th> 
                    <th>
                        <span class="left">(%) Ratio Penduduk/th</span>  
                        <span class="right">
                            <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('ratio', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                            <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('ratio', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                        </span>
                    </th>
                    <th>
                        <span class="left">Pengangkatan Baru</span>  
                        <span class="right">
                            <a class="tip-top" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('pengangkatan_baru', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                            <a class="tip-bottom" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('pengangkatan_baru', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                        </span>
                    </th>
                    <th colspan="2">
                        <span class="left">Pindah Masuk</span>  
                        <span class="right">
                            <a class="tip-top-right" original-title="<?php echo $this->lang['sort_by_ascending'] ?>" href="javascript:void(0)" onclick="_sort_asc('pindah_masuk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-up"></i></a>
                            <a class="tip-bottom-right" original-title="<?php echo $this->lang['sort_by_descending'] ?>" href="javascript:void(0)" onclick="_sort_desc('pindah_masuk', 'proyeksi/search', '#result-search')"><i class="fa fa-caret-down"></i></a>
                        </span>
                    </th>
                </tr>
                <?php
                $no = $offset + 1;
                if ($total > 0):
                    foreach ($rs as $row) {
                        $rowhighlight = ($no % 2 == 0) ? 'rowhighlight' : '';
                        ?>
                        <tr tabindex="<?php echo $no ?>" table-check-this="1" id="id_<?php echo $row->id_proyeksi ?>" class="ui-state-disabled <?php echo $rowhighlight; ?>">
                            <td> 
                                <p><input type="checkbox" name="id[]" value="<?php echo $row->id_proyeksi ?>" id="<?php echo $row->id_proyeksi ?>"> <label for ="<?php echo $row->id_proyeksi ?>"></label></p>
                            </td>
                            <td> 
                                <span class="dt-title"><?php echo $row->nama_prov ?></span>
                            </td>  
                            <td><?php echo $row->nama_jenis_sdmk ?></td> 
                            <td><?php echo $row->ratio ?></td>
                            <td><?php echo $row->pengangkatan_baru ?></td>
                            <td><?php echo $row->pindah_masuk ?></td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                        $no++;
                    }
                endif;
                ?>
            </table>
            <div class="xamzo-pagination">
                <div id="option">
                    <?php
                    $selected = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : '';
                    echo $this->lang['showing_1_to'] . form_dropdown($opt_limit, $selected, array('title' => 'limit', 'onchange' => 'change_limit(this)')) . "&nbsp;" . $this->lang['of'] . " $total " . $this->lang['entries']
                    ?> 
                </div>
                <div id="pagging">
                    <?php echo $paging; ?> 
                </div>
            </div>    
        </form> 
    </div>
</div>
<script type="text/javascript">
    close_box();
    $('.combobox').combobox(); 
</script>



