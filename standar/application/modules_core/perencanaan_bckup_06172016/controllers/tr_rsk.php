<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_rsk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('perencanaan_model','perencanaan');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Perencanaan";
		$data['subtitle'] = "Entry Data Perencanaan Kebutuhan Minimal SDMK Rumah Sakit Khusus";
		$data['rsk'] = $this->db->get_where('m_jenis_rsk', array('active'=>'Y'));
		$this->load->view('tr_rsk/index', $data);
	}

}
