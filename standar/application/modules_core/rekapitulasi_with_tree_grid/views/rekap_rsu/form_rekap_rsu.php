<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_rekapitulasi" action="<?php echo site_url('rekapitulasi/ajax_add')?>">
                <br>

                <!-- <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_rekapitulasi:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div> -->

                <div class="form-group">
                  <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value)?$value->tahun:'','rekapitulasi','rekapitulasi','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-4">
                    <?php echo Master::get_master_provinsi(isset($value)?$value->id_prov:'','rekapitulasi','rekapitulasi','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-4">
                    <?php echo Master::get_master_kabupaten(isset($value)?$value->id_kab:'','rekapitulasi','rekapitulasi','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Rumah Sakit</label>
                  <div class="col-md-4">
                    <?php echo Master::get_master_rumah_sakit(isset($value)?$value->kode_puskesmas:'','rekapitulasi','rekapitulasi','chosen-select form-control','required','inline');?>
                    
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-1">&nbsp;</label>
                  <div class="col-md-10">

                    <div class="form-group">
                      <label class="control-label col-md-2">Alamat</label>
                      <div class="col-md-6">
                        <input name="id" id="id" value="<?php echo isset($value)?$value->id_rekapitulasi:''?>" placeholder="Auto" class="form-control" type="text" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-2">Kelas RS</label>
                      <div class="col-md-2">
                        <input name="id" id="id" value="<?php echo isset($value)?$value->id_rekapitulasi:''?>" placeholder="Auto" class="form-control" type="text" readonly>
                      </div>
                      <label class="control-label col-md-2">Jumlah TT</label>
                      <div class="col-md-2">
                        <input name="id" id="id" value="<?php echo isset($value)?$value->id_rekapitulasi:''?>" placeholder="Auto" class="form-control" type="text" readonly>
                      </div>
                    </div>

                  </div>
                </div>

                <!-- <div class="form-group">
                  <label class="control-label col-md-2"></label>
                  <div class="col-md-9">

                  <span class="help-inline col-xs-12 col-sm-7">
                        <label class="middle">
                          <input class="ace" type="checkbox" id="id-disable-check" />
                          <span class="lbl"> Checklist untuk semua puskesmas</span>
                        </label>
                      </span>
                    
                  </div>
                </div> -->
                
                <!-- <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kawasan</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $data = Master::get_tipe_kawasan_data();
                          foreach($data as $row){
                        ?>
                          <label>
                            <input name="active" type="radio" class="ace" value="<?php echo $row['id_tipe_kawasan']?>" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> <?php echo $row['nama_tipe_kawasan']?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div> -->

                <!-- <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div> -->

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_rekapitulasi:0?>"> -->

                  <a onclick="getMenu('rekapitulasi')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/rekapitulasi.js'?>"></script>
