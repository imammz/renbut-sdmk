<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<div class="main-property">
    <button type="button" class="btn primary large tip-top" style="top:-3px;right:-1px;" original-title="<?php echo $this->lang['close'] ?>" onclick="close_property()">&times;</button> 
    <div class="xamzo-media-metadata">
        <span dir="<?php echo $file_url ?>"></span>        
    </div>
    <div class="tab" tab-parent=".my-tab1" tab-animation="bounce">
        <a class="active" tab-target="#information"><?php echo $this->lang['information'] ?></a>
        <a class="" tab-target="#action"><?php echo $this->lang['action'] ?></a> 
    </div>
    <div class="xamzo-media-metadata tab-target my-tab1">     
        <div class="child-tab" id="information" style="margin-top: -5px;"> 
            <p>
                <label class=".control-label small"><?php echo $this->lang['name'] ?></label>
                <?php echo str_replace('-', ' ', $name) ?>
            </p>
            <p>
                <label><?php echo $this->lang['date_uploaded'] ?></label>
                <?php echo str_replace('-', '/', $date) . ' ' . str_replace('-', ':', $time) ?>
            </p>
            <p>
                <label><?php echo $this->lang['uploaded_by'] ?></label>
                <?php echo $user ?>
            </p> 
            <p>
                <label><?php echo $this->lang['size_of_file'] ?></label>
                <?php echo round(intval($size) / 1000) ?> kb
            </p>
        </div>
        <div class="child-tab" id="action" style="margin-top: -5px;"> 
            <p>
                <span dir="<?php echo $file_url ?>"></span>        
                <a href="javascript:void(0)" onclick='file_delete(this);'><i class="fa fa-trash"></i> <?php echo $this->lang['delete_file'] ?></a>
            </p>
            <p>
                <a href="javascript:void(0)" onclick='file_rename(this,"<?php echo $type ?>");'><i class="fa fa-pencil"></i> <?php echo $this->lang['rename_file'] ?></a>
            </p> 
        </div>
    </div>
</div>