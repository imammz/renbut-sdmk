<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?>
<form id="crud-form">
   <?php
    if(count($con)>0){
        x_display_config($con,array('email'),'gmail');  
    } 
    ?>
    <p> 
        <label class="control-label big">&nbsp;</label>
        <button class="btn primary medium" href="<?php echo site_url('email/gmail') ?>" ajax="true" ajax-type="POST" ajax-target=".ca-right" ajax-redirect="<?php echo site_url('email'); ?>"  ajax-data="#crud-form"><?php echo $this->lang['submit'] ?></button>
    </p>
</form>