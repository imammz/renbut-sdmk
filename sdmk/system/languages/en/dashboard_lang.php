<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['edit_profile'] = 'edit profile';
$lang['change_password'] = 'change password';
$lang['this_page_to_manage_dashboard,_see_documentation_for_use_it'] = 'This page to manage dashboard, see documentation for use it';
$lang['blog_post'] = 'blog post'; 
$lang['comment'] = 'comment';
$lang['visitor'] = 'visitor';
$lang['language'] = 'language'; 
$lang['browsers'] = 'browsers';
$lang['countries'] = 'countries';
$lang['inbox'] = 'Inbox';
$lang['message'] = 'Message';
$lang['contact'] = 'Contact';