<?php

class Home extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
        
        unset($_SESSION['message']);
    }
    
    	public function index()
	{ 
            $this->output->enable_profiler(false);
            $this->checkLogin();
            
            $account_model = new account_model;

            $data = array();
            $data['class'] = 'home';
            $data['user'] = $account_model->_getLevelUser($_SESSION['user']['faskes_kode'], $_SESSION['user']['NO_KAB'], $_SESSION['user']['NO_PROV']);
            $this->load->view('home_view',$data);
	}
        
        
        
        public function info()
	{ 
            $this->output->enable_profiler(false);
            
          
            $data = array();
            $data['class'] = 'home';
            $this->load->view('info_view',$data);
	}
        
        
        public function rekap_puskesmas($NO_PROV) {
            
            header("Pragma: public");
header('Content-Type: application/vnd.ms-excel');
$filename = 'Provinsi '.$NO_PROV.'.xls';
header('Content-Disposition: attachment; filename=' . $filename);
            
            $this->orm->debug = TRUE;
            
            $this->db->select('*')->from('ref_kabupaten')->where('NO_PROV',$NO_PROV);
            $kab = $this->db->get()->result_array();
            
            $nakes = array('1101000','1102000','1301001','1301001','1502000','41990','1603000A1','00030A1','51990','1801000','130219','130319','130419');
            
            
            $data_nakes = $this->db->select('*')->from("ref_jenis_sdmk")->where_in('ref_jenis_sdmk_kode',$nakes)->get()->result_array();
            
            
            $html = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Sheet 1</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->';
            
            $html .= ' <html>';
            
            $html   .= '<h1>Data Keadaan Saat Ini Puskesmas</h1>'
                    . '<br/><table border="1"><tr><td> No </td><td> Nama Kabupaten/Kota </td><td> Jumlah Puskesmas </td>';
            
            
            foreach($data_nakes as $row) {
                $html .= '<td> '.$row['ref_jenis_sdmk_nama_level2'].' </td>';
            
            }
            
            $html .= '</tr>';
            $no = 1;
            foreach ($kab as $row) {
                
                $pkm = $this->db->select("count(Kode_puskesmas) as jml")->from("ref_puskesmas")->where("NO_KAB",$row['NO_KAB'])->get()->row_array();
                
                $html .= "<tr><td> $no </td><td> {$row['NAMA_KAB']} </td><td>{$pkm['jml']}</td>";
                
                 foreach($data_nakes as $row_nakes) {
                    
                            $kab_faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ?  AND LEFT(ref_jenis_faskes_kode,1) = ?', $row['NO_KAB'],1)->order('NO_KAB ASC');
                            $get_faskes_installasi = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode', $kab_faskes);
                            $faskes_jumlah = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as saat_ini, SUM(hasil_kebutuhan_sdmk_pembulatan) as seharusnya ')->where('faskes_installasi_id', $get_faskes_installasi)->and('ref_jenis_sdmk_kode', $row_nakes['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $faskes_jumlah['saat_ini'];
                            $kesenjangan = $jumlah_saat_ini - $faskes_jumlah['seharusnya'];
                     
                            
                $html .= "<td>  {$faskes_jumlah['saat_ini']} </td>";
                
            
            
            }
            $no++;
                $html .= '</tr>';
            }
            
            
            $html .= '</table><hr/><br/>';
            
            
            
            $html   .= '<h1>Data Kebutuhan Berdasarkan ABK di Puskesmas</h1>'
                    . '<br/><table border="1"><tr><td> No </td><td> Nama Kabupaten/Kota </td><td> Jumlah Puskesmas </td>';
            
            
            foreach($data_nakes as $row) {
                $html .= '<td> '.$row['ref_jenis_sdmk_nama_level2'].' </td>';
            
            }
            
            $html .= '</tr>';
            $no = 1;
            foreach ($kab as $row) {
                
                $pkm = $this->db->select("count(Kode_puskesmas) as jml")->from("ref_puskesmas")->where("NO_KAB",$row['NO_KAB'])->get()->row_array();
                
                $html .= "<tr><td> $no </td><td> {$row['NAMA_KAB']} </td><td>{$pkm['jml']}</td>";
                
                 foreach($data_nakes as $row_nakes) {
                    
                            $kab_faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ?  AND LEFT(ref_jenis_faskes_kode,1) = ?', $row['NO_KAB'],1)->order('NO_KAB ASC');
                            $get_faskes_installasi = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode', $kab_faskes);
                            $faskes_jumlah = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as saat_ini, SUM(hasil_kebutuhan_sdmk_pembulatan) as seharusnya ')->where('faskes_installasi_id', $get_faskes_installasi)->and('ref_jenis_sdmk_kode', $row_nakes['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $faskes_jumlah['saat_ini'];
                            $kesenjangan = $jumlah_saat_ini - $faskes_jumlah['seharusnya'];
                     
                            
                $html .= "<td>  {$faskes_jumlah['seharusnya']} </td>";
                
            
            
            }
            $no++;
                $html .= '</tr>';
            }
            
            
            $html .= '</table><hr/><br/>';
            
            
            
               $html   .= '<h1>Data Kesenjangan di Puskesmas</h1>'
                    . '<br/><table border="1"><tr><td> No </td><td> Nama Kabupaten/Kota </td><td> Jumlah Puskesmas </td>';
            
            
            foreach($data_nakes as $row) {
                $html .= '<td> '.$row['ref_jenis_sdmk_nama_level2'].' </td>';
            
            }
            
            $html .= '</tr>';
            $no = 1;
            foreach ($kab as $row) {
                
                $pkm = $this->db->select("count(Kode_puskesmas) as jml")->from("ref_puskesmas")->where("NO_KAB",$row['NO_KAB'])->get()->row_array();
                
                $html .= "<tr><td> $no </td><td> {$row['NAMA_KAB']} </td><td>{$pkm['jml']}</td>";
                
                 foreach($data_nakes as $row_nakes) {
                    
                            $kab_faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ?  AND LEFT(ref_jenis_faskes_kode,1) = ?', $row['NO_KAB'],1)->order('NO_KAB ASC');
                            $get_faskes_installasi = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode', $kab_faskes);
                            $faskes_jumlah = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as saat_ini, SUM(hasil_kebutuhan_sdmk_pembulatan) as seharusnya ')->where('faskes_installasi_id', $get_faskes_installasi)->and('ref_jenis_sdmk_kode', $row_nakes['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $faskes_jumlah['saat_ini'];
                            $kesenjangan = $jumlah_saat_ini - $faskes_jumlah['seharusnya'];
                     
                            
                $html .= "<td>  {$kesenjangan} </td>";
                
            
            
            }
            $no++;
                $html .= '</tr>';
            }
            
            
            $html .= '</table><hr/><br/>';
            
            
            
            $html .= '</html>';
            
            
            echo $html;
            
            
        }
        
        
        
          public function rekap_rumahsakit($NO_PROV) {
            
            header("Pragma: public");
header('Content-Type: application/vnd.ms-excel');
$filename = 'Provinsi '.$NO_PROV.'.xls';
header('Content-Disposition: attachment; filename=' . $filename);
            
            $this->orm->debug = TRUE;
            
            $this->db->select('*')->from('ref_kabupaten')->where('NO_PROV',$NO_PROV);
            $kab = $this->db->get()->result_array();
            
            $nakes = array('1101000','1102000','1301001','1301001','1502000','41990','1603000A1','00030A1','51990','1801000','130219','130319','130419');
            
            
            $data_nakes = $this->db->select('*')->from("ref_jenis_sdmk")->where_in('ref_jenis_sdmk_kode',$nakes)->get()->result_array();
            
            
            $html = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name>Sheet 1</x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->';
            
            $html .= ' <html>';
            
            $html   .= '<h1>Data Keadaan Saat Ini Rumah Sakit</h1>'
                    . '<br/><table border="1"><tr><td> No </td><td> Nama Kabupaten/Kota </td><td> Jumlah Rumah Sakit </td>';
            
            
            foreach($data_nakes as $row) {
                $html .= '<td> '.$row['ref_jenis_sdmk_nama_level2'].' </td>';
            
            }
            
            $html .= '</tr>';
            $no = 1;
            foreach ($kab as $row) {
                
                $pkm = $this->db->select("count(kode_rs) as jml")->from("ref_rumahsakit")->where("no_kab",$row['NO_KAB'])->get()->row_array();
                
                $html .= "<tr><td> $no </td><td> {$row['NAMA_KAB']} </td><td>{$pkm['jml']}</td>";
                
                 foreach($data_nakes as $row_nakes) {
                    
                            $kab_faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ?  AND LEFT(ref_jenis_faskes_kode,1) = ?', $row['NO_KAB'],2)->order('NO_KAB ASC');
                            $get_faskes_installasi = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode', $kab_faskes);
                            $faskes_jumlah = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as saat_ini, SUM(hasil_kebutuhan_sdmk_pembulatan) as seharusnya ')->where('faskes_installasi_id', $get_faskes_installasi)->and('ref_jenis_sdmk_kode', $row_nakes['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $faskes_jumlah['saat_ini'];
                            $kesenjangan = $jumlah_saat_ini - $faskes_jumlah['seharusnya'];
                     
                            
                $html .= "<td>  {$faskes_jumlah['saat_ini']} </td>";
                
            
            
            }
            $no++;
                $html .= '</tr>';
            }
            
            
            $html .= '</table><hr/><br/>';
            
            
            
            $html   .= '<h1>Data Kebutuhan Berdasarkan ABK di Rumah Sakit</h1>'
                    . '<br/><table border="1"><tr><td> No </td><td> Nama Kabupaten/Kota </td><td> Jumlah Rumah Sakit </td>';
            
            
            foreach($data_nakes as $row) {
                $html .= '<td> '.$row['ref_jenis_sdmk_nama_level2'].' </td>';
            
            }
            
            $html .= '</tr>';
            $no = 1;
            foreach ($kab as $row) {
                
               $pkm = $this->db->select("count(kode_rs) as jml")->from("ref_rumahsakit")->where("no_kab",$row['NO_KAB'])->get()->row_array();
                
                $html .= "<tr><td> $no </td><td> {$row['NAMA_KAB']} </td><td>{$pkm['jml']}</td>";
                
                 foreach($data_nakes as $row_nakes) {
                    
                            $kab_faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ?  AND LEFT(ref_jenis_faskes_kode,1) = ?', $row['NO_KAB'],2)->order('NO_KAB ASC');
                            $get_faskes_installasi = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode', $kab_faskes);
                            $faskes_jumlah = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as saat_ini, SUM(hasil_kebutuhan_sdmk_pembulatan) as seharusnya ')->where('faskes_installasi_id', $get_faskes_installasi)->and('ref_jenis_sdmk_kode', $row_nakes['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $faskes_jumlah['saat_ini'];
                            $kesenjangan = $jumlah_saat_ini - $faskes_jumlah['seharusnya'];
                     
                            
                $html .= "<td>  {$faskes_jumlah['seharusnya']} </td>";
                
            
            
            }
            $no++;
                $html .= '</tr>';
            }
            
            
            $html .= '</table><hr/><br/>';
            
            
            
               $html   .= '<h1>Data Kesenjangan di Rumah Sakit</h1>'
                    . '<br/><table border="1"><tr><td> No </td><td> Nama Kabupaten/Kota </td><td> Jumlah Rumah Sakit </td>';
            
            
            foreach($data_nakes as $row) {
                $html .= '<td> '.$row['ref_jenis_sdmk_nama_level2'].' </td>';
            
            }
            
            $html .= '</tr>';
            $no = 1;
            foreach ($kab as $row) {
                
             $pkm = $this->db->select("count(kode_rs) as jml")->from("ref_rumahsakit")->where("no_kab",$row['NO_KAB'])->get()->row_array();
                   
                $html .= "<tr><td> $no </td><td> {$row['NAMA_KAB']} </td><td>{$pkm['jml']}</td>";
                
                 foreach($data_nakes as $row_nakes) {
                    
                            $kab_faskes = $this->orm->faskes->select('faskes_kode')->where('NO_KAB = ?  AND LEFT(ref_jenis_faskes_kode,1) = ?', $row['NO_KAB'],2)->order('NO_KAB ASC');
                            $get_faskes_installasi = $this->orm->faskes_installasi->select('faskes_installasi_id')->where('faskes_kode', $kab_faskes);
                            $faskes_jumlah = $this->orm->sdmk_faskes_installasi->select('SUM(jumlah_PNS) as saat_ini, SUM(hasil_kebutuhan_sdmk_pembulatan) as seharusnya ')->where('faskes_installasi_id', $get_faskes_installasi)->and('ref_jenis_sdmk_kode', $row_nakes['ref_jenis_sdmk_kode'])->fetch();

                            //$jumlah_saat_ini = $sdmk_faskes_installasi['jumlah_PNS'] + $sdmk_faskes_installasi['jumlah_PPPK'] + $sdmk_faskes_installasi['jumlah_PTT'] + $sdmk_faskes_installasi['jumlah_HD'] + $sdmk_faskes_installasi['jumlah_DLL'];
                            $jumlah_saat_ini = $faskes_jumlah['saat_ini'];
                            $kesenjangan = $jumlah_saat_ini - $faskes_jumlah['seharusnya'];
                     
                            
                $html .= "<td>  {$kesenjangan} </td>";
                
            
            
            }
            $no++;
                $html .= '</tr>';
            }
            
            
            $html .= '</table><hr/><br/>';
            
            
            
            $html .= '</html>';
            
            
            echo $html;
            
            
        }
        
        

        
        

}

?>