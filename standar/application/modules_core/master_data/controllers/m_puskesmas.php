<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_puskesmas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_puskesmas_model','puskesmas');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Puskesmas";
		$data['subtitle'] = "Daftar Puskesmas";
		$data['total_rows_existing'] = $this->puskesmas->count_all();
		$data['total_rows_server'] = $this->puskesmas->get_total_rows_server();
		$this->load->view('puskesmas/index', $data);
	}

	public function form($id='')
	{
		if($id==''){
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}
		$data['title'] = "Form Puskesmas";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->puskesmas->get_by_id($id);
		}
		//echo '<pre>';print_r($data);die;
		$this->load->view('puskesmas/form', $data);
	}

	public function sinkronisasi()
	{
		
		$data['title'] = "Sinkronisasi Data Puskesmas";
		$data['subtitle'] = "Download data puskesmas";
		
		$this->load->view('puskesmas/form_sinkronisasi', $data);
	}

	public function ajax_list()
	{
		$list = $this->puskesmas->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $puskesmas) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($puskesmas->kode_puskesmas);
			$row[] = strtoupper($puskesmas->nama_puskesmas_kab);
			$row[] = $puskesmas->nama_tipe_puskesmas;
			$row[] = $puskesmas->nama_kabupaten;
			$row[] = ($puskesmas->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $puskesmas->updated_date?Tanggal::formatDateTime($puskesmas->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($puskesmas->id_puskesmas,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_puskesmas('."'".Regex::_genRegex($puskesmas->id_puskesmas,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->puskesmas->count_all(),
						"recordsFiltered" => $this->puskesmas->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_puskesmas = Regex::_genRegex($this->input->post('id_puskesmas'), 'RGXINT');

		$this->db->trans_begin();
		

		$dataexc = array(
			'kode_puskesmas' => Regex::_genRegex($this->input->post('kode_puskesmas'), 'RGXQSL'),
			'kode_puskesmas_kab' => 'P'.Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT').'',
			'id_kabupaten' => Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXQSL'),
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT'),
			'nama_puskesmas_kab' => Regex::_genRegex($this->input->post('nama_puskesmas_kab'), 'RGXQSL'),
			'id_tipe_puskesmas' => Regex::_genRegex($this->input->post('id_tipe_puskesmas'), 'RGXINT'),
			'id_tipe_kawasan' => Regex::_genRegex($this->input->post('id_tipe_kawasan'), 'RGXINT'),
			'alamat' => Regex::_genRegex($this->input->post('alamat'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ')
			
		);
		
		if( $id_puskesmas == 0 ){
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$dataexc['created_by'] = $this->session->userdata('data_user')->fullname;
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$this->puskesmas->save($dataexc);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->fullname;
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$this->puskesmas->update(array('id_puskesmas'=>$id_puskesmas), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function proses_sinkronisasi()
	{

		$this->db->trans_begin();

		$files = file_get_contents('uploaded_files/data/puskesmas.json');
        $data = json_decode($files);
        //echo '<pre>';print_r($data->rows);

        $this->db->truncate('m_puskesmas');

        foreach ($data->rows as $key => $value) {
        	
        	$field = array(
        		'kode_puskesmas' => $value->KD_PUSKESMAS,
        		'kode_puskesmas_kab' => 'P'.$value->KD_PROPINSI.''.$value->KD_KABUPATEN.'',
        		'id_provinsi' => $value->KD_PROPINSI,
        		'id_kabupaten' => ''.$value->KD_PROPINSI.''.$value->KD_KABUPATEN.'',
        		'nama_puskesmas_kab' => $value->NAMA_PUSKESMAS,
        		'id_tipe_puskesmas' => $value->JENIS_PUSKEMAS,
        		'alamat' => $value->ALAMAT,
        		'active' => 'Y',
        		'created_date' => date('Y-m-d H:i:s'),
        		'created_by' => $this->session->userdata('data_user')->fullname,
        		'updated_date' => date('Y-m-d H:i:s'),
        		'updated_by' => $this->session->userdata('data_user')->fullname,
        		);
        	//print_r($field);die;
        	$this->db->insert('m_puskesmas', $field);
        
        }

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->puskesmas->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function autocomplate()
	{
		$keyword = $this->input->get('q'); //print_r($_GET['keyword']);die;
		$data = $this->puskesmas->get_by_keyword($keyword);
		
		echo json_encode($data);
		exit;
	}

	function get_puskes_by_kode($kode_puskesmas) {
		
        $this->db->select('m_puskesmas.*, m_tipe_puskesmas.nama_tipe_puskesmas, m_tipe_kawasan.nama_tipe_kawasan');
        $this->db->from('m_puskesmas');
        $this->db->join('m_tipe_puskesmas', 'm_tipe_puskesmas.id_tipe_puskesmas=m_puskesmas.id_tipe_puskesmas','left');
        $this->db->join('m_tipe_kawasan', 'm_tipe_kawasan.id_tipe_kawasan=m_puskesmas.id_tipe_kawasan','left');
        $this->db->where('kode_puskesmas', $kode_puskesmas);
        $this->db->order_by('nama_puskesmas_kab', 'ASC');
        
        $result = $this->db->get()->row_array();
        $string = '<div class="form-group" id="form-puskesmas" >
                  <label class="control-label col-md-2">Kode Puskesmas</label>
                  <div class="col-md-2" style="padding-top: 9px">
                    : '.$result['kode_puskesmas'   ].'
                  </div>
                  <label class="control-label col-md-2">Tipe Puskesmas</label>
                  <div class="col-md-2" style="padding-top: 9px">
                    : '.$result['nama_tipe_kawasan'].'
                  </div>
                </div>
                <div class="form-group" id="form-puskesmas" >
                  <label class="control-label col-md-2">Alamat</label>
                  <div class="col-md-9" style="padding-top: 9px">
                    : '.$result['alamat'].'
                  </div>
                </div>';
        echo json_encode(array('string'=>$string));
        exit;
    }

    function get_puskes_by_kode_json($kode_puskesmas) {
		
        $this->db->select('m_puskesmas.*, m_tipe_puskesmas.nama_tipe_puskesmas, m_tipe_kawasan.nama_tipe_kawasan');
        $this->db->from('m_puskesmas');
        $this->db->join('m_tipe_puskesmas', 'm_tipe_puskesmas.id_tipe_puskesmas=m_puskesmas.id_tipe_puskesmas','left');
        $this->db->join('m_tipe_kawasan', 'm_tipe_kawasan.id_tipe_kawasan=m_puskesmas.id_tipe_kawasan','left');
        $this->db->where('kode_puskesmas', $kode_puskesmas);
        $this->db->order_by('nama_puskesmas_kab', 'ASC');
        
        $result = $this->db->get()->row_array();
        echo json_encode($result);
    }

	
}
