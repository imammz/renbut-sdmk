<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?>  
<script type="text/javascript" src="<?php echo site_url('application/modules/media/assets/javascript/script.js') ?>"></script>
<li id="<?php echo $id ?>"> 
    <img class="x-media tip-bottom-left" original-title="<?php echo $title ?>" src="<?php echo site_url("upload/i/" . $nfile); ?>"/>
    <span dir="<?php echo site_url("upload/i/" . str_replace('thumb_b', 'thumb_s', $nfile)); ?>">
        <div class="box-image-act">
            <div class="left">
                <input table-check-all='true' type="checkbox" onclick="_self_check(this);" name="id[]" value="<?php echo site_url("upload/i/" . $nfile); ?>">
            </div>
            <div class="right">
                <a href="javascript:void(0)" onclick="_append_to_image_editor_('li#<?php echo $id ?>');" class="tip-top" original-title="<?php echo $this->lang["add_to_editor"]; ?>"><i class='fa fa-plus'></i></a> 
                <a href="javascript:void(0)" onclick="img_property($(this).parent().parent().parent());" class="tip-top" original-title="<?php echo $this->lang["preview"]; ?> "><i class='fa fa-search'></i></a>
                <a href="javascript:void(0)" onclick="img_delete(this);" class="tip-top" original-title="<?php echo $this->lang["delete"]; ?>"><i class='fa fa-trash'></i></a> 
            </div>
        </div>
</li>