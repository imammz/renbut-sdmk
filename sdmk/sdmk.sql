/*
Navicat MySQL Data Transfer

Source Server         : localhost-mysql
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : sdmk

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2015-10-28 23:27:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ref_jenis_sdmk
-- ----------------------------
DROP TABLE IF EXISTS `ref_jenis_sdmk`;
CREATE TABLE `ref_jenis_sdmk` (
  `no_jenis_sdmk` smallint(2) NOT NULL AUTO_INCREMENT,
  `nama_jenis_sdmk` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`no_jenis_sdmk`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_jenis_sdmk
-- ----------------------------
INSERT INTO `ref_jenis_sdmk` VALUES ('1', 'Dokter Spesialis');
INSERT INTO `ref_jenis_sdmk` VALUES ('2', 'Dokter Umum');
INSERT INTO `ref_jenis_sdmk` VALUES ('3', 'Dokter Gigi');
INSERT INTO `ref_jenis_sdmk` VALUES ('4', 'Perawat');
INSERT INTO `ref_jenis_sdmk` VALUES ('5', 'Bidan');
INSERT INTO `ref_jenis_sdmk` VALUES ('6', 'Perawat Gigi');
INSERT INTO `ref_jenis_sdmk` VALUES ('7', 'Apoteker');
INSERT INTO `ref_jenis_sdmk` VALUES ('8', 'Ass. Apoteker');
INSERT INTO `ref_jenis_sdmk` VALUES ('9', 'SKM');
INSERT INTO `ref_jenis_sdmk` VALUES ('10', 'Sanitarian');
INSERT INTO `ref_jenis_sdmk` VALUES ('11', 'Nutrision / Ahli Gizi');
INSERT INTO `ref_jenis_sdmk` VALUES ('12', 'Keterapian Fisik');
INSERT INTO `ref_jenis_sdmk` VALUES ('13', 'Keterapian Medis');

-- ----------------------------
-- Table structure for ref_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `ref_provinsi`;
CREATE TABLE `ref_provinsi` (
  `no_prov` smallint(2) NOT NULL,
  `nama_prov` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`no_prov`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_provinsi
-- ----------------------------
INSERT INTO `ref_provinsi` VALUES ('11', 'Aceh');
INSERT INTO `ref_provinsi` VALUES ('12', 'Sumatera Utara');
INSERT INTO `ref_provinsi` VALUES ('13', 'Sumatera Barat');
INSERT INTO `ref_provinsi` VALUES ('14', 'Riau');
INSERT INTO `ref_provinsi` VALUES ('15', 'Jambi');
INSERT INTO `ref_provinsi` VALUES ('16', 'Sumatera Selatan');
INSERT INTO `ref_provinsi` VALUES ('17', 'Bengkulu');
INSERT INTO `ref_provinsi` VALUES ('18', 'Lampung');
INSERT INTO `ref_provinsi` VALUES ('19', 'Kep. Bangka Belitung ');
INSERT INTO `ref_provinsi` VALUES ('21', 'Kep. Riau');
INSERT INTO `ref_provinsi` VALUES ('31', 'DKI Jakarta ');
INSERT INTO `ref_provinsi` VALUES ('32', 'Jawa Barat');
INSERT INTO `ref_provinsi` VALUES ('33', 'Jawa Tengah');
INSERT INTO `ref_provinsi` VALUES ('34', 'DI Jogja2');
INSERT INTO `ref_provinsi` VALUES ('35', 'Jawa Timur');
INSERT INTO `ref_provinsi` VALUES ('36', 'Banten');
INSERT INTO `ref_provinsi` VALUES ('51', 'Bali');
INSERT INTO `ref_provinsi` VALUES ('52', 'Nusa Tenggara Barat');
INSERT INTO `ref_provinsi` VALUES ('53', 'Nusa Tenggara Timur');
INSERT INTO `ref_provinsi` VALUES ('61', 'Kalimantan Barat');
INSERT INTO `ref_provinsi` VALUES ('62', 'Kalimantan Tengah');
INSERT INTO `ref_provinsi` VALUES ('63', 'Kalimantan Selatan');
INSERT INTO `ref_provinsi` VALUES ('64', 'Kalimantan Timur');
INSERT INTO `ref_provinsi` VALUES ('65', 'Kalimantan Utara');
INSERT INTO `ref_provinsi` VALUES ('71', 'Sulawesi Utara');
INSERT INTO `ref_provinsi` VALUES ('72', 'Sulawesi Tengah');
INSERT INTO `ref_provinsi` VALUES ('73', 'Sulawesi Selatan');
INSERT INTO `ref_provinsi` VALUES ('74', 'Sulawesi Tenggara');
INSERT INTO `ref_provinsi` VALUES ('75', 'Gorontalo');
INSERT INTO `ref_provinsi` VALUES ('76', 'Sulawesi Barat');
INSERT INTO `ref_provinsi` VALUES ('81', 'Maluku');
INSERT INTO `ref_provinsi` VALUES ('82', 'Maluku Utara');
INSERT INTO `ref_provinsi` VALUES ('91', 'Papua Barat');
INSERT INTO `ref_provinsi` VALUES ('94', 'Papua ');

-- ----------------------------
-- Table structure for ref_proyeksi
-- ----------------------------
DROP TABLE IF EXISTS `ref_proyeksi`;
CREATE TABLE `ref_proyeksi` (
  `kode_ref_proyeksi` smallint(2) NOT NULL AUTO_INCREMENT,
  `nama_ref_proyeksi` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`kode_ref_proyeksi`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ref_proyeksi
-- ----------------------------
INSERT INTO `ref_proyeksi` VALUES ('1', 'Penduduk pada tahun awal');
INSERT INTO `ref_proyeksi` VALUES ('2', 'Target Rasio Nakes 2014, 2019, 2025');
INSERT INTO `ref_proyeksi` VALUES ('3', 'Kebutuhan Nakes berdasarkan Rasio Penduduk	');
INSERT INTO `ref_proyeksi` VALUES ('4', 'Jumlah tenaga');
INSERT INTO `ref_proyeksi` VALUES ('5', 'Pengangkatan baru');
INSERT INTO `ref_proyeksi` VALUES ('6', 'Pindah masuk');
INSERT INTO `ref_proyeksi` VALUES ('7', 'Keseluruhan masuk');
INSERT INTO `ref_proyeksi` VALUES ('8', 'Pensiun');
INSERT INTO `ref_proyeksi` VALUES ('9', 'Meninggal dan tdk mampu bekerja karena sakit');
INSERT INTO `ref_proyeksi` VALUES ('10', 'Keluar-cuti besar-dipecat');
INSERT INTO `ref_proyeksi` VALUES ('11', 'Keseluruhan keluar');
INSERT INTO `ref_proyeksi` VALUES ('12', 'Tenaga (Perawat) yang ada diakhir tahun');
INSERT INTO `ref_proyeksi` VALUES ('13', 'Kesenjangan Kebutuhan Nakes');

-- ----------------------------
-- Table structure for sdmk_proyeksi
-- ----------------------------
DROP TABLE IF EXISTS `sdmk_proyeksi`;
CREATE TABLE `sdmk_proyeksi` (
  `id_proyeksi` int(11) NOT NULL AUTO_INCREMENT,
  `no_prov` smallint(2) DEFAULT NULL,
  `no_jenis_sdmk` smallint(2) DEFAULT NULL,
  `ratio` varchar(5) DEFAULT NULL,
  `pengangkatan_baru` varchar(5) DEFAULT NULL,
  `pindah_masuk` varchar(5) DEFAULT NULL,
  `pensiun` varchar(5) DEFAULT NULL,
  `meninggal_tdkmampu_cacat` varchar(5) DEFAULT NULL,
  `keluar_cuti_dipecat` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_proyeksi`),
  KEY `idx_proyeksi_1` (`no_prov`),
  KEY `idx_proyeksi_2` (`no_jenis_sdmk`)
) ENGINE=InnoDB AUTO_INCREMENT=1431098795 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sdmk_proyeksi
-- ----------------------------
INSERT INTO `sdmk_proyeksi` VALUES ('1', '35', '4', '0.75', '6.81', '0.50', '1.00', '0.50', '1.00');

-- ----------------------------
-- Table structure for sdmk_tahun_proyeksi
-- ----------------------------
DROP TABLE IF EXISTS `sdmk_tahun_proyeksi`;
CREATE TABLE `sdmk_tahun_proyeksi` (
  `id_proyeksi` int(11) DEFAULT NULL,
  `kode_ref_proyeksi` smallint(2) DEFAULT NULL,
  `th_2014` varchar(15) DEFAULT NULL,
  `th_2015` varchar(15) DEFAULT NULL,
  `th_2016` varchar(15) DEFAULT NULL,
  `th_2017` varchar(15) DEFAULT NULL,
  `th_2018` varchar(15) DEFAULT NULL,
  `th_2019` varchar(15) DEFAULT NULL,
  `th_2020` varchar(15) DEFAULT NULL,
  `th_2021` varchar(15) DEFAULT NULL,
  `th_2022` varchar(15) DEFAULT NULL,
  `th_2023` varchar(15) DEFAULT NULL,
  `th_2024` varchar(15) DEFAULT NULL,
  `th_2025` varchar(15) DEFAULT NULL,
  KEY `idx_th_proyeksi_1` (`id_proyeksi`),
  KEY `idx_th_proyeksi_2` (`kode_ref_proyeksi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sdmk_tahun_proyeksi
-- ----------------------------
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '1', '170000', '171275', '172559.5625', '173853.75921875', '175157.66241289', '176471', '177794.5325', '179127.99149375', '180471.45142995', '181824.98731567', '183188.67472054', '184563');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '2', '158', '162.4', '166.8', '171.20000000000', '175.60000000000', '180', '183.33333333333', '186.66666666666', '190.00000000000', '193.33333333333', '196.66666666666', '200');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '3', '268.6', '278.1506', '287.828412', '297.63633600000', '307.575692', '317.6478', '325.95566666666', '334.3704', '342.89490000000', '351.52640000000', '360.26973333333', '369.12600000000');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '4', '216', '226.3896', '237.2602', '248.6599', '263.9256', '276.5759', '289.8515', '303.75239999999', '318.32669999999', '333.62249999999', '349.63979999999', '366.42669999999');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '5', '14.709599999999', '15.3906', '16.139699999999', '20.2257', '17.9103', '18.795599999999', '19.680899999999', '20.6343', '21.6558', '22.6773', '23.766899999999', '24.924599999999');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '6', '1.08', '1.1300000000000', '1.185', '1.24', '1.315', '1.3800000000000', '1.445', '1.5150000000000', '1.59', '1.665', '1.745', '1.83');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '7', '15.789599999999', '16.520599999999', '17.324699999999', '21.4657', '19.2253', '20.175599999999', '21.125899999999', '22.1493', '23.2458', '24.342299999999', '25.511899999999', '26.754599999999');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '8', '2.16', '2.2600000000000', '2.37', '2.48', '2.63', '2.7600000000000', '2.89', '3.0300000000000', '3.18', '3.33', '3.49', '3.66');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '9', '1.08', '1.1300000000000', '1.185', '1.24', '1.315', '1.3800000000000', '1.445', '1.5150000000000', '1.59', '1.665', '1.745', '1.83');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '10', '2.16', '2.2600000000000', '2.37', '2.48', '2.63', '2.7600000000000', '2.89', '3.0300000000000', '3.18', '3.33', '3.49', '3.66');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '11', '5.4', '5.65', '5.9250000000000', '6.1999999999999', '6.5749999999999', '6.9', '7.225', '7.575', '7.9500000000000', '8.325', '8.7250000000000', '9.15');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '12', '226.3896', '237.2602', '248.6599', '263.9256', '276.5759', '289.8515', '303.75239999999', '318.32669999999', '333.62249999999', '349.63979999999', '366.42669999999', '384.03129999999');
INSERT INTO `sdmk_tahun_proyeksi` VALUES ('1', '13', '42.210400000000', '40.8904', '39.168512000000', '33.710736000000', '30.999792000000', '27.796300000000', '22.203266666666', '16.043700000000', '9.2724000000001', '1.8866000000001', '-6.156966666666', '-14.90529999999');

-- ----------------------------
-- Table structure for sdmk_user
-- ----------------------------
DROP TABLE IF EXISTS `sdmk_user`;
CREATE TABLE `sdmk_user` (
  `id` int(11) NOT NULL,
  `level` enum('admin','user') COLLATE utf8_bin DEFAULT 'user',
  `fullname` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `born` text COLLATE utf8_bin,
  `birth_day` date DEFAULT NULL,
  `sex` enum('1','0') COLLATE utf8_bin DEFAULT '1',
  `photo` varchar(225) COLLATE utf8_bin DEFAULT 'upload/pp/default_x_user.jpg',
  `address` text COLLATE utf8_bin,
  `registred` datetime NOT NULL,
  `description` varchar(225) COLLATE utf8_bin DEFAULT NULL,
  `publish` enum('0','1') COLLATE utf8_bin DEFAULT '1',
  `order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `lspg_fk13` (`fullname`,`photo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of sdmk_user
-- ----------------------------
INSERT INTO `sdmk_user` VALUES ('1374050643', 'admin', 'Administrator', 'administrator@sdmk.io', '200ceb26807d6bf99fd6f4f0d1ca54d4', 0x4A616B61727461, '1990-11-28', '1', 'upload/pp/xamzo{x}me{x}2015-10-22{x}12-47-48{x}1374050643_pp.jpg', 0x2D, '2015-01-20 00:00:00', '', '1', '0');
INSERT INTO `sdmk_user` VALUES ('1430679644', 'user', 'Regular User', 'regularuser@sdmk.io', '7f4f225298a70f22e1e46645d3f5e60f', 0x4A616B61727461, '1990-11-28', '1', 'upload/pp/default_x_user.jpg', null, '2015-05-04 02:01:22', null, '1', '0');
