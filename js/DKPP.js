$(document).ready(function() {
	"use-strict";
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
					return false;
				}
			}
		});
	});
	
	var dlgtrigger = document.querySelector( '[data-dialog]' ),
			somedialog = document.getElementById( dlgtrigger.getAttribute( 'data-dialog' ) ),
			dlg = new DialogFx( login );

	dlgtrigger.addEventListener( 'click', dlg.toggle.bind(dlg) );
});

// URL
var urlWeb = "http://www.dkpp.go.id/web/";
var urlEPengaduan = "http://sipepp.info/e-pengaduan/";
var urlEPersidangan = "http://sipepp.info/e-persidangan/";
var urlEPersuratan = "http://sipepp.info/e-persuratan/";
var urlEKearsipan = "http://sipepp.info/e-arsip/";
var urlSistemEksekutif = "http://sipepp.info/";
var urlEAkun = "http://sipepp.info/_config/e-account/beranda";
var urlMasterData = "http://sipepp.info/_config/masterdata/beranda";
var urlESetting = "http://sipepp.info/_config/e-setting/beranda";

function goToWeb() {
	window.location = urlWeb;
}
function goToEPengaduan() {
	window.location = urlEPengaduan;
}
function goToEPersidangan() {
	window.location = urlEPersidangan;
}
function goToEPersuratan() {
	window.location = urlEPersuratan;
}
function goToEKearsipan() {
	window.location = urlEKearsipan;
}
function goToSistemEksekutif() {
	window.location = urlSistemEksekutif;
}
function goToEAkun() {
	window.location = urlEAkun;
}
function goToMasterData() {
	window.location = urlMasterData;
}
function goToESetting() {
	window.location = urlESetting;
}

/* 
START DATA CHART
*/
// DATA PENGADUAN
var dataPengaduan = {
	labels: ['Wilayah 1', 'Wilayah 2', 'Wilayah 3'],
	series: [20, 15, 40]
};
// Data Persidangan
var dataPersidangan = {
	labels: ['Rehabilitasi', 'Diberhentikan Sementara', 'Dipecat'],
	series: [497, 13, 207]
};
// Data Persuratan
var dataPersuratan = {
	labels: ['Surat Pemberitahuan Ke Pengadu', 'Disposisi Pengaduan', 'Undangan Rapat Verifikasi','Nota Dinas Hasil Rapat Verifikasi','Surat Pemberitahuan Hasil Rapat Verifikasi','Nota Dinas Pelimpahan Berkas','Surat Panggilan Sidang','Undangan Rapat Pleno','Surat Penyampaian Putusan'],
	series: [20, 15, 40,30,15,29,19,25,17]
};
// Data Kearsipan
var dataKearsipan = {
	labels: ['Hubungan Masyarakat', 'Penanganan Pengaduan Dugaan Pelanggaran Kode Etik Penyelenggara Pemilu', 'Penanganan Perkara Pelanggaran Kode Etik Penyelenggara Pemilu','Sosialisasi Pencegahan Pelanggaran Kode Etik Penyelenggara Pemilu'],
	series: [25, 65, 40,28]
};
// Data Kearsipan
var dataSistemEksekutif = {
	labels: ['Pengaduan Dismiss', 'Pengaduan Naik Perkara'],
	series: [450, 320]
};

var options = {
	labelInterpolationFnc: function(value) {
		return value[0]
	}
};

var responsiveOptions = [
	['screen and (min-width: 640px)', {
		chartPadding: 30,
		labelOffset: 100,
		labelDirection: 'explode',
		labelInterpolationFnc: function(value) {
			return value;
		}
	}],
	['screen and (min-width: 1024px)', {
		labelOffset: 80,
		chartPadding: 20
	}]
];

new Chartist.Pie('.chart-pengaduan', dataPengaduan, options, responsiveOptions);
new Chartist.Pie('.chart-persidangan', dataPersidangan, options, responsiveOptions);
new Chartist.Pie('.chart-persuratan', dataPersuratan, options, responsiveOptions);
new Chartist.Pie('.chart-kearsipan', dataKearsipan, options, responsiveOptions);
new Chartist.Pie('.chart-sistem-eksekutif', dataSistemEksekutif, options, responsiveOptions);
// END DATA CHART