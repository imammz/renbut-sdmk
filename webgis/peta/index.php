<!DOCTYPE html>
<html lang="en">
<?php $base_url="http://".$_SERVER['SERVER_NAME'];?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Google Maps</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
    <script src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=drawing&geometry"></script>
    <script src="../js/peta.js"></script>
	<link href="../js/chosen/chosen.css" rel="stylesheet">
	<script src="../js/chosen/chosen.jquery.js"></script>
	<script src="../js/chosen/prism.js" type="text/javascript" charset="utf-8"></script>
		<script src="../assets/js/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
	<script src="../assets/js/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
	<script src="../assets/js/plugins/amcharts/amcharts/exporting/amexport.js" type="text/javascript"></script>
	<script src="../assets/js/plugins/amcharts/amcharts/exporting/rgbcolor.js" type="text/javascript"></script>
	<script src="../assets/js/plugins/amcharts/amcharts/exporting/canvg.js" type="text/javascript"></script>
	<script src="../assets/js/plugins/amcharts/amcharts/exporting/jspdf.js" type="text/javascript"></script>
	<script src="../assets/js/plugins/amcharts/amcharts/exporting/filesaver.js" type="text/javascript"></script>
	<script src="../assets/js/plugins/amcharts/amcharts/exporting/jspdf.plugin.addimage.js" type="text/javascript"></script>
	<script>
		
$(document).ready(function(){
   $('.chosen-select', this).chosen({search_contains:true});
   $('#status').chosen({search_contains:true});
   $('#id_provinsi').chosen({search_contains:true});
});
	</script>	
</head>
<body onload="peta_awal()">
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
		<div class="navbar-header">
		  <a class="navbar-brand" href="#">Kebutuhan Tenaga Kesehatan (Dengan Metode ABK)</a>
		</div>
		<ul class="nav navbar-nav">
		  <li class="active"><a href="<?=$base_url?>/new_polygon/peta">Wilayah</a></li>
		  <li><a href="<?=$base_url?>/new_polygon/lokasi">Lokasi</a></li>
		  <li><a href="<?=$base_url?>/new_polygon/m_lokasi">Master Lokasi</a></li>
		</ul>
	  </div>
	</nav>
    <div class="container">
        <div class="row">
           <div class="col-lg-12">
                <?php require('../include/connect.php'); ?>
                <h2>Peta Tenaga Kesehatan (Dengan Metode ABK)</h2>
				<!--
				<div class="navbar-form pull-right" >
					<div class="input-group">
						<input type="text" name="status2" id="status2" placeholder="Search Status" class="form-control">
						<div class="input-group-btn">
							<button type="button" id="search2" class="btn btn-default">Search</button>
						</div>
					</div>
				</div>-->
            	<table class="table" border="0">
            		<tr>
                		<td style="width:215px">
                    		<select name="id_provinsi" id="id_provinsi" class="form-control" class="chosen-select">
								<option value="all">Semua Provinsi</option>
    		                	<?php 
        		                	$sql = mysql_query("SELECT * FROM `provinsi` order by nama_provinsi");
        		                	while($val = mysql_fetch_array($sql)) {
        		                		echo '<option value="'.$val['id_provinsi'].'">'.$val['nama_provinsi'].'</option>';
        		                	}
    		                	?>
                    		</select>
                		</td>
                		
                		<td>
                			<button type="button" id="search" class="btn btn-primary">Cari Lokasi Kesehatan</button>
                		</td>
                    </tr>
            	</table>
				<div class="row">
				<div id="grafik2" class="col-xs-6"></div>
				<div id="grafik" class="col-xs-6"></div>
				
				</div>
                <div id="map-canvas" style="width:100%; height:400px;"></div>
				
            </div>
        </div>
        <hr>
    </div>
   </body>
</html>
