<input type="hidden" name="emitter" value="1">
<span class="error"><?php echo isset($login_error) ? '<p align="center">' . $login_error . '</p>' : ''; ?></span>                        
<div class="xamzo-username">
    <span class="xamzo-form-icon"><i class="fa fa-envelope" style="font-size: 14px;position: relative;top: -3px;"></i></span>
    <input placeholder="Email" type="text" name="email" value="<?php echo isset($default['email']) ? $default['email'] : '' ?>"> 
</div>
<span class="error"><?php echo isset($errors['email']) ? $errors["email"] : ''; ?></span>
<div class="xamzo-password">
    <span class="xamzo-form-icon"><i class="fa fa-lock"></i></span>
    <input type="password" id="password" name="password" placeholder="Password" >                          
</div>
<span class="error" id="capsplock" style="display: none;"><?php echo $this->lang['capital_letter_is_on'] ?></span> 
<span class="error"><?php echo isset($errors['password']) ? $errors['password'] : ''; ?></span>
<button  class="btn glow primary" ajax-url="#!user/login/" ajax="true"  is_close_box="true" ajax-data="#crud-login" ajax-target="form#crud-login" ajax-type="POST"><?php echo $this->lang['login'] ?></button>
<div class="xamzo-keep">&nbsp;&nbsp; <a ajax="true" href="#!user/forgot" ajax-target="modalbox" modal-width="400px" modal-fix="true" title="<?php echo $this->lang['forgot_password'] ?>"><?php echo $this->lang['forgot_password'] ?>?</a></div>