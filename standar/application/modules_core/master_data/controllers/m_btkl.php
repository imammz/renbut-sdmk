<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_btkl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('btkl_model','btkl');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan BTKL";
		$data['subtitle'] = "Daftar BTKL";
		$this->load->view('btkl/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form BTKL";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->btkl->get_by_id($id);
		}
		$this->load->view('btkl/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->btkl->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $btkl) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.strtoupper($btkl->kode_btkl).' ]';
			$row[] = strtoupper($btkl->nama_btkl);
			$row[] = $btkl->nama_provinsi;
			$row[] = $btkl->nama_kabupaten;
			$row[] = $btkl->alamat;
			$row[] = $btkl->no_telp;
			$row[] = $btkl->kelas_btkl;
			$row[] = ($btkl->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $btkl->updated_date?Tanggal::formatDateTime($btkl->updated_date):Tanggal::formatDateTime($btkl->created_date);

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($btkl->id_btkl,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_btkl('."'".Regex::_genRegex($btkl->id_btkl,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->btkl->count_all(),
						"recordsFiltered" => $this->btkl->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	
	public function sinkronisasi()
	{
		
		$data['title'] = "Sinkronisasi Data Puskesmas";
		$data['subtitle'] = "Download data puskesmas";
		
		$this->load->view('btkl/form_sinkronisasi', $data);
	}

	public function ajax_add()
	{
		$id_btkl = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'kode_btkl' => Regex::_genRegex($this->input->post('kode_btkl'), 'RGXQSL'),
			'nama_btkl' => Regex::_genRegex($this->input->post('nama_btkl'), 'RGXQSL'),
			'kelas_btkl' => Regex::_genRegex($this->input->post('kelas_btkl'), 'RGXQSL'),
			'id_kabupaten' => Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXQSL'),
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXQSL'),
			'alamat' => Regex::_genRegex($this->input->post('alamat'), 'RGXQSL'),
			'no_telp' => Regex::_genRegex($this->input->post('no_telp'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ')
		);
		
		if( $id_btkl == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->id_user;
			$this->btkl->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->id_user;
			$this->btkl->update(array('id_btkl'=>$id_btkl), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->btkl->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
