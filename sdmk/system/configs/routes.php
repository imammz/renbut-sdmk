<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		configs/route
 * @author		xamzo Dev Team 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

$route["/"] = "dashboard/h"; 
$route['about'] = "dashboard/page/about"; 
$route['privacy'] = "dashboard/page/privacy"; 
$route['h'] = 'dashboard/h'; 
$route['login'] = 'user/login'; 
$route['register'] = 'user/register'; 
$route['logout'] = 'user/logout'; 