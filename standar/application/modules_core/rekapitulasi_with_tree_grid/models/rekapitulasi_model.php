<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi_model extends CI_Model {

	var $table = 't_perencanaan';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _query($params){

		
		$this->db->from('t_perencanaan');
		$this->db->join('(SELECT nama_puskesmas_kab,kode_puskesmas FROM m_puskesmas) as m_puskesmas', 'm_puskesmas.kode_puskesmas=t_perencanaan.kode_puskesmas','left');
		$this->db->join('m_provinsi','m_provinsi.id_provinsi=t_perencanaan.id_provinsi','left');
		$this->db->join('m_kabupaten','m_kabupaten.id_kabupaten=t_perencanaan.id_kabupaten','left');
		$this->db->where($params);

	}

	/*public function get_rekapitulasi($params)
	{

		// get perencanaan by params
		
		$this->db->select('m_kabupaten.id_kabupaten as main_id, m_puskesmas.id_puskesmas as parent_id, m_provinsi.nama_provinsi, m_kabupaten.nama_kabupaten, m_puskesmas.nama_puskesmas_kab, t_perencanaan.*');
		$this->_query($params);
		$perencanaan = $this->db->get()->result();
		foreach ($perencanaan as $key => $value) {
			# code...
			$sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$value->id_perencanaan,'tahun'=>$value->tahun, 'id_jenis_faskes'=>$value->id_jenis_faskes, 'id_tipe_puskesmas'=>$value->id_tipe_puskesmas,'id_tipe_kawasan'=>$value->id_tipe_kawasan));
			$value->sdmk = $sdmk;
			$getData[] = $value;
		}
		echo '<pre>';
		print_r($getData);die;


		
		return $getData;

	
	}*/

	public function get_rekapitulasi($params)
	{

		// get perencanaan by params
		
		$this->db->select('m_provinsi.nama_provinsi,t_perencanaan.id_provinsi');
		$this->_query($params);
		$this->db->group_by('m_provinsi.id_provinsi');
		$by_prov = $this->db->get()->result(); 

		$getData = array();
		$getDataPerencanaan = array();
		$getDataKab = array();

		foreach($by_prov as $row_by_prov){
			$this->db->select('m_kabupaten.nama_kabupaten,t_perencanaan.id_perencanaan,t_perencanaan.id_provinsi, t_perencanaan.id_kabupaten');
			$this->_query($params);
			$this->db->where(array('t_perencanaan.id_provinsi'=>$row_by_prov->id_provinsi));
			$this->db->group_by('m_kabupaten.id_kabupaten');
			$kabupaten = $this->db->get()->result(); 

			foreach ($kabupaten as $row_by_kab) {
				$this->db->select('m_puskesmas.nama_puskesmas_kab,t_perencanaan.*');
				$this->_query($params);
				$this->db->where(array('t_perencanaan.id_kabupaten'=>$row_by_kab->id_kabupaten));

				$perencanaan = $this->db->get()->result(); //print_r($this->db->last_query());die;

				foreach ($perencanaan as $key => $value) {
					# code...
					$sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$value->id_perencanaan,'tahun'=>$value->tahun, 'id_jenis_faskes'=>$value->id_jenis_faskes, 'id_tipe_puskesmas'=>$value->id_tipe_puskesmas,'id_tipe_kawasan'=>$value->id_tipe_kawasan));
					$value->sdmk = $sdmk;
					$getData[] = $value;
				}
				
				$row_by_kab->puskesmas = $perencanaan;
				$getDataKab[] = $row_by_kab;
			}

			//$row_by_prov->kabupaten = $kabupaten;
			$row_by_prov->kabupaten = $getDataKab;
			$getDataPerencanaan[] = $row_by_prov;
		}

		//echo '<pre>';print_r($getDataPerencanaan);die;
		return $getDataPerencanaan;

	
	}

	public function get_rekapitulasi_ori($params)
	{

		// get perencanaan by params
		
		$this->db->select('m_provinsi.nama_provinsi,t_perencanaan.id_provinsi');
		$this->_query($params);
		$this->db->group_by('m_provinsi.id_provinsi');
		$by_prov = $this->db->get()->result(); 

		$getData = array();
		$getDataPerencanaan = array();
		$getDataKab = array();

		foreach($by_prov as $row_by_prov){
			$this->db->select('m_kabupaten.nama_kabupaten,t_perencanaan.id_perencanaan,t_perencanaan.id_provinsi, t_perencanaan.id_kabupaten');
			$this->_query($params);
			$this->db->where(array('t_perencanaan.id_provinsi'=>$row_by_prov->id_provinsi));
			$this->db->group_by('m_kabupaten.id_kabupaten');
			$kabupaten = $this->db->get()->result(); 

			foreach ($kabupaten as $row_by_kab) {
				$this->db->select('m_puskesmas.nama_puskesmas_kab,t_perencanaan.*');
				$this->_query($params);
				$this->db->where(array('t_perencanaan.id_kabupaten'=>$row_by_kab->id_kabupaten));

				$perencanaan = $this->db->get()->result(); //print_r($this->db->last_query());die;

				foreach ($perencanaan as $key => $value) {
					# code...
					$sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_perencanaan'=>$value->id_perencanaan,'tahun'=>$value->tahun, 'id_jenis_faskes'=>$value->id_jenis_faskes, 'id_tipe_puskesmas'=>$value->id_tipe_puskesmas,'id_tipe_kawasan'=>$value->id_tipe_kawasan));
					$value->sdmk = $sdmk;
					$getData[] = $value;
				}
				
				$row_by_kab->puskesmas = $perencanaan;
				$getDataKab[] = $row_by_kab;
			}

			$row_by_prov->kabupaten = $kabupaten;
			$getDataPerencanaan[] = $row_by_prov;
		}

		//echo '<pre>';print_r($getDataPerencanaan);die;
		return $getDataPerencanaan;

	
	}

	public function get_perencanaan($params)
	{
		//print_r($params);die;
		

		return $getData;
	
	}


}
