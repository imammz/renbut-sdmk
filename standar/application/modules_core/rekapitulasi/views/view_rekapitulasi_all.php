<script>

function total_kabupaten(classname)

 {
 var items = document.getElementsByClassName(""+classname+"");
    var itemCount = items.length;
    var total = 0;
    for(var i = 0; i < itemCount; i++)
    {
        total = total +  parseInt(items[i].value);
    }

    document.getElementById(classname).value = total;

 }

  function myFunctionJml(idp, keys, idkab) {

    //alert(idp);return false;
    // JML PARENT //
    var jml = document.getElementById('jml_'+idp+'_'+keys+'_'+idkab); 
    jml = jml.value?jml.value:0;
    //alert(jml);return false;
    total_kabupaten('jml_'+keys+'_'+idkab);
}

</script>

<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT -->

    <center>
      <h2>Laporan Perencanaan Kebutuhan Minimal SDM Kesehatan<br> 
        <?php 
          echo ($posted['id_provinsi'] != 0) ? 'Provinsi '.Master::params_query(array('table'=>'m_provinsi', 'where'=>array('id_provinsi'=>$posted['id_provinsi'])))->row()->nama_provinsi : '';
          echo ($posted['id_kabupaten'] != 0) ? ', Kabupaten/Kota '.Master::params_query(array('table'=>'m_kabupaten', 'where'=>array('id_kabupaten'=>$posted['id_kabupaten'])))->row()->nama_kabupaten.'<br>' : '';
          if($id_jenis_faskes==1){
            echo ($posted['id_tipe_kawasan'] != 0) ? ' Kawasan '.Master::params_query(array('table'=>'m_tipe_kawasan', 'where'=>array('id_tipe_kawasan'=>$posted['id_tipe_kawasan'])))->row()->nama_tipe_kawasan : '';
            echo ($posted['id_tipe_puskesmas'] != 0) ? '   Kategori '.Master::params_query(array('table'=>'m_tipe_puskesmas', 'where'=>array('id_tipe_puskesmas'=>$posted['id_tipe_puskesmas'])))->row()->nama_tipe_puskesmas.'<br>' : '';
          }
          echo 'Tahun '.$posted['tahun']
        ?>
      </h2>
    </center>
      
    <div class="form-actions center">

      <button class="btn btn-sm btn-info" onclick="getMenu('rekapitulasi/form_rekapitulasi/<?php echo $id_jenis_faskes?>')"><i class="fa fa-arrow-left"></i> Kembali ke rekapitulasi </button>
      <a href="<?php echo base_url().'rekapitulasi/preview_all/excel/'.$posted['tahun'].'/'.$posted['id_provinsi'].'/'.$posted['id_kabupaten'].'/'.$posted['id_tipe_kawasan'].'/'.$posted['id_tipe_puskesmas'].'/'.$posted['id_jenis_faskes'].'/'.$isall.''?>" target="blank" class="btn btn-sm btn-success">
        <i class="ace-icon fa fa-file-excel-o icon-on-right bigger-110"></i>
        Excel
      </a>
      <a href="<?php echo base_url().'rekapitulasi/preview_all/json/'.$posted['tahun'].'/'.$posted['id_provinsi'].'/'.$posted['id_kabupaten'].'/'.$posted['id_tipe_kawasan'].'/'.$posted['id_tipe_puskesmas'].'/'.$posted['id_jenis_faskes'].'/'.$isall.''?>" class="btn btn-sm btn-inverse" value="submit">
        <i class="ace-icon fa fa-upload icon-on-right bigger-110"></i>
        Export
      </a>
    </div>

    <div id="form_import"></div>
    

    <table id="example2" class="table table-striped table-bordered table-hover" style="font-size:12px">
      <thead style="color:black">
        <tr>
          <th rowspan="2" class="center">&nbsp;&nbsp;&nbsp;No&nbsp;&nbsp;&nbsp;</th>
          <th class="center" rowspan="2">Nama Faskes</th>
          <?php
            if($posted['id_jenis_faskes'] == 1){
                $jenis_sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_jenis_faskes'=>isset($posted['id_jenis_faskes'])?$posted['id_jenis_faskes']:'','id_tipe_puskesmas'=>isset($posted['id_tipe_puskesmas'])?$posted['id_tipe_puskesmas']:'', 'id_tipe_kawasan'=>isset($posted['id_tipe_kawasan'])?$posted['id_tipe_kawasan']:'','tahun'=>isset($posted['tahun'])?$posted['tahun']:''));
            }else{
                $jenis_sdmk = $this->jenis_sdmk->get_sdmk_standar(array('id_jenis_faskes'=>isset($posted['id_jenis_faskes'])?$posted['id_jenis_faskes']:'','tahun'=>isset($posted['tahun'])?$posted['tahun']:'', 'kategori_kelas'=>isset($posted['kategori_kelas'])?$posted['kategori_kelas']:''));
            }
            
            //echo '<pre>'; print_r($jenis_sdmk);die;
            //print_r($this->db->last_query());die;
            $no=1;
            foreach ($jenis_sdmk as $key => $value) {
          ?>
          <th class="center" colspan="3"><?php echo ''.$no.'. '.$value->nama_jenis_sdmk.'';?></th>
            
            <?php $alp='a'; foreach($value->sub_parent as $key2=>$value2){?>
              <th class="center" colspan="3"><?php echo ''.$no.'.'.$alp.' '.$value2->nama_jenis_sdmk.'';?></th>
            <?php $alp++; }?>
          
          <?php $no++; }?>
        </tr>
        <tr>
          <?php

            $jenis_sdmk2 = Master::get_jenis_sdmk_data(isset($posted['id_jenis_faskes'])?$posted['id_jenis_faskes']:'');
            $no=1;
            foreach ($jenis_sdmk2 as $key2 => $value2) {
          ?>
          <th class="center">Jumlah SDMK Saat Ini</th>
          <th class="center">Standar SDMK</th>
          <th class="center">Kesenjangan</th>
          <?php $no++; }?>
        </tr>
      </thead>

      <tbody>
        
        <tr class="center">
          <td><b>[ 1 ]</b></td>
          <td><b>[ 2 ]</b></td>
          <?php
            $count = 3;
            for ($i=$count; $i < $no*3; $i++) {
          ?>
          <td><b>[ <?php echo $i?> ]</b></td>
        <?php } ?>
        </tr>

        <?php
          $count_puskes = 1; 
          //$puskesmas = Master::get_puskesmas_data(); //print_r($puskesmas);die;
          $params_array = array();
          $params_array['t_perencanaan.tahun'] = $posted['tahun'];
          $params_array['t_perencanaan.id_jenis_faskes'] = $posted['id_jenis_faskes'];

          if( $posted['id_jenis_faskes'] == 1 ){
              if( $posted['id_tipe_kawasan'] != 0){
                $params_array['t_perencanaan.id_tipe_kawasan'] = $posted['id_tipe_kawasan'];
              }

              if( $posted['id_tipe_puskesmas'] != 0){
                $params_array['t_perencanaan.id_tipe_puskesmas'] = $posted['id_tipe_puskesmas'];
              }

          }

          if( in_array($posted['id_jenis_faskes'], array('3','30','29','31')) ){
            if( $posted['kategori_kelas'] != ''){
                $params_array['kategori_kelas'] = $posted['kategori_kelas'];
            }
          }

          if( $posted['id_provinsi'] != 0){
            $params_array['t_perencanaan.id_provinsi'] = $posted['id_provinsi'];
          }

          if( $posted['id_kabupaten'] != 0){
            $params_array['t_perencanaan.id_kabupaten'] = $posted['id_kabupaten'];
          }
          //print_r($params_array);die;
          $rekapitulasi = $this->rekapitulasi->get_rekapitulasi($params_array); 
          /*echo '<pre>';print_r($rekapitulasi);;
          print_r($this->db->last_query());die;*/
        ?>

        <!-- PROVINSI -->
        <?php foreach ($rekapitulasi as $key3 => $value3) {?>

          <tr style="background-color: #0b57a1; color: white ">
            <td class="center"><?php echo $count_puskes?></td>
            <td><?php echo '<strong>PROV. '.strtoupper($value3->nama_provinsi).'</strong>'?></td>
            <?php
                $count = 3;
                for ($j=$count; $j < $no*3; $j++) {
              ?>
              <td class="center"> 0 </b></td>
            <?php } ?>
          </tr>

          <!-- KABUPATEN -->
          <?php foreach ($value3->kabupaten as $key4 => $value4) {?>

            <tr style="background-color: #6fb3e0; color: white ">
              <td class="center">&nbsp;</td>
              <td> <?php echo '<strong>'.$value4->nama_kabupaten.'</strong>'?></td>
              <?php
                $count = 3;
                for ($k=$count; $k < $no*3; $k++) {
              ?>
              <td class="center">0</td>
            <?php } ?>
            </tr>

              <!-- PUSKESMAS -->
              <?php foreach ($value4->puskesmas as $key5 => $value5) {?>
                        
                  <tr>
                    <td class="center" style="width: 100px">&nbsp;</td>
                    <td><a href="#" onclick="view_rekap_detail(<?php echo "'$value5->tahun','$value5->id_provinsi','$value5->id_kabupaten','$value5->kode_puskesmas','$value5->id_tipe_kawasan','$value5->id_tipe_puskesmas','$value5->id_jenis_faskes','N','$value5->kategori_kelas'"?>)"><?php echo $value5->nama_puskesmas_kab?></a></td>
                    <!-- <td><?php echo $value5->nama_puskesmas_kab?></td> -->

                    <?php
                      $count_from_kab=3; 
                      foreach ($value5->sdmk as $key_row_rekap=>$row_rekap_sdmk) {
                        if( in_array($row_rekap_sdmk->id_jenis_sdmk, array('10420','10410','212244') )){ // perawat pustu
                          $jml_standar = $row_rekap_sdmk->total_standar;
                        }else{
                          $jml_standar = $row_rekap_sdmk->jml_standar;
                        }
                        $selisih_main = $row_rekap_sdmk->total_jml - $jml_standar;
                        $p_saat_ini[$key_row_rekap][] = $row_rekap_sdmk->total_jml;
                        $p_standar[$key_row_rekap][] = $jml_standar;
                        $p_selisih[$key_row_rekap][] = $selisih_main;
                    ?>

                    <td class="center">
                      <input type="hidden" onKeyUp="myFunctionJml(<?php echo "'$value5->id_perencanaan','$row_rekap_sdmk->id_jenis_sdmk','$value5->id_kabupaten'"?>)" class="jml_<?php echo $row_rekap_sdmk->id_jenis_sdmk?>_<?php echo $value5->id_kabupaten?>" id="jml_<?php echo $value5->id_perencanaan?>_<?php echo $row_rekap_sdmk->id_jenis_sdmk?>_<?php echo $value5->id_kabupaten?>" value="<?php echo ($row_rekap_sdmk->total_jml!='')?$row_rekap_sdmk->total_jml:0?>">
                      <?php echo ($row_rekap_sdmk->total_jml!='')?$row_rekap_sdmk->total_jml:0?> 
                    </td>

                    <td class="center">
                      <input type="hidden" onKeyUp="myFunctionStandar(<?php echo $row_rekap_sdmk->id_jenis_sdmk?>)" class="standar_<?php echo $row_rekap_sdmk->id_jenis_sdmk?>_<?php echo $value5->id_kabupaten?>" id="standar<?php echo $row_rekap_sdmk->id_jenis_sdmk?>" value="<?php echo $jml_standar?$jml_standar:0?>">
                      <?php echo $jml_standar?$jml_standar:0?>
                    </td>

                    <td class="center" >
                      <input type="hidden" onKeyUp="myFunctionKesenjangan(<?php echo $row_rekap_sdmk->id_jenis_sdmk?>)" class="kesenjangan_<?php echo $row_rekap_sdmk->id_jenis_sdmk?>_<?php echo $value5->id_kabupaten?>" id="kesenjangan<?php echo $selisih_main?$selisih_main:0?>">
                      <?php echo Apps::get_format($selisih_main?$selisih_main:0)?>
                    </td>

                    <!-- SUB -->
                    <?php
                      $no_sub = 1;
                      foreach ($row_rekap_sdmk->sub_parent as $key_sub_rekap_row=>$sub_row_rekap_sdmk) {
                        $selisih = $sub_row_rekap_sdmk->total_jml - 0;
                        $sub_saat_ini[$key_sub_rekap_row][] = $sub_row_rekap_sdmk->total_jml;
                        $sub_standar[$key_sub_rekap_row][] = ($sub_row_rekap_sdmk->jml_standar)?$sub_row_rekap_sdmk->jml_standar:$sub_row_rekap_sdmk->total_standar;
                        $sub_selisih[$key_sub_rekap_row][] = $selisih;
                    ?>

                    <td class="center">
                      <?php echo $sub_row_rekap_sdmk->total_jml?$sub_row_rekap_sdmk->total_jml:0?> 
                    </td>

                    <td class="center">
                      <!-- <?php echo ($sub_row_rekap_sdmk->jml_standar)?$sub_row_rekap_sdmk->jml_standar:($sub_row_rekap_sdmk->total_standar)?$sub_row_rekap_sdmk->total_standar:0?> -->
                      -
                    </td>

                    <td class="center">
                      <!-- <?php echo Apps::get_format($selisih?$selisih:0)?> -->
                      -
                    </td>
                  <?php 
                    $no_sub++; } 
                  ?>
                  <!-- END SUB -->

                  <?php 
                      
                    $count_from_kab++; }
                  ?> 

                  </tr>


              <?php }?> <!--END PUSKESMAS-->

          <?php }?> <!--END KABUPATEN-->

        <?php $count_puskes++; }?> <!--END PROVINSI-->


      </tbody>
    </table>  
    <!-- PAGE CONTENT ENDS -->
  <?php //echo '<pre>';print_r($p_saat_ini);die;?>
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/rekapitulasi.js'?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example2').dataTable( {
        "scrollX": true,
        "orderable": false,
        "paging":   true,
        "ordering": false,
        "info":     false,
        "fixedHeader": true
    } );
} );
</script>