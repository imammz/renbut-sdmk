<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 12px;
}
a {
    color: #990000;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: none;
}
</style>
</head>
<body>
    <div style="padding-left: 20px; padding-right: 20px;">
        <h2> 
             || <a href="<?php echo base_url() ?>index.php/config/sdmk/all"> Semua </a> 
          <?php
          $sdmk = $this->db->query('select ref_jenis_sdmk_nama_level1 from ref_jenis_sdmk group by ref_jenis_sdmk_nama_level1')->result_array();
          
          foreach($sdmk as $row) {
          ?>
            
         || <a href="<?php echo base_url() ?>index.php/config/sdmk/jenis/<?php echo $row['ref_jenis_sdmk_nama_level1'] ?>"> <?php echo $row['ref_jenis_sdmk_nama_level1'] ?> </a>
        
          <?php } ?>
        </h2>
    <h3><?php echo $title; ?></h3>
    </div>
    
	<div>
		<?php echo $output; ?>
    </div>
</body>
</html>
