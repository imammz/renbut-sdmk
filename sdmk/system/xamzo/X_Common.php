<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		xamzo/common
 * @author		hadinug, xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
if (!function_exists('load_class')) {

    function load_class($class, $directory = 'libraries') {
        $cls = array();
        if (is_array($class)) {
            foreach ($class as $f) {
                $cls[$f] = BASEPATH . $directory . '/' . $f . EXT;
            }
        } else {
            $cls[$class] = BASEPATH . $directory . '/' . $class . EXT;
        }
        foreach ($cls as $class) {
            if (!class_exists($class)) { 
                include_once $class;
            }
        }
    }

}

if (!function_exists('load_helper')) {

    function load_helper($file, $directory = 'helpers') {
        if (is_array($file)) {
            foreach ($file as $f) {
                $file = BASEPATH . $directory . '/' . $f . '_helper' . EXT;
                if (is_file($file)) {
                    include_once $file;
                }
            }
        } else {
            $file = BASEPATH . $directory . '/' . $file . '_helper' . EXT;
            if (is_file($file)) {
                include_once $file;
            }
        }
    }

}


if (!function_exists('show_error')) {

    function show_error($message, $template = 'error_general', $status_code = 500, $heading = 'An Error Was Encountered') {        
        // showing the error
        $X = new Exceptions();
        echo $X->show_error($heading, $message, $template, $status_code);
        exit;
    }

}

if (!function_exists('set_status_header')) {

    function set_status_header($code = 200, $text = '') {
        $stat = array(
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );

        if ($code == '' OR !is_numeric($code)) {
            show_error('Status codes must be numeric', 500);
        }

        if (isset($stat[$code]) AND $text == '') {
            $text = $stat[$code];
        }

        if ($text == '') {
            show_error('No status text available.  Please check your status code number or supply your own message text.', 500);
        }

        $server_protocol = (isset($_SERVER['SERVER_PROTOCOL'])) ? $_SERVER['SERVER_PROTOCOL'] : FALSE;

        if (substr(php_sapi_name(), 0, 3) == 'cgi') {
            header("Status: {$code} {$text}", TRUE);
        } elseif ($server_protocol == 'HTTP/1.1' OR $server_protocol == 'HTTP/1.0') {
            header($server_protocol . " {$code} {$text}", TRUE, $code);
        } else {
            header("HTTP/1.1 {$code} {$text}", TRUE, $code);
        }
    }

}

load_class(array('X_Loader', 'X_Controller', 'X_Model', 'X_Router'), 'libraries/core');
load_class('Exceptions', 'libraries/error');
load_helper(array('app', 'language', 'database', 'file', 'security'));
load_class('X_Base', 'xamzo');
include_once BASEPATH . 'configs/routes' . EXT;

// define for default setting
define('URL_EXT', x_app_config('url_extension'));
define('X_LANG', x_app_config('default_language'));
define('X_DATE_FORMAT', x_app_config('date_format'));
define('X_SESS_NAME', x_app_config('sess_name'));
define('X_SESS_EXPIRE', x_app_config('sess_expired'));
define('X_SITE_URL', x_app_config('site_url'));
define('X_SITE_DOMAIN', x_app_config('site_domain'));
define('X_ANIMATE', x_app_config('animate'));

define('X_SITE_NAME', x_app_config('site_name'));
define('X_SITE_TAG_LINE', x_app_config('site_tag_line'));
define('X_SITE_DESCRIPTION', x_app_config('site_description'));
define('X_SITE_KEYWORD', x_app_config('site_keyword'));
define('X_TIME_ZONE', x_app_config('time_zone'));
define('X_GOOGLE_ANALYTIC', x_app_config('google_analytics'));  

date_default_timezone_set(X_TIME_ZONE);

$ip = $_SERVER['REMOTE_ADDR'];
$proxy = x_secure_config('ip_proxy');
$ban = explode('#', $proxy);
if (!in_array($ip, $ban)) {
    $router = new X_Router();
    $router->_add($route);
    $router->_instance();
} else {
    show_error('', 'ipbanned');
} 