<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/user
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource 
 */
class User extends X_Controller {

    public $limit = 5;
    public $user_id = null;

    public function __construct() {
        parent::__construct();
        $this->__composer('user');
        $this->user_id = $this->session->get_data('session_user_id');
    }

    public function index($offset = null) {
        if (is_ajax_request()) {
            if (is_auth('con_pengguna')) {
                $limit = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : $this->limit;
                $view['opt_limit'] = array();
                for ($i = 5; $i < 30; $i++) {
                    if ($i % 5 == 0) {
                        $view['opt_limit'][$i] = $i;
                    }
                }
                $view['offset'] = ($offset == null) ? 0 : $offset;
                $view['rs'] = $this->model_user->get_all($view['offset'], $limit);
                $view['total'] = $this->model_user->num_rows();
                $config['base_url'] = site_url('user/index');
                $config['total_rows'] = $view['total'];
                $config['per_page'] = $limit;
                $config['div'] = 'div.xamzo-right';
                $config['uri_segment'] = $view['offset'];
                $this->pagination->initialize($config);
                $view['paging'] = $this->pagination->crawler_links();
                $this->__view('user', 'index', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function create() {
        if (is_ajax_request()) {
            if (is_auth('con_pengguna', 'w')) {
                $this->validator->addRule('fullname', array('require'));

                $view['levels'][''] = '-select-';
                $levels = array('admin' => 'Administrator', 'user' => 'Regular User');
                foreach ($levels as $k => $v) {
                    $view['levels'][$k] = $v;
                }

                $pro_date = $this->request->post('birth_day', false);
                $ex_pro_date = explode('-', $pro_date);
                $birth_day = '0000-00-00';
                if (count($ex_pro_date) > 1) {
                    $birth_day = $ex_pro_date['2'] . '-' . $ex_pro_date['1'] . '-' . $ex_pro_date['0'];
                }

                $view['action'] = 'user/create';
                $data = array(
                    'id' => $this->request->post('id', false),
                    'registred' => date('Y-m-d H:i:s'),
                    'level' => $this->request->post('level'), 
                    'password' => md5($this->request->post('password', false)),
                    'fullname' => $this->request->post('fullname'),
                    'email' => $this->request->post('email', false),
                    'born' => $this->request->post('born'),
                    'birth_day' => $birth_day,
                    'sex' => $this->request->post('sex')
                );
                $this->validator->setData($data);
                if ($this->request->post('emitter') != '') {
                    if ($this->validator->isValid()) {
                        $this->model_user->insert($data); 
                    } else {
                        foreach ($this->validator->getErrors() as $field => $messages) {
                            $view['errors'][$field] = $messages['0'];
                        }
                        $this->__view('user', 'form', $view);
                    }
                } else {
                    $this->__view('user', 'form', $view);
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function update() {
        if (is_ajax_request()) {
            if (is_auth('con_pengguna', 'w')) {
                $this->validator->addRule('fullname', array('require'));
                $view['levels'][''] = '-select-';
                $levels = array('admin' => 'Administrator', 'user' => 'Regular User');
                foreach ($levels as $k => $v) {
                    $view['levels'][$k] = $v;
                }

                $level = $this->request->post('level');
                $password = $this->request->post('password', false);
                $fullname = $this->request->post('fullname');
                $born = $this->request->post('born');
                $email = $this->request->post('email',false);
                $sex = $this->request->post('sex');

                $pro_date = $this->request->post('birth_day', false);
                $ex_pro_date = explode('-', $pro_date);
                $birth_day = '00-00-0000';
                if (count($ex_pro_date) > 1) {
                    $birth_day = $ex_pro_date['2'] . '-' . $ex_pro_date['1'] . '-' . $ex_pro_date['0'];
                }
                $view['action'] = 'user/update';
                $id = $this->request->post('id', false);
                if ($this->request->post('emitter') != '') {
                    $view['id'] = $id;
                } else {
                    $view['id'] = $id['0'];
                }
                $row = $this->model_user->get_where_id($view['id']);
                if ($password != '') {
                    $view['default']['password'] = md5($password);
                }
                $view['default']['level'] = ($level) ? $level : $row->level;
                $view['default']['fullname'] = ($fullname) ? $fullname : $row->fullname;
                $view['default']['born'] = ($born) ? $born : $row->born;
                $xpro_date = explode('-', $row->birth_day);
                $xbirth_day = $xpro_date['2'] . '-' . $xpro_date['1'] . '-' . $xpro_date['0'];
                $view['default']['birth_day'] = ($birth_day != '00-00-0000') ? $birth_day : $xbirth_day;
                $view['default']['sex'] = ($sex) ? $sex : $row->sex;
                $view['default']['email'] = ($email) ? $email : $row->email;

                $this->validator->setData($view['default']);
                if ($this->request->post('emitter') != '') {
                    if ($this->validator->isValid()) {
                        $this->model_user->update($view['default'], array('id' => $id));
                    } else {
                        foreach ($this->validator->getErrors() as $field => $messages) {
                            $view['errors'][$field] = $messages['0'];
                        }
                        $this->__view('user', 'form', $view);
                    }
                } else {
                    $this->__view('user', 'form', $view);
                }
            } else {
                print_r('Direct access is not allowed');
            }
        }
    }

    public function delete() {
        if (is_ajax_request()) {
            if (is_auth('con_pengguna', 'x')) {
                extract($_POST);
                $in = '';
                for ($i = 0; $i < count($id); $i++) {
                    if ($i == 0) {
                        $in.=$id[$i];
                    } else {
                        $in.=",$id[$i]";
                    }
                    $this->lib_user->delete_auth($id[$i]);
                }
                if ($in != '') {
                    $this->model_user->delete_where_in($in);
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function login() {
        if (!is_login()) {
            // set rule
            $this->validator->addRule('email', array('require', 'email'));
            $view['action'] = 'login';
            $email = $this->request->post('email');
            $password = $this->request->post('password');
            $view['default'] = array(
                'email' => isset($email) ? $email : '',
                'password' => isset($password) ? md5($password) : ''
            );

            $this->validator->setData($view['default']);
            if ($this->request->post('emitter') != '') {
                if ($this->validator->isValid()) {
                    if ($this->model_user->login($view['default'])) {
                        header('Content-type: application/json');
                        $email = $view['default']['email'];
                        $array = array(
                            'controller' => 'userData',
                            'method' => 'login'
                        );
                        if (is_ajax_request()) {
                            echo json_encode($array);
                        } else {
                            redirect('');
                        }
                    } else {
                        $view['login_error'] = 'password atau email salah';
                        $this->__view('user', 'login_', $view);
                    }
                } else {
                    // display error
                    foreach ($this->validator->getErrors() as $field => $messages) {
                        if (array_key_exists($messages['0'], $this->lang)) {
                            $view['errors'][$field] = $this->lang[$messages['0']];
                        } else {
                            $view['errors'][$field] = str_replace('_', ' ', $messages['0']);
                        }
                    }
                    if (is_ajax_request()) {
                        $this->__view('user', 'login_', $view);
                    } else {
                        $this->__view('user', 'login', $view);
                    }
                }
            } else {
                $this->__view('user', 'login', $view);
            }
        } else {
            redirect('h');
        }
    }

    public function logout() {
        header('Content-type: application/json');
        $this->session->destroyed();
        $array = array(
            'controller' => 'userData',
            'method' => 'logout'
        );
        if (is_ajax_request()) {
            echo json_encode($array);
        } else {
            redirect('');
        }
    }

    public function search($offset = null) {
        if (is_ajax_request()) {
            if (is_auth('con_pengguna')) {
                $limit = ($this->session->get_data('limit')) ? $this->session->get_data('limit') : $this->limit;
                if ($offset == null) {
                    $view['offset'] = 0;
                } else {
                    $view['offset'] = $offset;
                }
                $this->session->read_data();
                $view['rs'] = $this->model_user->get_all($view['offset'], $limit);
                $total = $this->model_user->num_rows();
                // get number of user
                $view['total'] = $total;
                $view['opt_limit'] = array();
                for ($i = 5; $i < 30; $i++) {
                    if ($i % 5 == 0) {
                        $view['opt_limit'][$i] = $i;
                    }
                }

                $config['base_url'] = site_url('user/index');
                $config['total_rows'] = $total;
                $config['per_page'] = $limit;
                $config['div'] = 'div.xamzo-right';
                $config['uri_segment'] = $view['offset'];
                $this->pagination->initialize($config);
                $view['paging'] = $this->pagination->crawler_links();

                $view['order'] = $this->request->post('order');
                $view['by'] = $this->request->post('by');
                $view['search'] = $this->request->post('search');
                $this->__view('user', 'search', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function get_auth($ids = null) {
        if ($ids != null && is_ajax_request()) {
            if (is_auth('con_pengguna', 'r')) {
                $id = strtoupper($ids);
                $json = x_file_get_content(MODPATH . 'user/configs/auth' . JSON);
                $d = x_read_json_decode($json);
                $me = array();
                foreach ($d as $key => $value) {
                    foreach ($value as $k => $r) {
                        if ($k == $id) {
                            $me[] = $value[$k];
                        }
                    }
                }
                $view['auth'] = isset($me['0']) ? $me['0'] : '';
                $view['action'] = 'user/update_auth/';
                // get data user
                $row = $this->model_user->get_where_id($id);
                $view['user'] = $row->fullname;
                $view['id'] = $row->id;
                $view['photo'] = $row->photo;


                $view['dirr'] = $this->dirr_app();

                $this->__view('user', 'auth', $view);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function update_auth($ids = null) {
        if ($ids != null && is_ajax_request()) {
            if (is_auth('con_pengguna', 'x')) {
                $id = strtoupper($ids);
                $json = x_file_get_content(MODPATH . 'user/configs/auth' . JSON);
                $d = x_read_json_decode($json);
                $n = array();
                foreach ($d as $key => $value) {
                    foreach ($value as $k => $v) {
                        if (array_key_exists($id, $value)) {
                            unset($value[$id]);
                        }
                        if ($value != array()) {
                            $n[] = $value;
                        }
                    }
                }

                $arr = array();
                foreach ($_POST as $key) {
                    $arr[] = array($key['0'] => array(
                            "r" => isset($key["r"]) ? '1' : '0',
                            "w" => isset($key["w"]) ? '1' : '0',
                            "x" => isset($key["x"]) ? '1' : '0',
                            "p" => isset($key["p"]) ? '1' : '0'
                    ));
                };
                $j = array(array($id => $arr));
                $join = (array_merge($n, $j));
                x_file_put_content(MODPATH . "user/configs/auth" . JSON, json_encode($join)); 
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function account() {
        if (is_login()) {
            $id = $this->session->get_data("session_user_id");
            $view['row'] = $this->model_user->get_where_id($id);
            switch (subdomain()) {
                case 'm':
                    $this->__view('user', 'm/account', $view);
                    break;
                default:
                    $this->edit_profile();
                    break;
            }
        }
    }

    public function edit_profile() {
        if (is_ajax_request()) {
            if (is_login()) {
                $this->validator->addRule('fullname', array('require'));
                $id = $this->session->get_data("session_user_id");
                $personal_mail = $this->request->post('personal_mail');
                $view['action'] = 'user/edit_profile';

                $pro_date = $this->request->post('birth_day', false);
                $ex_pro_date = explode('-', $pro_date);
                $birth_day = '00-00-0000';
                if (count($ex_pro_date) > 1) {
                    $birth_day = $ex_pro_date['2'] . '-' . $ex_pro_date['1'] . '-' . $ex_pro_date['0'];
                }

                $view['id'] = $id;
                $fullname = $this->request->post('fullname');
                $born = $this->request->post('born');
                $sex = $this->request->post('sex');
                $hp = $this->request->post('hp', false);
                $email = $this->request->post('email', false);
                //
                $row = $this->model_user->get_where_id($view['id']);
                $view['default']['fullname'] = ($fullname) ? $fullname : $row->fullname;
                $view['default']['born'] = ($born) ? $born : $row->born;
                $xpro_date = explode('-', $row->birth_day);
                $xbirth_day = $xpro_date['2'] . '-' . $xpro_date['1'] . '-' . $xpro_date['0'];
                $view['default']['birth_day'] = ($birth_day != '00-00-0000') ? $birth_day : $xbirth_day;
                $view['default']['sex'] = ($sex) ? $sex : $row->sex;
                $view['default']['email'] = ($email) ? $email : $row->email;
            }
            $this->validator->setData($view['default']);
            if ($this->request->post('emitter') != '') {
                if ($this->validator->isValid()) {
                    $this->model_user->update($view['default'], array('id' => $id));
                    echo 'Profile Berhasil Dirubah';
                } else {
                    foreach ($this->validator->getErrors() as $field => $messages) {
                        $view['errors'][$field] = $messages['0'];
                    }
                    $this->__view('user', 'form_profile', $view);
                }
            } else {
                $this->__view('user', 'form_profile', $view);
            }
        } else {
            print_r("You don't have access in this module");
        }
    }

    public function edit_password() {
        if (is_ajax_request()) {
            $id = $this->session->get_data('session_user_id');
            if (is_login()) {
                $this->__library(array('validator'));
                $password = $this->request->post('password', false);
                $old_password = $this->request->post('old_password', false);
                $view['action'] = 'user/edit_password';

                // get user data
                $r = $this->model_user->get_where_id($id);
                $view['default']['password'] = md5($password) ? md5($password) : '';
                // set validation
                $this->validator->setData($view['default']);
                if ($this->request->post('emitter') != '') {
                    // if valid
                    if ($this->validator->isValid()) {

                        if (md5($old_password) == $r->password) {
                            $this->model_user->update($view['default'], array('id' => $id)); 
                            echo 'Password Berhasil Dirubah';
                        } else {
                            echo 'Password yang anda masukan salah';
                        }
                    } else {
                        foreach ($this->validator->getErrors() as $field => $messages) {
                            $view['errors'][$field] = $messages['0'];
                        }
                        $this->__view('user', 'form_password', $view);
                    }
                } else {
                    $this->__view('user', 'form_password', $view);
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    function pp_upload() {
        if (is_login()) {
            $user_id = $this->session->get_data('session_user_id');
            $file = $_FILES['userpp'];
            $tmpFilePath = $file['tmp_name'];
            $uploaddir = UPLOADPATH . "pp/";
            if ($tmpFilePath != "") {
                $uploadfile = $uploaddir . basename($file['name']);
                $theFileSize = $file['size'];
                if ($theFileSize > $this->lib_media->_config('max_file_size')) {
                    echo '{"status":"error"}';
                    exit();
                }
                if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
                    $userfile_name = $file['name'];
                    $t = substr($userfile_name, strrpos($userfile_name, '.') + 1);
                    $f = explode('.', $userfile_name);
                    $arrep = array('#', '$', '@', ' ', $t);
                    $oldname = str_replace($arrep, '-', $f['0']);
                    $new_name = strtolower("xamzo" . '{x}' . $oldname . '{x}' . date('Y-m-d') . '{x}' . date('h-i-s') . '{x}' . $user_id);
                    rename($uploadfile, $uploaddir . $new_name . ".$t");
                    $filename = $new_name . ".$t";
                    chmod($uploaddir . $filename, 0777); 
                    echo '{"status":"success","file":"' . $filename . '"}';
                } else {
                    echo '{"status":"error"}';
                }
            }
        } else {
            print_r("You don't have access in this module");
        }
    }

    function pp_thumb($file = null) {
        if (is_ajax_request()) {
            $id = $this->session->get_data('session_user_id');
            if (is_login()) {
                $uploaddir = UPLOADPATH . "pp/";
                if (is_file($uploaddir . $file)) {
                    $conf_small = $this->lib_media->set_thumb_pp($uploaddir, $file);
                    $this->image_lib->initialize($conf_small);
                    if ($this->image_lib->resize()) {
                        
                    }
                    $xfile = explode('.', $file);
                    $nfile = $xfile['0'] . '_pp.' . $xfile['1'];
                    if (is_file($uploaddir . $file)) {
                        unlink($uploaddir . $file);
                    }
                    $row = $this->model_user->get_photo($id);
                    if ($row->photo != UPLOADPATH . 'pp/default_x_user.jpg') {
                        if (is_file($row->photo)) {
                            unlink($row->photo);
                        }
                    }
                    $this->model_user->update(array('photo' => $uploaddir . $nfile), array('id' => $id));
                    echo site_url($uploaddir . $nfile);
                } else {
                    echo 'no-file<br/>' . $uploaddir . $file;
                }
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function dirr_app() {
        return array(
            "statistik" => array(
                "proyeksi", 
            ),
            "aplikasi" => array(
                "con_database",
                "con_keamanan", 
                "con_pengaturan_umum",
                "con_pengguna",
            )
        );
    }

    public function forgot() {
        if (is_ajax_request()) {
            $email = $this->request->post('email');
            $this->validator->addRule('email', array('require', 'email'));
            $view['action'] = 'user/forgot';
            $emitter = isset($emitter) ? $emitter : '';
            $new_password = strtoupper(random_string());
            $encrypt = md5($new_password);
            $view['default']['email'] = $email;
            $this->validator->setData($view['default']);
            if ($this->request->post('emitter') != '') {
                // if valid
                if ($this->validator->isValid()) {
                    $usermail = $this->model_user->get_where_email($email);
                    $num = $usermail->num_rows();
                    $row = $usermail->row();
                    if ($num > 0) {
                        $s_name = $this->lib_email->config('name_sender');
                        $s_email = $this->lib_email->config('email_sender');
                        $mail_content = "Untuk mendapatkan pasword baru di website " . site_domain() . ", silahkan klik alamat berikut <b><a href='" . site_domain('user/reset_password/' . $row->id . '/' . $row->email . '/' . $new_password . '/' . $encrypt) . "'>reset password</a></b>";
                        $data_mail = array(
                            'from' => $s_name,
                            'email' => $row->email,
                            'username' => $row->fullname,
                            'date' => x_date_format(date('Y-m-d')),
                            'mail_content' => $mail_content
                        );
                        send_mail('password', 'Permintaan Perubahan Password', 'Permintaan Perubahan Password [' . X_SITE_NAME . ']', $s_email, $s_name, $row->fullname, $email, $data_mail);
                        echo "Kami telah mengirimkan <b>email</b> notifikasi perubahan password. Silahkan cek inbox <b>email</b> anda, untuk mendaftarkan ulang <b>password</b>. <br/>Tekan <b>[ESC]</b> untuk keluar";
                    } else {
                        $view['message'] = 'Email yang anda masukan tidak terdaftar dalam sistem kami!';
                        $this->__view('user', 'forgot', $view);
                    }
                } else {
                    foreach ($this->validator->getErrors() as $field => $messages) {
                        $view['errors'][$field] = $messages['0'];
                    }
                    $view['message'] = 'Email yang anda masukan tidak terdaftar dalam sistem kami!';
                    $this->__view('user', 'forgot', $view);
                }
            } else {
                $this->__view('user', 'forgot', $view);
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function reset_password($user_id = null, $email = null, $newpass = null, $md5 = null) {
        if ($user_id != null && $email != null && $newpass != null && $md5 != null) {
            $usermail = $this->model_user->get_where_email($email);
            $num = $usermail->num_rows();
            $row = $usermail->row();
            if ($num > 0) {
                $this->model_user->update(array('password' => $md5), array('id' => $user_id));
                $s_name = $this->lib_email->config('name_sender');
                $s_email = $this->lib_email->config('email_sender');
                $mail_content = "Pasword baru mu di " . site_domain() . " adalah <b>" . strtoupper($newpass) . "</b>";
                $data_mail = array(
                    'from' => $s_name,
                    'username' => $row->fullname,
                    'email' => $email,
                    'date' => x_date_format(date('Y-m-d')),
                    'mail_content' => $mail_content
                );
                send_mail('password', 'Konfirmasi Perubahan Password', 'Konfirmasi Perubahan Password [' . X_SITE_NAME . ']', $s_email, $s_name, $row->fullname, $email, $data_mail);
                redirect('');
            } else {
                $view['message'] = 'Email yang anda masukan tidak terdaftar dalam sistem kami!';
                $this->__view('user', 'forgot', $view);
            }
        }
    }

}
