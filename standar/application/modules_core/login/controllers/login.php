<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

	function __construct() {
        parent::__construct();

        $this->load->model('encryption');
        $this->load->library('encrypt_2015');

    }

	public function index()
	{
		$this->load->view('login_view');
	}
	
    public function process(){
        $this->output->enable_profiler(TRUE);
        $username = $this->input->post('username');
        $password = Encryption::encrypt_password_callback($this->input->post('password'), SECURITY_KEY);

        //cek akun pengguna portal
        $user_account = $this->_checkAccount($username, $password);

        // jika username dan password true response 1
        if($user_account->response == 1){

            //check session terlebih dahulu
            $sess_exis = $this->_checkSessionId($this->session->userdata('session_id'));

            //jika session belum berakhir
            if($sess_exis->response == 1){

                //maka redirect ke main dan session tetap tersimpan
                redirect(base_url().'dashboard');

            }else{

                //jika session berakhir atau belum ada maka simpan session dalam log dan buat session baru
                $log_session['session_id'] = $this->session->userdata('session_id');
                $log_session['ip_address'] = $_SERVER['REMOTE_ADDR'];
                $log_session['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                $log_session['last_activity'] = date("Y-m-d H:i:s");
                $log_session['id_user'] = $user_account->data->id_user;
                $this->db->insert('ci_sessions', $log_session);

                //session user
                $all_data_user = $this->db->get_where('m_user', array('id_user' => $user_account->data->id_user))->row(); //print_r($all_data_user);die;
                $session_user = new stdClass();
                $session_user->data_user = $all_data_user;
                //$session_user->menu_role = $this->get_hak_akses_menu_role($all_data_user->id_role);
                $session_user->user_id = $user_account->data->id_user;
                $session_user->login = TRUE;

                $this->session->set_userdata($session_user);
                
                redirect(base_url().'dashboard');
            }

        }else{

            redirect(LOGIN_URL.'?m=wrong');

        }


    }

    public function get_hak_akses_menu_role($id_role){

        // get menu role
        $menu_role = $this->qry_main_menu($id_role);

        foreach ($menu_role->result_array() as $key => $value) {

            if( ($value['parent_menu'] == 0) && ($value['link'] == '#') ){

                $this->db->select('m_menu.*,t_menu_role.id_role');
                $this->db->from('t_menu_role');
                $this->db->join('m_menu', 'm_menu.id_menu=t_menu_role.id_menu', 'left');
                $this->db->group_by('t_menu_role.id_menu');
                $this->db->where(array('id_role'=>$id_role, 'parent_menu'=>$value['id_menu'], 'm_menu.active'=>'Y'));
                $submenu = $this->db->get()->result_array();

                foreach ($submenu as $row_submenu) {

                    $code_action = $this->db->select('code')->get_where('t_menu_role', array('id_role'=>$id_role, 'id_menu'=>$row_submenu['id_menu']))->result_array();

                    /*foreach ($code_action as $row_code) {
                        $arr_code[] = $row_code['code'];
                    }*/    

                    $row_submenu['action'] = $code_action;
                    $arr_submenu[] = $row_submenu;

                }
                
                $value['submenu'] = $arr_submenu;

            }else{
                $code_action = $this->db->select('code')->get_where('t_menu_role', array('id_role'=>$id_role, 'id_menu'=>$value['id_menu']))->result_array();
                foreach ($code_action as $row_code) {
                    $arr_codes[] = $row_code['code'];
                }
                $value['submenu'] = array();
                $value['action'] = $arr_codes;
            }

            
            $getData[] = $value;
        }

        return $getData;
    }

    public function qry_submenu($id_role, $id_menu){

        $this->db->select('m_menu.*,t_menu_role.id_role');
        $this->db->from('t_menu_role');
        $this->db->join('m_menu', 'm_menu.id_menu=t_menu_role.id_menu', 'left');
        $this->db->group_by('t_menu_role.id_menu');
        $this->db->where(array('id_role'=>$id_role, 'parent_menu'=>$id_menu, 'm_menu.active'=>'Y'));
        $submenu = $this->db->get()->result_array();

        return $submenu;
    }

    public function qry_main_menu($id_role){

        $this->db->select('m_menu.*,t_menu_role.id_role');
        $this->db->from('t_menu_role');
        $this->db->join('m_menu', 'm_menu.id_menu=t_menu_role.id_menu', 'left');
        $this->db->group_by('t_menu_role.id_menu');
        $this->db->where(array('id_role'=>$id_role, 'parent_menu'=>0, 'm_menu.active'=>'Y'));
        $menu_role = $this->db->get();

        return $menu_role;
    }

	public function _checkSessionId($sessId) {

         $check = $this->db->get_where('ci_sessions', array('session_id' => $sessId));
		 
		 $result = new stdClass();
         $result->response = ($check->num_rows() > 0) ? TRUE : FALSE;
         $result->data = $check->row();

         return $result;

     }


     public function _checkAccount($username, $password) {
        
         $check = $this->db->get_where('m_user', array('email' => $username, 'password' => $password, 'active'=>'Y'));
		 
		 $result = new stdClass();
         $result->response = ($check->num_rows() > 0) ? TRUE : FALSE;
         $result->data = $check->row();

         return $result;

     }

	public function logout()
	{    
		
        $this->session->sess_destroy();
         
        redirect(base_url().'login?m=out');
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */