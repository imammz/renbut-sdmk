<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package		library/controller
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

class X_Controller extends X_Loader {
    //put your code here
    function __construct() {
        parent::__construct();  
    }
    
} 