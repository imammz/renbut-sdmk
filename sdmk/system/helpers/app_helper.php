<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package		helpers/app
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

/**
 * read for each application configs
 * @param type $config_name
 * @return null
 */
function xml2array($xmlObject, $out = array()) {
    foreach ((array) $xmlObject as $index => $node)
        $out[$index] = ( is_object($node) ) ? xml2array($node) : $node;

    return $out;
}

function add_array($original) {
    $newArray = array();
// loop through each entry from the original array in turn
    foreach ($original as $entry) {
        // temporarily use the name as the key for our new array (we'll reset to a numeric later)
        $code = $entry['service_code'];
        $newArray[$code]['service_code'] = $code;
        $service = $entry['service'];
        $newArray[$code]['service'] = $service;
        //  test if we have multiple entries for the same name/status
        if (isset($newArray[$code][$entry['service_code']])) {
            $newArray[$code][$entry['service_code']] += $entry['value'];
        } else {
            $newArray[$code][$entry['service_code']] = $entry['value'];
        }
    }

//  Reset top-level keys to numerics
    return array_values($newArray);
}

if (!function_exists('x_app_config')) {

    function x_app_config($config_name = null, $file = 'configs/general') {
        if ($config_name != null) {
            $json = x_file_get_content(BASEPATH . $file . JSON);
            $con = x_read_json_decode($json);
            $data = array();
            for ($i = 0; $i < count($con); $i++) {
                foreach ($con[$i] as $key => $val) {
                    $data[$key]['val'] = $val['val'];
                    $data[$key]['desc'] = $val['desc'];
                }
            }
            if (isset($data[$config_name]['val'])) {
                return $data[$config_name]['val'];
            } else {
                return null;
            }
        }
    }

}

/**
 * 
 * @param type $file
 * @return type
 */
function x_get_version($file) {
    $json = x_file_get_content($file . JSON);
    $con = x_read_json_decode($json);
    return $con['version'];
}

/**
 * change array to object
 * @param type $array
 * @return \stdClass|boolean
 */
if (!function_exists('x_array_to_object')) {

    function x_array_to_object($array) {
        $obj = new stdClass;
        if (is_array($array)) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    $obj->{$k} = x_array_to_object($v); //RECURSION
                } else {
                    $obj->{$k} = $v;
                }
            }
            return $obj;
        }
        return true;
    }

}

/**
 * read directory file and folder
 * @param type $start_dir
 * @return boolean
 */
if (!function_exists('x_list_dir')) {

    function x_list_dir($start_dir = '.') {
        $files = array();
        if (is_dir($start_dir)) {
            $fh = opendir($start_dir);
            while (($file = readdir($fh)) !== false) {
                if (strcmp($file, '.') == 0 || strcmp($file, '..') == 0)
                    continue;
                $filepath = $start_dir . '/' . $file;
                if (is_dir($filepath))
                    $files = array_merge($files, list_dir($filepath));
                else
                    array_push($files, $filepath);
            }
            closedir($fh);
        } else {
            $files = false;
        }

        return $files;
    }

}

/**
 * get mime type of file
 * @param type $type
 * @return type
 */
if (!function_exists('x_mimes')) {

    function x_mimes($type = null) {
        include_once BASEPATH . 'configs/mimes' . EXT;
        foreach ($mimes as $key => $value) {

            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    if ($v == $type) {
                        return $key;
                    }
                }
            } else {
                if ($value == $type) {
                    return $key;
                }
            }
        }
    }

}

/**
 * 
 * @param type $con
 */
if (!function_exists('x_display_config')) {

    function x_display_config($con = null, $langs = array(), $folder = null) {
        if ($con != null & is_array($langs)) {
            if (is_array($con)) {
                $X = &get_instance();
                $X->__language($langs, $folder);
                $view['con'] = $con;
                $X->__view('setting', 'helpers/app/display', $view);
            }
        }
    }

}

if (!function_exists('x_update_setting')) {

    function x_update_setting($json_file) {
        if ($json_file != null) {
            $arr = array();
            $bol = array();
            $bolk = array();
            $bolt = array();
            $ol = array();
            foreach ($_POST as $key) {
                if (isset($key['key'])) {
                    $ink = $key['key'];
                    if ($key['type'] != 'checkbox') {
                        $arr[] = isset($key['option']) ? array($key['key'] => array('val' => stripslashes($key['val']), 'desc' => $key['desc'], 'type' => $key['type'], 'validation' => isset($key['validation']) ? $key['validation'] : '', 'option' => $key['option'], 'arrow' => $key['arrow'])) : array($key['key'] => array('val' => stripslashes($key['val']), 'desc' => $key['desc'], 'type' => $key['type'], 'type' => $key['type'], 'validation' => isset($key['validation']) ? $key['validation'] : ''));
                        if ($key['key'] == 'DirectoryIndex') {
                            x_security_dir_index($key['val']);
                        }
                    } else {
                        $ol[$ink][] = array('desc' => $key['desc'], 'type' => $key['type']);
                        foreach ($key as $k => $v) {
                            if ($v == 'true' || $v == 'false') {
                                $bolk[$ink][] = array($k => $v);
                            }
                        }

                        $tck = explode('#', $key['tck']);
                        if (count($tck) > 1) {
                            for ($i = 0; $i < count($tck) - 1; $i++) {
                                if ($tck[$i] != '') {
                                    $bolt[$ink][] = array($tck[$i] => 'false');
                                    $bol[$ink] = array_merge_recursive($bolk[$ink], $bolt[$ink]);
                                } else {
                                    $bol[$ink] = $bolk[$ink];
                                }
                            }
                        }
                        $data_r[$ink] = array_merge($ol[$ink], $bol[$ink]);
                        $x_data = str_replace('},{', ',', json_encode($data_r[$ink]));
                        $a_data = json_decode(str_replace(array('[', ']'), '', $x_data));
                        $arr[] = array($key['key'] => $a_data);
                    }
                }
            }
            if (is_file($json_file)) {
                x_file_put_content($json_file, json_encode($arr));
            }
        }
    }

}

if (!function_exists('execute_time')) {

    function execute_time() {
        $exec = microtime();
        $exectime = explode(" ", $exec);
        return $exectime[1] + $exectime[0];
    }

}
if (!function_exists('x_open_html')) {

    function x_open_html() {

        return '<html id="ca" lang="en-US" prefix="og: http://ogp.me/ns #fb: http://ogp.me/ns/fb#">';
    }

}
if (!function_exists('x_close_html')) {

    function x_close_html() {
        return '</html>';
    }

}
if (!function_exists('x_header')) {

    function x_header() {
        x_page_ext('seo');
    }

}

if (!function_exists('x_check_connection')) {

    function x_check_connection() {
        $fp = fsockopen("www.google.com", 80, $errno, $errstr, 30);
//if the socket failed it's offline...
        if ($fp) {
            return true;
        }
        return false;
    }

}
 
if (!function_exists('format_rupiah')) {

    function format_rupiah($number) {
        return 'Rp.' . number_format($number, 2);
    }

}

if (!function_exists('get_app_setup')) {

    function get_app_setup($user_id, $key) {
        $X = & get_instance();
        $X->__library(array('database'));
        $rs = $X->db->select('id,`key`,`value`')
                ->from($X->db->prefix . 'setting')
                ->where(array('user_id' => $user_id, '`key`' => $key))
                ->get();
        if ($rs->num_rows() > 0) {
            return $rs->row();
        } else {
            return null;
        }
    }

}

if (!function_exists('update_app_setup')) {

    function update_app_setup($user_id, $key, $value) {
        $X = & get_instance();
        $X->__library(array('database'));
        $app = get_app_setup($user_id, $key);
        if ($app != null) {
            $X->db->update(array(
                'value' => $value
                    ), array(
                'user_id' => $user_id,
                'key' => $key
                    ), $X->db->prefix . 'setting');
        } else {
            $X->db->insert(
                    array('user_id' => $user_id, 'key' => $key, 'value' => $value)
                    , $X->db->prefix . 'setting');
        }
    }

} 