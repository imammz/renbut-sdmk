<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Masterdata extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
    }

    public function index($function, $subtitle) {
        $data = array();
        $data['class'] = 'masterdata';
        $data['subclass'] = $function;
        $data['title'] = "Registrasi Data";
        $data['subtitle'] = $subtitle;
        $data['function'] = "$function";
        $this->load->view('masterdata_view', $data);
    }

    public function _example_output($output = null) {
        $this->load->view('grocery_view', $output);
    }

    public function _example_output2($output = null) {
        $this->load->view('sdmk_view', $output);
    }

    public function _example_output3($output = null) {
        $this->load->view('faskes_view', $output);
    }

    public function all() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_jenis_sdmk');
        $crud->set_subject('Jenis SDMK');
        $crud->display_as('ref_jenis_sdmk_kode', 'Kode SDMK');
        $crud->display_as('ref_jenis_sdmk_nama_level2', 'Nama SDMK');
        $crud->display_as('ref_kelompok_sdmk_id', 'Nama Kelompok');

        $crud->set_theme('twitter-bootstrap');
        

        $crud->set_relation('ref_kelompok_sdmk_id', 'ref_kelompok_sdmk', 'ref_kelompok_sdmk_nama');

        $crud->unset_fields('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');
        $crud->unset_columns('ref_jenis_sdmk_kode', 'ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');

        $output = $crud->render();
        $output->title = 'Data Jenis SDMK';
        $this->_example_output2($output);
    }

    public function jenis($rumpun) {
        $rumpun = str_replace('%20', ' ', $rumpun);

        $crud = new grocery_CRUD();

        $crud->set_table('ref_jenis_sdmk');
        $crud->set_subject('Jenis SDMK');
        $crud->display_as('ref_jenis_sdmk_kode', 'Kode SDMK');
        $crud->display_as('ref_jenis_sdmk_nama_level2', 'Nama SDMK');
        $crud->display_as('ref_kelompok_sdmk_id', 'Nama Kelompok');

        $crud->where('ref_jenis_sdmk_nama_level1', $rumpun);

        $crud->set_theme('twitter-bootstrap');

        $crud->set_relation('ref_kelompok_sdmk_id', 'ref_kelompok_sdmk', 'ref_kelompok_sdmk_nama');

        $crud->unset_fields('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');
        $crud->unset_columns('ref_jenis_sdmk_kode', 'ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');

        $output = $crud->render();
        $output->title = 'Data Jenis SDMK';
        $this->_example_output2($output);
    }

    public function kota() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_kabupaten');
        $crud->set_subject('Kota / Kabupaten');
        $crud->set_relation('NO_PROV', 'ref_provinsi', 'NAMA_PROV');

        $crud->display_as('NO_PROV', 'Provinsi');
        $crud->display_as('NO_KAB', 'Kode Kabupaten/Kota');

        $crud->unset_fields('NO_KLAS_KAB');
        $crud->unset_columns('NO_KLAS_KAB');
        $crud->set_theme('bootstrap');

        $output = $crud->render();
        $output->title = 'Data Kota Kabupaten';
        $this->_example_output($output);
    }

    public function provinsi() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_provinsi');
        $crud->set_subject('Provinsi');

        $crud->display_as('NO_PROV', 'Kode Provinsi');



        $output = $crud->render();
        $output->title = 'Data Provinsi';
        $this->_example_output($output);
    }

    public function faskes() {
        $crud = new grocery_CRUD();

        $crud->set_table('faskes');
        $crud->set_subject('Data FASKES');
        $crud->set_relation('NO_KAB', 'ref_kabupaten', 'NAMA_KAB');
        $crud->set_relation('ref_faskes_kelas_id', 'ref_faskes_kelas', 'ref_faskes_kelas_nama');
        $crud->set_relation('ref_jenis_faskes_kode', 'ref_jenis_faskes', 'ref_jenis_faskes_nama');
        $crud->set_relation('status_pengelolaan_id', 'status_pengelolaan', 'status_pengelolaan_nama');

        $crud->display_as('NO_KAB', 'Kabupaten');
        $crud->display_as('tahun', 'Tahun Entry');

        $crud->unset_fields('update_date', 'insert_date');
        $crud->unset_columns('update_date', 'insert_date');

        $output = $crud->render();
        $output->title = 'Data Faskes';
        $this->_example_output($output);
    }

    public function jenis_tugas() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_jenis_tugas');
        $crud->set_subject('Jenis Tugas');
        //  $crud->set_relation('NO_PROV','ref_provinsi','NAMA_PROV');
        // $crud->display_as('NO_PROV','Provinsi'); 
        // $crud->unset_fields('NO_KLAS_KAB');
        // $crud->unset_columns('NO_KLAS_KAB');

        $output = $crud->render();
        $output->title = 'Data Jenis Tugas';
        $this->_example_output($output);
    }

    public function satuan() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_satuan');
        $crud->set_subject('Data Satuan Waktu');
        //  $crud->set_relation('NO_PROV','ref_provinsi','NAMA_PROV');
        // $crud->display_as('NO_PROV','Provinsi'); 
        // $crud->unset_fields('NO_KLAS_KAB');
        // $crud->unset_columns('NO_KLAS_KAB');

        $output = $crud->render();
        $output->title = 'Data Satuan Waktu';
        $this->_example_output($output);
    }

    public function kbk() {
        $crud = new grocery_CRUD();

        $crud->set_table('kbk');
        $crud->set_subject('Data Komponen Beban Kerja (KBK)');
        $crud->set_relation('jenis_tugas_id', 'ref_jenis_tugas', 'jenis_tugas_nama');
        $crud->set_relation('ref_jenis_sdmk_kode', 'ref_jenis_sdmk', '{ref_jenis_sdmk_nama_level1} - {ref_jenis_sdmk_nama_level2}');
        $crud->set_relation('satuan_id', 'ref_satuan', 'satuan_nama');

        $crud->display_as('ref_jenis_sdmk_kode', 'Nama SDMK');

        $crud->unset_fields('SBK', 'STP');
        $crud->unset_columns('SBK', 'STP');

        $output = $crud->render();
        $output->title = 'Data Komponen Beban Kerja (KBK)';
        $this->_example_output($output);
    }

    public function jenisSDMK() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_jenis_sdmk');
        $crud->set_subject('Jenis SDMK');
        $crud->display_as('ref_jenis_sdmk_kode', 'Kode SDMK');
        $crud->display_as('ref_jenis_sdmk_nama_level2', 'Nama SDMK');
        $crud->display_as('ref_kelompok_sdmk_id', 'Nama Kelompok');
       

        $crud->set_theme('twitter-bootstrap');

        $crud->set_relation('ref_kelompok_sdmk_id', 'ref_kelompok_sdmk', 'ref_kelompok_sdmk_nama');

        $crud->unset_fields('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');
        $crud->unset_columns('ref_jenis_sdmk_nama_level1', 'ref_jenis_sdmk_nama_level3', 'Kode_Level2', 'Kode_Level3', 'ref_kelompok_sdmk_standar');

        $output = $crud->render();
        $output->title = 'Data Jenis SDMK';
        $this->_example_output($output);
    }

    public function TahunEntry() {
        $crud = new grocery_CRUD();

        $crud->set_table('tahun_entry');
        $crud->set_subject('Tahun Entry');

        $output = $crud->render();
        $output->title = 'Periode Tahun Entry';
        $this->_example_output($output);
    }

    public function Puskesmas() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_puskesmas');
        $crud->set_subject('Data Puskesmas');
        $crud->unset_edit();
        $crud->unset_delete();
        //$crud->unset_add();
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        
        $crud->set_relation('Tipe_Puskesmas', 'ref_tipe_puskesmas', 'Tipe');
        $crud->set_relation('Tipe_kawasan', 'ref_tipe_kawasan', 'Tipe');
        
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');
        $crud->display_as('Nama_pus_kab', 'Nama Puskesmas');
        $crud->display_as('Kode_pus_kab', 'Kode Kabupaten');
        
        

        //$crud->set_theme('twitter-bootstrap');

        $crud->unset_columns('Tipe_Puskesmas', 'Tipe_kawasan', 'Nama_pus_kab');

        
        $crud->callback_field('Kode_puskesmas', array($this, 'puskesmas_kode_callback'));
        $crud->callback_after_insert(array($this, 'puskesmas_akun_callback'));
     
       
        $_SESSION['ref_jenis_faskes_kode'] = 1;
        $_SESSION['jenis_faskes_kode'] = 'P';
        

        $output = $crud->render();
        $output->title = 'Data Puskesmas';
        $this->_example_output3($output);
    }

    public function Rumahsakit() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_rumahsakit');
        $crud->set_subject('Data Rumah Sakit');

        $crud->unset_edit();
        $crud->unset_delete();
                //$crud->unset_add();
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
     
        $crud->unset_fields('long', 'lat','kd_op','KD_JENIS_RS');
        
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');
        $crud->display_as('kode_rs', 'Kode Rumah Sakit');
        $crud->display_as('Nama_rs', 'Nama Rumah Sakit');
        $crud->display_as('JENIS_RS', 'Jenis Rumah Sakit');
        $crud->display_as('KELAS_RS', 'Kelas Rumah Sakit');
        $crud->display_as('JML_TT', 'Jumlah Tempat Tidur');
       

        //$crud->set_theme('datatables');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');
        
        
        $crud->callback_field('kode_rs', array($this, 'rs_kode_callback'));
        $crud->callback_after_insert(array($this, 'rs_akun_callback'));
     
       
        $_SESSION['ref_jenis_faskes_kode'] = 2;
        $_SESSION['jenis_faskes_kode'] = 'R';

        $output = $crud->render();
        $output->title = 'Data Rumah Sakit';
        $this->_example_output3($output);
    }

    public function Bapelkes() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_bapelkes');
        $crud->set_primary_key('faskes_kode');
        $crud->set_subject('Data Bapelkes');

        $crud->unset_edit();
        $crud->unset_delete();
                
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');
      
        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 5;
        $_SESSION['jenis_faskes_kode'] = 'B';
        

        $output = $crud->render();
        $output->title = 'Data Bapelkes';
        $this->_example_output3($output);
    }

    public function Faskeslain() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_faskeslain');
        $crud->set_subject('Data Faskes Lain');

        $crud->unset_edit();
        $crud->unset_delete();
                
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');
      

        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 99;
        $_SESSION['jenis_faskes_kode'] = 'Z';
        

        $output = $crud->render();
        $output->title = 'Data Faskes Lain';
        $this->_example_output3($output);
    }
    
    public function Upt() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_upt');
        $crud->set_subject('Data UPT Pusat');

        $crud->unset_edit();
        $crud->unset_delete();
                
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Lokasi Kabupaten');
        $crud->display_as('no_prov', 'Lokasi Provinsi');
   
        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 98;
        $_SESSION['jenis_faskes_kode'] = '1U';
        

        $output = $crud->render();
        $output->title = 'Data UPT Pusat';
        $this->_example_output3($output);
    }
    
     public function Uptdprov() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_uptdprov');
        $crud->set_subject('Data UPTD Provinsi');

        $crud->unset_edit();
        $crud->unset_delete();
                
        $crud->unset_back_to_list();
        
        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Lokasi Kabupaten');
        $crud->display_as('no_prov', 'Lokasi Provinsi');

        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 97;
        $_SESSION['jenis_faskes_kode'] = '2U';
        

        $output = $crud->render();
        $output->title = 'Data UPTD Provinsi';
        $this->_example_output3($output);
    }
    
    public function Uptdkab() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_uptdkab');
        $crud->set_subject('Data UPT Kab/Kota');

        $crud->unset_edit();
        $crud->unset_delete();
        
        $crud->unset_back_to_list();
        
        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Lokasi Kabupaten');
        $crud->display_as('no_prov', 'Lokasi Provinsi');

        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 96;
        $_SESSION['jenis_faskes_kode'] = '3U';
        

        $output = $crud->render();
        $output->title = 'Data UPT Kab/Kota';
        $this->_example_output3($output);
    }
    
   
    
    

    public function kkp() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_kkp');
        $crud->set_subject('Data KKP');

        $crud->unset_edit();
        $crud->unset_delete();
        
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');

        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 4;
        $_SESSION['jenis_faskes_kode'] = 'E';
        

        $output = $crud->render();
        $output->title = 'Data KKP';
        $this->_example_output3($output);
    }

    public function klinik() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_klinik');
        $crud->set_subject('Data Klinik');

        $crud->unset_edit();
        $crud->unset_delete();
        
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');

        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

        $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 9;
        $_SESSION['jenis_faskes_kode'] = 'K';

        $output = $crud->render();
        $output->title = 'Data Klinik';
        $this->_example_output3($output);
    }

  

    public function labkes() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_labkes');
        $crud->set_subject('Data Labkes');

        $crud->unset_edit();
        $crud->unset_delete();
        
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');

        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

        
         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 10;
        $_SESSION['jenis_faskes_kode'] = 'M';

        $output = $crud->render();
        $output->title = 'Data Labkes';
        $this->_example_output3($output);
    }

    public function bpkm() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_bpkm');
        $crud->set_subject('Data Balai Pengobatan / Kesehatan Masyarakat');

        $crud->unset_edit();
        $crud->unset_delete();
         
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');

        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 8;
        $_SESSION['jenis_faskes_kode'] = 'H';
        
        $output = $crud->render();
        $output->title = 'Data Balai Pengobatan / Kesehatan Masyarakat';
        $this->_example_output3($output);
    }

    public function btklp() {
        $crud = new grocery_CRUD();

        $crud->set_table('ref_btklp');
        $crud->set_subject('Data Bidang Teknik Kesehatan Lingkungan Dan Pengendalian Penyakit');

        $crud->unset_edit();
        $crud->unset_delete();
        
        $crud->unset_back_to_list();

        $crud->set_relation('no_prov', 'ref_provinsi', 'NAMA_PROV');
        $crud->set_relation('no_kab', 'ref_kabupaten', 'NAMA_KAB');
        $crud->unset_columns('long', 'lat');
        $crud->display_as('no_kab', 'Kabupaten');
        $crud->display_as('no_prov', 'Provinsi');
        
        //$crud->set_theme('twitter-bootstrap');
        // $crud->unset_columns('Tipe_Puskesmas','Tipe_kawasan','Nama_pus_kab');

         $crud->callback_field('faskes_kode', array($this, 'faskes_kode_callback'));
        $crud->callback_after_insert(array($this, 'akun_callback'));
        $crud->unset_fields('tipe','long','lat');
       
        $_SESSION['ref_jenis_faskes_kode'] = 3;
        $_SESSION['jenis_faskes_kode'] = 'F';
        

        $output = $crud->render();
        $output->title = 'Data Balai Pengobatan / Kesehatan Masyarakat';
        $this->_example_output3($output);
    }

    
      function faskes_kode_callback($value = '', $primary_key = null) {
        return '<input type="text" maxlength="90" value="'.$_SESSION['jenis_faskes_kode'] . rand(0, 9999) . 'T" name="faskes_kode" style="width:462px" readonly>';
    }

    function akun_callback($post_array,$ref_jenis_faskes_kode) {
      
        $data_akun = array();
        $data_akun['nama_akun'] = $post_array['faskes_kode'];
        $data_akun['NO_KAB'] = $post_array['no_kab'];
        $data_akun['NO_PROV'] = $post_array['no_prov'];
        $data_akun['faskes_kode'] = $post_array['faskes_kode'];
        $data_akun['ref_jenis_faskes_kode'] = $_SESSION['ref_jenis_faskes_kode'];
        
        $this->db->insert('tbl_akun',$data_akun);

        return true;
    }
    
      function rs_kode_callback($value = '', $primary_key = null) {
        return '<input type="text" maxlength="90" value="'.$_SESSION['jenis_faskes_kode']. rand(0, 9999) . 'T" name="kode_rs" style="width:462px" readonly>';
    }

    function rs_akun_callback($post_array,$ref_jenis_faskes_kode) {
      
        $data_akun = array();
        $data_akun['nama_akun'] = $post_array['kode_rs'];
        $data_akun['NO_KAB'] = $post_array['no_kab'];
        $data_akun['NO_PROV'] = $post_array['no_prov'];
        $data_akun['faskes_kode'] = $post_array['kode_rs'];
        $data_akun['ref_jenis_faskes_kode'] = $_SESSION['ref_jenis_faskes_kode'];
        
        $this->db->insert('tbl_akun',$data_akun);

        return true;
    }
    
    function puskesmas_kode_callback($value = '', $primary_key = null) {
        return '<input type="text" maxlength="90" value="'.$_SESSION['jenis_faskes_kode']. rand(0, 9999) . 'T" name="Kode_puskesmas" style="width:462px" readonly>';
    }

    function puskesmas_akun_callback($post_array,$ref_jenis_faskes_kode) {
      
        $data_akun = array();
        $data_akun['nama_akun'] = $post_array['Kode_puskesmas'];
        $data_akun['NO_KAB'] = $post_array['no_kab'];
        $data_akun['NO_PROV'] = $post_array['no_prov'];
        $data_akun['faskes_kode'] = $post_array['Kode_puskesmas'];
        $data_akun['ref_jenis_faskes_kode'] = $_SESSION['ref_jenis_faskes_kode'];
        
        $this->db->insert('tbl_akun',$data_akun);

        return true;
    }
    
    
    public function uraian_tugas() {
        $crud = new grocery_CRUD();

        $crud->set_table('kbk');
        $crud->set_subject('Entry Uraian Tugas Tenaga Kesehatan');

        $crud->set_relation('jenis_tugas_id', 'ref_jenis_tugas', 'jenis_tugas_nama');
        $crud->set_relation('satuan_id', 'ref_satuan', 'satuan_nama');
        $crud->set_relation('ref_jenis_sdmk_kode', 'ref_jenis_sdmk', 'ref_jenis_sdmk_nama_level2');
         $crud->set_relation('ref_jenis_sdmk_jft_id', 'ref_jenis_sdmk_jft', 'jft');
 
        $crud->unset_fields('SBK', 'STP');
        $crud->unset_columns('SBK', 'STP','norma_waktu','satuan_id');
        
        $crud->display_as('jenis_tugas_id', 'Jenis');
        $crud->display_as('satuan_id', 'Satuan');
        $crud->display_as('ref_jenis_sdmk_kode', 'SDMK');
        $crud->display_as('ref_jenis_sdmk_jft_id', 'Jabatan');
        $crud->display_as('kbk_uraian', 'Uraian Tugas');        

        $crud->set_theme('twitter-bootstrap');
      

        $output = $crud->render();
        $output->title = 'Data Balai Pengobatan / Kesehatan Masyarakat';
        $this->_example_output($output);
    }
    
}
