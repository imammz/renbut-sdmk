<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3 Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
<head>
		<title>CMS Enjoy Aceh Mobile App</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
                <link href="<?php echo base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/style.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<!--<link rel="stylesheet/less" type="text/css" href="assets/css/styles.less">-->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/theme_light.css" type="text/css" id="skin_color">
		<link rel="icon" href="<?php echo base_url() ?>assets/images/icon.png">
		<!--[if IE 7]>
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: HEADER -->
		<div class="navbar navbar-inverse navbar-fixed-top">
			<!-- start: TOP NAVIGATION CONTAINER -->
			<div class="container">
				<div class="navbar-header">
					<!-- start: RESPONSIVE MENU TOGGLER -->
					<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
						<span class="clip-list-2"></span>
					</button>
					<!-- end: RESPONSIVE MENU TOGGLER -->
					<!-- start: LOGO -->
					<a class="navbar-brand" href="#">
						<img src="http://up1.tuspics.net/IM13/i/00141/fhsk285dipvp.png" width="24" />
                                                Dinas Kebudayaan dan Pariwisata Provinsi Aceh
                                        </a>
					<!-- end: LOGO -->
				</div>
				<div class="navbar-tools">
					<!-- start: TOP NAVIGATION MENU -->
					<ul class="nav navbar-right">
						<!-- start: USER DROPDOWN -->
						<li class="current-user">
							<a href="#">
								<span class="username"><i class="clip-user"></i> <strong>Nama Admin</strong></span>
							</a>
						</li>
						<!-- end: USER DROPDOWN -->
					</ul>
					<!-- end: TOP NAVIGATION MENU -->
				</div>
			</div>
			<!-- end: TOP NAVIGATION CONTAINER -->
		</div>
		<!-- end: HEADER -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container">
			<div class="navbar-content">
				<!-- start: SIDEBAR -->
				<div class="main-navigation navbar-collapse collapse">
					<!-- start: MAIN MENU TOGGLER BUTTON -->
					<div class="navigation-toggler">
						<i class="clip-chevron-left"></i>
						<i class="clip-chevron-right"></i>
					</div>
					<!-- end: MAIN MENU TOGGLER BUTTON -->
					<!-- start: MAIN NAVIGATION MENU -->
					<ul class="main-navigation-menu">
						<li class="active open">
                                                    <a href="<?php echo base_url() ?>dashboard/dashboard/home"><i class="clip-home-3"></i>
								<span class="title"> Beranda </span><span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard/dashboard/venue"><i class="clip-map"></i>
								<span class="title">Data Venue Wisata</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard/dashboard/category"><i class="clip-list-3"></i>
								<span class="title">Data Category Venue</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard/dashboard/travel_blog"><i class="clip-pencil"></i>
								<span class="title">Data Travel Blog</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard/dashboard/travel_package"><i class="clip-airplane"></i>
								<span class="title">Data Travel Package</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard/dashboard/news"><i class="clip-book"></i>
								<span class="title">Berita</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard/dashboard/event"><i class="clip-calendar"></i>
								<span class="title">Event Budaya</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard/dashboard/city"><i class="clip-location"></i>
								<span class="title">Data Kota</span>
								<span class="selected"></span>
							</a>
						</li>
						<li>
							<a href="javascript:void(0)"><i class="clip-wrench-2"></i>
								<span class="title">Config</span><i class="icon-arrow"></i>
								<span class="selected"></span>
							</a>
							<ul class="sub-menu">
								<li>
									<a href="<?php echo base_url() ?>dashboard/dashboard/account">
										<span class="title">Account</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url() ?>dashboard/dashboard/user">
										<span class="title">User Management </span>
									</a>
								</li>								
							</ul>
						</li>
						<li>
							<a href="<?php echo base_url() ?>dashboard"><i class="clip-user-cancel"></i>
								<span class="title">Logout</span>
								<span class="selected"></span>
							</a>
						</li>
					</ul>
					<!-- end: MAIN NAVIGATION MENU -->
				</div>
				<!-- end: SIDEBAR -->
			</div>
			<!-- start: PAGE -->
			<div class="main-content">
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="row">
						<div class="col-sm-12">
							<!-- start: PAGE TITLE & BREADCRUMB -->
							<ol class="breadcrumb">
								<li>
									<i class="clip-home-3"></i>
									<a href="#">
										Dashboard
									</a>
								</li>
								<li class="active">
									Home
								</li>
								<li class="search-box">
									<form class="sidebar-search">
										<div class="form-group">
											<input type="text" placeholder="Start Searching...">
											<button class="submit">
												<i class="clip-search-3"></i>
											</button>
										</div>
									</form>
								</li>
							</ol>
							<div class="page-header">
								<h1>Dashboard <small>overview &amp; stats </small></h1>
							</div>
							<!-- end: PAGE TITLE & BREADCRUMB -->
						</div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-sm-12">
                                                    <h2>Selamat Datang Di Content Management System (CMS) Applikasi Visit Aceh</h2>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<div class="footer clearfix">
			<div class="footer-inner">
				2014 &copy; Visit Aceh.
			</div>
			<div class="footer-items">
				<span class="go-top"><i class="clip-chevron-up"></i></span>
			</div>
		</div>
		<!-- end: FOOTER -->
		<div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title">Event Management</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" data-dismiss="modal" class="btn btn-light-grey">
							Close
						</button>
						<button type="button" class="btn btn-danger remove-event no-display">
							<i class='fa fa-trash-o'></i> Delete Event
						</button>
						<button type='submit' class='btn btn-success save-event'>
							<i class='fa fa-check'></i> Save
						</button>
					</div>
				</div>
			</div>
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<![endif]-->
		<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="<?php echo base_url() ?>assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.pie.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/flot/jquery.flot.resize.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
		<script src="<?php echo base_url() ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.js"></script>
		<script src="<?php echo base_url() ?>assets/js/index.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Index.init();
			});
		</script>
	</body>
	<!-- end: BODY -->
</html>