<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang["sorry,_that_page_doesn't_exist"] = "sorry, that page doesn't exist";
$lang['the_link_you_followed_may_be_broken,_or_the_page_may_have_been_removed'] = 'The link you followed may be broken, or the page may have been removed.';
$lang["sorry,_can't_open_this_content"] = "sorry, can't open this content";
$lang['you_does_not_have_access_rights_to_the_content_so_server_is_rejecting_to_give_proper_response'] = 'you does not have access rights to the content so server is rejecting to give proper response';