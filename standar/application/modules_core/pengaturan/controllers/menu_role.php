<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_role extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pengaturan/menu_role_model','menu_role');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Menu Role";
		$data['subtitle'] = "Daftar Menu Role";
		$this->load->view('menu_role/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Menu Role";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->menu_role->get_by_id($id);
		}
		$this->load->view('menu_role/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->menu_role->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $menu_role) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($menu_role->role_name);
			$row[] = ''.$this->menu_role->get_menu_role_action($menu_role->id_role).'';
			$row[] = ($menu_role->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $menu_role->updated_date?Tanggal::formatDateTime($menu_role->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void()" title="Edit" onclick="setting('."'".Regex::_genRegex($menu_role->id_role,'RGXINT')."'".')"><i class="glyphicon glyphicon-cog"></i> Setting Hak Aksess</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->menu_role->count_all(),
						"recordsFiltered" => $this->menu_role->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		//print_r($_POST);die;
		$id_role = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();

		// Begin Proses //

		// delete all field by id_role
		$this->db->delete('t_menu_role', array('id_role'=>$id_role));

		$post_chk = $this->input->post('chk');

		if($post_chk){

			foreach ($post_chk as $key => $value) {

				$explode_post = explode('-', $value);
				$id_menu = $explode_post[0];
				$code = $explode_post[1];

				// cek main menu //
				$check_main_menu = $this->db->get_where('m_menu', array('id_menu'=>$id_menu))->row();

				if($check_main_menu->parent_menu != 0){

					// cek t_menu_role by main_menu id
					$check_menu_role_exist = $this->db->get_where('t_menu_role', array('id_menu'=>$check_main_menu->parent_menu, 'id_role'=>$id_role))->row();

					if(empty($check_menu_role_exist)){
						
						// jika tidak ada main menu maka insert 
						$main_menu = array(
							'id_role' => $id_role,
							'id_menu' => $check_main_menu->parent_menu,
							'code' => $code,
						);	

						$this->db->insert('t_menu_role', $main_menu);
					}
				}

				$result_explode = array(
					'id_role' => $id_role,
					'id_menu' => $id_menu,
					'code' => $code,
					);

				$this->db->insert('t_menu_role', $result_explode);

			}

		}
		

		// End Proses //
		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->menu_role->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
