<?php
require('../include/connect.php');

################ Save & delete markers #################
if ($_POST) 
    {
    $xhr = $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
    if (!$xhr) {
        header('HTTP/1.1 500 Error: Request must come from Ajax!');
        exit();
    }
    
    // get marker position and split it for database
    $mLatLang = explode(',', $_POST["latlang"]);
    $mLat     = filter_var($mLatLang[0], FILTER_VALIDATE_FLOAT);
    $mLng     = filter_var($mLatLang[1], FILTER_VALIDATE_FLOAT);
    
    //Delete Marker
    if (isset($_POST["del"]) && $_POST["del"] == true) {
        $results = mysql_query("DELETE FROM markers WHERE lat=$mLat AND lng=$mLng");
        if (!$results) {
            header('HTTP/1.1 500 Error: Could not delete Markers!');
            exit();
        }
        exit("Success Delete Marker!");
    }
    
    $mName    = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    $mAddress = filter_var($_POST["address"], FILTER_SANITIZE_STRING);
    $mInfo1    = filter_var($_POST["info1"], FILTER_SANITIZE_STRING);
	$mInfo2    = filter_var($_POST["info2"], FILTER_SANITIZE_STRING);
	$mInfo3    = filter_var($_POST["info3"], FILTER_SANITIZE_STRING);
	$mInfo4    = filter_var($_POST["info4"], FILTER_SANITIZE_STRING);
    
    $results = mysql_query("INSERT INTO markers (name, address, lat, lng, info1,info2,info3,info4) VALUES ('$mName','$mAddress',$mLat, $mLng, '$mInfo1', '$mInfo2', '$mInfo3', '$mInfo4')");
    if (!$results) {
        header('HTTP/1.1 500 Error: Could not create marker!');
        exit();
    }
    
    $output = '<h1 class="marker-heading">' . $mName . '</h1><p>' . $mAddress . '</p>';
    exit($output);
}

$dom     = new DOMDocument("1.0");
$node    = $dom->createElement("markers"); //Create new element node
$parnode = $dom->appendChild($node); //make the node show up 

// Select all the rows in the markers table
$results = mysql_query("SELECT * FROM markers WHERE 1");
if (!$results) {
    header('HTTP/1.1 500 Error: Could not get markers!');
    exit();
}

//set document header to text/xml
header("Content-type: text/xml");


while ($x = mysql_fetch_assoc($results)) {
    $node    = $dom->createElement("marker");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("name", $x['name']);
    $newnode->setAttribute("address", $x['address']);
    $newnode->setAttribute("lat", $x['lat']);
    $newnode->setAttribute("lng", $x['lng']);
    $newnode->setAttribute("type", $x['type']);
}

echo $dom->saveXML();
