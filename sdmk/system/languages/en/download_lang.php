<?php if (!defined('BASEPATH')) exit('no direct script user allowed');
$lang['set_as_publish'] = 'set as publish';
$lang['set_as_draft'] = 'set as draft';
$lang['download_type'] = 'download type';
$lang['this_download_to_manage_download_content,_see_documentation_for_use_it'] = 'This download to manage download content, see documentation for use it';