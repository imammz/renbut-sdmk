<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">

<!-- end: HEAD -->
<body>
    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 300px;">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                echo "<h2>Hapus Data Installasi</h2>";
                ?>
                <hr/>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/hapusInstallasi_proses/<?php echo $faskes_installasi['faskes_installasi_id'] ?>">


                    <div class="form-group">
                        <div class="col-sm-12" style="text-align: center;">
                            <a href="#" class="list-group-item list-group-item-danger">
                                <h2>	Anda Yakin Akan Menghapus Data  <strong><?php echo $faskes_installasi['installasi_nama'] ?></strong> </h2>		
                                <h4>)* Menghapus Data Installasi akan menghapus seluruh data perhitungan Kebutuhan SDMK di Installasi / Subinstallasi / Bagian / Subbagian tersebut.</h4>
                            </a>
                        </div>
                    </div>

                    <div class="form-group" style="text-align: center;">
                        <a href="#" class="btn btn-blue btn-flat" onclick="parent.GB_hide();">Batal</a>	
                        &nbsp;
                        <button class="btn btn-danger btn-flat" onclick="return confirm('Anda Yakin?')" type="submit" name="simpan"> <i class="fa fa-trash"></i> HAPUS </button>
                        &nbsp;
                    </div>
                </form>  
            </div>
        </div>
        <!-- end: YOUR CONTENT HERE -->
    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });

                            function genKode() {

                                var selected = document.getElementById('faskes_kode');
                                var faskes_kode = selected.options[selected.selectedIndex].value;

                                var url = '<?php echo base_url() ?>index.php/entry/<?php echo $class ?>/getData/';

                                $.post(url,
                                        {
                                            faskes_kode: faskes_kode
                                        },
                                        function (data) {

                                            if (data.result) {

                                                $('#faskes_kode').val(data.faskes_kode);
                                                $('#NO_KAB option[value=' + data.NO_KAB + ']').attr('selected', 'selected');
                                                $('#faskes_nama').val(data.faskes_nama);
                                                $('#faskes_telp').val(data.faskes_telp);
                                                $('#faskes_alamat').val(data.faskes_alamat);
                                                $('#faskes_email').val(data.faskes_email);
                                                $('#ref_faskes_kelas_id option[value=' + data.ref_faskes_kelas_id + ']').attr('selected', 'selected');
                                                $('#ref_jenis_faskes_kode option[value=' + data.ref_jenis_faskes_kode + ']').attr('selected', 'selected');
                                                $('#status_pengelolaan_id option[value=' + data.status_pengelolaan_id + ']').attr('selected', 'selected');

                                            } else {
                                                alert('Data <?php echo $subtitle ?> Tidak Tersedia');
                                            }

                                        }, "json");


                            }

    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
