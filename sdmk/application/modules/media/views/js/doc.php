<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<script type="text/javascript">
    $(function() {
        var d = new Date();
        var id = d.getTime();
        var ul = $('div#xamzo-media ul#docs');

        $('#jq-drop a').click(function() {
            $(this).parent().find('input').click();
        });

        $('#jq-upload').fileupload({
            dropZone: $('#jq-drop'),
            submit: function(e, data) {
                var et = (data.files[0].name);
                var ext = (et.split('.').pop());
                if (!ext.match(/<?php echo $this->lib_media->_config('type_document') ?>/gi)) {
                    $('li#' + id).remove();
                    notive('Only <?php echo $this->lib_media->_config('type_document') ?> files are allowed');
                    return false;
                    data.stop();
                } else {
                    return true;
                }
            },
            add: function(e, data) {
                var tpl = $('<li id="' + id + '" style="padding:20px 0; text-align:center"><input type="text" value="0" data-width="48" data-height="48"' +
                        ' data-fgColor="#5E5408" data-readOnly="1" data-bgColor="#4B4200" /><p></p><span></span></li>');
                data.context = tpl.prependTo(ul);
                tpl.find('input').knob();
                tpl.find('span').click(function() {
                    if (tpl.attr('id', id)) {
                        jqXHR.abort();
                    }
                    tpl.fadeOut(function() {
                        tpl.remove();
                    });
                });
                var jqXHR = data.submit();
            },
            progress: function(e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                data.context.find('input').val(progress).change();
            },
            fail: function(e, data) {
                data.context.addClass('error');
            },
            success: function(rs) {
                if (rs !== 'error') {
                    setTimeout(function() {
                        $('div#xamzo-media ul#docs').prepend(rs);
                    }, 1000);
                }else{
                    notive(rs);
                }
                $('li#' + id).remove();
            }
        });
        $(document).on('drop dragover', function(e) {
            e.preventDefault();
        });
    });
</script>    