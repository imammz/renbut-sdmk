<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<div class="modal-header">
    <button type="button" class="close" onclick='close_box()' data-dismiss="modal" aria-hidden="true">&times;</button>
    <h2><?php echo $this->lang["confirmation"]; ?></h2>
</div>
<div class="modal-body">
    <p><?php echo $this->lang["are_you_sure,_want_to_delete_file_selected_?"]; ?></p>
</div>
<div class="modal-footer border-top">
    <a class="btn primary medium right tip-top" original-title="<?php echo $this->lang['yes'] ?>" onclick="this_file_delete('<?php echo $url ?>');"><?php echo $this->lang['yes']; ?></a>
</div> 
