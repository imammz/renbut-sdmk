<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package     libraries/database
 * @author      xamzo developer
 * @copyright   Copyright (c) 2013, xamzo, Inc.
 * @license     http://xamzo.com/license/
 * @link        http://xamzo.com
 * @since       Version 1.0
 * @filesource
 */ 

$db = x_db_config();               // load connection array setting
$driver = $db['database']['driver'];

/**
 * Description of Orm
 *
 * @author user
 */
switch ($driver) {
    case 'mysql':
    case 'pdo':
        // load driver
        load_class(array('Db_prepare', 'Db_result_obj', 'Db_active_rec'), 'libraries/database/driver');
        load_class('Mysql_pdo', 'libraries/database/driver/mysql_pdo');
        class_alias('Mysql_pdo', 'extDb');
        break; 
    default:
        break;
}

class Database extends extDb {

    public function __construct() {
        parent::__construct();
    }

}

class_alias('Database', 'Db'); 