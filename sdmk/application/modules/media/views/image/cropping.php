<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?>  
<script src="<?php echo site_url('application/modules/media/assets/jcrop/main.js') ?>"></script>
<script type="text/javascript">
    jQuery(function($) {

        // The variable jcrop_api will hold a reference to the
        // Jcrop API once Jcrop is instantiated.
        var jcrop_api;

        // In this example, since Jcrop may be attached or detached
        // at the whim of the user, I've wrapped the call into a function
        initJcrop();

        // The function is pretty simple
        function initJcrop()//{{{
        {
            // Hide any interface elements that require Jcrop
            // (This is for the local user interface portion.)
            $('.requiresjcrop').hide();

            // Invoke Jcrop in typical fashion
            $('#target').Jcrop({
                onRelease: releaseCheck,
                onSelect: updateCoords
            }, function() {

                jcrop_api = this;
                jcrop_api.animateTo([100, 100, 400, 300]);

                // Setup and dipslay the interface for "enabled"
                $('#can_click,#can_move,#can_size').attr('checked', 'checked');
                $('#ar_lock,#size_lock,#bg_swap').attr('checked', false);
                $('.requiresjcrop').show();

            });

        }
        ;
        //}}}
        function updateCoords(c)
        {
            $('#x').val(c.x);
            $('#y').val(c.y);
            $('#w').val(c.w);
            $('#h').val(c.h);
        }
        ;

        // Use the API to find cropping dimensions
        // Then generate a random selection
        // This function is used by setSelect and animateTo buttons
        // Mainly for demonstration purposes
        function getRandom() {
            var dim = jcrop_api.getBounds();
            return [
                Math.round(Math.random() * dim[0]),
                Math.round(Math.random() * dim[1]),
                Math.round(Math.random() * dim[0]),
                Math.round(Math.random() * dim[1])
            ];
        }
        ;

        // This function is bound to the onRelease handler...
        // In certain circumstances (such as if you set minSize
        // and aspectRatio together), you can inadvertently lose
        // the selection. This callback re-enables creating selections
        // in such a case. Although the need to do this is based on a
        // buggy behavior, it's recommended that you in some way trap
        // the onRelease callback if you use allowSelect: false
        function releaseCheck()
        {
            jcrop_api.setOptions({allowSelect: true});
            $('#can_click').attr('checked', false);
        }
        ;

        // Attach interface buttons
        // This may appear to be a lot of code but it's simple stuff
        $('#setSelect').click(function(e) {
            // Sets a random selection
            jcrop_api.setSelect(getRandom());
        });
        $('#animateTo').click(function(e) {
            // Animates to a random selection
            jcrop_api.animateTo(getRandom());
        });
        $('#release').click(function(e) {
            // Release method clears the selection
            jcrop_api.release();
        });
        $('#disable').click(function(e) {
            // Disable Jcrop instance
            jcrop_api.disable();
            // Update the interface to reflect disabled state
            $('#enable').show();
            $('.requiresjcrop').hide();
        });
        $('#enable').click(function(e) {
            // Re-enable Jcrop instance
            jcrop_api.enable();
            // Update the interface to reflect enabled state
            $('#enable').hide();
            $('.requiresjcrop').show();
        });
        $('#rehook').click(function(e) {
            // This button is visible when Jcrop has been destroyed
            // It performs the re-attachment and updates the UI
            $('#rehook,#enable').hide();
            initJcrop();
            $('#unhook,.requiresjcrop').show();
            return false;
        });
        $('#unhook').click(function(e) {
            // Destroy Jcrop widget, restore original state
            jcrop_api.destroy();
            // Update the interface to reflect un-attached state
            $('#unhook,#enable,.requiresjcrop').hide();
            $('#rehook').show();
            return false;
        });


        // The checkboxes simply set options based on it's checked value
        // Options are changed by passing a new options object

        // Also, to prevent strange behavior, they are initially checked
        // This matches the default initial state of Jcrop

        $('#can_click').change(function(e) {
            jcrop_api.setOptions({allowSelect: !!this.checked});
            jcrop_api.focus();
        });
        $('#can_move').change(function(e) {
            jcrop_api.setOptions({allowMove: !!this.checked});
            jcrop_api.focus();
        });
        $('#can_size').change(function(e) {
            jcrop_api.setOptions({allowResize: !!this.checked});
            jcrop_api.focus();
        });
        $('#ar_lock').change(function(e) {
            jcrop_api.setOptions(this.checked ?
                    {aspectRatio: 4 / 3} : {aspectRatio: 0});
            jcrop_api.focus();
        });
        $('#size_lock').change(function(e) {
            jcrop_api.setOptions(this.checked ? {
                minSize: [80, 80],
                maxSize: [350, 350]
            } : {
                minSize: [0, 0],
                maxSize: [0, 0]
            });
            jcrop_api.focus();
        });

    });


</script> 
<link rel="stylesheet" href="<?php echo site_url('application/modules/media/assets/jcrop/style.css') ?>" type="text/css" />
<style type="text/css">
    .optdual { position: relative; }
    .optdual .offset { position: absolute; left: 18em; }
    .optlist label { width: 16em; display: block; }
    #dl_links { margin-top: .5em; }

</style>

<div class="modal-header">
    <button type="button" class="close" onclick='close_box();' data-dismiss="modal" aria-hidden="true">&times;</button>
    <h2><i class="fa fa-crop"></i> <?php echo $this->lang["crop"]; ?></h2>
</div>
<div class="modal-body">
    <form id="wm-form" onsubmit="return false;">
        <input type="hidden" name="wm_emitter" value="1">
        <div> 
            <div style="position: relative">
                <input type="hidden" name="src" value="<?php echo $src ?>">
                <center>
                    <img src="<?php echo $src ?>" style="max-width: 100%;" id="target" alt="[Jcrop Example]"> 
                </center>
            </div> 
        </div>
        <div> 
            <div style="margin: .8em 0 .5em;">
                <center>
                    <span class="requiresjcrop">
                        <button id="setSelect" class="btn primary medium">setSelect</button>
                        <button id="animateTo" class="btn primary medium">animateTo</button>
                        <button id="release" class="btn primary medium">Release</button>
                        <button id="disable" class="btn primary medium">Disable</button>
                    </span>
                    <button id="enable" class="btn primary medium" style="display:none;">Re-Enable</button>
                    <button id="unhook" class="btn primary medium">Destroy!</button>
                    <button id="rehook" class="btn primary medium" style="display:none;">Attach Jcrop</button>
                </center>
            </div>

            <fieldset class="optdual requiresjcrop">
                <legend>Option Toggles</legend>
                <div>
                    <label><input type="checkbox" id="ar_lock" />Aspect ratio</label>
                    <label><input type="checkbox" id="size_lock" />minSize/maxSize setting</label> 
                    <label><input type="checkbox" id="can_move" />Selection can be moved</label>
                    <label><input type="checkbox" id="can_size" />Resizable selection</label> 
                    <label><input type="checkbox" id="can_click" />Allow new selections</label> 
                </div>
            </fieldset>
            <p class="info">
                <input type="hidden" id="x" name="x" />
                <input type="hidden" id="y" name="y" />
                <input type="hidden" id="w" name="w" />
                <input type="hidden" id="h" name="h" />
                <?php
                echo $this->lang['submit_and_get_new_image_cropping_in_your_media'];
                ?>
            </p>
            <p>
                <button class="btn primary medium" href="<?php echo site_url('media/cropping'); ?>" ajax-target=".xamzo-right" ajax="true" ajax-redirect="<?php echo site_url('media/crop') ?>" ajax-data="#wm-form" ajax-type="POST">Submit</button>
                <button type="button" class="btn primary medium" onclick='close_box();' data-dismiss="modal" aria-hidden="true"><?php echo $this->lang['cancel'] ?></button>
            </p>
        </div>
    </form>
</div>  