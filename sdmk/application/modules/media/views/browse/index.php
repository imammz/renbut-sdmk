<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<script type="text/javascript" src="<?php echo site_url('application/modules/media/assets/javascript/script.js') ?>"></script>
<div class="modal-header">
    <button type="button" class="close" onclick='close_box()' data-dismiss="modal" aria-hidden="true">&times;</button>
    <h2><i class="fa fa-picture"></i> <?php echo $this->lang['all_photos_in_your_media'] ?></h2>
</div>
<div class="modal-body media-browse-photo"> 
    <ul class="list_image">
        <?php
        $id = 0;
        foreach ($images as $r) { 
            $t = explode('{ca}', $r);
            $title = isset($t['1']) ? $t['1'] : '';
            echo "<li><img onclick=\"append_image(this,'$target')\" alt='$r' id='caImage'  src='" . site_url() . UPLOADPATH . $folder . '/' . $r . "'/></li>";
            $id++;
        }
        ?> 
    </ul> 
    <div class="xamzo-pagination">
        <div id="option">
            <?php 
            echo $this->lang['showing_1_to'] . "&nbsp;" . $limit . "&nbsp;" . $this->lang['of'] . " $total " . $this->lang['entries']
            ?> 
        </div>
        <div id="pagging">
            <?php echo $paging; ?> 
        </div>
    </div>
</div>
