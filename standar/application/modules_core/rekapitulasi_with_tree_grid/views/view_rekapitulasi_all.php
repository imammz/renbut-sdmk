<link rel="stylesheet" href="<?php echo base_url()?>assets/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxdata.js"></script> 
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxdatatable.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxtreegrid.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxcheckbox.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/jqwidgets/scripts/demos.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var employees = [
                { "EmployeeID": 1, "FirstName": "Nancy", "LastName": "Davolio", "ReportsTo": 2, "Country": "USA", "Title": "Sales Representative", "HireDate": "1992-05-01 00:00:00", "BirthDate": "1948-12-08 00:00:00", "City": "Seattle", "Address": "507 - 20th Ave. E.Apt. 2A" },
                { "EmployeeID": 2, "FirstName": "Andrew", "LastName": "Fuller", "ReportsTo": null, "Country": "USA", "Title": "Vice President, Sales", "HireDate": "1992-08-14 00:00:00", "BirthDate": "1952-02-19 00:00:00", "City": "Tacoma", "Address": "908 W. Capital Way" },
                { "EmployeeID": 3, "FirstName": "Janet", "LastName": "Leverling", "ReportsTo": 2, "Country": "USA", "Title": "Sales Representative", "HireDate": "1992-04-01 00:00:00", "BirthDate": "1963-08-30 00:00:00", "City": "Kirkland", "Address": "722 Moss Bay Blvd." },
                { "EmployeeID": 4, "FirstName": "Margaret", "LastName": "Peacock", "ReportsTo": 2, "Country": "USA", "Title": "Sales Representative", "HireDate": "1993-05-03 00:00:00", "BirthDate": "1937-09-19 00:00:00", "City": "Redmond", "Address": "4110 Old Redmond Rd." },
                { "EmployeeID": 5, "FirstName": "Steven", "LastName": "Buchanan", "ReportsTo": 2, "Country": "UK", "Title": "Sales Manager", "HireDate": "1993-10-17 00:00:00", "BirthDate": "1955-03-04 00:00:00", "City": "London", "Address": "14 Garrett Hill" },
                { "EmployeeID": 6, "FirstName": "Michael", "LastName": "Suyama", "ReportsTo": 5, "Country": "UK", "Title": "Sales Representative", "HireDate": "1993-10-17 00:00:00", "BirthDate": "1963-07-02 00:00:00", "City": "London", "Address": "Coventry House Miner Rd." },
                { "EmployeeID": 7, "FirstName": "Robert", "LastName": "King", "ReportsTo": 5, "Country": "UK", "Title": "Sales Representative", "HireDate": "1994-01-02 00:00:00", "BirthDate": "1960-05-29 00:00:00", "City": "London", "Address": "Edgeham Hollow Winchester Way" },
                { "EmployeeID": 8, "FirstName": "Laura", "LastName": "Callahan", "ReportsTo": 2, "Country": "USA", "Title": "Inside Sales Coordinator", "HireDate": "1994-03-05 00:00:00", "BirthDate": "1958-01-09 00:00:00", "City": "Seattle", "Address": "4726 - 11th Ave. N.E." },
                { "EmployeeID": 9, "FirstName": "Anne", "LastName": "Dodsworth", "ReportsTo": 5, "Country": "UK", "Title": "Sales Representative", "HireDate": "1994-11-15 00:00:00", "BirthDate": "1966-01-27 00:00:00", "City": "London", "Address": "7 Houndstooth Rd." }
            ];
        // prepare the data
        var source =
        {
            dataType: "json",
            dataFields: [
                { name: 'EmployeeID', type: 'number' },
                { name: 'ReportsTo', type: 'number' },
                { name: 'FirstName', type: 'string' },
                { name: 'LastName', type: 'string' },
                { name: 'Country', type: 'string' },
                { name: 'City', type: 'string' },
                { name: 'Address', type: 'string' },
                { name: 'Title', type: 'string' },
                { name: 'HireDate', type: 'date' },
                { name: 'BirthDate', type: 'date' }
            ],
            hierarchy:
            {
                keyDataField: { name: 'EmployeeID' },
                parentDataField: { name: 'ReportsTo' }
            },
            id: 'EmployeeID',
            localData: employees,
        };

        /*var source =
        {
            dataType: "json",
            dataFields: [
                { name: 'EmployeeID', type: 'number' },
                { name: 'ParentEmployeeID', type: 'number' },
                { name: 'FirstName', type: 'string' },
                { name: 'LastName', type: 'string' },
                { name: 'Title', type: 'string' },
                { name: 'HireDate', type: 'date' },
                { name: 'BirthDate', type: 'date' },
                { name: 'Phone', type: 'string' },
                { name: 'Gender', type: 'string' },
                { name: 'DepartmentName', type: 'string' }
            ],
            hierarchy:
            {
                keyDataField: { name: 'EmployeeID' },
                parentDataField: { name: 'ParentEmployeeID' }
            },
            id: 'EmployeeID',
            url: '../sampledata/employeesadv.csv'
        };*/
        
        var dataAdapter = new $.jqx.dataAdapter(source);
        // create Tree Grid
        $("#treeGrid").jqxTreeGrid(
        {
            width: 1300,
            source: dataAdapter,
            pageable: true,
            columnsResize: true,
            sortable: true,
            filterable: true,
            ready: function () {
                // expand row with 'EmployeeID = 32'
                $("#treeGrid").jqxTreeGrid('expandRow', 32);
                $("#treeGrid").jqxTreeGrid('expandRow', 112);
            },
            columns: [
              { text: 'FirstName', columnGroup: 'Name', dataField: 'FirstName', width: 200 },
              { text: 'LastName', columnGroup: 'Name', dataField: 'LastName', width: 200 },
              { text: 'Title', dataField: 'Title', width: 160 },
              { text: 'Birth Date', dataField: 'BirthDate', cellsFormat: 'd', width: 120 },
              { text: 'Hire Date', dataField: 'HireDate', cellsFormat: 'd', width: 120 },
              { text: 'Address', dataField: 'Address', width: 250 },
              { text: 'City', dataField: 'City', width: 120 },
              { text: 'Country', dataField: 'Country' }
            ],
        });
    });
</script>

<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT -->

    <center><h2>Laporan Perencanaan Kebutuhan Minimal SDM Kesehatan Puskesmas<br> 

     <?php echo ($posted['id_provinsi'] != 0) ? 'Provinsi '.Master::params_query(array('table'=>'m_provinsi', 'where'=>array('id_provinsi'=>$posted['id_provinsi'])))->row()->nama_provinsi : '' ?>

     <?php echo ($posted['id_kabupaten'] != 0) ? 'Kabupaten/Kota '.Master::params_query(array('table'=>'m_kabupaten', 'where'=>array('id_kabupaten'=>$posted['id_kabupaten'])))->row()->nama_kabupaten.'<br>' : '' ?>

     <?php echo ($posted['id_tipe_kawasan'] != 0) ? 'Kawasan '.Master::params_query(array('table'=>'m_tipe_kawasan', 'where'=>array('id_tipe_kawasan'=>$posted['id_tipe_kawasan'])))->row()->nama_tipe_kawasan : '' ?>

     <?php echo ($posted['id_tipe_puskesmas'] != 0) ? 'Kategori '.Master::params_query(array('table'=>'m_tipe_puskesmas', 'where'=>array('id_tipe_puskesmas'=>$posted['id_tipe_puskesmas'])))->row()->nama_tipe_puskesmas.'<br>' : '' ?>

       Tahun <?php echo $posted['tahun']?> </h2></center>
    
    <div id="treeGrid"></div>
    
    <div class="form-actions center">

      <button class="btn btn-sm btn-info" onclick="getMenu('rekapitulasi/form_rekapitulasi/<?php echo $id_jenis_faskes?>')"><i class="fa fa-arrow-left"></i> Kembali ke rekapitulasi </button>
      <a href="<?php echo base_url().'rekapitulasi/preview_all/excel/'.$posted['tahun'].'/'.$posted['id_provinsi'].'/'.$posted['id_kabupaten'].'/'.$posted['id_tipe_kawasan'].'/'.$posted['id_tipe_puskesmas'].'/'.$posted['id_jenis_faskes'].'/'.$isall.''?>" target="blank" class="btn btn-sm btn-success">
        <i class="ace-icon fa fa-file-excel-o icon-on-right bigger-110"></i>
        Excel
      </a>
      <a href="<?php echo base_url().'rekapitulasi/preview_all/json/'.$posted['tahun'].'/'.$posted['id_provinsi'].'/'.$posted['id_kabupaten'].'/'.$posted['id_tipe_kawasan'].'/'.$posted['id_tipe_puskesmas'].'/'.$posted['id_jenis_faskes'].'/'.$isall.''?>" class="btn btn-sm btn-inverse" value="submit">
        <i class="ace-icon fa fa-upload icon-on-right bigger-110"></i>
        Export
      </a>
    </div>

    
    

    

