<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['this_page_to_manage_website_setting,_see_documentation_for_use_it'] = 'This page to manage website setting, see documentation for use it';
$lang['info'] = 'info';
$lang['general'] = 'general';
$lang['media'] = 'media';
$lang['database'] = 'database';
$lang['email'] = 'email';
$lang['language'] = 'language';
/**
 * info
 */
$lang['engine'] = 'engine';
$lang['version'] = 'version';
$lang['copyright'] = 'copyright';
$lang['develop_by'] = 'develop by';
$lang['this_is_engine_cms_was_create_your_website'] = 'this is engine cms was create your website';
$lang['this_is_engine_version'] = 'this is engine version';
$lang['this_is_official_website'] = 'this is official website';
/**
 * database 
 */
$lang['driver'] = 'driver';
$lang['host'] = 'host';
$lang['user'] = 'user';
$lang['password'] = 'password';
$lang['name'] = 'name';
$lang['database_driver'] = 'database driver';
$lang['database_host'] = 'database host';
$lang['database_name'] = 'database name';
$lang['database_user'] = 'database user';
$lang['database_password'] = 'database password';
$lang['restore'] = 'restore';
$lang['download'] = 'download';
$lang['backup'] = 'backup';
$lang['check_all'] = 'check all';
$lang['list_table'] = 'list table';
$lang['list_backup'] = 'list backup';
$lang['backup_DB_on'] = 'backup DB on';
/**
 * general 
 */
$lang['site_url'] = 'base URL';
$lang['site_name'] = 'site name';
$lang['site_domain'] = 'site domain';
$lang['site_keyword'] = 'site keyword';
$lang['site_description'] = 'site description';
$lang['sess_name'] = 'session name';
$lang['sess_expired'] = 'session expired';
$lang['sess_encrypt'] = 'session encryptpe';
$lang['sess_encrypt_code'] = 'session encryptpe code';
$lang['default_base_url'] = 'default base url';
$lang['your_site_name'] = 'your site name';
$lang['your_site_domain'] = 'your site domain';
$lang['your_session_name'] = 'your session name';
$lang['your_site_keyword'] = 'your site keyword';
$lang['your_site_description'] = 'your site description';
$lang['session_expired_in_second'] = 'session expired in second';
$lang['encryptpe_your_session'] = 'encryptpe your session';
$lang['session_encryptpe_code'] = 'session encryptpe code';
$lang['date_format'] = 'Date format';
$lang['default_date_format_of_system'] = 'default date format of system';
$lang['default_language'] = 'Default language';
$lang['default_language_of_system'] = 'default language of system';
$lang['google_analytics'] = 'google analytics';
$lang['google_analytics_tracking_code'] = 'google analytics tracking code';
/**
 * media
 */
$lang['type_document'] = 'type document';
$lang['type_zip'] = 'type zip';
$lang['type_image'] = 'type image';
$lang['max_file_size'] = 'max file size';
$lang['big_thumb_width'] = 'big thumb width';
$lang['big_thumb_height'] = 'big thumb height';
$lang['middle_thumb_width'] = 'middle thumb width';
$lang['middle_thumb_height'] = 'middle thumb height';
$lang['small_thumb_width'] = 'small thumb width';
$lang['small_thumb_height'] = 'small thumb height';
$lang['type_file_document_allowed_to_upload_in_folder_document,_if_type_more_than_one_split_with(|)'] = 'type file document allowed to upload in folder document, if file more than one split with(|)';
$lang['type_file_zip_allowed_to_upload_in_folder_zip,_if_type_more_than_one_split_with(|)'] = 'type file zip allowed to upload in folder zip, if file more than one split with(|)';
$lang['type_file_image_allowed_to_upload_in_folder_image,_if_type_more_than_one_split_with(|)'] = 'type file image allowed to upload in folder image, if file more than one split with(|)';
$lang['maximum_size_for_file_upload'] = 'maximal size for file upload';
$lang['size_width_for_image_big_thumb'] = 'size width for image big thumb';
$lang['size_height_for_image_big_thumb'] = 'size height for image big thumb';
$lang['size_width_for_image_middle_thumb'] = 'size width for image middle thumb';
$lang['size_of_height_image_middle_thumb'] = 'size height for image middle thumb';
$lang['size_width_for_image_small_thumb'] = 'size width for image small thumb';
$lang['size_height_for_image_small_thumb'] = 'size height for image small thumb'; 
$lang['manage_your_configuration_media_file,_see_documentation_for_use_it'] = 'Manage configuration media file, see documentation for use it';
$lang['manage_your_configuration_project_file,_see_documentation_for_use_it'] = 'Manage configuration project file, see documentation for use it';
/**
 *  email
 */
$lang['template'] = 'template';
$lang['no_template_selected'] = 'no template selected';
$lang['no_email_configuration'] = 'No email configuration';
$lang['send_email_use_phpmail'] = 'Send email use phpmail';
$lang['send_email_use_smtp'] = 'Send email use smtp';
$lang['send_email_use_google_smtp'] = 'Send email use google smtp';
$lang['email_reply'] = 'Reply';
$lang['email_sender'] = 'Sender';
$lang['port'] = 'Port';
$lang['username'] = 'Username';
$lang['set_the_hostname_of_the_mail_server'] = 'Set the hostname of the mail server';
$lang['set_the_SMTP_port_number_-_likely_to_be_25,_26,_465_or_587'] = 'Set the SMTP port number -likely to be 25, 26, 465 or 587';
$lang['username_to_use_for_SMTP_authentication'] = 'Username to use for SMTP authentication';
$lang['password_to_use_for_SMTP_authentication'] = 'Password to use for SMTP authentication';
/**
 * security
 */
$lang['security'] = 'Security';
$lang['xss_filtering'] = 'XSS Filtering';
$lang['sql_injection'] = 'SQL Injection';
$lang['ip_proxy'] = 'IP Proxy';
$lang['set_true,_to_make_your_website_defence_from_xss_filtering'] = 'Set true, to make your website defence from xss filtering';
$lang['set_true,_to_make_your_website_defence_from_sql_injection'] = 'Set true, to make your website defence from sql injection';
$lang['defend_your_website_from_ip,_separate_with_[#]'] = 'Defend your website from ip, separate with [#]';
/**
 * modules/extension
 */
$lang['choose_extension_to_set'] = 'Choose extension to set';
$lang['choose_module_to_set'] = 'Choose extension to set';

