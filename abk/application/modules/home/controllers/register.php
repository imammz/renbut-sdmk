<?php

class Register extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
       // $this->checkLogin();
        unset($_SESSION['message']);
    }
    
    	public function index()
	{ 
            $this->output->enable_profiler(false);
            
            $data = array();
            $data['class'] = 'register';
            $data['jenis_faskes'] = $this->_getJenisFaskes($_SESSION['user']['nama_akun']);
            $data['ref_jenis_faskes_id'] = $this->_getJenisFaskes($_SESSION['user']['nama_akun']);
           
            $this->load->view('register_view',$data);
	}
        
        
        public function online($ref_jenis_faskes_kode)
	{ 
            $this->output->enable_profiler(false);
            
            $data = array();
            $data['class'] = 'register';
            $data['ref_jenis_faskes'] = $this->orm->ref_jenis_faskes->where('ref_jenis_faskes_kode',$ref_jenis_faskes_kode)->fetch();
           
            $this->load->view('register_online_view',$data);
	}
        
        
        public function _getJenisFaskes($username) {
            $account_model = new account_model();
            
            $jenis_faskes = $this->orm->ref_jenis_faskes->where('ref_jenis_faskes_kode',$account_model->_checkJenisFaskes($username))->fetch();
            
            return $jenis_faskes['ref_jenis_faskes_nama'];
        }
        
        

}

?>