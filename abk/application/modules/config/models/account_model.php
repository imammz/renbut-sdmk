<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class account_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function cekLogin($nama_akun, $password) {
        $check = $this->orm->tbl_akun->where('nama_akun = ? AND password_akun = ?', $nama_akun, $password);

        if (count($check) != 0) {
            return true;
        } else {
            return false;
        }
    }

    public function cekAvaible($nama_akun) {

        $check = $this->orm->tbl_akun->where('nama_akun', $nama_akun);

        if (count($check) != 0) {
            return false;
        } else {
            return true;
        }
    }

    public function _getStoreName($idrefstore) {
        $ress = $this->orm->refstore->where('idrefstore', $idrefstore)->fetch();

        return $ress['nama'];
    }

    public function _getLevelUser($faskes_kode, $no_kab, $no_prov) {
        
        $user = '';

        if ($faskes_kode == 'A' && $no_kab == 'A' && $no_prov == 'A') {
            $user = 'PUSRENGUN BPPSDM Kementrian Kesehatan RI';
        } elseif ($faskes_kode == 'A' && $no_kab == 'A' && $no_prov != 'A') {
            $prov = $this->orm->ref_provinsi->where('NO_PROV', $no_prov)->fetch();
            $user = 'Dinas Kesehatan Provinsi ' . $prov['NAMA_PROV'];
        } elseif ($faskes_kode == 'A' && $no_kab != 'A' && $no_prov != 'A') {
            $prov = $this->orm->ref_kabupaten->where('NO_KAB', $no_kab)->fetch();
            $user = 'Dinas Kesehatan ' . $prov['NAMA_KAB'];
        } elseif ($faskes_kode != 'A' && $no_kab != 'A' && $no_prov != 'A') {

            $puskesmas = $this->orm->ref_puskesmas->where('Kode_puskesmas', $faskes_kode);
            if (count($puskesmas) != 0) {
                $faskes = $puskesmas->fetch();
                $user = 'Puskesmas ' . $faskes['Nama_pus_kab'];
            }

            $rumahsakit = $this->orm->ref_rumahsakit->where('kode_rs', $faskes_kode);
            if (count($rumahsakit) != 0) {
                $faskes = $rumahsakit->fetch();
                $user = $faskes['Nama_rs'];
            }

            $kkp = $this->orm->ref_kkp->where('faskes_kode', $faskes_kode);
            if (count($kkp) != 0) {
                $faskes = $kkp->fetch();
                $user = $faskes['faskes_nama'];
            }

            $bapelkes = $this->orm->ref_bapelkes->where('faskes_kode', $faskes_kode);
            if (count($bapelkes) != 0) {
                $faskes = $bapelkes->fetch();
                $user = $faskes['faskes_nama'];
            }

            $btkl = $this->orm->ref_btkl->where('faskes_kode', $faskes_kode);
            if (count($btkl) != 0) {
                $faskes = $btkl->fetch();
                $user = $faskes['faskes_nama'];
            }
            
            $bpkm = $this->orm->ref_bpkm->where('faskes_kode', $faskes_kode);
            if (count($bpkm) != 0) {
                $faskes = $bpkm->fetch();
                $user = $faskes['faskes_nama'];
            }

            $klinik = $this->orm->ref_klinik->where('faskes_kode', $faskes_kode);
            if (count($klinik) != 0) {
                $faskes = $klinik->fetch();
                $user = $faskes['faskes_nama'];
            }

            $lainnya = $this->orm->ref_faskeslain->where('faskes_kode', $faskes_kode);
            if (count($lainnya) != 0) {
                $faskes = $lainnya->fetch();
                $user = $faskes['faskes_nama'];
            }
            
            $labkes = $this->orm->ref_labkes->where('faskes_kode', $faskes_kode);
            if (count($labkes) != 0) {
                $faskes = $labkes->fetch();
                $user = $faskes['faskes_nama'];
            }
            
            
            $upt = $this->orm->ref_upt->where('faskes_kode', $faskes_kode);
            if (count($upt) != 0) {
                $faskes = $upt->fetch();
                $user = $faskes['faskes_nama'];
            }
            
            $uptkab = $this->orm->ref_uptdkab->where('faskes_kode', $faskes_kode);
            if (count($uptkab) != 0) {
                $faskes = $uptkab->fetch();
                $user = $faskes['faskes_nama'];
            }
            
            $uptprov = $this->orm->ref_uptdprov->where('faskes_kode', $faskes_kode);
            if (count($uptprov) != 0) {
                $faskes = $uptprov->fetch();
                $user = $faskes['faskes_nama'];
            }
        }

        return $user;
    }

    public function _checkJenisFaskes($username) {

        $data = explode('--', $username);
        if (COUNT($data) == 2) {
            $kode = $this->orm->ref_jenis_faskes->where('ref_jenis_faskes_kode', $data[1])->fetch();
            $ref_jenis_faskes_kode = $kode['ref_jenis_faskes_kode'];  // Selain Puskesmas, Rumah Sakit, Dinkes Kab, Dinkes Prov, Pusat
        } else {
            $puskesmas = $this->orm->ref_puskesmas->where('Kode_puskesmas', $username);
            if (count($puskesmas) != 0) {
                $ref_jenis_faskes_kode = 1;  //Puskesmas              
            }

            $rumahsakit = $this->orm->ref_rumahsakit->where('kode_rs', $username);
            if (count($rumahsakit) != 0) {
                $ref_jenis_faskes_kode = 2;  //Rumah Sakit              
            }

            $tbl_akun = $this->orm->tbl_akun->where('nama_akun', $username)->fetch();
            if ($tbl_akun['faskes_kode'] == 'A' && $tbl_akun['NO_KAB'] == 'A' && $tbl_akun['NO_PROV'] == 'A') {
                 $ref_jenis_faskes_kode = 0;  //Pusat     
            } elseif ($tbl_akun['faskes_kode'] == 'A' && $tbl_akun['NO_KAB'] == 'A' && $tbl_akun['NO_PROV'] != 'A') {
                $ref_jenis_faskes_kode = 7;  //Dinkes Prov
            } elseif ($tbl_akun['faskes_kode'] == 'A' && $tbl_akun['NO_KAB'] != 'A' && $tbl_akun['NO_PROV'] != 'A') {
                $ref_jenis_faskes_kode = 6;  //Dinkes Kab
            }
        }

        return (!isset($ref_jenis_faskes_kode))?'NULL':$ref_jenis_faskes_kode;
    }

    public function _getKodeLevelUser($faskes_kode, $no_kab, $no_prov) {

        $kode = '';

        if ($faskes_kode == 'A' && $no_kab == 'A' && $no_prov == 'A') {
            $kode = 1;  // Pusat
        } elseif ($faskes_kode == 'A' && $no_kab == 'A' && $no_prov != 'A') {
            $kode = 2; // Dinkes Provinsi
        } elseif ($faskes_kode == 'A' && $no_kab != 'A' && $no_prov != 'A') {
            $kode = 3; // Dinkes Kab/Kota
        } elseif ($faskes_kode != 'A' && $no_kab != 'A' && $no_prov != 'A') {
            $puskesmas = $this->orm->ref_puskesmas->where('Kode_puskesmas', $faskes_kode);
            if (count($puskesmas) != 0) {
                $kode = 41;          // Puskesmas  
            }

            $rumahsakit = $this->orm->ref_rumahsakit->where('kode_rs', $faskes_kode);
            if (count($rumahsakit) != 0) {
                $kode = 42;          // Rumah Sakit     
            }

             $klinik = $this->orm->ref_klinik->where('faskes_kode', $faskes_kode);
            if (count($klinik) != 0) {
                $kode = 49;          //    
            }

            
             $labkes = $this->orm->ref_labkes->where('faskes_kode', $faskes_kode);
            if (count($labkes) != 0) {
                $kode = 410;          //   
            }
 
             $bpkm = $this->orm->ref_bpkm->where('faskes_kode', $faskes_kode);
            if (count($bpkm) != 0) {
                $kode = 48;          //   
            }
 
             $bptklpp = $this->orm->ref_bptklpp->where('faskes_kode', $faskes_kode);
            if (count($bptklpp) != 0) {
                $kode = 43;          //   
            }

            
             $kkp = $this->orm->ref_kkp->where('faskes_kode', $faskes_kode);
            if (count($kkp) != 0) {
                $kode = 44;          //   
            }

            
             $upt = $this->orm->ref_upt->where('faskes_kode', $faskes_kode);
            if (count($upt) != 0) {
                $kode = 498;          //   
            }
           
             $uptdprov = $this->orm->ref_uptdprov->where('faskes_kode', $faskes_kode);
            if (count($uptdprov) != 0) {
                $kode = 497;          //   
            }
         
             $uptdkab = $this->orm->ref_uptdkab->where('faskes_kode', $faskes_kode);
            if (count($uptdkab) != 0) {
                $kode = 496;          //   
            }

            
            
            
            
        }

        return $kode;
    }

}
