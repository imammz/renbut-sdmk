var nama = [];
var nama_provinsi = [];
var alamat = [];
var keterangan = [];
var total = [];
var lokasi = [];
var cords = '';
var id = [];
var infoWindow;
var loading ='<div align="center"><img class="img-circle" src="../assets/images/ajax-loader.gif"></div>';
function peta_awal(){
    var indonesia = new google.maps.LatLng(-3.5451241, 112.7293194);
    var petaoption = {
        zoom: 5,
        center: indonesia,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    peta = new google.maps.Map(document.getElementById("map-canvas"),petaoption);
	
    url = "get_data.php";
    $.ajax({
        url: url,
        dataType: 'json',
        cache: false,
        success: function(msg){
            var polygon;
            var cords = [];
            for(i=0;i<msg.indonesia.lahan.length;i++){
                id[i] = msg.indonesia.lahan[i].id;
                nama_provinsi[i] = msg.indonesia.lahan[i].nama_provinsi;
                total[i] = msg.indonesia.lahan[i].total;
                keterangan[i] = msg.indonesia.lahan[i].keterangan;
                lokasi[i] = msg.indonesia.lahan[i].polygon;
               
                var str = lokasi[i].split(" "); 
                
                for (var j=0; j < str.length; j++) { 
                    var point = str[j].split(",");
                    cords.push(new google.maps.LatLng(parseFloat(point[0]), parseFloat(point[1])));
                }

               var contentString = '<b>'+nama_provinsi[i]+'</b><br>' +
                                    'Keterangan: '+ keterangan[i] +
                                    '<br>' +
                                    'Total : '+ total[i] +
                                    '<br>';

                polygon = new google.maps.Polygon({
                    paths: [cords],
                    strokeColor: msg.indonesia.lahan[i].warna,
                    strokeOpacity: 0,
                    strokeWeight: 1,
                    fillColor: msg.indonesia.lahan[i].warna,
                    fillOpacity: 0.5,
                    html: contentString,
					id  :id[i],
					 nama_provinsi: nama_provinsi[i]
                });     

                cords = []; 
                polygon.setMap(peta); 
				
				var lat = parseFloat(msg.indonesia.lahan[i].lat);
					console.log(cords);
					var lng  =parseFloat(msg.indonesia.lahan[i].lng);
					var myLatLng = {lat:lat, lng:lng};
					
                infoWindow = new google.maps.InfoWindow();
                google.maps.event.addListener(polygon, 'click', function(event) {
					$('#grafik').html(loading);
					$('#grafik2').html(loading);
					$.post("grafik.php",{id:this.id,name:this.nama_provinsi},function(data){
						$('#grafik').html(data);
					});
					$.post("grafik_target.php",{id:this.id,name:this.nama_provinsi},function(data){
							$('#grafik2').html(data);
						});
                    infoWindow.setContent(this.html);
                    infoWindow.setPosition(event.latLng);
                    infoWindow.open(peta);
                });
            }               
        }
    });
	
	$('#grafik').html(loading);
	$.post("grafik.php",{id:$('#id_provinsi').find('option:selected').val(),name:$('#id_provinsi').find('option:selected').text()},function(data){
		$('#grafik').html(data);
	});
	$('#grafik2').html(loading);
	$.post("grafik_target.php",{id:$('#id_provinsi').find('option:selected').val(),name:$('#id_provinsi').find('option:selected').text()},function(data){
		$('#grafik2').html(data);
	});
}

$(document).ready(function(){
    $("#search").click(function(){
        var id = $('#id_provinsi').find('option:selected').val();

        url = "search_data.php";
		$.ajax({
			url: url,
			data: "id="+id,
			dataType: 'json',
			cache: false,
			success: function(msg){
				
				var indonesia2 = new google.maps.LatLng(-2.05274,119.73395);
                var petaoption2 = {
                    zoom: 5,
                    center: indonesia2,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var peta2 = new google.maps.Map(document.getElementById("map-canvas"),petaoption2);

				var polygon;
				var cords = [];
				for(i=0;i<msg.indonesia.lahan.length;i++){
					id[i] = msg.indonesia.lahan[i].id;
					nama_provinsi[i] = msg.indonesia.lahan[i].nama_provinsi;
					total[i] = msg.indonesia.lahan[i].total;
					keterangan[i] = msg.indonesia.lahan[i].keterangan;
					lokasi[i] = msg.indonesia.lahan[i].polygon;
				   
					var str = lokasi[i].split(" "); 
					
					for (var j=0; j < str.length; j++) { 
						var point = str[j].split(",");
						cords.push(new google.maps.LatLng(parseFloat(point[0]), parseFloat(point[1])));
					}

				   var contentString = '<b>'+nama_provinsi[i]+'</b><br>' +
										'Keterangan: '+ keterangan[i] +
										'<br>' +
										'Total : '+ total[i] +
										'<br>';

					polygon = new google.maps.Polygon({
						paths: [cords],
						strokeColor: msg.indonesia.lahan[i].warna,
						strokeOpacity: 0,
						strokeWeight: 1,
						fillColor: msg.indonesia.lahan[i].warna,
						fillOpacity: 0.5,
						html: contentString,
						id  :id[i],
						 nama_provinsi: nama_provinsi[i]
					});     

					cords = []; 
					polygon.setMap(peta2); 
					
					var lat = parseFloat(msg.indonesia.lahan[i].lat);
						console.log(cords);
						var lng  =parseFloat(msg.indonesia.lahan[i].lng);
						var myLatLng = {lat:lat, lng:lng};
						
					infoWindow = new google.maps.InfoWindow();
					google.maps.event.addListener(polygon, 'click', function(event) {
						$('#grafik').html(loading);
						$('#grafik2').html(loading);
						$.post("grafik.php",{id:this.id,name:this.nama_provinsi},function(data){
							$('#grafik').html(data);
						});
						$.post("grafik_target.php",{id:this.id,name:this.nama_provinsi},function(data){
							$('#grafik2').html(data);
						});
						infoWindow.setContent(this.html);
						infoWindow.setPosition(event.latLng);
						infoWindow.open(peta2);
					});
				}               
			}
		});
		
		$('#grafik').html(loading);
		$.post("grafik.php",{id:$('#id_provinsi').find('option:selected').val(),name:$('#id_provinsi').find('option:selected').text()},function(data){
			$('#grafik').html(data);
		});
		
		$('#grafik2').html(loading);
		$.post("grafik_target.php",{id:$('#id_provinsi').find('option:selected').val(),name:$('#id_provinsi').find('option:selected').text()},function(data){
			$('#grafik2').html(data);
		});
	
    });
	
	
});