<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<div class="modal-header">
    <button type="button" class="close" onclick="close_box()">×</button>
    <h2><?php echo $this->lang['forgot_password'] ?></h2>
</div>
<div class="modal-body">
    <form id="forgot-pass-user" onsubmit="return false; get_reset();">
        <?php
        echo isset($message) ? '<div>&nbsp;</div><p class="xamzo-error">' . $message . '</p>' : '';
        ?>
        <input type="hidden" name="emitter" value="1"> 
        <p>
            <label class="control-label">Masukan alamat email personal atau kantor yang terdaftar</label>
            <input type="text" name="email" validation="required|email"> 
        </p>
        <p class="modal-footer"> 
            <a href="javascript:void(0)" class="btn primary right medium" id="riset_password" onclick="get_reset()">&nbsp;&nbsp;Kirim&nbsp;&nbsp;</a> 
        </p>
    </form> 
</div>

<script type="text/javascript">
        function get_reset() {
            $.getScript(base_ + 'application/templates/private/assets/javascript/libraries/xamzo/validation.js', function() {
                $('form#forgot-pass-user').validation();
                if (!$('form#forgot-pass-user').validate()) {

                } else {
                    $.ajax({
                        type: 'post',
                        beforeSend: function() {
                            loading();
                        },
                        data: $('form#forgot-pass-user').serialize(),
                        url: '<?php echo site_url('user/forgot') ?>',
                        success: function(rs) {
                            return_modalbox(rs, 'true', '400px');
                        }
                    });
                }
            });
            return false;
        }
</script>