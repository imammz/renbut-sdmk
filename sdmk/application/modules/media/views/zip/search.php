<?php if (!defined('BASEPATH')) {exit('no direct script user allowed');} ?>
<script type="text/javascript" src="<?php echo site_url('application/modules/media/assets/javascript/script.js') ?>"></script>

<div class="xamzo-block" id="xamzo-media">
    <div class="xamzo-block-content">
        <div class="content">
            <form id="crud-data">  
                <ul class="list_zip xamzo-box-media" id="zip">
                    <li style="display: none">
                        <input type="checkbox" table-check-all="true">
                    </li>
                    <?php
                    $id = 0;
                    foreach ($files as $file) :
                        $f = explode('{x}', $file);
                        $filename = substr($f['1'], 0, 9) . '..';
                        echo "<li id='" . $id . "'>
                                <span class='icon_zip xamzo-media tip-bottom-left' original-title='$f[1]' dir='" . site_url('upload/z/' . $file) . "'>$filename</span>
                            <div class='box-image-act'>
                                <div class='left'>
                                    <input table-check-all='true' type='checkbox' onclick='_self_check(this);' name='id[]' value='" . site_url('upload/z/' . $file) . "' id='" . site_url('upload/z/' . $file) . "'> <label for='" . site_url('upload/z/' . $file) . "'></label>
                                </div>
                                <div class='right'>
                                        <a href='javascript:void(0)' onclick='file_url($(this).parent().parent().parent());' class='tip-top' original-title='" . $this->lang["link"] . "'><i class='fa fa-link'></i></a>
                                        <a href='javascript:void(0)' onclick='file_property($(this).parent());' class='tip-top no-phone' original-title='" . $this->lang["properties"] . "'><i class='fa fa-search'></i></a>
                                        <a href='javascript:void(0)' onclick='file_delete($(this).parent().parent().parent())' class='tip-top' original-title='" . $this->lang["delete"] . "'><i class='fa fa-trash'></i></a> 
                                </div>
                            </div>
                          </li>";
                        $id++;
                    endforeach; 
                    ?>
                </ul> 
            </form>
        </div>
    </div>
</div>
<div class="xamzo-block" id="xamzo-property"></div>