<?php echo Modules::run('templates/cliptwo/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >

                <div class="container-fluid container-fullw bg-white">
                    <div class="row">
                        <div class="col-md-8">
                            &nbsp;
                        </div>
                        <ol class="breadcrumb">                           
                            <li class="active">
                                <span>Refrensi Kegiatan Beban Kerja</span>
                            </li>
                        </ol>
                    </div>

                    <!-- start: YOUR CONTENT HERE -->                        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">                                            
                                <div class="panel-body" style="min-height: 500px;">

                                    <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/PMK No. 73 th 2013 ttg Jabatan Fungsional Umum di Kementerian Kesehatan.pdf') ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong>PMK No. 73 th 2013 ttg Jabfung Umum di Kementerian Kesehatan</strong>
                                        </a>
                                    </div>
                                    
                                    <?php 
                                    $refrensi = $this->orm->ref_refrensiabk();
                                   
                                    foreach($refrensi as $row) {
                                    ?>
                                    
                                     <div class="col-sm-6">
                                        <a href="<?php echo base_url('uploads/'.$row['judul'].'.'.$row['ext']) ?>" target="_blank" class="btn btn-icon margin-bottom-5 margin-bottom-5 btn-block">
                                            <i class="ti-layers-alt block text-primary text-extra-large margin-bottom-10"></i>
                                            <strong><?php echo $row['judul']; ?></strong>
                                        </a>
                                    </div>
                                    
                                    <?php } ?>
                                    
                                    
                                    

                                </div>
                            </div>

                        </div>



                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
            FormElements.init();
        });

        function faskes_cetak() {
            //  alert($("#faskes_kode").val());
            var faskes_kode = $("#faskes_kode").val();
            var faskes_tahun = $("#faskes_tahun").val();

            if (faskes_kode === '') {
                alert('Pilih Terlebih Dahulu Faskes Puskesmas');
            }
            else {
                window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/faskes_excel/" + faskes_kode + '/' + faskes_tahun;
            }
        }
        function faskes_proses() {
            //  alert($("#faskes_kode").val());
            var faskes_kode = $("#faskes_kode").val();
            var faskes_tahun = $("#faskes_tahun").val();
            if (faskes_kode === '') {
                alert('Pilih Terlebih Dahulu Faskes Puskesmas');
            }
            else {
                GB_show("Laporan Kebutuhan SDMK Faskes ", '<?php echo base_url() ?>index.php/laporan/puskesmas/faskes_proses/' + faskes_kode + '/' + faskes_tahun, 700, 1100);
                $('html, body').animate({scrollTop: 0}, 'slow');
            }


        }

        function kab_cetak() {
            //  alert($("#faskes_kode").val());
            var NO_KAB = $("#NO_KAB").val();
            var kab_tahun = $("#kab_tahun").val();
            if (NO_KAB === '') {
                alert('Pilih Terlebih Dahulu Kabupaten/Kota');
            }
            else {
                window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/kab_excel/" + NO_KAB + '/' + kab_tahun;
            }
        }
        function kab_proses() {
            //  alert($("#faskes_kode").val());
            var NO_KAB = $("#NO_KAB").val();
            var kab_tahun = $("#kab_tahun").val();
            if (NO_KAB === '') {
                alert('Pilih Terlebih Dahulu Kabupaten/Kota');
            }
            else {
                GB_show("Laporan Kebutuhan SDMK Kabupaten ", '<?php echo base_url() ?>index.php/laporan/puskesmas/kab_proses/' + NO_KAB + '/' + kab_tahun, 700, 1100);
                $('html, body').animate({scrollTop: 0}, 'slow');
            }


        }

        function prov_cetak() {
            //  alert($("#faskes_kode").val());
            var NO_PROV = $("#NO_PROV").val();
            var prov_tahun = $("#prov_tahun").val();
            if (NO_PROV === '') {
                alert('Pilih Terlebih Dahulu Provinsi');
            }
            else {
                window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/prov_excel/" + NO_PROV + '/' + prov_tahun;
            }
        }
        function prov_proses() {
            //  alert($("#faskes_kode").val());
            var NO_PROV = $("#NO_PROV").val();
            var prov_tahun = $("#prov_tahun").val();
            if (NO_PROV === '') {
                alert('Pilih Terlebih Dahulu Provinsi');
            }
            else {
                GB_show("Laporan Kebutuhan SDMK Provinsi ", '<?php echo base_url() ?>index.php/laporan/puskesmas/prov_proses/' + NO_PROV + '/' + prov_tahun, 700, 1100);
                $('html, body').animate({scrollTop: 0}, 'slow');
            }


        }

        function nas_cetak() {
            //  alert($("#faskes_kode").val());
            var nas_tahun = $("#nas_tahun").val();
            window.location.href = "<?php echo base_url() ?>index.php/laporan/puskesmas/nas_excel/" + nas_tahun;
        }
        function nas_proses() {
            //  alert($("#faskes_kode").val());
            var nas_tahun = $("#nas_tahun").val();

            GB_show("Laporan Kebutuhan SDMK Nasional ", '<?php echo base_url() ?>index.php/laporan/puskesmas/nas_proses/' + nas_tahun, 700, 1100);
            $('html, body').animate({scrollTop: 0}, 'slow');



        }

    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
