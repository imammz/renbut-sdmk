<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Combobox extends MX_Controller {

    function __construct() {
        parent::__construct();

    }

    public function loadJft_2() {

        $sdmk_post = explode('#',$this->input->post('ref_jenis_sdmk_kode'));
        $ref_jenis_sdmk_kode = $sdmk_post[0];
        
        $this->db->select('a.ref_jenis_sdmk_jft_id,b.jft');
        $this->db->from('ref_jenis_sdmk_jft_detail a');
        $this->db->join('ref_jenis_sdmk_jft b','a.ref_jenis_sdmk_jft_id = b.ref_jenis_sdmk_jft_id');
        $this->db->where('ref_jenis_sdmk_kode', $ref_jenis_sdmk_kode);
        $this->db->order_by('ref_jenis_sdmk_jft_id asc');

        $data = array();
        $data['result'] = TRUE;
        $data['data_jft'] = $this->db->get()->result_array();

        echo json_encode($data);
    }
    
    public function loadJft() {

        $sdmk_post = explode('#',$this->input->post('ref_jenis_sdmk_kode'));
        $ref_jenis_sdmk_kode = $sdmk_post[0];
        
        $this->db->select('*');
        $this->db->from('ref_jenis_sdmk_jft');
        $this->db->order_by('ref_jenis_sdmk_jft_id asc');

        $data = array();
        $data['result'] = TRUE;
        $data['data_jft'] = $this->db->get()->result_array();

        echo json_encode($data);
    }

}
