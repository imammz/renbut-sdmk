<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo WEB_TITLE.' '.COPYRIGHT ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>assets/login/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>assets/login/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>assets/login/css/AdminLTE.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="<?php echo base_url() ?>assets/images/icon.png">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
              html{
            background: url('<?php echo base_url() ?>assets/login/img/blur-background08.jpg') 100%;
        }
        .content-login{
            padding:20px;
            box-shadow: 0px 1px 2px 1px #888;
            background: #f0f0f0;
            margin:10px auto;
        }
        .logo{
            width: 150px;
            height: 150px;
            background: #f0f0f0;
            margin:auto;
            border-radius:50%;
            padding: 10px;
            box-shadow: 0px 1px 2px 1px #888;
        }
        .form{
            width:400px;
            padding:20px;
            position: fixed;

        }
        </style>
    </head>
    <body>
        <div class="form">
            <div class="text-center"><h2>LOGIN PENGGUNA</h2></div>        
            <div class="text-center"><img src="https://4.bp.blogspot.com/-3g_4R2tFhx8/WD5JNjOg--I/AAAAAAAAbx8/nvmsyQb7Ka0dILIl24011IPrlQvyybT8gCLcB/s320/Logo%2BBaru%2BKementerian%2BKesehatan.png" class="logo-img"> 
            <div class="text-center"><h3> Applikasi Perencanaan Kebutuhan Tenaga Kesehatan (Metode ABK)</h3></div>
            <div class="text-center text-red"><h4> <?php echo VERSI ?> </h4></div>
             <?php 
                    if(isset($_SESSION['info'])){
                       echo '<div class="alert alert-danger alert-dismissable" style="margin-left:0px">
                         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                               username atau password anda salah                    </div>';
                        if(isset($_SESSION['info'])){
                            unset($_SESSION['info']);
                        }
                    }
                ?>
            <form action="<?php echo base_url() ?>index.php/config/login/proses" method="post" class="content-login">
                <div class="form-group">
                   <input type="text" name="nama_pengguna" class="form-control" required placeholder="nama pengguna"/>
                </div>
                <div class="form-group">
                   <input type="password" name="password" class="form-control" required placeholder="password"/>
                </div>          
                <div class="form-group">
                    <button type="submit" name="login" class="btn btn-info form-control">Masuk</button>  
                </div> 
            </form>
            <div class="text-center">
</div>
            </div>
        </div>

        <!-- jQuery 2.0.2 -->
         <script type="text/javascript" src="<?php echo base_url() ?>assets/login/js/jquery-2.1.1.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>assets/login/js/bootstrap.min.js" type="text/javascript"></script>   
        <script src="<?php echo base_url() ?>assets/login/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">

            $('.form').position({
                    my: "center",
                    at: "center",
                    of: window,
                    collision: "fit",
                });
       
       </script>     

    </body>
</html>
