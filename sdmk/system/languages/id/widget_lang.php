<?php if (!defined('BASEPATH')) exit('no direct script user allowed');

$lang['top'] = 'atas';
$lang['right'] = 'kanan';
$lang['bottom'] = 'bawah';
$lang['left'] = 'kiri';
$lang['position'] = 'posisi';
$lang['drag_here'] = 'tarik disini';
$lang['html_script']='kode html';
$lang['is_removed_from_list']='dihapus dari daftar';
$lang['get_more_widgets'] = 'cari widget lain';
$lang['widget_is_up_to_date'] = 'widget terupdate';
$lang['this_page_to_manage_widget,_see_documentation_for_use_it'] = 'Halaman ini untuk mengelola widget, lihat dokumentasi untuk menggunakannya';