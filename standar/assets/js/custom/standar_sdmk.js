    var save_method; //for save method string
    var table;
    var tablersu;
      $(document).ready(function() {
        var type = $('#dynamic-table').attr('rel');
        table = $('#dynamic-table').DataTable({ 
          
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "paging": false, //Feature control DataTables' server-side processing mode.
          "bInfo": false, //Feature control DataTables' server-side processing mode.
          
          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "master_data/m_standar_sdmk/find_content/"+type,
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
          },
          ],

        });


      });

      // greating //
      function greatSuccess() {

        $.gritter.add({
               title: 'SUKSES!',
               text: 'Data berhasil diproses.',
               sticky: false,
               time: '1000',
               class_name: 'gritter-success'
        })

      }

    function greatError() {
        $.gritter.add({
             title: 'PERINGATAN!',
             text: 'Anda tidak memiliki hak akses atau terjadi kesalahan dalam proses.',
             sticky: false,
             time: '1000',
             class_name: 'gritter-error'
        })

    }
    function edit(id)
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('master_data/m_standar_sdmk/form/' + id + '?_=' + (new Date()).getTime());

    }

    function backlist()
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('master_data/m_standar_sdmk'+ '?_=' + (new Date()).getTime());

    }

    function add()
    {
      $('#page-area-content').empty();
      $('#page-area-content').load('master_data/m_standar_sdmk/form/'+ '?_=' + (new Date()).getTime());

    }

    function tab_rsu()
    {
      $('#content_tab_jenis_faskes').empty();
      $('#content_tab_jenis_faskes').load('master_data/m_standar_sdmk/tab_rsu/'+ '?_=' + (new Date()).getTime());
    }

    function tab_rsk()
    {
      $('#content_tab_jenis_faskes').empty();
      $('#content_tab_jenis_faskes').load('master_data/m_standar_sdmk/tab_rsk/'+ '?_=' + (new Date()).getTime());
    }

    function tab_kkp()
    {
      $('#content_tab_jenis_faskes').empty();
      $('#content_tab_jenis_faskes').load('master_data/m_standar_sdmk/tab_kkp/'+ '?_=' + (new Date()).getTime());
    }

    function tab_btklpp()
    {
      $('#content_tab_jenis_faskes').empty();
      $('#content_tab_jenis_faskes').load('master_data/m_standar_sdmk/tab_btkl/'+ '?_=' + (new Date()).getTime());
    }

    function tab_klinik()
    {
      $('#content_tab_jenis_faskes').empty();
      $('#content_tab_jenis_faskes').load('master_data/m_standar_sdmk/tab_klinik/'+ '?_=' + (new Date()).getTime());
    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_jenis_sdmk(id)
    {

      if(confirm('Apakah anda yakin akan menghapus data ini?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "master_data/m_standar_sdmk/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               greatSuccess();
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                greatError();
            }
        });
         
      }

    }


$.validator.setDefaults({
  submitHandler: function() { 
    
     url = "master_data/m_standar_sdmk/ajax_search_by_params";

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_standar_sdmk').serialize(),
            dataType: "JSON",
            success: function(data)
            { 

              greatSuccess();
              reload_table();
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

              greatError();

            }
        });

  }

});

function disabledBtn()
  {
    $('#btnSave').disabled = true;
    return true;
  }


// jquery validation //
$().ready(function() {

  // validate signup form on keyup and submit
  $("#form_standar_sdmk").validate({
    rules: {
      
      nama_jenis_sdmk: "required",
      counter: {
        required: true,
        number: true
      },
      active: "required"
    },

    messages: {
      nama_jenis_sdmk: "Masukan nama jenis SDMK!",
      counter: {
        required: "Masukan counter!",
        number: "Counter harus diisi dengan angka!"
      },
    }
  });


});

    
function update_row(id_standar){

    value=document.getElementById(id_standar).value;
    
    var link_url = 'master_data/m_standar_sdmk/update_row/'+id_standar;
    $.ajax({
       type: "POST",
       dataType: "json",
       url: link_url,
       data: "value="+value,
       success: function(msg){

        greatSuccess();
        reload_table();
                                               
       }
    }); 

}

function update_row_with_class(id_standar){

    value=document.getElementById(id_standar).value;
    
    var link_url = 'master_data/m_standar_sdmk/update_row_with_class/'+id_standar;
    $.ajax({
       type: "POST",
       dataType: "json",
       url: link_url,
       data: "value="+value,
       success: function(msg){

        greatSuccess();
        reload_table();
                                               
       }
    }); 

}
