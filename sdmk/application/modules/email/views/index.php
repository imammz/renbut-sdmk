<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?> 
<title> Pengaturan - Email</title>
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-wrench"></span> &nbsp;<?php echo $this->lang['setting'] ?>
            <sup class="sup-menu">
                <i class="fa fa-envelope"></i> <?php echo $this->lang['email']; ?>
            </sup>
        </h3>
        <span class="xamzo-head-info"><?php echo $this->lang['this_page_to_manage_website_setting,_see_documentation_for_use_it'] ?>.</span>
    </div>
    <div class="right">
    </div>
</div>
<div id="xamzo-head-action">
    <button class="btn primary large fa fa-angle-down tip-top" dropdown="true" dropdown-relative="left" dropdown-target="post-add"  original-title="<?php echo $this->lang['option']; ?>"><span class="no-phone "><?php echo $this->lang['setting']; ?></span></button>
    <button class="btn primary large fa fa-star tip-top" href="#!email/template" ajax="true" url-push="true" ajax-target=".xamzo-right" original-title="<?php echo $this->lang['template']; ?>"><span class="no-phone "><?php echo $this->lang['template']; ?></span></button>
    <button ajax="true" url-push="true" href="#!email" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top-right" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div>
<ul class="dropdown-target dropdown-target-left post-add">
    <div class="arrow-up"></div>
    <li>
        <a ajax="true" url-push="true" href="#!setting/index" ajax-target=".xamzo-right"><i class="fa fa-info-circle"></i> <?php echo $this->lang['info']; ?></a>
    </li>   
    <li>
        <a ajax="true" url-push="true" href="#!setting/general" ajax-target=".xamzo-right"><i class="fa fa-cog"></i> <?php echo $this->lang['general']; ?></a>
    </li>    
    <li>    
        <a ajax="true" url-push="true" href="#!setting/media" ajax-target=".xamzo-right"><i class="fa fa-folder-open"></i> <?php echo $this->lang['media']; ?></a>
    </li>   
    <li>   
        <a ajax="true" url-push="true" href="#!setting/database" ajax-target=".xamzo-right"><i class="fa fa-hdd-o"></i> <?php echo $this->lang['database']; ?></a>
    </li>    
    <li>    
        <a ajax="true" url-push="true" href="#!setting/security" ajax-target=".xamzo-right"><i class="fa fa-lock"></i> <?php echo $this->lang['security']; ?></a>
    </li> 
</ul>
<div id="crud-target" class="xamzo-setting"> 
    <div class="xamzo-block small no-tablet">
        <div class="xamzo-block-content">
            <div class="xamzo-block-header">
                <h4><?php echo $this->lang['email']; ?> <?php echo $this->lang['setting']; ?></h4>
            </div>
            <div class="content">
                <input type="hidden" name="selected" value="<?php echo $option ?>">
                <form id="xamzo-email-conf" onsubmit="return false;">
                    <?php
                    $offset=0;
                    foreach ($method as $key => $val) {
                        $checked = '';
                        if ($key == $option) {
                            $checked = 'checked="true"';
                        }
                        ?>
                        <p>
                           <label class="control-label medium"></label>
                           <input class="no-phone" type="radio" onclick="_set_mail(this)" name="option" <?php echo $checked ?> value="<?php echo $key ?>" id="<?php echo $key ?><?php echo $offset ?>"> <label for="<?php echo $key ?><?php echo $offset ?>"><?php echo $key ?></label>
                        </p>
                        <?php
                        $offset++;
                    }
                    ?>
                    <p style="border-bottom: 1px solid #ccc;"></p>
                    <p>
                        <label class="control-label medium"><b><?php echo $this->lang['email'] ?></b></label>
                        <input type="text" name="email_sender" value="<?php echo $email_sender ?>">
                    </p>
                    <p>
                        <label class="control-label medium"><b><?php echo $this->lang['name'] ?></b></label>
                        <input type="text"  name="name_sender" value="<?php echo $name_sender ?>">
                    </p>
                    <p>
                        <label class="control-label medium">&nbsp;</label>
                        <button class="btn primary medium" ajax="true" ajax-type="POST" href="<?php echo site_url('email/update') ?>" ajax-target=".xamzo-right" ajax-redirect="<?php echo site_url('email'); ?>" ajax-data="form#xamzo-email-conf"><?php echo $this->lang['submit'] ?></button>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <div class="xamzo-block separated"></div>
    <div class="xamzo-block big">
        <div class="xamzo-block-content" >
            <div id="xamzo-setting-config">
                <p><?php echo $this->lang['no_email_configuration'] ?></p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
            _set_mail($('input[name="selected"]'));
            function _set_mail(i) {
                $.ajax({
                    type: 'POST',
                    data: 'option=' + $(i).val(),
                    url: base_ + 'email/option',
                    success: function(rs) {
                        $('#xamzo-setting-config').html(rs);
                    }
                });
            }
            _set_mail_($('input[name="selected"]'));
            function _set_mail_(i) {
                $.ajax({
                    type: 'POST',
                    data: 'option=' + $(i).val(),
                    url: base_ + 'email/option',
                    success: function(rs) {
                        $('#xamzo-setting-config').html(rs);
                    }
                });
            }
</script>