<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ex_master extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('ex_master_model','ex_master');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Ex Master";
		$data['subtitle'] = "Daftar Ex Master";
		$this->load->view('index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');

		$data['title'] = "Form Ex Master";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->ex_master->get_by_id($id);
		}

		$this->load->view('form', $data);

	}

	public function ajax_list()
	{
		$list = $this->ex_master->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $ex_master) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($ex_master->ex_master_name);
			$row[] = ($ex_master->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $ex_master->updated_date?Tanggal::formatDateTime($ex_master->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($ex_master->ex_master_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_ex_master('."'".Regex::_genRegex($ex_master->ex_master_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->ex_master->count_all(),
						"recordsFiltered" => $this->ex_master->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$ex_master_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'ex_master_name' => Regex::_genRegex($this->input->post('ex_master_name'), 'RGQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => '',
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $ex_master_id == 0 ){
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->ex_master->save($dataexc);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->ex_master->update(array('ex_master_id'=>$ex_master_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->ex_master->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
