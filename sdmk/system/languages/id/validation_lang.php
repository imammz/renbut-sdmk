<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang["this_field_is_required"] = "kolom inputan tidak boleh kosong.";
$lang["too_long_(%s_characters_max)"] = "inputan terlalu panjang (maksimal %s karakter).";
$lang["too_short_(%s_characters_min)"] = "inputan terlalu pendek (minimall %s karakter).";
$lang["value_must_be_numeric"] ="inputan harus berupa numerik.";
$lang["value_must_be_an_integer"] = "inputan harus berupa integer atau angka.";
$lang["value_must_be_at_most %s"] = "inputan harus berupa lebih dari %s";
$lang["value_must_be_at_least %s"] = "inputan harus berupa setidaknya harus %s";
$lang["value_must_be_a_valid_URL"] = "inputan harus berupa URL yang benar.";
$lang["value_must_be_a_valid_email"] = "inputan harus berupa email yang benar.";
$lang["invalid_value"] = "Nilai tidak valid.";