    var save_method; //for save method string
    var table;
      $(document).ready(function() {
        table = $('#dynamic-table').DataTable({ 
          
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          
          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "rekapitulasi/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
          },
          ],

        });
      });

      // greating //
      function greatSuccess() {

        $.gritter.add({
               title: 'SUKSES!',
               text: 'Data berhasil diproses.',
               sticky: false,
               time: '1000',
               class_name: 'gritter-success'
        })

      }

    function greatError() {
        $.gritter.add({
             title: 'PERINGATAN!',
             text: 'Anda tidak memiliki hak akses atau terjadi kesalahan dalam proses.',
             sticky: false,
             time: '1000',
             class_name: 'gritter-error'
        })

    }
    function view(id)
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('rekapitulasi/rekap_puskesmas/view/' + id + '?_=' + (new Date()).getTime());

    }
    function edit(id)
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('rekapitulasi/form/' + id + '?_=' + (new Date()).getTime());

    }

    function backlist()
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('rekapitulasi'+ '?_=' + (new Date()).getTime());

    }

    function add()
    {
      $('#page-area-content').empty();
      $('#page-area-content').load('rekapitulasi/form/'+ '?_=' + (new Date()).getTime());

    }

    function form_import()
    {
      $('#form_import').empty();
      $('#form_import').load('rekapitulasi/form_import/'+ '?_=' + (new Date()).getTime());

    }

    function view_rekap_detail(tahun, id_provinsi, id_kabupaten, kode_puskesmas, id_tipe_kawasan, id_tipe_puskesmas, id_jenis_faskes, all_puskesmas, kategori_kelas)
    {
      $('#page-area-content').empty();
      $('#page-area-content').load('rekapitulasi/view_rekapitulasi/' + tahun + '/' +id_provinsi+ '/'+id_kabupaten+'/'+kode_puskesmas+'/'+id_tipe_kawasan+'/'+id_tipe_puskesmas+'/'+id_jenis_faskes+'/'+all_puskesmas+'/'+kategori_kelas);
    }


    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_rekapitulasi(id)
    {

      if(confirm('Apakah anda yakin akan menghapus data ini?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "rekapitulasi/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               greatSuccess();
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                greatError();
            }
        });
         
      }

    }


$.validator.setDefaults({
  submitHandler: function() { 
    
     url = "rekapitulasi/post_form";

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_rekapitulasi').serialize(),
            dataType: "JSON",
            success: function(data)
            { 

              $('#page-area-content').empty();
              $('#page-area-content').load('rekapitulasi/view_rekapitulasi/' + data.tahun + '/' +data.id_provinsi+ '/'+data.id_kabupaten+'/'+data.kode_puskesmas+'/'+data.id_tipe_kawasan+'/'+data.id_tipe_puskesmas+'/'+data.id_jenis_faskes+'/'+data.all_puskesmas+'/'+data.kategori_kelas);

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

              greatError();

            }
        });

  }

});

function disabledBtn()
  {
    $('#btnSave').disabled = true;
    return true;
  }


// jquery validation //
$().ready(function() {

  // validate signup form on keyup and submit
  $("#form_rekapitulasi").validate({
    rules: {
      
      tahun: "required",
      active: "required"
    },

    messages: {
      tahun: "Silahkan pilih tahun!"
    }
  });


});


// jquery choosen //
jQuery(function($) {
      
      if(!ace.vars['touch']) {
        $('.chosen-select').chosen({allow_single_deselect:true}); 
        //resize the chosen on window resize
    
        $(window)
        .off('resize.chosen')
        .on('resize.chosen', function() {
          $('.chosen-select').each(function() {
             var $this = $(this);
             $this.next().css({'width': $this.parent().width()});
          })
        }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
          if(event_name != 'sidebar_collapsed') return;
          $('.chosen-select').each(function() {
             var $this = $(this);
             $this.next().css({'width': $this.parent().width()});
          })
        });
    
      }
    
    
    });
