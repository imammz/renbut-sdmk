<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$lang['config'] = 'config';
$lang['extensions_list'] = 'Extensions list';
$lang['get_more_extensions'] = 'get more extensions';
$lang['extension_is_up_to_date'] = 'extension is up to date';
$lang['or_can'] = 'or_can';
$lang['no_configuration_inside_the_extension'] = 'no configuration inside the extension';
$lang['this_page_to_manage_website_extensions,_see_documentation_for_use_it'] = 'This page to manage website extensions, see documentation for use it';