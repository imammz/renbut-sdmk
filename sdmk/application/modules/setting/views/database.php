<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');} ?> 
<title> Pengaturan - Database</title>
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-wrench"></span> &nbsp;<?php echo $this->lang['setting'] ?>
            <sup class="sup-menu">
                <i class="fa fa-hdd-o"></i> <?php echo $this->lang['database']; ?>
            </sup>
        </h3>
        <span class="xamzo-head-info"><?php echo $this->lang['this_page_to_manage_website_setting,_see_documentation_for_use_it'] ?>.</span>
    </div>
    <div class="right">
    </div>
</div>
<div id="xamzo-head-action">
    <button class="btn primary large fa fa-angle-down" dropdown="true" dropdown-relative="left" dropdown-target="post-add"><span class="no-phone"><?php echo $this->lang['setting']; ?></span></button>
    <button ajax="true" url-push="true" href="#!setting/database" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div>
<ul class="dropdown-target dropdown-target-left post-add">
    <div class="arrow-up"></div>
    <li>
        <a ajax="true" url-push="true" href="#!setting/index" ajax-target=".xamzo-right"><i class="fa fa-info-circle"></i> <?php echo $this->lang['info']; ?></a>
    </li>   
    <li>
        <a ajax="true" url-push="true" href="#!setting/general" ajax-target=".xamzo-right"><i class="fa fa-cog"></i> <?php echo $this->lang['general']; ?></a>
    </li>   
    <li>    
        <a ajax="true" url-push="true" href="#!setting/security" ajax-target=".xamzo-right"><i class="fa fa-lock"></i> <?php echo $this->lang['security']; ?></a>
    </li> 
</ul>

<div id="crud-target" class="xamzo-setting"> 
    <div class="xamzo-block small no-tablet">
        <div class="xamzo-block-content">
            <div class="xamzo-block-header">
                <h4><?php echo $this->lang['database']; ?> <?php echo $this->lang['setting']; ?></h4>
            </div>
            <div class="content">
                <form id="crud-form">
                <div class="xamzo-setting-list">
                    <p>
                        <label class="control-label small"><b><?php echo $this->lang['driver'] ?></b></label>
                        : (<?php echo $con['database']['driver']; ?>)<i class="no-768 no-float"> &RightArrow; <?php echo $this->lang['database_driver'];?></i>
                    </p>
                    <p>
                        <label class="control-label small"><b><?php echo $this->lang['host'] ?></b></label>
                        : (<?php echo $con['database']['host']; ?>) <i class="no-768 no-float"> &RightArrow; <?php echo $this->lang['database_host']; ?></i>
                    </p>
                    <p>
                        <label class="control-label small"><b><?php echo $this->lang['name'] ?></b></label>
                        : (<?php echo $con['database']['name']; ?>)  <i class="no-768 no-float"> &RightArrow; <?php echo $this->lang['database_name']; ?></i>
                    </p>
                    <p>
                        <label class="control-label small"><b><?php echo $this->lang['user'] ?></b></label>
                        : (<?php echo $con['database']['user']; ?>)<i class="no-768 no-float"> &RightArrow; <?php echo $this->lang['database_user']; ?></i>
                    </p>
                    <p>
                        <label class="control-label small"><b><?php echo $this->lang['password'] ?></b></label>
                        : (<?php echo $con['database']['pass']; ?>) <i class="no-768 no-float"> &RightArrow; <?php echo $this->lang['database_password']; ?></i>
                    </p>
                </div>
                </form>
            </div>
        </div>
    </div>

     <div class="xamzo-block separated"></div>
    <div class="xamzo-block big">
        <div class="tab" tab-parent=".my-tab1" tab-animation="bounce">
            <span style="float: left; margin-right: 4px;">
                <button style="margin-top:-7px;padding: 7px 10px;" crud-name="check" class="btn primary medium fa fa-check-square-o tip-top" original-title="<?php echo $this->lang['select_all'] ?>" table-check-all="true"></button>
                <button style="margin-top:-7px;padding: 7px 10px;" crud-name="backup" href="javascript:void(0)" class="btn tip-top-right fa fa-database medium primary" original-title="<?php echo $this->lang['backup'];?>" onclick="backupdb()"></button>
            </span>
            <a class="active" tab-target="#list-table"><?php echo $this->lang['list_table'] ?></a>
            <a class="" tab-target="#list-backup"><?php echo $this->lang['list_backup'] ?></a>  
        </div>
        <div class="tab-target my-tab1">
            <div class="child-tab" id="list-table" style="overflow-x: inherit;">
                <div class="list_tb box">
                    <form id="backup_db">      
                        <ul class="list-tbl">     
                            <input type="checkbox" style="display: none;" table-check-all="true">
                        <?php
                        $tbl = 'Tables_in_'.$con['database']['name'];
                            foreach ($tables_name as $row) {
                                if (isset($row[$tbl])) {
                                    echo "<li>
                            <input table-check-all='true' type='checkbox' name='id[]'   value='" . $row[$tbl] . "' id='" . $row[$tbl] . "'>
                            <label for='" . $row[$tbl] . "'> " . $row[$tbl] . "</label>
                            <li>
                            ";
                                }
                            }
                        ?>  
                        </ul>           
                    </form>
                    
                </div>
            </div>
            <div class="child-tab" id="list-backup" style="overflow-x: inherit">
                <?php
                $base = BACKUP;
                $dirs = opendir($base);
                while (false !== ($file = readdir($dirs))) {
                    $dir[] = $file;
                }
                for ($i = 0; $i < count($dir); $i++) {
                    if ($dir[$i] != '.' && $dir[$i] != '..' && $dir[$i] != 'index.html' && $dir[$i] != '.htaccess') {
                        ?>
                        <div class="list-b-backup" id="db_<?php echo $i ?>">
                            <div class="l-data">
                                <span class="right">
                                    <a href="javascript:void(0)" onclick="db_delete('<?php echo $dir[$i] ?>', '#db_<?php echo $i ?>')" original-title="<?php echo $this->lang['delete']; ?>" class="tip-top"><i class=" fa fa-trash"></i> </a> 
                                    <a href="<?php echo site_url() . 'setting/download_db/' . $dir[$i] ?>" original-title="<?php echo $this->lang['download']; ?>" class="tip-top"> <i class=" fa fa-download"></i> </a>
                                </span>
                                <span class="left">
                                    <a href="javascript:void(0)" id="header" itemid="show"  onclick="slide_(this, 'ul.db_<?php echo $i ?>')"><?php echo $this->lang['backup_DB_on']; ?> <?php echo $dir[$i] ?> &nbsp; <i class=" fa fa-caret-down"></i></a>
                                </span>
                            </div> 
                            <ul class="db_<?php echo $i ?>" style="display: none">
                                <?php
                                $ca = x_list_dir($base . $dir[$i]);
                                for ($j = 0; $j < count($ca); $j++) {
                                    echo "<li>" . $ca[$j] . "</li>";
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                    }
                }
                ?>
            </div> 
        </div> 
    </div>
</div>
