<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		modules/email
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.5
 * @filesource
 */
class Email extends X_Controller {

    public function __construct() {
        parent::__construct();
        $this->__composer('email'); 
    }

    public function index() {
        if (is_ajax_request()) {
            if (is_auth('con_email', 'x')) {
                $view['option'] = $this->lib_email->config('option');
                $view['email_sender'] = $this->lib_email->config('email_sender');
                $view['name_sender'] = $this->lib_email->config('name_sender');
                $view['method'] = array('phpmail' => 'send_email_use_phpmail', 'smtp' => 'send_email_use_smtp', 'gmail_smtp' => 'send_email_use_google_smtp');
                $this->__view('email', 'index', $view);
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function template() {
        if (is_admin() && is_ajax_request()) {
            $this->__view('email', 'template');
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function editor($file = null) {
        if (is_admin() && is_ajax_request()) {
            $php = x_file_get_content(MODPATH . 'email/views/template/'.$file . EXT);
            $view['content'] = $php;
            $view['file_dir'] = MODPATH . 'email/views/template/'.$file . EXT;
            $this->__view('email', 'editor/read', $view);
        } else {
            print_r('Direct access is not allowed');
        }
    }
    public function save_editor() {
        if (is_admin() && is_ajax_request()) {
            $content = $this->request->post('content', false);
            $file = $this->request->post('file', false);
            x_file_put_content($file, $content); 
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function reader() {
        $message = $this->__view('email','rss/reader');
        foreach ($this->lib_email->reader() as $name => $email) {
            send_mail('title', 'News Letter',  site_domain(), $name, $email, $message);
        }
    }

    public function option() {
        if (is_ajax_request()) {
            if (is_auth('con_email', 'x')) {
                $opt = $this->request->post('option');
                $this->lib_email->update(array('option' => $opt));
                switch ($opt) {
                    case 'phpmail':
                        $this->__view('email', 'option/phpmail');
                        break;
                    case 'smtp':
                        $json = x_file_get_content(MODPATH . 'email/configs/smtp' . JSON);
                        $view['con'] = x_read_json_decode($json);
                        $this->__view('email', 'option/smtp', $view);
                        break;
                    case 'gmail_smtp':
                        $json = x_file_get_content(MODPATH . 'email/configs/gmail' . JSON);
                        $view['con'] = x_read_json_decode($json);
                        $this->__view('email', 'option/gmail', $view);
                        break;
                    default:
                        break;
                }
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function update() {
        if (is_ajax_request()) {
            if (is_auth('con_email', 'x')) {
                $this->lib_email->update(array('email_sender' => $this->request->post('email_sender')));
                $this->lib_email->update(array('name_sender' => $this->request->post('name_sender')));
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function gmail() {
        if (is_ajax_request()) {
            if (is_auth('con_email', 'x')) {
                x_update_setting('application/modules/email/configs/gmail' . JSON);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

    public function smtp() {
        if (is_ajax_request()) {
            if (is_auth('con_email', 'x')) {
                x_update_setting('application/modules/email/configs/smtp' . JSON);
            } else {
                print_r("You don't have access in this module");
            }
        } else {
            print_r('Direct access is not allowed');
        }
    }

}
