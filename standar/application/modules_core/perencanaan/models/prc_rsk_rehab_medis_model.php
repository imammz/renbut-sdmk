<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prc_rsk_rehab_medis_model extends CI_Model {

	var $id_jenis_faskes = 28;
	var $table = 't_perencanaan';
	var $column = array('t_perencanaan.tahun','t_perencanaan.jumlah_tt','t_perencanaan.kategori_kelas','m_kabupaten.nama_kabupaten','m_rumah_sakit.nama_rs');
	var $select = 't_perencanaan.*, m_rumah_sakit.nama_rs, m_kabupaten.nama_kabupaten';

	var $order = array('t_perencanaan.id_perencanaan' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_rumah_sakit','m_rumah_sakit.kode_rs='.$this->table.'.kode_rsu','left');
		$this->db->join('m_kabupaten','m_kabupaten.id_kabupaten=m_rumah_sakit.id_kabupaten','left');
		if ( $this->session->userdata('data_user')->id_role == 5 ) {
			$this->db->where(array(''.$this->table.'.kode_rsu'=>$this->session->userdata('data_user')->kode_rsu));
		}else if ( $this->session->userdata('data_user')->id_role == 4 ) {
			$this->db->where(array(''.$this->table.'.id_kabupaten'=>$this->session->userdata('data_user')->id_kabupaten));
		}else if ( $this->session->userdata('data_user')->id_role == 3 ) {
			$this->db->where(array(''.$this->table.'.id_provinsi'=>$this->session->userdata('data_user')->id_provinsi));
		}
		
		$this->db->where('id_jenis_faskes', $this->id_jenis_faskes);

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->where(''.$this->table.'.id_perencanaan',$id);
		$this->db->join('m_rumah_sakit','m_rumah_sakit.kode_rs='.$this->table.'.kode_rsu','left');
		$this->db->join('m_kabupaten','m_kabupaten.id_kabupaten=m_rumah_sakit.kode_rsu','left');

		$query = $this->db->get();

		return $query->row();
	}

	public function get_data_by_custom($where)
	{

		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->where($where);
		$this->db->where('id_jenis_faskes', $this->id_jenis_faskes);
		$this->db->join('m_rumah_sakit','m_rumah_sakit.kode_rs='.$this->table.'.kode_rsu ','left');
		$this->db->join('m_kabupaten','m_kabupaten.id_kabupaten=m_rumah_sakit.id_kabupaten','left');

		$query = $this->db->get();
		//print_r($this->db->last_query());die;
		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		//delete t_detail_perencanaan
		$this->db->delete('t_detail_perencanaan', array('id_perencanaan'=>$id));
		$this->db->delete('t_detail_perencanaan_form', array('id_perencanaan'=>$id));
		$this->db->where(''.$this->table.'.id_perencanaan', $id);
		$this->db->delete($this->table);

	}

	public function get_value_form($id_perencanaan)
	{
		$this->db->from('t_detail_perencanaan_form');
		$this->db->where('id_perencanaan', $id_perencanaan);
		$query = $this->db->get();

		return $query->result();
	}

}
