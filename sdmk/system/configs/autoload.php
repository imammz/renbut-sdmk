<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		configs/autoload
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

/**
 *  load library in xamzo Framewrok
 */
$autoload['libraries'] = array();

/**
 * load helpers in xamzo Framework
 */
$autoload['helpers'] = array('app','url','date','form','widget','logs','extension','module','theme','security','string','text','auth','media');