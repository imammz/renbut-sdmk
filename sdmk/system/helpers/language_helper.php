<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
 

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/language
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

if (!function_exists('x_default_lang')) {

    function x_default_lang() {
        $con_lang = explode('(', X_LANG);
        return trim($con_lang['0']);
    }

}
 
/**
 * End of file ./system/helpers/language_helper.php
 */