<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: #990000;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: none;
}
</style>
</head>
<body>
<!--    <div>
        <h2> || <a href="<?php echo base_url() ?>dashboard/venue"> Data Venue </a> || <a href="<?php echo base_url() ?>dashboard/category"> Data Category Venue </a> || <a href="<?php echo base_url() ?>dashboard/travel_blog"> Data Travel Blog </a> || <a href="<?php echo base_url() ?>dashboard/travel_package"> Data Travel Package </a> || <a href="<?php echo base_url() ?>dashboard/news"> News </a> || <a href="<?php echo base_url() ?>dashboard/video"> Video </a> || <a href="<?php echo base_url() ?>dashboard/event"> Event </a></h2>
    <h3><?php echo $title; ?></h3>
    </div> -->
    
	<div>
		<?php echo $output; ?>
    </div>
</body>
</html>
