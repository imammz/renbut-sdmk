<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class Lib_menus {

   
    function get_menus() {
		
		$CI =&get_instance();
		$db = $CI->load->database('default', TRUE);
		
		$getData = array();
		$sess_menu = Lib_menus::get_hak_akses_menu_role($this->session->userdata('data_user')->id_role);

        if($sess_menu){
            foreach ($sess_menu as $key => $value) {
                # code...
                $getData[] = $value;
            }
        }        

		return $getData;
		
    }


    public function get_hak_akses_menu_role($id_role){

        // get menu role
        $menu_role = Lib_menus::qry_main_menu($id_role);

        foreach ($menu_role->result_array() as $key => $value) {

            if( ($value['parent_menu'] == 0) && ($value['link'] == '#') ){

                /*$this->db->select('m_menu.*,t_menu_role.id_role');
                $this->db->from('t_menu_role');
                $this->db->join('m_menu', 'm_menu.id_menu=t_menu_role.id_menu', 'left');
                $this->db->group_by('t_menu_role.id_menu');
                $this->db->where(array('id_role'=>$id_role, 'parent_menu'=>$value['id_menu'], 'm_menu.active'=>'Y'));
                $submenu = $this->db->get()->result_array();

                foreach ($submenu as $row_submenu) {

                    $code_action = Lib_menus::get_code_action(array('id_role'=>$id_role, 'id_menu'=>$row_submenu['id_menu']));

                    $row_submenu['action'] = $code_action;
                    $arr_submenu[] = $row_submenu;

                }*/
                
                $value['submenu'] = Lib_menus::qry_submenu($id_role, $value['id_menu']);

            }else{
                
                $code_action = Lib_menus::get_code_action(array('id_role'=>$id_role, 'id_menu'=>$value['id_menu']));

                $value['submenu'] = array();
                $value['action'] = $code_action;
            }

            
            $getData[] = $value;
        }

        return $getData;
    }

    public function get_code_action($where){

        $code_action = $this->db->select('code')->get_where('t_menu_role', $where)->result_array();
        foreach ($code_action as $key => $value) {
            # code...
            $arr_code[] = $value['code'];
        }

        return $arr_code;
    }

    public function qry_submenu($id_role, $id_menu){

        $this->db->select('m_menu.*,t_menu_role.id_role');
        $this->db->from('t_menu_role');
        $this->db->join('m_menu', 'm_menu.id_menu=t_menu_role.id_menu', 'left');
        $this->db->group_by('t_menu_role.id_menu');
        $this->db->where(array('id_role'=>$id_role, 'parent_menu'=>$id_menu, 'm_menu.active'=>'Y'));
        $submenu = $this->db->get()->result_array();

        foreach ($submenu as $row_submenu) {

            $code_action = Lib_menus::get_code_action(array('id_role'=>$id_role, 'id_menu'=>$row_submenu['id_menu']));

            $row_submenu['action'] = $code_action;
            $arr_submenu[] = $row_submenu;

        }

        return $arr_submenu;
    }

    public function qry_main_menu($id_role){

        /*$this->db->select('m_menu.*,t_menu_role.id_role');
        $this->db->from('t_menu_role');
        $this->db->join('(SELECT * FROM m_menu WHERE parent_menu = 0 ORDER BY counter ASC)AS tbl_menu', 'm_menu.id_menu=t_menu_role.id_menu', 'left');
        $this->db->group_by('t_menu_role.id_menu');
        $this->db->where(array('id_role'=>$id_role, 'parent_menu'=>0, 'm_menu.active'=>'Y'));
        $menu_role = $this->db->get();*/

        $query = "SELECT tbl_menu.*,t_menu_role.id_role
                    FROM t_menu_role
                    JOIN (SELECT * FROM m_menu WHERE parent_menu = 0 AND active='Y' ORDER BY counter ASC)AS tbl_menu ON tbl_menu.id_menu=t_menu_role.id_menu
                    WHERE t_menu_role.id_role = ".$id_role."
                    GROUP BY t_menu_role.`id_menu`
                    ORDER BY tbl_menu.counter ASC";
        $menu_role = $this->db->query($query);

        return $menu_role;
    }


	
}

?> 