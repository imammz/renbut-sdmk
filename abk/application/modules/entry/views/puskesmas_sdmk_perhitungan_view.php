<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />

<link href="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/select2/select2.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker3.standalone.min.css" rel="stylesheet" media="screen">
<link href="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">
<style>
    .CSSTableGenerator {
        margin:0px;padding:0px;
        width:100%; box-shadow: 10px 10px 5px #888888;
        border:1px solid #000000;

        -moz-border-radius-bottomleft:0px;
        -webkit-border-bottom-left-radius:0px;
        border-bottom-left-radius:0px;

        -moz-border-radius-bottomright:0px;
        -webkit-border-bottom-right-radius:0px;
        border-bottom-right-radius:0px;

        -moz-border-radius-topright:0px;
        -webkit-border-top-right-radius:0px;
        border-top-right-radius:0px;

        -moz-border-radius-topleft:0px;
        -webkit-border-top-left-radius:0px;
        border-top-left-radius:0px;
    }.CSSTableGenerator table{
        width:100%;
        height:100%;
        margin:0px;padding:0px;
    }.CSSTableGenerator tr:last-child td:last-child {
        -moz-border-radius-bottomright:0px;
        -webkit-border-bottom-right-radius:0px;
        border-bottom-right-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:first-child {
        -moz-border-radius-topleft:0px;
        -webkit-border-top-left-radius:0px;
        border-top-left-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:last-child {
        -moz-border-radius-topright:0px;
        -webkit-border-top-right-radius:0px;
        border-top-right-radius:0px;
    }.CSSTableGenerator tr:last-child td:first-child{
        -moz-border-radius-bottomleft:0px;
        -webkit-border-bottom-left-radius:0px;
        border-bottom-left-radius:0px;
    }.CSSTableGenerator tr:hover td{

    }.CSSTableGenerator tr:nth-child(odd){ background-color:#e5e5e5; }
    .CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }
    .CSSTableGenerator td{
        vertical-align:middle;


        border:1px solid #000000;
        border-width:0px 1px 1px 0px;
        text-align:left;
        padding:7px;
        font-size:10px;
        font-family:arial;
        font-weight:normal;
        color:#000000;
    }.CSSTableGenerator tr:last-child td{
        border-width:0px 1px 0px 0px;
    }.CSSTableGenerator tr td:last-child{
        border-width:0px 0px 1px 0px;
    }.CSSTableGenerator tr:last-child td:last-child{
        border-width:0px 0px 0px 0px;
    }
    .CSSTableGenerator tr:first-child td{
        background:-o-linear-gradient(bottom, #4c4c4c 5%, #000000 100%);    background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #000000) );    background:-moz-linear-gradient( center top, #4c4c4c 5%, #000000 100% );    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#000000");  background: -o-linear-gradient(top,#4c4c4c,000000);
        background-color:#4c4c4c;
        border:0px solid #000000;
        text-align:center;
        border-width:0px 0px 1px 1px;
        font-size:14px;
        font-family:arial;
        font-weight:bold;
        color:#ffffff;
    }
    .CSSTableGenerator tr:first-child:hover td{
        background:-o-linear-gradient(bottom, #4c4c4c 5%, #000000 100%);    background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #4c4c4c), color-stop(1, #000000) );    background:-moz-linear-gradient( center top, #4c4c4c 5%, #000000 100% );    filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#4c4c4c", endColorstr="#000000");  background: -o-linear-gradient(top,#4c4c4c,000000);
        background-color:#4c4c4c;
    }
    .CSSTableGenerator tr:first-child td:first-child{
        border-width:0px 0px 1px 0px;
    }
    .CSSTableGenerator tr:first-child td:last-child{
        border-width:0px 0px 1px 1px;
    }
    .CSSTableGenerator th {
        text-align: center;
    }

    .CSSTableGenerator td {
        text-align: center;
    }
</style>
<!-- end: HEAD -->
<body>



    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1200px;">

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();
                
                echo "<center>";
                echo "<h3>{$sdmk_faskes_installasi['sdmk_faskes_installasi_nama']}</h3>";
                echo "<h3>{$faskes_installasi['installasi_nama']} - {$faskes['faskes_nama']}</h3>";
                echo "<h4>{$kab['NAMA_KAB']} - {$prov['NAMA_PROV']}</h4>";
                echo "</center><br>";
                echo "<h2>Langkah 1 - Menetapkan Jenis SDMK</h2>";
                ?>
                <hr/>

                <table border="1" class="CSSTableGenerator" style="text-align: center;">
                    <tr>
                        <th> Jumah PNS </th>
                        <th> Jumah PPPK </th>
                        <th> Jumah PTT </th>
                        <th> Jumah Honoror Daerah</th>
                        <th> Jumah yang lainnya</th>
                    </tr>

                    <tr>
                        <td> <?php echo $sdmk_faskes_installasi['jumlah_PNS']  ?> </td>
                        <td> <?php echo $sdmk_faskes_installasi['jumlah_PPPK']  ?> </td>
                        <td> <?php echo $sdmk_faskes_installasi['jumlah_PTT']  ?> </td>
                        <td> <?php echo $sdmk_faskes_installasi['jumlah_HD']  ?> </td>
                        <td> <?php echo $sdmk_faskes_installasi['jumlah_DLL']  ?> </td>
                    </tr>
                </table>

            </div>
        </div>
        <br>
        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                echo "<h2>Langkah 2 - Penetapan Waktu Kerja tersedia</h2>";
                ?>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/puskesmas/tambahABK/<?php echo $sdmk_faskes_installasi_id ?>">

                    <sub style="text-align:justify;">Dalam Keputusan Presiden Nomor 68 Tahun 1995 telah ditentukan jam kerja instansi pemerintah 37 jam 30 menit per minggu, baik untuk yang 5 (lima) hari kerja ataupun yang 6 (enam) hari kerja sesuai dengan yang ditetapkan Kepala Daerah masing-masing. 
                        Berdasarkan Peraturan Badan Kepegawaian Negara Nomor 19 Tahun 2011 tentang Pedoman Umum Penyusunan Kebutuhan Pegawai Negeri Sipil, Jam Kerja Efektif (JKE) sebesar 1200 jam per tahun. Demikian juga menurt Permen PA-RB No. 26 tahun 2011, Jam Kerja Efektif (JKE) sebesar 1200 jam per tahun atau 72000 menit per tahun baik 5 hari kerja atau 6 hari kerja.
                    </sub>
                    
                    <div class="box-body">
                        <table width="100%" border="1" class="CSSTableGenerator" style="text-align: center;">
                            <tr><th width="50" rowspan="2"> No </th><th width="80"> Kode </th> <th> Komponen </th> <th> Keterangan </th> <th> Rumus </th> <th> Jumlah </th> <th> Satuan </th> </tr>
                            <tr><th width="80"> A </th> <th> B </th> <th> C </th> <th> D </th> <th> E </th> <th> F </th> </tr>
                            
                            <?php 
                            $no = 1;
                            foreach($wkt as $row) { ?>
                            
                            <tr><td> <?php echo $no; ?> </td> <td> <?php  echo $row['komponen_wkt_kode']; ?></td> <td> <?php echo $row['komponen_wkt_komponen'] ?></td> <td> <?php echo $row['komponen_wkt_keterangan'] ?></td> <td> <?php echo $row['komponen_wkt_rumus'] ?></td> <td> <?php echo $row['komponen_wkt_jumlah'] ?></td> <td> <?php $satuan =  $this->orm->ref_satuan->where('satuan_id',$row['satuan_id'])->fetch(); echo $satuan['satuan_nama'] ?></td> </tr>  
                            
                                
                                <?php $no++; } ?>
                            
                            <tr> <td colspan="5"> <b> <?php echo $wkt_hasil_jam['komponen_wkt_komponen'] ?> </b> </td>   <td> <b> <?php echo number_format($wkt_hasil_jam['komponen_wkt_jumlah'], 2, '.', ',') ?> </b></td>  <td> <?php $satuan =  $this->orm->ref_satuan->where('satuan_id',$wkt_hasil_jam['satuan_id'])->fetch(); echo $satuan['satuan_nama'] ?></td> </tr> 
                            <tr> <td colspan="5"> <b> <?php echo $wkt_hasil_menit['komponen_wkt_komponen'] ?> </b> </td>   <td> <b> <?php echo number_format($wkt_hasil_menit['komponen_wkt_jumlah'], 2, '.', ',')  ?> </b></td>  <td> <?php $satuan =  $this->orm->ref_satuan->where('satuan_id',$wkt_hasil_menit['satuan_id'])->fetch(); echo $satuan['satuan_nama'] ?></td> </tr> 
                            
                        </table> 
                    </div>
                    <br/>
                   
                </form>  


            </div>
        </div>

        <br>

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $abk_model = new abk_model();
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                echo "<h2>Langkah 3 - Menetapkan Komponen Beban Kerja dan Norma Waktu</h2>";
                ?>
                <br/>

                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/puskesmas/tambahABK_proses/<?php echo $sdmk_faskes_installasi_id ?>">

                    <br/>
                    <h3> Tugas Pokok </h3>
                    <table id="tbl_tugas_pokok" width="90%" style='margin-top: 10px; margin-left: 10px;'>
                        <tr bgcolor="#dddddd" >
                            <td width="25px"><b>|#</b> &nbsp; </td>
                            <td width="40px"><b>| No </b> &nbsp; </td>
                            <td width="350px"><b>| Uraian Pekerjaan </b> &nbsp; </td>
                            <td width="60px"><b>| Norma Waktu </b> &nbsp; </td>
                            <td width="60px"><b>| Satuan </b> &nbsp; </td>
                        </tr>

                        <?php                            
                            $no = 1;
                            foreach ($this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?', $sdmk_faskes_installasi_id, 1)->order('urut') as $row) {
                                echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_pokok[]\" value=\"" . $no . "\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_pokok[]\" value=\"" . $row['kbk_uraian'] . "\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"norma_waktu_tugas_pokok\"   name=\"norma_waktu_tugas_pokok[]\" value=\"" . $row['norma_waktu'] . "\"/>
                                                    <input type=\"hidden\" name=\"capaian[]\" value=\"" . $row['capaian'] . "\"/>    
                                                    </td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_pokok[]\" data-placeholder=\"Pilih Satuan\">"
                                . "";

                                foreach ($ref_satuan1 as $row_satuan) {
                                    $selected = ($row['satuan_id'] == $row_satuan['satuan_id']) ? 'selected' : '';
                                    echo "<option {$selected}  value=\"{$row_satuan['satuan_id']}\"> <sup>{$row_satuan['satuan_nama']}</sup> </option>";
                                }

                                echo "</select></td>
                                                    </tr>";
                                $no++;
                                
                            }
                        
                        ?>
                    </table>

                    <p> <hr/> <hr/> </p>


                    <h3> Tugas Penunjang </h3>
                    <br/>
                    <table id="tbl_tugas_penunjang" width="90%" style='margin-top: 10px; margin-left: 10px;'>
                        <tr bgcolor="#dddddd" >
                            <td width="25px"><b>|#</b> &nbsp; </td>
                            <td width="40px"><b>| No </b> &nbsp; </td>
                            <td width="350px"><b>| Uraian Pekerjaan </b> &nbsp; </td>
                            <td width="60px"><b>| Norma Waktu </b> &nbsp; </td>
                            <td width="60px"><b>| Satuan </b> &nbsp; </td>
                        </tr>
<?php
    $no = 1;
    foreach ($this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?', $sdmk_faskes_installasi_id, 2)->order('urut') as $row) {

        echo "<tr><td width=\"35px\"><input type=\"checkbox\" size=\"1\" name=\"chk\"/></td>
                                                    <td width=\"40px\"><input class=\"form-control\" type=\"text\" size=\"3\"  name=\"urut_tugas_penunjang[]\" value=\"" . $no . "\"/></td>
                                                    <td width=\"350px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"list_kbk\"   name=\"uraian_tugas_penunjang[]\" value=\"" . $row['kbk_uraian'] . "\"/></td>
                                                    <td width=\"50px\"><input class=\"form-control\" type=\"text\" autocomplete=\"off\" list=\"norma_waktu_tugas_penunjang\"   name=\"norma_waktu_tugas_penunjang[]\" value=\"" . $row['norma_waktu'] . "\"/></td>
                                                    <td width=\"50px\"><select  name=\"satuan_id_tugas_penunjang[]\" data-placeholder=\"Pilih Satuan\">"
        . "";

        foreach ($ref_satuan2 as $row_satuan) {
            $selected = ($row['satuan_id'] == $row_satuan['satuan_id']) ? 'selected' : '';
            echo "<option {$selected}  value=\"{$row_satuan['satuan_id']}\"> <sup>{$row_satuan['satuan_nama']}</sup> </option>";
        }

        echo "</select></td>
                                                    </tr>";
        $no++;
    }
?>
                    </table>

                    <datalist id="list_kbk">
                        <?php foreach ($kbk as $row) { ?>
                            <option value="<?php echo $row['kbk_uraian'] ?>">
                        <?php } ?>                          
                    </datalist>
                    <p>
                </form>  


            </div>
        </div>

        <div class="row">            
            <div class="col-md-12"> 
                <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                ?>
                <hr/>
                <hr/>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/puskesmas/tambahResult_proses/<?php echo $sdmk_faskes_installasi_id ?>">
                    
                    <div class="box-body">
                        <?php echo "<h2>Langkah 4 - Standar Beban Kerja (SBK)</h2>"; ?>
                        <h4>Tugas Pokok</h4>
                        <table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th>Norma Waktu</th><th>Satuan</th><th>WKT (mnt/th)</th><th>SBK <br/> (Standar Beban Kerja)</th></tr>   
                            <?php 
                            $abk_model = new abk_model();
                            $no = 1;
                            $kbk1 = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,1)->order('urut ASC');
                            foreach($kbk1 as $row3) {
                                $satuan = $this->orm->ref_satuan->where('satuan_id',$row3['satuan_id'])->fetch();
                                ?>
                                <tr><td><?php echo $no ?></td><td><?php echo $row3['kbk_uraian'] ?></td><td style="text-align: right"><?php echo $row3['norma_waktu'] ?></td><td style="text-align: right"><?php echo $satuan['satuan_nama'] ?></td><td style="text-align: right"><?php echo number_format($abk_model->_getWKTMenit(),0); ?></td><td style="text-align: right"><?php echo $row3['SBK'] ?></td></tr>
                            <?php $no++; } ?>
                            </table>
                            <hr/>
                            <h2>Langkah 5 - Menghitung Faktor Tugas Penunjang (FTP) dan Standar Tugas Penunjang (STP)</h2>
                            <h4>Tugas Penunjang</h4>
                            <table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th>Norma Waktu</th><th>Satuan</th><th>Waktu Kegiatan (mnt/th)</th><th>WKT <br/> (mnt/th)</th><th style="text-align: center;"> <b> FTP <br/> % </b></th></tr>   
                            <?php 
                            $no = 1;
                            $ftp_total = 0;
                            $kbk2 = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id = ? AND jenis_tugas_id = ?',$sdmk_faskes_installasi_id,2)->order('urut ASC');
                            foreach($kbk2 as $row3) {
                                $satuan = $this->orm->ref_satuan->where('satuan_id',$row3['satuan_id'])->fetch();
                                $ftp_total += $row3['FTP'];
                                ?>
                                <tr><td><?php echo $no ?></td><td><?php echo $row3['kbk_uraian'] ?></td><td style="text-align: right"><?php echo $row3['norma_waktu'] ?></td><td style="text-align: right"><?php echo $satuan['satuan_nama'] ?></td> <td style="text-align: right"><?php echo $row3['waktu_kegiatan'] ?></td> <td style="text-align: right"> <?php echo number_format($abk_model->_getWKTMenit(),0); ?> </td><td style="text-align: right"><?php echo number_format($row3['FTP']*100, 2); ?></td></tr>
                            <?php $no++; } ?>
                                <tr><td colspan="5">  </td><td><b>TOTAL FTP</b></td><td style="text-align: right"><?php echo number_format($ftp_total*100, 2); ?></td></tr>
                                <tr><td colspan="6" style="text-align: right"><b>Standard  Tugas Penunjang  (STP)   =(1/(1 – FTP/100)</b></td><td style="text-align: right"><?php  echo $abk_model->_fmlSTP($ftp_total) ?></td></tr>
                            </table>
                            
                            <hr/>
                             <h2>Langkah 6 - Menghitung Kebutuhan SDMK</h2>
                            <table width="100%" border="1">
                                <tr><th width="10">No</th><th width="500">Uraian Tugas</th><th width="30" style="background-color: #FFE495"> Capaian </th><th>SBK <br/> (Standar Beban Kerja)</th><th> Kebutuhan SDMK </th></tr>   
                            <?php 
                            $no = 1;
                            foreach($kbk1 as $row3) {
                                $satuan = $this->orm->ref_satuan->where('satuan_id',$row3['satuan_id'])->fetch();
                                ?>
                                <tr><td><?php echo $no ?></td><td><?php echo $row3['kbk_uraian'] ?></td><td style="text-align: right"> 
                                        <input style="background-color: #FFE495" size="30" type="text" value="<?php echo $row3['capaian'] ?>" name="<?php echo $row3['sdmk_kbk_id'] ?>_capaian" required/> 
                                        <input type="hidden" name="sdmk_kbk_id[]" value="<?php echo $row3['sdmk_kbk_id'] ?>" />
                                    </td><td style="text-align: right"><?php echo $row3['SBK'] ?></td><td style="text-align: center"> ? </td></tr>
                            <?php $no++; } ?>
                            </table>
                        
                    </div>
                    <br/>
                    
                </form>  


            </div>
        </div>

        <div class="row">            
            <div class="col-md-12"> 
                <h2>Hasil Perhitungan</h2>
                <?php
                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('sdmk_faskes_installasi_id', $sdmk_faskes_installasi_id)->fetch();
                $faskes_installasi = $this->orm->faskes_installasi->where('faskes_installasi_id', $sdmk_faskes_installasi['faskes_installasi_id'])->fetch();
                $faskes = $this->orm->faskes->where('faskes_kode', $faskes_installasi['faskes_kode'])->fetch();
                $kab = $this->orm->ref_kabupaten->where('NO_KAB', $faskes['NO_KAB'])->fetch();
                $prov = $this->orm->ref_provinsi->where('NO_PROV', $kab['NO_PROV'])->fetch();

                ?>
                <hr/>
                <hr/>
                <form role="form" class="form-horizontal" method="post" action="<?php echo base_url() ?>index.php/entry/puskesmas/tambahResult_proses/<?php echo $sdmk_faskes_installasi_id ?>">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="panel panel-white no-radius text-center">
                                <div class="panel-body" style="min-height: 195px;">                                    
                                    <h3 class="StepTitle">Jumlah Saat Ini (PNS)</h3>
                                    
                                    <h1> <strong> <?php echo $sdmk_faskes_installasi['jumlah_PNS']  ?> </strong> </h1>
                                    <table style="text-align: left;"> 
                                        
                                        <tr><td>    Jumlah PPPK </td><td> &nbsp; : &nbsp; </td><td>  <b> <?php echo $sdmk_faskes_installasi['jumlah_PPPK']  ?> </b>  </td></tr>
                                     <tr><td> Jumlah PTT </td><td> &nbsp; : &nbsp; </td><td>  <b> <?php echo $sdmk_faskes_installasi['jumlah_PTT']  ?> </b>  </td></tr>
                                     <tr><td> Jumlah Honor Daerah </td><td> &nbsp; :  &nbsp;</td><td>  <b>  <?php echo $sdmk_faskes_installasi['jumlah_HD']  ?> </b>  </td></tr>
                                     <tr><td> Jumlah Lainnya </td><td> &nbsp; : &nbsp;</td><td>  <b> <?php echo $sdmk_faskes_installasi['jumlah_DLL']  ?> </b>  </td></tr>
                                    </table>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel panel-white no-radius text-center">
                                <div class="panel-body" style="min-height: 195px;">                                    
                                    <h3 class="StepTitle">Jumlah Seharusnya <br/>Berdasarkan ABK</h3>
                                    <p class="text-small">
                                    <h1> <strong> <?php echo $sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan'] ?> </strong> </h1>
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel panel-white no-radius text-center">
                                <div class="panel-body" style="min-height: 195px;">                                    
                                    <h3 class="StepTitle">Keterangan</h3>
                                    <p class="text-small">
                                    
                                   <?php 
                                    $selisih = $sdmk_faskes_installasi['jumlah_PNS']-$sdmk_faskes_installasi['hasil_kebutuhan_sdmk_pembulatan'];
                                        if($selisih<0) {
                                            $keterangan = "<div class='alert alert-danger'><strong>KURANG<br/>".$selisih."</strong></div>";
                                        }
                                        elseif($selisih>0) {
                                            $keterangan = "<div class='alert alert-danger'><strong>LEBIH<br/>".$selisih."</strong></div>";
                                        }
                                        else {
                                            $keterangan="<div class='alert alert-success'><strong>SESUAI</strong></div>";
                                        }
                                    ?>
                                    <h4><?php echo $keterangan ?></h4>
                                    </p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="text-align: center;">
                        <a class="btn btn-success btn-sm" target="_blank" type="button" name="CETAK" href="<?php echo base_url('index.php/entry/entry/detil_perhitungan_abk_excel/'.$sdmk_faskes_installasi_id) ?>"><i class="fa fa-file-excel-o"></i> Cetak </a>
                        
                    </div>
                    
                    <br/>
                    
                </form>  
            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>
    <script src="<?php echo base_url() ?>vendor/maskedinput/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/autosize/autosize.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/classie.js"></script>
    <script src="<?php echo base_url() ?>vendor/selectFx/selectFx.js"></script>
    <script src="<?php echo base_url() ?>vendor/select2/select2.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url() ?>vendor/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

    <script src="<?php echo base_url() ?>assets/js/form-elements.js"></script>
    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                            jQuery(document).ready(function () {
                                Main.init();
                                FormElements.init();

                            });

                            function genKode() {

                                var selected = document.getElementById('faskes_kode');
                                var faskes_kode = selected.options[selected.selectedIndex].value;

                                var url = '<?php echo base_url() ?>index.php/entry/puskesmas/getData/';

                                $.post(url,
                                        {
                                            faskes_kode: faskes_kode
                                        },
                                function (data) {

                                    if (data.result) {

                                            $('#faskes_kode').val(data.faskes_kode);
                                            $('#NO_KAB option[value=' + data.NO_KAB + ']').attr('selected', 'selected');
                                            $('#faskes_nama').val(data.faskes_nama);
                                            $('#faskes_telp').val(data.faskes_telp);
                                            $('#faskes_alamat').val(data.faskes_alamat);
                                            $('#faskes_email').val(data.faskes_email);
                                            $('#ref_faskes_kelas_id option[value=' + data.ref_faskes_kelas_id + ']').attr('selected', 'selected');
                                            $('#ref_jenis_faskes_kode option[value=' + data.ref_jenis_faskes_kode + ']').attr('selected', 'selected');
                                            $('#status_pengelolaan_id option[value=' + data.status_pengelolaan_id + ']').attr('selected', 'selected');
                                        
                                    }
                                    else {
                                        alert('Data Puskesmas Tidak Tersedia');
                                    }

                                }, "json");


                            }

    </script>


    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
