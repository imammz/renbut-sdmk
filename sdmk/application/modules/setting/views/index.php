<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?> 
<title> Informasi Aplikasi</title>
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-wrench"></span> &nbsp;<?php echo $this->lang['setting'] ?>
         <sup class="sup-menu">
                <i class="fa fa-info-circle"></i> <?php echo $this->lang['info']; ?>
            </sup>
        </h3>
        <span class="xamzo-head-info"><?php echo $this->lang['this_page_to_manage_website_setting,_see_documentation_for_use_it'] ?>.</span>
    </div>
    <div class="right">
    </div>
</div>
<div id="xamzo-head-action">
    <button class="btn primary large fa fa-angle-down" dropdown="true" dropdown-relative="left" dropdown-target="post-add"><span class="no-phone"><?php echo $this->lang['setting']; ?></span></button>
    <button ajax="true" url-push="true" href="#!setting/index" ajax-target=".xamzo-right" class="right btn success large fa fa-refresh tip-top" original-title="<?php echo $this->lang['refresh']; ?>"></button>
</div>
<ul class="dropdown-target dropdown-target-left post-add"> 
    <div class="arrow-up"></div>
    <li>
        <a ajax="true" url-push="true" href="#!setting/general" ajax-target=".xamzo-right"><i class="fa fa-cog"></i> <?php echo $this->lang['general']; ?></a>
    </li>     
    <li>   
        <a ajax="true" url-push="true" href="#!setting/database" ajax-target=".xamzo-right"><i class="fa fa-hdd-o"></i> <?php echo $this->lang['database']; ?></a>
    </li>    
    <li>    
        <a ajax="true" url-push="true" href="#!setting/security" ajax-target=".xamzo-right"><i class="fa fa-lock"></i> <?php echo $this->lang['security']; ?></a>
    </li> 
</ul>
<div id="crud-target"> 
    <div class="xamzo-block-full">
        <form id="crud-form">
            <?php
            for ($i = 0; $i < count($con); $i++) {
                foreach ($con[$i] as $key => $val) {
                    ?>
                    <p class="font-12 ">
                        <label class="control-label xamzo-info-label">
                            <?php echo $this->lang[$key] ?>
                        </label>
                        <?php
                        if ($val['val'] == 'true' || $val['val'] == 'false') {
                            ?>
                            <?php
                        } else {
                            ?> 
                            <?php
                            echo $val['val'];
                        }
                        ?>
                    </p>
                    <?php
                }
            }
            ?> 
            <p class="no-margin font-12 xamzo-info" style="margin-top: -10px;">xamzo by <a href="http://yafii.co/" target="_blank"><strong>Yafii Solusi Internasional</strong></a> is Premium Software released
            <p class="no-margin font-12 xamzo-info">The <strong>xamzo</strong> <sup> &reg; </sup>name and logo are trademarks in indonesian and other countries </p>
            <p class="no-margin font-12 xamzo-info">Icon By <a href="http://fontawesome.github.io/Font-Awesome/" target="_blank"><i class="fa fa-flag"></i> <strong>Font Awesome 3.2.1</strong></a></p>
        </form> 
        <div class="xamzo-block-genuine no-phone">
            <div class="xamzo-genuine"></div>
        </div>
    </div>
</div>
