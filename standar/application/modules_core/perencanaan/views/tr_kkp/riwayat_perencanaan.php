<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        <div class="clearfix">
          <button class="btn btn-sm btn-primary" onclick="getMenu('perencanaan/tr_kkp')"><i class="glyphicon glyphicon-plus"></i> Tambah Perencanaan </button><div class="pull-right tableTools-container"></div>
        </div>

        <div>

          <table id="dynamic-table" class="table table-striped table-bordered table-hover">
             <thead>
              <tr>  
                <th class="center"></th>
                <th>Nama Kabupaten</th>
                <th>Nama KKP</th>
                <th style="width: 100px">Tahun</th>
                <th>Kategori<br>Kelas</th>
                <th>Jumlah<br>Instalasi</th>
                <th>Jumlah<br>Wilayah Kerja</th>
                <th style="width: 100px">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/prc_kkp.js'?>"></script>
