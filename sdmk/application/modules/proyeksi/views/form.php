<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?> 
<div class="xamzo-head-right"> 
    <div class="left">
        <h3><span class="fa fa-bar-chart"></span> &nbsp;Proyeksi</h3>
        <span class="xamzo-head-info">Perhitungan Proyeksi Kebutuhan SDMK  Tahun 2014-2025</span>
    </div>
    <div class="right no-phone">
    </div>
</div>
<div id="xamzo-head-action">
    <button class="btn primary large fa fa-chevron-left tip-top-left" ajax-url="#!proyeksi/index" crud-name="list" ajax="true" url-push="true"  ajax-target=".xamzo-right" original-title="<?php echo $this->lang['back']; ?>" accesskey="ctrl+backspace"></button>
    <button class="btn primary large fa fa-save tip-top-right right" original-title="simpan [ctrl+s]" url-push="true" accesskey="ctrl+s" href="<?php echo isset($action) ? $action : '' ?>" ajax="true" ajax-data="#crud-form" ajax-redirect="#!proyeksi/index" ajax-target=".xamzo-right" ajax-type="POST"></button>
</div> 
<div id="crud-target">  
    <form id="crud-form" onsubmit="return false;">
        <input type="hidden" name="id" value="<?php echo isset($id) ? $id : '' ?>">
        <input type="hidden" name="emitter" value="1">
        <div class="xamzo-block xamzo-block-full">
            <table class="table hack-table table-border" data-id="spredsheet" cellpadding="0" cellspacing="0">
                <tr>
                     <th class="text-center">3</th>
                     <td>&nbsp;</td> 
                     <td colspan="2" class="text-right"><span style="background: #FFFFA5;width:20px;height:20px;float:right"></span> <b>keterangan</b>&nbsp; </td>
                    <td colspan="13">* Isilah Kolom berwarna kuning</td> 
                </tr>
                <tr>
                    <th class="text-center">2</thd>
                    <td colspan="15" class="text-center">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">3</th>
                    <td>&nbsp;</td> 
                    <td class="text-right" colspan="2" style="min-width: 250px">Jenis SDMK</td> 
                    <td colspan="13">
                        <?php echo isset($id) ? '<span class="enable-dd"></span>' : '' ?> 
                        <span style="float: left;width:150px;margin-top: 3px">
                            <?php
                            echo form_dropdown($jenis_sdmk, isset($default['no_jenis_sdmk']) ? $default['no_jenis_sdmk'] : '', array('name' => 'no_jenis_sdmk', 'class' => 'combobox', 'validation' => 'required', 'placeholder' => 'jenis-sdmk', 'style' => 'width:50px'));
                            ?> 
                            <?php echo isset($errors['no_jenis_sdmk']) ? $errors['no_jenis_sdmk'] : ''; ?>
                        </span>
                    </td> 
                </tr>
                <tr> 
                    <th class="text-center">4</th>
                    <td>&nbsp;</td> 
                    <td class="text-right" colspan="2">Kode Propinsi</td>
                    <td colspan="13">
                        <?php echo isset($id) ? '<span class="enable-dd"></span>' : '' ?> 
                        <span style="float: left;width:150px;margin-top: 3px">
                            <?php
                            echo form_dropdown($propinsi, isset($default['no_prov']) ? $default['no_prov'] : '', array('name' => 'no_prov', 'class' => 'combobox', 'validation' => 'required', 'placeholder' => 'kode-propinsi', 'style' => 'width:100px'));
                            ?> 
                            <?php echo isset($errors['no_prov']) ? $errors['no_prov'] : ''; ?>
                        </span>
                    </td> 
                </tr> 
                <tr>
                    <th class="text-center">5</th>
                    <td colspan="16" class="text-center">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">6</th>
                    <td colspan="16" class="text-center">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">7</th>
                    <td colspan="16" class="text-center">&nbsp;</td>
                </tr>
                <tr class="spredsheet-th-fix"> 
                    <th class="text-center">8</th>
                    <th>A</th>
                    <th>B</th>
                    <th>C</th>
                    <th>D</th>
                    <th>E</th>
                    <th>F</th>
                    <th>G</th>
                    <th>H</th>
                    <th>I</th>
                    <th>J</th>
                    <th>K</th>
                    <th>L</th>
                    <th>M</th>
                    <th>N</th>
                    <th>O</th>  
                </tr>  
                <tr>
                    <th class="text-center">9</th>
                    <td class="text-center">1</td>
                    <td colspan="7" class="text-center"><b>Proyeksi kebutuhan tenaga <span class="ref_jenis_sdmk">(...)</span> 2014-2025</b></td> 
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">10</th>
                    <td class="text-center">2</td>
                    <td class="text-right" colspan="2">Asumsi yang digunakan</td> 
                    <td colspan="12">&nbsp;</td> 
                </tr>
                <tr>
                    <th class="text-center">11</th>
                    <td class="text-center">3</td>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="3">Angka Pertumbuhan Penduduk</td> 
                    <td><input type="text" class="no-border mark color" name="ratio" validation="required" value="<?php echo isset($default['ratio']) ? $default['ratio'] : '0.75' ?>"></td>
                    <td>%/th</td> 
                    <td colspan="5">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">12</th>
                    <td class="text-center">4</td>
                    <td class="text-right" colspan="2">Pegawai Masuk</td> 
                    <td colspan="12">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">13</th>
                    <td class="text-center">5</td>
                    <td class="text-right" colspan="2">1</td>
                    <td colspan="3">Pengangkatan Baru</td> 
                    <td><input type="text" class="no-border color" name="pengangkatan_baru" validation="required" value="<?php echo isset($default['pengangkatan_baru']) ? $default['pengangkatan_baru'] : '6.81' ?>"></td>
                    <td>%/th</td>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">14</th>
                    <td class="text-center">6</td>
                    <td class="text-right" colspan="2">2</td>
                    <td colspan="3">Pindah Masuk</td> 
                    <td><input type="text" class="no-border color" name="pindah_masuk" validation="required" value="<?php echo isset($default['pindah_masuk']) ? $default['pindah_masuk'] : '0.50' ?>"></td>
                    <td>%/th</td>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">15</th>
                    <td class="text-center">7</td>
                    <td class="text-right" colspan="2">Pegawai Keluar</td> 
                    <td colspan="12">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">16</th>
                    <td class="text-center">8</td>
                    <td class="text-right" colspan="2">1</td>
                    <td colspan="3">Pensiun</td> 
                    <td><input type="text" class="no-border color" name="pensiun" validation="required" value="<?php echo isset($default['pensiun']) ? $default['pensiun'] : '1.00' ?>"></td>
                    <td>%/th</td>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">17</th>
                    <td class="text-center">9</td>
                    <td class="text-right" colspan="2">2</td>
                    <td colspan="3">Meninggal dan tidak mampu bekerja karena sakit / cacat</td> 
                    <td><input type="text" class="no-border color" name="meninggal_tdkmampu_cacat" validation="required" value="<?php echo isset($default['meninggal_tdkmampu_cacat']) ? $default['meninggal_tdkmampu_cacat'] : '0.50' ?>"></td>
                    <td>%/th</td>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">18</th>
                    <td class="text-center">10</td>
                    <td class="text-right" colspan="2">3</td>
                    <td colspan="3">Keluar, cuti diluar tanggungan negara , dipecat</td> 
                    <td><input type="text" class="no-border color" name="keluar_cuti_dipecat" validation="required" value="<?php echo isset($default['keluar_cuti_dipecat']) ? $default['keluar_cuti_dipecat'] : '1.00' ?>"></td>
                    <td>%/th</td>
                    <td colspan="7">&nbsp;</td>
                </tr>
                <tr>
                    <th class="text-center">19</th>
                    <td colspan="16">&nbsp;</td>
                </tr> 
                <tr class="text-center">
                    <th class="text-center">20</th>
                    <td>12</td>
                    <th colspan="2">Tahun</th>
                    <th>2014</th>
                    <th>2015</th>
                    <th>2016</th>
                    <th>2017</th>
                    <th>2018</th>
                    <th>2019</th>
                    <th>2020</th>
                    <th>2021</th>
                    <th>2022</th>
                    <th>2023</th>
                    <th>2024</th>
                    <th>2025</th> 
                </tr>
                <?php
                $t = 21;
                $no = 13;
                foreach ($ref_proyeksi as $rp) {
                    if (isset($id)) {
                        $tpr = $this->model_proyeksi->get_tahun_proyeksi($id, $rp->kode_ref_proyeksi);
                    }
                    ?>
                    <tr>
                        <th class="text-center"><?php echo $t ?></th>
                        <td id="A<?php echo $t ?>" class="text-center"><?php echo $no ?></td>
                        <td colspan="2" id="C<?php echo $t ?>"><?php echo $rp->nama_ref_proyeksi ?> <?php echo ($rp->kode_ref_proyeksi == 4) ? '<span class="ref_jenis_sdmk">(...)</span> di awal th (saat ini)' : '' ?></td> 
                        <td id="D<?php echo $t ?>"><?php echo (!in_array($rp->kode_ref_proyeksi, array(1, 2, 4))) ? '<input readonly="true" type="text" class="round no-border mask">' : '' ?><input value="<?php echo isset($tpr) ? $tpr->th_2014 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(3, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2014[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="E<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2015 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2015[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="F<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2016 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2016[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="G<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2017 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2017[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="H<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2018 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2018[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="I<?php echo $t ?>"><?php echo (!in_array($rp->kode_ref_proyeksi, array(1,2))) ? '<input readonly="true" type="text" class="round no-border mask">' : '' ?><input value="<?php echo isset($tpr) ? $tpr->th_2019 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2019[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="J<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2020 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2020[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="K<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2021 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2021[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="L<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2022 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2022[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="M<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2023 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2023[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="N<?php echo $t ?>"><input readonly="true" type="text" class="round no-border mask"><input value="<?php echo isset($tpr) ? $tpr->th_2024 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2024[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                        <td id="O<?php echo $t ?>"><?php echo (!in_array($rp->kode_ref_proyeksi, array(1, 2))) ? '<input readonly="true" type="text" class="round no-border mask">' : '' ?><input value="<?php echo isset($tpr) ? $tpr->th_2025 : '0' ?>" <?php echo (in_array($rp->kode_ref_proyeksi, array(3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13))) ? 'readonly="true"' : '' ?> type="text" class="no-border mask float" name="th_2025[<?php echo $rp->kode_ref_proyeksi ?>]" ></td>  
                    </tr>
                    <?php
                    $t++;
                    $no++;
                }
                ?> 
            </table>
        </div> 
    </form> 
</div> 
<script type="text/javascript">
    $('.combobox').combobox();
    $(document).ready(function () {
        $('select[name="no_jenis_sdmk"]').combobox({
            selected: function () {
                var filter = ($(this).find(':selected').html());
                var no_jenis_sdmk = $(this).find(':selected').val();
                var no_prov = $('select[name="no_prov"] :selected').val();
                if (no_prov !== '') {
                    _json(no_jenis_sdmk, no_prov);
                }
                sw(filter);
            }
        });

        $('select[name="no_prov"]').combobox({
            selected: function () {
                var no_prov = $(this).find(':selected').val();
                var no_jenis_sdmk = $('select[name="no_jenis_sdmk"] :selected').val();
                if (no_jenis_sdmk !== '') {
                    _json(no_jenis_sdmk, no_prov);
                }
            }
        });

        function _json(no_jenis_sdmk, no_prov) {
            $.ajax({
                type: 'POST',
                beforeSend: function () {
                    loading();
                },
                url: '<?php echo site_url('proyeksi/json_') ?>',
                data: 'no_jenis_sdmk=' + no_jenis_sdmk + '&no_prov=' + no_prov,
                success: function (rs) {
                    if (rs.length !== 0) {
                        $('form#crud-form input[name="id"]').val(rs.id_proyeksi);
                        $('input[name="ratio"]').val(rs.ratio);
                        $('input[name="pengangkatan_baru"]').val(rs.pengangkatan_baru);
                        $('input[name="pindah_masuk"]').val(rs.pindah_masuk);
                        $('input[name="pensiun"]').val(rs.pensiun);
                        $('input[name="meninggal_tdkmampu_cacat"]').val(rs.meninggal_tdkmampu_cacat);
                        $('input[name="keluar_cuti_dipecat"]').val(rs.keluar_cuti_dipecat);

                        $('td#D21 input').val(rs.D21);
                        $('td#E21 input').val(rs.E21);
                        $('td#D22 input').val(rs.D22);
                        $('td#J21 input').val(rs.J21);
                        $('td#I22 input').val(rs.I22);
                        $('td#O21 input').val(rs.O21);
                        $('td#O22 input').val(rs.O22);
                        $('td#D23 input').val(rs.D23);
                        $('td#D24 input').val(rs.D24);
                        $('td#D25 input').val(rs.D25);
                        $('td#D26 input').val(rs.D26);
                        $('td#D27 input').val(rs.D27);
                        $('button[ajax="true"]').attr('href', 'proyeksi/update');
                    } else {
                        $('form#crud-form input[name="id"]').val(0);
                        $('input[name="ratio"]').val(0);
                        $('input[name="pengangkatan_baru"]').val(0);
                        $('input[name="pindah_masuk"]').val(0);
                        $('input[name="pensiun"]').val(0);
                        $('input[name="meninggal_tdkmampu_cacat"]').val(0);
                        $('input[name="keluar_cuti_dipecat"]').val(0);

                        //$('td#D21 input').val(0);
                        $('td#D22 input').val(0);
                        $('td#I21 input').val(0);
                        $('td#I22 input').val(0);
                        $('td#O21 input').val(0);
                        $('td#O22 input').val(0);
                        $('td#D23 input').val(0);
                        $('td#D24 input').val(0);
                        $('td#D25 input').val(0);
                        $('td#D26 input').val(0);
                        $('td#D27 input').val(0);
                        $('button[ajax="true"]').attr('href', 'proyeksi/create');
                    }
                    close_box();
                }
            });
        }

        sw($('select[name="no_jenis_sdmk"] :selected').html());
        function sw(txt) {
            var txts = txt.split('-');
            $('span.ref_jenis_sdmk').html('(' + txts['1'] + ')');
        }
        var D = [];var _D = [];
        var E = [];var _E = [];
        var F = [];var _F = [];
        var G = [];var _G = [];
        var H = [];var _H = [];
        var I = [];var _I = [];
        var J = [];var _J = [];
        var K = [];var _K = [];
        var L = [];var _L = [];
        var M = [];var _M = [];
        var N = [];var _N = [];
        var O = [];var _O = [];
        for (var i = 21; i < 34; i++) {
            D[i] = $('td#D' + i + ' input.float');
            E[i] = $('td#E' + i + ' input.float');
            F[i] = $('td#F' + i + ' input.float');
            G[i] = $('td#G' + i + ' input.float');
            H[i] = $('td#H' + i + ' input.float');
            I[i] = $('td#I' + i + ' input.float');
            J[i] = $('td#J' + i + ' input.float');
            K[i] = $('td#K' + i + ' input.float');
            L[i] = $('td#L' + i + ' input.float');
            M[i] = $('td#M' + i + ' input.float');
            N[i] = $('td#N' + i + ' input.float');
            O[i] = $('td#O' + i + ' input.float');
            // round
            _D[i] = $('td#D' + i + ' input.round');
            _E[i] = $('td#E' + i + ' input.round');
            _F[i] = $('td#F' + i + ' input.round');
            _G[i] = $('td#G' + i + ' input.round');
            _H[i] = $('td#H' + i + ' input.round');
            _I[i] = $('td#I' + i + ' input.round');
            _J[i] = $('td#J' + i + ' input.round');
            _K[i] = $('td#K' + i + ' input.round');
            _L[i] = $('td#L' + i + ' input.round');
            _M[i] = $('td#M' + i + ' input.round');
            _N[i] = $('td#N' + i + ' input.round');
            _O[i] = $('td#O' + i + ' input.round');
        }
        setInterval(function () {
            //21
            ratio_penduduk = (parseFloat($('input[name="ratio"]').val()) / parseInt(100)); 
             
            var D21 = parseInt(D[21].val());
            var I21 = parseInt(I[21].val());

            E[21].val(D21 + (D21 * parseFloat(ratio_penduduk)));
            F[21].val(parseFloat(E[21].val()) + (parseFloat(E[21].val()) * parseFloat(ratio_penduduk)));
            G[21].val(parseFloat(F[21].val()) + (parseFloat(F[21].val()) * parseFloat(ratio_penduduk)));
            H[21].val(parseFloat(G[21].val()) + (parseFloat(G[21].val()) * parseFloat(ratio_penduduk))); 
            
            J[21].val(I21 + (I21 * parseFloat(ratio_penduduk)));
            K[21].val(parseFloat(J[21].val()) + (parseFloat(J[21].val()) * parseFloat(ratio_penduduk)));
            L[21].val(parseFloat(K[21].val()) + (parseFloat(K[21].val()) * parseFloat(ratio_penduduk)));
            M[21].val(parseFloat(L[21].val()) + (parseFloat(L[21].val()) * parseFloat(ratio_penduduk)));
            N[21].val(parseFloat(M[21].val()) + (parseFloat(M[21].val()) * parseFloat(ratio_penduduk))); 

            //22
            E[22].val(parseFloat(D[22].val()) + ((parseFloat(I[22].val()) - parseFloat(D[22].val())) / 5));
            F[22].val(parseFloat(E[22].val()) + ((parseFloat(I[22].val()) - parseFloat(D[22].val())) / 5));
            G[22].val(parseFloat(F[22].val()) + ((parseFloat(I[22].val()) - parseFloat(D[22].val())) / 5));
            H[22].val(parseFloat(G[22].val()) + ((parseFloat(I[22].val()) - parseFloat(D[22].val())) / 5));
            //
            J[22].val(parseFloat(I[22].val()) + ((parseFloat(O[22].val()) - parseFloat(I[22].val())) / 6));
            K[22].val(parseFloat(J[22].val()) + ((parseFloat(O[22].val()) - parseFloat(I[22].val())) / 6));
            L[22].val(parseFloat(K[22].val()) + ((parseFloat(O[22].val()) - parseFloat(I[22].val())) / 6));
            M[22].val(parseFloat(L[22].val()) + ((parseFloat(O[22].val()) - parseFloat(I[22].val())) / 6));
            N[22].val(parseFloat(M[22].val()) + ((parseFloat(O[22].val()) - parseFloat(I[22].val())) / 6));
            //23
            D[23].val((parseFloat(D[22].val()) / parseInt(100000)) * parseInt(D[21].val()));
            E[23].val((parseFloat(E[22].val()) / parseInt(100000)) * parseInt(E[21].val()));
            F[23].val((parseFloat(F[22].val()) / parseInt(100000)) * parseInt(F[21].val()));
            G[23].val((parseFloat(G[22].val()) / parseInt(100000)) * parseInt(G[21].val()));
            H[23].val((parseFloat(H[22].val()) / parseInt(100000)) * parseInt(H[21].val()));
            I[23].val((parseFloat(I[22].val()) / parseInt(100000)) * parseInt(I[21].val()));
            J[23].val((parseFloat(J[22].val()) / parseInt(100000)) * parseInt(J[21].val()));
            K[23].val((parseFloat(K[22].val()) / parseInt(100000)) * parseInt(K[21].val()));
            L[23].val((parseFloat(L[22].val()) / parseInt(100000)) * parseInt(L[21].val()));
            M[23].val((parseFloat(M[22].val()) / parseInt(100000)) * parseInt(M[21].val()));
            N[23].val((parseFloat(N[22].val()) / parseInt(100000)) * parseInt(N[21].val()));
            O[23].val((parseFloat(O[22].val()) / parseInt(100000)) * parseInt(O[21].val()));
            //25
            ratio_pengangkatan_baru = (parseFloat($('input[name="pengangkatan_baru"]').val()) / parseInt(100));
            D[25].val(ratio_pengangkatan_baru * (parseInt(D[24].val())));
            E[25].val(ratio_pengangkatan_baru * (parseInt(E[24].val())));
            F[25].val(ratio_pengangkatan_baru * (parseInt(F[24].val())));
            G[25].val(ratio_pengangkatan_baru * (parseInt(G[23].val())));
            H[25].val(ratio_pengangkatan_baru * (parseInt(H[24].val())));
            I[25].val(ratio_pengangkatan_baru * (parseInt(I[24].val())));
            J[25].val(ratio_pengangkatan_baru * (parseInt(J[24].val())));
            K[25].val(ratio_pengangkatan_baru * (parseInt(K[24].val())));
            L[25].val(ratio_pengangkatan_baru * (parseInt(L[24].val())));
            M[25].val(ratio_pengangkatan_baru * (parseInt(M[24].val())));
            N[25].val(ratio_pengangkatan_baru * (parseInt(N[24].val())));
            O[25].val(ratio_pengangkatan_baru * (parseInt(O[24].val())));


            //26
            ratio_pindah_masuk = (parseFloat($('input[name="pindah_masuk"]').val()) / parseInt(100));
            D[26].val(ratio_pindah_masuk * (parseInt(D[24].val())));
            E[26].val(ratio_pindah_masuk * (parseInt(E[24].val())));
            F[26].val(ratio_pindah_masuk * (parseInt(F[24].val())));
            G[26].val(ratio_pindah_masuk * (parseInt(G[24].val())));
            H[26].val(ratio_pindah_masuk * (parseInt(H[24].val())));
            I[26].val(ratio_pindah_masuk * (parseInt(I[24].val())));
            J[26].val(ratio_pindah_masuk * (parseInt(J[24].val())));
            K[26].val(ratio_pindah_masuk * (parseInt(K[24].val())));
            L[26].val(ratio_pindah_masuk * (parseInt(L[24].val())));
            M[26].val(ratio_pindah_masuk * (parseInt(M[24].val())));
            N[26].val(ratio_pindah_masuk * (parseInt(N[24].val())));
            O[26].val(ratio_pindah_masuk * (parseInt(O[24].val())));
            //27
            D[27].val(parseFloat(D[25].val()) + parseFloat(D[26].val()));
            E[27].val(parseFloat(E[25].val()) + parseFloat(E[26].val()));
            F[27].val(parseFloat(F[25].val()) + parseFloat(F[26].val()));
            G[27].val(parseFloat(G[25].val()) + parseFloat(G[26].val()));
            H[27].val(parseFloat(H[25].val()) + parseFloat(H[26].val()));
            I[27].val(parseFloat(I[25].val()) + parseFloat(I[26].val()));
            J[27].val(parseFloat(J[25].val()) + parseFloat(J[26].val()));
            K[27].val(parseFloat(K[25].val()) + parseFloat(K[26].val()));
            L[27].val(parseFloat(L[25].val()) + parseFloat(L[26].val()));
            M[27].val(parseFloat(M[25].val()) + parseFloat(M[26].val()));
            N[27].val(parseFloat(N[25].val()) + parseFloat(N[26].val()));
            O[27].val(parseFloat(O[25].val()) + parseFloat(O[26].val()));

            //28
            ratio_pensiun = (parseFloat($('input[name="pensiun"]').val()) / parseInt(100));
            D[28].val(ratio_pensiun * (parseInt(D[24].val())));
            E[28].val(ratio_pensiun * (parseInt(E[24].val())));
            F[28].val(ratio_pensiun * (parseInt(F[24].val())));
            G[28].val(ratio_pensiun * (parseInt(G[24].val())));
            H[28].val(ratio_pensiun * (parseInt(H[24].val())));
            I[28].val(ratio_pensiun * (parseInt(I[24].val())));
            J[28].val(ratio_pensiun * (parseInt(J[24].val())));
            K[28].val(ratio_pensiun * (parseInt(K[24].val())));
            L[28].val(ratio_pensiun * (parseInt(L[24].val())));
            M[28].val(ratio_pensiun * (parseInt(M[24].val())));
            N[28].val(ratio_pensiun * (parseInt(N[24].val())));
            O[28].val(ratio_pensiun * (parseInt(O[24].val())));

            //29
            ratio_meninggal_tdkmampu_cacat = (parseFloat($('input[name="meninggal_tdkmampu_cacat"]').val()) / parseInt(100));
            D[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(D[24].val())));
            E[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(E[24].val())));
            F[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(F[24].val())));
            G[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(G[24].val())));
            H[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(H[24].val())));
            I[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(I[24].val())));
            J[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(J[24].val())));
            K[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(K[24].val())));
            L[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(L[24].val())));
            M[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(M[24].val())));
            N[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(N[24].val())));
            O[29].val(ratio_meninggal_tdkmampu_cacat * (parseInt(O[24].val())));
            //30
            ratio_keluar_cuti_dipecat = (parseFloat($('input[name="keluar_cuti_dipecat"]').val()) / parseInt(100));
            D[30].val(ratio_keluar_cuti_dipecat * (parseInt(D[24].val())));
            E[30].val(ratio_keluar_cuti_dipecat * (parseInt(E[24].val())));
            F[30].val(ratio_keluar_cuti_dipecat * (parseInt(F[24].val())));
            G[30].val(ratio_keluar_cuti_dipecat * (parseInt(G[24].val())));
            H[30].val(ratio_keluar_cuti_dipecat * (parseInt(H[24].val())));
            I[30].val(ratio_keluar_cuti_dipecat * (parseInt(I[24].val())));
            J[30].val(ratio_keluar_cuti_dipecat * (parseInt(J[24].val())));
            K[30].val(ratio_keluar_cuti_dipecat * (parseInt(K[24].val())));
            L[30].val(ratio_keluar_cuti_dipecat * (parseInt(L[24].val())));
            M[30].val(ratio_keluar_cuti_dipecat * (parseInt(M[24].val())));
            N[30].val(ratio_keluar_cuti_dipecat * (parseInt(N[24].val())));
            O[30].val(ratio_keluar_cuti_dipecat * (parseInt(O[24].val())));

            //31 
            D[31].val(parseFloat(D[28].val()) + parseFloat(D[29].val()) + parseFloat(D[30].val()));
            E[31].val(parseFloat(E[28].val()) + parseFloat(E[29].val()) + parseFloat(E[30].val()));
            F[31].val(parseFloat(F[28].val()) + parseFloat(F[29].val()) + parseFloat(F[30].val()));
            G[31].val(parseFloat(G[28].val()) + parseFloat(G[29].val()) + parseFloat(G[30].val()));
            H[31].val(parseFloat(H[28].val()) + parseFloat(H[29].val()) + parseFloat(H[30].val()));
            I[31].val(parseFloat(I[28].val()) + parseFloat(I[29].val()) + parseFloat(I[30].val()));
            J[31].val(parseFloat(J[28].val()) + parseFloat(J[29].val()) + parseFloat(J[30].val()));
            K[31].val(parseFloat(K[28].val()) + parseFloat(K[29].val()) + parseFloat(K[30].val()));
            L[31].val(parseFloat(L[28].val()) + parseFloat(L[29].val()) + parseFloat(L[30].val()));
            M[31].val(parseFloat(M[28].val()) + parseFloat(M[29].val()) + parseFloat(M[30].val()));
            N[31].val(parseFloat(N[28].val()) + parseFloat(N[29].val()) + parseFloat(N[30].val()));
            O[31].val(parseFloat(O[28].val()) + parseFloat(O[29].val()) + parseFloat(O[30].val()));
            //32  
            var D32 = parseFloat(D[24].val()) + (parseFloat(D[27].val()) - parseFloat(D[31].val()));
            var E32 = parseFloat(E[24].val()) + (parseFloat(E[27].val()) - parseFloat(E[31].val()));
            var F32 = parseFloat(F[24].val()) + (parseFloat(F[27].val()) - parseFloat(F[31].val()));
            var G32 = parseFloat(G[24].val()) + (parseFloat(G[27].val()) - parseFloat(G[31].val()));
            var H32 = parseFloat(H[24].val()) + (parseFloat(H[27].val()) - parseFloat(H[31].val()));
            var I32 = parseFloat(I[24].val()) + (parseFloat(I[27].val()) - parseFloat(I[31].val()));
            var J32 = parseFloat(J[24].val()) + (parseFloat(J[27].val()) - parseFloat(J[31].val()));
            var K32 = parseFloat(K[24].val()) + (parseFloat(K[27].val()) - parseFloat(K[31].val()));
            var L32 = parseFloat(L[24].val()) + (parseFloat(L[27].val()) - parseFloat(L[31].val()));
            var M32 = parseFloat(M[24].val()) + (parseFloat(M[27].val()) - parseFloat(M[31].val()));
            var N32 = parseFloat(N[24].val()) + (parseFloat(N[27].val()) - parseFloat(N[31].val()));
            var O32 = parseFloat(O[24].val()) + (parseFloat(O[27].val()) - parseFloat(O[31].val()));

            D[32].val(D32);
            E[24].val(D32);

            E[32].val(E32);
            F[24].val(E32);

            F[32].val(F32);
            G[24].val(F32);

            G[32].val(G32);
            H[24].val(G32);

            H[32].val(H32);
            I[24].val(H32);

            I[32].val(I32);
            J[24].val(I32);

            J[32].val(J32);
            K[24].val(J32);

            K[32].val(K32);
            L[24].val(K32);

            L[32].val(L32);
            M[24].val(L32);

            M[32].val(M32);
            N[24].val(M32);

            N[32].val(N32);
            O[24].val(N32);

            O[32].val(O32);
            //33
            D[33].val(parseFloat(D[23].val()) - parseFloat(D[32].val()));
            E[33].val(parseFloat(E[23].val()) - E32);
            F[33].val(parseFloat(F[23].val()) - F32);
            G[33].val(parseFloat(G[23].val()) - G32);
            H[33].val(parseFloat(H[23].val()) - H32);
            I[33].val(parseFloat(I[23].val()) - I32);
            J[33].val(parseFloat(J[23].val()) - J32);
            K[33].val(parseFloat(K[23].val()) - K32);
            L[33].val(parseFloat(L[23].val()) - L32);
            M[33].val(parseFloat(M[23].val()) - M32);
            N[33].val(parseFloat(N[23].val()) - N32);
            O[33].val(parseFloat(O[23].val()) - O32);


        }, 1000);

        setInterval(function () {
            for (var k = 21; k < 34; k++) {
                _D[k].val(Math.round(D[k].val()));
                _E[k].val(Math.round(E[k].val()));
                _F[k].val(Math.round(F[k].val()));
                _G[k].val(Math.round(G[k].val()));
                _H[k].val(Math.round(H[k].val()));
                _I[k].val(Math.round(I[k].val()));
                _J[k].val(Math.round(J[k].val()));
                _K[k].val(Math.round(K[k].val()));
                _L[k].val(Math.round(L[k].val()));
                _M[k].val(Math.round(M[k].val()));
                _N[k].val(Math.round(N[k].val()));
                _O[k].val(Math.round(O[k].val()));
                switch (k) {
                    case 22:
                        E[k].val(Math.round(E[k].val()));
                        F[k].val(Math.round(F[k].val()));
                        G[k].val(Math.round(G[k].val()));
                        H[k].val(Math.round(H[k].val()));
                        J[k].val(Math.round(J[k].val()));
                        K[k].val(Math.round(K[k].val()));
                        L[k].val(Math.round(L[k].val()));
                        M[k].val(Math.round(M[k].val()));
                        N[k].val(Math.round(N[k].val()));
                        break; 
                    default:
                        break;
                }
            }
        }, 999);
    });
</script>