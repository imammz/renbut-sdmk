<?php

session_start();

error_reporting(E_ALL);
$system_path = 'system'; 
$application_path = 'application';
$backup_path = 'backup';
$upload_path = 'upload';
$template_path = 'application/templates';
$ext_path = 'application/extensions';
$modules_path = 'application/modules';
$widgets_path = 'application/widgets';
$report_path = 'report';
/*
 * ---------------------------------------------------------------
 *  Resolve the system path for increased reliability
 * ---------------------------------------------------------------
 */

// Set the current directory correctly for CLI requests
if (defined('STDIN')) {
    chdir(dirname(__FILE__));
}


if (realpath($system_path) !== FALSE) {
    $system_path = realpath($system_path) . '/';
}

// ensure there's a trailing slash
$system_path = rtrim($system_path, '/') . '/';

// Is the system path correct?
if (!is_dir($system_path)) {
    exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: " . pathinfo(__FILE__, PATHINFO_BASENAME));
}

/*
 * -------------------------------------------------------------------
 *  Now that we know the path, set the main path constants
 * -------------------------------------------------------------------
 */
// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

define('JSON', '.json');

// The PHP file extension
// this global constant is deprecated.
define('EXT', '.php');

// Path to the system folder
define('BASEPATH', str_replace("\\", "/", $system_path));

// Path to the front controller (this file)
define('FCPATH', str_replace(SELF, '', __FILE__));

// Name of the "system folder"
define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));


// The path to the "application" folder
if (is_dir($application_path)) {
    define('APPPATH', $application_path . '/');
} else {
    if (!is_dir(BASEPATH . $application_path . '/')) {
        exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }

    define('APPPATH', BASEPATH . $application_path . '/');
}


// The path to the "application" folder
if (is_dir($upload_path)) {
    define('UPLOADPATH', $upload_path . '/');
} else {
    if (!is_dir(UPLOADPATH . $upload_path . '/')) {
        exit("Your upload folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }
    define('UPLOADPATH', UPLOADPATH . $upload_path . '/');
}



// backup directory 
if (is_dir($backup_path)) {
    define('BACKUP', $backup_path . '/');
} else {
    if (!is_dir(BASEPATH . $backup_path . '/')) {
        exit("Your backup folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }

    define('APPPATH', BASEPATH . $nodejs_path . '/');
}

// The path to the "application" folder
if (is_dir($modules_path)) {
    define('MODPATH', $modules_path . '/');
} else {
    if (!is_dir(BASEPATH . $modules_path . '/')) {
        exit("Your module folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }

    define('MODPATH', BASEPATH . $modules_path . '/');
}


// The path to the "application" folder
if (is_dir($report_path)) {
    define('REPORTPATH', $report_path . '/');
} else {
    if (!is_dir(BASEPATH . $report_path . '/')) {
        exit("Your report folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }

    define('REPORTPATH', BASEPATH . $report_path . '/');
}

// The path to the "application" folder
if (is_dir($ext_path)) {
    define('EXTPATH', $ext_path . '/');
} else {
    if (!is_dir(EXTPATH . $ext_path . '/')) {
        exit("Your extensions folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }
    define('EXTPATH', EXTPATH . $ext_path . '/');
}

// The path to the "application" folder
if (is_dir($template_path)) {
    define('THEMEPATH', $template_path . '/');
} else {
    if (!is_dir(BASEPATH . $template_path . '/')) {
        exit("Your theme folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }
    define('THEMEPATH', BASEPATH . $template_path . '/');
}

// The path to the "application" folder
if (is_dir($widgets_path)) {
    define('WIPATH', $widgets_path . '/');
} else {
    if (!is_dir(BASEPATH . $widgets_path . '/')) {
        exit("Your widget folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }
    define('WIPATH', BASEPATH . $widgets_path . '/');
} 


$base = DIRNAME($_SERVER['PHP_SELF']);
if (is_dir(EXTPATH . 'installer') || !is_file('.htaccess') || !is_file(APPPATH . '.htaccess')) {
    include_once EXTPATH . 'installer/helpers/installer_helper' . EXT;
    create_htaccess($base);
    create_htaccess($base, APPPATH);
    general_setting($base . '/');
}
require_once BASEPATH . 'xamzo/X_Common' . EXT;
