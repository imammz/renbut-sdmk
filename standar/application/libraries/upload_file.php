<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class upload_file {

    function doUpload($filename)
    {

        $type = pathinfo($_FILES[$filename]['name'], PATHINFO_EXTENSION);
        $name = $_FILES[$filename]['name'];
        $size = $_FILES[$filename]['size'];
        $tmp  = $_FILES[$filename]['tmp_name'];
        $rand = mt_rand(1000,9999);
        if(trim($name)!=''){
            $path = ''.strtoupper($type).'-'.date('ymdhis').'-'.$rand.'.'.$type;
            if(move_uploaded_file($tmp,'upload/file/'.$path)){
                
                return $path;
            }
            return false;
        }

    } 

    function upload_multiple_file($params)
    {

        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);

        foreach($_FILES[''.$params['filename'].'']['name'] as $key=>$image){

            $type = pathinfo($_FILES[''.$params['filename'].'']['name'][$key], PATHINFO_EXTENSION); //print_r($type);die;
            $name = $_FILES[''.$params['filename'].'']['name'][$key];
            $size = $_FILES[''.$params['filename'].'']['size'][$key];
            $tmp  = $_FILES[''.$params['filename'].'']['tmp_name'][$key];
            $rand = mt_rand(1000,9999);
            $random           = rand(1,99999);
            $file_name_unique = ''.$rand.'_'.$name.'';

            if(trim($name)!=''){
                //$path = ''.strtoupper($type).'-'.date('ymdhis').'-'.$rand.'.'.$type;
                if(move_uploaded_file($tmp,'uploaded_files/file/'.$params['path_folder'].'/'.$file_name_unique)){
                    
                    $insert = array(
                            'attachment_name' => $file_name_unique,
                            'attachment_path' => $params['path_folder'],
                            'attachment_refer_id' => $params['refer_id'],
                            'attachment_refer_table' => $params['refer_table'],
                            'attachment_type' => $params['type'],
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => $this->session->userdata('data_user')->id_user,
                    );
                    $this->db->insert('t_attachment', $insert);

                }

            }

        }

        return true;

    } 
    
    public function upload_single_file($params){

    //direktori file
    $CI =&get_instance();
    $db = $CI->load->database('default', TRUE);

      $lokasi_file    = $_FILES[''.$params['filename'].'']['tmp_name'];
      $tipe_file      = $_FILES[''.$params['filename'].'']['type'];
      $nama_file      = $_FILES[''.$params['filename'].'']['name'];
      $random           = rand(1,99999);
      $file_name_unique = ''.$random.'_'.$nama_file.'';

      $vdir_upload = "uploaded_files/file/".$params['path_folder']."/";

      $vfile_upload = $vdir_upload . $file_name_unique;

      $tipe_file   = $_FILES[''.$params['filename'].'']['type'];

      //Simpan gambar dalam ukuran sebenarnya

      if(move_uploaded_file($_FILES[''.$params['filename'].'']["tmp_name"], $vfile_upload)) { 

        return $file_name_unique;

      }else{

        return false;

      }

    }

    public function get_attachment_name($refer_id, $refer_table){

    $CI =&get_instance();
    $db = $CI->load->database('default', TRUE);

    $attachment_name = $db->get_where('t_attachment', array('attachment_refer_id'=>$refer_id, 'attachment_refer_table'=>$refer_table));

        if($attachment_name->num_rows() > 0){

            return $attachment_name->row()->attachment_name;
            
        }else{
            return false;
        }

    }

    public function get_all_attachment($refer_id, $refer_table, $type){

    $CI =&get_instance();
    $db = $CI->load->database('default', TRUE);

    $result = $db->get_where('t_attachment', array('attachment_refer_id'=>$refer_id, 'attachment_refer_table'=>$refer_table, 'attachment_type'=>$type))->result();

    return $result;

    }

    public function delete_attachment($refer_id, $refer_table, $type){

    $CI =&get_instance();
    $db = $CI->load->database('default', TRUE);

    $attachment = $db->get_where('t_attachment', array('attachment_refer_id'=>$refer_id, 'attachment_refer_table'=>$refer_table, 'attachment_type'=>$type))->result();

        if(!empty($attachment)){
            foreach ($attachment as $key => $value) {
                $path = "uploaded_files/file/$value->attachment_path/$value->attachment_name";
                if(unlink($path)){
                    $db->delete('t_attachment', array('attachment_id'=>$value->attachment_id));
                }
            }
        }

        return true;

    }

    public function delete_attachment_by_id($id){

    $CI =&get_instance();
    $db = $CI->load->database('default', TRUE);

    $attachment = $db->get_where('t_attachment', array('attachment_id'=>$id))->result();

        if(!empty($attachment)){
            foreach ($attachment as $key => $value) {
                $path = "uploaded_files/file/$value->attachment_path/$value->attachment_name";
                if(unlink($path)){
                    $db->delete('t_attachment', array('attachment_id'=>$value->attachment_id));
                }
            }
        }

        return true;

    }

}

?>
