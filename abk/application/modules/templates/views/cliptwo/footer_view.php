<!-- start: FOOTER -->
<footer>
    <div class="footer-inner">
        <div class="pull-left">
            &copy; <span class="current-year"></span> <span class="text-bold text-uppercase">PUSRENGUN BPPSDM KEMENKES RI -- AIPHSS</span>. 
        </div>
    </div>
</footer>
<!-- end: FOOTER -->