<?php echo Modules::run('templates/cliptwo/header'); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/gb/greybox.css" />



<!-- end: HEAD -->
<body>
    <!-- start: YOUR CONTENT HERE -->
    <div class="container-fluid container-fullw bg-white" style="min-height: 1900px;">

        <div class="row">

            <div class="col-md-12"> 
                <div class="row">
                    <div class="col-sm-2"> 
                        <a class="btn btn-danger btn-xs" href="<?php echo base_url('index.php/entry/uptdprov/data') ?>">
                            <i class="fa fa-refresh"></i>    Refresh Data
                        </a>
                    </div>
                    <div class="col-sm-4"> &nbsp; </div>

                    <div class="col-sm-6">
                        <form method="get" action="<?php echo base_url() ?>index.php/entry/Uptdprov/get/">

                            <select class="js-example-basic-single js-states form-control" name="faskes_kode" onchange="this.form.submit()" required>
                            <option value="">-- Cari UPTD Kab/Kota --</option>
                            <?php                            
                            foreach($master_Uptdprov as $row) {
                                $selected = '';
                                if(isset($faskes_kode)) {
                                    if($faskes_kode==$row['NO_KAB']) $selected = 'selected';
                                }
                                
                            ?>    
                            <option <?php echo $selected ?> value="<?php echo $row['NO_KAB'] ?>"> <?php echo $row['NAMA_KAB'].' Prov. '.$row['NAMA_PROV']?> </option>
                            <?php     
                            }
                            ?>
                        </select>
                        </form>

                    </div>
                </div>
                <br/>
                <?php if (count($data_faskes['provinsi']) == 0) { ?>
                    <div class="row">            
                        <div class="col-sm-12"> 
                            <a href="#" class="list-group-item list-group-item-danger" style="text-align: center">
                                <h3>  Data UPTD Kab/KotaTersebut Belum Dibuat </h3>
                                )* Tambahkan Data UPTD Kab/KotaPada Baris Kabupaten/Kecamatan Setempat Pada Tabel dibawah Ini. (<strong>Klik Refresh Data Terlebih Dahulu</strong>)
                            </a>   
                        </div>
                    </div>  
                <?php } ?>
                <table id="example-advanced">
                    <caption>
                        <a href="#" onclick="jQuery('#example-advanced').treetable('expandAll');
                                return false;">+ Buka Semua Data</a>
                        &nbsp; &nbsp;
                        <a href="#" onclick="jQuery('#example-advanced').treetable('collapseAll');
                                return false;">- Tutup Semua Data</a>
                    </caption>
                    <thead>
                        <tr>
                            <th>UPTD Kab/Kota</th>
                            <th width="30">EDIT</th>
                            <th width="80">Jumlah Saat ini <br/> (PNS)</th>
                            <th width="80">Jumlah <br/> Seharusnya</th>
                            <th width="120">AKSI</th>
                            <th width="30">HAPUS</th>
                            
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $no_prov = 1;
                        foreach ($data_faskes['provinsi'] as $row_prov) {
                            ?>   

                        <tr  data-tt-id='<?php echo $no_prov; ?>'><td><span class='folder' style="font-size: 18px;"> <strong> <?php echo $row_prov['NAMA_PROV'] ?> </strong> </span></td><td>-</td><td style="font-size: 14px;"> <?php echo $row_prov['jumlah_saat_ini'] ?> </td><td style="font-size: 14px;"> <?php echo $row_prov['hasil_kebutuhan_sdmk'] ?> </td><td>  </td><td></td></tr>
                            <?php
                            $no_kab = 1;
                            foreach ($row_prov['kabupaten'] as $row_kab) {
                                ?>
                        <tr data-tt-id='<?php echo $no_prov . '-' . $no_kab ?>' data-tt-parent-id='<?php echo $no_prov; ?>'><td><span class='folder' style="font-size: 17px;"> <?php echo $row_kab['NAMA_KAB'] ?></span></td><td>-</td><td style="font-size: 14px;"> <?php echo $row_kab['jumlah_saat_ini'] ?> </td><td style="font-size: 14px;"> <?php echo $row_kab['hasil_kebutuhan_sdmk'] ?> </td><td> <button class="btn btn-primary btn-xs" type="button" onclick="tambahFaskes('<?php echo $row_kab['NO_KAB'] ?>')"> <i class="fa fa-plus"></i> Daftarkan UPTD Kab/Kota</button> </td><td></td></tr>

                                <?php
                                $no_faskes = 1;
                                foreach ($row_kab['faskes'] as $row_faskes) {

                                    $selected = '';
                                    if (isset($faskes_kode)) {
                                        if ($faskes_kode == $row_faskes['faskes_kode'])
                                            $selected = 'class="selected"';
                                    }
                                    ?>
                        <tr <?php echo $selected; ?> data-tt-id='<?php echo $no_prov . '-' . $no_kab . '-' . $no_faskes ?>' data-tt-parent-id='<?php echo $no_prov . '-' . $no_kab ?>'><td><span class='folder' style="font-size: 14px;"> <strong> <?php echo $row_faskes['faskes_nama'] ?> </strong>  </span></td><td><small><a class="btn btn-yellow btn-xs" href="#" onclick="editFaskes('<?php echo $row_faskes['faskes_kode'] ?>')"><i class="fa fa-pencil"></i></a></small></td><td style="font-size: 14px;"> <?php echo $row_faskes['jumlah_saat_ini'] ?> </td><td style="font-size: 14px;"> <?php echo $row_faskes['hasil_kebutuhan_sdmk'] ?> </td><td> <button class="btn btn-dark-purple btn-xs" type="button" onclick="tambahBagian('<?php echo $row_faskes['faskes_kode'] ?>')"> <i class="fa fa-plus"></i> Tambah Bagian/Bidang </button> <br/><br/> <button class="btn btn-light-purple btn-xs" type="button" onclick="tambahSubBagian('<?php echo $row_faskes['faskes_kode'] ?>')"> <i class="fa fa-plus"></i> Tambah SubBagian/SubBidang </button> </td><td></td></tr>

                                    <?php
                                    $no_installasi = 1;
                                    foreach ($row_faskes['installasi'] as $row_installasi) {
                                        
                                         if($row_installasi['parent'] == 0) {
                                            $bg = '#99bcff';
                                        } //bagian
                                        
                                        if($row_installasi['eselon'] == 'IV') {
                                            $bg = '#dbdcff';
                                        } //subbagian
                                        
                                        ?>
                        <tr  data-tt-id='<?php echo $no_prov . '-' . $no_kab . '-' . $no_faskes . '-' . $no_installasi ?>' data-tt-parent-id='<?php echo $no_prov . '-' . $no_kab . '-' . $no_faskes ?>'><td style="background-color: <?php echo $bg; ?>;"><span class='folder' style="font-size: 14px;">  <?php echo $row_installasi['installasi_nama'] ?> </span>  </td><td><small><a class="btn btn-yellow btn-xs" href="#" onclick="editInstallasi('<?php echo $row_installasi['faskes_installasi_id'] ?>')"><i class="fa fa-pencil"></i></a></small></td><td style="font-size: 14px;"> <?php echo $row_installasi['jumlah_saat_ini'] ?> </td><td style="font-size: 14px;"> <?php echo $row_installasi['hasil_kebutuhan_sdmk'] ?> </td><td> <button class="btn btn-dark-green btn-xs" type="button" onclick="tambahSDMK('<?php echo $row_installasi['faskes_installasi_id'] ?>')"> <i class="fa fa-recycle"></i> Tambah SDMK </button> </td><td><small><a class="btn btn-danger btn-xs" onclick="hapusInstallasi('<?php echo $row_installasi['faskes_installasi_id'] ?>')" href="#"><i class="fa fa-trash"></i></a></small></td></tr>

                                        <?php
                                        $no_sdmk = 1;
                                        foreach ($row_installasi['sdmk'] as $row_sdmk) {
                                            ?>
                        <tr data-tt-id='<?php echo $no_prov . '-' . $no_kab . '-' . $no_faskes . '-' . $no_installasi . '-' . $no_sdmk ?>' data-tt-parent-id='<?php echo $no_prov . '-' . $no_kab . '-' . $no_faskes . '-' . $no_installasi ?>'><td style="font-size: 13px;"><span class='file'> <strong style="color: #00438a"><?php echo substr($row_sdmk['sdmk_faskes_installasi_nama'], 0, 80); ?></strong> </span></td><td><small><a class="btn btn-yellow btn-xs" href="#" onclick="editSDMK('<?php echo $row_sdmk['sdmk_faskes_installasi_id'] ?>')"><i class="fa fa-pencil"></i></a></small></td><td style="font-size: 14px;"> <?php echo $row_sdmk['jumlah_saat_ini'] ?> </td><td style="font-size: 14px;"> <?php echo $row_sdmk['hasil_kebutuhan_sdmk'] ?></td><td> <button class="btn btn-info btn-xs" type="button" onclick="detilABK('<?php echo $row_sdmk['sdmk_faskes_installasi_id'] ?>')"> <i class="fa fa-file-o"></i> Detil </button> <a class="btn btn-success btn-xs" target="_blank" type="button" name="CETAK" href="<?php echo base_url('index.php/entry/entry/detil_perhitungan_abk_excel/'.$row_sdmk['sdmk_faskes_installasi_id']) ?>"><i class="fa fa-file-excel-o"></i> Cetak </a> </td><td><small><a class="btn btn-danger btn-xs" onclick="hapusSDMK('<?php echo $row_sdmk['sdmk_faskes_installasi_id'] ?>')" href="#"><i class="fa fa-trash"></i></a></small></td></tr>

                                            <?php $no_sdmk++;
                                        }
                                        ?> 
                                        <?php $no_installasi++;
                                    }
                                    ?> 

                                    <?php $no_faskes++;
                                }
                                ?>   

                                <?php $no_kab++;
                            }
                            ?>    


    <?php $no_prov++;
}
?>
                    </tbody>
                </table>



            </div>
        </div>


        <!-- end: YOUR CONTENT HERE -->

    </div>


<?php echo Modules::run('templates/cliptwo/js'); ?>
    <script src="<?php echo base_url() ?>assets/library/tree/jquery.treetable.js"></script>
    <script src="<?php echo base_url() ?>assets/library/gb/greybox.js"></script>


    <!-- start: JavaScript Event Handlers for this page -->

    <script>
                                            jQuery(document).ready(function () {
                                                Main.init();
                                                jQuery('#example-advanced').treetable('expandAll');
                                                //jQuery('#example-advanced').treetable();
                                                $('.modal').modal({show: true});
                                                FormElements.init();

                                            });

    </script>

    <script>

        function tambahFaskes(NO_KAB) {
            GB_show("Tambah Data Faskes", '<?php echo base_url() ?>index.php/entry/uptdprov/tambahFaskes/' + NO_KAB, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function tambahBagian(faskes_kode) {
            GB_show("Tambah Data Bagian/Bidang", '<?php echo base_url() ?>index.php/entry/uptdprov/tambahBagian/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
		
		 function tambahSubBagian(faskes_kode) {
            GB_show("Tambah Data SubBidang / SubBagian", '<?php echo base_url() ?>index.php/entry/uptdprov/tambahSubBagian/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function editInstallasi(faskes_kode) {
            GB_show("Edit Data Bagian/Bidang - SubBidang/SubBagian", '<?php echo base_url() ?>index.php/entry/uptdprov/editInstallasi/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }

        function tambahSDMK(faskes_installasi_id) {
            GB_show("Hitung Data Kebutuhan SDMK", '<?php echo base_url() ?>index.php/entry/uptdprov/tambahSDMK/' + faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
         function hapusSDMK(sdmk_faskes_installasi_id) {
            GB_show("Hapus SDMK", '<?php echo base_url() ?>index.php/entry/uptdprov/hapusSDMK/' + sdmk_faskes_installasi_id, 300, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function hapusInstallasi(faskes_installasi_id) {
            GB_show("Hapus Installasi / Sub Installasi / Bagian / Sub bagian", '<?php echo base_url() ?>index.php/entry/uptdprov/hapusInstallasi/' + faskes_installasi_id, 300, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function detilABK(sdmk_faskes_installasi_id) {
            GB_show("Detil Data ABK", '<?php echo base_url() ?>index.php/entry/entry/detil_perhitungan_abk/' + sdmk_faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editFaskes(faskes_kode) {
            GB_show("Edit Data UPTD Kab/Kota", '<?php echo base_url('index.php/entry/uptdprov/editfaskes'); ?>/' + faskes_kode, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editInstallasi(installasi_id) {
            GB_show("Edit Data Bagian Bidang", '<?php echo base_url('index.php/entry/uptdprov/editinstallasi'); ?>/' + installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
        function editSDMK(sdmk_faskes_installasi_id) {
            GB_show("Edit Data SDMK", '<?php echo base_url('index.php/entry/uptdprov/editsdmk'); ?>/' + sdmk_faskes_installasi_id, 700, 900);
            $('html, body').animate({scrollTop: 0}, 'slow');
        }
        
     


    </script>


    <script>


        $("#example-advanced").treetable({expandable: true});



// Highlight selected row
        $("#example-advanced tbody").on("mousedown", "tr", function () {
            $(".selected").not(this).removeClass("selected");
            $(this).toggleClass("selected");
        });


        $("#example-advanced .folder").each(function () {
            $(this).parents("#example-advanced tr").droppable({
                accept: ".file, .folder",
                drop: function (e, ui) {
                    var droppedEl = ui.draggable.parents("tr");
                    $("#example-advanced").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
                },
                hoverClass: "accept",
                over: function (e, ui) {
                    var droppedEl = ui.draggable.parents("tr");
                    if (this != droppedEl[0] && !$(this).is(".expanded")) {
                        $("#example-advanced").treetable("expandNode", $(this).data("ttId"));
                    }
                }
            });
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
