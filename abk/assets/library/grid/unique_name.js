/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function addRow(tableID) {

    var table = document.getElementById(tableID);

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var colCount = table.rows[1].cells.length;

    for(var i=0; i<colCount; i++) {

        var newcell = row.insertCell(i);

        newcell.innerHTML = table.rows[1].cells[i].innerHTML;
        //alert(newcell.childNodes[0].type);
        switch(newcell.childNodes[0].type) {
            case "text":
                newcell.childNodes[0].value = "";
                break;
            case "checkbox":
                newcell.childNodes[0].checked = false;
                break;
            case "select-one":
                newcell.childNodes[0].selectedIndex = 0;
                break;
        }
        switch(newcell.childNodes[0].name) {
            
            case "hasil_scann[]":
                newcell.childNodes[0].setAttribute('id','hasil_scann_'+rowCount);
                break;

            case "urutan_hasil_scann[]":
                newcell.childNodes[0].value= (rowCount);
                newcell.childNodes[0].setAttribute('id','urutan_hasil_scann_'+(rowCount-1));
                break;
            case "softcopy[]":
                newcell.childNodes[0].setAttribute('id','softcopy_'+rowCount);
                break;

            case "urutan_softcopy[]":
                newcell.childNodes[0].value= (rowCount);
                newcell.childNodes[0].setAttribute('id','urutan_softcopy_'+(rowCount-1));
                break;

           case "tujuan[]":
                newcell.childNodes[0].setAttribute('id','tujuan_'+rowCount);
                newcell.childNodes[0].setAttribute('style','display:none;');
                break;

            case "id_user_tujuan[]":
                newcell.childNodes[0].value= (rowCount);
                newcell.childNodes[0].setAttribute('title',rowCount);
                newcell.childNodes[0].setAttribute('id','id_user_tujuan_'+(rowCount));
                break;

            case "urutan[]":
                newcell.childNodes[0].value= (rowCount);
                break;

        }
    }
}

function deleteRow(tableID) {
    try {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        for(var i=0; i<rowCount; i++) {
            var row = table.rows[i];
            var chkbox = row.cells[0].childNodes[0];
            if(null != chkbox && true == chkbox.checked) {
                if(rowCount <= 2) {
                    alert("Tidak bisa menghapus semua baris\nCukup kosongkan field agar nilai\ntidak tersimpan!!!");
                    break;
                }
                table.deleteRow(i);
                rowCount--;
                i--;
            }

        }
    }catch(e) {
        alert(e);
    }
}

