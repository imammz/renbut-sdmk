    var save_method; //for save method string
    var table;
      $(document).ready(function() {
        table = $('#dynamic-table').DataTable({ 
          
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          
          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "master_data/m_rs/ajax_list",
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
          },
          ],

        });
      });

      // greating //
      function greatSuccess() {

        $.gritter.add({
               title: 'SUKSES!',
               text: 'Data berhasil diproses.',
               sticky: false,
               time: '1000',
               class_name: 'gritter-success'
        })

      }

    function greatError() {
        $.gritter.add({
             title: 'PERINGATAN!',
             text: 'Anda tidak memiliki hak akses atau terjadi kesalahan dalam proses.',
             sticky: false,
             time: '1000',
             class_name: 'gritter-error'
        })

    }
    function sinkronisasi()
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('master_data/m_puskesmas/sinkronisasi/?_=' + (new Date()).getTime());

    }
    function edit(id)
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('master_data/m_rs/form/' + id + '?_=' + (new Date()).getTime());

    }

    function backlist()
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('master_data/m_rs'+ '?_=' + (new Date()).getTime());

    }

    function add()
    {
      $('#page-area-content').empty();
      $('#page-area-content').load('master_data/m_rs/form/'+ '?_=' + (new Date()).getTime());

    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }


    function proses_sinkronisasi()
    {

      if(confirm('Apakah anda yakin akan melakukan sinkronisasi data rumah sakit?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "master_data/m_rs/proses_sinkronisasi/",
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               greatSuccess();
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                greatError();
            }
        });
         
      }

    }

    function delete_rs(id)
    {

      if(confirm('Apakah anda yakin akan menghapus data ini?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "master_data/m_rs/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               greatSuccess();
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                greatError();
            }
        });
         
      }

    }


$.validator.setDefaults({
  submitHandler: function() { 
    
     url = "master_data/m_rs/ajax_add";

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_rs').serialize(),
            dataType: "JSON",
            success: function(data)
            { 

              greatSuccess();
              $('#btnSave').hide();
              $('#btnReset').hide();
              $('#btnAdd').show();
              //$('input[type="text"],texatrea, select', this).val('');
              //$('#form_rs').resetForm();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

              greatError();

            }
        });

  }

});

function disabledBtn()
  {
    $('#btnSave').disabled = true;
    return true;
  }


// jquery validation //
$().ready(function() {

  // validate signup form on keyup and submit
  $("#form_rs").validate({
    rules: {
      
      kode_rs: "required",
      nama_rs: "required",
      jumlah_tt: "required",
      id_provinsi: "required",
      id_kabupaten: "required",
      active: "required"
    },

    messages: {
      kode_rs: "Masukan kode rs!",
      nama_rs: "Masukan nama rs!",
      jumlah_tt: "Masukan jumlah TT!",
      id_provinsi: "Pilih provinsi!",
      id_kabupaten: "Pilih kabupaten!",
      active: "Pilih status aktif!",
    }
  });


});


// jquery choosen //
jQuery(function($) {
      
      if(!ace.vars['touch']) {
        $('.chosen-select').chosen({allow_single_deselect:true}); 
        //resize the chosen on window resize
    
        $(window)
        .off('resize.chosen')
        .on('resize.chosen', function() {
          $('.chosen-select').each(function() {
             var $this = $(this);
             $this.next().css({'width': $this.parent().width()});
          })
        }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
          if(event_name != 'sidebar_collapsed') return;
          $('.chosen-select').each(function() {
             var $this = $(this);
             $this.next().css({'width': $this.parent().width()});
          })
        });
    
      }
    
    
    });