<?php echo Modules::run('templates/cliptwo/header'); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/library/tree/css/screen.css" media="screen" />

<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >

                <?php   Message::_modal();     ?>


                <div class="wrap-content container" id="container">

                    <!-- start: YOUR CONTENT HERE -->

                    <div class="row">
                        <div class="col-md-12"> 

                            <div class="container-fluid container-fullw bg-white">
                                <div class="row">
                                    <div class="col-md-8">
                                        &nbsp;
                                    </div>
                                    <ol class="breadcrumb">
                                        <li>
                                            <span>Import Data</span>
                                        </li>
                                        
                                    </ol>
                                </div>
                                
                                <form enctype='multipart/form-data' action="<?php echo site_url('config/import/proses/'.date('Y')) ?>" method="post">
                                <!-- start: YOUR CONTENT HERE -->                        
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-white">                                            
                                            <div class="panel-body" style="min-height: 500px;">
                                                <div class="tabbable">
                                                    <ul id="myTab1" class="nav nav-tabs">

                                                        <li class="active">
                                                            <a href="#tab_faskes" data-toggle="tab">Import Dari Data Faskes</a>
                                                        </li>

                                                    </ul>
                                                    <div class="tab-content">

                                                        <div class="tab-pane fade in active" id="tab_faskes">

                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    Upload Data Faskes
                                                                </div>
                                                                <div class="col-md-2">   
                                                                    <input type="file" name="import" required/>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-12">&nbsp;</div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="col-md-12" style="text-align: center;">   
                                                                    <input class="btn btn-lg btn-primary" type="submit" value="proses"/>
                                                                  
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>   
                                                </div>                                                   
                                            </div>
                                        </div>

                                    </div>



                                </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
              $('.modal').modal({show: true});
             FormElements.init();
        });

        function export_proses() {
            //  alert($("#faskes_kode").val());
            var faskes_kode = <?php echo $_SESSION['user']['faskes_kode']; ?>;
            var faskes_tahun = $("#faskes_tahun").val();

            if (faskes_tahun === '') {
                alert('Pilih Tahun Terlebih Dahulu');
            } else {
                window.location.replace("<?php echo base_url() ?>index.php/config/export/proses/" + faskes_kode + '/' + faskes_tahun);
            }
        }
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
