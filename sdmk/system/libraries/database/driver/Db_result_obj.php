<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

/**
 * xamzo
 *
 * An premium framework and CMS for PHP 5 or newer
 *
 * @package		libraries/database
 * @author		xamzo Dev Team 
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.5
 * @filesource
 */

class Db_result_obj extends Db_prepare{

    /**
     * make result as object
     * @param type $array
     * @return \stdClass
     */
    public function object($array = null) {
        $obj = new stdClass;
        if (is_array($array)) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    $obj->{$k} = $this->object($v); //RECURSION
                } else {
                    $obj->{$k} = $v;
                }
            }
            return $obj;
        }
    }

    public function row() {
        return $this->object($this->__execute_row()); // make result array as object
    }

    /**
     * 
     * @return type
     */
    public function result_object() {
        return $this->object($this->__execute_all()); // make result array as object
    }

    /**
     * 
     * @return type
     */
    public function result() {
        return $this->result_object(); // make result array as object
    }

    /**
     * 
     * @return type
     */
    public function result_array() {
        return $this->__execute_all();  // make result as array
    }
    
    public function result_both() {
        return $this->__execute_all_array();  // make result as array
    }
    
     public function row_array() {
        return $this->__execute_row(); // make result array as object
    }

    public function show_query() {
        return '<div onclick="this.parentElement.removeChild(this);" title="click for remove" style="padding:5px;border:1px solid #ffff66;background:#ffff99; color:#999900;">' . $this->query . '</div>';
    }

}

