<!DOCTYPE html>
<html id="xamzo">
    <head> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" /> 
        <link rel="shortcut icon" href="<?php echo site_url('application/templates/private/assets/favicon.png') ?>" type="image/png" />   
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('application/templates/private/assets/styles/x_x.css') ?>">  
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('application/templates/private/assets/javascript/upload/style.css') ?>">  
        <link rel="stylesheet" type="text/css" href="<?php echo site_url(SOCKET . 'assets/style/main.css') ?>">  
        <script src="<?php echo site_url('application/templates/private/assets/javascript/libraries/jquery/1.7.2/jquery.min.js'); ?>"></script>  
        <script src="<?php echo site_url('application/templates/private/assets/javascript/libraries/jqueryui/1.10.3/jquery.ui.min.js'); ?>"></script>
        <script type="text/javascript">
            var _h = _h || [];
            _h.push([
                '<?php echo site_url() ?>',
                '<div id="circularG"><div id="circularG_1" class="circularG"></div><div id="circularG_2" class="circularG"></div><div id="circularG_3" class="circularG"></div><div id="circularG_4" class="circularG"></div><div id="circularG_5" class="circularG"></div><div id="circularG_6" class="circularG"></div><div id="circularG_7" class="circularG"></div><div id="circularG_8" class="circularG"></div></div>',
                '<?php echo X_ANIMATE ?>',
                '.xamzo-right']
                    );
            (function () {
                var _s = _h['0']['0'];
                var h = document.createElement('script');
                h.type = 'text/javascript';
                h.async = true;
                h.src = ('https:' === document.location.protocol ? 'https://ssl' : _s) + 'application/templates/private/assets/javascript/libraries/xamzo/wrapper.js';
                var sr = document.getElementsByTagName('script')[2];
                sr.parentNode.insertBefore(h, sr);
            })();</script> 
        <script type="text/javascript" src="<?php echo site_url('application/templates/private/assets/javascript/libraries/xamzo/validation.js'); ?>"></script> 

        
    <script src="<?php echo site_url('application/templates/private/assets/javascript/libraries/xamzo/mask.js'); ?>"></script>  
        <!--lib js for upload-->
        <script src="<?php echo site_url('application/templates/private/assets/javascript/upload/knob.js'); ?>"></script>
        <script src="<?php echo site_url('application/templates/private/assets/javascript/upload/upload.widget.js'); ?>"></script>
        <script src="<?php echo site_url('application/templates/private/assets/javascript/upload/iframe-transport.js'); ?>"></script>
        <script src="<?php echo site_url('application/templates/private/assets/javascript/upload/fileupload.js'); ?>"></script>  
</head>
<body>

    <div id="theme_preview"></div>
    <div id="in-cms">
        <div class="xamzo-top">
            <?php
            include_once 'part/top.php';
            ?> 
        </div>  
        <!--socket flash notice-->
        <div id="flash"></div>
        <div class="xamzo-main">                
            <div class="xamzo-left">
                <?php
                include_once 'part/left.php';
                ?> 
            </div>
            <div class="xamzo-right"></div>
        </div> 
        <script type="text/javascript">
            if (!window.location.hash) {
                    $.ajax({
                        url: '<?php site_url() ?>dashboard/index',
                        success: function (rs) {
                        $('.xamzo-right').html(rs);
                        }
                    }); 
            }
            $('.xamzo-right').html('loading...');
        </script> 
        <div class="xamzo-preview"></div>
        <div class="xamzo-bottom">
            <?php
            include_once 'part/bottom.php';
            ?>
            <span class="mysound"> </span>
        </div>
    </div>    
    <ul id="menu-right" class="mouse_menu" style="display:none">
        <li><a href="<?php echo site_url('h') ?>"><i class="fa fa-home"></i>&nbsp;Dashboard</a></li>  
        <li><a ajax="true" ajax-target="modalbox" modal-width="400px" href="#!user/edit_profile">&nbsp;<i class="fa fa-angle-right"></i>&nbsp;Edit Profile</a></li>  
        <li><a ajax="true" ajax-target="modalbox" modal-width="350px" url-push="false" href="#!user/edit_password">&nbsp;<i class="fa fa-angle-right"></i>&nbsp;Edit Password</a></li>  
        <?php if (is_auth('con_logs')): ?>
            <li><a ajax="true" ajax-target=".xamzo-right" url-push="true" href="#!user/detail/<?php echo $this->session->get_data('session_user_id') ?>">&nbsp;<i class="fa fa-angle-right"></i>&nbsp;Statistik</a></li>  
        <?php endif; ?>
        <li><a ajax="true" ajax-target=".xamzo-right" url-push="false" href="#!setting/index">&nbsp;<i class="fa fa-angle-right"></i>&nbsp;Tentang</a></li>  
    </ul> 
    <script src="<?php echo site_url('application/templates/private/assets/javascript/libraries/jquery/ui.touch.punch.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo site_url('application/templates/private/assets/javascript/libraries/scroller.js') ?>"></script> 
    <script src="<?php echo site_url('application/templates/private/assets/javascript/libraries/xamzo/print.js'); ?>"></script>   
</body>
</html>  