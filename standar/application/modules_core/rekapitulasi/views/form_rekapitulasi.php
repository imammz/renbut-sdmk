<script type="text/javascript">
   
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }

            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_btkl_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#btkl-box option').remove()
                    $('<option value="">(Pilih BTKLPP)</option>').appendTo($('#btkl-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_btkl+'">'+o.nama_btkl+'</option>').appendTo($('#btkl-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih BTKL)</option>').appendTo($('#kab-box'));
            }

        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_puskes_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-box option').remove()
                    $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_puskesmas+'">'+o.nama_puskesmas_kab+'</option>').appendTo($('#puskes-box'));
                    });

                });
            } else {
                $('#puskes-box option').remove()
                $('<option value="">(Pilih Puskesmas)</option>').appendTo($('#puskes-box'));
            }

            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rs-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rs-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rs-box'));
                    });

                });
            } else {
                $('#rs-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rs-box'));
            }

            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_kkp_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#kkp-box option').remove()
                    $('<option value="">(Pilih KKP)</option>').appendTo($('#kkp-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_kkp+'">'+o.nama_kkp+'</option>').appendTo($('#kkp-box'));
                    });

                });
            } else {
                $('#kkp-box option').remove()
                $('<option value="">(Pilih KKP)</option>').appendTo($('#kkp-box'));
            }

            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_klinik_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#klinik-box option').remove()
                    $('<option value="">(Pilih Klinik)</option>').appendTo($('#klinik-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_klinik+'">'+o.nama_klinik+'</option>').appendTo($('#klinik-box'));
                    });

                });
            } else {
                $('#klinik-box option').remove()
                $('<option value="">(Pilih Klinik)</option>').appendTo($('#klinik-box'));
            }
            

        });

        $('select[name="kode_puskesmas"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_puskesmas/get_puskes_by_kode') ?>/" + $(this).val(), '', function(data) {
                    $('#puskes-result').html(data.string);
                   
                });
            } else {
                $('#puskes-result').html();
            }
        });

        $('select[name="kode_puskesmas"]').click(function() {

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_puskesmas/get_puskes_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#result_detail').show();
                    $('#alamat').val(data.alamat);
                    $('#kode').val(data.kode_puskesmas);
                    $("input[name=id_tipe_kawasan][value=" + data.id_tipe_kawasan + "]").prop('checked', true);
                    $("input[name=id_tipe_puskesmas][value=" + data.id_tipe_puskesmas + "]").prop('checked', true);

                });
            } 

        });

        $('select[name="kode_kkp"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_kkp/get_kkp_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#result_detail').show();
                    $('#alamat').val(data.alamat);
                    $('#kode').val(data.kode_kkp);
                    $("input[name=kategori_kelas][value=" + data.kelas_kkp + "]").prop('checked', true);

                });
            } 

        });

        $('select[name="kode_rs"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#result_detail').show();
                    $('#alamat').val(data.alamat);
                    $('#kode').val(data.kode_rs);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });


    });
</script>

<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_rekapitulasi" action="<?php echo site_url('rekapitulasi/post_form')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">Jenis Faskes</label>
                  <div class="col-md-3">
                    <?php echo Master::get_master_jenis_faskes($id_jenis_faskes,'id_jenis_faskes','id_jenis_faskes','form-control','required readonly','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value)?$value->tahun:'','tahun','tahun','form-control','required','inline');?>
                  </div>
                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5','6','8','9')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','5','6','8','9')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '','id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5','6','8')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('5','8','9')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_kabupaten(isset($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '' ,'id_kabupaten','kab-box','form-control',''.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( $id_jenis_faskes == 1 ){ if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5')) ){?>
                <div class="form-group" id="form-puskesmas" >
                  <label class="control-label col-md-2">Puskesmas</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('0')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_puskesmas(isset($value) ? $value->kode_puskesmas : $this->session->userdata('data_user')->kode_puskesmas,'kode_puskesmas','puskes-box','form-control',''.$readonly.'','inline');?>
                  </div>
                  
                </div>
                <?php } }?>

                 <?php if( $id_jenis_faskes == 3 ){ if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','5','6')) ){?>
                <div class="form-group" id="form-rumah-sakit" >
                  <label class="control-label col-md-2">Rumah Sakit</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('0')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_rsu(isset($value) ? $value->kode_rs : $this->session->userdata('data_user')->kode_rs,'kode_rs','rs-box','form-control',''.$readonly.'','inline');?>
                  </div>
                  
                </div>
                <?php } }?>

                <?php if( $id_jenis_faskes == 30 ){if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','8')) ){
                  ?>
                <div class="form-group" id="form-kkp" >
                  <label class="control-label col-md-2">KKP</label>
                  <div class="col-md-4">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('8')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_kkp(isset($value) ? $value->kode_kkp : ($this->session->userdata('data_user')->kode_kkp) ? $this->session->userdata('data_user')->kode_kkp : '','kode_kkp','kkp-box','form-control',' '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php } }?>

                <?php if( $id_jenis_faskes == 29 ){if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','9')) ){?>
                <div class="form-group" id="form-btkl" >
                  <label class="control-label col-md-2">BTKL</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('9')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_btkl(isset($value) ? $value->kode_btkl : ($this->session->userdata('data_user')->kode_btkl) ? $this->session->userdata('data_user')->kode_btkl : '','kode_btkl','btkl-box','form-control',' '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php } }?>

                <?php if( $id_jenis_faskes == 31 ){if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','10')) ){?>
                <div class="form-group" id="form-klinik" >
                  <label class="control-label col-md-2">Klinik</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('10')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_klinik(isset($value) ? $value->kode_klinik : ($this->session->userdata('data_user')->kode_klinik) ? $this->session->userdata('data_user')->kode_klinik : '','kode_klinik','klinik-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php } } ?>

                <div id="result_detail" style="display: none;">

                  <div class="form-group">
                    <label class="control-label col-md-1"></label>
                    <div class="col-md-11">

                      <div class="form-group">
                        <label class="control-label col-md-2">Kode</label>
                        <div class="col-md-3">
                          <input name="kode" id="kode" value=""  class="form-control" type="text" readonly>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-2">Alamat</label>
                        <div class="col-md-8">
                          <input name="alamat" id="alamat" value=""  class="form-control" type="text" readonly>
                        </div>
                      </div>

                      <?php if( $id_jenis_faskes == 1 ){?>
                        <div class="form-group">
                          <label class="control-label col-md-2">Kategori Rawat Inap</label>
                          <div class="col-md-10">
                            <div class="radio">
                                <label>
                                  <input name="id_tipe_puskesmas" type="radio" class="ace" value="0" checked />
                                  <span class="lbl"> Semua Kategori </span>
                                </label>
                                <?php
                                  $data_tipe_puskesmas = Master::get_tipe_puskesmas_data();
                                  foreach($data_tipe_puskesmas as $row_data_tipe_puskesmas){
                                ?>
                                  <label>
                                    <input name="id_tipe_puskesmas" type="radio" class="ace" value="<?php echo $row_data_tipe_puskesmas['id_tipe_puskesmas']?>"/>
                                    <span class="lbl"> <?php echo $row_data_tipe_puskesmas['nama_tipe_puskesmas']?></span>
                                  </label>
                                <?php }?>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-2">Kategori Kawasan</label>
                          <div class="col-md-10">
                            <div class="radio">
                                <label>
                                    <input name="id_tipe_kawasan" type="radio" class="ace" value="0" checked  />
                                    <span class="lbl"> Semua Kategori </span>
                                  </label>

                                <?php
                                  $data = Master::get_tipe_kawasan_data();
                                  foreach($data as $row){
                                ?>
                                  <label>
                                    <input name="id_tipe_kawasan" type="radio" class="ace" value="<?php echo $row['id_tipe_kawasan']?>" />
                                    <span class="lbl"> <?php echo $row['nama_tipe_kawasan']?></span>
                                  </label>
                                <?php }?>
                            </div>
                          </div>
                        </div>
                        <?php }?>

                        <?php if( $id_jenis_faskes == 3 ){?>
                        <div class="form-group">
                          <label class="control-label col-md-2">Kategori Kelas</label>
                          <div class="col-md-9">
                            <div class="radio">
                                <?php
                                  $katergori_kelas = array('A','B','C','D');
                                  foreach($katergori_kelas as $row_kategori_kelas){
                                ?>
                                  <label>
                                    <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_kategori_kelas?>" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : ''; ?>  />
                                    <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                                  </label>
                                <?php }?>
                            </div>
                          </div>
                        </div>

                        <?php }?>

                        <?php if( $id_jenis_faskes == 30 ){?>
                        <div class="form-group">
                          <label class="control-label col-md-2">Kategori Kelas</label>
                          <div class="col-md-9">
                            <div class="radio">
                                <?php
                                   $katergori_kelas = array('I','II','III');
                                  foreach($katergori_kelas as $row_kategori_kelas){
                                ?>
                                  <label>
                                    <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_kategori_kelas?>" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                                    <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                                  </label>
                                <?php }?>
                            </div>
                          </div>
                        </div>

                        <?php }?>

                        <?php if( $id_jenis_faskes == 29 ){?>
                        <div class="form-group">
                          <label class="control-label col-md-2">Kategori Kelas</label>
                          <div class="col-md-9">
                            <div class="radio">
                                <?php
                                  $katergori_kelas = array('BB','I','II');
                                  foreach($katergori_kelas as $row_kategori_kelas){
                                ?>
                                  <label>
                                    <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_kategori_kelas?>" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                                    <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                                  </label>
                                <?php }?>
                            </div>
                          </div>
                        </div>

                        <?php }?>
                        
                        <?php if( $id_jenis_faskes == 31 ){?>

                        <div class="form-group">
                          <label class="control-label col-md-2">Kategori Kelas</label>
                          <div class="col-md-9">
                            <div class="radio">
                                <?php
                                  $katergori_kelas = array('utama','pratama');
                                  foreach($katergori_kelas as $row_kategori_kelas){
                                ?>
                                  <label>
                                    <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_kategori_kelas?>" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : ''; ?>  />
                                    <span class="lbl"> <?php echo strtoupper($row_kategori_kelas)?></span>
                                  </label>
                                <?php }?>
                            </div>
                          </div>
                        </div>
                        <?php }?>

                    </div>
                  </div>

                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4'))){?>
                <div class="form-group">
                  <label class="control-label col-md-3"></label>
                  <div class="col-md-9">

                    <span class="help-inline col-xs-12 col-sm-7">
                      <label class="middle">
                        <input class="ace" type="checkbox" id="id-disable-check" name="all_puskesmas" value="Y" />
                        <span class="lbl"> Checklist untuk semua</span>
                      </label>
                    </span>
                    
                  </div>
                </div>
                <?php }?>

                

                <div class="form-actions center">

                  <a onclick="getMenu('rekapitulasi')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info" value="submit">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/rekapitulasi.js'?>"></script>
