<?php

 class master_model extends CI_Model {

     function __construct() {
         
     }
     
     
     public function _getRSKelasId($KELAS_RS) {
      
         switch ($KELAS_RS) {
             case 'A':
                 $ref_faskes_kelas_id = 9;
                 break;
             case 'B':
                 $ref_faskes_kelas_id = 10;
                 break;
             case 'C':
                 $ref_faskes_kelas_id = 11;
                 break;
             case 'D':
                 $ref_faskes_kelas_id = 12;
                 break;

             default:
                 $ref_faskes_kelas_id = 1;
                 break;
         }
            return $ref_faskes_kelas_id;
     }
     
     public function _getStatusPengelolaan($operator) {
         
         switch ($operator) {
             case 'Pemkab':
                    $status_pengelolaan_id = 1;
                break;
            case 'Pemkot':
                    $status_pengelolaan_id = 2;
                break;
            case 'Swasta Non Profit':
                    $status_pengelolaan_id = 3;
                break;
            case 'Swasta':
                    $status_pengelolaan_id = 4;
                break;
            case 'BUMN':
                    $status_pengelolaan_id = 5;
                break;
            case 'Pemprop':
                    $status_pengelolaan_id = 6;
                break;
            case 'TNI / POLRI':
                    $status_pengelolaan_id = 7;
                break;
            case 'Kemkes':
                    $status_pengelolaan_id = 8;
                break;
            case 'Kementerian Lain':
                    $status_pengelolaan_id = 9;
                break;

             default:
                 $status_pengelolaan_id = 0;
                 break;
         }
         
         return $status_pengelolaan_id;
         
     }
     
     
     public function _getPuskesmasKelasId() {
         
     }


    

     

 }

?>