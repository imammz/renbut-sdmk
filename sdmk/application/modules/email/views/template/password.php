<html>
    <head>
        <title>Forgot Password</title>
    </head>
    <body style='float:left; width:100%; margin:0px auto;padding:0px; font-family: sans-serif, serif; background: #f5f5f5; line-height:25px; font-size: 13px'>
        <div style="background: #fff; width: 100%; max-width:600px; margin: 10px auto;">
            <h3 style="background: #4B4207; color:#fff; padding: 5px; margin:0px; border-bottom: 1px solid #ddd">Hi,.. {%username%}</h3>
            <h3 style="padding: 5px; margin:0px; color:#666; border-bottom: 1px solid #ddd">New password for {%email%}</h3>
            <div style="border-bottom: 1px solid #ddd; color:#666; padding: 5px; font-size: 12px">
                 <span><b>Date:</b> {%date%} </span>|<span> <b>From:</b> {%from%}</span>                        
            </div> 
            <div style="width: 98%; padding: 5px 1%; color:#666">
                 <p>{%mail_content%}</p>
            </div>
        </div>
    </body>
</html>