<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Export extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_CRUD');
        $this->load->model('entry/faskes_model');
    }

    public function index() {

        $faskes_kode = $_SESSION['user']['faskes_kode'];
        
        if($faskes_kode =='A' AND $_SESSION['user']['NO_KAB']=='A') {
            $faskes_kode = $_SESSION['user']['NO_PROV'];
        }
        if($faskes_kode =='A' AND $_SESSION['user']['NO_KAB']!='A') {
            $faskes_kode = $_SESSION['user']['NO_KAB'];
        }

        
        $this->orm->debug = true;
        $data = array();
        $data['class'] = 'export';
        $data['title'] = "Export";
        $data['faskes_kode'] = $faskes_kode;
        $data['faskes'] = $this->orm->faskes->where('faskes_kode', $faskes_kode)->fetch();
        $data['installasi'] = $this->orm->faskes_installasi->where('faskes_kode', $faskes_kode);
        $data['sdmk_faskes_installasi'] = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $data['installasi']);
       
        
         switch ($data['faskes']['ref_jenis_faskes_kode']) {
            case 1:
                $view = 'export_puskesmas_view';
            break;
            case 2:
                $view = 'export_rumahsakit_view';
                break;
            case 3:
                $view = 'export_view';
                break;
            case 4:
                $view = 'export_view';
                break;
            case 5:
                $view = 'export_view';
                break;
            case 6:
                $view = 'export_dinkes_view';
                break;
            case 7:
                $view = 'export_dinkesprov_view';
                break;
            case 8:
                $view = 'export_view';
                break;
            case 9:
                $view = 'export_view';
                break;
            case 10:
                $view = 'export_view';
                break;
            case 96:
                $view = 'export_view';
                break;
            case 97:
                $view = 'export_view';
                break;
            case 98:
                $view = 'export_view';
                break;
            case 99:
                $view = 'export_view';
                break;

            default:
                $view = 'export_view';
                break;
        }

        $this->load->view($view, $data);
    }

    public function proses($faskes_kode, $faskes_tahun = null) {
        
        if($faskes_tahun==null) {
            $faskes_tahun = $this->input->post('faskes_tahun');
        }
        
        //print_r($_POST); exit;
        
        header('Content-Type: application/json; charset=utf-8');
        //header('Content-Length: ' . $length);
        header('Access-Control-Allow-Origin: *');
        $filename = $faskes_kode . '_' . $faskes_tahun . '.json';
        header('Content-Disposition: attachment; filename=' . $filename);

        $faskes = $this->orm->faskes->where('faskes_kode = ? AND tahun = ?', $faskes_kode, $faskes_tahun);
        $this->orm->debug = true;
        $data = array();

        foreach ($faskes as $row_faskes) {

            $row_faskes['faskes_installasi'] = array();

            $data_faskes = array();
            $data_faskes['faskes_kode'] = $row_faskes['faskes_kode'];
            $data_faskes['NO_KAB'] = $row_faskes['NO_KAB'];
            $data_faskes['faskes_nama'] = $row_faskes['faskes_nama'];
            $data_faskes['faskes_telp'] = $row_faskes['faskes_telp'];
            $data_faskes['faskes_alamat'] = $row_faskes['faskes_alamat'];
            $data_faskes['faskes_email'] = $row_faskes['faskes_email'];
            $data_faskes['ref_faskes_kelas_id'] = $row_faskes['ref_faskes_kelas_id'];
            $data_faskes['ref_jenis_faskes_kode'] = $row_faskes['ref_jenis_faskes_kode'];
            $data_faskes['status_pengelolaan_id'] = $row_faskes['status_pengelolaan_id'];
            $data_faskes['tahun'] = $row_faskes['tahun'];
            $data_faskes['update_date'] = $row_faskes['update_date'];
            $data_faskes['insert_date'] = $row_faskes['insert_date'];
            
            $faskes_installasi_id = $this->input->post('faskes_installasi_id');
            $ref_jenis_sdmk_kode = $this->input->post('ref_jenis_sdmk_kode');

            $faskes_installasi = $this->orm->faskes_installasi->where('faskes_kode', $row_faskes['faskes_kode']);
            
            if(!empty($faskes_installasi_id)) {
                $faskes_installasi->and('faskes_installasi_id',$faskes_installasi_id);
            }
            
            $installasi = array();
            foreach ($faskes_installasi as $row_installasi) {
                $row_installasi_data = array();
                $row_installasi_data['faskes_installasi_id'] = $row_installasi['faskes_installasi_id'];
                $row_installasi_data['faskes_kode'] = $row_installasi['faskes_kode'];
                $row_installasi_data['installasi_nama'] = $row_installasi['installasi_nama'];
                $row_installasi_data['eselon'] = $row_installasi['eselon'];
                $row_installasi_data['parent'] = $row_installasi['parent'];
                $row_installasi_data['urut'] = $row_installasi['urut'];

                $sdmk_faskes_installasi = $this->orm->sdmk_faskes_installasi->where('faskes_installasi_id', $row_installasi['faskes_installasi_id']);
                
                if(!empty($ref_jenis_sdmk_kode)) {
                    $sdmk_faskes_installasi->and('ref_jenis_sdmk_kode',$ref_jenis_sdmk_kode);
                }
               

                
                $sdmk = array();
                foreach ($sdmk_faskes_installasi as $row_sdmk) {

                    $row_sdmk_data['sdmk_faskes_installasi_id'] = $row_sdmk['sdmk_faskes_installasi_id'];
                    $row_sdmk_data['ref_jenis_sdmk_kode'] = $row_sdmk['ref_jenis_sdmk_kode'];
                    $row_sdmk_data['ref_jenis_sdmk_jft_id'] = $row_sdmk['ref_jenis_sdmk_jft_id'];
                    $row_sdmk_data['sdmk_faskes_installasi_nama'] = $row_sdmk['sdmk_faskes_installasi_nama'];
                    $row_sdmk_data['faskes_installasi_id'] = $row_sdmk['faskes_installasi_id'];
                    $row_sdmk_data['jumlah_PNS'] = $row_sdmk['jumlah_PNS'];
                    $row_sdmk_data['jumlah_PPPK'] = $row_sdmk['jumlah_PPPK'];
                    $row_sdmk_data['jumlah_PTT'] = $row_sdmk['jumlah_PTT'];
                    $row_sdmk_data['jumlah_HD'] = $row_sdmk['jumlah_HD'];
                    $row_sdmk_data['jumlah_DLL'] = $row_sdmk['jumlah_DLL'];
                    $row_sdmk_data['jumlah_seluruh'] = $row_sdmk['jumlah_seluruh'];
                    $row_sdmk_data['hasil_kebutuhan_sdmk'] = $row_sdmk['hasil_kebutuhan_sdmk'];
                    $row_sdmk_data['hasil_kebutuhan_sdmk_pembulatan'] = $row_sdmk['hasil_kebutuhan_sdmk_pembulatan'];

                    $sdmk_kbk = $this->orm->sdmk_kbk->where('sdmk_faskes_installasi_id', $row_sdmk['sdmk_faskes_installasi_id']);
                    $kbk = array();
                    foreach ($sdmk_kbk as $row_kbk) {
                        $row_kbk_data['sdmk_kbk_id'] = $row_kbk['sdmk_kbk_id'];
                        $row_kbk_data['sdmk_faskes_installasi_id'] = $row_kbk['sdmk_faskes_installasi_id'];
                        $row_kbk_data['capaian'] = $row_kbk['capaian'];
                        $row_kbk_data['norma_waktu'] = $row_kbk['norma_waktu'];
                        $row_kbk_data['kbk_uraian'] = $row_kbk['kbk_uraian'];
                        $row_kbk_data['jenis_tugas_id'] = $row_kbk['jenis_tugas_id'];
                        $row_kbk_data['SBK'] = $row_kbk['SBK'];
                        $row_kbk_data['waktu_kegiatan'] = $row_kbk['waktu_kegiatan'];
                        $row_kbk_data['FTP'] = $row_kbk['FTP'];
                        $row_kbk_data['STP'] = $row_kbk['STP'];
                        $row_kbk_data['satuan_id'] = $row_kbk['satuan_id'];
                        $row_kbk_data['urut'] = $row_kbk['urut'];
                        $kbk[] = $row_kbk_data;
                    }

                    $row_sdmk_data['kbk'] = $kbk;
                    $sdmk[] = $row_sdmk_data;
                }

                $row_installasi_data['sdmk'] = $sdmk;

                $installasi[] = $row_installasi_data;
            }
            $data_faskes['faskes_installasi'] = $installasi;

            $data[] = $data_faskes;
        }

        echo json_encode($data);
    }

}
