<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @version     : 1.0
 * @name        : Router Class
 * @author      : writed by (hadinug: http://www.hadinug.net)
 * @todo        : Make simple route in class
 * @description 
 * Class route make web programing to be eassy and simple with oop for PHP 5.3 or newer
 */
class X_Router extends X_Loader {

    public $_uri = false;  // SERVER REQUEST URI
    public $_routes = array(); // list routes in array
    public $_add = array(); // add route in array
    public $_method = ''; // view the method
    public $_params = array(); // get params after class/method
    public $data_dir;
    public $not_in = array();

    /**
     * instance and define
     */
    public function __construct() {
        parent::__construct();
        $this->_uri = isset($_GET['uri']) ? strip_tags(trim(preg_replace("/\/$/", "", ($_GET['uri'])))) : '/';
        $this->_method = $this->_method();
        $this->_params = $this->_params();
        $this->not_in = array('.', '..', 'index.html', 'Thumb.db', '.htaccess');
    }

    function array_remove_by_value($array, $value) {
        return array_values(array_diff($array, $value));
    }

    /**
     *
     * @return array
     * read the class method and make it to be route array 
     */
    public function _autoload() {
        $route = array();
        // read directory 
        if ($dir_handle = opendir(MODPATH)) {
            while ($dir = readdir($dir_handle)) {
                if (is_dir(MODPATH . $dir)) {
                    if ($this->_method != $dir) {
                        $base_config = MODPATH . $dir . '/configs/routes' . EXT;
                        if (is_file($base_config)) {
                            include_once $base_config;
                        }
                    }
                    // here we load
                    $base_controller = MODPATH . $dir . '/controllers/';
                    if (is_dir($base_controller)) {
                        if ($handle = opendir($base_controller)) {
                            while (false !== ($entry = readdir($handle))) {
                                if (strpos($entry, '.php', 1)) {
                                    $controller = strtolower(str_replace(EXT, '', strip_tags($entry)));
                                    // check the available class
                                    if ($this->_class() == $controller) {

                                        require_once $base_controller . ucfirst($controller) . EXT;
                                        $class_methods = get_class_methods($controller);
                                        $newarray = $this->array_remove_by_value($class_methods, array(
                                            '__initialize',
                                            '__construct',
                                            '__library',
                                            '__model',
                                            '__helper',
                                            '__language',
                                            '__view',
                                            '__template',
                                            '__config',
                                            'get_instance'
                                                )
                                        );

                                        foreach ($newarray as $method_name) { 
                                            if ($method_name == 'index') {
                                                $route[$controller] = $controller . '/' . $method_name; 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            closedir($dir_handle);
        }

        // extention class
        if ($dir_handle = opendir(EXTPATH)) {
            while ($dir = readdir($dir_handle)) {
                if (is_dir(EXTPATH . $dir)) {
                    if ($this->_method != $dir) {
                        $base_config = EXTPATH . $dir . '/configs/routes' . EXT;
                        if (is_file($base_config)) {
                            include_once $base_config;
                        }
                    }
                    $base_controller = EXTPATH . $dir . '/controllers/';
                    if (is_dir($base_controller)) {
                        if ($handle = opendir($base_controller)) {
                            while (false !== ($entry = readdir($handle))) {
                                if (strpos($entry, '.php', 1)) {
                                    $controller = strtolower(str_replace(EXT, '', strip_tags($entry)));
                                    // check the available class
                                    if ($this->_class() == $controller) {
                                        require_once $base_controller . ucfirst($controller) . EXT;
                                        $class_methods = get_class_methods($controller);
                                        $newarray = $this->array_remove_by_value($class_methods, array(
                                            '__initialize',
                                            '__construct',
                                            '__library',
                                            '__model',
                                            '__helper',
                                            '__language',
                                            '__view',
                                            '__template',
                                            '__config',
                                            'get_instance'
                                                )
                                        );
                                        foreach ($newarray as $method_name) {
                                            if ($method_name == 'index') {
                                                $route[$controller] = $controller . '/' . $method_name;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            closedir($dir_handle);
        } 
        return $route;
    }

    /**
     * get the parameter uri in array
     * @return type 
     */
    public function _params() {
        $args = array();
        $ar = explode('/', $this->_uri);
        for ($i = 0; $i < count($ar); $i++) {
            if ($i > 1) {
                $args[] = $ar[$i];
            }
        }

        return $args;
    }

    /**
     * get value of the array params
     * @return type 
     */
    public function _val_params($type = null) {
        if ($type == null) {
            $args = $this->_add[$this->_uri];
        } else {
            $args = $this->_uri;
        }
        $a = array();
        $ar = explode('/', $args);
        for ($i = 0; $i < count($ar); $i++) {
            if ($i > 1) {
                $a[] = $ar[$i];
            }
        }

        return $a;
    }

    /**
     * get method of uri
     * @return type 
     */
    public function _method() {
        $met = array();
        $m = explode('/', $this->_uri);
        for ($i = 0; $i < count($m); $i++) {
            if ($i < 2) {
                $met[] = $m[$i];
            }
        }

        return implode("/", $met);
    }

    /**
     * add uri route in array
     * @param type $add
     * @return type 
     */
    public function _add($add = array()) {
        if (is_array($add)) {
            return $this->_add = array_merge($add, $this->_autoload());
        }
    }

    /**
     * get name class of route
     * @return type 
     */
    public function _class() {
        $cls = explode('/', $this->_uri);
        return $cls['0'];
    }

    function _detect($_uri = null) {
        if ($_uri != null) {
            $_u = explode('/', $_uri);
            $cls = ucfirst($_u['0']);
            $base_module = MODPATH;
            if ($dir_handle = opendir($base_module)) {
                while ($dir = readdir($dir_handle)) {
                    if (is_dir($base_module . $dir)) {
                        $base_controller = $base_module . $dir . '/controllers/';
                        if (is_dir($base_controller)) {
                            if (is_file($base_controller . $cls . EXT)) {
                                (is_file($base_controller . $cls . EXT)) ? $this->data_dir = $base_controller . $cls . EXT : ''; 
                                if (array_key_exists($this->_uri, $this->_add)) {
                                    require_once $base_controller . $cls . EXT;
                                    $c = new $cls;
                                    $args = $this->_val_params();
                                    $c->$_u['1'](
                                            isset($args['0']) ? $args['0'] : '', isset($args['1']) ? $args['1'] : '', isset($args['2']) ? $args['2'] : '', isset($args['3']) ? $args['3'] : '', isset($args['4']) ? $args['4'] : '', isset($args['5']) ? $args['5'] : '', isset($args['6']) ? $args['6'] : '', isset($args['7']) ? $args['7'] : ''
                                    ); 
                                } else if (array_key_exists($this->_method, $this->_add)) {
                                    require_once $base_controller . $cls . EXT;
                                    $c = new $cls;

                                    $args = $this->_params;
                                    $c->$_u['1'](
                                            isset($args['0']) ? $args['0'] : '', isset($args['1']) ? $args['1'] : '', isset($args['2']) ? $args['2'] : '', isset($args['3']) ? $args['3'] : '', isset($args['4']) ? $args['4'] : '', isset($args['5']) ? $args['5'] : '', isset($args['6']) ? $args['6'] : '', isset($args['7']) ? $args['7'] : ''
                                    ); 
                                } else {
                                    //show_error('','error_404');
                                }
                            } else {
                                //show_error('','error_404');
                            }
                        }
                    }
                }
                closedir($dir_handle);
            }

            // extention 
            $base_ext = EXTPATH;
            if ($dir_handle = opendir($base_ext)) {
                while ($dir = readdir($dir_handle)) {
                    if (is_dir($base_ext . $dir)) {
                        $base_controller = $base_ext . $dir . '/controllers/';
                        if (is_dir($base_controller)) {
                            if (is_file($base_controller . $cls . EXT)) {
                                if (array_key_exists($this->_uri, $this->_add)) {
                                    require_once $base_controller . $cls . EXT;
                                    $c = new $cls;
                                    $args = $this->_val_params();
                                    $c->$_u['1'](
                                            isset($args['0']) ? $args['0'] : '', isset($args['1']) ? $args['1'] : '', isset($args['2']) ? $args['2'] : '', isset($args['3']) ? $args['3'] : '', isset($args['4']) ? $args['4'] : '', isset($args['5']) ? $args['5'] : '', isset($args['6']) ? $args['6'] : '', isset($args['7']) ? $args['7'] : ''
                                    ); 
                                } else if (array_key_exists($this->_method, $this->_add)) {
                                    require_once $base_controller . $cls . EXT;
                                    $c = new $cls;
                                    $args = $this->_params;
                                    $c->$_u['1'](
                                            isset($args['0']) ? $args['0'] : '', isset($args['1']) ? $args['1'] : '', isset($args['2']) ? $args['2'] : '', isset($args['3']) ? $args['3'] : '', isset($args['4']) ? $args['4'] : '', isset($args['5']) ? $args['5'] : '', isset($args['6']) ? $args['6'] : '', isset($args['7']) ? $args['7'] : ''
                                    ); 
                                } else {
                                    // redirect('_404');
                                }
                            } else {
                                
                            }
                        }
                    }
                }
                closedir($dir_handle);
            }
        } else {
            // redirect('_404');
        }
    }

    /**
     * instance object
     */
    public function _instance() {
        if ($this->_uri != false) {
            if (array_search($this->_method(), array_keys($this->_add)) > 0 && array_search($this->_class(), array_keys($this->_add))) {
                if (is_dir(EXTPATH . 'installer') && $this->_uri != '_404' && $this->_class() != 'installer' && $this->_class() != 'setting') {
                    $this->_uri = 'installer/index';
                }
                if (array_key_exists($this->_uri, $this->_add)) {
                    $_uri = strip_tags($this->_add[$this->_uri]);
                } else {
                    $_uri = strip_tags($this->_uri);
                }
                $this->_detect($_uri);
            } else if (isset($this->_add[$this->_uri])) {
                if (is_dir(EXTPATH . 'installer')) {
                    $_uri = strip_tags('installer/index');
                } else {
                    $_uri = strip_tags($this->_add[$this->_uri]);
                }
                $this->_detect($_uri);
            } else {
                $this->other_folder($this->_uri);
            }
        }
    }

    public function other_folder($_uri) {
        $_u = explode('/', $_uri);
        $cls = ucfirst($_u['0']);
        $base_module = MODPATH;
        $data_dir = array();
        if ($dir_handle = opendir($base_module)) {
            while ($dir = readdir($dir_handle)) {
                if (is_dir($base_module . $dir)) {
                    $base_controller = $base_module . $dir . '/controllers/';
                    (is_dir($base_controller)) ? $data_dir[] = $base_controller : '';
                }
            }
            closedir($dir_handle);
        }
        $data_class = '';
        foreach ($data_dir as $key => $value) {
            $data_class.= (is_file($value . $cls . EXT)) ? $value . $cls . EXT : '';
        }

        if ($data_class != '') {
            require_once $data_class;
            $c = new $cls;
            $args = $this->_val_params(true);
            $class_methods = get_class_methods($cls);
            if (isset($_u['1'])) {
                if (in_array($_u['1'], $class_methods) && $_u['1'] != '') {
                    $c->$_u['1'](
                            isset($args['0']) ? $args['0'] : '', isset($args['1']) ? $args['1'] : '', isset($args['2']) ? $args['2'] : '', isset($args['3']) ? $args['3'] : '', isset($args['4']) ? $args['4'] : '', isset($args['5']) ? $args['5'] : '', isset($args['6']) ? $args['6'] : '', isset($args['7']) ? $args['7'] : ''
                    ); 
                } else {
                    show_error('', 'error_404');
                }
            } else {
                show_error('', 'error_404');
            }
        } else {
            $_u = explode('/', $_uri);
            $cls = ucfirst($_u['0']);
            $base_module = EXTPATH;
            $data_dir = array();
            if ($dir_handle = opendir($base_module)) {
                while ($dir = readdir($dir_handle)) {
                    if (is_dir($base_module . $dir)) {
                        $base_controller = $base_module . $dir . '/controllers/';
                        (is_dir($base_controller)) ? $data_dir[] = $base_controller : '';
                    }
                }
                closedir($dir_handle);
            }
            $data_class = '';
            foreach ($data_dir as $key => $value) {
                $data_class.= (is_file($value . $cls . EXT)) ? $value . $cls . EXT : '';
            }

            if ($data_class != '') {
                require_once $data_class;
                $c = new $cls;
                $args = $this->_val_params(true);
                $class_methods = get_class_methods($cls);
                if (in_array($_u['1'], $class_methods) && $_u['1'] != '') {
                    $c->$_u['1'](
                            isset($args['0']) ? $args['0'] : '', isset($args['1']) ? $args['1'] : '', isset($args['2']) ? $args['2'] : '', isset($args['3']) ? $args['3'] : '', isset($args['4']) ? $args['4'] : '', isset($args['5']) ? $args['5'] : '', isset($args['6']) ? $args['6'] : '', isset($args['7']) ? $args['7'] : ''
                    ); 
                } else {
                    show_error('', 'error_404');
                }
            } else {
                show_error('', 'error_404');
            }
        }
    }

    /**
     *  
     */
    public function _list() {
        print_r($this->_add);
    }

}

