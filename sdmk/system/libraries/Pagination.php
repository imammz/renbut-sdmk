<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework for PHP 5.3 or newer
 *
 * @package		xamzo
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013 xamzo.
 * @license		http://xamzo.com/user_guide/license.html
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */
class Pagination {

    public $base_url = ''; // The page we are linking to
    public $total_rows = ''; // Total number of items (database results)
    public $per_page = 10; // Max number of items you want shown per page
    public $num_links = 1; // Number of "digit" links to show before/after the currently viewed page
    public $cur_page = 0; // The current page being viewed
    public $first_link = '&lsaquo; First';
    public $next_link = '&gt;';
    public $prev_link = '&lt;';
    public $last_link = 'Last &rsaquo;';
    public $uri_segment = 3;
    public $close_box = 'false';
    public $url_push = 'true';
    public $div = '.center';
    public $full_tag_open = '';
    public $full_tag_close = '';
    public $first_tag_open = '';
    public $first_tag_close = '&nbsp;';
    public $last_tag_open = '&nbsp;';
    public $last_tag_close = '';
    public $cur_tag_open = '&nbsp;<strong>';
    public $cur_tag_close = '</strong>';
    public $next_tag_open = '&nbsp;';
    public $next_tag_close = '&nbsp;';
    public $prev_tag_open = '&nbsp;';
    public $prev_tag_close = '';
    public $num_tag_open = '&nbsp;';
    public $num_tag_close = '';
    public $page_query_string = FALSE;
    public $query_string_segment = 'per_page';

    /**
     * Constructor
     *
     * @access	public
     * @param	array	initialization parameters
     */
    function Pagination($params = array()) {
        if (count($params) > 0) {
            $this->initialize($params);
        }
    }

    // --------------------------------------------------------------------

    /**
     * Initialize Preferences
     *
     * @access	public
     * @param	array	initialization parameters
     * @return	void
     */
    function initialize($params = array()) {
        if (count($params) > 0) {
            foreach ($params as $key => $val) {
                if (isset($this->$key)) {
                    $this->$key = $val;
                }
            }
        }
    }

    // --------------------------------------------------------------------

    /**
     * Generate the pagination links
     *
     * @access	public
     * @return	string
     */
    public function crawler_links() {
        // If our item count or per-page total is zero there is no need to continue.
        if ($this->total_rows == 0 OR $this->per_page == 0) {
            return '';
        }

        // Calculate the total number of pages
        $num_pages = ceil($this->total_rows / $this->per_page);

        // Is there only one page? Hm... nothing more to do here then.
        if ($num_pages == 1) {
            return '';
        }

        // Determine the current page number. 

        if ($this->page_query_string === TRUE) {
            if (($this->query_string_segment) != 0) {
                $this->cur_page = ($this->query_string_segment);

                // Prep the current page - no funny business!
                $this->cur_page = (int) $this->cur_page;
            }
        } else {
            if ($this->uri_segment != 0) {
                $this->cur_page = $this->uri_segment;

                // Prep the current page - no funny business!
                $this->cur_page = (int) $this->cur_page;
            }
        }

        $this->num_links = (int) $this->num_links;

        if ($this->num_links < 1) {
            show_error('Your number of links must be a positive number.');
        }

        if (!is_numeric($this->cur_page)) {
            $this->cur_page = 0;
        }

        // Is the page number beyond the result range?
        // If so we show the last page
        if ($this->cur_page > $this->total_rows) {
            $this->cur_page = ($num_pages - 1) * $this->per_page;
        }

        $uri_page_number = $this->cur_page;
        $this->cur_page = floor(($this->cur_page / $this->per_page) + 1);

        // Calculate the start and end numbers. These determine
        // which number to start and end the digit links with
        $start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
        $end = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

        // Is pagination being used over GET or POST?  If get, add a per_page query
        // string. If post, add a trailing slash to the base URL if needed
        if ($this->page_query_string === TRUE) {
            $this->base_url = rtrim($this->base_url) . '&amp;' . $this->query_string_segment . '=';
        } else {
            $this->base_url = rtrim($this->base_url, '/') . '/';
        }

        if (site_url() != '/') {
            $this->base_url = str_replace(site_url(), '', $this->base_url);
        } else {
            $this->base_url = $this->base_url;
        }

        // And here we go...
        $output = '';

        // Render the "First" link
        if ($this->cur_page > ($this->num_links + 1)) {
            $output .= $this->first_tag_open . '
                            <a ajax="true" href="#!' . $this->base_url . '" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" ajax-target="' . $this->div . '">' . $this->first_link . '</a>' . $this->first_tag_close;
        }

        // Render the "previous" link
        if ($this->cur_page != 1) {
            $i = $uri_page_number - $this->per_page;
            if ($i == 0)
                $i = '';
            $output .= $this->prev_tag_open . '
                            <a ajax="true" href="#!' . $this->base_url . $i . '" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" ajax-target="' . $this->div . '">' . $this->prev_link . '</a>' . $this->prev_tag_close;
        }

        // Write the digit links
        for ($loop = $start - 1; $loop <= $end; $loop++) {
            $i = ($loop * $this->per_page) - $this->per_page;

            if ($i >= 0) {
                if ($this->cur_page == $loop) {
                    $output .= $this->cur_tag_open . $loop . $this->cur_tag_close; // Current page
                } else {
                    $n = ($i == 0) ? '' : $i;
                    $output .= $this->num_tag_open . '<a ajax="true" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" href="#!' . $this->base_url . $n . '" ajax-target="' . $this->div . '">' . $loop . '</a>' . $this->num_tag_close;
                }
            }
        }

        // Render the "next" link
        if ($this->cur_page < $num_pages) {
            $output .= $this->next_tag_open . '<a ajax="true" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" href="#!' . $this->base_url . ($this->cur_page * $this->per_page) . '" ajax-target="' . $this->div . '">' . $this->next_link . '</a>';
        }

        // Render the "Last" link
        if (($this->cur_page + $this->num_links) < $num_pages) {
            $i = (($num_pages * $this->per_page) - $this->per_page);
            $output .= $this->last_tag_open . '<a ajax="true" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" href="#!' . $this->base_url . $i . '" ajax-target="' . $this->div . '">' . $this->last_link . '</a>' . $this->last_tag_close . ' ';
        }

        // Kill double slashes.  Note: Sometimes we can end up with a double slash
        // in the penultimate link so we'll kill all double slashes.
        $output = preg_replace("#([^:])//+#", "\\1/", $output);

        // Add the wrapper HTML if exists
        $output = $this->full_tag_open . $output . $this->full_tag_close;

        return $output;
    }

    public function general_links() {
        $query_link = '?true';
        foreach ($_GET as $key => $value) {
            if($key != 'uri' && $key!= 'true'){
                $query_link.='&'.$key.'='.$value;
            }
        }
        // If our item count or per-page total is zero there is no need to continue.
        if ($this->total_rows == 0 OR $this->per_page == 0) {
            return '';
        }

        // Calculate the total number of pages
        $num_pages = ceil($this->total_rows / $this->per_page);

        // Is there only one page? Hm... nothing more to do here then.
        if ($num_pages == 1) {
            return '';
        }

        // Determine the current page number. 

        if ($this->page_query_string === TRUE) {
            if (($this->query_string_segment) != 0) {
                $this->cur_page = ($this->query_string_segment);

                // Prep the current page - no funny business!
                $this->cur_page = (int) $this->cur_page;
            }
        } else {
            if (($this->uri_segment) != 0) {
                $this->cur_page = ($this->uri_segment);

                // Prep the current page - no funny business!
                $this->cur_page = (int) $this->cur_page;
            }
        }

        $this->num_links = (int) $this->num_links;

        if ($this->num_links < 1) {
            show_error('Your number of links must be a positive number.');
        }

        if (!is_numeric($this->cur_page)) {
            $this->cur_page = 0;
        }

        // Is the page number beyond the result range?
        // If so we show the last page
        if ($this->cur_page > $this->total_rows) {
            $this->cur_page = ($num_pages - 1) * $this->per_page;
        }

        $uri_page_number = $this->cur_page;
        $this->cur_page = floor(($this->cur_page / $this->per_page) + 1);

        // Calculate the start and end numbers. These determine
        // which number to start and end the digit links with
        $start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
        $end = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

        // Is pagination being used over GET or POST?  If get, add a per_page query
        // string. If post, add a trailing slash to the base URL if needed
        if ($this->page_query_string === TRUE) {
            $this->base_url = rtrim($this->base_url) . '&amp;' . $this->query_string_segment . '=';
        } else {
            $this->base_url = rtrim($this->base_url, '/') . '/';
        }

        // And here we go...
        $output = '<ul class="pagination">';

        // Render the "First" link
        if ($this->cur_page > ($this->num_links + 1)) {
            $output .= $this->first_tag_open . '<li><a href="' . $this->base_url .$query_link. '">' . $this->first_link . '</a></li>' . $this->first_tag_close;
        }

        // Render the "previous" link
        if ($this->cur_page != 1) {
            $i = $uri_page_number - $this->per_page;
            if ($i == 0)
                $i = '';
            $output .= $this->prev_tag_open . '<li><a href="' . $this->base_url . $i .$query_link. '">' . $this->prev_link . '</a></li>' . $this->prev_tag_close;
        }

        // Write the digit links
        for ($loop = $start - 1; $loop <= $end; $loop++) {
            $i = ($loop * $this->per_page) - $this->per_page;

            if ($i >= 0) {
                if ($this->cur_page == $loop) {
                    $output .= '<li><a href="javascript:void(0)">'.$this->cur_tag_open . $loop . $this->cur_tag_close.'</a></li>'; // Current page
                } else {
                    $n = ($i == 0) ? '' : $i;
                    $output .= $this->num_tag_open . '<li><a href="' . $this->base_url . $n .$query_link. '">' . $loop . '</a></li>' . $this->num_tag_close;
                }
            }
        }

        // Render the "next" link
        if ($this->cur_page < $num_pages) {
            $output .= $this->next_tag_open . '<li><a href="' . $this->base_url . ($this->cur_page * $this->per_page) .$query_link. '">' . $this->next_link . '</a></li>' . $this->next_tag_close;
        }

        // Render the "Last" link
        if (($this->cur_page + $this->num_links) < $num_pages) {
            $i = (($num_pages * $this->per_page) - $this->per_page);
            $output .= $this->last_tag_open . '<li><a href="' . $this->base_url . $i .$query_link. '">' . $this->last_link . '</a></li>' . $this->last_tag_close;
        }

        // Kill double slashes.  Note: Sometimes we can end up with a double slash
        // in the penultimate link so we'll kill all double slashes.
        $output = preg_replace("#([^:])//+#", "\\1/", $output);

        // Add the wrapper HTML if exists
        $output = $this->full_tag_open . $output . $this->full_tag_close;

        return $output.'</ul>';
    }
    
    public function ajax_links() {
        // If our item count or per-page total is zero there is no need to continue.
        if ($this->total_rows == 0 OR $this->per_page == 0) {
            return '';
        }

        // Calculate the total number of pages
        $num_pages = ceil($this->total_rows / $this->per_page);

        // Is there only one page? Hm... nothing more to do here then.
        if ($num_pages == 1) {
            return '';
        }

        // Determine the current page number. 

        if ($this->page_query_string === TRUE) {
            if (($this->query_string_segment) != 0) {
                $this->cur_page = ($this->query_string_segment);

                // Prep the current page - no funny business!
                $this->cur_page = (int) $this->cur_page;
            }
        } else {
            if ($this->uri_segment != 0) {
                $this->cur_page = $this->uri_segment;

                // Prep the current page - no funny business!
                $this->cur_page = (int) $this->cur_page;
            }
        }

        $this->num_links = (int) $this->num_links;

        if ($this->num_links < 1) {
            show_error('Your number of links must be a positive number.');
        }

        if (!is_numeric($this->cur_page)) {
            $this->cur_page = 0;
        }

        // Is the page number beyond the result range?
        // If so we show the last page
        if ($this->cur_page > $this->total_rows) {
            $this->cur_page = ($num_pages - 1) * $this->per_page;
        }

        $uri_page_number = $this->cur_page;
        $this->cur_page = floor(($this->cur_page / $this->per_page) + 1);

        // Calculate the start and end numbers. These determine
        // which number to start and end the digit links with
        $start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
        $end = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

        // Is pagination being used over GET or POST?  If get, add a per_page query
        // string. If post, add a trailing slash to the base URL if needed
        if ($this->page_query_string === TRUE) {
            $this->base_url = rtrim($this->base_url) . '&amp;' . $this->query_string_segment . '=';
        } else {
            $this->base_url = rtrim($this->base_url, '/') . '/';
        }

        if (site_url() != '/') {
            $this->base_url = str_replace(site_url(), '', $this->base_url);
        } else {
            $this->base_url = $this->base_url;
        }

        // And here we go...
        $output = '<ul class="pagination">';

        // Render the "First" link
        if ($this->cur_page > ($this->num_links + 1)) {
            $output .= '<li>'.$this->first_tag_open . '
                            <li><a ajax="true" href="' .site_url($this->base_url) . '" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" ajax-target="' . $this->div . '">' . $this->first_link . '</a>' . $this->first_tag_close.'</li>';
        }

        // Render the "previous" link
        if ($this->cur_page != 1) {
            $i = $uri_page_number - $this->per_page;
            if ($i == 0)
                $i = '';
            $output .= '<li>'.$this->prev_tag_open . '
                            <a ajax="true" href="' . site_url($this->base_url . $i) . '" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" ajax-target="' . $this->div . '">' . $this->prev_link . '</a>' . $this->prev_tag_close.'</li>';
        }

        // Write the digit links
        for ($loop = $start - 1; $loop <= $end; $loop++) {
            $i = ($loop * $this->per_page) - $this->per_page;

            if ($i >= 0) {
                if ($this->cur_page == $loop) {
                    $output .= '<li><a href="javascript:void(0)">'.$this->cur_tag_open . $loop . $this->cur_tag_close.'</a></li>'; // Current page
                } else {
                    $n = ($i == 0) ? '' : $i;
                    $output .= '<li>'.$this->num_tag_open . '<a ajax="true" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" href="' . site_url($this->base_url . $n). '" ajax-target="' . $this->div . '">' . $loop . '</a>' . $this->num_tag_close.'</li>';
                }
            }
        }

        // Render the "next" link
        if ($this->cur_page < $num_pages) {
            $output .= '<li>'.$this->next_tag_open . '<a ajax="true" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" href="' . site_url($this->base_url . ($this->cur_page * $this->per_page)) . '" ajax-target="' . $this->div . '">' . $this->next_link . '</a>'.'</li>';
        }

        // Render the "Last" link
        if (($this->cur_page + $this->num_links) < $num_pages) {
            $i = (($num_pages * $this->per_page) - $this->per_page);
            $output .= '<li>'.$this->last_tag_open . '<a ajax="true" is_close_box="' . $this->close_box . '" url-push="'.$this->url_push.'" href="' . site_url($this->base_url . $i). '" ajax-target="' . $this->div . '">' . $this->last_link . '</a>' . $this->last_tag_close .'</li>';
        }

        // Kill double slashes.  Note: Sometimes we can end up with a double slash
        // in the penultimate link so we'll kill all double slashes.
        $output = preg_replace("#([^:])//+#", "\\1/", $output);

        // Add the wrapper HTML if exists
        $output = $this->full_tag_open . $output . $this->full_tag_close;

        return $output;
    }

}

