<?php echo Modules::run('templates/cliptwo/header'); ?>
<!-- end: HEAD -->
<body>
    <div id="app">
        <?php echo Modules::run('templates/cliptwo/menu'); ?>
        <div class="app-content">
            <?php echo Modules::run('templates/cliptwo/topnavbar'); ?>
            <div class="main-content" >



                <div class="wrap-content container" id="container">

                    <!-- start: YOUR CONTENT HERE -->
                        
                        <div class="row">
                            <div class="col-md-12"> 
                                <iframe width="100%" style="min-height: 1250px" 
                                        src="<?php echo base_url() ?>index.php/config/masterdata/<?php echo $function; ?>">
                                </iframe> 

                            </div>
                        </div>
                </div>
                <!-- end: YOUR CONTENT HERE -->

            </div>
        </div>
        <?php echo Modules::run('templates/cliptwo/footer'); ?>
    </div>
    <?php echo Modules::run('templates/cliptwo/js'); ?>
    <!-- start: JavaScript Event Handlers for this page -->
    <script>
        jQuery(document).ready(function () {
            Main.init();
        });
    </script>
    <!-- end: JavaScript Event Handlers for this page -->
    <!-- end: CLIP-TWO JAVASCRIPTS -->
</body>
</html>
