<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once "notorm-master/NotORM.php";

class Orm extends NotORM {

    function __construct(NotORM_Cache $cache = null) {
        $CI = & get_instance();
        $CI->load->database(); //required to get the db parameters 
        //Please set $active_record = FALSE & $db['default']['autoinit'] = FALSE; 
        //in case you don't intend to use them, for low resource usage and better performance
        $connection = new PDO("mysql:dbname={$CI->db->database};host={$CI->db->hostname}", $CI->db->username, $CI->db->password);

        $structure = new NotORM_Structure_Convention(
                $primary = '%s_id', $foreign = '%s_id', $table = '%s', $prefix = ''
        );
        parent::__construct($connection, $structure, $cache);
    }

}
