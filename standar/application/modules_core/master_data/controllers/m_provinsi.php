<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_provinsi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_provinsi_model','provinsi');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Provinsi";
		$data['subtitle'] = "Daftar Provinsi";
		$this->load->view('provinsi/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Provinsi";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->provinsi->get_by_id($id);
		}
		$this->load->view('provinsi/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->provinsi->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $provinsi) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($provinsi->id_provinsi);
			$row[] = strtoupper($provinsi->nama_provinsi);
			$row[] = ($provinsi->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $provinsi->updated_date?Tanggal::formatDateTime($provinsi->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($provinsi->id_provinsi,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_provinsi('."'".Regex::_genRegex($provinsi->id_provinsi,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->provinsi->count_all(),
						"recordsFiltered" => $this->provinsi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_provinsi = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'id_provinsi' => $id_provinsi,
			'nama_provinsi' => Regex::_genRegex($this->input->post('nama_provinsi'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( !$this->provinsi->get_by_id($id_provinsi) ){
			$this->provinsi->save($dataexc);
		}else{
			$this->provinsi->update(array('id_provinsi'=>$id_provinsi), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->provinsi->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	function get_kab_by_prov($id) {
		
        $this->db->select('id_kabupaten, nama_kabupaten')->where('id_provinsi', $id)->order_by('nama_kabupaten', 'ASC');
        $result = $this->db->get('m_kabupaten')->result_array();
        echo json_encode($result);
        exit;
    }

    function get_btkl_by_prov($id) {
		
        $this->db->select('kode_btkl, nama_btkl')->where('id_provinsi', $id)->order_by('nama_btkl', 'ASC');
        $result = $this->db->get('m_btkl')->result_array();
        echo json_encode($result);
        exit;
    }
}
