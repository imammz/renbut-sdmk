<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tr_puskesmas extends CI_Controller {

	var $table = 't_perencanaan';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('prc_puskesmas_model','prc_puskesmas');
		$this->load->model('master_data/m_jenis_sdmk_model','jenis_sdmk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Perencanaan SDMK Puskesmas";
		$data['subtitle'] = "Entry Data Perencanaan Kebutuhan Minimal SDMK Puskesmas";
		$this->load->view('tr_puskesmas/form_entry_puskesmas', $data);
	}
	
	public function riwayat_perencanaan()
	{
		
		$data['title'] = "Perencanaan SDMK Puskesmas";
		$data['subtitle'] = "Riwayat Perencanaan Kebutuhan Minimal SDMK Puskesmas";
		$this->load->view('tr_puskesmas/riwayat_perencanaan', $data);
	}

	public function form_standar_sdmk($id_perencanaan, $kode_puskesmas, $tahun)
	{
		
		$data['title'] = "Perencanaan SDMK Puskesmas";
		$data['id_jenis_faskes'] = 1;
		$data['subtitle'] = "Riwayat Perencanaan Kebutuhan Minimal SDMK Puskesmas";
		$data['value'] = $this->prc_puskesmas->get_data_by_custom(array('id_perencanaan'=>$id_perencanaan, ''.$this->table.'.kode_puskesmas'=>$kode_puskesmas,''.$this->table.'.tahun'=>$tahun));
		//echo '<pre>';print_r($data['value']);die;
		$this->load->view('tr_puskesmas/form_standar_sdmk', $data);
	}

	public function ajax_list()
	{
		$list = $this->prc_puskesmas->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $prc_puskesmas) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($prc_puskesmas->nama_kabupaten);
			$row[] = strtoupper($prc_puskesmas->nama_puskesmas_kab);
			$row[] = strtoupper($prc_puskesmas->tahun);
			$row[] = $prc_puskesmas->nama_tipe_kawasan;
			$row[] = $prc_puskesmas->nama_tipe_puskesmas;
			$row[] = $prc_puskesmas->jumlah_pustu;
			$row[] = $prc_puskesmas->jumlah_desa;
			/*$row[] = ($prc_puskesmas->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';*/
			/*$row[] = $prc_puskesmas->updated_date?Tanggal::formatDateTime($prc_puskesmas->updated_date):'-';*/

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="form_standar_sdmk('."'".Regex::_genRegex($prc_puskesmas->id_perencanaan,'RGXQSL')."'".','."'".Regex::_genRegex($prc_puskesmas->kode_puskesmas,'RGXQSL')."'".','."'".Regex::_genRegex($prc_puskesmas->tahun,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_prc_puskesmas('."'".Regex::_genRegex($prc_puskesmas->id_perencanaan,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->prc_puskesmas->count_all(),
						"recordsFiltered" => $this->prc_puskesmas->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		//print_r($_POST['submit']);die;

		$this->db->trans_begin();

		// form post header //
		$dataexc = array(
			'tahun' => Regex::_genRegex($this->input->post('tahun'), 'RGXINT'),
			'id_jenis_faskes' => Regex::_genRegex($this->input->post('id_jenis_faskes'), 'RGXINT'),
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT'),
			'id_kabupaten' => Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT'),
			'kode_puskesmas' => Regex::_genRegex($this->input->post('kode_puskesmas'), 'RGXQSL'),
			'id_tipe_kawasan' => Regex::_genRegex($this->input->post('id_tipe_kawasan'), 'RGXINT'),
			'id_tipe_puskesmas' => Regex::_genRegex($this->input->post('id_tipe_puskesmas'), 'RGXINT'),
			'jumlah_pustu' => Regex::_genRegex($this->input->post('jumlah_pustu'), 'RGXINT'),
			'jumlah_desa' => Regex::_genRegex($this->input->post('jumlah_desa'), 'RGXINT'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $this->input->post('submit') == 'save_methode' ){
			// main //
			$this->prc_puskesmas->update(array('id_perencanaan'=>$id_perencanaan), $dataexc);
			$last_id_perencanaan = $id_perencanaan;

			$id_perencanaan = Regex::_genRegex($this->input->post('id'), 'RGXINT');
			$pns = Regex::_genRegex($this->input->post('pns'), 'RGXINT');
			$pppk = Regex::_genRegex($this->input->post('pppk'), 'RGXINT');
			$ptt = Regex::_genRegex($this->input->post('ptt'), 'RGXINT');
			$honorer = Regex::_genRegex($this->input->post('honorer'), 'RGXINT');
			$blud = Regex::_genRegex($this->input->post('blud'), 'RGXINT');
			$tks = Regex::_genRegex($this->input->post('tks'), 'RGXINT');
			$total = Regex::_genRegex($this->input->post('total'), 'RGXINT');
			$jml_standar = Regex::_genRegex($this->input->post('jml_standar'), 'RGXINT');
			$id_jenis_sdmk = Regex::_genRegex($this->input->post('id_jenis_sdmk'), 'RGXINT');
			
			// sub parent //
			$pns_sub = Regex::_genRegex($this->input->post('pns_sub'), 'RGXINT');
			$pppk_sub = Regex::_genRegex($this->input->post('pppk_sub'), 'RGXINT');
			$ptt_sub = Regex::_genRegex($this->input->post('ptt_sub'), 'RGXINT');
			$honorer_sub = Regex::_genRegex($this->input->post('honorer_sub'), 'RGXINT');
			$blud_sub = Regex::_genRegex($this->input->post('blud_sub'), 'RGXINT');
			$tks_sub = Regex::_genRegex($this->input->post('tks_sub'), 'RGXINT');
			$total_sub = Regex::_genRegex($this->input->post('total_sub'), 'RGXINT');
			$jml_standar_sub = Regex::_genRegex($this->input->post('jml_standar_sub'), 'RGXINT');
			$id_jenis_sdmk_sub = Regex::_genRegex($this->input->post('id_jenis_sdmk_sub'), 'RGXINT');

			// delete existing //
			$this->db->delete('t_detail_perencanaan', array('id_perencanaan'=>$id_perencanaan));
			//print_r($this->db->last_query());die;
			foreach ($pns as $key => $rows_value) {
				# code...
				$insert = array(
					'id_perencanaan' => $id_perencanaan,
					'id_jenis_sdmk' => $id_jenis_sdmk[$key],
					'jml_pns' => $rows_value,
					'jml_pppk' => $pppk[$key],
					'jml_ptt' => $ptt[$key],
					'jml_honorer' => $honorer[$key],
					'jml_blud' => $blud[$key],
					'jml_tks' => $tks[$key],
					'total_jml' => $total[$key],
					'total_standar' => $jml_standar[$key],
					);

				$this->db->insert('t_detail_perencanaan', $insert);

			}

			if( $pns_sub ){
				foreach ($pns_sub as $key2 => $rows_value_sub) {
				# code...
					$insert2 = array(
						'id_perencanaan' => $id_perencanaan,
						'id_jenis_sdmk' => $id_jenis_sdmk_sub[$key2],
						'jml_pns' => $rows_value_sub,
						'jml_pppk' => $pppk_sub[$key2],
						'jml_ptt' => $ptt_sub[$key2],
						'jml_honorer' => $honorer_sub[$key2],
						'jml_blud' => $blud_sub[$key2],
						'jml_tks' => $tks_sub[$key2],
						'total_jml' => $total_sub[$key2],
						'total_standar' => $jml_standar_sub[$key2],
						);

					$this->db->insert('t_detail_perencanaan', $insert2);

				}
			} 
			

		}else{

			$id_perencanaan = Regex::_genRegex($this->input->post('id'), 'RGXINT');

			if( $id_perencanaan == 0 ){

				// cek existing by tahun,kode_puskesmas,tipe puskesmas, tipe kawasan
				$existing = $this->db->get_where('t_perencanaan', array('tahun'=>$dataexc['tahun'], 'kode_puskesmas'=>$dataexc['kode_puskesmas'],'id_tipe_kawasan'=>$dataexc['id_tipe_kawasan'],'id_tipe_puskesmas'=>$dataexc['id_tipe_puskesmas']));
				if( $existing->num_rows() > 0 ){
					$this->prc_puskesmas->update( array('id_perencanaan'=>$existing->row()->id_perencanaan ), $dataexc);
					$last_id_perencanaan = $id_perencanaan;
				} else {
					$this->prc_puskesmas->save($dataexc);
					$last_id_perencanaan = $this->db->insert_id();
				}

			}else{
				$this->prc_puskesmas->update(array('id_perencanaan'=>$id_perencanaan), $dataexc);
				$last_id_perencanaan = $id_perencanaan;
			}

		}

		/*if( $pns_sub ){
				foreach ($pns_sub as $key2 => $rows_value_sub) {
				# code...
					$insert2 = array(
						'id_perencanaan' => $id_perencanaan,
						'id_jenis_sdmk' => $id_jenis_sdmk_sub[$key2],
						'jml_pns' => $rows_value_sub,
						'jml_pppk' => $pppk_sub[$key2],
						'jml_ptt' => $ptt_sub[$key2],
						'jml_honorer' => $honorer_sub[$key2],
						'jml_blud' => $blud_sub[$key2],
						'jml_tks' => $tks_sub[$key2],
						'total_jml' => $total_sub[$key2],
						'total_standar' => $jml_standar_sub[$key2],
						);

					$this->db->insert('t_detail_perencanaan', $insert2);

				}
			} */
			

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE ));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array(
				"status" => TRUE,
				"id_perencanaan" => Regex::_genRegex(isset($last_id_perencanaan)?$last_id_perencanaan:$id_perencanaan, 'RGXINT'),
				"tahun" => Regex::_genRegex($this->input->post('tahun'), 'RGXINT'),
				"kode_puskesmas" => Regex::_genRegex($this->input->post('kode_puskesmas'), 'RGXQSL')
				));
		}
		
	}


	public function ajax_delete($id)
	{
		$this->prc_puskesmas->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
