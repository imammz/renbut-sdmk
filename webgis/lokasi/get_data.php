<?php
header('Content-Type: application/json');

require ('../include/connect.php');

$xhr = $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'; 
if (!$xhr){ 
	header('HTTP/1.1 500 Error: Request must come from Ajax!'); 
	exit();	
}

$sql = "SELECT * FROM daerah a,kecamatan b,STATUS c
		WHERE a.id=b.`daerah`
		AND b.`status`=c.`id`
		ORDER BY `a`.`id`";

$data = mysql_query($sql);

$json = '{"indonesia": {';
$json .= '"lahan":[ ';
while($x = mysql_fetch_assoc($data)){
	$json .= '{';
	$json .= '"id":"'.$x['id'].'",
		"nama_lokasi":"'.htmlspecialchars($x['nama_lokasi']).'",
		"nama_daerah":"'.htmlspecialchars($x['nama_daerah']).'",
		"status":"'.$x['nama_status'].'",
		"keterangan":"'.htmlspecialchars($x['keterangan']).'",
		"alamat":"'.htmlspecialchars($x['alamat']).'",
		"polygon":"'.$x['polygon'].'",
		"warna":"'.$x['warna'].'",
		"lat":"'.$x['lat'].'",
		"lng":"'.$x['lng'].'"
	},';
}

$json = substr($json,0,strlen($json)-1);
$json .= ']';
$json .= '}}';

echo $json;

?>
