<?php if (!defined('BASEPATH'))    exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5.3 or newer
 *
 * @package		library/validator
 * @author		xamzo developer
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

class Validator {

    protected $_rules = array();
    protected $_data = array();
    protected $_messages = array();
    protected $_errors = array();
    public $CA;

    public function __construct() {
        $this->CA = get_instance();
        $this->CA->__language(array('validator'));
        $this->setDefaultMessages();
    }

    /**
     * Add a rule
     *
     * @param string $field Field name (index of data to be validated)
     * @param array  $rules Array of rule(s)
     */
    public function addRule($field, array $rules) {
        $this->_rules[$field] = $rules;
    }

    /**
     * Set data to be validated
     *
     * @param array $data   Data to be validated
     */
    public function setData(array $data) {
        $this->_data = $data;
    }

    /**
     * Set error message for rule
     *
     * @param string $rule  Rule name
     * @param string $message   New message
     */
    public function setMessage($rule, $message) {
        $this->_messages[$rule] = $message;
    }

    /**
     * Validates current data with current rules
     *
     * @return boolean
     */
    public function isValid() {
        $valid = true;
        foreach ($this->_rules as $field => $rules) {
            $value = isset($this->_data[$field]) ? $this->_data[$field] : '';

            foreach ($rules as $rule => $parameter) {
                // If rule does not require parameter
                if (is_int($rule)) {
                    $rule = $parameter;
                    $parameter = null;
                }

                if (!$this->check($value, $rule, $parameter)) {
                    $valid = false;

                    if (stripos($this->_messages[$rule], '%s') !== false) {  
                        $this->_errors[$field][] = sprintf($this->_messages[$rule], $parameter); 
                        
                    } else {
                        $this->_errors[$field][] = $this->_messages[$rule];
                    }
                }
            }
        }

        return $valid;
    }

    /**
     * Get error messages if validation fails
     *
     * @return array    Error messages
     */
    public function getErrors() {
        return $this->_errors;
    }

    protected function check($value, $rule, $parameter) {
        switch ($rule) {
            case 'require' :
                return !(trim($value) == '');

            case 'maxlength' :
                return (strlen($value) <= $parameter);

            case 'minlength' :
                return (strlen($value) >= $parameter);

            case 'numeric' :
                return is_numeric($value);

            case 'int' :
                return is_int($value);

            case 'min' :
                return $value > $parameter ? true : false;

            case 'max' :
                return $value < $parameter ? true : false;

            case 'url':
                // Regex taken from symfony
                return preg_match('~^
                  (https?)://                               # protocol
                  (
                ([a-z0-9-]+\.)+[a-z]{2,6}               # a domain name
                  |                                     #  or
                \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}      # a IP address
                  )
                  (:[0-9]+)?                                # a port (optional)
                  (/?|/\S+)                                 # a /, nothing or a / with something
                $~ix', $value);

            case 'email':
                return preg_match('/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i', $value);

            case 'regex':
                return preg_match($parameter, $value);

            case 'pass':
                return true;

            default :
                return false;
        }
    }

    protected function setDefaultMessages() {
        $this->_messages = array(
            'require' => $this->CA->lang['this_field_is_required'],
            'maxlength' => $this->CA->lang['too_long_(%s_characters_max)'],
            'minlength' => $this->CA->lang['too_short_(%s_characters_min)'],
            'numeric' => $this->CA->lang['value_must_be_numeric'],
            'int' => $this->CA->lang['value_must_be_an_integer'],
            'max' => $this->CA->lang['value_must_be_at_most %s'],
            'min' => $this->CA->lang['value_must_be_at_least %s'],
            'url' => $this->CA->lang['value_must_be_a_valid_URL'],
            'email' => $this->CA->lang['value_must_be_a_valid_email'],
            'regex' => $this->CA->lang['invalid_value']
        );
    }

}
