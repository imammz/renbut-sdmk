<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo WEB_TITLE.' '.COPYRIGHT ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url() ?>assets/login/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url() ?>assets/login/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url() ?>assets/login/css/AdminLTE.css" rel="stylesheet" type="text/css" />
		<link rel="icon" href="<?php echo base_url() ?>assets/images/icon.png">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
              html{
            background: url('<?php echo base_url() ?>assets/login/img/blur-background09.jpg') 100%;
        }
        .content-login{
            padding:10px;
            box-shadow: 0px 1px 2px 1px #888;
            background: #f0f0f0;
            margin:10px auto;
        }
        .logo{
            width: 100px;
            height: 100px;
            background: #f0f0f0;
            margin:auto;
            border-radius:50%;
            padding: 10px;
            box-shadow: 0px 1px 2px 1px #888;
        }
        .form{
            width:900px;
            position: fixed;

        }
        </style>
    </head>
    <body>
        
        <div class="form">
          
           
            <div class="text-center"><h3> <img src="<?php echo base_url() ?>assets/login/img/logo_depkes.png" class="logo" >  Registrasi Ulang Sebagai <span class="text-azure"><?php echo $jenis_faskes ?></span>
                    
                </h3>
                <h4> Applikasi Perencanaan Kebutuhan Tenaga Kesehatan (Metode ABK) <span style="color: #bd362f"><?php echo VERSI ?></span> </h4>
            </div>   
             
            <form action="<?php echo base_url() ?>index.php/config/login/proses" method="post" class="content-login">
                
                <div class="form-group col-md-12">
                    <label class="col-md-3 text-right">Nama Pengguna : </label>
                    <strong> <?php echo $_SESSION['user']['nama_akun'] ?> </strong>
                        
                    <input  type="hidden" name="nama_pengguna" value="<?php echo $_SESSION['user']['nama_akun'] ?>"  required />
                </div>
                <div class="form-group col-md-12">
                      <label class="col-md-3 text-right">Password : </label>
                   <input type="password" name="password" class=" col-md-9" required placeholder="masukan password"/>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-3 text-right">Nama Faskes : </label>
                    <input  type="text" name="nama_pengguna" placeholder="Masukan Nama <?php echo $jenis_faskes ?>" class=" col-md-9" required />
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-3 text-right">Telepon : </label>
                    <input  type="text" name="faskes_telp" placeholder="masukan nomor telepon" class=" col-md-3"  />
                    <label class="col-md-2 text-right">Email : </label>
                    <input  type="text" name="faskes_telp" placeholder="masukan email" class=" col-md-3" />
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-3 text-right">Provinsi : </label>
                    <select class=" col-md-9">
                        <option value="">-- Pilih Provinsi --</option>
                        <?php foreach($this->orm->ref_provinsi() as $row) { ?>
                        <option value="<?php echo $row['NO_PROV'] ?>"> <?php echo $row['NAMA_PROV'] ?> </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-3 text-right">Kabupaten : </label>
                    <select class=" col-md-9">
                        <option value="">-- Pilih Provinsi --</option>
                        <?php foreach($this->orm->ref_kabupaten() as $row) { ?>
                        <option value="<?php echo $row['NO_KAB'] ?>"> <?php echo $row['NAMA_KAB'] ?> </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-12">
                    <label class="col-md-3 text-right">Alamat  : </label>
                    <textarea name="faskes_alamat" class="col-md-9"> </textarea>
                </div>
                
                <div class="form-group">
                    <button type="submit" name="login" class="btn btn-info form-control">Masuk</button>  
                </div> 
            </form>
            <div class="text-center">
            <img width="50%" src="<?php echo base_url() ?>assets/images/didukung.jpg" class="logo-img"></div>
            </div>
        </div>

        <!-- jQuery 2.0.2 -->
         <script type="text/javascript" src="<?php echo base_url() ?>assets/login/js/jquery-2.1.1.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url() ?>assets/login/js/bootstrap.min.js" type="text/javascript"></script>   
        <script src="<?php echo base_url() ?>assets/login/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">

            $('.form').position({
                    my: "center",
                    at: "center",
                    of: window,
                    collision: "fit",
                });
       
       </script>     

    </body>
</html>