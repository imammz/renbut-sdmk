<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perencanaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('perencanaan_model','perencanaan');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Perencanaan";
		$data['subtitle'] = "Daftar Menu Perencanaan";
		$this->load->view('index_main_menu', $data);
	}

	
}
