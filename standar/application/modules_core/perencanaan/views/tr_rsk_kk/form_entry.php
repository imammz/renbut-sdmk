<script type="text/javascript">
   
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsk_kk-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsk_kk-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsk_kk-box'));
                    });

                });
            } else {
                $('#rsk_kk-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsk_kk-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_prc_rsk_kk" action="<?php echo site_url('perencanaan/tr_rsk_kk/ajax_add')?>">
                <br>
                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_perencanaan:'( Auto )'?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                  <label class="control-label col-md-2">Jenis Faskes</label>
                  <div class="col-md-3">
                    <?php echo Master::get_master_jenis_faskes($id_jenis_faskes,'id_jenis_faskes','id_jenis_faskes','form-control','required readonly','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Tahun</label>
                  <div class="col-md-3">
                    <?php echo Master::get_tahun(isset($value)?$value->tahun:'','tahun','tahun','form-control','required','inline');?>
                  </div>
                </div>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','6')) ){?>
                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($value) ? $value->id_provinsi : $this->session->userdata('data_user')->id_provinsi,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','6')) ){?>
                <div class="form-group" id="form-kabupaten" >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                      echo Master::get_change_master_kabupaten(isset($value->id_kabupaten) ? $value->id_kabupaten : ($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '' ,'id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <?php if( in_array($this->session->userdata('data_user')->id_role, array('1','2','3','4','6')) ){?>
                <div class="form-group" id="form-rsk_kk" >
                  <label class="control-label col-md-2">Rumah Sakit</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ; 
                      echo Master::get_change_master_rsu(isset($value) ? $value->kode_rs : $this->session->userdata('data_user')->kode_rs,'kode_rsu','rsk_kk-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>
                <?php }?>

                <div class="form-group" id="alamat_rs" style="display: none;">
                  <label class="control-label col-md-2">&nbsp;</label>
                  <div class="col-md-6">
                    <input name="alamat" id="alamat_rs_form" value=""  class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('A','B','C','D');
                          foreach($katergori_kelas as $row_katergori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" value="<?php echo $row_katergori_kelas?>" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : ''; ?>  />
                            <span class="lbl"> <?php echo $row_katergori_kelas?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3">Jumlah Tempat Tidur</label>
                  <div class="col-md-2">
                    <input name="jumlah_tt" id="jumlah_tt" value="<?php echo isset($value)?$value->jumlah_tt:''?>" class="form-control" type="text">
                  </div>
                </div>
                
                <!-- <div class="form-group">
                  <label class="control-label col-md-3">Jumlah Tempat Tidur</label>
                  <div class="col-md-2">
                    <input name="jumlah_tt" id="jumlah_tt" value="<?php echo isset($value)?$value->name:''?>" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3">Jumlah Ruang Operasi</label>
                  <div class="col-md-2">
                    <input name="jumlah_ro" id="jumlah_ro" value="<?php echo isset($value)?$value->name:''?>" class="form-control" type="text">
                  </div>
                </div> -->

                <?php if(!empty($form_entry_rsk_kk)){ foreach ($form_entry_rsk_kk->result() as $key => $value) {?>
                <div class="form-group">
                  <label class="control-label col-md-3"><?php echo $value->nama_jenis_sdmk?></label>
                  <div class="col-md-2" style="padding-top:10px">
                    <input  name="value[]" id="jml_<?php echo $value->id_jenis_sdmk?>" value="" class="form-control" type="text">
                    <input  name="id_jenis_sdmk_form[]" value="<?php echo $value->id_jenis_sdmk?>" class="form-control" type="hidden">
                  </div>
                </div>
                <?php } }?>

                <div class="form-actions center">

                  <a onclick="getMenu('perencanaan/tr_rsk')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="getMenu('perencanaan/tr_rsk_kk/riwayat_perencanaan')" href="#" class="btn btn-sm btn-warning">
                    <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
                    Lihat Riwayat Perencanaan
                  </a>
                  <button type="reset" onclick="getMenu('perencanaan/tr_rsk_kk')" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" value="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/prc_rsk_kk.js'?>"></script>
