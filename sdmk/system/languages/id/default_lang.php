<?php if (!defined('BASEPATH')) exit('no direct script user allowed');
 
$lang['cancel'] = 'batal';
$lang['upgrade'] = 'upgrade'; 
$lang['go_to_web'] = 'lihat web';
$lang['user_statistic'] = 'statistik user';
$lang['hello'] = 'halo';
$lang['website'] = 'website'; 
$lang['logout'] = 'keluar'; 
$lang['update_available'] = 'update yang tersedia';
$lang['comments_unread'] = 'komentar belum dibaca';
$lang['view_all'] = 'Lihat semua';
/**
 * Heading
 */
$lang['order_notice'] = 'pemberitahuan order';
$lang['click_here'] = 'klik disini'; 
$lang['see_documentation'] = 'lihat dokumentasi';
$lang['search_all_modules_by_name'] = 'Cari semua modul berdasarkan namanya';
/**
 * Tables
 */
$lang['form'] = 'form';
$lang['author'] = 'author';
$lang['create'] = 'buat'; 
$lang['insert'] = 'masukkan'; 
$lang['update'] = 'ubah';
$lang['update_now'] = 'ubah sekarang';
$lang['edit'] = 'ubah';
$lang['back'] 	= 'kembali'; 
$lang['index'] 	= 'index'; 
$lang['upload'] = 'unggah';
$lang['delete'] = 'hapus';
$lang['reset'] 	= 'ulang'; 
$lang['close'] 	= 'keluar'; 
$lang['exit'] 	= 'keluar'; 
$lang['submit'] = 'simpan'; 
$lang['yes'] = 'ya';
$lang['no'] = 'tidak';
$lang['copy'] = 'salin';
$lang['browse'] = 'cari';
$lang['select'] = 'pilih';
$lang['link'] = 'link';
$lang['all'] = 'semua';
$lang['draft'] = 'daftar';
$lang['publish'] = 'Terbitkan';
$lang['preview'] = 'preview';
$lang['search'] = 'cari';
$lang['type'] = 'type';
$lang['search_by_name'] = 'cari berdasarkan nama';
$lang['sort_by_ascending'] = 'urutkan berdasarkan menaik';
$lang['sort_by_descending'] = 'urutkan berdasarkan menurun';
$lang['name'] = 'nama';
$lang['title'] = 'judul'; 
$lang['date'] = 'tanggal';
$lang['url'] = 'url'; 
$lang['permalink'] = 'permalink';
$lang['content'] = 'isi'; 
$lang['keyword'] = 'keyword';
$lang['description'] = 'description';
$lang['email'] = 'email'; 
$lang['writer'] = 'penulis';
$lang['select_all'] = 'pilih semua';
$lang['create_data'] = 'buat data';
$lang['update_data'] = 'ubah data';
$lang['delete_data'] = 'hapus data';
$lang['data_list'] = 'daftar data';
$lang['refresh'] = 'segarkan';
$lang['location'] = 'lokasi';
$lang['country'] = 'negara';
$lang['province'] = 'propinsi';
$lang['regency'] = 'kabupaten';
$lang['utility'] = 'utility';
$lang['slide'] = 'slide';
$lang['gallery'] = 'gallery';
$lang['phone'] = 'phone';
$lang['loker_category'] = 'kategory pekerjaan';
$lang['data_not_found'] = 'tidak ditemukan data';
$lang['experience'] = 'pengalaman';
$lang['education'] = 'pendidikan';
$lang['loker'] = 'loker';
/**
 * Notive
 */
$lang['confirmation'] = 'konfirmasi';
$lang['only'] = 'hanya';
$lang['files_are_allowed'] = 'file diperbolehkan';
$lang['file_max_size'] = 'ukuran maksimal file';
$lang['are_you_sure,_want_to_delete'] = 'apakah kamu yakin, ingin menghapus';
$lang['choose_one_yes_or_no'] = 'pilih salah satu, ya atau tidak';
/**
 * pagging
 */
$lang['showing_1_to'] = 'menampilkan 1 hingga';
$lang['of'] = 'dari';
$lang['entries'] = 'entri';
/*
 * left bar
 */
$lang['dashboard'] = 'dashboard'; 
$lang['page'] = 'halaman';  
$lang['user'] = 'pengguna'; 
$lang['post'] = 'post'; 
$lang['setting'] = 'pengaturan';
$lang['category'] = 'kategori';   
$lang['media'] = 'media'; 
$lang['menu'] = 'menu';  
$lang['module'] = 'modul';
$lang['widget'] = 'widget';
$lang['theme'] = 'tema';
$lang['activity'] = 'aktivitas';
$lang['expand_menu'] = 'menu runtuh';
$lang['extensions'] = 'extensions';
$lang['market'] = 'market';
$lang['community'] = 'komunitas';
$lang['codebin'] = 'codebin';
$lang['loker_category'] = 'Kategory Loker';
/**
 *bottom bar 
 */
$lang['about'] = 'tentang';
$lang['term_of_service'] = 'ketentuan penggunaan';
$lang['privacy_policy'] = 'kebijakan privasi';
$lang['help'] = 'bantuan';