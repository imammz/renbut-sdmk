<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * xamzo
 *
 * An open source application development framework and CMS for PHP 5 or newer
 *
 * @package		helpers/zip
 * @author		xamzo Dev Team
 * @copyright           Copyright (c) 2013, xamzo, Inc.
 * @license		http://xamzo.com/license/
 * @link		http://xamzo.com
 * @since		Version 1.0
 * @filesource
 */

if (!function_exists('unzip')) {

    function unzip($path, $dest = '.') {
        $zip = new ZipArchive;
        if ($zip->open($path) === true) {
            for ($i = 0; $i < $zip->numFiles; $i++) {
                $zip->extractTo($dest, array($zip->getNameIndex($i)));
            }
            return $zip->getNameIndex('0');
            $zip->close();
            return true;
        }
    }

}
/**
 * End of file ./system/helpers/zip_helper.php
 */